{
    "id": "023b8270-ab19-42e1-a7bf-3bce7b122b8a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_proxbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "181df3ad-8ca3-482b-a9ee-a499ed8365a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "023b8270-ab19-42e1-a7bf-3bce7b122b8a",
            "compositeImage": {
                "id": "7cdb046d-9e60-483c-a42e-2812aac020cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "181df3ad-8ca3-482b-a9ee-a499ed8365a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34d2dffe-93ef-4773-810b-38e3719c0d75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "181df3ad-8ca3-482b-a9ee-a499ed8365a9",
                    "LayerId": "d260c35c-11b2-498d-97c9-51dc5b93c363"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "d260c35c-11b2-498d-97c9-51dc5b93c363",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "023b8270-ab19-42e1-a7bf-3bce7b122b8a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}