{
    "id": "c473c164-bbf7-49ef-a2ec-7895bd2419e9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_myu",
    "eventList": [
        {
            "id": "4c3b8224-4437-4a5c-b7d3-df65b6201ea5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c473c164-bbf7-49ef-a2ec-7895bd2419e9"
        },
        {
            "id": "a5ae724a-e75f-411b-b6c0-29bcd5d4395f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c473c164-bbf7-49ef-a2ec-7895bd2419e9"
        },
        {
            "id": "94f0736d-612f-410d-a770-dcd267747fbf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "c473c164-bbf7-49ef-a2ec-7895bd2419e9"
        },
        {
            "id": "d63dcfd5-43d7-42b0-8d9d-a55961ff46d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c473c164-bbf7-49ef-a2ec-7895bd2419e9"
        },
        {
            "id": "a7acd2b9-5262-4322-932d-8ddfe41cb927",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "c473c164-bbf7-49ef-a2ec-7895bd2419e9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "764a5c31-d766-4159-bbb8-1162177a46bb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
    "visible": true
}