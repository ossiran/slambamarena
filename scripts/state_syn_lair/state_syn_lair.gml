live_call();

if(state_new == 1)
{
    with(hurtbox0)
	{
	    xoffset = -12;
	    yoffset = -13;
	    image_xscale  = 24;
	    image_yscale  = 40;
	    xshift  = 0;
	}
	
	with(hurtbox1)
	{
	    xoffset = -8;
	    yoffset = -31;
	    image_xscale  = 16;
	    image_yscale  = 18;
	    xshift = 0;
	}
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -13;
	    image_xscale  = 16;
	    image_yscale  = 40;
	    xshift = 0;
	}
	
	chainmove = 0;
	attackframe0 = 3;
	endstate = 36;
}

//animation
if(synmode > 0)
{
    sprite_index = spr_lsyn_jl;
}
else if(synmode < 0)
{
    sprite_index = spr_dsyn_jl;
}

//animation freeze
if(state_timer > 3 && !hitpausing())
{
    animationdelay ++;
}


//attack
if(state_timer == attackframe0)
{
    hitbox0 = create_hitbox(9,-24,35,15,6.75,0,14,20,11,15,0,0,33,4,"mid",-1);
    attackhurtbox0 = create_hurtbox(8 * image_xscale, -17,22 * image_xscale,16,33,-2);
    hitbox0.hitsound = snd_hitsound1;
    hitbox0.scaling = 1;
    hitbox0.juggle = 0;
}

//chainmove
if(state_timer > 3)
{
    if(input_check(0,0,1,0,0))
    {
        chainmove = "mair"
    }
        
    if(input_check(0,0,0,1,0))
    {
        chainmove = "hair"
    }
}

attack_base(endstate,1,0,1,1,0);