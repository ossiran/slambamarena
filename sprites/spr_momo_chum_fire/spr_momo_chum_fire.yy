{
    "id": "cd66bd29-f84c-432c-8570-587c15970e46",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_chum_fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 120,
    "bbox_left": 24,
    "bbox_right": 141,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e7159ff-e044-470b-9a61-685c438f8266",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "compositeImage": {
                "id": "944bd836-2246-4b95-a64b-2bd991aaa107",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e7159ff-e044-470b-9a61-685c438f8266",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c139164-f7f7-4fa0-bac5-96c6b144302d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e7159ff-e044-470b-9a61-685c438f8266",
                    "LayerId": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57"
                }
            ]
        },
        {
            "id": "79d72f09-5dfb-4ed3-86b1-fb9341a5dcb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "compositeImage": {
                "id": "5d6dd440-af86-4212-a3ba-a3f187633d4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79d72f09-5dfb-4ed3-86b1-fb9341a5dcb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7964af3e-3704-4fb0-bae5-af93fd081dc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79d72f09-5dfb-4ed3-86b1-fb9341a5dcb5",
                    "LayerId": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57"
                }
            ]
        },
        {
            "id": "783122ec-b446-4867-87ea-5c93c17a2113",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "compositeImage": {
                "id": "efd56792-4ef5-4dfa-83ac-73524c626167",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "783122ec-b446-4867-87ea-5c93c17a2113",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "339524e2-674b-4b57-b014-96a0b46917c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "783122ec-b446-4867-87ea-5c93c17a2113",
                    "LayerId": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57"
                }
            ]
        },
        {
            "id": "c2e81481-2c8e-496e-a18f-7b8a6c8539ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "compositeImage": {
                "id": "bcb45f8d-1977-4a7f-a683-90418d6197aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2e81481-2c8e-496e-a18f-7b8a6c8539ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15ebaf83-5a9f-464a-a81e-3b6f95ba28fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2e81481-2c8e-496e-a18f-7b8a6c8539ca",
                    "LayerId": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57"
                }
            ]
        },
        {
            "id": "ae255095-a655-4805-8b47-cc702634c0a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "compositeImage": {
                "id": "4808a44a-a4c8-4209-8c2e-49ed21df9249",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae255095-a655-4805-8b47-cc702634c0a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da8c4679-6840-41bf-8d30-8380267404f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae255095-a655-4805-8b47-cc702634c0a2",
                    "LayerId": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57"
                }
            ]
        },
        {
            "id": "5551ffee-34a2-4860-a5d6-780d014b051f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "compositeImage": {
                "id": "86825eb7-e845-4357-8738-c7c7c01d9bfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5551ffee-34a2-4860-a5d6-780d014b051f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d240e27-d945-490e-835a-19871c215a93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5551ffee-34a2-4860-a5d6-780d014b051f",
                    "LayerId": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57"
                }
            ]
        },
        {
            "id": "24ff1be3-53ec-428c-9368-d3a39c5d51cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "compositeImage": {
                "id": "784cac45-56c5-4578-a6ef-be98b2fbbe9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24ff1be3-53ec-428c-9368-d3a39c5d51cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7c0c399-20bc-48b9-ad7d-83fab4af5b0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24ff1be3-53ec-428c-9368-d3a39c5d51cb",
                    "LayerId": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57"
                }
            ]
        },
        {
            "id": "4df8a9b4-7023-4196-b5e9-f88a0bb63dbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "compositeImage": {
                "id": "632fb1fc-c09d-4f36-bb21-f66c78e96352",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4df8a9b4-7023-4196-b5e9-f88a0bb63dbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b92f598-df88-4158-9bca-7eb05196d396",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4df8a9b4-7023-4196-b5e9-f88a0bb63dbd",
                    "LayerId": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57"
                }
            ]
        },
        {
            "id": "78b68339-378c-43fc-b1c4-c8fffe611b8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "compositeImage": {
                "id": "11ebf954-135c-451d-918c-fa11949d5bfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78b68339-378c-43fc-b1c4-c8fffe611b8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d44ffb4-ebc4-4408-9a2f-61c454895a23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78b68339-378c-43fc-b1c4-c8fffe611b8f",
                    "LayerId": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57"
                }
            ]
        },
        {
            "id": "7e78a54e-1ac4-4fc0-a599-e54ba298a297",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "compositeImage": {
                "id": "c0ee7e7d-4b23-4827-8696-d9bddceea6c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e78a54e-1ac4-4fc0-a599-e54ba298a297",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02f1a6f3-92a6-4e03-b619-87b14331f40e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e78a54e-1ac4-4fc0-a599-e54ba298a297",
                    "LayerId": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57"
                }
            ]
        },
        {
            "id": "e62a520b-7347-43cc-b3e3-d6ea225d30d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "compositeImage": {
                "id": "c3272473-14da-4968-80be-daa338680c13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e62a520b-7347-43cc-b3e3-d6ea225d30d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b59ff60-b488-42a5-9a9a-1eb50e510d18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e62a520b-7347-43cc-b3e3-d6ea225d30d1",
                    "LayerId": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57"
                }
            ]
        },
        {
            "id": "d964340c-8f39-4280-93a0-0f0b7eae7f22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "compositeImage": {
                "id": "5d124882-f545-440f-a8e2-441491491642",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d964340c-8f39-4280-93a0-0f0b7eae7f22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f500cde1-6647-4dd1-871b-9a4170a12d2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d964340c-8f39-4280-93a0-0f0b7eae7f22",
                    "LayerId": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57"
                }
            ]
        },
        {
            "id": "9da71388-e9eb-449c-aac9-703974a46c01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "compositeImage": {
                "id": "364af933-60b5-4f5e-bcfa-cb371b591d8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9da71388-e9eb-449c-aac9-703974a46c01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e518c795-b9d2-4d4d-b53d-55f1af9f99a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9da71388-e9eb-449c-aac9-703974a46c01",
                    "LayerId": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57"
                }
            ]
        },
        {
            "id": "c4096afd-536f-47f5-9b7a-8bb2bbfd09e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "compositeImage": {
                "id": "d8b31bc0-6a17-4b7d-91ed-df08a3310373",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4096afd-536f-47f5-9b7a-8bb2bbfd09e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad174cd2-3b67-4deb-82f5-dd7dd902dbdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4096afd-536f-47f5-9b7a-8bb2bbfd09e5",
                    "LayerId": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57"
                }
            ]
        },
        {
            "id": "1effaaf8-8344-4da1-a430-98587af71a36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "compositeImage": {
                "id": "c5a97761-daa5-45fb-83fc-76aadaab6991",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1effaaf8-8344-4da1-a430-98587af71a36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d9a0aa4-16bf-4087-897b-f53f809913a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1effaaf8-8344-4da1-a430-98587af71a36",
                    "LayerId": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57"
                }
            ]
        },
        {
            "id": "863a93bb-b124-4161-9ac5-8c92ebf48198",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "compositeImage": {
                "id": "da685876-ac90-43a8-9877-b50096855f23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "863a93bb-b124-4161-9ac5-8c92ebf48198",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fee441eb-1734-4637-893a-46b56dd3b8c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "863a93bb-b124-4161-9ac5-8c92ebf48198",
                    "LayerId": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57"
                }
            ]
        },
        {
            "id": "ea9e20e5-1789-4cef-a3b9-a0c07e9e0118",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "compositeImage": {
                "id": "be8ffaa3-cd0e-4720-b550-55c237badc71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea9e20e5-1789-4cef-a3b9-a0c07e9e0118",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58427384-6e38-4b6a-90a1-a596de8c33ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea9e20e5-1789-4cef-a3b9-a0c07e9e0118",
                    "LayerId": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57"
                }
            ]
        },
        {
            "id": "7f0f4960-688e-44f8-ad05-d13b88175555",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "compositeImage": {
                "id": "2621a0db-8e34-432c-8fc1-5cb2c87456a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f0f4960-688e-44f8-ad05-d13b88175555",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "735af9e3-3bb4-424d-810c-4e329d32bcb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f0f4960-688e-44f8-ad05-d13b88175555",
                    "LayerId": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57"
                }
            ]
        },
        {
            "id": "5275dc66-6ce5-4a12-acac-63d3e88df3e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "compositeImage": {
                "id": "bca3ba8a-9d1b-4482-9967-c5475cd5f2ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5275dc66-6ce5-4a12-acac-63d3e88df3e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55842420-81c7-476c-a6e0-60f9b559ec77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5275dc66-6ce5-4a12-acac-63d3e88df3e2",
                    "LayerId": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57"
                }
            ]
        },
        {
            "id": "20e15ac0-3ea6-42dc-9b76-2325e45feca3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "compositeImage": {
                "id": "8bdd1fe5-da86-4b15-b8b6-ba30c3114f53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20e15ac0-3ea6-42dc-9b76-2325e45feca3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64984d19-11f6-4994-8256-123bde369f10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20e15ac0-3ea6-42dc-9b76-2325e45feca3",
                    "LayerId": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57"
                }
            ]
        },
        {
            "id": "e591073b-6769-42df-a8a1-a1dd015b49f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "compositeImage": {
                "id": "3e08ef4a-ddd7-48cb-9a1c-9c4c054d89d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e591073b-6769-42df-a8a1-a1dd015b49f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1db06b07-21e6-4a8d-b47f-4886827f7720",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e591073b-6769-42df-a8a1-a1dd015b49f7",
                    "LayerId": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57"
                }
            ]
        },
        {
            "id": "a6a6c448-f2af-465a-88d9-73ce9111c7a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "compositeImage": {
                "id": "da346892-b9e6-4397-91a4-9335566dcf1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6a6c448-f2af-465a-88d9-73ce9111c7a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc442a9b-309f-4d21-8551-faefb61ff6a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6a6c448-f2af-465a-88d9-73ce9111c7a7",
                    "LayerId": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57"
                }
            ]
        },
        {
            "id": "a68d99ff-0caf-465f-89c2-2b2092c25a73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "compositeImage": {
                "id": "01eae844-d9c5-4374-aa6e-fbfdf999ecf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a68d99ff-0caf-465f-89c2-2b2092c25a73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72c9c39f-79df-4cf5-bb4b-8d03cd5a3aea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a68d99ff-0caf-465f-89c2-2b2092c25a73",
                    "LayerId": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57"
                }
            ]
        },
        {
            "id": "6b7b789f-7ab6-487e-8edd-59d7be71d2b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "compositeImage": {
                "id": "6e1a1a3c-63d1-4beb-ab68-39226c15c0ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b7b789f-7ab6-487e-8edd-59d7be71d2b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc00b317-3308-4ced-81b5-35f806ebc96a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b7b789f-7ab6-487e-8edd-59d7be71d2b1",
                    "LayerId": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57"
                }
            ]
        },
        {
            "id": "108387c5-32f2-42bf-8b3d-a7a3cc308aea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "compositeImage": {
                "id": "1f6fb533-69a7-428f-935f-1042b99c5f41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "108387c5-32f2-42bf-8b3d-a7a3cc308aea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10c799c4-e1d9-4fe7-8b7a-1b6c56e6af7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "108387c5-32f2-42bf-8b3d-a7a3cc308aea",
                    "LayerId": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57"
                }
            ]
        },
        {
            "id": "96b9ccae-56e0-4284-9f10-c2dbef2dea69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "compositeImage": {
                "id": "9d0815e8-e15a-4f3f-8eb7-7453a2ee2a99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96b9ccae-56e0-4284-9f10-c2dbef2dea69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d066f915-cdc1-4a78-a293-ec0a185ca54c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96b9ccae-56e0-4284-9f10-c2dbef2dea69",
                    "LayerId": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57"
                }
            ]
        },
        {
            "id": "52811253-84bc-4c70-9fd4-9c42c76c36e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "compositeImage": {
                "id": "726f8aef-9676-4096-b24d-bb59f6c20868",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52811253-84bc-4c70-9fd4-9c42c76c36e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c5eed0e-ade8-40ab-8a2c-b7799ae699e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52811253-84bc-4c70-9fd4-9c42c76c36e8",
                    "LayerId": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57"
                }
            ]
        },
        {
            "id": "97495ffc-a155-4228-ae8d-2221edcce04c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "compositeImage": {
                "id": "76396bae-2605-43b4-b404-223ab2fd3a96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97495ffc-a155-4228-ae8d-2221edcce04c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d13b08f1-c0e8-47a0-9fbd-62c64ae9b881",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97495ffc-a155-4228-ae8d-2221edcce04c",
                    "LayerId": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 148,
    "layers": [
        {
            "id": "cced03d9-5372-4a14-9e2d-ae2c9e33ae57",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd66bd29-f84c-432c-8570-587c15970e46",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 148,
    "xorig": 63,
    "yorig": 87
}