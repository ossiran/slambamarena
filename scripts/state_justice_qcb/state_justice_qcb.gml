if(state_new == 1)
{
    with(hurtbox)
	{
	    xoffset = -13;
	    yoffset = -43;
	    image_xscale  = 26;
	    image_yscale  = 75;
	    xshift  = -2;
	}
	
	attackframe0 =  15;
}

sprite_index = justice_medium;

if(state_timer < 5 && owner.otherplayer.hitpause == 0)
{
    x += 5 * image_xscale;
}

if(state_timer == attackframe0)
{
    hitbox0 = create_hitbox(-15,-50,40,30,6,-7,7,25,10,8,0,0,3,0,"mid",0);
    hitbox0.owner = owner;
    hitbox0.following = id;
    hitbox0.hitsound = snd_hit;
    hitbox0.scaling = 0.8;
}

attack_base_genie(45);