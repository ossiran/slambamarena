if(state_new == 1)
{
    	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -43;
	    image_xscale  = 26;
	    image_yscale  = 75;
	    xshift  = -2;
	}
	
			with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 16;
	    image_yscale  = 47;
	    xshift = -2
	}
}


sprite_index = spr_bill_idle;
image_xscale = -frontOrBack;

//walk
if(input_check(4,0,0,0,0) || input_check(6,0,0,0,0))
{
	state_switch("walk");
}

//crouch
if(input_check(1,0,0,0,0) || input_check(2,0,0,0,0) || input_check(3,0,0,0,0))
{
     state_switch("crouch");   
}

//friction
vx = Approach(vx,0,ground_fric*2);

//jab
if(input_check(0,1,0,0,0))
{
    state_switch("light");
}

//clight
if(input_check(2,1,0,0,0) || input_check(1,1,0,0,0) || input_check(3,1,0,0,0))
{
    state_switch("clight");
}

//medium
if(input_check(0,0,1,0,0))
{
    state_switch("medium");
}

//cmedium
if(input_check(2,0,1,0,0) || input_check(1,0,1,0,0) || input_check(3,0,1,0,0))
{
    state_switch("cmedium");
}

//heavy
if(input_check(0,0,0,1,0))
{
    state_switch("heavy");
}


//cheavy
if(input_check(2,0,0,1,0))
{
    state_switch("cheavy");
}

//qcf
if(qcf_buffer > 0)
{
    if(input_check(0,0,0,1,0))
    {
        if(ex > 49.9)
        {
            state_switch("qcfh");
        }
        else
        {
            state_switch("qcfm");
        }
    }
    else if(input_check(0,0,1,0,0))
    {
        state_switch("qcfm");
    }
    else if(input_check(0,1,0,0,0))
    {
        state_switch("qcfl");
    }
}

//qcb
if(qcb_buffer > 0)
{
    if(input_check(0,0,0,1,0))
    {
        if(ex > 49.9)
        {
            state_switch("qcbh");
        }
        else
        {
            state_switch("qcbm");
        }
    }
    else if(input_check(0,0,1,0,0))
    {
        state_switch("qcbm");
    }
    else if(input_check(0,1,0,0,0))
    {
        state_switch("qcbl");
    }
}

//srk
if(srk_buffer > 0)
{
    if(input_check(0,0,0,1,0))
    {
        if(ex > 49.9)
        {
            state_switch("srkh");
        }
        else
        {
            state_switch("srkm");
        }
    }
    else if(input_check(0,0,1,0,0))
    {
        state_switch("srkm");
    }
    else if(input_check(0,1,0,0,0))
    {
        state_switch("srkl");
    }
}

if(dqcf_buffer > 0 && ex > 49.9)
{
    if(input_check(0,0,0,1,0))
    {
        state_switch("dqcf");
    }
    else if(input_check(0,0,1,0,0))
    {
        state_switch("dqcf");
    }
    else if(input_check(0,1,0,0,0))
    {
        state_switch("dqcf");
    }
}

/*
//genie
if(input_check(0,0,0,0,1) == 1 && !input_check(0,1,0,0,0) && hitstun == 0 && gxcounter == 0 && gx > 0)
{
    if(gon == 0)
    {
        gxcounter = 15;
        gon = 1;
        gonx = x;
        gonxdist = 16;
        gony = y;
        gonydist = 0;
        gsprite = justice_idle;
    }
    else
    {
        gon = 0;
        gxcounter = 10;
    }
}
*/

//grab
if(input_check(0,1,0,0,1))
{
    state_switch("grab");
}

//dash
//fdash
if(ff_buffer > 0 || (input_check(6, 1, 1, 0, 0)) || (input_check(5, 1, 1, 0, 0)))
{
        state_switch("fdash");
}
//bdash
if(bb_buffer > 0 || (input_check(4, 1, 1, 0, 0)))
{
        state_switch("bdash");
}

//jump
if(input_check(7,0,0,0,0) || input_check(8,0,0,0,0) || input_check(9,0,0,0,0))
{
	state_switch("jumpsquat");
}

//air
if(!onground())
{
	state_switch("air");	
}

genie_attacks();

//gbust
if(input_check(0,1,1,1,0) && ex > 30 && gbust == 0)
{
    ex -= 50;
    gbust = gbustmax;
}


//block
if(blockstun > 0)
{
    if(key_down == 1)
    {
        state_switch("cblock");
    }
    else
    {
        state_switch("sblock")
    }
}

//hitstun
if(hitstun > 0)
{
    state_switch("hitstun");   
}

if(grabbed == 1)
{
 state_switch("grabbed");   
}
