{
    "id": "166b67d4-9b53-4ac6-b2cd-ce56d1f2228b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_myu_airdashF",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 12,
    "bbox_right": 35,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "84bea40c-d252-4ccc-976e-acacc643f58b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "166b67d4-9b53-4ac6-b2cd-ce56d1f2228b",
            "compositeImage": {
                "id": "1af31bfb-c68a-4d07-bfcd-8cf5b9b8d709",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84bea40c-d252-4ccc-976e-acacc643f58b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf820171-529e-4af3-8adc-4758566cb990",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84bea40c-d252-4ccc-976e-acacc643f58b",
                    "LayerId": "16ee3dda-44c6-4c8b-a73a-903ec598a348"
                }
            ]
        },
        {
            "id": "d35b93db-fbbf-4979-9063-33bfa476a6ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "166b67d4-9b53-4ac6-b2cd-ce56d1f2228b",
            "compositeImage": {
                "id": "0b1ca2df-f0d7-4dcf-bff0-ee4eac65e79b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d35b93db-fbbf-4979-9063-33bfa476a6ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "700ef9c5-de41-4d10-a483-4eccbcdef8f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d35b93db-fbbf-4979-9063-33bfa476a6ee",
                    "LayerId": "16ee3dda-44c6-4c8b-a73a-903ec598a348"
                }
            ]
        },
        {
            "id": "7c1aa1ef-c055-4397-9002-cb2f9d2412dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "166b67d4-9b53-4ac6-b2cd-ce56d1f2228b",
            "compositeImage": {
                "id": "32537a71-d9f0-47a6-9de4-f96b63f1b126",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c1aa1ef-c055-4397-9002-cb2f9d2412dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41ca433a-4289-4d4c-bee2-c1611d5894d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c1aa1ef-c055-4397-9002-cb2f9d2412dc",
                    "LayerId": "16ee3dda-44c6-4c8b-a73a-903ec598a348"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "16ee3dda-44c6-4c8b-a73a-903ec598a348",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "166b67d4-9b53-4ac6-b2cd-ce56d1f2228b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 32
}