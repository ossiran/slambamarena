live_call();
if (heat > 50)
{
    if(state_new == 1)
    {
        with(hurtbox0)
    	{
    	    xoffset = -29;
    	    yoffset = -36;
    	    image_xscale  = 58;
    	    image_yscale  = 68;
    	    xshift  = -1;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -15;
    	    yoffset = -61;
    	    image_xscale  = 30;
    	    image_yscale  = 25;
    	    xshift = -3;
    	}
    	
    	
    	with(collisionbox)
    	{
    	    xoffset = -20;
    	    yoffset = -22;
    	    image_xscale  = 40;
    	    image_yscale  = 54;
    	    xshift = 0;
    	}
    	
    	chainmove = 0;
    	attackframe0 = 9;
    	attackframe1 = 11;
    	attackframe2 = 14;
    	endstate = 28   ;
    }
    
    //move
    if(state_timer < attackframe0 && state_timer > 6)
    {
        vx = Approach(vx,12 * image_xscale, 4);
    }
    else
    {
        vx = Approach(vx,0,ground_fric);
    }
    
    //attack
    if(state_timer == attackframe0)
    {
        hitbox0 = create_hitbox(25,-37,74,40,2,1,6,15,7,15,0,0,0,2,"mid",1);
        hitbox1 = create_hitbox(33,-13,73,35,2,1,6,15,7,15,0,0,0,2,"mid",1);
        hitbox2 = create_hitbox(15,-46,65,32,2,1,6,15,7,15,0,0,0,2,"mid",1);
        attackhurtbox0 = create_hurtbox(8 * image_xscale, -17,22 * image_xscale,16,2,-2);
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 0.3;
        hitbox1.hitsound = snd_hitsound1;
        hitbox1.scaling = 0.3;
        hitbox2.hitsound = snd_hitsound1;
        hitbox2.scaling = 0.3;
        hitbox0.juggle = 0;
        hitbox1.juggle = 0;
        hitbox2.juggle = 0;
        hitbox0.chipdamage = 1;
        hitbox1.chipdamage = 1;
        hitbox2.chipdamage = 1;
    }
    
    //attack
    if(state_timer == attackframe1)
    {
        hitbox0 = create_hitbox(25,-37,74,40,2,1,6,15,7,13,0,0,0,2,"mid",1);
        hitbox1 = create_hitbox(33,-13,73,35,2,1,6,15,7,13,0,0,0,2,"mid",1);
        hitbox2 = create_hitbox(15,-46,65,32,2,1,6,15,7,13,0,0,0,2,"mid",1);
        attackhurtbox0 = create_hurtbox(8 * image_xscale, -17,22 * image_xscale,16,2,-2);
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 0.3;
        hitbox1.hitsound = snd_hitsound1;
        hitbox1.scaling = 0.3;
        hitbox2.hitsound = snd_hitsound1;
        hitbox2.scaling = 0.3;
        hitbox0.juggle = 0;
        hitbox1.juggle = 0;
        hitbox2.juggle = 0;
        hitbox0.chipdamage = 1;
        hitbox1.chipdamage = 1;
        hitbox2.chipdamage = 1;
    }
    
    //attack
    if(state_timer == attackframe2)
    {
        hitbox0 = create_hitbox(25,-37,74,40,6,8,8,15,11,11,0,0,0,2,"mid",1);
        hitbox1 = create_hitbox(33,-13,73,35,6,8,8,15,11,11,0,0,0,2,"mid",1);
        hitbox2 = create_hitbox(15,-46,65,32,6,8,8,15,11,11,0,0,0,2,"mid",1);
        attackhurtbox0 = create_hurtbox(8 * image_xscale, -17,22 * image_xscale,16,2,-2);
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 0.2;
        hitbox1.hitsound = snd_hitsound1;
        hitbox1.scaling = 0.2;
        hitbox2.hitsound = snd_hitsound1;
        hitbox2.scaling = 0.2;
        hitbox0.juggle = 0;
        hitbox1.juggle = 0;
        hitbox2.juggle = 0;
        hitbox0.chipdamage = 2;
        hitbox1.chipdamage = 2;
        hitbox2.chipdamage = 2;
    }
    
    if(state_timer > attackframe0 +  8 && state_timer < 20)
    {
        vx = Approach(vx, -3 * image_xscale, 1);
    }
    sprite_index = spr_momo_5h_fire;
    
    //chainmove
    if(state_timer > 2)
    {
        
        if((input_check(2,0,0,1,0) || input_check(1,0,0,1,0) || input_check(3,0,0,1,0)))
        {
            chainmove = "cheavy";
        }
    }
    attack_base(endstate,0,0,1,1,0);
}
else if(heat < -50)
{
    if(state_new == 1)
    {
        with(hurtbox0)
    	{
    	    xoffset = -29;
    	    yoffset = -36;
    	    image_xscale  = 58;
    	    image_yscale  = 68;
    	    xshift  = -1;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -15;
    	    yoffset = -61;
    	    image_xscale  = 30;
    	    image_yscale  = 25;
    	    xshift = -3;
    	}
    	
    	
    	with(collisionbox)
    	{
    	    xoffset = -20;
    	    yoffset = -22;
    	    image_xscale  = 40;
    	    image_yscale  = 54;
    	    xshift = 0;
    	}
    	
    	chainmove = 0;
    	attackframe0 = 9;
    	attackframe1 = 12;
    	endstate = 28   ;
    }
    
    //move
    if(state_timer < attackframe0 && state_timer > 6)
    {
        vx = Approach(vx,12 * image_xscale, 4);
    }
    else
    {
        vx = Approach(vx,0,ground_fric);
    }
    
    //attack
    if(state_timer == attackframe0)
    {
        hitbox0 = create_hitbox(25,-32,90,37,3,1,9,15,6,15,0,0,1,2,"mid","momoheavy1");
        hitbox1 = create_hitbox(33,-10,80,32,3,1,9,15,6,15,0,0,1,2,"mid","momoheavy1");
        hitbox2 = create_hitbox(15,-44,78,17,3,1,9,15,6,15,0,0,1,2,"mid","momoheavy1");
        hitbox3 = create_hitbox(15,-54,50,10,3,1,9,15,6,15,0,0,1,2,"mid","momoheavy1");
        attackhurtbox0 = create_hurtbox(8 * image_xscale, -17,22 * image_xscale,16,2,-2);
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 0.4;
        hitbox1.hitsound = snd_hitsound1;
        hitbox1.scaling = 0.4;
        hitbox2.hitsound = snd_hitsound1;
        hitbox2.scaling = 0.4;
        hitbox0.juggle = 0;
        hitbox1.juggle = 0;
        hitbox2.juggle = 0;
        
    }
    
    //attack
    if(state_timer == attackframe1)
    {
        hitbox0 = create_hitbox(25,-32,90,37,6.5,8,11,15,11,15,0,0,0,3,"mid","momoheavy2");
        hitbox1 = create_hitbox(33,-10,80,32,6.5,8,11,15,11,15,0,0,0,3,"mid","momoheavy2");
        hitbox2 = create_hitbox(15,-44,78,17,6.5,8,11,15,11,15,0,0,0,3,"mid","momoheavy2");
        hitbox3 = create_hitbox(15,-54,50,10,6.5,8,11,15,11,15,0,0,0,3,"mid","momoheavy2");
        attackhurtbox0 = create_hurtbox(8 * image_xscale, -17,22 * image_xscale,16,2,-2);
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 0.4;
        hitbox1.hitsound = snd_hitsound1;
        hitbox1.scaling = 0.4;
        hitbox2.hitsound = snd_hitsound1;
        hitbox2.scaling = 0.4;
        hitbox3.hitsound = snd_hitsound1;
        hitbox3.scaling = 0.4;
        hitbox0.juggle = 0;
        hitbox1.juggle = 0;
        hitbox2.juggle = 0;
        hitbox3.juggle = 0;
        hitbox0.chipdamage = 1;
        hitbox1.chipdamage = 1;
        hitbox2.chipdamage = 1;
        hitbox3.chipdamage = 1;
    }
    
    if(state_timer > attackframe0 +  8 && state_timer < 20)
    {
        vx = Approach(vx, -3 * image_xscale, 1);
    }
    sprite_index = spr_momo_5h_ice;
    
    //chainmove
    if(state_timer > 2)
    {
        
        if((input_check(2,0,0,1,0) || input_check(1,0,0,1,0) || input_check(3,0,0,1,0)))
        {
            chainmove = "cheavy";
        }
    }
    attack_base(endstate,0,0,1,1,0);
}
else
{
    if(state_new == 1)
    {
        with(hurtbox0)
    	{
    	    xoffset = -29;
    	    yoffset = -36;
    	    image_xscale  = 58;
    	    image_yscale  = 68;
    	    xshift  = -1;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -15;
    	    yoffset = -61;
    	    image_xscale  = 30;
    	    image_yscale  = 25;
    	    xshift = -3;
    	}
    	
    	
    	with(collisionbox)
    	{
    	    xoffset = -20;
    	    yoffset = -22;
    	    image_xscale  = 40;
    	    image_yscale  = 54;
    	    xshift = 0;
    	}
    	
    	chainmove = 0;
    	attackframe0 = 9;
    	endstate = 28   ;
    }
    
    //move
    if(state_timer < attackframe0 && state_timer > 6)
    {
        vx = Approach(vx,12 * image_xscale, 4);
    }
    else
    {
        vx = Approach(vx,0,ground_fric);
    }
    
    //attack
    if(state_timer == attackframe0)
    {
        hitbox0 = create_hitbox(25,-32,56,37,7,8,18,17,11,15,0,0,0,4,"mid",1);
        hitbox1 = create_hitbox(33,-11,46,20,7,8,18,17,11,15,0,0,0,4,"mid",1);
        hitbox2 = create_hitbox(15,-39,44,12,7,8,18,17,11,15,0,0,0,4,"mid",1);
        attackhurtbox0 = create_hurtbox(8 * image_xscale, -17,22 * image_xscale,16,2,-2);
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 0.8;
        hitbox1.hitsound = snd_hitsound1;
        hitbox1.scaling = 0.8;
        hitbox2.hitsound = snd_hitsound1;
        hitbox2.scaling = 0.8;
        hitbox0.juggle = 0;
        hitbox1.juggle = 0;
        hitbox2.juggle = 0;
        
    }
    
    if(state_timer > attackframe0 +  8 && state_timer < 20)
    {
        vx = Approach(vx, -3 * image_xscale, 1);
    }
    sprite_index = spr_momo_5h;
    
    //chainmove
    if(state_timer > 2)
    {
        
        if((input_check(2,0,0,1,0) || input_check(1,0,0,1,0) || input_check(3,0,0,1,0)))
        {
            chainmove = "cheavy";
        }
    }
    attack_base(endstate,0,0,1,1,0);
}