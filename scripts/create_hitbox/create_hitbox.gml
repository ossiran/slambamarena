///create_hitbox(xoffset, yoffset, xscale, yscale, xhit, yhit, damage, hitstun, hitpause, blockstun, hitfall, bounce, duration, ex give, mod, group)
//make it now work during hitpause
hitbox = instance_create_depth(other.x+ (argument0 * image_xscale),other.y+argument1,-101,obj_hitbox);
hitbox.owner        =   other.id;
hitbox.image_xscale =   argument2 * image_xscale;
/*
if(hitbox.image_xscale < 0)
{
    hitbox.image_xscale += -1;
}
*/
hitbox.xoffset      =   argument0 * image_xscale;
hitbox.yoffset      =   argument1;
hitbox.image_yscale =   argument3;
hitbox.xhit         =   argument4 * image_xscale;
hitbox.yhit         =   argument5;
hitbox.damage       =   argument6;   
hitbox.hitstun      =   argument7;
hitbox.hitpause     =   argument8;
hitbox.blockstun    =   argument9;
hitbox.hitfall      =   argument10;
hitbox.bounce       =   argument11;
hitbox.duration     =   argument12;
hitbox.exgive       =   argument13;
hitbox.modifier     =   argument14;
hitbox.group        =   argument15;
//hitbox.xshift       =   hurtbox0.xshift - 1;

hitbox.prox = instance_create_depth(hitbox.x,hitbox.y,1,obj_proxbox);
hitbox.prox.owner = hitbox.id;
hitbox.prox.image_xscale = hitbox.image_xscale * 2;
hitbox.prox.image_yscale = 2;

return hitbox;