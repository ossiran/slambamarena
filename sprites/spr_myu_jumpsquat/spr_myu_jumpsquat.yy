{
    "id": "1cdc4028-72ee-4573-8d74-1ee2c595aab4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_myu_jumpsquat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 12,
    "bbox_right": 35,
    "bbox_top": 14,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3bcfad3-33ca-4ed2-9e7f-6336dc3ae2f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cdc4028-72ee-4573-8d74-1ee2c595aab4",
            "compositeImage": {
                "id": "a9bea15c-22a1-4e09-afba-ae9377ff0a5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3bcfad3-33ca-4ed2-9e7f-6336dc3ae2f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81ca13e3-9a0f-45af-bfd1-9286ebc7d098",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3bcfad3-33ca-4ed2-9e7f-6336dc3ae2f3",
                    "LayerId": "20d4b3f3-5c10-4eee-b518-b686733e2b75"
                }
            ]
        },
        {
            "id": "0bfe6eb6-292a-4337-afb0-1f4327068c6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cdc4028-72ee-4573-8d74-1ee2c595aab4",
            "compositeImage": {
                "id": "e4b23863-ced1-4954-a7c3-c3cc47944cb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bfe6eb6-292a-4337-afb0-1f4327068c6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d98c762-7523-49ea-804f-3dd3a3640dc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bfe6eb6-292a-4337-afb0-1f4327068c6f",
                    "LayerId": "20d4b3f3-5c10-4eee-b518-b686733e2b75"
                }
            ]
        },
        {
            "id": "b1a239e9-6606-4724-9655-ef18ec703e05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cdc4028-72ee-4573-8d74-1ee2c595aab4",
            "compositeImage": {
                "id": "1053a517-40b8-4b92-b055-2a08bb3c14cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1a239e9-6606-4724-9655-ef18ec703e05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ed44e49-23fd-42c0-8a30-99be75339ae7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1a239e9-6606-4724-9655-ef18ec703e05",
                    "LayerId": "20d4b3f3-5c10-4eee-b518-b686733e2b75"
                }
            ]
        },
        {
            "id": "dcbfc93c-a246-4339-ae18-57ece908e8fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cdc4028-72ee-4573-8d74-1ee2c595aab4",
            "compositeImage": {
                "id": "827abfca-a2f9-4ec6-b7c9-4eb3ce7cb180",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcbfc93c-a246-4339-ae18-57ece908e8fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6b1246b-12b5-4fb3-bc7c-9355c016e6b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcbfc93c-a246-4339-ae18-57ece908e8fa",
                    "LayerId": "20d4b3f3-5c10-4eee-b518-b686733e2b75"
                }
            ]
        },
        {
            "id": "68468555-fda5-448f-a8f9-6a8d2127c363",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cdc4028-72ee-4573-8d74-1ee2c595aab4",
            "compositeImage": {
                "id": "6ad851b5-4b87-4f46-a793-585c2cb1cd51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68468555-fda5-448f-a8f9-6a8d2127c363",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09eae54a-a5ed-4f37-bae0-c57765cf54b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68468555-fda5-448f-a8f9-6a8d2127c363",
                    "LayerId": "20d4b3f3-5c10-4eee-b518-b686733e2b75"
                }
            ]
        },
        {
            "id": "00031171-5e95-4374-820f-f665488a4a80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cdc4028-72ee-4573-8d74-1ee2c595aab4",
            "compositeImage": {
                "id": "90207aac-a6be-4b57-b303-8568bc846da1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00031171-5e95-4374-820f-f665488a4a80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb049293-73c8-4985-908b-e8d449ec2285",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00031171-5e95-4374-820f-f665488a4a80",
                    "LayerId": "20d4b3f3-5c10-4eee-b518-b686733e2b75"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "20d4b3f3-5c10-4eee-b518-b686733e2b75",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1cdc4028-72ee-4573-8d74-1ee2c595aab4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 32
}