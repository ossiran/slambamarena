{
    "id": "37074093-01c2-483e-ae3b-7745ce356700",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_myu_attack_fjab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 9,
    "bbox_right": 49,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a26b6e6-4bef-4725-9814-2d004e2b0983",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "2b5e69f8-c1d0-4cc9-9d45-d0b17dbbce07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a26b6e6-4bef-4725-9814-2d004e2b0983",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "292b9d15-3cfd-49a4-8f4d-e9790c1df95d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a26b6e6-4bef-4725-9814-2d004e2b0983",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "91e3d5f7-c2e3-400e-a97c-f45499973c44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "6c900a12-3eef-4238-bb6c-aeb9d8a95dd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91e3d5f7-c2e3-400e-a97c-f45499973c44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "394b8e28-6f95-491b-99f2-10991532a8e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91e3d5f7-c2e3-400e-a97c-f45499973c44",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "a3d50148-f4d2-4e69-8fb1-3fb9422d9e23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "aec39b77-f6bd-4e5d-b473-c3a831c17a89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3d50148-f4d2-4e69-8fb1-3fb9422d9e23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32c13d91-631d-49ae-a25a-08b854f2ceac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3d50148-f4d2-4e69-8fb1-3fb9422d9e23",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "4f6bc167-274b-4975-9407-9f93696459a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "7595123c-de46-4b11-a4f1-eb1152ee9f86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f6bc167-274b-4975-9407-9f93696459a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff15d78a-f8a8-40e5-a735-c996f275a8b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f6bc167-274b-4975-9407-9f93696459a7",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "5634fc6a-d4e5-4a3d-9e2c-471b5c16b65b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "2aafaa3a-b1fb-4bf3-a0b8-eeb6f1850bfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5634fc6a-d4e5-4a3d-9e2c-471b5c16b65b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea220075-ed95-4b08-92eb-b8f483d245fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5634fc6a-d4e5-4a3d-9e2c-471b5c16b65b",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "9f810607-793a-4c17-b310-e81c3a7e390e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "2738d92d-244a-4a22-892e-3ddda08dce2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f810607-793a-4c17-b310-e81c3a7e390e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30707541-a821-4f0a-8e2a-2ac86bca5453",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f810607-793a-4c17-b310-e81c3a7e390e",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "eeb040f8-b905-4039-92ef-33351145b801",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "458c4407-41a7-42c7-89e0-fc308f4dc8ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eeb040f8-b905-4039-92ef-33351145b801",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f1eb4c6-322d-4e75-857d-534db7458d9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eeb040f8-b905-4039-92ef-33351145b801",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "7791bf3d-bed0-4eaf-8631-4cf6aeeeb8a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "afabd37a-8d7b-474f-a99c-3f7aa086a5e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7791bf3d-bed0-4eaf-8631-4cf6aeeeb8a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98d4ad29-62d9-4c37-b967-46eabe99ccc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7791bf3d-bed0-4eaf-8631-4cf6aeeeb8a3",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "6dca4700-b91b-4ae5-96c0-0c8e1d2b260f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "abfcd733-f7f5-4f27-8bf5-68ee60c49f18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dca4700-b91b-4ae5-96c0-0c8e1d2b260f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b6e07a0-fbde-4e20-a94a-09781f13be4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dca4700-b91b-4ae5-96c0-0c8e1d2b260f",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "75ecb4a5-1572-4a15-bd14-e20c0f33527b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "d5481212-35de-4f16-ab6f-df939a6b51d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75ecb4a5-1572-4a15-bd14-e20c0f33527b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67916b7a-22a7-4903-888f-66688f8e1f33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75ecb4a5-1572-4a15-bd14-e20c0f33527b",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "c0c1a7f4-d56f-4087-830a-f3ab05544b23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "217200ef-2356-4969-878a-5c57756f6c22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0c1a7f4-d56f-4087-830a-f3ab05544b23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9933708-7be2-43d5-aa83-54f85b6c9799",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0c1a7f4-d56f-4087-830a-f3ab05544b23",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "1c308450-da54-486a-8471-a02029537af0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "4f3da81a-0c54-470a-acd5-e5b6c50ed796",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c308450-da54-486a-8471-a02029537af0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16b310a3-9c54-416c-baf7-f47d3ce8c34a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c308450-da54-486a-8471-a02029537af0",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "9df694df-fba7-47fb-b540-5c9a9dedeacd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "8545d30a-8c63-4faa-8e74-37d613e91405",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9df694df-fba7-47fb-b540-5c9a9dedeacd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b781997-08de-4ccd-89da-c28ac45b1e6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9df694df-fba7-47fb-b540-5c9a9dedeacd",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "c360b818-ee95-4d0c-860b-9a990f5dab20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "ac890e2c-f526-4641-9d9d-8185e3f28e09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c360b818-ee95-4d0c-860b-9a990f5dab20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f621e84-ebb5-4600-a38c-dcfc7abe7d6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c360b818-ee95-4d0c-860b-9a990f5dab20",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "17e007bf-33d0-4c27-a542-572113d81a7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "7979d180-b875-4490-a1d7-f2fe101e561f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17e007bf-33d0-4c27-a542-572113d81a7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d88c11af-359f-47fe-be90-a55f11a7d8ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17e007bf-33d0-4c27-a542-572113d81a7b",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "fbb2b252-6277-447f-81d3-67a2367d760d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "ad8feee3-f9d4-4d93-9f54-2987ca5a49aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbb2b252-6277-447f-81d3-67a2367d760d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e36cb4bf-ece4-4fba-84fb-d25c80c2538e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbb2b252-6277-447f-81d3-67a2367d760d",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "9f477769-907c-498d-af8f-9f0a8716a3f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "10a2dce1-9d8b-4536-a4ed-25a0263f6d26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f477769-907c-498d-af8f-9f0a8716a3f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36132bdb-4937-4da5-bf2a-40ff29073292",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f477769-907c-498d-af8f-9f0a8716a3f7",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "dbd0fbef-c129-469e-a119-e3ff6f4c1616",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "08db3ce9-4172-4bce-a81d-90baebd27b12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbd0fbef-c129-469e-a119-e3ff6f4c1616",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22d4af49-215a-418e-82a8-17ce3dee1890",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbd0fbef-c129-469e-a119-e3ff6f4c1616",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "39158be7-3111-4296-bf1f-4b24705a47b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "15be714d-1970-45eb-8028-56dfded16a63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39158be7-3111-4296-bf1f-4b24705a47b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "809163ec-7dde-406f-9803-e8c51dcda6ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39158be7-3111-4296-bf1f-4b24705a47b4",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "44f1071e-497f-4dee-be08-fb2e01ef09bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "a25b6e02-5c88-4b55-bef3-2dc02b69dd87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44f1071e-497f-4dee-be08-fb2e01ef09bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "037a9896-b660-4318-9ee8-9a803d2884f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44f1071e-497f-4dee-be08-fb2e01ef09bc",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "0ee37dad-c8f5-4736-9094-a1ebd906e69b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "3cba92af-3b15-4a55-902b-fddd7b17dc12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ee37dad-c8f5-4736-9094-a1ebd906e69b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8310ccd1-5f7f-4e7b-bb80-f426322e9cb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ee37dad-c8f5-4736-9094-a1ebd906e69b",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "b728bcee-68e0-4fc3-9a1a-70e91eab1579",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "00b434c3-f6ee-48c3-8402-1edf8910e957",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b728bcee-68e0-4fc3-9a1a-70e91eab1579",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b794672-df81-4021-81fb-9eb7a05e59e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b728bcee-68e0-4fc3-9a1a-70e91eab1579",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "1ebc2e85-2635-4517-81f7-b0cd3fff3918",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "be135043-e142-4376-8e85-d47bf83f32a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ebc2e85-2635-4517-81f7-b0cd3fff3918",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc0ba20f-d0a8-4bdc-ab77-ca722be9906e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ebc2e85-2635-4517-81f7-b0cd3fff3918",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "ac050030-573d-44bb-a8bb-1e1fda5e473e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "0f4b1224-6407-482b-80d0-9ac723701d74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac050030-573d-44bb-a8bb-1e1fda5e473e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f963b420-473a-488a-b60a-6199c60ac3e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac050030-573d-44bb-a8bb-1e1fda5e473e",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "3baacef6-e6f4-4baf-9013-59965ade3776",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "af32de98-f427-478d-b84a-2ebdece66e4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3baacef6-e6f4-4baf-9013-59965ade3776",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0281aa6-8ab3-4ac2-8206-fde7afc1c8e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3baacef6-e6f4-4baf-9013-59965ade3776",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "1a4346c1-ae4f-484d-b336-ae02abb2e481",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "40a19201-51f2-46ed-b334-2eeac4619e6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a4346c1-ae4f-484d-b336-ae02abb2e481",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf53fc74-c55d-4218-b67c-3f6c856fbd8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a4346c1-ae4f-484d-b336-ae02abb2e481",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "aa98fea3-f142-4cfa-ae07-9270f45a46bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "5991bd7a-aa12-47e9-a5da-33b9d8e368ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa98fea3-f142-4cfa-ae07-9270f45a46bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bc55c0d-9d71-40ee-a467-3420275c4011",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa98fea3-f142-4cfa-ae07-9270f45a46bd",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "63c2916a-3431-4360-960f-9e7c6d58bd41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "eb47dd59-8ecf-40bc-9aa0-2edf49887142",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63c2916a-3431-4360-960f-9e7c6d58bd41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ad31832-76d2-4f7b-8784-18fdb380e82e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63c2916a-3431-4360-960f-9e7c6d58bd41",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "807d9e96-7dad-443c-a770-6ac8dabb31b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "d1da62a4-2589-4882-b163-8e8cdbd3f898",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "807d9e96-7dad-443c-a770-6ac8dabb31b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9174224d-88a8-40fa-998c-0a2e85f781b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "807d9e96-7dad-443c-a770-6ac8dabb31b9",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        },
        {
            "id": "690de7f0-6804-4b7b-81a8-87b22fff126f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "compositeImage": {
                "id": "3112d328-5bde-4bc7-90f0-24fdae5874de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "690de7f0-6804-4b7b-81a8-87b22fff126f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ce7044f-4a64-4d6c-abe2-0cb227b2a112",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "690de7f0-6804-4b7b-81a8-87b22fff126f",
                    "LayerId": "1216b375-1c7a-4b3e-8757-49bcffad8d8d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1216b375-1c7a-4b3e-8757-49bcffad8d8d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37074093-01c2-483e-ae3b-7745ce356700",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 24,
    "yorig": 32
}