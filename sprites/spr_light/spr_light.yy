{
    "id": "14578850-9511-403f-8bcd-e7f7e417934d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_light",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 16,
    "bbox_right": 47,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c9b6a9c-6c58-49b1-bc83-598f4a2815a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14578850-9511-403f-8bcd-e7f7e417934d",
            "compositeImage": {
                "id": "8f6ad8d9-982a-4fa7-a78e-b499ada77a2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c9b6a9c-6c58-49b1-bc83-598f4a2815a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5213aea-7cf3-4bbf-9ec2-392d1f56e2b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c9b6a9c-6c58-49b1-bc83-598f4a2815a7",
                    "LayerId": "1d2a713e-0999-476d-a4ab-043903cd5027"
                }
            ]
        },
        {
            "id": "4ab85f39-eac3-4b76-abe8-62768725028f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14578850-9511-403f-8bcd-e7f7e417934d",
            "compositeImage": {
                "id": "fd777dea-3d54-4113-8abf-0a3f3aff412b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ab85f39-eac3-4b76-abe8-62768725028f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "036a5c93-4fe9-4500-a9e6-e15ad74f3838",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ab85f39-eac3-4b76-abe8-62768725028f",
                    "LayerId": "1d2a713e-0999-476d-a4ab-043903cd5027"
                }
            ]
        },
        {
            "id": "fee5d424-ea1e-4523-be70-af41f8421ce5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14578850-9511-403f-8bcd-e7f7e417934d",
            "compositeImage": {
                "id": "132b7244-9584-45ec-9174-fc8a358e61a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fee5d424-ea1e-4523-be70-af41f8421ce5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24fa4c00-9e8e-44ad-8051-e8b4b78a3394",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fee5d424-ea1e-4523-be70-af41f8421ce5",
                    "LayerId": "1d2a713e-0999-476d-a4ab-043903cd5027"
                }
            ]
        },
        {
            "id": "66ecc6bd-280e-4c4a-9b66-460cea6c8455",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14578850-9511-403f-8bcd-e7f7e417934d",
            "compositeImage": {
                "id": "63226893-4e49-49f7-b7d9-a7be535f9724",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66ecc6bd-280e-4c4a-9b66-460cea6c8455",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58faa2bb-1789-4536-86db-5e591191c274",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66ecc6bd-280e-4c4a-9b66-460cea6c8455",
                    "LayerId": "1d2a713e-0999-476d-a4ab-043903cd5027"
                }
            ]
        },
        {
            "id": "db653157-2f50-4cb2-a419-83c11f2b327c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14578850-9511-403f-8bcd-e7f7e417934d",
            "compositeImage": {
                "id": "6e56bfbf-664c-4600-b38d-759d1943d723",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db653157-2f50-4cb2-a419-83c11f2b327c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0aa3242-0299-4f88-b361-794cc4fa4a33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db653157-2f50-4cb2-a419-83c11f2b327c",
                    "LayerId": "1d2a713e-0999-476d-a4ab-043903cd5027"
                }
            ]
        },
        {
            "id": "db8b8a2c-20ae-4326-99b4-162dbd3b5537",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14578850-9511-403f-8bcd-e7f7e417934d",
            "compositeImage": {
                "id": "c890ce24-db31-4181-a279-644f614c880a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db8b8a2c-20ae-4326-99b4-162dbd3b5537",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9058cef-f657-478d-9c8e-5d01ffb2c6e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db8b8a2c-20ae-4326-99b4-162dbd3b5537",
                    "LayerId": "1d2a713e-0999-476d-a4ab-043903cd5027"
                }
            ]
        },
        {
            "id": "ebf5c321-3b71-4c6a-9ffd-64c73cab8066",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14578850-9511-403f-8bcd-e7f7e417934d",
            "compositeImage": {
                "id": "7740e3b7-4883-4203-b975-a9e7778ca8de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebf5c321-3b71-4c6a-9ffd-64c73cab8066",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f42333a9-2124-4158-898a-d75ae6713186",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebf5c321-3b71-4c6a-9ffd-64c73cab8066",
                    "LayerId": "1d2a713e-0999-476d-a4ab-043903cd5027"
                }
            ]
        },
        {
            "id": "44d7066c-27c5-40fb-b330-6e5ff6f2b0fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14578850-9511-403f-8bcd-e7f7e417934d",
            "compositeImage": {
                "id": "87c77079-3721-42de-90b8-66a7a148bd8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44d7066c-27c5-40fb-b330-6e5ff6f2b0fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f448d157-8174-4f9e-b0bb-5ca5d3702605",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44d7066c-27c5-40fb-b330-6e5ff6f2b0fc",
                    "LayerId": "1d2a713e-0999-476d-a4ab-043903cd5027"
                }
            ]
        },
        {
            "id": "7339301a-3750-493f-ad35-c5cb84f78a7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14578850-9511-403f-8bcd-e7f7e417934d",
            "compositeImage": {
                "id": "739e3bcf-184d-4166-97f7-ef130793826b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7339301a-3750-493f-ad35-c5cb84f78a7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2acfc1a3-3728-49e4-a3b2-dd28c5ce643b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7339301a-3750-493f-ad35-c5cb84f78a7a",
                    "LayerId": "1d2a713e-0999-476d-a4ab-043903cd5027"
                }
            ]
        },
        {
            "id": "16df637f-bc03-4e78-92af-baf76288fda7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14578850-9511-403f-8bcd-e7f7e417934d",
            "compositeImage": {
                "id": "afbcc9fc-f9b8-41d2-9a2c-7a0e6843e670",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16df637f-bc03-4e78-92af-baf76288fda7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47452fbb-b970-4302-87ba-97039bab51e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16df637f-bc03-4e78-92af-baf76288fda7",
                    "LayerId": "1d2a713e-0999-476d-a4ab-043903cd5027"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1d2a713e-0999-476d-a4ab-043903cd5027",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14578850-9511-403f-8bcd-e7f7e417934d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}