{
    "id": "a6440d09-f3e2-42cc-ad32-ce0fb7a6936d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_idle",
    "For3D": false,
    "HTile": true,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 52,
    "bbox_right": 88,
    "bbox_top": 75,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b1018c7-d514-4c0c-9111-4c4ced631f3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6440d09-f3e2-42cc-ad32-ce0fb7a6936d",
            "compositeImage": {
                "id": "c20c1ada-30ec-43fd-8485-2dfb2daa27ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b1018c7-d514-4c0c-9111-4c4ced631f3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf981f69-cd4a-4f52-a24b-ac668a85560d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b1018c7-d514-4c0c-9111-4c4ced631f3d",
                    "LayerId": "a3f779bf-bfa5-4028-9a67-bb180f049e8b"
                }
            ]
        },
        {
            "id": "f2fa71db-888a-47ff-848c-7ffccc4e9fcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6440d09-f3e2-42cc-ad32-ce0fb7a6936d",
            "compositeImage": {
                "id": "c4216c24-1a5d-4c59-8d25-310ea5475bc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2fa71db-888a-47ff-848c-7ffccc4e9fcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27d140a0-dcd3-4166-baad-97dec6f26b9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2fa71db-888a-47ff-848c-7ffccc4e9fcd",
                    "LayerId": "a3f779bf-bfa5-4028-9a67-bb180f049e8b"
                }
            ]
        },
        {
            "id": "6ae6dcf9-58da-47ac-a279-c41474e95c64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6440d09-f3e2-42cc-ad32-ce0fb7a6936d",
            "compositeImage": {
                "id": "23ce11f2-1236-4ea5-82bf-e504a468f4ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ae6dcf9-58da-47ac-a279-c41474e95c64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7660649-e7ca-4e04-a279-629ee1cb6bf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ae6dcf9-58da-47ac-a279-c41474e95c64",
                    "LayerId": "a3f779bf-bfa5-4028-9a67-bb180f049e8b"
                }
            ]
        },
        {
            "id": "60a8d123-0c3e-4532-a5ea-d5a23dd50598",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6440d09-f3e2-42cc-ad32-ce0fb7a6936d",
            "compositeImage": {
                "id": "009c2228-9238-48d4-b9be-9f211e8f405b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60a8d123-0c3e-4532-a5ea-d5a23dd50598",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a7168dd-0033-450f-8a44-b876cc526414",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60a8d123-0c3e-4532-a5ea-d5a23dd50598",
                    "LayerId": "a3f779bf-bfa5-4028-9a67-bb180f049e8b"
                }
            ]
        },
        {
            "id": "ce4d4d05-120c-4ad1-99a2-9f01850bbc07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6440d09-f3e2-42cc-ad32-ce0fb7a6936d",
            "compositeImage": {
                "id": "18494385-368b-4483-897d-a0c49c586e0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce4d4d05-120c-4ad1-99a2-9f01850bbc07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "824c363d-cb97-4ad9-b052-0beb2846d4e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce4d4d05-120c-4ad1-99a2-9f01850bbc07",
                    "LayerId": "a3f779bf-bfa5-4028-9a67-bb180f049e8b"
                }
            ]
        },
        {
            "id": "3965c1c5-ec01-4813-afb6-c77b9c30331f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6440d09-f3e2-42cc-ad32-ce0fb7a6936d",
            "compositeImage": {
                "id": "2a2d6d17-6ec6-420a-8249-d5639662bfe5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3965c1c5-ec01-4813-afb6-c77b9c30331f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c1447dc-442f-4e35-a6b9-e4c1bc5704af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3965c1c5-ec01-4813-afb6-c77b9c30331f",
                    "LayerId": "a3f779bf-bfa5-4028-9a67-bb180f049e8b"
                }
            ]
        },
        {
            "id": "800ab80c-8229-4e9d-876c-9a8fda195865",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6440d09-f3e2-42cc-ad32-ce0fb7a6936d",
            "compositeImage": {
                "id": "08ad5249-d336-4687-9975-1ce8336cef7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "800ab80c-8229-4e9d-876c-9a8fda195865",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6007fd7b-8f81-4df1-91d3-74f8a924376d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "800ab80c-8229-4e9d-876c-9a8fda195865",
                    "LayerId": "a3f779bf-bfa5-4028-9a67-bb180f049e8b"
                }
            ]
        },
        {
            "id": "780d9f61-ba77-41a1-83c5-1fb1bbd86493",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6440d09-f3e2-42cc-ad32-ce0fb7a6936d",
            "compositeImage": {
                "id": "40919b96-73b3-4ad5-98c8-01c542f83cbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "780d9f61-ba77-41a1-83c5-1fb1bbd86493",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d558c04a-cb59-4349-a2af-c6b2ab340f25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "780d9f61-ba77-41a1-83c5-1fb1bbd86493",
                    "LayerId": "a3f779bf-bfa5-4028-9a67-bb180f049e8b"
                }
            ]
        },
        {
            "id": "32a76701-325b-4552-9d78-623451ca7424",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6440d09-f3e2-42cc-ad32-ce0fb7a6936d",
            "compositeImage": {
                "id": "580faf23-1b0c-4b9c-95d1-12184ba12ca3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32a76701-325b-4552-9d78-623451ca7424",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bdf8a96-c781-4679-af60-a273283cfe89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32a76701-325b-4552-9d78-623451ca7424",
                    "LayerId": "a3f779bf-bfa5-4028-9a67-bb180f049e8b"
                }
            ]
        },
        {
            "id": "d672c201-7fd9-4537-9094-f675017be4c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6440d09-f3e2-42cc-ad32-ce0fb7a6936d",
            "compositeImage": {
                "id": "69ef3d32-07d0-4440-afef-de43b9b74a51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d672c201-7fd9-4537-9094-f675017be4c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1676fc1d-f9c4-49ee-933f-e525d22bf2c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d672c201-7fd9-4537-9094-f675017be4c3",
                    "LayerId": "a3f779bf-bfa5-4028-9a67-bb180f049e8b"
                }
            ]
        },
        {
            "id": "a3e37b34-5c55-47b1-a5b8-f2a24fec46f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6440d09-f3e2-42cc-ad32-ce0fb7a6936d",
            "compositeImage": {
                "id": "7d8cd296-4e72-4d15-b3bc-f3a961a409f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3e37b34-5c55-47b1-a5b8-f2a24fec46f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c6d172b-8aba-4a3e-89d9-2f1ca8f7d443",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3e37b34-5c55-47b1-a5b8-f2a24fec46f5",
                    "LayerId": "a3f779bf-bfa5-4028-9a67-bb180f049e8b"
                }
            ]
        },
        {
            "id": "20119e11-3936-49e2-b2d8-f3d5a032f903",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6440d09-f3e2-42cc-ad32-ce0fb7a6936d",
            "compositeImage": {
                "id": "4b166345-6cb2-45a3-a9b0-393d5b23ced6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20119e11-3936-49e2-b2d8-f3d5a032f903",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2927977f-c426-4cc8-80b9-39611f5c2884",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20119e11-3936-49e2-b2d8-f3d5a032f903",
                    "LayerId": "a3f779bf-bfa5-4028-9a67-bb180f049e8b"
                }
            ]
        },
        {
            "id": "4a35b4a7-26ac-4f24-bb77-23c72307b15b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6440d09-f3e2-42cc-ad32-ce0fb7a6936d",
            "compositeImage": {
                "id": "6fbc34ea-83b0-435a-a969-d1e152c4baa6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a35b4a7-26ac-4f24-bb77-23c72307b15b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1c78540-c007-44b3-88b4-330a3bba273f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a35b4a7-26ac-4f24-bb77-23c72307b15b",
                    "LayerId": "a3f779bf-bfa5-4028-9a67-bb180f049e8b"
                }
            ]
        },
        {
            "id": "014eda95-2a4e-42db-baf5-61a0a32fa292",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6440d09-f3e2-42cc-ad32-ce0fb7a6936d",
            "compositeImage": {
                "id": "b13aa774-1536-4106-bab7-84d1f98831c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "014eda95-2a4e-42db-baf5-61a0a32fa292",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0d6036e-f60a-4097-93f7-d43d07e2718c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "014eda95-2a4e-42db-baf5-61a0a32fa292",
                    "LayerId": "a3f779bf-bfa5-4028-9a67-bb180f049e8b"
                }
            ]
        },
        {
            "id": "42c49e3b-d8be-4453-9d32-d0db46973576",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6440d09-f3e2-42cc-ad32-ce0fb7a6936d",
            "compositeImage": {
                "id": "02dc6eae-cd40-46f3-8d8a-30f61cc65229",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42c49e3b-d8be-4453-9d32-d0db46973576",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8869f4c-0bb1-450d-b45d-612eeebd7bc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42c49e3b-d8be-4453-9d32-d0db46973576",
                    "LayerId": "a3f779bf-bfa5-4028-9a67-bb180f049e8b"
                }
            ]
        },
        {
            "id": "a7b56477-f361-4879-9ea2-09962569fb5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6440d09-f3e2-42cc-ad32-ce0fb7a6936d",
            "compositeImage": {
                "id": "31df3bc5-e3e0-4d76-88f0-7bfd6486dd9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7b56477-f361-4879-9ea2-09962569fb5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "784575d0-0f60-4eb5-a6b7-703e507522b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7b56477-f361-4879-9ea2-09962569fb5c",
                    "LayerId": "a3f779bf-bfa5-4028-9a67-bb180f049e8b"
                }
            ]
        },
        {
            "id": "82b2e40c-d7dc-472a-a22c-e7c8dd177a90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6440d09-f3e2-42cc-ad32-ce0fb7a6936d",
            "compositeImage": {
                "id": "86bd4a66-422f-4ddc-8b76-bfb9701fb5b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82b2e40c-d7dc-472a-a22c-e7c8dd177a90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0983ec8-efca-4446-8641-9239da10c07d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82b2e40c-d7dc-472a-a22c-e7c8dd177a90",
                    "LayerId": "a3f779bf-bfa5-4028-9a67-bb180f049e8b"
                }
            ]
        },
        {
            "id": "f5caf2ea-ce1c-4556-8410-fc4e3c8075bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6440d09-f3e2-42cc-ad32-ce0fb7a6936d",
            "compositeImage": {
                "id": "22505e74-7fdf-4e08-ba6f-56305dec310e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5caf2ea-ce1c-4556-8410-fc4e3c8075bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ed55766-6834-41a9-b6e8-fc0439f548b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5caf2ea-ce1c-4556-8410-fc4e3c8075bf",
                    "LayerId": "a3f779bf-bfa5-4028-9a67-bb180f049e8b"
                }
            ]
        },
        {
            "id": "2035ff83-cebc-40f5-bf35-fd481597a18e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6440d09-f3e2-42cc-ad32-ce0fb7a6936d",
            "compositeImage": {
                "id": "7a9102b0-3437-49e4-b5c9-808ea2e545ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2035ff83-cebc-40f5-bf35-fd481597a18e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b140782-c862-49fd-bbac-0bf0d534461a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2035ff83-cebc-40f5-bf35-fd481597a18e",
                    "LayerId": "a3f779bf-bfa5-4028-9a67-bb180f049e8b"
                }
            ]
        },
        {
            "id": "090720f7-fabc-4582-bcf7-a758b1032d35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6440d09-f3e2-42cc-ad32-ce0fb7a6936d",
            "compositeImage": {
                "id": "1a996928-c53b-4209-b2a9-916948768a7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "090720f7-fabc-4582-bcf7-a758b1032d35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8f049d7-0864-4bb1-bf69-9a5662e623e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "090720f7-fabc-4582-bcf7-a758b1032d35",
                    "LayerId": "a3f779bf-bfa5-4028-9a67-bb180f049e8b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "a3f779bf-bfa5-4028-9a67-bb180f049e8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a6440d09-f3e2-42cc-ad32-ce0fb7a6936d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}