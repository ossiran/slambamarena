/// @description Insert description here
// You can write your code in this editor
//

//player detection

//make a spawning sequence


//movement
with(owner)
{
    
    if((key_light_hold == 1 && other.mode == "light") || (key_medium_hold == 1 && other.mode == "medium") || (key_heavy_hold == 1 && other.mode == "heavy"))
    {
        with(other)
        {
            if(movementdone == 0)
            {
                if(vx != 0)
                {
                    vx = Approach(vx, vxMax * image_xscale, 0.1);
                }
                    
                if(vy != 0)
                {
                    vy = Approach(vy, vyMax, 0.1);
                    vxMax = Approach(vxMax, 0.5, 0.025);
                }
            }
        }
    }
    else
    {
        other.movementdone = 1;
    }
}

if(movementdone == 1)
{
    vx = Approach(vx, 0, 0.4);
    
    vy = Approach(vy, 0, 0.4)
}

//detecting player
if(abs(vx) < 1.2 && movementdone == 1 && duration == 2)
{
    with(obj_hurtbox)
    {
        with(other)
        {
            if(place_meeting(x,y, other) && other.owner != owner)
            {
                onplayer = 1;
            }
        }
    }
}

//starts duration on player detection
if((onplayer == 1) || (duration > 2))//&& owner.otherplayer.hitpause == 0
{
    duration --;
}


if(duration == 0)
{
    //juggling correctly

    
    if(mode == "heavy")
    {
        hitbox = create_hitbox(-15,-15, 35, 35, 6, -18, 18, 8, 12, 14, -1, 0, 0, 3, "projectilemid", "lightb");
        hitbox.owner = owner;
        hitbox.following = id;
        hitbox.hitsound = snd_hit;
        hitbox.scaling = 2;
        hitbox.projectile = 1;
        hitbox.airxtrahitstun = -4;
    }
    else
    {
        hitbox = create_hitbox(-15,-15, 35, 35, 4, -14, 18, 8, 7, 6, -1, 0, 0, 3, "projectilemid", "lightb");
        hitbox.owner = owner;
        hitbox.following = id;
        hitbox.hitsound = snd_hit;
        hitbox.scaling = 1.5;
        hitbox.projectile = 1;
        hitbox.airxtrahitstun = -4;
    }
}



if(duration == -9)
{
    instance_destroy();
}