if(state_timer == 2)
{
    if(object_exists(obj_controller))
    {
        instance_destroy(obj_controller);
    }
    
    //resetting menu
    menu_maxx = 0;
    menu_maxy = 0;
    //keyboard
    controller0 = instance_create_layer(100,100,"layer1",obj_controller);
    controller0.player_num = 0;
    controller0.controller_num = -1;

    
    //gamepad0
    if(gamepad_is_connected(0))
    {
    	controller1 = instance_create_layer(100,100,"layer1",obj_controller);
    	controller1.player_num = 1;
    	controller1.controller_num = 0;
    }
    
	if(gamepad_is_connected(1))
    {
    	controller2 = instance_create_layer(100,100,"layer1",obj_controller);
    	controller2.player_num = 2;
    	controller2.controller_num = 1;
    }
    
    if(gamepad_is_connected(2))
    {
    	controller3 = instance_create_layer(100,100,"layer1",obj_controller);
    	controller3.player_num = 3;
    	controller3.controller_num = 2;
    }
    
    if(gamepad_is_connected(3))
    {
    	controller4 = instance_create_layer(100,100,"layer1",obj_controller);
    	controller4.player_num = 4;
    	controller4.controller_num = 3;
    }
	
	player1_controller = -2;
	player2_controller = -2;
	
	//resetting menu
    menu_maxx = 0;
    menu_indexx = 4;
    
    //background
    var back_id = layer_background_get_id("Background");
    layer_background_sprite(back_id,spr_select);
}
else if(state_timer > 2)
{
    /*
    //Gamepad 0
    if(gamepad_button_check_pressed(0, gp_start) == 1 || gamepad_button_check_pressed(0, gp_face1) == 1)
    {
    	if(player1_controller == -2)
    	{
    		player1_controller = 0;
    		controller1.controller_num = 0;
    	}
    	if(player2_controller == -2 && player1_controller != 0)
    	{
    		player2_controller = 0;
    		controller2.controller_num = 0;
    	}
    }
    
    
    //Gamepad 1
    if(gamepad_button_check_pressed(1, gp_start) == 1 || gamepad_button_check_pressed(1, gp_face1) == 1)
    {
    	if(player1_controller == -2)
    	{
    		player1_controller = 1;
    		controller1.controller_num = 1;
    		
    	}
    	if(player2_controller == -2  && player1_controller != 1)
    	{
    		player2_controller = 1;
    		controller2.controller_num = 1;
    	}
    }
    //Gamepad 2
    if(gamepad_button_check_pressed(2, gp_start) == 1 || gamepad_button_check_pressed(2, gp_face1) == 1)
    {
    	if(player1_controller == -2)
    	{
    		player1_controller = 2;
    		controller1.controller_num = 2;
    	}
    	if(player2_controller == -2 && player1_controller != 2)
    	{
    		player2_controller = 2;
    		controller2.controller_num = 2;
    	}
    }
    //Gamepad 3
    if(gamepad_button_check_pressed(3, gp_start) == 1 || gamepad_button_check_pressed(3, gp_face1) == 1)
    {
    	if(player1_controller == -2)
    	{
    		player1_controller = 3;
    		controller1.controller_num = 3;
    	}
        if(player2_controller == -2 && player1_controller != 3)
    	{
    		player2_controller = 3;
    		controller2.controller_num = 3;
    	}
    }
    
    //Keyboard
    if(keyboard_check(obj_system.keyboard_start) == 1)
    {
    	if(player1_controller == -2)
    	{
    		player1_controller = -1;
    		controller1.controller_num = -1;
    	}
    	if(player2_controller == -2 && player1_controller != -1)
    	{
    		player2_controller = -1;
    		controller2.controller_num = -1;
    	}
    }
    */
    //make controllers not able to be 2 in same position
    
    //ready up room change
    if(ready_up == 2)
    {
        ready_up = 0;
    	state_switch("char_select2");
    }
    
    if(ready_up == 0)
    {
        with(obj_controller)
        {
            if(key_essence == 1)
            {
                with(other)
                {
                    state_switch("main_menu");
                    instance_destroy(obj_controller);
                }
            }
        }
    }
}