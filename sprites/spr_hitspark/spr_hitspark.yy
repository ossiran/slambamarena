{
    "id": "7f86d40b-455f-4f26-95b7-a21ed55779c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hitspark",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 10,
    "bbox_right": 54,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4e3c8f98-8115-459d-9292-9a78494b9884",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f86d40b-455f-4f26-95b7-a21ed55779c2",
            "compositeImage": {
                "id": "9dfbd787-9e8b-4765-8549-44a9fa8eeb16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e3c8f98-8115-459d-9292-9a78494b9884",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "969014cb-b379-49fd-82a4-095e4dffcf77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e3c8f98-8115-459d-9292-9a78494b9884",
                    "LayerId": "bf20ffb7-f2b6-49a5-8915-de575c87ed55"
                }
            ]
        },
        {
            "id": "10ca4e8c-eb8a-4ed8-83f7-8828899b945f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f86d40b-455f-4f26-95b7-a21ed55779c2",
            "compositeImage": {
                "id": "0f7a249c-a5bf-41dd-8602-b5b8d48a75fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10ca4e8c-eb8a-4ed8-83f7-8828899b945f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61afb6c9-7ba3-449c-a31a-c44868fdf638",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10ca4e8c-eb8a-4ed8-83f7-8828899b945f",
                    "LayerId": "bf20ffb7-f2b6-49a5-8915-de575c87ed55"
                }
            ]
        },
        {
            "id": "ce14452a-6a93-4cc6-b0a1-0a73889c67f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f86d40b-455f-4f26-95b7-a21ed55779c2",
            "compositeImage": {
                "id": "984fbdb5-0eaa-47f2-9b27-70337cbcc7a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce14452a-6a93-4cc6-b0a1-0a73889c67f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8b149fd-2cb4-42e2-b560-b4442b0d350c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce14452a-6a93-4cc6-b0a1-0a73889c67f7",
                    "LayerId": "bf20ffb7-f2b6-49a5-8915-de575c87ed55"
                }
            ]
        },
        {
            "id": "474d0cb1-48e0-4dc6-9294-8af67e5a2707",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f86d40b-455f-4f26-95b7-a21ed55779c2",
            "compositeImage": {
                "id": "5b16f980-ac69-40c1-8210-cafcd5701b91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "474d0cb1-48e0-4dc6-9294-8af67e5a2707",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c62569bd-0055-446c-b0ec-906f2b98e1b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "474d0cb1-48e0-4dc6-9294-8af67e5a2707",
                    "LayerId": "bf20ffb7-f2b6-49a5-8915-de575c87ed55"
                }
            ]
        },
        {
            "id": "00e3043f-ec7f-489c-a959-e0d13bedaed9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f86d40b-455f-4f26-95b7-a21ed55779c2",
            "compositeImage": {
                "id": "11f141fd-6d5b-446f-b827-aa68e0523845",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00e3043f-ec7f-489c-a959-e0d13bedaed9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "244b8fcf-8bad-46dc-814e-3f6978f0f4d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00e3043f-ec7f-489c-a959-e0d13bedaed9",
                    "LayerId": "bf20ffb7-f2b6-49a5-8915-de575c87ed55"
                }
            ]
        },
        {
            "id": "8cd0fe3b-1e1f-42dc-a7a5-1dcb0a11faab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f86d40b-455f-4f26-95b7-a21ed55779c2",
            "compositeImage": {
                "id": "b59637a4-19a8-4941-b9e0-6d80d8c6295a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cd0fe3b-1e1f-42dc-a7a5-1dcb0a11faab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e02db113-56f6-4702-86f2-886376a3fe6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cd0fe3b-1e1f-42dc-a7a5-1dcb0a11faab",
                    "LayerId": "bf20ffb7-f2b6-49a5-8915-de575c87ed55"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bf20ffb7-f2b6-49a5-8915-de575c87ed55",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f86d40b-455f-4f26-95b7-a21ed55779c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 32
}