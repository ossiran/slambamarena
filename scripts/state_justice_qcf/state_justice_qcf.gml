if(state_new == 1)
{
    with(hurtbox)
	{
	    xoffset = -13;
	    yoffset = -43;
	    image_xscale  = 26;
	    image_yscale  = 75;
	    xshift  = -2;
	}
	
	attackframe0 = 18
}

sprite_index = justice_flamebreath;

if(state_timer < 5 && owner.otherplayer.hitpause == 0)
{
    if(image_xscale > 0)
    {
        x += 3;
        if(x > 700)
        {
            x = 700
        }
    }
    else if(image_xscale < 0 )
    {
        x += -3;
        if(x < 100)
        {
            x = 100
        }
    }
}

if(state_timer == attackframe0)
{
    hitbox0 = create_hitbox(18,-33,45,30,2,-4,7,10,5,8,-1,0,1,0,"mid",0);
    hitbox0.owner = owner;
    hitbox0.following = id;
    hitbox0.hitsound = snd_hit;
    hitbox0.scaling = 0.8;
}

if(state_timer == 22)
{
    hitbox0 = create_hitbox(18,-33,45,30,2,-4,7,10,5,8,-1,0,1,0,"mid",0);
    hitbox0.owner = owner;
    hitbox0.following = id;
    hitbox0.hitsound = snd_hit;
    hitbox0.scaling = 0.8;
}

if(state_timer == 26)
{
    hitbox0 = create_hitbox(18,-33,45,30,2,-4,7,10,5,8,-1,0,1,0,"mid",0);
    hitbox0.owner = owner;
    hitbox0.following = id;
    hitbox0.hitsound = snd_hit;
    hitbox0.scaling = 0.8;
}

if(state_timer == 31)
{
    hitbox0 = create_hitbox(18,-33,45,30,2,-4,7,10,5,8,-1,0,1,0,"mid",0);
    hitbox0.owner = owner;
    hitbox0.following = id;
    hitbox0.hitsound = snd_hit;
    hitbox0.scaling = 0.8;
}

attack_base_genie(50);