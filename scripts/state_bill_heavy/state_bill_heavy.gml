if(state_new == 1)
{
	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -43;
	    image_xscale  = 26;
	    image_yscale  = 75;
	    xshift  = -2;
	}
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 16;
	    image_yscale  = 47;
	    xshift = -2;
	}
	
    chainmove = 0;
	attackframe1 = 7;
	endstate = 26;
	
}


sprite_index = spr_bill_heavy;
//attack


if(state_timer == attackframe1)
{
        hitbox1 = create_hitbox(8, -24,36, 16, 6.9,0,18,21,13,12,-1,0,4,4.5,"mid",-1);
        attackhurtbox0 = create_hurtbox(12 * image_xscale, -25,34 * image_xscale,18,4,-2);
        hitbox1.scaling = 1;
        hitbox1.hitsound = snd_hitsound1;
        hitbox1.counterstagger = 1;
        hitbox1.juggle = 1;
}

//chainmove
if(state_timer > 2)
{
    
    if((input_check(2,0,0,1,0) || input_check(1,0,0,1,0) || input_check(3,0,0,1,0)))
    {
        chainmove = "cheavy";
    }
}

attack_base(endstate,0,0,1,1,0);

if(state_timer == attackframe1 - 3)
{
    vx = 4 * image_xscale;
}



