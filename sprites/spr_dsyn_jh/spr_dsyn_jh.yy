{
    "id": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dsyn_jh",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 31,
    "bbox_right": 106,
    "bbox_top": 35,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7fdd0d4-3a62-487c-a2cf-257f54ef3a0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "0737cf5f-60e8-4d5b-a98e-5d36dd08dabf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7fdd0d4-3a62-487c-a2cf-257f54ef3a0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4279b7c-e8b9-422a-a7d2-13ab1f6e451d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7fdd0d4-3a62-487c-a2cf-257f54ef3a0e",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "f0d26b07-f956-4998-9e23-f12d8529f959",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "2124192b-a00e-459d-bce8-8a9b5c1f7f9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0d26b07-f956-4998-9e23-f12d8529f959",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ca25cbc-076a-4b0a-bea6-f1a31552fb40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0d26b07-f956-4998-9e23-f12d8529f959",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "5c125c00-7fa0-4cf6-872a-e8b58df5e9ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "22448d5f-06fa-4b84-bb2d-577363238b5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c125c00-7fa0-4cf6-872a-e8b58df5e9ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ae74d2b-66b2-42fa-906e-5b4e83421752",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c125c00-7fa0-4cf6-872a-e8b58df5e9ed",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "4cdb4e7d-655c-41aa-b583-3c6e36dd0cc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "266722a5-2b21-4caa-b6c7-330ea1226806",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cdb4e7d-655c-41aa-b583-3c6e36dd0cc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ef8ddd9-30f8-473a-8277-4f1050f40e16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cdb4e7d-655c-41aa-b583-3c6e36dd0cc4",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "bf60b76b-595e-4e70-9b00-07a32d65ad65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "9b112327-aba0-4433-bfaf-be8b5cb2bb7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf60b76b-595e-4e70-9b00-07a32d65ad65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b29f4e8f-c46c-458e-9826-ad8505386e4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf60b76b-595e-4e70-9b00-07a32d65ad65",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "51ecd78c-aa7c-4c95-8f4a-14afc06fd8eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "3aabaf97-64f5-42e7-a2b0-f9a1e9a80987",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51ecd78c-aa7c-4c95-8f4a-14afc06fd8eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3387183-e9b5-4529-aaec-f1dc1a8ba2fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51ecd78c-aa7c-4c95-8f4a-14afc06fd8eb",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "208056bd-e802-4c46-9f7b-f8b153c9fa68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "bf307bf4-4bb7-46b6-9dfd-b6a5c8436335",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "208056bd-e802-4c46-9f7b-f8b153c9fa68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5b99fd6-588e-4930-9237-59e1a6d3ed19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "208056bd-e802-4c46-9f7b-f8b153c9fa68",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "7eac13a7-8270-418d-b58a-61bb0f0bfd14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "a5a5b0ce-6cfb-46b0-99ae-eec9676917d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7eac13a7-8270-418d-b58a-61bb0f0bfd14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdadc264-630d-40b1-80b3-a84a96c5c863",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7eac13a7-8270-418d-b58a-61bb0f0bfd14",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "b166bbf3-7398-4c87-a98c-13bccc747450",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "d3bf3055-c5c1-4d60-a9d4-afb4a21c8394",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b166bbf3-7398-4c87-a98c-13bccc747450",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e7bf257-cbdf-418b-a9e7-6c71d803e3ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b166bbf3-7398-4c87-a98c-13bccc747450",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "d6efddd6-8609-429f-8d44-c4046a4c22e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "c94d795d-df53-42be-89f8-3fb49941b71f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6efddd6-8609-429f-8d44-c4046a4c22e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7925b04b-6aeb-4f51-a947-735853055930",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6efddd6-8609-429f-8d44-c4046a4c22e7",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "48eae5be-1cf8-4886-9d38-ad974c66990e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "c96bf8e8-8d36-4b95-9def-1aa74f5e4f2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48eae5be-1cf8-4886-9d38-ad974c66990e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16a0ab22-1ecf-44bc-9b4c-1b3ceb2ff07e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48eae5be-1cf8-4886-9d38-ad974c66990e",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "6ee4b773-dabd-4a26-a6a4-c30cdc6a2f0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "1cba4930-6321-4974-bcf3-51acee9e2b20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ee4b773-dabd-4a26-a6a4-c30cdc6a2f0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61ff29b8-96f7-4947-9caf-ec917ba73a96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ee4b773-dabd-4a26-a6a4-c30cdc6a2f0d",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "f8b21fe4-a64b-4195-bace-e1016c8aec0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "b2fbe88b-1581-4d09-8f59-9b02d74051ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8b21fe4-a64b-4195-bace-e1016c8aec0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a349c2c-9ff5-413a-9bd0-dfa583e04351",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8b21fe4-a64b-4195-bace-e1016c8aec0b",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "7309f0fc-ed21-4a71-abb5-5bf40e852ab0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "f55d707f-3d53-4cbe-8c0f-a470ada14fcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7309f0fc-ed21-4a71-abb5-5bf40e852ab0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "804f5ea4-b8d5-49da-bef9-36c1fd90f312",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7309f0fc-ed21-4a71-abb5-5bf40e852ab0",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "7b8733d7-f9c8-485e-9e21-79c3f664f638",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "0c43dc87-3041-4ad2-8640-67e955eb4107",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b8733d7-f9c8-485e-9e21-79c3f664f638",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3373e575-a6d1-4d83-bede-ca23ed303c2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b8733d7-f9c8-485e-9e21-79c3f664f638",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "553fab9b-4948-47ea-8f7e-4d98c06b6b7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "def28ba9-8408-40f3-ac09-7a05461923e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "553fab9b-4948-47ea-8f7e-4d98c06b6b7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "330dee08-a990-46c3-ba2f-7cd8250f76c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "553fab9b-4948-47ea-8f7e-4d98c06b6b7c",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "9e869ca0-c119-4962-a4a2-0be349f59d6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "c739d780-28df-4ed2-96f5-0ac88578d44a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e869ca0-c119-4962-a4a2-0be349f59d6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35375b00-2aa3-4b65-b8cb-177b865c5aba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e869ca0-c119-4962-a4a2-0be349f59d6b",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "337536f7-a86c-41d5-9757-49299088f1ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "87feb740-b95b-4478-9663-e907780792ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "337536f7-a86c-41d5-9757-49299088f1ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d53b282-fcde-4f77-b8cc-28edd78f6050",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "337536f7-a86c-41d5-9757-49299088f1ae",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "784eaadc-69bd-48b2-8868-8e6c7b42241b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "7dba369a-5c34-4b6a-b7fb-6e0023fd0d5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "784eaadc-69bd-48b2-8868-8e6c7b42241b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "214abfa8-b1d1-4318-9d09-5e26db4e4c38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "784eaadc-69bd-48b2-8868-8e6c7b42241b",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "841d6911-9d61-41cb-8c69-e647cdae0d4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "09e69e40-a06b-4451-a436-f99d2f29525a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "841d6911-9d61-41cb-8c69-e647cdae0d4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e4e2782-4df1-4595-b5f6-cb066b1939d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "841d6911-9d61-41cb-8c69-e647cdae0d4d",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "af5aabc5-445b-4f4c-9737-c476b10cd86e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "a512178f-7663-4ffb-9354-7e93f4878932",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af5aabc5-445b-4f4c-9737-c476b10cd86e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "595c3ab7-f20b-4171-88da-d24ec1778e59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af5aabc5-445b-4f4c-9737-c476b10cd86e",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "9d2260f8-c5a4-4099-916f-2b2ca56cb357",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "1237c7e2-4067-4356-a990-2caf687da103",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d2260f8-c5a4-4099-916f-2b2ca56cb357",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eda68acd-a051-446a-9aad-424a84c9478e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d2260f8-c5a4-4099-916f-2b2ca56cb357",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "2922d4a4-345b-491b-bf77-ca210bb045e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "c9f5bed5-ead1-4065-9236-e27775097456",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2922d4a4-345b-491b-bf77-ca210bb045e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d624582-bac4-4c46-80db-a60c1f719123",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2922d4a4-345b-491b-bf77-ca210bb045e3",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "6faaf783-22e5-4fb9-9843-8b66ad97da15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "baf495bf-5d7f-4dbc-8ad4-91a4afbc515e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6faaf783-22e5-4fb9-9843-8b66ad97da15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e567c15-37c4-4ed8-a233-14bb12e78f34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6faaf783-22e5-4fb9-9843-8b66ad97da15",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "b50a9afd-27d2-46b6-93b3-88600a2d4504",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "e62caad1-5301-4239-b65f-648edf6961c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b50a9afd-27d2-46b6-93b3-88600a2d4504",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cad27190-efff-40a9-8a38-3a21344a38e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b50a9afd-27d2-46b6-93b3-88600a2d4504",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "fe4c9d61-fd8c-4e20-b6fc-006b576a433c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "cd00f3ee-6632-456d-8e53-6b4576ae1848",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe4c9d61-fd8c-4e20-b6fc-006b576a433c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3183c456-7958-485a-9cf6-1c6982460380",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe4c9d61-fd8c-4e20-b6fc-006b576a433c",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "9febc3e8-e8cb-4606-97c1-6eeb553c765b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "70b24679-f5e4-452f-930e-a1d5e22b41c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9febc3e8-e8cb-4606-97c1-6eeb553c765b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d809637-1783-4318-ae95-1fb2459f41b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9febc3e8-e8cb-4606-97c1-6eeb553c765b",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "73f8eb1b-bd5e-4be6-b6b9-74b147c8f74b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "8f3f9bd9-cd24-43f7-8e2f-00634ec4e32d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73f8eb1b-bd5e-4be6-b6b9-74b147c8f74b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "517d105e-c1cd-4a80-89c8-646ee05f9c2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73f8eb1b-bd5e-4be6-b6b9-74b147c8f74b",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "48a966cd-54b6-46d0-9dfd-3b19625257db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "57d6ffdc-10d0-42dc-9405-1a29e74b60bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48a966cd-54b6-46d0-9dfd-3b19625257db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb4b1ce1-2379-443d-84a1-e32bff437cc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48a966cd-54b6-46d0-9dfd-3b19625257db",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "dedefdac-32c7-4f68-91df-42700461c5cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "4d45f916-ae9d-463c-abeb-ed7dc5680200",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dedefdac-32c7-4f68-91df-42700461c5cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98e3e239-e40b-4f9e-8d12-74d7aadfef9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dedefdac-32c7-4f68-91df-42700461c5cf",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "86dc8b3a-014a-4b9f-8028-e58b421fe0da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "bb396136-5b0c-4fb2-bd02-395972533246",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86dc8b3a-014a-4b9f-8028-e58b421fe0da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c7bfb19-3847-48dd-98ef-0299dbe86180",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86dc8b3a-014a-4b9f-8028-e58b421fe0da",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "affdf140-a29f-4dcf-be0a-8bcdb19067bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "c834ec63-fece-41fd-a7ff-12c834c4cd64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "affdf140-a29f-4dcf-be0a-8bcdb19067bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b43d195-a939-4187-a000-a667078c4021",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "affdf140-a29f-4dcf-be0a-8bcdb19067bc",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "a8bf2770-1535-4e74-a941-a8a0d8005c76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "9ae3fbeb-9155-4540-a2bd-b73d71cd51ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8bf2770-1535-4e74-a941-a8a0d8005c76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a31c877b-8996-4a26-b64c-32fe1289b51e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8bf2770-1535-4e74-a941-a8a0d8005c76",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        },
        {
            "id": "389dbc39-a62f-40fa-a449-0451054f1852",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "compositeImage": {
                "id": "eadc316e-e23c-4976-b1b7-570f85e1bfa3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "389dbc39-a62f-40fa-a449-0451054f1852",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00ad9f20-52e1-45fe-a4a8-3ac083907c70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "389dbc39-a62f-40fa-a449-0451054f1852",
                    "LayerId": "2b446309-01ee-4a18-ad74-5dbb01762aac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "2b446309-01ee-4a18-ad74-5dbb01762aac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2538c4a3-9068-4a12-8775-a6d01ee50c8c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}