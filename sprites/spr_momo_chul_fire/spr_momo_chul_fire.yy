{
    "id": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_chul_fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 120,
    "bbox_left": 24,
    "bbox_right": 130,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ba71475f-6043-45c7-80cc-13f9008c590b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "compositeImage": {
                "id": "b290d46b-8757-4472-95bf-abb5e0d01d06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba71475f-6043-45c7-80cc-13f9008c590b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20bbb072-7a6f-4884-863e-f9a3af2a6bb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba71475f-6043-45c7-80cc-13f9008c590b",
                    "LayerId": "fc71aab1-3c48-49d5-827c-4eebd114a703"
                }
            ]
        },
        {
            "id": "60511ff6-cfad-42f1-845d-0dc65d269291",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "compositeImage": {
                "id": "aaf01047-f3e3-4d18-a8e6-9ae4196fff13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60511ff6-cfad-42f1-845d-0dc65d269291",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc365d3c-8e18-4c95-a656-cdb3150ab38d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60511ff6-cfad-42f1-845d-0dc65d269291",
                    "LayerId": "fc71aab1-3c48-49d5-827c-4eebd114a703"
                }
            ]
        },
        {
            "id": "3456ed7c-e6d7-4358-9d98-9083ad660390",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "compositeImage": {
                "id": "2d5ffa71-8f34-4641-b074-3ce1f7b91320",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3456ed7c-e6d7-4358-9d98-9083ad660390",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c073d7ea-98e0-458e-9733-042047146f09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3456ed7c-e6d7-4358-9d98-9083ad660390",
                    "LayerId": "fc71aab1-3c48-49d5-827c-4eebd114a703"
                }
            ]
        },
        {
            "id": "8a668539-be1a-4f52-bec5-5ef48bfd5522",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "compositeImage": {
                "id": "216ef822-2fc7-48f7-8f7a-e6aa068b4ec2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a668539-be1a-4f52-bec5-5ef48bfd5522",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c90940f-ffd5-4179-8fcd-7f5d774e4279",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a668539-be1a-4f52-bec5-5ef48bfd5522",
                    "LayerId": "fc71aab1-3c48-49d5-827c-4eebd114a703"
                }
            ]
        },
        {
            "id": "7ddf0b03-c0e1-4ef6-b615-fa4c0f522c54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "compositeImage": {
                "id": "67872038-57c6-4684-b422-193d377d9868",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ddf0b03-c0e1-4ef6-b615-fa4c0f522c54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dbccb7f-2d8d-485f-90dc-8a70c8150382",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ddf0b03-c0e1-4ef6-b615-fa4c0f522c54",
                    "LayerId": "fc71aab1-3c48-49d5-827c-4eebd114a703"
                }
            ]
        },
        {
            "id": "3b7590a2-6602-4989-916f-1663eb70bee8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "compositeImage": {
                "id": "fbc2a7b0-44d4-43eb-b956-e67b12a47102",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b7590a2-6602-4989-916f-1663eb70bee8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60e8f73f-7c6c-4a54-9342-79949a438e92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b7590a2-6602-4989-916f-1663eb70bee8",
                    "LayerId": "fc71aab1-3c48-49d5-827c-4eebd114a703"
                }
            ]
        },
        {
            "id": "a876d071-65c1-46db-9a8c-591a4e3e0354",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "compositeImage": {
                "id": "492f937a-60c2-44d3-9350-0f5d52f80d65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a876d071-65c1-46db-9a8c-591a4e3e0354",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8086faa-5594-41ea-a0f2-f14bab529708",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a876d071-65c1-46db-9a8c-591a4e3e0354",
                    "LayerId": "fc71aab1-3c48-49d5-827c-4eebd114a703"
                }
            ]
        },
        {
            "id": "2af80b8f-064d-4c31-9d7c-5e923eaba29b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "compositeImage": {
                "id": "58ce8f06-3446-4549-a1ee-99bdb35d18d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2af80b8f-064d-4c31-9d7c-5e923eaba29b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02e59cd4-e113-42c3-b6c7-ed1a0131ffe9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2af80b8f-064d-4c31-9d7c-5e923eaba29b",
                    "LayerId": "fc71aab1-3c48-49d5-827c-4eebd114a703"
                }
            ]
        },
        {
            "id": "879f3a1c-aaf9-40b8-a5d3-7abeec648743",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "compositeImage": {
                "id": "e263bc28-b1a7-4ffe-aee4-3df00e718c2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "879f3a1c-aaf9-40b8-a5d3-7abeec648743",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "245f3c97-9a96-4c66-af12-21ff6d5569a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "879f3a1c-aaf9-40b8-a5d3-7abeec648743",
                    "LayerId": "fc71aab1-3c48-49d5-827c-4eebd114a703"
                }
            ]
        },
        {
            "id": "260d8bc2-e117-4d2a-9c61-5a5a9dd5671b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "compositeImage": {
                "id": "e0803512-2911-4093-9d01-16cead155b4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "260d8bc2-e117-4d2a-9c61-5a5a9dd5671b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6935036-448c-47d8-8c6e-1075813c4545",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "260d8bc2-e117-4d2a-9c61-5a5a9dd5671b",
                    "LayerId": "fc71aab1-3c48-49d5-827c-4eebd114a703"
                }
            ]
        },
        {
            "id": "cc904534-cf7f-4c1a-baf7-535567f1b1b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "compositeImage": {
                "id": "59896a3c-1270-473d-8369-d374675bf3db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc904534-cf7f-4c1a-baf7-535567f1b1b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11d8b222-30cf-4eec-8a0a-b8578e140574",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc904534-cf7f-4c1a-baf7-535567f1b1b6",
                    "LayerId": "fc71aab1-3c48-49d5-827c-4eebd114a703"
                }
            ]
        },
        {
            "id": "678af2ed-d1a9-4d48-9ff5-83ce90b7e67d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "compositeImage": {
                "id": "8d60ad3c-002c-403b-8eae-a071a188dd6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "678af2ed-d1a9-4d48-9ff5-83ce90b7e67d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02ffa44f-0435-4314-8f9b-1a18770c155d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "678af2ed-d1a9-4d48-9ff5-83ce90b7e67d",
                    "LayerId": "fc71aab1-3c48-49d5-827c-4eebd114a703"
                }
            ]
        },
        {
            "id": "769a905f-6b7c-494a-b81f-022fac2fd069",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "compositeImage": {
                "id": "20aa700b-b575-4a21-9f7d-fa6e96dd738a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "769a905f-6b7c-494a-b81f-022fac2fd069",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "239eecba-cefb-48b6-8048-b2476c9b802b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "769a905f-6b7c-494a-b81f-022fac2fd069",
                    "LayerId": "fc71aab1-3c48-49d5-827c-4eebd114a703"
                }
            ]
        },
        {
            "id": "2fb669e9-d239-4f81-b61d-6007afb23b76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "compositeImage": {
                "id": "47ed05ec-43bc-435c-afb8-781e1aed5ad6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fb669e9-d239-4f81-b61d-6007afb23b76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd418f5c-24d6-471f-b19c-1eedcf9bf636",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fb669e9-d239-4f81-b61d-6007afb23b76",
                    "LayerId": "fc71aab1-3c48-49d5-827c-4eebd114a703"
                }
            ]
        },
        {
            "id": "3342e39c-fd48-4a97-9671-7b49288a17e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "compositeImage": {
                "id": "7899c86a-204a-4a19-a28e-70a1e3be4df5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3342e39c-fd48-4a97-9671-7b49288a17e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c19c4d0-4ae5-4aa7-b627-b074f8cc7597",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3342e39c-fd48-4a97-9671-7b49288a17e3",
                    "LayerId": "fc71aab1-3c48-49d5-827c-4eebd114a703"
                }
            ]
        },
        {
            "id": "7ada3438-5ff8-4a1b-92bc-504afcc1c8b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "compositeImage": {
                "id": "fc22a763-d797-43b8-9c06-b75d3d000dc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ada3438-5ff8-4a1b-92bc-504afcc1c8b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac0cd2e1-135b-4717-8b13-8ee07f3d80f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ada3438-5ff8-4a1b-92bc-504afcc1c8b0",
                    "LayerId": "fc71aab1-3c48-49d5-827c-4eebd114a703"
                }
            ]
        },
        {
            "id": "65fcebbd-0b12-4a37-90ca-8c715e64944a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "compositeImage": {
                "id": "4d34b820-0631-40ca-a0cb-6173396496cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65fcebbd-0b12-4a37-90ca-8c715e64944a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fad941ec-b5c6-4564-a729-aaeeeda860dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65fcebbd-0b12-4a37-90ca-8c715e64944a",
                    "LayerId": "fc71aab1-3c48-49d5-827c-4eebd114a703"
                }
            ]
        },
        {
            "id": "57a3dcb6-6a4c-4824-b120-46ad8606cacb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "compositeImage": {
                "id": "ad514bcf-cac8-44ec-a9a6-5dc35a80efaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57a3dcb6-6a4c-4824-b120-46ad8606cacb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f42c4da5-911f-4d40-a1bb-a20a00f8dfbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57a3dcb6-6a4c-4824-b120-46ad8606cacb",
                    "LayerId": "fc71aab1-3c48-49d5-827c-4eebd114a703"
                }
            ]
        },
        {
            "id": "7dcb7159-d40f-4609-952d-4dcc3965d528",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "compositeImage": {
                "id": "cd6fe542-20db-42b3-9a9d-94bf957c49aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dcb7159-d40f-4609-952d-4dcc3965d528",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "293990df-070a-42d8-9be9-4cf6fa091428",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dcb7159-d40f-4609-952d-4dcc3965d528",
                    "LayerId": "fc71aab1-3c48-49d5-827c-4eebd114a703"
                }
            ]
        },
        {
            "id": "2ee009ac-229f-47bc-b03f-f716394812d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "compositeImage": {
                "id": "e4895408-148e-4bb4-bce3-6e4b388023ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ee009ac-229f-47bc-b03f-f716394812d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76df6ba7-e133-4f12-86e5-cc98f7555b9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ee009ac-229f-47bc-b03f-f716394812d8",
                    "LayerId": "fc71aab1-3c48-49d5-827c-4eebd114a703"
                }
            ]
        },
        {
            "id": "23a4bc1b-5690-4462-8044-209606512d4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "compositeImage": {
                "id": "4adc70c4-7533-4893-8856-ec67d9a44533",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23a4bc1b-5690-4462-8044-209606512d4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c848d26e-4fe8-4235-96e7-3c2f3185ec8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23a4bc1b-5690-4462-8044-209606512d4c",
                    "LayerId": "fc71aab1-3c48-49d5-827c-4eebd114a703"
                }
            ]
        },
        {
            "id": "10f23e51-c744-433e-8c3a-55418054eb0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "compositeImage": {
                "id": "c1655ecb-72d6-46fa-9db4-176be0908d43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10f23e51-c744-433e-8c3a-55418054eb0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a9b1646-1148-4b44-8713-dd79f76eb132",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10f23e51-c744-433e-8c3a-55418054eb0b",
                    "LayerId": "fc71aab1-3c48-49d5-827c-4eebd114a703"
                }
            ]
        },
        {
            "id": "06ac1f28-f1d9-478f-ba4c-91776632dce1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "compositeImage": {
                "id": "8fa4d36b-4f71-4e29-a204-7516d0335e69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06ac1f28-f1d9-478f-ba4c-91776632dce1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92020cf6-0069-48ea-a694-5b1ecb2c4ae4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06ac1f28-f1d9-478f-ba4c-91776632dce1",
                    "LayerId": "fc71aab1-3c48-49d5-827c-4eebd114a703"
                }
            ]
        },
        {
            "id": "03b16702-cf9e-4022-a998-b2ceebe41695",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "compositeImage": {
                "id": "424521df-05f6-4ba9-8bf1-712f860ac2ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03b16702-cf9e-4022-a998-b2ceebe41695",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07bc77d4-c20d-465d-9847-a5b3fbb3db25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03b16702-cf9e-4022-a998-b2ceebe41695",
                    "LayerId": "fc71aab1-3c48-49d5-827c-4eebd114a703"
                }
            ]
        },
        {
            "id": "24470f7d-96ed-4afc-bac9-37ae96cae37a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "compositeImage": {
                "id": "796ab9d3-6390-4057-8d36-6e32247a548c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24470f7d-96ed-4afc-bac9-37ae96cae37a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43b1f74b-036a-492b-af2a-67ce0c754392",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24470f7d-96ed-4afc-bac9-37ae96cae37a",
                    "LayerId": "fc71aab1-3c48-49d5-827c-4eebd114a703"
                }
            ]
        },
        {
            "id": "52afcff6-f69b-4ede-aa63-372a5bbbdc65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "compositeImage": {
                "id": "c6b8c970-35f6-42d6-8ecc-52b2697be1a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52afcff6-f69b-4ede-aa63-372a5bbbdc65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7910d61-3d6d-48d7-a597-92ace464738f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52afcff6-f69b-4ede-aa63-372a5bbbdc65",
                    "LayerId": "fc71aab1-3c48-49d5-827c-4eebd114a703"
                }
            ]
        },
        {
            "id": "1c1234b1-5f68-4fbb-b805-b781ab018e7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "compositeImage": {
                "id": "6044a4c4-a8e7-4e63-a41c-8cab618d3819",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c1234b1-5f68-4fbb-b805-b781ab018e7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68cf506f-784e-4d8a-98ce-6b8109b3610e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c1234b1-5f68-4fbb-b805-b781ab018e7b",
                    "LayerId": "fc71aab1-3c48-49d5-827c-4eebd114a703"
                }
            ]
        },
        {
            "id": "85bc5858-5a8e-4815-bdc2-e53cdae16dbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "compositeImage": {
                "id": "9406c1a4-a7b6-431d-b39c-a7e288be6143",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85bc5858-5a8e-4815-bdc2-e53cdae16dbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b218846e-3476-4ab2-8245-23330171c4f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85bc5858-5a8e-4815-bdc2-e53cdae16dbc",
                    "LayerId": "fc71aab1-3c48-49d5-827c-4eebd114a703"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 148,
    "layers": [
        {
            "id": "fc71aab1-3c48-49d5-827c-4eebd114a703",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2b9e38a-a57b-441a-b284-df2e07f664b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 148,
    "xorig": 63,
    "yorig": 87
}