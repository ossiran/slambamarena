{
    "id": "eea3e7e4-2598-4b47-bf92-97cda631f6b8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_myu_walking",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 12,
    "bbox_right": 35,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b435fdd-b8e1-46e1-b237-3159986dc449",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eea3e7e4-2598-4b47-bf92-97cda631f6b8",
            "compositeImage": {
                "id": "12387e8e-3fc1-4c32-804e-6e3aeacce688",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b435fdd-b8e1-46e1-b237-3159986dc449",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c715f95b-8f11-4a57-ab3d-a2eb1a350a2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b435fdd-b8e1-46e1-b237-3159986dc449",
                    "LayerId": "156b442d-a965-4aee-b277-ad7bc73b9f41"
                }
            ]
        },
        {
            "id": "87220158-2a2d-466e-b9d3-2717fba0eea3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eea3e7e4-2598-4b47-bf92-97cda631f6b8",
            "compositeImage": {
                "id": "2dda4aa4-7767-4f89-b7b1-ae8c8d7324aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87220158-2a2d-466e-b9d3-2717fba0eea3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "916c74aa-dc4a-4c84-a786-aa21cf0c8b81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87220158-2a2d-466e-b9d3-2717fba0eea3",
                    "LayerId": "156b442d-a965-4aee-b277-ad7bc73b9f41"
                }
            ]
        },
        {
            "id": "6a478d15-97a5-4c2f-8a41-0d9a0be42fdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eea3e7e4-2598-4b47-bf92-97cda631f6b8",
            "compositeImage": {
                "id": "9e5a96cd-1f5a-4d63-a224-b80f504ad8be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a478d15-97a5-4c2f-8a41-0d9a0be42fdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ee61682-b020-4b0f-9a96-b143a5c3b2c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a478d15-97a5-4c2f-8a41-0d9a0be42fdb",
                    "LayerId": "156b442d-a965-4aee-b277-ad7bc73b9f41"
                }
            ]
        },
        {
            "id": "35587ad5-2478-459a-9407-3f9b2255a839",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eea3e7e4-2598-4b47-bf92-97cda631f6b8",
            "compositeImage": {
                "id": "cf304ae8-1e88-4a5c-9385-fd3a04b211bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35587ad5-2478-459a-9407-3f9b2255a839",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d817c5d-eaec-484f-8501-562e19a237f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35587ad5-2478-459a-9407-3f9b2255a839",
                    "LayerId": "156b442d-a965-4aee-b277-ad7bc73b9f41"
                }
            ]
        },
        {
            "id": "c9e43844-5cdf-4646-9934-dfc51b25be3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eea3e7e4-2598-4b47-bf92-97cda631f6b8",
            "compositeImage": {
                "id": "ff842e7f-161f-42bb-a38d-9406f6bd8239",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9e43844-5cdf-4646-9934-dfc51b25be3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7070a1da-c9ed-4c96-bf9b-55d3663ed6a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9e43844-5cdf-4646-9934-dfc51b25be3d",
                    "LayerId": "156b442d-a965-4aee-b277-ad7bc73b9f41"
                }
            ]
        },
        {
            "id": "94b70ec5-9727-4530-8a9e-84067aef97fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eea3e7e4-2598-4b47-bf92-97cda631f6b8",
            "compositeImage": {
                "id": "17868ddc-3690-4653-8fba-86d4daf7e1d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94b70ec5-9727-4530-8a9e-84067aef97fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5055939-74e3-4524-9669-b2a664f9c339",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94b70ec5-9727-4530-8a9e-84067aef97fb",
                    "LayerId": "156b442d-a965-4aee-b277-ad7bc73b9f41"
                }
            ]
        },
        {
            "id": "cac91c89-8f83-45c9-a318-d59f690bc241",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eea3e7e4-2598-4b47-bf92-97cda631f6b8",
            "compositeImage": {
                "id": "e7e4dec2-ff06-4c24-ba8c-bf77a3ed5026",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cac91c89-8f83-45c9-a318-d59f690bc241",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52607b8f-a3b9-4825-84e2-59c1dd16659d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cac91c89-8f83-45c9-a318-d59f690bc241",
                    "LayerId": "156b442d-a965-4aee-b277-ad7bc73b9f41"
                }
            ]
        },
        {
            "id": "04564c33-8808-46df-bea0-c76ef2cc95c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eea3e7e4-2598-4b47-bf92-97cda631f6b8",
            "compositeImage": {
                "id": "84a78753-de4a-45c0-b11f-63a11c5906d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04564c33-8808-46df-bea0-c76ef2cc95c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cb35b7d-e957-4a8c-beb0-80f384a30158",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04564c33-8808-46df-bea0-c76ef2cc95c8",
                    "LayerId": "156b442d-a965-4aee-b277-ad7bc73b9f41"
                }
            ]
        },
        {
            "id": "33f730bf-880c-45ea-baf5-b8df5c0c39f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eea3e7e4-2598-4b47-bf92-97cda631f6b8",
            "compositeImage": {
                "id": "a6406777-9eea-4e2e-98ed-a0c575cf5822",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33f730bf-880c-45ea-baf5-b8df5c0c39f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03f6fbfa-0946-4549-acf3-c34eefe92d91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33f730bf-880c-45ea-baf5-b8df5c0c39f3",
                    "LayerId": "156b442d-a965-4aee-b277-ad7bc73b9f41"
                }
            ]
        },
        {
            "id": "d805d2f9-8e0e-4ed0-922a-67b95be57eb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eea3e7e4-2598-4b47-bf92-97cda631f6b8",
            "compositeImage": {
                "id": "05f4bdc0-35ba-4f85-b254-a189a4c9e540",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d805d2f9-8e0e-4ed0-922a-67b95be57eb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fe47ccc-db09-40a1-8142-7a45273f5095",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d805d2f9-8e0e-4ed0-922a-67b95be57eb9",
                    "LayerId": "156b442d-a965-4aee-b277-ad7bc73b9f41"
                }
            ]
        },
        {
            "id": "f7f80302-bda5-47a0-bccd-67143f801228",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eea3e7e4-2598-4b47-bf92-97cda631f6b8",
            "compositeImage": {
                "id": "60a4aa3f-7896-49e2-b10f-3ddea06ed811",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7f80302-bda5-47a0-bccd-67143f801228",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6357f2dd-cae1-4e73-ae0f-f4b30d0dd26c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7f80302-bda5-47a0-bccd-67143f801228",
                    "LayerId": "156b442d-a965-4aee-b277-ad7bc73b9f41"
                }
            ]
        },
        {
            "id": "e24cff24-b955-45f5-813a-9fee8cbc0b27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eea3e7e4-2598-4b47-bf92-97cda631f6b8",
            "compositeImage": {
                "id": "23b9ac28-d810-4d5d-ab0a-4d08959d8fa7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e24cff24-b955-45f5-813a-9fee8cbc0b27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7dc4988-d9c3-4573-9863-39f50979311d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e24cff24-b955-45f5-813a-9fee8cbc0b27",
                    "LayerId": "156b442d-a965-4aee-b277-ad7bc73b9f41"
                }
            ]
        },
        {
            "id": "0db1ec8f-b368-4272-afac-1216a574060d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eea3e7e4-2598-4b47-bf92-97cda631f6b8",
            "compositeImage": {
                "id": "cf628e2c-253a-44e6-9923-2df5576ee61c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0db1ec8f-b368-4272-afac-1216a574060d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "638d601c-b197-45e3-a6d7-d83003080481",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0db1ec8f-b368-4272-afac-1216a574060d",
                    "LayerId": "156b442d-a965-4aee-b277-ad7bc73b9f41"
                }
            ]
        },
        {
            "id": "7e5a6d6d-1f74-4eb7-91f1-ba5e8ef52796",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eea3e7e4-2598-4b47-bf92-97cda631f6b8",
            "compositeImage": {
                "id": "54b0de9c-9ba8-42cb-b458-b1c426cf0f6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e5a6d6d-1f74-4eb7-91f1-ba5e8ef52796",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abe7ab82-e55b-4aa2-bdf5-f016c96a0d77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e5a6d6d-1f74-4eb7-91f1-ba5e8ef52796",
                    "LayerId": "156b442d-a965-4aee-b277-ad7bc73b9f41"
                }
            ]
        },
        {
            "id": "321e721e-f17c-42a6-930b-20c767cd99c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eea3e7e4-2598-4b47-bf92-97cda631f6b8",
            "compositeImage": {
                "id": "97ac4863-63a2-44bc-9b2f-ee4dd5e8437c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "321e721e-f17c-42a6-930b-20c767cd99c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbf7ec16-7845-4163-a67e-5758cc0bc873",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "321e721e-f17c-42a6-930b-20c767cd99c9",
                    "LayerId": "156b442d-a965-4aee-b277-ad7bc73b9f41"
                }
            ]
        },
        {
            "id": "c15093d9-6a3b-42ea-a6ee-e4ac9a0ff24c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eea3e7e4-2598-4b47-bf92-97cda631f6b8",
            "compositeImage": {
                "id": "fc6e3076-340a-4e7d-afcf-fec4d526f32f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c15093d9-6a3b-42ea-a6ee-e4ac9a0ff24c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b486c5f3-730d-464c-b787-c9000b65c103",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c15093d9-6a3b-42ea-a6ee-e4ac9a0ff24c",
                    "LayerId": "156b442d-a965-4aee-b277-ad7bc73b9f41"
                }
            ]
        },
        {
            "id": "132b98eb-d91b-411a-971c-a9331cb0e7e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eea3e7e4-2598-4b47-bf92-97cda631f6b8",
            "compositeImage": {
                "id": "e816e409-fc42-4dd2-a6a8-2430f14cc296",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "132b98eb-d91b-411a-971c-a9331cb0e7e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1ffe5a5-65a6-41e7-97c2-f62575063cd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "132b98eb-d91b-411a-971c-a9331cb0e7e0",
                    "LayerId": "156b442d-a965-4aee-b277-ad7bc73b9f41"
                }
            ]
        },
        {
            "id": "15880209-1122-432e-a1ab-79ffd26be719",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eea3e7e4-2598-4b47-bf92-97cda631f6b8",
            "compositeImage": {
                "id": "810569de-7910-473a-abaf-68667c161513",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15880209-1122-432e-a1ab-79ffd26be719",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45b94d2a-dd82-4b57-b0c1-6f97df7d4304",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15880209-1122-432e-a1ab-79ffd26be719",
                    "LayerId": "156b442d-a965-4aee-b277-ad7bc73b9f41"
                }
            ]
        },
        {
            "id": "314b1070-daf5-4e09-adb0-9b9e49a0cfb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eea3e7e4-2598-4b47-bf92-97cda631f6b8",
            "compositeImage": {
                "id": "29a2c4da-5fc1-4271-828d-8869e5fd5fca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "314b1070-daf5-4e09-adb0-9b9e49a0cfb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f1eba8f-f1db-45e6-8951-5b963960b6da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "314b1070-daf5-4e09-adb0-9b9e49a0cfb2",
                    "LayerId": "156b442d-a965-4aee-b277-ad7bc73b9f41"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "156b442d-a965-4aee-b277-ad7bc73b9f41",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eea3e7e4-2598-4b47-bf92-97cda631f6b8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 32
}