{
    "id": "d1866b3b-e161-4103-a8e9-29428fd152a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_66h_ice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fcca91ca-4b84-4a78-b931-6e402703ff5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1866b3b-e161-4103-a8e9-29428fd152a8",
            "compositeImage": {
                "id": "d9b282b2-faac-4599-9afd-9a0e75eca6e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcca91ca-4b84-4a78-b931-6e402703ff5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1646c61e-389c-4ce2-b423-926b782aadcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcca91ca-4b84-4a78-b931-6e402703ff5e",
                    "LayerId": "6f053d6f-220b-45ac-8f2f-0a6061f61061"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6f053d6f-220b-45ac-8f2f-0a6061f61061",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1866b3b-e161-4103-a8e9-29428fd152a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}