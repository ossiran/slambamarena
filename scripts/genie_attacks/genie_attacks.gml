if(genie = "justice")
{
    if(input_check(0,0,0,0,1) && qcf_buffer > 0)
    {
        genieobj = instance_create_depth(x,y,depth - 1, obj_justice);
        with(genieobj)
        {
            state_switch("qcf");
            owner = other;
            image_xscale = other.image_xscale;
        }
        state_switch("geniemove");
    }
    
    if(input_check(0,0,0,0,1) && qcb_buffer > 0)
    {
        genieobj = instance_create_depth(x,y,depth - 1, obj_justice);
        with(genieobj)
        {
            state_switch("qcb");
            owner = other;
            image_xscale = other.image_xscale;
        }
        state_switch("geniemove");
    }
}