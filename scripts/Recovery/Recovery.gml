if(state_new == 1)
{
	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -1000;
	    image_xscale  = 26;
	    image_yscale  = 75;
	    xshift  = -2;
	}
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -1000;
	    image_xscale  = 0;
	    image_yscale  = 0;
	    xshift = -2
	}
	
	hitstun = 0;
	hitpause = 0;
}

if(state_timer < 4)
{
    vx = -1 * image_xscale;
    vy = -6.5;
}

sprite_index = spr_bill_jumpD;

if(state_timer == 30)
{
    end_combo();
}

if(!onground())
{
    vy = Approach(vy, 5, grav);
}

if(onground())
{
    end_combo();
    state_switch("stand");
}