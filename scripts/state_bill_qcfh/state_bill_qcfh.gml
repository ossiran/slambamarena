

if(state_new == 1)
{
	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -42;
	    xscale  = 26;
	    yscale  = 74;
	    xshift  = -2;
	}
	
			with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 16;
	    image_yscale  = 47;
	    xshift = -2
	}
	attackframe0 =  15;
	endstate = 38;
	
    ex += -50;
}

vx = 0;
vy = 0;

if(state_timer == attackframe0)
{
    var rand = random(1000);
    hitbox1 = create_hitbox(18, -25,15, 15, 3.65, -3.5, 25,25,10,12,40,0,330,0,"mid","billnspecial");
    hitbox2 = create_hitbox(5, -38,28, 28, 3.65, -3.5, 25,25,10,12,40,0,1,0,"mid","billnspecial" + string(inputcounter));
	hitbox1.image_alpha = 1;
	hitbox1.hitsound = snd_hit;
	hitbox1.vx = 5.75 * image_xscale;
}

sprite_index = spr_bill_qcfl;

attack_base(endstate,0,1,0,1,0);

