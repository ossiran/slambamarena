///get_controls(controller);

if(argument0 > -1)
{
key_up		=				gamepad_button_check(argument0, obj_system.key_up_1);
key_down	=				gamepad_button_check(argument0, obj_system.key_down_1);
key_left	=				gamepad_button_check(argument0, obj_system.key_left_1);
key_right	=				gamepad_button_check(argument0, obj_system.key_right_1);
key_tapleft =               gamepad_button_check_pressed(argument0, obj_system.key_left_1);
key_tapright=               gamepad_button_check_pressed(argument0, obj_system.key_right_1);

key_light	=				gamepad_button_check_pressed(argument0, obj_system.key_light_1);
key_heavy	=				gamepad_button_check_pressed(argument0, obj_system.key_heavy_1);
key_medium =				gamepad_button_check_pressed(argument0, obj_system.key_medium_1);
key_essence =				gamepad_button_check_pressed(argument0, obj_system.key_essence_1);
key_dash	=				gamepad_button_check_pressed(argument0, obj_system.key_dash_1);
key_grab	=				gamepad_button_check_pressed(argument0, obj_system.key_grab_1);
key_jump	=				gamepad_button_check_pressed(argument0, obj_system.key_jump_1);
key_tapjump	=				gamepad_button_check_pressed(argument0, obj_system.key_up_1);

key_light_hold	=				gamepad_button_check(argument0, obj_system.key_light_1);
key_heavy_hold	=				gamepad_button_check(argument0, obj_system.key_heavy_1);
key_medium_hold =				gamepad_button_check(argument0, obj_system.key_medium_1);
key_essence_hold =				gamepad_button_check(argument0, obj_system.key_essence_1);

key_start	=				gamepad_button_check_pressed(argument0, gp_start);
}
else if(argument0 == -1)
{
key_up		=				keyboard_check(obj_system.keyboard_up);
key_down	=				keyboard_check(obj_system.keyboard_down);
key_left	=				keyboard_check(obj_system.keyboard_left);
key_right	=				keyboard_check(obj_system.keyboard_right);
key_tapleft	=				keyboard_check_pressed(obj_system.keyboard_left);
key_tapright=				keyboard_check_pressed(obj_system.keyboard_right);

key_light	=				keyboard_check_pressed(obj_system.keyboard_light);
key_heavy	=				keyboard_check_pressed(obj_system.keyboard_heavy);
key_medium =				keyboard_check_pressed(obj_system.keyboard_medium);
key_essence =				keyboard_check_pressed(obj_system.keyboard_essence);
key_dash	=				keyboard_check_pressed(obj_system.keyboard_dash);
key_grab	=				keyboard_check_pressed(obj_system.keyboard_grab);
key_jump	=				keyboard_check_pressed(obj_system.keyboard_jump);
key_tapjump =               keyboard_check_pressed(obj_system.keyboard_up);

key_light_hold	=				keyboard_check(obj_system.keyboard_light);
key_heavy_hold	=				keyboard_check(obj_system.keyboard_heavy);
key_medium_hold =				keyboard_check(obj_system.keyboard_medium);
key_essence_hold =				keyboard_check(obj_system.keyboard_essence);

key_start	=				keyboard_check_pressed(obj_system.keyboard_start);
}