//
if(momomode == 1)
{
    if(state_new == 1)
    {
        with(hurtbox0)
        {
            xoffset = -12;
            yoffset = -13;
            image_xscale  = 24;
            image_yscale  = 40;
            xshift  = 0;
        }
    	
    	with(hurtbox1)
    	{
    	    xoffset = -8;
    	    yoffset = -31;
    	    image_xscale  = 16;
    	    image_yscale  = 18;
    	    xshift = 0;
    	}
    	
    	with(collisionbox)
    	{
    	    xoffset = -8;
    	    yoffset = -13;
    	    image_xscale  = 16;
    	    image_yscale  = 40;
    	    xshift = 0;
    	}
    	
    	
    	attackframe0 =  8;
    	endstate = 35;
    }
        
        
    
    if(state_timer == attackframe0)
    {
        var missle = instance_create_depth(x + 46 * image_xscale, y - 10, 1, obj_missle);
        missle.image_xscale = image_xscale;
        missle.mode = "firel";
        missle.owner = self;
        missle.vxMax = 4.5;
        missle.hits = 2;
        
        heat += 20;
        ex += 3;
        vx = -3.5 * image_xscale;
    }
        
        
    sprite_index = spr_momo_chbl_fire;
        
    attack_base(endstate,0,1,0,1,0);
}
else
{
    if(state_new == 1)
    {
        with(hurtbox0)
        {
            xoffset = -12;
            yoffset = -13;
            image_xscale  = 24;
            image_yscale  = 40;
            xshift  = 0;
        }
    	
    	with(hurtbox1)
    	{
    	    xoffset = -8;
    	    yoffset = -31;
    	    image_xscale  = 16;
    	    image_yscale  = 18;
    	    xshift = 0;
    	}
    	
    	with(collisionbox)
    	{
    	    xoffset = -8;
    	    yoffset = -13;
    	    image_xscale  = 16;
    	    image_yscale  = 40;
    	    xshift = 0;
    	}
    	
    	
    	attackframe0 =  8;
    	endstate = 35;
    }
        
        
    
    if(state_timer == attackframe0)
    {
        var missle = instance_create_depth(x + 46 * image_xscale, y - 10, 1, obj_missle);
        missle.image_xscale = image_xscale;
        missle.mode = "icel";
        missle.owner = self;
        missle.vxMax = 5.5;
        missle.hits = 1;
        
        heat += -20;
        ex += 3;
        vx = -3.5 * image_xscale;
    }
        
        
    sprite_index = spr_momo_chbl_ice;
        
    attack_base(endstate,0,1,0,1,0);

}