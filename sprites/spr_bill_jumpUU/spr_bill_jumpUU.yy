{
    "id": "1098f973-6127-4410-bddd-ed5635b51ec1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_jumpUU",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 52,
    "bbox_right": 88,
    "bbox_top": 64,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2064e6cb-b36e-497d-899f-61de99238dd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1098f973-6127-4410-bddd-ed5635b51ec1",
            "compositeImage": {
                "id": "e8311f22-b5e3-4ae8-a96a-94b04ce5c1f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2064e6cb-b36e-497d-899f-61de99238dd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3638424-1af3-4242-afbb-4b46c04eb43c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2064e6cb-b36e-497d-899f-61de99238dd9",
                    "LayerId": "9da2dfed-d76f-4714-894b-a35f28b67b7c"
                }
            ]
        },
        {
            "id": "028a459c-6b41-4161-b1b5-2a7cc6d013d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1098f973-6127-4410-bddd-ed5635b51ec1",
            "compositeImage": {
                "id": "d9f379a6-4586-4550-9691-322291de9142",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "028a459c-6b41-4161-b1b5-2a7cc6d013d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6087b5e8-eaa6-4659-9124-6d64816b955a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "028a459c-6b41-4161-b1b5-2a7cc6d013d2",
                    "LayerId": "9da2dfed-d76f-4714-894b-a35f28b67b7c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "9da2dfed-d76f-4714-894b-a35f28b67b7c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1098f973-6127-4410-bddd-ed5635b51ec1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}