{
    "id": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_qcfm",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 49,
    "bbox_right": 103,
    "bbox_top": 75,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9628954-4289-496e-9869-ff72010812cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "6f7234bb-d7d4-41b4-969e-ebc0e76ce2a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9628954-4289-496e-9869-ff72010812cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1efd719-1558-4d05-b1d2-b4d9b6bbebf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9628954-4289-496e-9869-ff72010812cd",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "84b04cd8-6c61-476c-b4ae-61db9cb55a5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "2d6f01a8-11de-47cc-a557-971d15f541ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84b04cd8-6c61-476c-b4ae-61db9cb55a5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "059ee860-c784-455d-9bda-bed0600854bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84b04cd8-6c61-476c-b4ae-61db9cb55a5e",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "712e9db0-0708-49c9-8f0b-c680f475e566",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "b2bc3f2a-217d-4094-b8be-c00e433f07fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "712e9db0-0708-49c9-8f0b-c680f475e566",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e5d9198-6fb8-442f-8d29-918e8b7596bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "712e9db0-0708-49c9-8f0b-c680f475e566",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "bff5adb6-8df4-4f67-8b33-b10e40023b1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "f00e90e8-c04c-4170-9baa-d4d386c25e4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bff5adb6-8df4-4f67-8b33-b10e40023b1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f76a45d-a5b9-489d-9f3a-102a32383f7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bff5adb6-8df4-4f67-8b33-b10e40023b1c",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "5938f84f-1f2c-4e90-b33b-fc665e752b67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "deaeddc1-f069-49c5-b0c0-b5e3fa6a766a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5938f84f-1f2c-4e90-b33b-fc665e752b67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "967d89a5-5710-4065-906e-bd9d3d546d2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5938f84f-1f2c-4e90-b33b-fc665e752b67",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "72cf5207-990f-4d0c-8249-5618c197f678",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "80f2d165-8963-4aa8-85e9-d18584d550ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72cf5207-990f-4d0c-8249-5618c197f678",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "049fa405-73a0-46d5-b0a4-546972e163d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72cf5207-990f-4d0c-8249-5618c197f678",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "4e10af89-4ba0-49bd-8d84-5782c643baa7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "83b9e05b-e912-4a17-bd59-79c06a1eba9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e10af89-4ba0-49bd-8d84-5782c643baa7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e623dfd-c93d-4b16-bcac-192687472545",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e10af89-4ba0-49bd-8d84-5782c643baa7",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "8b2b5436-a213-44ef-bfb7-0dd23ff37ad2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "93635fcc-7575-473c-b4f8-a05109b39ce7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b2b5436-a213-44ef-bfb7-0dd23ff37ad2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ed88f42-adf3-4239-8a32-f0f453ae1a2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b2b5436-a213-44ef-bfb7-0dd23ff37ad2",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "74221158-08be-46f4-a179-a1db95889356",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "0fd824cd-a536-439c-b6d8-3ec85d8abfe5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74221158-08be-46f4-a179-a1db95889356",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cda1ea23-c844-4a26-9f49-dc7f4f261067",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74221158-08be-46f4-a179-a1db95889356",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "d2c4f98c-5006-403e-8db9-9b775fd6b080",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "120fcc4b-fff5-48e4-87d2-571e2250b338",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2c4f98c-5006-403e-8db9-9b775fd6b080",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36168cb8-040a-47ac-b943-14b215a7c89d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2c4f98c-5006-403e-8db9-9b775fd6b080",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "9c3b100e-fdf5-4e7d-9f9d-0723057141a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "132cb2ff-a64c-4d09-bb8e-05d1d9031862",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c3b100e-fdf5-4e7d-9f9d-0723057141a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf2d2ecc-900c-499f-ab56-1ff4a07970c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c3b100e-fdf5-4e7d-9f9d-0723057141a5",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "d444cdc0-1a6b-4702-a389-2840639c5a38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "0e891c59-69b0-41f1-97b4-ac12f565c55e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d444cdc0-1a6b-4702-a389-2840639c5a38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0627b4bb-24b4-4bc5-8c00-6715417f342a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d444cdc0-1a6b-4702-a389-2840639c5a38",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "8eeddf56-e354-4825-a4ce-506ef959105f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "5fb5b109-d3f5-4bab-949a-6aa904a94bd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8eeddf56-e354-4825-a4ce-506ef959105f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a36aa548-574e-46a6-b3c0-6fb6fe2c4fdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8eeddf56-e354-4825-a4ce-506ef959105f",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "2e52a126-69c9-43a0-885f-71e21b796d58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "72c18359-9dba-4263-a783-c2f9887dc7dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e52a126-69c9-43a0-885f-71e21b796d58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aed7062a-2bb2-47b9-b505-687ab25f52f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e52a126-69c9-43a0-885f-71e21b796d58",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "03e136f1-b230-487d-b3b5-3ce4d64b29bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "74357520-dd26-4fad-af7f-8c69e2fa1b27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03e136f1-b230-487d-b3b5-3ce4d64b29bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9488719-d6e3-4c2e-b1bf-bec74b10fb56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03e136f1-b230-487d-b3b5-3ce4d64b29bb",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "9a002f75-41b4-46ea-ac65-80cc86edaf73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "b537586e-2a50-45e5-9ed8-d74632d65ed2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a002f75-41b4-46ea-ac65-80cc86edaf73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60862095-499a-498c-a091-819075f016d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a002f75-41b4-46ea-ac65-80cc86edaf73",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "45b3fdb3-7a0c-4015-a8bd-8151f0b32395",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "e09bb164-20f4-4c87-be9f-fd980afd5a39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45b3fdb3-7a0c-4015-a8bd-8151f0b32395",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a53a48e1-ef72-4666-8c10-d0e82956d078",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45b3fdb3-7a0c-4015-a8bd-8151f0b32395",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "757c6119-96a8-47b6-989a-d6a11a1055e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "9ae3d94f-511b-4178-806f-61aefa96a430",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "757c6119-96a8-47b6-989a-d6a11a1055e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "950bcdc2-4011-462a-82c9-70f52917cfa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "757c6119-96a8-47b6-989a-d6a11a1055e0",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "61ae46df-ac50-436a-bb49-843701a3a6cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "1b1402f1-5ad3-4829-9bba-ad1105ac9d13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61ae46df-ac50-436a-bb49-843701a3a6cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0a8132d-c6ef-40c0-aae3-0a3c9b05502d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61ae46df-ac50-436a-bb49-843701a3a6cd",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "a0957909-a4aa-4d7a-bb9e-0fd3c267f411",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "786ea9cf-731f-4c6d-9ae0-6ec873b41303",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0957909-a4aa-4d7a-bb9e-0fd3c267f411",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b067498-d7e5-48ff-8bfa-9d26c2a77a95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0957909-a4aa-4d7a-bb9e-0fd3c267f411",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "4436dbe6-de55-41e4-8a83-3aa5acb04462",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "349d8817-caba-4e25-bf41-d6af757b789a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4436dbe6-de55-41e4-8a83-3aa5acb04462",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cc679eb-6106-43e3-8815-127057f6984f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4436dbe6-de55-41e4-8a83-3aa5acb04462",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "d08873e4-7604-4e75-a085-0009f0170e68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "21024248-a6cf-4f22-a5c0-689cd565bc6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d08873e4-7604-4e75-a085-0009f0170e68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3806a132-0657-4e4e-9df2-736fd90de804",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d08873e4-7604-4e75-a085-0009f0170e68",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "cf032d46-f448-45c8-a821-49d67d1d1133",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "6b23a8ee-af98-4094-a495-2532bee5d2ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf032d46-f448-45c8-a821-49d67d1d1133",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29799927-d569-4ccf-9570-3e6126dc4c29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf032d46-f448-45c8-a821-49d67d1d1133",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "013b620b-054c-450a-8122-3f12919249bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "21a9cc95-9695-49b1-9a19-34d038e8a2d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "013b620b-054c-450a-8122-3f12919249bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37572c54-b06d-4dbb-bf1b-44f7bcc19331",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "013b620b-054c-450a-8122-3f12919249bb",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "2b531235-faa9-43aa-ae05-43c4500d65b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "579b7c83-5076-48e9-ac87-de17ac369f6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b531235-faa9-43aa-ae05-43c4500d65b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c84c5df5-92c6-41de-9301-7a8ff656eeab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b531235-faa9-43aa-ae05-43c4500d65b5",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "75ddfdeb-dea7-4743-a9b4-ae63eeb3e305",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "e23776bf-f5de-4260-80f4-14d0351cf9b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75ddfdeb-dea7-4743-a9b4-ae63eeb3e305",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e779c8c0-d818-4d15-9cb7-1b53d79e2c50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75ddfdeb-dea7-4743-a9b4-ae63eeb3e305",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "8d0e2afa-65d7-4c79-acad-e57160bfe68f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "9f729c25-ff34-42a8-9766-725d13566909",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d0e2afa-65d7-4c79-acad-e57160bfe68f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "716a5ff6-977f-4aa2-9606-9f4c5967b9f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d0e2afa-65d7-4c79-acad-e57160bfe68f",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "be68a71f-cc51-45ba-86a7-eebbd5a4b497",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "b3e84f43-5c25-488c-a464-64bb89a145fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be68a71f-cc51-45ba-86a7-eebbd5a4b497",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88bcdb88-4690-4618-952b-47c8390892c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be68a71f-cc51-45ba-86a7-eebbd5a4b497",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "581a239d-e7db-4e87-bfd1-85f9a747b430",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "c998a19e-d250-4543-9fd7-a6359a7e3efb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "581a239d-e7db-4e87-bfd1-85f9a747b430",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0da48105-b1e7-4e6c-872d-c843d9180338",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "581a239d-e7db-4e87-bfd1-85f9a747b430",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "7b81e687-2ff6-468d-92e0-2bcd1c701dc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "1d4155ca-3e2f-44be-9f39-ea4c602e8ccd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b81e687-2ff6-468d-92e0-2bcd1c701dc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae9bc2f7-587a-49f5-9897-9dd6ee73c849",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b81e687-2ff6-468d-92e0-2bcd1c701dc0",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "328fc354-a3f9-4c4d-bbc3-0758fe5dfa19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "f13e5ac6-8ef0-4034-bece-9a957f4e3071",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "328fc354-a3f9-4c4d-bbc3-0758fe5dfa19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a118851c-9a74-485b-b8c5-ae87db2947cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "328fc354-a3f9-4c4d-bbc3-0758fe5dfa19",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "0fa54208-dc40-42ad-89c3-40f1497959c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "d4a26f26-ce6e-4b91-9717-8d81b99459d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fa54208-dc40-42ad-89c3-40f1497959c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cadfa544-8a56-4af5-9c21-f76f3a1933b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fa54208-dc40-42ad-89c3-40f1497959c9",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "485ebb49-af09-4bb0-b58d-6812ac774c53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "90eb0932-bab0-4a2c-b286-705a41d77797",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "485ebb49-af09-4bb0-b58d-6812ac774c53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5daf43f-0546-4667-ac49-487a07e6a236",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "485ebb49-af09-4bb0-b58d-6812ac774c53",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "f590565b-fc6a-4aef-9b83-5a84f1d2848b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "ef43683d-0420-4716-bb58-1f35178e0fe6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f590565b-fc6a-4aef-9b83-5a84f1d2848b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bcfea51-6232-4e6f-97e3-d0363dbfb3b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f590565b-fc6a-4aef-9b83-5a84f1d2848b",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "595ddeb9-ce0e-4040-aef0-b33d42441669",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "b826c039-bd54-4af6-899b-b502567fd64e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "595ddeb9-ce0e-4040-aef0-b33d42441669",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "311155d8-c329-44c0-a8d0-a1fa15a0b9fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "595ddeb9-ce0e-4040-aef0-b33d42441669",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        },
        {
            "id": "537a0d54-0346-4370-9fe2-b6edc47d3750",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "compositeImage": {
                "id": "bf91bfe9-b701-4efc-a131-9e0ce12d3ffc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "537a0d54-0346-4370-9fe2-b6edc47d3750",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a20eab18-a227-4f82-a632-6db618c2a16c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "537a0d54-0346-4370-9fe2-b6edc47d3750",
                    "LayerId": "99c831a8-1ea8-4319-8e3c-c33d1717fb98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "99c831a8-1ea8-4319-8e3c-c33d1717fb98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "79ce4a2d-a1e8-4b92-a0ab-2e8f1c7b16a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}