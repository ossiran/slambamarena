{
    "id": "a1703a6a-ab7e-4bf5-b60b-907bd46d53a7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_clight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 48,
    "bbox_right": 108,
    "bbox_top": 91,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4aa78f0a-7550-4e97-9732-d2d1a5a07a62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1703a6a-ab7e-4bf5-b60b-907bd46d53a7",
            "compositeImage": {
                "id": "4d2c0b02-d804-4302-b981-fd1ca4012c27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4aa78f0a-7550-4e97-9732-d2d1a5a07a62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0e46a4d-37e8-4d58-b41e-5f2defaa7451",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4aa78f0a-7550-4e97-9732-d2d1a5a07a62",
                    "LayerId": "1473e96a-f072-46e9-a898-528a5ca7cf7e"
                }
            ]
        },
        {
            "id": "432a1af9-5250-4558-b993-2961cd3c5c51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1703a6a-ab7e-4bf5-b60b-907bd46d53a7",
            "compositeImage": {
                "id": "0f87c95a-e350-4c6f-b2bf-5df5f991001d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "432a1af9-5250-4558-b993-2961cd3c5c51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1694e8b-ca9a-401b-92b4-b82f4853bc10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "432a1af9-5250-4558-b993-2961cd3c5c51",
                    "LayerId": "1473e96a-f072-46e9-a898-528a5ca7cf7e"
                }
            ]
        },
        {
            "id": "0872c05d-d674-4fdf-a5a9-ac3f2b2a1f52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1703a6a-ab7e-4bf5-b60b-907bd46d53a7",
            "compositeImage": {
                "id": "aae06449-4f45-4415-8d83-dc6c820af6e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0872c05d-d674-4fdf-a5a9-ac3f2b2a1f52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6185ceae-2e28-40c3-aa97-849b6b65fa5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0872c05d-d674-4fdf-a5a9-ac3f2b2a1f52",
                    "LayerId": "1473e96a-f072-46e9-a898-528a5ca7cf7e"
                }
            ]
        },
        {
            "id": "2a1c8e56-c719-4e82-b615-2af3ce148949",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1703a6a-ab7e-4bf5-b60b-907bd46d53a7",
            "compositeImage": {
                "id": "7aa1d7cc-6cb4-4933-82a7-0c7ee910e371",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a1c8e56-c719-4e82-b615-2af3ce148949",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c459487-4c16-4d58-8450-d608dfe618dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a1c8e56-c719-4e82-b615-2af3ce148949",
                    "LayerId": "1473e96a-f072-46e9-a898-528a5ca7cf7e"
                }
            ]
        },
        {
            "id": "6ac8110d-40e0-4aad-9c5a-8a96ae0731d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1703a6a-ab7e-4bf5-b60b-907bd46d53a7",
            "compositeImage": {
                "id": "283325c2-51b9-46b3-95d6-bb7a2fa804f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ac8110d-40e0-4aad-9c5a-8a96ae0731d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb134eaf-20f2-4d8b-8a7a-bb71c2764bd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ac8110d-40e0-4aad-9c5a-8a96ae0731d5",
                    "LayerId": "1473e96a-f072-46e9-a898-528a5ca7cf7e"
                }
            ]
        },
        {
            "id": "e6ec9921-4086-457e-b10c-8dfe4c5677cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1703a6a-ab7e-4bf5-b60b-907bd46d53a7",
            "compositeImage": {
                "id": "f9048b88-c624-461a-bccb-a96b4c446f18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6ec9921-4086-457e-b10c-8dfe4c5677cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb7be5bb-43b7-4125-8d85-dd307e4ef8fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6ec9921-4086-457e-b10c-8dfe4c5677cb",
                    "LayerId": "1473e96a-f072-46e9-a898-528a5ca7cf7e"
                }
            ]
        },
        {
            "id": "1b0f4a46-d5e7-4253-afd0-666a3d716d52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1703a6a-ab7e-4bf5-b60b-907bd46d53a7",
            "compositeImage": {
                "id": "03e89d8c-d31f-4a61-b7f2-f67dbc02f5f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b0f4a46-d5e7-4253-afd0-666a3d716d52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44f49368-24d6-4d9f-afde-4297725d1ffc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b0f4a46-d5e7-4253-afd0-666a3d716d52",
                    "LayerId": "1473e96a-f072-46e9-a898-528a5ca7cf7e"
                }
            ]
        },
        {
            "id": "3739962f-631c-4eda-b382-4ebf3631db39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1703a6a-ab7e-4bf5-b60b-907bd46d53a7",
            "compositeImage": {
                "id": "a70ee749-4854-45be-911d-11436b60511b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3739962f-631c-4eda-b382-4ebf3631db39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfeb0612-dcfb-46b8-9b07-35aed1b5901a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3739962f-631c-4eda-b382-4ebf3631db39",
                    "LayerId": "1473e96a-f072-46e9-a898-528a5ca7cf7e"
                }
            ]
        },
        {
            "id": "3ffd1f15-e78d-4e8f-97da-b85233f179c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1703a6a-ab7e-4bf5-b60b-907bd46d53a7",
            "compositeImage": {
                "id": "00b494a3-1032-4ec7-a9a3-ade9a7b8adb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ffd1f15-e78d-4e8f-97da-b85233f179c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "900d53c3-d6c1-494b-9d82-d9482adebfe1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ffd1f15-e78d-4e8f-97da-b85233f179c8",
                    "LayerId": "1473e96a-f072-46e9-a898-528a5ca7cf7e"
                }
            ]
        },
        {
            "id": "33ad9b60-b500-4eeb-8810-737eef7151a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1703a6a-ab7e-4bf5-b60b-907bd46d53a7",
            "compositeImage": {
                "id": "abe8a148-fd58-40f8-8958-348533a791b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33ad9b60-b500-4eeb-8810-737eef7151a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0a99f14-a900-4854-8903-65bbb2f36f4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33ad9b60-b500-4eeb-8810-737eef7151a5",
                    "LayerId": "1473e96a-f072-46e9-a898-528a5ca7cf7e"
                }
            ]
        },
        {
            "id": "417089a0-bd90-46c1-85ff-0b5963a1a8f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1703a6a-ab7e-4bf5-b60b-907bd46d53a7",
            "compositeImage": {
                "id": "4c7710d8-5682-402e-8a5c-268a173455c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "417089a0-bd90-46c1-85ff-0b5963a1a8f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1785f3bf-688a-4294-a3b8-b6a0d4c96bd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "417089a0-bd90-46c1-85ff-0b5963a1a8f1",
                    "LayerId": "1473e96a-f072-46e9-a898-528a5ca7cf7e"
                }
            ]
        },
        {
            "id": "22d3ec06-837e-442c-aba3-135ef03295a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1703a6a-ab7e-4bf5-b60b-907bd46d53a7",
            "compositeImage": {
                "id": "29beca61-4c2b-4e39-af0a-c847c932806e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22d3ec06-837e-442c-aba3-135ef03295a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "347d4ffa-31e3-44b3-9c10-e37395a8c78f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22d3ec06-837e-442c-aba3-135ef03295a2",
                    "LayerId": "1473e96a-f072-46e9-a898-528a5ca7cf7e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "1473e96a-f072-46e9-a898-528a5ca7cf7e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a1703a6a-ab7e-4bf5-b60b-907bd46d53a7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}