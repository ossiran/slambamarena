if(state_new == 1)
{
    	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -43;
	    image_xscale  = 26;
	    image_yscale  = 75;
	    xshift  = -2;
	}
			with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 16;
	    image_yscale  = 47;
	    xshift = -2
	}
	    image_index = 0;
	
}

doublejump = 1;
airdash = 1;

sprite_index = spr_bill_walkingf;
image_xscale = -frontOrBack;

//right
if(input_check(4,0,0,0,0))
{
	vx = Approach(vx,-vxMax * image_xscale,ground_accel);
}

//left
if(input_check(6,0,0,0,0))
{
	vx = Approach(vx,vxMax * image_xscale,ground_accel);
}

//friction
if((key_left == 0 && key_right == 0)||(key_left == 1 && key_right == 1))
{
	vx = Approach(vx,0,ground_fric);
}


//crouch
if(input_check(1,0,0,0,0) || input_check(2,0,0,0,0) || input_check(3,0,0,0,0))
{
     state_switch("crouch");   
}

//stand
if(input_check(5,0,0,0,0))
{
	state_switch("stand");
}

//jab
if(input_check(0,1,0,0,0))
{
    state_switch("light");
}

//medium
if(input_check(0,0,1,0,0))
{
    state_switch("medium");
}

//heavy
if(input_check(0,0,0,1,0))
{
    state_switch("heavy");
}

//qcf
if(qcf_buffer > 0)
{
    if(input_check(0,0,0,1,0))
    {
        state_switch("qcfh");
    }
    else if(input_check(0,0,1,0,0))
    {
        state_switch("qcfm");
    }
    else if(input_check(0,1,0,0,0))
    {
        state_switch("qcfl");
    }
}

//qcf
if(qcf_buffer > 0)
{
    if(input_check(0,0,0,1,0))
    {
        if(ex > 49.9)
        {
            state_switch("qcfh");
        }
        else
        {
            state_switch("qcfm");
        }
    }
    else if(input_check(0,0,1,0,0))
    {
        state_switch("qcfm");
    }
    else if(input_check(0,1,0,0,0))
    {
        state_switch("qcfl");
    }
}


//qcb
if(qcb_buffer > 0)
{
    if(input_check(0,0,0,1,0))
    {
        if(ex > 49.9)
        {
            state_switch("qcbh");
        }
        else
        {
            state_switch("qcbm");
        }
    }
    else if(input_check(0,0,1,0,0))
    {
        state_switch("qcbm");
    }
    else if(input_check(0,1,0,0,0))
    {
        state_switch("qcbl");
    }
}

//srk
if(srk_buffer > 0)
{
    if(input_check(0,0,0,1,0))
    {
        if(ex > 49.9)
        {
            state_switch("srkh");
        }
        else
        {
            state_switch("srkm");
        }
    }
    else if(input_check(0,0,1,0,0))
    {
        state_switch("srkm");
    }
    else if(input_check(0,1,0,0,0))
    {
        state_switch("srkl");
    }
}

if(dqcf_buffer > 0 && ex > 49.9)
{
    if(input_check(0,0,0,1,0))
    {
        state_switch("dqcf");
    }
    else if(input_check(0,0,1,0,0))
    {
        state_switch("dqcf");
    }
    else if(input_check(0,1,0,0,0))
    {
        state_switch("dqcf");
    }
}



//grab
if(input_check(0,1,0,0,1))
{
    state_switch("grab");
}

//jump
if(input_check(7,0,0,0,0) || input_check(8,0,0,0,0) || input_check(9,0,0,0,0))
{
	state_switch("jumpsquat");
}

//dash
//fdash
if(ff_buffer > 0 || (input_check(6, 1, 1, 0, 0)) || (input_check(5, 1, 1, 0, 0)))
{
        state_switch("fdash");
}
//bdash
if(bb_buffer > 0 || (input_check(4, 1, 1, 0, 0)))
{
        state_switch("bdash");
}

//air
if(!onground())
{
	state_switch("air");	
}


genie_attacks();

//gbust
if(input_check(0,1,1,1,0) && ex > 30 && gbust == 0)
{
    ex -= 50;
    gbust = gbustmax;
}

//hitstun
if(hitstun > 0)
{
    state_switch("hitstun");   
}

//block
if(blockstun > 0)
{
	state_switch("sblock");	
}