{
    "id": "3d58cfa7-f359-4cff-854b-6889af445e8d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lsyn_jl",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 49,
    "bbox_right": 99,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "31ea46d5-3d8d-42de-94c1-c31248d9b35b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d58cfa7-f359-4cff-854b-6889af445e8d",
            "compositeImage": {
                "id": "a3bd245f-c5fe-445e-9acf-7f32d81b9fef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31ea46d5-3d8d-42de-94c1-c31248d9b35b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "030ea9c6-d89b-4fd3-b44f-b8200ebd3cd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31ea46d5-3d8d-42de-94c1-c31248d9b35b",
                    "LayerId": "a03a78ba-4d3f-4312-8385-580e6a9ee50f"
                }
            ]
        },
        {
            "id": "946f46cc-8379-4355-a361-6a5738586602",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d58cfa7-f359-4cff-854b-6889af445e8d",
            "compositeImage": {
                "id": "cffdeeb7-9816-449a-a992-c0e6e16ca33f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "946f46cc-8379-4355-a361-6a5738586602",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27f7a055-548e-4acb-b912-8644c6ea8fde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "946f46cc-8379-4355-a361-6a5738586602",
                    "LayerId": "a03a78ba-4d3f-4312-8385-580e6a9ee50f"
                }
            ]
        },
        {
            "id": "abb6abf9-1e61-4a15-b229-cf343733b694",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d58cfa7-f359-4cff-854b-6889af445e8d",
            "compositeImage": {
                "id": "2f4747e2-03dd-4a43-8eb3-70f18d6450d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abb6abf9-1e61-4a15-b229-cf343733b694",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f40df5be-7e39-414c-9db0-f2c03d2c7a8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abb6abf9-1e61-4a15-b229-cf343733b694",
                    "LayerId": "a03a78ba-4d3f-4312-8385-580e6a9ee50f"
                }
            ]
        },
        {
            "id": "ef9c9a5d-86de-4069-af83-0d3fa79849cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d58cfa7-f359-4cff-854b-6889af445e8d",
            "compositeImage": {
                "id": "d1ba42a8-1858-4ee4-b412-c3ccd93f177f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef9c9a5d-86de-4069-af83-0d3fa79849cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ababe555-019f-4749-b5a5-3b204066cecd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef9c9a5d-86de-4069-af83-0d3fa79849cf",
                    "LayerId": "a03a78ba-4d3f-4312-8385-580e6a9ee50f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "a03a78ba-4d3f-4312-8385-580e6a9ee50f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3d58cfa7-f359-4cff-854b-6889af445e8d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}