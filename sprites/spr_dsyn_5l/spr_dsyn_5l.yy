{
    "id": "79b9eb7b-b56a-440c-86e1-c4ffe880aa33",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dsyn_5l",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 36,
    "bbox_right": 105,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0bae359-a440-4fb5-9c47-503838abb1e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79b9eb7b-b56a-440c-86e1-c4ffe880aa33",
            "compositeImage": {
                "id": "995d6c6c-4344-4303-9003-933741aa62e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0bae359-a440-4fb5-9c47-503838abb1e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48411d07-9bc4-45c2-b3aa-b73fb13cd351",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0bae359-a440-4fb5-9c47-503838abb1e6",
                    "LayerId": "7b830c3e-5b0d-4e06-8b4f-efb0caf9ff20"
                }
            ]
        },
        {
            "id": "e081d868-49ae-4d5f-ade4-75c48d195371",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79b9eb7b-b56a-440c-86e1-c4ffe880aa33",
            "compositeImage": {
                "id": "7c8a4617-2719-4f48-83e2-1de29affd550",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e081d868-49ae-4d5f-ade4-75c48d195371",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5eb4295f-c188-445f-86cf-abdfd98c879d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e081d868-49ae-4d5f-ade4-75c48d195371",
                    "LayerId": "7b830c3e-5b0d-4e06-8b4f-efb0caf9ff20"
                }
            ]
        },
        {
            "id": "183e2478-b4f1-453f-92c4-bb0888635f4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79b9eb7b-b56a-440c-86e1-c4ffe880aa33",
            "compositeImage": {
                "id": "ae5d4b9e-6925-4ef5-a183-8840835a3105",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "183e2478-b4f1-453f-92c4-bb0888635f4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5332672b-8257-4d40-8c46-1f5f363a76db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "183e2478-b4f1-453f-92c4-bb0888635f4a",
                    "LayerId": "7b830c3e-5b0d-4e06-8b4f-efb0caf9ff20"
                }
            ]
        },
        {
            "id": "e82cb1a8-1a24-44de-9a54-765c6933f38b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79b9eb7b-b56a-440c-86e1-c4ffe880aa33",
            "compositeImage": {
                "id": "a2f7d63e-ee9d-4311-9002-7aa3c09fbf49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e82cb1a8-1a24-44de-9a54-765c6933f38b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b5a2767-ea37-4140-a9cb-9367a0fa55cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e82cb1a8-1a24-44de-9a54-765c6933f38b",
                    "LayerId": "7b830c3e-5b0d-4e06-8b4f-efb0caf9ff20"
                }
            ]
        },
        {
            "id": "dcb8daf3-323c-476c-8b80-bd5c20162bb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79b9eb7b-b56a-440c-86e1-c4ffe880aa33",
            "compositeImage": {
                "id": "adaddd03-4e4f-41b7-bd4c-a4cd7e0e6147",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcb8daf3-323c-476c-8b80-bd5c20162bb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e82b2652-fd7c-49a9-b3aa-a0b764188531",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcb8daf3-323c-476c-8b80-bd5c20162bb5",
                    "LayerId": "7b830c3e-5b0d-4e06-8b4f-efb0caf9ff20"
                }
            ]
        },
        {
            "id": "41457a70-e261-4824-8514-bc0a17a34c46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79b9eb7b-b56a-440c-86e1-c4ffe880aa33",
            "compositeImage": {
                "id": "26c2dd36-d488-4e87-a60a-1733ba40efde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41457a70-e261-4824-8514-bc0a17a34c46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "616308f7-6e43-4de8-8492-63affedf7a32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41457a70-e261-4824-8514-bc0a17a34c46",
                    "LayerId": "7b830c3e-5b0d-4e06-8b4f-efb0caf9ff20"
                }
            ]
        },
        {
            "id": "eed0ec0f-2000-4f74-ad52-8148ca360d0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79b9eb7b-b56a-440c-86e1-c4ffe880aa33",
            "compositeImage": {
                "id": "668291fb-6c8c-4ca0-9257-9f90cc899294",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eed0ec0f-2000-4f74-ad52-8148ca360d0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "551d7a11-55d6-4554-b0ea-817339efa203",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eed0ec0f-2000-4f74-ad52-8148ca360d0c",
                    "LayerId": "7b830c3e-5b0d-4e06-8b4f-efb0caf9ff20"
                }
            ]
        },
        {
            "id": "7f9858a7-9ca4-429a-b2e4-57d48de7c841",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79b9eb7b-b56a-440c-86e1-c4ffe880aa33",
            "compositeImage": {
                "id": "733e57ea-ea0f-492a-bc97-60a0c96b9ecd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f9858a7-9ca4-429a-b2e4-57d48de7c841",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d167586a-7c5d-4cce-b973-7cd01ee72395",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f9858a7-9ca4-429a-b2e4-57d48de7c841",
                    "LayerId": "7b830c3e-5b0d-4e06-8b4f-efb0caf9ff20"
                }
            ]
        },
        {
            "id": "0c6ce81c-8d5b-4f1c-b4eb-38e0e16fa707",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79b9eb7b-b56a-440c-86e1-c4ffe880aa33",
            "compositeImage": {
                "id": "a80bd83f-4fa9-40d7-9cea-eba90e436241",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c6ce81c-8d5b-4f1c-b4eb-38e0e16fa707",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a304525-8427-4b48-a28e-a92c6a8e1720",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c6ce81c-8d5b-4f1c-b4eb-38e0e16fa707",
                    "LayerId": "7b830c3e-5b0d-4e06-8b4f-efb0caf9ff20"
                }
            ]
        },
        {
            "id": "b89be906-0af7-4772-9485-82e5772e65a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79b9eb7b-b56a-440c-86e1-c4ffe880aa33",
            "compositeImage": {
                "id": "dab2a6a8-614a-48b0-b5d4-0ed2db83c35f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b89be906-0af7-4772-9485-82e5772e65a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d346a75-017a-49cf-ab63-f72e3abe8e27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b89be906-0af7-4772-9485-82e5772e65a3",
                    "LayerId": "7b830c3e-5b0d-4e06-8b4f-efb0caf9ff20"
                }
            ]
        },
        {
            "id": "6152a1ab-87cb-4fc9-8698-8195cf17dc53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79b9eb7b-b56a-440c-86e1-c4ffe880aa33",
            "compositeImage": {
                "id": "1fca3aac-feb7-4387-86a4-3995415bbb3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6152a1ab-87cb-4fc9-8698-8195cf17dc53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f13d7a1-bcbe-482d-b4fd-13e51ea118a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6152a1ab-87cb-4fc9-8698-8195cf17dc53",
                    "LayerId": "7b830c3e-5b0d-4e06-8b4f-efb0caf9ff20"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "7b830c3e-5b0d-4e06-8b4f-efb0caf9ff20",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "79b9eb7b-b56a-440c-86e1-c4ffe880aa33",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}