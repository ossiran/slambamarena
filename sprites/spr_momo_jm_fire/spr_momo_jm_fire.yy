{
    "id": "405c8a12-7ac7-48f2-a9ec-fc7b884de97a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_jm_fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd584f0d-f34e-4da2-8150-cb3d99166e39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "405c8a12-7ac7-48f2-a9ec-fc7b884de97a",
            "compositeImage": {
                "id": "4f5cb2f2-5858-4765-b236-d9f006f1319b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd584f0d-f34e-4da2-8150-cb3d99166e39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e1e5b2c-e097-4afd-adae-95153ebf6d19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd584f0d-f34e-4da2-8150-cb3d99166e39",
                    "LayerId": "0363d459-5596-4f04-8917-baa212655736"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0363d459-5596-4f04-8917-baa212655736",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "405c8a12-7ac7-48f2-a9ec-fc7b884de97a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}