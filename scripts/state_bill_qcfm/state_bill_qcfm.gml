

if(state_new == 1)
{
	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -42;
	    xscale  = 26;
	    yscale  = 74;
	    xshift  = -2;
	}
	
			with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 16;
	    image_yscale  = 47;
	    xshift = -2
	}
	attackframe0 =  15;
	endstate = 33;
	

}


if(state_timer == attackframe0 - 1)
{
    hitbox2 = create_hitbox(5, -28,38, 28, 7 , 0, 18,20,10,12,-1,0,0,6,"mid","billnspecial" + string(inputcounter));
    hitbox2.scaling = 1.5;
    hitbox2.chipdamage = 5;
    hitbox2.airxtrayhit = -9;
    hitbox2.airxtraxhit = -2;
    hitbox2.airxtrahitstun = -6;
    ex += 2;
    audio_play_sound(snd_laser,1,0);
}

if(state_timer == attackframe0 && hitting == 0)
{
    hitbox4 = create_hitbox(28, -25,15, 16, 7 , 0, 18,10 ,10,8,-1,0,330,6,"projectilemid","billnspecial" + string(inputcounter));
    hitbox4.image_alpha = 1;
    hitbox4.hitsound = snd_hit;
    hitbox4.vx = 4.5 * image_xscale;
    hitbox4.scaling = 1.5;
    hitbox4.chipdamage = 5;
    vx =  -5  * image_xscale;
    hitbox4.extrasprite = spr_fireball;
    hitbox4.airxtrayhit = -9;
    hitbox4.airxtrahitstun = -6;
    hitbox4.projectile = 1;
}

sprite_index = spr_bill_qcfl;

attack_base(endstate,0,1,0,1,0);


