if(onground())
        {
            if(state_exists("qcfl") == 1 && qcf_buffer > 0)
            {
                if(input_check(0,1,0,0,0))
                {
                    cancelmove = "qcfl";
                }
                else if(input_check(0,0,1,0,0))
                {
                    cancelmove = "qcfm";
                }
                else if(input_check(0,0,0,1,0))
                {
                    if(ex > 24.9)
                    {
                        cancelmove = "qcfh";
                        //ex += -50;
                    }
                    else
                    {
                        cancelmove = "qcfm";
                    }
                }
            }
            if(state_exists("qcbl") == 1 && qcb_buffer > 0)
            {
                if(input_check(0,1,0,0,0))
                {
                    cancelmove = "qcbl";
                }
                else if(input_check(0,0,1,0,0))
                {
                    cancelmove = "qcbm";
                }
                else if(input_check(0,0,0,1,0))
                {
                    if(ex > 24.9)
                    {
                        cancelmove = "qcbh";
                        //ex += -50;
                    }
                    else
                    {
                        cancelmove = "qcbm";
                    }
                }
            }
            if(state_exists("srkl") == 1 && srk_buffer > 0)
            {
                if(input_check(0,1,0,0,0))
                {
                    cancelmove = "srkl";
                }
                else if(input_check(0,0,1,0,0))
                {
                    cancelmove = "srkm";
                }
                else if(input_check(0,0,0,1,0))
                {
                    if(ex > 24.9)
                    {
                        cancelmove = "srkh";
                        //ex += -50;
                    }
                    else
                    {
                        cancelmove = "srkm";
                    }
                }
            }
            
            if(state_exists("ddl") == 1 && dd_buffer > 0 )
            {
                if(input_check(0,1,0,0,0))
                {
                    cancelmove = "ddl";
                }
                else if(input_check(0,0,1,0,0))
                {
                    cancelmove = "ddm";
                }
                else if(input_check(0,0,0,1,0))
                {
                    if(ex > 24.9)
                    {
                        cancelmove = "ddh";
                    }
                    else
                    {
                        cancelmove = "ddm";
                    }
                }
            }
            
            if(state_exists("chdl") == 1 && chd() == 1)
            {
                if(input_check(8,1,0,0,0))
                {
                    cancelmove = "chdl";
                }
                else if(input_check(8,0,1,0,0))
                {
                    cancelmove = "chdm";
                }
                else if(input_check(8,0,0,1,0))
                {
                    if(ex > 24.9)
                    {
                        cancelmove = "chdh";
                    }
                    else
                    {
                        cancelmove = "chdm";
                    }
                }
            }
            
            if(state_exists("chbl") == 1 && chb() == 1)
            {
                if(input_check(6,1,0,0,0))
                {
                    cancelmove = "chbl";
                }
                else if(input_check(6,0,1,0,0))
                {
                    cancelmove = "chbm";
                }
                else if(input_check(6,0,0,1,0))
                {
                    if(ex > 24.9)
                    {
                        cancelmove = "chbh";
                    }
                    else
                    {
                        cancelmove = "chbm";
                    }
                }
            }
            /*
            //dash medium
            if(state_exists("dmedium") == 1 && ff_buffer > 0 && input_check(0,0,1,0,0))
            {
                cancelmove = "dmedium"
            }
            //dash heavy
            if(state_exists("dheavy") == 1 && ff_buffer > 0 && input_check(0,0,0,1,0))
            {
                cancelmove = "dheavy"
            }
            */
        }
        else
        {
            if(state_exists("aqcfl") == 1 && qcf_buffer > 0)
            {
                if(input_check(0,1,0,0,0))
                {
                    cancelmove = "aqcfl";
                }
                else if(input_check(0,0,1,0,0))
                {
                    cancelmove = "aqcfm";
                }
                else if(input_check(0,0,0,1,0))
                {
                    if(ex > 24.9)
                    {
                        cancelmove = "aqcfh";
                        //ex += -50;
                    }
                    else
                    {
                        cancelmove = "aqcfm";
                    }
                }
            }
            if(state_exists("aqcbl") == 1 && qcb_buffer > 0)
            {
                if(input_check(0,1,0,0,0))
                {
                    cancelmove = "aqcbl";
                }
                else if(input_check(0,0,1,0,0))
                {
                    cancelmove = "aqcbm";
                }
                else if(input_check(0,0,0,1,0))
                {
                    if(ex > 24.9)
                    {
                        cancelmove = "aqcbh";
                        //ex += -50;
                    }
                    else
                    {
                        cancelmove = "aqcbm";
                    }
                }
            }
            if(state_exists("asrkl") == 1 && srk_buffer > 0)
            {
                if(input_check(0,1,0,0,0))
                {
                    cancelmove = "asrkl";
                }
                else if(input_check(0,0,1,0,0))
                {
                    cancelmove = "asrkm";
                }
                else if(input_check(0,0,0,1,0))
                {
                    if(ex > 24.9)
                    {
                        cancelmove = "asrkh";
                        //ex += -50;
                    }
                    else
                    {
                        cancelmove = "asrkm";
                    }
                }
            }
            
            if(state_exists("addl") == 1 && dd_buffer > 0)
            {
                if(input_check(0,1,0,0,0))
                {
                    cancelmove = "addl";
                    
                    
                }
                else if(input_check(0,0,1,0,0))
                {
                    cancelmove = "addm";
                    
                    
                }
                else if(input_check(0,0,0,1,0))
                {
                    if(ex > 24.9)
                    {
                        cancelmove = "addh";
                        

                        
    
                    }
                    else
                    {
                        cancelmove = "addm";
                        
                        

                        
                    }
                }
            }
            
            if(state_exists("chul") == 1 && chb() == 1)
            {
                if(input_check(2,1,0,0,0))
                {
                    cancelmove = "chul";
                }
                else if(input_check(2,0,1,0,0))
                {
                    cancelmove = "chum";
                }
                else if(input_check(2,0,0,1,0))
                {
                    if(ex > 24.9)
                    {
                        cancelmove = "chuh";
                    }
                    else
                    {
                        cancelmove = "chum";
                    }
                }
            }
        }