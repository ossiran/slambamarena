{
    "id": "cd329960-2929-4fee-a92d-78072da8442d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_darkball",
    "eventList": [
        {
            "id": "09c042a7-3c47-45ea-8eeb-f247f8fed9b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cd329960-2929-4fee-a92d-78072da8442d"
        },
        {
            "id": "4302faf8-94e8-4a8d-8b27-abeb05671f39",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cd329960-2929-4fee-a92d-78072da8442d"
        },
        {
            "id": "f38da0d2-5dde-450d-9ac5-251643224fe3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "cd329960-2929-4fee-a92d-78072da8442d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2132d2fb-2953-4d2d-a20d-f37cdc53310f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "db6fb90d-5b12-4df7-8581-68bf49acc77f",
    "visible": true
}