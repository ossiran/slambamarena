{
    "id": "a8b9a082-bbfc-4142-8870-56f8c5364010",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_myu_rising",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 12,
    "bbox_right": 35,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9af32007-9711-48b9-91c3-fb7296aba4d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8b9a082-bbfc-4142-8870-56f8c5364010",
            "compositeImage": {
                "id": "e7f71ed6-788e-4081-9370-2d1396ea9c6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9af32007-9711-48b9-91c3-fb7296aba4d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5686ab7a-9231-4009-99ab-b5c864bdfa3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9af32007-9711-48b9-91c3-fb7296aba4d3",
                    "LayerId": "2f29e667-0816-4e11-96ea-18e7ecbd48a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2f29e667-0816-4e11-96ea-18e7ecbd48a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8b9a082-bbfc-4142-8870-56f8c5364010",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 32
}