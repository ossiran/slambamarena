{
    "id": "237914a2-6010-4c42-a804-0752c68716c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_qcbm_fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "47fa532e-c3a1-4edb-9e1f-08e77dd1106b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "237914a2-6010-4c42-a804-0752c68716c2",
            "compositeImage": {
                "id": "d32f57ea-d27f-4bb1-9bd3-76b62e673a1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47fa532e-c3a1-4edb-9e1f-08e77dd1106b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30ebe47b-d7bd-4f08-88dc-56492fda703a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47fa532e-c3a1-4edb-9e1f-08e77dd1106b",
                    "LayerId": "27b84477-f52d-4f81-a91f-c83b982f969e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "27b84477-f52d-4f81-a91f-c83b982f969e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "237914a2-6010-4c42-a804-0752c68716c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}