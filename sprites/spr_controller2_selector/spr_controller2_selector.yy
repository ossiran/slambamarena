{
    "id": "6e476f89-42cc-414f-88c2-8c5a0c87b023",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_controller2_selector",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "33701606-5b9c-49d0-9656-fb9883ec1d22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e476f89-42cc-414f-88c2-8c5a0c87b023",
            "compositeImage": {
                "id": "df938487-326b-4d38-a6eb-1b9f6aeaf290",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33701606-5b9c-49d0-9656-fb9883ec1d22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "299e5100-3b01-4a0e-a85c-c08db8411afc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33701606-5b9c-49d0-9656-fb9883ec1d22",
                    "LayerId": "8015119e-eca2-4293-b8fe-b08ca11c5562"
                }
            ]
        },
        {
            "id": "5fd32cb7-690d-4618-a676-8d574b5571c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e476f89-42cc-414f-88c2-8c5a0c87b023",
            "compositeImage": {
                "id": "304dc357-341c-460e-bac2-fd610d6efb6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fd32cb7-690d-4618-a676-8d574b5571c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64a58cc7-ba59-4569-be43-ba674c4fbfcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fd32cb7-690d-4618-a676-8d574b5571c1",
                    "LayerId": "8015119e-eca2-4293-b8fe-b08ca11c5562"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8015119e-eca2-4293-b8fe-b08ca11c5562",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6e476f89-42cc-414f-88c2-8c5a0c87b023",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}