if(state_new == 1)
{
    menu_maxx = 2;
    menu_indexx = 0;
    
    //menu array. 2nd axis is what displayed on menu followed my sys state name
    menu_array[0,0] = "versus";
    menu_array[0,1] = "char_select";
    menu_array[0,2] = rm_select;
    menu_array[1,0] = "training"
    menu_array[1,1] = "char_select";
    menu_array[1,2] = rm_select;
    menu_array[2,0] = "exit";
    menu_array[2,1] = "main_menu";
    menu_array[2,2] = 0;
}

//gamepad loop
for(var i = 0; i < 3; i++)
{
    if(gamepad_is_connected(i) && menu_maxx != 0)
    {
        //down
        if(gamepad_button_check_pressed(i, gp_padd))
        {
            menu_indexx++;
            //looping if over
            if(menu_indexx > menu_maxx)
            {
                menu_indexx = 0;
            }
        }
        
        //up
        if(gamepad_button_check_pressed(i, gp_padu))
        {
            menu_indexx--;
            //looping if under
            if(menu_indexx < 0)
            {
                menu_indexx = menu_maxx;
            }
        }
        
        //select
        if(gamepad_button_check_pressed(i, gp_face1) || gamepad_button_check_pressed(i, gp_start))
        {
            
            
            if(menu_array[menu_indexx,0] == "training")
            {
                training = 1;
            }
            else
            {
                training = 0;
            }
            
            //switching rooms
            if(menu_array[menu_indexx,2] != 0)
            {
                room_goto(menu_array[menu_indexx,2]);
            }
            
            
            
            //switching to selected state
            state_switch(menu_array[menu_indexx,1]);
            //resetting
            menu_maxx = 0;
            menu_indexx = 0;
        }
    }
}

//keyboard

//down
if(keyboard_check_pressed(keyboard_down) || keyboard_check_pressed(vk_down))
{
    menu_indexx++;
    //looping if over
    if(menu_indexx > menu_maxx)
    {
        menu_indexx = 0;
    }
}

//up
if(keyboard_check_pressed(keyboard_up) || keyboard_check_pressed(vk_up))
{
    menu_indexx--;
    //looping if under
    if(menu_indexx < 0)
    {
        menu_indexx = menu_maxx;
    }
}

//select
if(keyboard_check_pressed(keyboard_start))
{
    
    
    if(menu_array[menu_indexx,0] == "training")
    {
        training = 1;
    }
    else
    {
        training = 0;
    }
    
    //switching to selected state
    state_switch(menu_array[menu_indexx,1]);
    //switching rooms
    if(menu_array[menu_indexx,2] != 0)
    {
        room_goto(menu_array[menu_indexx,2]);
    }
    //resetting
    menu_maxx = 0;
    menu_indexx = 0;
}