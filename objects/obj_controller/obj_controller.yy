{
    "id": "5a7184dc-0b13-4d41-998f-2c5c19f74e42",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_controller",
    "eventList": [
        {
            "id": "333d06b8-8369-4ac5-a734-6f8178a9b263",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5a7184dc-0b13-4d41-998f-2c5c19f74e42"
        },
        {
            "id": "520ac36c-0c97-4997-9d2f-83068a3263f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5a7184dc-0b13-4d41-998f-2c5c19f74e42"
        },
        {
            "id": "130dbcbb-9686-4ac5-a2d0-ff75e599a47a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5a7184dc-0b13-4d41-998f-2c5c19f74e42"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d2482485-3701-4c82-a85c-00ad465861d4",
    "visible": true
}