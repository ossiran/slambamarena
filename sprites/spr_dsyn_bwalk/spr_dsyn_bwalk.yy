{
    "id": "d5e63c44-43c2-486d-be37-24e3e53fd752",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dsyn_bwalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 108,
    "bbox_left": 51,
    "bbox_right": 77,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9da0033c-4171-47d1-be03-9e067512cdf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "6c04bd2b-cf70-40aa-92ce-4d562bb69c6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9da0033c-4171-47d1-be03-9e067512cdf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b006845c-8f7b-4741-8bfa-a42b09e08b7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9da0033c-4171-47d1-be03-9e067512cdf8",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "77917125-9346-4763-b1d8-522e3187342a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "a5207d5f-9490-480b-99c1-51895cff5f7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77917125-9346-4763-b1d8-522e3187342a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e824606-cad2-4add-83f1-331f51ee855e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77917125-9346-4763-b1d8-522e3187342a",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "24e62d7a-85d2-477e-9fc5-1a58791da3e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "35b9ef71-141a-47e6-a073-7bbf7e7caa0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24e62d7a-85d2-477e-9fc5-1a58791da3e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c964c3d-3d3e-4f12-ae1a-f543eefca004",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24e62d7a-85d2-477e-9fc5-1a58791da3e0",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "ee06effe-8afa-4575-a4ce-7ed914156ac8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "ea9cd3d1-9136-4fdc-a3c4-ba89587109a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee06effe-8afa-4575-a4ce-7ed914156ac8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1803db44-4760-4746-8531-f4cd9c1ccdf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee06effe-8afa-4575-a4ce-7ed914156ac8",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "148cc8e4-6ddf-42c7-8845-1212614b0125",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "e7b19f19-2174-4942-b780-12f246409111",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "148cc8e4-6ddf-42c7-8845-1212614b0125",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bc33633-e85a-475c-908f-ed324b4d9cb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "148cc8e4-6ddf-42c7-8845-1212614b0125",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "c79c7551-30f8-4ab5-b77a-2247406f657f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "2f6780ec-a60f-4ce8-8a96-19f76f520034",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c79c7551-30f8-4ab5-b77a-2247406f657f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "637b33e6-7d82-4554-8cf8-b7b0a91c6973",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c79c7551-30f8-4ab5-b77a-2247406f657f",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "24b19ea0-edde-4b02-958e-431b2ffb18d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "c3185293-c80e-45b2-a9a9-c425417d7103",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24b19ea0-edde-4b02-958e-431b2ffb18d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7f9c456-4511-4e37-87d3-e9ef0c8a6348",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24b19ea0-edde-4b02-958e-431b2ffb18d2",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "6c3b9e06-4eb0-4bad-a9e7-73bf19407192",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "fb0906d7-04b1-48cc-9331-fd03940529cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c3b9e06-4eb0-4bad-a9e7-73bf19407192",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "767231af-1c27-42ce-b9dc-b699dd974f6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c3b9e06-4eb0-4bad-a9e7-73bf19407192",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "058b99a7-4ea2-4040-b0f0-ebc570f8ba6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "936bec69-ce3a-4547-a3f8-85a60ee119ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "058b99a7-4ea2-4040-b0f0-ebc570f8ba6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "920e96d0-3306-4b2e-ab55-35a4a7895986",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "058b99a7-4ea2-4040-b0f0-ebc570f8ba6c",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "2c7e5e80-8401-4196-81f4-7a75f1e54ce3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "2b1346e3-ddc3-432d-a0a3-8cbdb66d173c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c7e5e80-8401-4196-81f4-7a75f1e54ce3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbf5f9be-c85f-472f-bfb8-7307876538b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c7e5e80-8401-4196-81f4-7a75f1e54ce3",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "e2eb20d9-4878-4f7c-ad7b-bd150f58281f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "49d67ade-9cec-4f77-8ec8-a65b486d596c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2eb20d9-4878-4f7c-ad7b-bd150f58281f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13761e87-de87-4487-871d-5e64fc6b0969",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2eb20d9-4878-4f7c-ad7b-bd150f58281f",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "389bc64b-26fd-46a7-be50-35f008d0afb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "6fd34813-c816-4328-9611-f0f2ca22e52a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "389bc64b-26fd-46a7-be50-35f008d0afb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42d311b0-e25e-4a91-9204-68cadaddddbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "389bc64b-26fd-46a7-be50-35f008d0afb4",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "ce218886-adba-42b3-b73e-ebfb3c8e62e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "9b4de234-11eb-4dde-b779-1862016931c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce218886-adba-42b3-b73e-ebfb3c8e62e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "457b6abb-c428-4ff8-a86b-35ee640b7023",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce218886-adba-42b3-b73e-ebfb3c8e62e4",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "a1f90c02-b9f8-43cf-be43-f02db18770c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "4ee88871-0553-4309-bb6e-c8f8ed2cd130",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1f90c02-b9f8-43cf-be43-f02db18770c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ddc1ae1-35f7-45ce-a1a3-9bb724787e4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1f90c02-b9f8-43cf-be43-f02db18770c3",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "23fee376-1496-4cef-8340-dd833b39b247",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "47b1355c-9262-4b97-8184-e4116570d94f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23fee376-1496-4cef-8340-dd833b39b247",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd04981c-1924-4506-be27-3a8265453202",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23fee376-1496-4cef-8340-dd833b39b247",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "37cde638-efee-487a-a256-26842f13c716",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "bf0f3f3a-d0e7-4a59-8619-709ad98b8b14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37cde638-efee-487a-a256-26842f13c716",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0cf44f2-a125-4304-94fe-a82209399c72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37cde638-efee-487a-a256-26842f13c716",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "8aae8e42-0794-4c73-b3b1-2acbe833510c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "bd4a56a9-0880-405c-ab4e-e73d45030cb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8aae8e42-0794-4c73-b3b1-2acbe833510c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "779ffc6d-957f-4999-b340-20297f32f8fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8aae8e42-0794-4c73-b3b1-2acbe833510c",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "992f599c-7513-43b1-b00d-d41d796b0ca3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "201e09b1-d62b-4879-b16f-5fa47d6c6033",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "992f599c-7513-43b1-b00d-d41d796b0ca3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b59007a-774c-4917-aa50-0153030fe08b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "992f599c-7513-43b1-b00d-d41d796b0ca3",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "30119506-f19c-4354-b810-32f0a2154116",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "69f73c8a-75d2-4047-8db3-5376faa72e9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30119506-f19c-4354-b810-32f0a2154116",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2cdc6e8-ece7-4520-a78d-fd682feed4d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30119506-f19c-4354-b810-32f0a2154116",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "95fa687c-6de6-4b9c-b4df-b779d86f6270",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "cc1d2b74-80e9-4952-9047-161520053ce8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95fa687c-6de6-4b9c-b4df-b779d86f6270",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c12901d9-4d1f-452e-ae0d-859ce236bb0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95fa687c-6de6-4b9c-b4df-b779d86f6270",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "3e0eb927-8df3-4144-9a6c-3f1aa846d230",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "3cef0af0-dde7-4dd7-bdee-f640b415a753",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e0eb927-8df3-4144-9a6c-3f1aa846d230",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "290d5f7f-e713-4383-8a57-71366700e8d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e0eb927-8df3-4144-9a6c-3f1aa846d230",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "9d97e6b8-7322-4c26-b433-f4c1ef0a6c6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "11bbad7e-6091-4d93-a6ee-75399e5651ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d97e6b8-7322-4c26-b433-f4c1ef0a6c6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f67a650-dca0-45c2-b0b7-968b2cc45852",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d97e6b8-7322-4c26-b433-f4c1ef0a6c6a",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "76738333-77e1-4ab7-b10c-3c44b798f149",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "6003dc6e-c194-4cce-a593-58f01eccdb4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76738333-77e1-4ab7-b10c-3c44b798f149",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d150656-9cd1-4ece-b041-d2f417b9e995",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76738333-77e1-4ab7-b10c-3c44b798f149",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "efbdf4e1-e7d9-4c12-9f8c-abd96fc9aaf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "23824c58-254c-47ce-917f-f2d6c329ee66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efbdf4e1-e7d9-4c12-9f8c-abd96fc9aaf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2114bd0e-68e7-4da3-9784-9c6fe2e0592d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efbdf4e1-e7d9-4c12-9f8c-abd96fc9aaf4",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "3907a360-9ad8-449b-b8c8-daa3715e0866",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "b914fae2-c7e6-484d-83f1-d1921f7aa61c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3907a360-9ad8-449b-b8c8-daa3715e0866",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6887a18f-a354-4f27-9780-3942ccb89882",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3907a360-9ad8-449b-b8c8-daa3715e0866",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "e56b1db6-8aee-473c-99d7-1d51c88f8e36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "cf7563bc-4393-4690-8eeb-12b3e706039c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e56b1db6-8aee-473c-99d7-1d51c88f8e36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71261fda-3384-4b55-846a-9f31295cf2af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e56b1db6-8aee-473c-99d7-1d51c88f8e36",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "80625f44-3322-409e-9916-564943d491fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "7fe3ad72-1122-4f0c-9d67-36ef699d6cad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80625f44-3322-409e-9916-564943d491fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dd10c5f-29a4-432b-8c95-8d4ba941fbec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80625f44-3322-409e-9916-564943d491fb",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "bd93e2c6-454f-4abd-aac8-b6f38d045329",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "e498362c-c929-4544-9fca-78099fd6e8f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd93e2c6-454f-4abd-aac8-b6f38d045329",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddaccf8d-5211-4d29-be61-ca63f10aa312",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd93e2c6-454f-4abd-aac8-b6f38d045329",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "4fbbad0e-cf4a-4c73-85ba-1eb3a50a58a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "82a75a1f-85b6-4b65-9364-a2648fcad097",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fbbad0e-cf4a-4c73-85ba-1eb3a50a58a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6c20cda-c8da-49f4-b1ea-73bfa4e80d79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fbbad0e-cf4a-4c73-85ba-1eb3a50a58a4",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "6646225b-e751-47ff-acdc-8dfe7e0663cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "97e8b3ac-1a1f-4d88-ba05-cfa65999f161",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6646225b-e751-47ff-acdc-8dfe7e0663cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47a874d6-7af4-438c-8da4-dd7ca8e414d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6646225b-e751-47ff-acdc-8dfe7e0663cb",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "7e92111e-8b2b-49f6-918f-1b46043fc232",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "a7828d9a-9c45-49fa-83c4-bd20a5bcd5e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e92111e-8b2b-49f6-918f-1b46043fc232",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e32ecc0a-6429-4660-b14d-f11ee666f494",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e92111e-8b2b-49f6-918f-1b46043fc232",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "49a8142c-0702-454f-916d-af6802626623",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "3d41e0a1-29f3-47f0-a42c-eef278250fb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49a8142c-0702-454f-916d-af6802626623",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "182719e7-bef8-4df5-9431-ea888a11902e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49a8142c-0702-454f-916d-af6802626623",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "1776d5ae-ea7d-4d2a-a763-b15fc724c126",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "62c02363-2869-403a-bad7-6d453839a111",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1776d5ae-ea7d-4d2a-a763-b15fc724c126",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4925bf98-1619-438e-9909-0131fac43d81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1776d5ae-ea7d-4d2a-a763-b15fc724c126",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "abe6e8c9-a890-4ca9-9abb-bd6c10465c21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "8f6775ca-bb0b-4b6c-87d3-700df1c46de4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abe6e8c9-a890-4ca9-9abb-bd6c10465c21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d832e14-5ccd-465a-a428-e656791f76cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abe6e8c9-a890-4ca9-9abb-bd6c10465c21",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "825516a6-59ba-4e12-85d1-875f86b321f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "bea5a065-b93d-47ac-8717-a68aff0734a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "825516a6-59ba-4e12-85d1-875f86b321f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d945129e-a267-4cca-a298-9afb954991df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "825516a6-59ba-4e12-85d1-875f86b321f3",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "e117616e-5589-460e-81af-49a34f5237cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "afe6851e-52c2-4067-9e5e-13d72720f9cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e117616e-5589-460e-81af-49a34f5237cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7f10aad-2654-4783-89d6-8b0dc2042ac6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e117616e-5589-460e-81af-49a34f5237cc",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "564146a8-2848-4585-8109-e2e0f32268fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "2a8af918-1b37-4e5d-bdad-b2f09f84507d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "564146a8-2848-4585-8109-e2e0f32268fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26998faa-0d99-41b3-a0e9-53b929e554ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "564146a8-2848-4585-8109-e2e0f32268fa",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "7bcbcfff-bbd4-4636-88ff-7f8c10ad7387",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "e31c397d-50a9-456a-a620-111ef21b31b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bcbcfff-bbd4-4636-88ff-7f8c10ad7387",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10da7ba6-4272-4ec1-8713-911cb514013f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bcbcfff-bbd4-4636-88ff-7f8c10ad7387",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "cca79709-65c8-40f9-9928-d2637c15b3f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "2d4134c0-b3b9-4b4b-9131-686b5897adf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cca79709-65c8-40f9-9928-d2637c15b3f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b02d2d1-d76a-4486-ba1d-8590ca18041c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cca79709-65c8-40f9-9928-d2637c15b3f6",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        },
        {
            "id": "f63f97db-eb92-4237-b845-a1deb60db758",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "compositeImage": {
                "id": "98341e4a-d22b-4b62-b1cb-27d5e008a9b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f63f97db-eb92-4237-b845-a1deb60db758",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb2aa606-4fcd-4a2c-8165-368143b02e8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f63f97db-eb92-4237-b845-a1deb60db758",
                    "LayerId": "f24c2669-491c-41d4-ac4b-131b7c218a3f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "f24c2669-491c-41d4-ac4b-131b7c218a3f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d5e63c44-43c2-486d-be37-24e3e53fd752",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}