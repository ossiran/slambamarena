{
    "id": "fc4b0ebd-3c46-476c-8eb6-72f883788d4f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_jh",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7390eee4-d6f9-4cc3-95e3-528512cbca69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc4b0ebd-3c46-476c-8eb6-72f883788d4f",
            "compositeImage": {
                "id": "bce32056-6600-4be6-a2c7-378530a40ec5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7390eee4-d6f9-4cc3-95e3-528512cbca69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c5f92ef-07dd-4636-a4a5-e4abcbf3660a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7390eee4-d6f9-4cc3-95e3-528512cbca69",
                    "LayerId": "bfc7ae7c-6cbf-4112-b5d5-49808d4dbb69"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bfc7ae7c-6cbf-4112-b5d5-49808d4dbb69",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fc4b0ebd-3c46-476c-8eb6-72f883788d4f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}