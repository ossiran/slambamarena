{
    "id": "39d98c26-f7db-416d-90ca-5a307648e6c7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_chbm_ice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 26,
    "bbox_right": 139,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a3e7605c-0da5-4bf6-9775-51738dc793ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "acce5c01-7856-4843-8cbe-f59f663276f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3e7605c-0da5-4bf6-9775-51738dc793ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bc0fdff-eae1-45ae-a56b-04f28fe8c3bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3e7605c-0da5-4bf6-9775-51738dc793ad",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "5fcef745-5fff-415e-af70-d1d5e171ba0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "5a2b2ba7-e351-4f6b-a426-467990b085b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fcef745-5fff-415e-af70-d1d5e171ba0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5343ef4-74d3-4969-a12b-adb1ca2bbe35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fcef745-5fff-415e-af70-d1d5e171ba0f",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "52e532d0-d0d9-4fbb-823b-6b3c9f57972f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "80228996-d74a-4e62-a366-109fdc1a6bfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52e532d0-d0d9-4fbb-823b-6b3c9f57972f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70493bdc-4abb-40de-83ca-033263f4d6a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52e532d0-d0d9-4fbb-823b-6b3c9f57972f",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "aca6145b-69e4-4169-8eb7-3bfe7a1e1cd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "b3a673cd-b103-449b-8e3b-8fb868d6c2f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aca6145b-69e4-4169-8eb7-3bfe7a1e1cd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31332dec-18e5-4cda-8820-7a3c4d93f7fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aca6145b-69e4-4169-8eb7-3bfe7a1e1cd2",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "9d28f1fb-eb42-4417-8177-5aeb1dfc31c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "401a8eae-bfa2-4562-85e1-b9f9f8944809",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d28f1fb-eb42-4417-8177-5aeb1dfc31c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2800140c-cdef-4fb8-a9e3-0cf2ea097217",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d28f1fb-eb42-4417-8177-5aeb1dfc31c5",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "f1d60462-5c49-4d2e-863f-62dd79636994",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "3bc2fdcd-873c-4936-ae45-754661785e93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1d60462-5c49-4d2e-863f-62dd79636994",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6be1b6a8-f8bd-403a-a051-1f81e9875c57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1d60462-5c49-4d2e-863f-62dd79636994",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "766eaf9e-6fea-418b-95fc-c43e327a41cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "191af9f1-7e79-4e25-8fb9-ee5943dc1db8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "766eaf9e-6fea-418b-95fc-c43e327a41cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfccb133-6c75-43d5-968e-f4c39b35dc2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "766eaf9e-6fea-418b-95fc-c43e327a41cf",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "38a82497-c8f6-4897-90f5-95ddb60bcd44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "728ad23e-13a0-4805-a85a-8bca7e450fec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38a82497-c8f6-4897-90f5-95ddb60bcd44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2b62082-fd85-49eb-9f82-9ac3f458592f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38a82497-c8f6-4897-90f5-95ddb60bcd44",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "c7769af8-15e1-4aca-a800-afe260355956",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "9a882564-1f7a-4a9e-a40f-51617908c5a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7769af8-15e1-4aca-a800-afe260355956",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4be66bf-d58f-4de4-afca-928c10a29a2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7769af8-15e1-4aca-a800-afe260355956",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "7e89d501-c7c5-4b9c-91fe-8ca6e51cdeeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "e739d3a2-f741-4525-9376-13ae882d3e5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e89d501-c7c5-4b9c-91fe-8ca6e51cdeeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1d993d1-de65-4492-9d99-868c1cdb4a43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e89d501-c7c5-4b9c-91fe-8ca6e51cdeeb",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "06c63586-5661-4aa5-81e9-320415c6e167",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "72059ed9-eafc-4d03-966a-8407bc8e2b96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06c63586-5661-4aa5-81e9-320415c6e167",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab45bcf8-d5bf-43d6-907d-60bf03200669",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06c63586-5661-4aa5-81e9-320415c6e167",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "86b166b5-f45a-4a4b-bec2-7e2e6c2537de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "a751c0f4-e5d5-449c-b2b4-6ba15e6105cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86b166b5-f45a-4a4b-bec2-7e2e6c2537de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "151db42b-101e-46fc-909f-ac1e89e8b852",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86b166b5-f45a-4a4b-bec2-7e2e6c2537de",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "0bd31ea1-e983-4914-946c-ab09dc74df05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "6b779e5a-601f-49e4-842a-a0574f2c7e0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bd31ea1-e983-4914-946c-ab09dc74df05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba1f2aa4-b79b-4c68-a92e-f73e965fd986",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bd31ea1-e983-4914-946c-ab09dc74df05",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "703fe60f-ef83-443d-bb07-9b9aa0857c6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "c4bbb358-4cb3-4f00-83e9-54986fda15dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "703fe60f-ef83-443d-bb07-9b9aa0857c6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d81127d7-70cd-4ea5-879c-ccdac0acdd48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "703fe60f-ef83-443d-bb07-9b9aa0857c6e",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "e4699f7e-1275-41ab-8302-9caa6895e6ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "36e8f11e-ac5f-4e76-b803-a74decab7ee5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4699f7e-1275-41ab-8302-9caa6895e6ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd27398d-f411-405b-b993-939ecb5ed844",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4699f7e-1275-41ab-8302-9caa6895e6ac",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "afce7c97-fe13-493a-a6fa-0180f351321b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "27352a07-592a-410b-9c71-1b81b26f13d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afce7c97-fe13-493a-a6fa-0180f351321b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b63ab7b-5a4c-4afc-a5a7-e7377224ffc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afce7c97-fe13-493a-a6fa-0180f351321b",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "00328d74-0272-4bdb-9f72-0116ae5e934b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "e240d5d2-1b19-4c61-a632-9574ce6b141f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00328d74-0272-4bdb-9f72-0116ae5e934b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecec9e66-237e-499a-b775-e684d852dd67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00328d74-0272-4bdb-9f72-0116ae5e934b",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "cc1fd805-d6f8-45ed-84ce-941688234e87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "4413c61a-cff6-450b-a9db-c5e1575b0ba7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc1fd805-d6f8-45ed-84ce-941688234e87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d9be81b-4032-4016-8f9f-47b4a8ea02ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc1fd805-d6f8-45ed-84ce-941688234e87",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "a53e4dcf-9699-4a72-8258-83c3d744f794",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "fbec0bd0-9008-41af-a5b5-60fa47ce174b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a53e4dcf-9699-4a72-8258-83c3d744f794",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f34d543a-ef8e-4b12-87df-ab86906b912e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a53e4dcf-9699-4a72-8258-83c3d744f794",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "163d29d1-fed5-43ac-96e8-3ccfaa13a6dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "8a9655e9-4427-4612-a98e-7ebbbade3cae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "163d29d1-fed5-43ac-96e8-3ccfaa13a6dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c13ba86f-5c7b-42de-837a-9cc284c69922",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "163d29d1-fed5-43ac-96e8-3ccfaa13a6dd",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "387043ae-c812-48de-b954-989cfcdf8d67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "50a2c50a-60e5-42a8-afe6-61477c4553bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "387043ae-c812-48de-b954-989cfcdf8d67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9eef0d5-0616-485b-886b-06d85dfe3d60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "387043ae-c812-48de-b954-989cfcdf8d67",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "10c7c5b9-fb59-4939-9ad9-57d1af4438b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "d03381bf-9eb3-42ac-b1d8-751480c94ca2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10c7c5b9-fb59-4939-9ad9-57d1af4438b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b8e7e1a-d24c-4297-b3b3-ad784e01ef9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10c7c5b9-fb59-4939-9ad9-57d1af4438b0",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "75409f17-65e9-4d3f-b3df-4b741b3acc51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "5ccd1ae0-39b2-49c7-a8cb-5c9dbbfc12d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75409f17-65e9-4d3f-b3df-4b741b3acc51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16a79670-fc1d-4244-a10e-2bc874b59b13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75409f17-65e9-4d3f-b3df-4b741b3acc51",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "2a66d905-802d-4ecc-a719-5010c8b0a262",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "0133a73d-1562-45fe-9fb1-58642b4a41e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a66d905-802d-4ecc-a719-5010c8b0a262",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f33dd5fb-f9d1-438e-816f-73ef416f99a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a66d905-802d-4ecc-a719-5010c8b0a262",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "c54427cf-13b4-4baa-b83b-b9e9b7430fed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "ecbd651d-1922-4d9a-b38e-0774254bf830",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c54427cf-13b4-4baa-b83b-b9e9b7430fed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db5b9eb6-f42a-4a56-ac53-a7dd2416ef9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c54427cf-13b4-4baa-b83b-b9e9b7430fed",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "2c457656-8044-4ed6-98df-b41b912b27f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "cde893d9-adaf-4000-a8d4-79a3263841f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c457656-8044-4ed6-98df-b41b912b27f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "954810bd-606b-4b35-b7e3-d0560842e24f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c457656-8044-4ed6-98df-b41b912b27f3",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "b2d21db2-90c4-4244-bc94-9cb8ab61d8e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "08136818-c5b9-470e-bde1-67c6a891aa81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2d21db2-90c4-4244-bc94-9cb8ab61d8e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f43a239b-a302-48e0-9128-a3bc206228b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2d21db2-90c4-4244-bc94-9cb8ab61d8e8",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "071b174b-24d2-4d48-b42b-df1a0c36ece8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "c2c37d30-c70e-47ab-a227-44c4b7c498bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "071b174b-24d2-4d48-b42b-df1a0c36ece8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4fa7a06-193d-41d1-95d0-f4c3bcb96646",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "071b174b-24d2-4d48-b42b-df1a0c36ece8",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "a94686ec-6a20-4f85-ad82-11fbc9b78c0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "4a0cec6b-bc40-4f9f-8196-90dd31f57b55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a94686ec-6a20-4f85-ad82-11fbc9b78c0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb270bde-acb6-48b5-ba68-3ea181955bac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a94686ec-6a20-4f85-ad82-11fbc9b78c0d",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "ab3f22c6-f0b3-4c60-8a1e-341a8da379a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "840f7268-115b-4d38-bf3e-70240420ae35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab3f22c6-f0b3-4c60-8a1e-341a8da379a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be481050-b05d-4067-a983-f1469d7416bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab3f22c6-f0b3-4c60-8a1e-341a8da379a0",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "f2d91eeb-9977-4c39-b44b-f12feb0421e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "4708a87a-0aaa-4825-818c-82a93731c25a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2d91eeb-9977-4c39-b44b-f12feb0421e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7952d11e-29ef-4ccc-9669-17c596c70496",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2d91eeb-9977-4c39-b44b-f12feb0421e7",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "df0e3532-9ea0-4eef-aa71-bb4d2abb1e86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "021fb5e4-41ab-46d7-9a33-0c75908c4f90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df0e3532-9ea0-4eef-aa71-bb4d2abb1e86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cc698d1-d190-4cbc-b740-efc5e5220ca7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df0e3532-9ea0-4eef-aa71-bb4d2abb1e86",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "5ab95b29-1853-4ed1-8462-cf489d3a9e9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "135eac06-38fe-48c1-b40c-2d2e01bce732",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ab95b29-1853-4ed1-8462-cf489d3a9e9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e5fbf37-53e5-4433-afe1-f6eaca985218",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ab95b29-1853-4ed1-8462-cf489d3a9e9d",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        },
        {
            "id": "78421ce0-da33-41ed-9d43-87e985e7f4aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "compositeImage": {
                "id": "c39cf362-587f-44de-a9f3-f8985a1b836a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78421ce0-da33-41ed-9d43-87e985e7f4aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "800507a7-39d0-4eb2-9215-93a00e97f582",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78421ce0-da33-41ed-9d43-87e985e7f4aa",
                    "LayerId": "b0836a32-5fe8-4b7e-8424-46ddf07514a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "b0836a32-5fe8-4b7e-8424-46ddf07514a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "39d98c26-f7db-416d-90ca-5a307648e6c7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 175,
    "xorig": 63,
    "yorig": 87
}