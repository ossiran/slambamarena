{
    "id": "5935fe86-c75b-4896-8327-62f5727e1488",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_66h_fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "774cf055-db94-4e20-be86-28dc1261cc43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5935fe86-c75b-4896-8327-62f5727e1488",
            "compositeImage": {
                "id": "aa1ccd8c-2263-4893-bd43-696a2e416c67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "774cf055-db94-4e20-be86-28dc1261cc43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d51fa97-b645-45e9-8df6-036037d24ac3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "774cf055-db94-4e20-be86-28dc1261cc43",
                    "LayerId": "090fa00c-af2b-483f-8f80-165cb807d1fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "090fa00c-af2b-483f-8f80-165cb807d1fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5935fe86-c75b-4896-8327-62f5727e1488",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}