{
    "id": "1fb82c67-7ac5-4fe1-a113-8eee52ae54b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_myu_airneutral",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 12,
    "bbox_right": 35,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1130dff4-4adc-4218-bbeb-7d15a74b738f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fb82c67-7ac5-4fe1-a113-8eee52ae54b9",
            "compositeImage": {
                "id": "32be4710-b4bd-42e1-921a-cf20ca659330",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1130dff4-4adc-4218-bbeb-7d15a74b738f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65495b93-5d54-40e9-b08f-41872c6c7fd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1130dff4-4adc-4218-bbeb-7d15a74b738f",
                    "LayerId": "9c18cedd-7344-443a-bbc8-4e9305620d62"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9c18cedd-7344-443a-bbc8-4e9305620d62",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1fb82c67-7ac5-4fe1-a113-8eee52ae54b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}