if(state_new == 1)
{
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 16;
	    image_yscale  = 47;
	    xshift = -2
	}

	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -28;
	    xscale  = 26;
	    yscale  = 60;
	    xshift  = -2;
	}
    sprite_index = spr_bill_crouch;
}

image_index = -1;

if(key_down == 0)
{
    state_switch("stand")
}

vx = Approach(vx,0,ground_fric);

image_xscale = -frontOrBack;


//clight
if(input_check(2,1,0,0,0) || input_check(1,1,0,0,0) || input_check(3,1,0,0,0))
{
    state_switch("clight");
}

//cmedium
if(input_check(2,0,1,0,0) || input_check(1,0,1,0,0) || input_check(3,0,1,0,0))
{
    state_switch("cmedium");
}

//cheavy
if(input_check(2,0,0,1,0) || input_check(1,0,0,1,0) || input_check(3,0,0,1,0))
{
    state_switch("cheavy");
}

//qcf
if(qcf_buffer > 0)
{
    if(input_check(0,0,0,1,0))
    {
        if(ex > 49.9)
        {
            state_switch("qcfh");
        }
        else
        {
            state_switch("qcfm");
        }
    }
    else if(input_check(0,0,1,0,0))
    {
        state_switch("qcfm");
    }
    else if(input_check(0,1,0,0,0))
    {
        state_switch("qcfl");
    }
}

//qcb
if(qcb_buffer > 0)
{
    if(input_check(0,0,0,1,0))
    {
        if(ex > 49.9)
        {
            state_switch("qcbh");
        }
        else
        {
            state_switch("qcbm");
        }
    }
    else if(input_check(0,0,1,0,0))
    {
        state_switch("qcbm");
    }
    else if(input_check(0,1,0,0,0))
    {
        state_switch("qcbl");
    }
}

//srk
if(srk_buffer > 0)
{
    if(input_check(0,0,0,1,0))
    {
        if(ex > 49.9)
        {
            state_switch("srkh");
        }
        else
        {
            state_switch("srkm");
        }
    }
    else if(input_check(0,0,1,0,0))
    {
        state_switch("srkm");
    }
    else if(input_check(0,1,0,0,0))
    {
        state_switch("srkl");
    }
}

if(dqcf_buffer > 0 && ex > 49.9)
{
    if(input_check(0,0,0,1,0))
    {
        state_switch("dqcf");
    }
    else if(input_check(0,0,1,0,0))
    {
        state_switch("dqcf");
    }
    else if(input_check(0,1,0,0,0))
    {
        state_switch("dqcf");
    }
}

//grab
if(input_check(0,1,0,0,1))
{
    state_switch("grab");
}


//block
if(blockstun > 0)
{
    state_switch("cblock")
}

//hitstun
if(hitstun > 0)
{
    state_switch("hitstun");   
}