{
    "id": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_fwalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 30,
    "bbox_right": 111,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "57a10f15-bba2-4faf-af6c-e73286f2eed3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "1a5abb4e-ed35-4062-af3c-7690e351be85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57a10f15-bba2-4faf-af6c-e73286f2eed3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d404496-9688-4748-912a-c70d19b0cd03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57a10f15-bba2-4faf-af6c-e73286f2eed3",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "6a6b033d-21ac-405a-804f-eddddecb57b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "9377b2c1-a8d9-4da6-a42e-ff7ffbcd72b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a6b033d-21ac-405a-804f-eddddecb57b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54797b19-383f-4989-9559-25e8bc7375e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a6b033d-21ac-405a-804f-eddddecb57b2",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "3a4ffdbb-0c7c-4246-b331-5904c86ef717",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "0088041b-97a2-400f-beae-8fce5bb2338f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a4ffdbb-0c7c-4246-b331-5904c86ef717",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5623bb1-6352-42e1-bc8c-bef5ada4e062",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a4ffdbb-0c7c-4246-b331-5904c86ef717",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "b46277af-97d9-40e2-a277-dd03825e1b43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "4c0c6cf3-d03d-47de-91ed-dafd78c30a96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b46277af-97d9-40e2-a277-dd03825e1b43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a6cb84a-34cd-418a-b7ec-23356cd266ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b46277af-97d9-40e2-a277-dd03825e1b43",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "138dd371-57df-48f2-a7b2-2222fb249aa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "3ae1e481-a964-4aed-8e18-98ba263f77f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "138dd371-57df-48f2-a7b2-2222fb249aa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21c4a658-ee78-4c8d-b351-aa6bf5224b01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "138dd371-57df-48f2-a7b2-2222fb249aa5",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "cca0bb07-9316-4f95-942f-8a07949256ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "c9e8df5e-30d7-47f0-a347-16efbfc98e23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cca0bb07-9316-4f95-942f-8a07949256ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00938a41-3aaa-4b50-a10a-08be27f6bac6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cca0bb07-9316-4f95-942f-8a07949256ad",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "a9f63fa5-d378-4067-afa2-5f624fefcca2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "e9abf204-c631-4a52-bbfb-ef91a4b45393",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9f63fa5-d378-4067-afa2-5f624fefcca2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28ff834d-9d40-4720-982d-7655f856baa3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9f63fa5-d378-4067-afa2-5f624fefcca2",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "e8b8759c-43fe-427a-99f1-3170c247be0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "6e22d25b-aa95-4cb0-9eeb-2d64aeee2f72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8b8759c-43fe-427a-99f1-3170c247be0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f78b8a30-fb36-4a9d-94dc-fd10ee7cae7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8b8759c-43fe-427a-99f1-3170c247be0f",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "2536245c-b998-446a-ad03-3b60cf9eeb88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "7e46c1f8-48b2-4ca7-a6aa-56cf6bd2cf8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2536245c-b998-446a-ad03-3b60cf9eeb88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e57c8cb-ab5e-48a4-8028-84143084ce82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2536245c-b998-446a-ad03-3b60cf9eeb88",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "f3416a7b-0eec-4bb8-9210-024cc7c08726",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "1b375128-6165-4edd-9c6c-b180be78f5b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3416a7b-0eec-4bb8-9210-024cc7c08726",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b97d765e-aae6-4e1a-af4f-b520932af249",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3416a7b-0eec-4bb8-9210-024cc7c08726",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "001f2b3a-aa65-498c-adf7-b1c6918d828f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "afe19fb8-02f9-420b-9af1-344f77538220",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "001f2b3a-aa65-498c-adf7-b1c6918d828f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd090a74-a215-4ef6-a5a3-ae8bd1a8b904",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "001f2b3a-aa65-498c-adf7-b1c6918d828f",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "f0586af0-f78a-4e8e-966a-0798d3c576ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "0c68a85c-7ab2-4c63-8f1c-34086a058883",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0586af0-f78a-4e8e-966a-0798d3c576ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01f0d843-2950-4d6c-9e6b-63411c195925",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0586af0-f78a-4e8e-966a-0798d3c576ef",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "e274457b-6972-4e4b-8806-4966d18a6a2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "f74a26fa-8240-49e4-9166-aab98e7d6b41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e274457b-6972-4e4b-8806-4966d18a6a2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef209912-6dc6-4f11-9a17-878dc06ae832",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e274457b-6972-4e4b-8806-4966d18a6a2a",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "4224351e-fb47-4ba3-9dde-497be1c5712e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "7f5b4921-27a4-4ca5-a079-144b53d0abd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4224351e-fb47-4ba3-9dde-497be1c5712e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fccb800-b93a-4a2e-b375-c621c14f7099",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4224351e-fb47-4ba3-9dde-497be1c5712e",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "b4f562fe-619c-467e-8e83-416cc1b621b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "cf4d443f-8731-43fd-be02-705e43061a88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4f562fe-619c-467e-8e83-416cc1b621b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df1a9603-9f08-49e4-8bc8-2f865021993f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4f562fe-619c-467e-8e83-416cc1b621b7",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "999cdaf3-2a33-46ec-93fa-ff36b243d430",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "48641d85-9f6e-4b85-8b80-635478ae3044",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "999cdaf3-2a33-46ec-93fa-ff36b243d430",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43ed38f5-5a8c-45ca-8766-aa8de7293256",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "999cdaf3-2a33-46ec-93fa-ff36b243d430",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "913fb60b-0a1f-4819-882d-0d20ba481c8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "2fac54c8-35a9-49e1-b7c5-5c963e913e54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "913fb60b-0a1f-4819-882d-0d20ba481c8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46377106-98bf-44f0-9008-6f42758a2870",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "913fb60b-0a1f-4819-882d-0d20ba481c8d",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "708cfccf-03a7-4026-9713-ac83402e06d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "3540c43f-00aa-4eec-949f-d3d4b89c2de0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "708cfccf-03a7-4026-9713-ac83402e06d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cdef239-bb30-4c88-b6fc-e0575e4386e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "708cfccf-03a7-4026-9713-ac83402e06d1",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "6c0f3c0f-f847-4ecd-9ec6-bef6f68cb6b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "4c2c9ec6-6e4c-474e-8186-9d3eb12c80a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c0f3c0f-f847-4ecd-9ec6-bef6f68cb6b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cff7a2f5-f3a5-4891-b7e7-a44843518c9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c0f3c0f-f847-4ecd-9ec6-bef6f68cb6b8",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "f525e4b4-143c-47f2-9b32-6ff84ed4cae7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "3762e440-ef8f-4e6c-9f8b-7b5d221e9721",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f525e4b4-143c-47f2-9b32-6ff84ed4cae7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ee62d70-5bf0-4897-a4f0-250ca66a2e1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f525e4b4-143c-47f2-9b32-6ff84ed4cae7",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "cc530852-fe65-4a32-bd3e-de56a088cf31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "d25bd91d-02c3-4b07-a4fd-5e36f143fa07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc530852-fe65-4a32-bd3e-de56a088cf31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e61dfd6d-3d76-4a08-8d24-afb9f37df4ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc530852-fe65-4a32-bd3e-de56a088cf31",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "52ad1d5a-e4fb-4941-af30-9395afd866de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "1d027912-0285-4273-9a72-c2347ce16a2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52ad1d5a-e4fb-4941-af30-9395afd866de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e42ce66-4803-4083-aea8-9ab9d7839af0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52ad1d5a-e4fb-4941-af30-9395afd866de",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "4ee358a5-9f9e-46d0-b387-9cd131b48185",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "024118c9-ce56-4eb8-9753-d2727b7971e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ee358a5-9f9e-46d0-b387-9cd131b48185",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2bd5dcc-8690-4b58-9ef4-5fbfce412e5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ee358a5-9f9e-46d0-b387-9cd131b48185",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "69817d31-6325-4b17-85b0-3bbb33a9a033",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "d2b14522-5d34-4f83-931f-eccbd5091921",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69817d31-6325-4b17-85b0-3bbb33a9a033",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "757204a5-6f24-44bf-a6a7-9ebae265aa7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69817d31-6325-4b17-85b0-3bbb33a9a033",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "25ae44dd-f626-4f6a-abec-f0bf2deb4a2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "0ad33fdc-4087-4fd6-b22c-cccc7a09b5f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25ae44dd-f626-4f6a-abec-f0bf2deb4a2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "039b20bf-ae46-4350-8991-da7406302e70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25ae44dd-f626-4f6a-abec-f0bf2deb4a2d",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "ae8fba57-d868-47d0-9e83-0b2ef892e87c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "c80d53e6-ed73-4b26-85ff-4f85a0c7c4c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae8fba57-d868-47d0-9e83-0b2ef892e87c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f57176f-d26b-408f-aebd-57e1a010aa3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae8fba57-d868-47d0-9e83-0b2ef892e87c",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "451d493c-921c-4917-897f-e3d68a15697f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "b5499143-45e4-4d11-b18b-af99d7ccd238",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "451d493c-921c-4917-897f-e3d68a15697f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "750c0410-90d4-4fc7-a842-603e2551d9c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "451d493c-921c-4917-897f-e3d68a15697f",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "58296a53-a0ca-41a5-8271-b3803011e4f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "40edd2d8-1546-485c-8370-93581603a266",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58296a53-a0ca-41a5-8271-b3803011e4f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24f480d2-4c03-43cb-b5db-e9b72ac4fb5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58296a53-a0ca-41a5-8271-b3803011e4f5",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "2f34b13a-cae2-4ccc-8ebf-10a318fcb8ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "0e8b0454-a72b-49bb-9ef9-97a561336147",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f34b13a-cae2-4ccc-8ebf-10a318fcb8ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7662ac83-752e-4601-9909-0758900f1424",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f34b13a-cae2-4ccc-8ebf-10a318fcb8ae",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "fce8c6f2-e884-4a42-9495-ca4d4ad143ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "fb97062a-764c-45f0-bd38-2920c0c24bcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fce8c6f2-e884-4a42-9495-ca4d4ad143ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7001d0eb-e985-4a96-8aac-6cddde7942fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fce8c6f2-e884-4a42-9495-ca4d4ad143ae",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "88fc7c1b-4435-4f7f-8483-9bab7ac629ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "972ae848-84e8-4826-8b3c-0ae2af837ab0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88fc7c1b-4435-4f7f-8483-9bab7ac629ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2421e9ce-4db6-40a0-a15b-44e8e9da0f70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88fc7c1b-4435-4f7f-8483-9bab7ac629ef",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "9f16278e-b562-4500-8d11-0ce71e4721cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "cababe3e-e28e-4429-9952-372508636e28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f16278e-b562-4500-8d11-0ce71e4721cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57019a6f-7640-460a-9394-b604d0797d00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f16278e-b562-4500-8d11-0ce71e4721cf",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "ef009e7b-68c5-4ebd-a60b-090645fe237a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "7239e999-1621-416a-b2b6-320c0499eee4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef009e7b-68c5-4ebd-a60b-090645fe237a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d28a683a-6851-4727-bf14-0fe52718e3a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef009e7b-68c5-4ebd-a60b-090645fe237a",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "794d34f3-1146-4b31-bc5c-5b3c586a9447",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "53d44ea3-951a-4dd7-aa47-0f7948066ad0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "794d34f3-1146-4b31-bc5c-5b3c586a9447",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ff2b213-5d7e-4459-a534-e9804be37f67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "794d34f3-1146-4b31-bc5c-5b3c586a9447",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "a593e9d7-b552-41d8-b843-1e977ff85679",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "acf944bf-f7dc-4289-ac97-a85c4ec02b55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a593e9d7-b552-41d8-b843-1e977ff85679",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a0f232e-f775-48c4-87a4-8a6b2d323ca4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a593e9d7-b552-41d8-b843-1e977ff85679",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "dfb889b0-4de6-4c10-b4c5-b4f5bf3d77e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "08887d89-940d-4da3-bc10-2f6389a1db39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfb889b0-4de6-4c10-b4c5-b4f5bf3d77e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6b73289-b91d-4e8f-9dde-3274982b0aff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfb889b0-4de6-4c10-b4c5-b4f5bf3d77e9",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "af0ceaa0-103e-4bec-b150-3996364586c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "cb286ac7-2870-4458-bff7-a8ddd5200f8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af0ceaa0-103e-4bec-b150-3996364586c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdca249c-3d48-4658-984b-1be599b8042d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af0ceaa0-103e-4bec-b150-3996364586c6",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "b5bb1bff-5077-46b9-9e25-dbcf772c798d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "f0f710b6-d57b-48d5-b852-6a110c22e63f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5bb1bff-5077-46b9-9e25-dbcf772c798d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbd0ff16-d94c-4fa0-8837-54153cc0e64e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5bb1bff-5077-46b9-9e25-dbcf772c798d",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "06a4fec1-c5fb-4f65-9fef-c0e3dc3dff00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "5af9a33e-ff87-4609-9937-9ab381fd18e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06a4fec1-c5fb-4f65-9fef-c0e3dc3dff00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5373574d-febf-40e8-a712-1c60d773aebf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06a4fec1-c5fb-4f65-9fef-c0e3dc3dff00",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "f5b129d5-2ba3-45b7-90af-18d632914828",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "1b3940f3-5b75-439c-ab39-93f9f6e5db96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5b129d5-2ba3-45b7-90af-18d632914828",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2e12328-3c3f-45bd-9168-3c0e2c399f66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5b129d5-2ba3-45b7-90af-18d632914828",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "72f76b10-a00d-4053-9250-21799bd3c9d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "f31d4eb5-da50-4a1d-9a0d-e4096bd40e73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72f76b10-a00d-4053-9250-21799bd3c9d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e54d0922-a0a8-42b2-b6eb-4eeed336b45a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72f76b10-a00d-4053-9250-21799bd3c9d9",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "9a4f472d-b571-432f-ad84-386edfd5c422",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "aa5ec9fe-b1de-4eb1-b0d4-897651137610",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a4f472d-b571-432f-ad84-386edfd5c422",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15d710ef-6ccb-4b6d-99d7-c196d0d833fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a4f472d-b571-432f-ad84-386edfd5c422",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "12e3b64d-1688-4232-bb5e-81d9e0a624c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "7c00dbe9-d375-4c59-a4f7-9a756e85dec1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12e3b64d-1688-4232-bb5e-81d9e0a624c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ef59f00-8830-4b20-9504-5e2971289389",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12e3b64d-1688-4232-bb5e-81d9e0a624c8",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "4351fd75-1ae4-4c2c-be75-89dffc8dc0e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "7db3592e-5b81-4f52-a2c9-8bab7df8c3e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4351fd75-1ae4-4c2c-be75-89dffc8dc0e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5afeedaa-884b-4569-81e6-140df8532bd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4351fd75-1ae4-4c2c-be75-89dffc8dc0e0",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "479e6d4a-dc9a-49d3-9d9f-e1846de5db14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "818c49eb-af1c-4e09-a303-4d99d2148211",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "479e6d4a-dc9a-49d3-9d9f-e1846de5db14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58a834d6-b099-4f5e-9f30-b7ddef573c03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "479e6d4a-dc9a-49d3-9d9f-e1846de5db14",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "c80934cd-be61-49a8-9799-ecaa3c41ab7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "364b69d1-1c45-4159-abea-a77ca1b5b2c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c80934cd-be61-49a8-9799-ecaa3c41ab7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f588d726-f745-4230-bd3c-2f3c855bd6dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c80934cd-be61-49a8-9799-ecaa3c41ab7a",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "7a31c7b3-decf-4386-aecb-d2ac8dd020ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "2d126b4b-874f-4421-9540-c84917b0aff6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a31c7b3-decf-4386-aecb-d2ac8dd020ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4896d955-bea3-48af-a455-53f8c5dbaf63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a31c7b3-decf-4386-aecb-d2ac8dd020ad",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "a6012926-1a42-45f7-8b42-22098d25191d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "f6b891eb-8899-4bba-934f-13260cd07916",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6012926-1a42-45f7-8b42-22098d25191d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b30c1a68-aac6-4ac3-82c4-2553ff329ed2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6012926-1a42-45f7-8b42-22098d25191d",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "74938dd1-bc98-4a5a-9ae2-0ae56e9b0e2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "1c871105-6bb7-4c84-83c0-4ebd10fb86b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74938dd1-bc98-4a5a-9ae2-0ae56e9b0e2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2939af8a-4812-4af7-8376-a1c8f3aec0a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74938dd1-bc98-4a5a-9ae2-0ae56e9b0e2c",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "8ed2448a-c14b-4193-a462-5cd2d836105d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "70a38646-6b79-4526-b830-a77da7087360",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ed2448a-c14b-4193-a462-5cd2d836105d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb77b956-a55f-4b2f-931c-10ce60333953",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ed2448a-c14b-4193-a462-5cd2d836105d",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "024e12c0-d792-4d11-9889-020d78cdf281",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "708807cc-ea83-48fd-b281-9a158c0238f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "024e12c0-d792-4d11-9889-020d78cdf281",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7717ec39-f77c-498a-b2e3-e8560afeede4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "024e12c0-d792-4d11-9889-020d78cdf281",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "108485bf-dd83-4e52-9b3a-f7bc2aa8e06b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "66e7e656-1810-466d-b200-388c9bf87bee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "108485bf-dd83-4e52-9b3a-f7bc2aa8e06b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79ef0191-e0d1-468c-9b21-d534feeaee4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "108485bf-dd83-4e52-9b3a-f7bc2aa8e06b",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "530f8f06-bcf6-49fa-b919-62d0c28d8e4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "a5df2192-5dfe-4e86-b652-e3ead7bdc405",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "530f8f06-bcf6-49fa-b919-62d0c28d8e4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55051f36-a1ab-4c9d-8072-c7648feadb21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "530f8f06-bcf6-49fa-b919-62d0c28d8e4b",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "21b0697f-9d0d-47a0-adbf-a72c984fdc60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "8c8d5fba-d8ce-4eea-a10c-1391f4112110",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21b0697f-9d0d-47a0-adbf-a72c984fdc60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e50e4432-7077-47bc-b8e9-b2b4ab1627f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21b0697f-9d0d-47a0-adbf-a72c984fdc60",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "0b136637-885b-469c-a184-9fcbbb59cde1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "351ae095-800d-43fc-b609-d494778000df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b136637-885b-469c-a184-9fcbbb59cde1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "902c6a2b-d372-447b-9e4e-1087337849ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b136637-885b-469c-a184-9fcbbb59cde1",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "02e899d6-0cbd-4fcb-be5d-9a9851f49b8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "47045c6e-4f9f-4866-a818-bfcfc7a9ed07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02e899d6-0cbd-4fcb-be5d-9a9851f49b8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f5d2b89-f884-45cf-8567-993de2a0431a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02e899d6-0cbd-4fcb-be5d-9a9851f49b8c",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "f7ccd99d-87d4-4d10-8346-c7b46864b62d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "ce140525-122b-423b-ab9b-6f9d986b52b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7ccd99d-87d4-4d10-8346-c7b46864b62d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e472f1c-0234-4037-a694-3b4987906d81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7ccd99d-87d4-4d10-8346-c7b46864b62d",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "ded3347a-627f-41ef-b5f7-c944801ea90b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "9d480772-7542-494f-b5d0-bc887d837992",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ded3347a-627f-41ef-b5f7-c944801ea90b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73a8e12a-b9ea-4e99-9923-c21848f7f93f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ded3347a-627f-41ef-b5f7-c944801ea90b",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "3953750a-cb31-4138-9833-b2ef3cd21b93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "9d09c63f-0e7d-47ec-a637-7537a7e5fbb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3953750a-cb31-4138-9833-b2ef3cd21b93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e06f1da0-d986-40da-b5f2-51729180fae3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3953750a-cb31-4138-9833-b2ef3cd21b93",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        },
        {
            "id": "c8a142b8-053c-438d-9a8a-0fa7c008dfca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "compositeImage": {
                "id": "2ea6bae9-77ee-4047-b37b-cde603d1cd36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8a142b8-053c-438d-9a8a-0fa7c008dfca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b42aa554-33f0-4bee-b26b-2564fc8adf85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8a142b8-053c-438d-9a8a-0fa7c008dfca",
                    "LayerId": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "b2a3eb51-aa2f-4a26-a5cc-1866ab3faf87",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df227d04-40ea-49d3-b1fd-9c5d9de1412a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 63,
    "yorig": 87
}