{
    "id": "262ce8a2-329f-4411-89de-3270d29d72c7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_66m_ice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9d0c0a8-2315-439e-8b7e-af775210af8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "262ce8a2-329f-4411-89de-3270d29d72c7",
            "compositeImage": {
                "id": "3ddb9398-9b26-4a78-b362-b8e5dfbfbc32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9d0c0a8-2315-439e-8b7e-af775210af8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce5b66e0-f718-4a62-9606-af41656dd983",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9d0c0a8-2315-439e-8b7e-af775210af8e",
                    "LayerId": "0fb6ad21-7d41-458c-8b18-608e62e03c5f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0fb6ad21-7d41-458c-8b18-608e62e03c5f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "262ce8a2-329f-4411-89de-3270d29d72c7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}