{
    "id": "212cce69-397a-4bd8-92ae-888b1739b98f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dsyn_jumpsquat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 49,
    "bbox_right": 79,
    "bbox_top": 48,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e3e4a54-6396-451f-95b8-7649f9a24bd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "212cce69-397a-4bd8-92ae-888b1739b98f",
            "compositeImage": {
                "id": "f8802c34-a290-40de-9231-1348480d5192",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e3e4a54-6396-451f-95b8-7649f9a24bd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe5bf0e6-a5ff-466f-8ffc-6d1d99472048",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e3e4a54-6396-451f-95b8-7649f9a24bd6",
                    "LayerId": "d01aaa44-9274-4b89-a035-19c02bb4b5b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d01aaa44-9274-4b89-a035-19c02bb4b5b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "212cce69-397a-4bd8-92ae-888b1739b98f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}