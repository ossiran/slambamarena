//

if(state_timer == 0)
{
        if(player1 == 0)
        {
		    player1 = instance_create_layer(300,700,"instances",player1_char);
        }
        if(player2 == 0)
        {
            player2 = instance_create_layer(500,700,"instances",player2_char);
        }
		
		
		player1.player_num = 1;
		player2.player_num = 2;
		player1.controller_num = player1_controller;
		player2.controller_num = player2_controller;
		
		players_control = 2;
		
		game_set_speed(60,gamespeed_fps);
		
		x = room_width /2;
		y = 700;
}
else
{
    
    
    //position
    var cameradistx = (player1.x + player2.x) /2;
    var cameradisty = (player1.y + player2.y) /2;
    
    //screen shake
    var ranx1 = 0;
    var rany1 = 0;
    
    if(player1.hitpause > 10 || player2.hitpause > 10)
    {
        ranx1 = irandom_range(-1,1);
        rany1 = irandom_range(-1,1);
    }
    
    
    
    
    if(cameradistx > 212 && cameradistx < room_width - 212)
    {
        x = Approach(x,cameradistx,5) + ranx1;
    }
    else
    {
        if(cameradistx < 212)
        {
            x = Approach(x,212,5) + ranx1;
        }
        if(cameradistx > room_width - 212)
        {
            x = Approach(x,room_width - 212,5) + ranx1;
        }
    }
    
    
    if(cameradisty < 650)
    {
        y = Approach(y, 640, 2) + rany1;
    }
    else if(cameradisty < 698)
    {
        y = Approach(y, 685, 2) + rany1;
    }
    else
    {
         y = Approach(y,699,2) + rany1;
    }
}