{
    "id": "e0cf87bd-6f57-4ae8-86ef-11805740632d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_barbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2414be04-0b8c-4ede-af2c-916d63a0aece",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0cf87bd-6f57-4ae8-86ef-11805740632d",
            "compositeImage": {
                "id": "e688e33a-8f6a-4f09-aca8-61cd40934705",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2414be04-0b8c-4ede-af2c-916d63a0aece",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c87fc7a-691f-4500-8c7d-9d5124bb8305",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2414be04-0b8c-4ede-af2c-916d63a0aece",
                    "LayerId": "574b2ae2-1e60-437d-9599-f641faa845c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "574b2ae2-1e60-437d-9599-f641faa845c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0cf87bd-6f57-4ae8-86ef-11805740632d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}