{
    "id": "a784ab67-0d58-44b9-8541-902d5787c1bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_myu_airdashB",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 12,
    "bbox_right": 35,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8574e0f2-ceea-47fa-9df7-da03493f03e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a784ab67-0d58-44b9-8541-902d5787c1bf",
            "compositeImage": {
                "id": "10e6f769-76ac-496c-b8e2-f4b45b346031",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8574e0f2-ceea-47fa-9df7-da03493f03e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2e0085d-0f61-4aec-b725-26336b511640",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8574e0f2-ceea-47fa-9df7-da03493f03e3",
                    "LayerId": "630dafe3-f19e-4aa7-9901-9f7590491931"
                }
            ]
        },
        {
            "id": "2f381617-f9ce-49ca-bc6b-f949791df1b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a784ab67-0d58-44b9-8541-902d5787c1bf",
            "compositeImage": {
                "id": "e6c5282f-02be-4131-841d-f69f7d4afddc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f381617-f9ce-49ca-bc6b-f949791df1b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf97f5d3-b139-4aa6-850c-ad8ce4dbe2ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f381617-f9ce-49ca-bc6b-f949791df1b5",
                    "LayerId": "630dafe3-f19e-4aa7-9901-9f7590491931"
                }
            ]
        },
        {
            "id": "fe9ff1e5-6e2b-4701-a32a-077f7c5bbe95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a784ab67-0d58-44b9-8541-902d5787c1bf",
            "compositeImage": {
                "id": "9b1f2d6d-a6eb-49f9-9825-d241ddc6c7ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe9ff1e5-6e2b-4701-a32a-077f7c5bbe95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4867d4a7-67de-4f29-a195-bf6dbc91e64c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe9ff1e5-6e2b-4701-a32a-077f7c5bbe95",
                    "LayerId": "630dafe3-f19e-4aa7-9901-9f7590491931"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "630dafe3-f19e-4aa7-9901-9f7590491931",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a784ab67-0d58-44b9-8541-902d5787c1bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 32
}