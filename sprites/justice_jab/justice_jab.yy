{
    "id": "085c2050-4789-4f48-8bd7-692093da2112",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "justice_jab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 145,
    "bbox_left": 57,
    "bbox_right": 123,
    "bbox_top": 74,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29754359-9e39-4cca-8266-f8519fa1fd62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "085c2050-4789-4f48-8bd7-692093da2112",
            "compositeImage": {
                "id": "702f8135-688f-41d2-a6da-ab03ed75c33c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29754359-9e39-4cca-8266-f8519fa1fd62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "257293b7-d0c6-467f-baf0-5863bc193e1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29754359-9e39-4cca-8266-f8519fa1fd62",
                    "LayerId": "5abd8213-2508-4129-9975-a97b62629623"
                }
            ]
        },
        {
            "id": "179ebc8c-1eca-4e71-86d4-282b8ccd27d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "085c2050-4789-4f48-8bd7-692093da2112",
            "compositeImage": {
                "id": "841747a1-3e49-4f5b-9b75-da5d33901acd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "179ebc8c-1eca-4e71-86d4-282b8ccd27d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c10ea32-49ae-4dc5-8abf-25a5150f78b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "179ebc8c-1eca-4e71-86d4-282b8ccd27d7",
                    "LayerId": "5abd8213-2508-4129-9975-a97b62629623"
                }
            ]
        },
        {
            "id": "691656f5-4ce5-4e35-b781-aacb1c3b2966",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "085c2050-4789-4f48-8bd7-692093da2112",
            "compositeImage": {
                "id": "86de4d5a-7a04-4736-a2e0-6412edb822d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "691656f5-4ce5-4e35-b781-aacb1c3b2966",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6aa61d81-c9a5-4702-9fa9-3df8c1a86e24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "691656f5-4ce5-4e35-b781-aacb1c3b2966",
                    "LayerId": "5abd8213-2508-4129-9975-a97b62629623"
                }
            ]
        },
        {
            "id": "ce89e501-0228-4b3f-84af-e534c86ed477",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "085c2050-4789-4f48-8bd7-692093da2112",
            "compositeImage": {
                "id": "a8f1327f-5416-4f3f-9b62-c489bc04414f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce89e501-0228-4b3f-84af-e534c86ed477",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc294c13-c3cb-432a-a22d-7c09e579253c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce89e501-0228-4b3f-84af-e534c86ed477",
                    "LayerId": "5abd8213-2508-4129-9975-a97b62629623"
                }
            ]
        },
        {
            "id": "148c215c-2086-4d68-9ddd-7adcdb6f02ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "085c2050-4789-4f48-8bd7-692093da2112",
            "compositeImage": {
                "id": "1ec81d87-2bcd-4934-8de3-8efaf7572793",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "148c215c-2086-4d68-9ddd-7adcdb6f02ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69037d52-b034-4461-9102-355767a8487e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "148c215c-2086-4d68-9ddd-7adcdb6f02ec",
                    "LayerId": "5abd8213-2508-4129-9975-a97b62629623"
                }
            ]
        },
        {
            "id": "85598fb1-c10e-4d8f-873b-4258341e9cb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "085c2050-4789-4f48-8bd7-692093da2112",
            "compositeImage": {
                "id": "8e60970f-0f55-4192-bf44-3b9ddae2d305",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85598fb1-c10e-4d8f-873b-4258341e9cb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dbf4b34-fe50-4680-a87d-5f5ba17c4672",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85598fb1-c10e-4d8f-873b-4258341e9cb9",
                    "LayerId": "5abd8213-2508-4129-9975-a97b62629623"
                }
            ]
        },
        {
            "id": "625f41b3-cd39-4ec4-ad58-c88f19ced544",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "085c2050-4789-4f48-8bd7-692093da2112",
            "compositeImage": {
                "id": "8d8f656f-a45c-46b8-8579-ff37eaf587e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "625f41b3-cd39-4ec4-ad58-c88f19ced544",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5c9da19-08d9-4eac-a25b-643756aa9311",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "625f41b3-cd39-4ec4-ad58-c88f19ced544",
                    "LayerId": "5abd8213-2508-4129-9975-a97b62629623"
                }
            ]
        },
        {
            "id": "e1873064-ad16-4f18-b6ba-fe8ba33f2d35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "085c2050-4789-4f48-8bd7-692093da2112",
            "compositeImage": {
                "id": "ddf46156-48ee-4ebc-8975-e54d1a8a5d5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1873064-ad16-4f18-b6ba-fe8ba33f2d35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f72f39e2-9813-47c1-81ac-a128620cadb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1873064-ad16-4f18-b6ba-fe8ba33f2d35",
                    "LayerId": "5abd8213-2508-4129-9975-a97b62629623"
                }
            ]
        },
        {
            "id": "d6c529fb-7d45-4810-924e-9cce1c5f0502",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "085c2050-4789-4f48-8bd7-692093da2112",
            "compositeImage": {
                "id": "d95442bd-c16e-4bea-8968-372ba6b55806",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6c529fb-7d45-4810-924e-9cce1c5f0502",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff764232-4b56-4b3b-b932-d9b7684c99a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6c529fb-7d45-4810-924e-9cce1c5f0502",
                    "LayerId": "5abd8213-2508-4129-9975-a97b62629623"
                }
            ]
        },
        {
            "id": "9a86dd98-ae05-4a59-be6c-6b64e2636841",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "085c2050-4789-4f48-8bd7-692093da2112",
            "compositeImage": {
                "id": "948f4cd8-820f-4e56-a301-48145aa83262",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a86dd98-ae05-4a59-be6c-6b64e2636841",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c231789f-f20c-438a-aa46-d1d70c11cb87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a86dd98-ae05-4a59-be6c-6b64e2636841",
                    "LayerId": "5abd8213-2508-4129-9975-a97b62629623"
                }
            ]
        },
        {
            "id": "e4489098-b1f6-46fb-9a7d-862eb68b4058",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "085c2050-4789-4f48-8bd7-692093da2112",
            "compositeImage": {
                "id": "67786c92-1421-4859-851a-d1d2ef12ca51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4489098-b1f6-46fb-9a7d-862eb68b4058",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "272d5aef-6036-4d06-a035-c3282e6364ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4489098-b1f6-46fb-9a7d-862eb68b4058",
                    "LayerId": "5abd8213-2508-4129-9975-a97b62629623"
                }
            ]
        },
        {
            "id": "da4fdc64-7306-49bc-ab62-4474b10bd9a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "085c2050-4789-4f48-8bd7-692093da2112",
            "compositeImage": {
                "id": "b42b553a-2654-40dd-a3e7-0159b0db22b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da4fdc64-7306-49bc-ab62-4474b10bd9a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8a5c392-e148-4828-918e-bc8aef7ace5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da4fdc64-7306-49bc-ab62-4474b10bd9a8",
                    "LayerId": "5abd8213-2508-4129-9975-a97b62629623"
                }
            ]
        },
        {
            "id": "0ff928ab-fe2d-4578-a1da-349e805013c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "085c2050-4789-4f48-8bd7-692093da2112",
            "compositeImage": {
                "id": "f347be61-4d76-4767-bcc8-6e7d99c348ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ff928ab-fe2d-4578-a1da-349e805013c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71cecbd3-28bd-4208-8ddf-900ed04a0147",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ff928ab-fe2d-4578-a1da-349e805013c3",
                    "LayerId": "5abd8213-2508-4129-9975-a97b62629623"
                }
            ]
        },
        {
            "id": "fb32ca60-054a-4718-b5c9-7f9c7785c013",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "085c2050-4789-4f48-8bd7-692093da2112",
            "compositeImage": {
                "id": "4f9630af-0c0f-42b6-b635-33ec16a74e3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb32ca60-054a-4718-b5c9-7f9c7785c013",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98529fa9-fb4e-421a-a275-e8b2d86f774f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb32ca60-054a-4718-b5c9-7f9c7785c013",
                    "LayerId": "5abd8213-2508-4129-9975-a97b62629623"
                }
            ]
        },
        {
            "id": "6cc08e8c-3673-4859-8403-fe1b7ed4f71b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "085c2050-4789-4f48-8bd7-692093da2112",
            "compositeImage": {
                "id": "babde080-8791-4485-8b29-83764e427902",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cc08e8c-3673-4859-8403-fe1b7ed4f71b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db26ff93-084b-4471-9b04-621428f833ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cc08e8c-3673-4859-8403-fe1b7ed4f71b",
                    "LayerId": "5abd8213-2508-4129-9975-a97b62629623"
                }
            ]
        },
        {
            "id": "3bdb2903-14b4-4cce-9e6a-3c45a470916d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "085c2050-4789-4f48-8bd7-692093da2112",
            "compositeImage": {
                "id": "3d55c2dd-3a2a-4c9b-8fbf-d0c628bb6779",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bdb2903-14b4-4cce-9e6a-3c45a470916d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbf24b3d-beb2-404f-a91e-f0a39a4c742c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bdb2903-14b4-4cce-9e6a-3c45a470916d",
                    "LayerId": "5abd8213-2508-4129-9975-a97b62629623"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "5abd8213-2508-4129-9975-a97b62629623",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "085c2050-4789-4f48-8bd7-692093da2112",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 79,
    "yorig": 121
}