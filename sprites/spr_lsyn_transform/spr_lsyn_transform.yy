{
    "id": "477b4d5c-8ba1-4a84-b519-1a1667304004",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lsyn_transform",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 108,
    "bbox_left": 53,
    "bbox_right": 109,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3cc33f57-f4d6-4d91-ac5f-b92e057b94e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "8f5715c7-65f4-412b-89eb-d629aef19a80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cc33f57-f4d6-4d91-ac5f-b92e057b94e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b1271a6-8c47-4d51-bb34-32257ca164c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cc33f57-f4d6-4d91-ac5f-b92e057b94e0",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "c5467fed-37f3-4029-b94c-30ec2d83c06b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "253f9fc1-8cab-4890-8520-84d1b25f520e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5467fed-37f3-4029-b94c-30ec2d83c06b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac88fd31-08bd-487f-ae69-fd162c251448",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5467fed-37f3-4029-b94c-30ec2d83c06b",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "cf681fe0-e237-4ca9-a6c9-34a17526f7e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "d3783b41-858f-482a-bf4e-1747cf6e0ebe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf681fe0-e237-4ca9-a6c9-34a17526f7e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "259dc874-518f-4cb5-81dd-3e6b2d634e16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf681fe0-e237-4ca9-a6c9-34a17526f7e6",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "a0d236fd-fc79-4838-b8d6-9f1d4944881f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "d9d3581f-81f4-4279-8113-94a766b52b22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0d236fd-fc79-4838-b8d6-9f1d4944881f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56637614-1d47-4ebf-8137-78dd4238ff7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0d236fd-fc79-4838-b8d6-9f1d4944881f",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "24ee988c-228f-4b25-acb7-f41f07ad619d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "1a737e7c-02da-4263-afe7-26a30169e106",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24ee988c-228f-4b25-acb7-f41f07ad619d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b55a615b-66bb-4c33-b219-8cabacffd5fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24ee988c-228f-4b25-acb7-f41f07ad619d",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "e20e1418-213d-487a-9934-f24c06dce032",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "7c9a5c37-5072-44ce-af99-8fb15a876329",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e20e1418-213d-487a-9934-f24c06dce032",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b49f8427-b3bd-4efb-8906-2996e91dd7d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e20e1418-213d-487a-9934-f24c06dce032",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "8f0d9771-aa85-43b4-8df1-5155f243f19b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "fa497d7b-09c7-4270-a1b1-179cc7a20726",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f0d9771-aa85-43b4-8df1-5155f243f19b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4fc6829-e2f3-4d8c-b9e0-a97f309879b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f0d9771-aa85-43b4-8df1-5155f243f19b",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "a34a532e-e925-4992-a0d9-fbc8b8c00922",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "337276bb-71d2-443d-8298-46c6d377c7a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a34a532e-e925-4992-a0d9-fbc8b8c00922",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42d8fa39-b094-47bd-b01f-e6b47953323e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a34a532e-e925-4992-a0d9-fbc8b8c00922",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "3bee25bc-f5c9-4e68-a873-76524de9fa09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "e0d29ffd-fb53-45f7-a80d-7c3745bde2b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bee25bc-f5c9-4e68-a873-76524de9fa09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6482f513-e769-466f-a45e-26cc82a8a3b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bee25bc-f5c9-4e68-a873-76524de9fa09",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "b70cc9df-f20c-4bf6-93f7-30bb10c16d19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "bdf4f8b6-4e6c-49bf-85f1-8a47dcb833d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b70cc9df-f20c-4bf6-93f7-30bb10c16d19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc1bbd2b-03ca-4926-af73-27a48083c5d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b70cc9df-f20c-4bf6-93f7-30bb10c16d19",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "515c0b79-a649-4d06-85b5-33117e77dcb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "06d4e1a1-ea79-4155-8d6c-940e1db0315b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "515c0b79-a649-4d06-85b5-33117e77dcb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f8a40f2-7cb2-4eb9-beb1-86215b2f455b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "515c0b79-a649-4d06-85b5-33117e77dcb6",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "ed3cf52f-399a-4938-a787-c91e49ee178e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "05f05b18-e8fc-4f22-946b-70ffa17ff71e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed3cf52f-399a-4938-a787-c91e49ee178e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ff94dcb-8105-4922-964b-faaf088a2eb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed3cf52f-399a-4938-a787-c91e49ee178e",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "d2aef6fc-b307-4df7-be9d-091b86b940a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "dc36565a-be45-47f5-8712-25b01250fc74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2aef6fc-b307-4df7-be9d-091b86b940a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a50558dd-1a4c-418d-bae9-b0aa87dda336",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2aef6fc-b307-4df7-be9d-091b86b940a1",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "f46628e2-8183-4bda-b269-a74b2c1a81e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "4f33ae39-6a2a-45c3-bb37-5182e92b73bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f46628e2-8183-4bda-b269-a74b2c1a81e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a40a209e-fdef-41f9-a679-86786d4581e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f46628e2-8183-4bda-b269-a74b2c1a81e5",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "c9af8016-4b1a-49e1-9f18-d6fd22ecd2fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "a11ca202-9e55-4c21-a64c-7c3864e80908",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9af8016-4b1a-49e1-9f18-d6fd22ecd2fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96a8d48c-be9b-4d75-97e4-c98c85fa2adc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9af8016-4b1a-49e1-9f18-d6fd22ecd2fa",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "0b302bb8-ff55-4cdb-8d39-c5010a874df8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "a5703c0a-a158-4ca5-b19d-9f53267894b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b302bb8-ff55-4cdb-8d39-c5010a874df8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cf19d4e-d1ac-472a-983b-58074c0047ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b302bb8-ff55-4cdb-8d39-c5010a874df8",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "a0aff2a9-319a-4ac3-90c0-eb42157e414f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "1b7960fc-019f-4078-89e5-04c795d08316",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0aff2a9-319a-4ac3-90c0-eb42157e414f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "160b5b3d-49ee-474d-adf5-d413416e42b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0aff2a9-319a-4ac3-90c0-eb42157e414f",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "6a564f50-62a4-4373-86b1-6932fbd7a3fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "8ec8f292-822a-4090-b974-379493b236dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a564f50-62a4-4373-86b1-6932fbd7a3fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6252a5c6-0d94-4448-80e7-e4724ba29593",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a564f50-62a4-4373-86b1-6932fbd7a3fd",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "307510d2-ac64-4573-8867-daec2ca7ebc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "fb45663a-7f0e-4f5b-aebd-171fe678d577",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "307510d2-ac64-4573-8867-daec2ca7ebc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "883f9404-4d48-4c0d-9345-003551e92be5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "307510d2-ac64-4573-8867-daec2ca7ebc3",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "3c908001-c39a-4313-bc61-67b57d9f0a02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "c5e52d54-637b-4026-8c29-ef62889d92e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c908001-c39a-4313-bc61-67b57d9f0a02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad09ef1b-c211-439f-b407-6b09455b3a12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c908001-c39a-4313-bc61-67b57d9f0a02",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "f92ac00e-cca7-4deb-9e61-ec83dfddf8ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "9e4435fa-f059-4abd-85af-89ae0cacc019",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f92ac00e-cca7-4deb-9e61-ec83dfddf8ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "650923ab-dd26-4ac4-9212-735e786ac1fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f92ac00e-cca7-4deb-9e61-ec83dfddf8ca",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "c7596270-5f60-4e8e-899e-ac29f36437f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "ebb6b747-d89e-4eb8-8de0-6ae3634f543a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7596270-5f60-4e8e-899e-ac29f36437f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "396550cd-dfce-4201-a720-d30cbe0d1b1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7596270-5f60-4e8e-899e-ac29f36437f4",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "d2231aa3-908b-4b94-9d20-e905ce7bc60c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "70955f96-22cc-4f1c-948a-e6179104d3f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2231aa3-908b-4b94-9d20-e905ce7bc60c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65af368d-88fa-4409-8046-ac3bff997f1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2231aa3-908b-4b94-9d20-e905ce7bc60c",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "0cf19e84-cdc8-4ba8-8b58-8e4819afa812",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "d9b4ad3f-5282-40b7-b3f5-b3042f673018",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cf19e84-cdc8-4ba8-8b58-8e4819afa812",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7f981da-c4ce-4861-a5b2-6a56bc9fcc0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cf19e84-cdc8-4ba8-8b58-8e4819afa812",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "d9ff2193-2571-42c4-a95a-0e6eb6f4eaaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "1d541edc-7a5d-4ab5-9dc5-64bfac3eb711",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9ff2193-2571-42c4-a95a-0e6eb6f4eaaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c78607c6-2390-4f1d-ba10-7cab1adea9e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9ff2193-2571-42c4-a95a-0e6eb6f4eaaa",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "5e410459-7e76-4003-a2a3-48b44523f97f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "f596bd12-fcde-4bce-bf71-3aa20a64e913",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e410459-7e76-4003-a2a3-48b44523f97f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dea8188a-8aff-43ee-aac4-cf30e898ac94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e410459-7e76-4003-a2a3-48b44523f97f",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "64b26de7-1e99-4212-b475-6869b70550a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "5c1881fc-8b4a-409b-9ee0-8f71dcd0c8a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64b26de7-1e99-4212-b475-6869b70550a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5e47f3f-83d5-4481-b621-b128a9585700",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64b26de7-1e99-4212-b475-6869b70550a4",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "bdc93118-0745-40a4-adaa-0e852e8d5555",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "96215b34-a75f-428c-bed2-b7fcc3f6d4ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdc93118-0745-40a4-adaa-0e852e8d5555",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86f31992-db2c-4193-991e-ce5a4c772816",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdc93118-0745-40a4-adaa-0e852e8d5555",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "c305084f-dfb9-4e78-a742-0b9836d34649",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "1ea35de2-a0bb-48bd-8603-d8b0ee90fa66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c305084f-dfb9-4e78-a742-0b9836d34649",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dcf8fb7-29cd-4e09-adf3-7d88c2990e83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c305084f-dfb9-4e78-a742-0b9836d34649",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "5c367343-14af-4b31-9d79-069a5df9accc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "c025f3e1-c145-450a-bef4-bc667594a5eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c367343-14af-4b31-9d79-069a5df9accc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95133918-bb1d-40ba-9c60-53031d323cfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c367343-14af-4b31-9d79-069a5df9accc",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "3b6d10b2-9823-43fe-9ee0-fe7ccfdb2b90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "a4feaecb-1a53-430b-9e67-697970907dff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b6d10b2-9823-43fe-9ee0-fe7ccfdb2b90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbd088da-36a7-4934-91cb-1074b549d2d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b6d10b2-9823-43fe-9ee0-fe7ccfdb2b90",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "2e22f14d-8c84-44bf-8727-7a008e991cc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "5c17cc6b-ae9f-4be8-add7-d08f0de0856a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e22f14d-8c84-44bf-8727-7a008e991cc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c95b4e81-f13a-4e50-9af4-6b8a310e059a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e22f14d-8c84-44bf-8727-7a008e991cc0",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "e23f6693-08f1-4520-9c9b-60a793575aa4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "daa0a3d5-4516-495c-829a-190f884ca8f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e23f6693-08f1-4520-9c9b-60a793575aa4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "551af085-82c3-45b0-a47c-f2275b734d5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e23f6693-08f1-4520-9c9b-60a793575aa4",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "7ade730c-a8c5-4297-9e2e-f992f50f873a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "335e3f7e-f287-46a7-bfbd-2d591604f709",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ade730c-a8c5-4297-9e2e-f992f50f873a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "057a87a0-396d-422d-a6e1-23c7e6106671",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ade730c-a8c5-4297-9e2e-f992f50f873a",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        },
        {
            "id": "5c3ea3d7-a4b3-4e15-af09-d882e2a713e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "compositeImage": {
                "id": "0a34e4ee-ed49-4252-9b82-94a91a77c91b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c3ea3d7-a4b3-4e15-af09-d882e2a713e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3b15409-a71e-4563-80b1-16b8857298a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c3ea3d7-a4b3-4e15-af09-d882e2a713e0",
                    "LayerId": "5f44eb84-bb5a-41d6-b410-23c11176c35e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "5f44eb84-bb5a-41d6-b410-23c11176c35e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "477b4d5c-8ba1-4a84-b519-1a1667304004",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 168,
    "xorig": 85,
    "yorig": 80
}