{
    "id": "d2482485-3701-4c82-a85c-00ad465861d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_controller1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 5,
    "bbox_right": 62,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4cde95c5-f0b2-4345-ba6e-529e2c9d36e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2482485-3701-4c82-a85c-00ad465861d4",
            "compositeImage": {
                "id": "2b4e287f-bec5-4ad0-a1da-20095218262b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cde95c5-f0b2-4345-ba6e-529e2c9d36e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "889b821a-ea3b-4932-b563-736634ff0193",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cde95c5-f0b2-4345-ba6e-529e2c9d36e7",
                    "LayerId": "8eb41d06-611c-4e64-9957-d9e83f618fd8"
                }
            ]
        },
        {
            "id": "ae775f6c-7756-4d14-bb0b-10b6f5e900f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2482485-3701-4c82-a85c-00ad465861d4",
            "compositeImage": {
                "id": "06b34b33-6af7-4c3d-b0e6-49f73cd31daf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae775f6c-7756-4d14-bb0b-10b6f5e900f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebeb02d8-fc05-4ec1-be3c-02830c81a9f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae775f6c-7756-4d14-bb0b-10b6f5e900f6",
                    "LayerId": "8eb41d06-611c-4e64-9957-d9e83f618fd8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8eb41d06-611c-4e64-9957-d9e83f618fd8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d2482485-3701-4c82-a85c-00ad465861d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}