{
    "id": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_chdh_fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 125,
    "bbox_left": 24,
    "bbox_right": 153,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d02b0070-23b8-433c-b345-592f8ef28dd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "d7efb6a2-6fcd-4a4e-a6a9-fcb047df741e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d02b0070-23b8-433c-b345-592f8ef28dd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00a21b98-7887-47d1-9af0-8cf6915cae9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d02b0070-23b8-433c-b345-592f8ef28dd5",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "9d11390e-31ef-4bee-be29-92caa00349fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "9c09e4d9-8d4f-4019-bf6f-d1cf77da8469",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d11390e-31ef-4bee-be29-92caa00349fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42465fc1-1cf1-45bf-8efe-c11dcc10f4b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d11390e-31ef-4bee-be29-92caa00349fd",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "01c24c4a-3741-4888-ada0-7d63bbdc268d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "ea3bee0a-2ba7-410c-aa5b-e75940371d9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01c24c4a-3741-4888-ada0-7d63bbdc268d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "837d3d99-d58f-4bb6-a214-a323f8ede110",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01c24c4a-3741-4888-ada0-7d63bbdc268d",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "6a0cfea2-2675-4e29-b4ee-dad6366a23d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "06129fbe-5d94-495a-acdd-fe182dea736d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a0cfea2-2675-4e29-b4ee-dad6366a23d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d05de8e8-0f98-4969-bdde-2a21f822a541",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a0cfea2-2675-4e29-b4ee-dad6366a23d8",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "a82ef6e1-602b-4ad4-a355-28d147a241e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "ffc64ea3-7e9a-4411-ac91-977c7c1fb345",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a82ef6e1-602b-4ad4-a355-28d147a241e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a06f1558-7690-4d1a-ae9f-94dda3d82f08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a82ef6e1-602b-4ad4-a355-28d147a241e7",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "17c46f1a-1d03-4e2f-8897-4b2b4ac52df9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "a5f0617a-eb4d-4ae5-9f2a-def84bb60120",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17c46f1a-1d03-4e2f-8897-4b2b4ac52df9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7956656e-ac25-49e7-ad69-0aa08d955d53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17c46f1a-1d03-4e2f-8897-4b2b4ac52df9",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "a2441deb-8b18-4955-a63f-d80ccc1f1aec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "ee044cfb-bb6e-4a1e-ac8e-bbddb3b90edb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2441deb-8b18-4955-a63f-d80ccc1f1aec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "096322c8-d3b5-4bd5-9a0e-62aa5f360d9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2441deb-8b18-4955-a63f-d80ccc1f1aec",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "c93162e5-20f7-464f-a5d3-c05bd9231dac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "2bb52960-b3b4-44df-96f6-6a7958eab70c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c93162e5-20f7-464f-a5d3-c05bd9231dac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1a0261b-6d1e-47cd-ab30-711495eecac5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c93162e5-20f7-464f-a5d3-c05bd9231dac",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "6c711e4a-a1e4-4ded-89e5-da5f34d9a212",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "a3e1dc22-cadc-4bf6-ae35-262ad515dff8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c711e4a-a1e4-4ded-89e5-da5f34d9a212",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51f5c059-7f12-446b-b017-d65cac6f0fa3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c711e4a-a1e4-4ded-89e5-da5f34d9a212",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "94fcc8da-6234-4146-b591-22fc3bf5f0b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "0fcc86b6-3382-4b36-9412-195cd96feb45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94fcc8da-6234-4146-b591-22fc3bf5f0b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dc26c5e-49be-4a10-a47d-50e0837272da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94fcc8da-6234-4146-b591-22fc3bf5f0b2",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "0e1cfe0f-b70b-46d8-850c-9c74ba8bda03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "0a551fcb-54a6-458c-8ea9-1715b3afe153",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e1cfe0f-b70b-46d8-850c-9c74ba8bda03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dad94f3d-5742-4103-bb83-f6867d993126",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e1cfe0f-b70b-46d8-850c-9c74ba8bda03",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "5b91ed16-ab62-4343-b453-faa32502465d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "4309ab40-8650-4355-b6aa-f97a516df21f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b91ed16-ab62-4343-b453-faa32502465d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0265e56b-64e3-48c7-8cb4-e78475c31825",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b91ed16-ab62-4343-b453-faa32502465d",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "8202f006-f82f-4125-b882-ea5dda1e6dad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "7db3a9e0-3217-487a-8eef-6e2f4dc3d787",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8202f006-f82f-4125-b882-ea5dda1e6dad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71a7c9b7-0160-4bff-be5c-517ae7952336",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8202f006-f82f-4125-b882-ea5dda1e6dad",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "38e9f396-5614-4210-bc71-6ca5ce815879",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "08482f0e-fd02-4ace-b046-582fffa95fea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38e9f396-5614-4210-bc71-6ca5ce815879",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d2ea167-d36a-480b-bd5e-8ad3da13b015",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38e9f396-5614-4210-bc71-6ca5ce815879",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "22a7c22c-9202-405d-9dca-1ffeb399188e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "a734722e-b4c8-403f-9840-839eaf8d9b09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22a7c22c-9202-405d-9dca-1ffeb399188e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64a732c9-b24c-4298-9a82-b77bab9da375",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22a7c22c-9202-405d-9dca-1ffeb399188e",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "6c6d366f-c629-4692-b89a-873eaa8b8dd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "ff09d218-8c37-4722-9624-6f363c61ad69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c6d366f-c629-4692-b89a-873eaa8b8dd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cace55d6-7fc8-4b9b-8c80-90548adaf2f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c6d366f-c629-4692-b89a-873eaa8b8dd0",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "91cafe46-8106-4c42-8a5e-bb94bff6a49e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "9e978aa3-4332-471d-9b79-a159bec004e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91cafe46-8106-4c42-8a5e-bb94bff6a49e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e136547e-8d88-4fdd-806d-04ca6a626e72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91cafe46-8106-4c42-8a5e-bb94bff6a49e",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "a1659edd-36d2-488f-9118-160bbad6357d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "3261eab7-0bc8-4869-9c97-4ab33d3e62f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1659edd-36d2-488f-9118-160bbad6357d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5a6923c-f36a-446f-9d85-acecab1ebbc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1659edd-36d2-488f-9118-160bbad6357d",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "29e5baeb-6f6a-4c82-8b46-f408332c0510",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "9701f5ce-8107-4b25-a27b-47214055a7ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29e5baeb-6f6a-4c82-8b46-f408332c0510",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84eeaeca-70c7-4d16-a10b-36f65c3e60f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29e5baeb-6f6a-4c82-8b46-f408332c0510",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "e3c2a04b-9c84-4bee-97ec-e23f3a3c93f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "f2f463f0-6141-4640-b868-d2930ecbd492",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3c2a04b-9c84-4bee-97ec-e23f3a3c93f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4aac2cc5-f3a4-43a1-b088-13d2689460a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3c2a04b-9c84-4bee-97ec-e23f3a3c93f8",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "a429525c-e1ff-4771-8eb2-50859a30b60f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "bfe883da-26c0-45be-8657-632f00e51c9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a429525c-e1ff-4771-8eb2-50859a30b60f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6697fc2-2855-4620-bdf2-9d07c22201b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a429525c-e1ff-4771-8eb2-50859a30b60f",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "be4516d3-8866-463a-84e8-948a9525858f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "eec8a8b8-8402-4474-8e21-d286a876d1dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be4516d3-8866-463a-84e8-948a9525858f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15c01652-ef3c-4491-8847-de48d8ae847d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be4516d3-8866-463a-84e8-948a9525858f",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "1c9986c7-c71d-4832-aae2-b612d6eef928",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "f577c2cb-447f-4264-857c-c652ebd2197a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c9986c7-c71d-4832-aae2-b612d6eef928",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e819ef2b-ede8-4d5b-88bf-27289cc50407",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c9986c7-c71d-4832-aae2-b612d6eef928",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "3099b063-e772-41b6-b469-186959eefd8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "dbbfb2c6-1c56-4739-b016-8c0d22af8b44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3099b063-e772-41b6-b469-186959eefd8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29b627af-fcf7-4c63-8b4f-3e6027aac1c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3099b063-e772-41b6-b469-186959eefd8e",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "1390da39-c5e5-477c-b796-9898c0df31e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "05382f84-a2b7-429e-8fb7-88413b2e5e17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1390da39-c5e5-477c-b796-9898c0df31e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a025684-9146-492b-95c9-5bc4899348db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1390da39-c5e5-477c-b796-9898c0df31e0",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "9fd50a95-3198-440f-bf00-aa798215c0e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "34e205bd-c985-4f9a-b77c-a5b51a8498e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fd50a95-3198-440f-bf00-aa798215c0e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "527b9c16-1f99-4153-9859-4555fc4e8e75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fd50a95-3198-440f-bf00-aa798215c0e2",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "37fe5e5e-dbae-4f41-9dd5-a215384df527",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "f38c087b-b1e7-4a3b-85de-9d621b5d88d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37fe5e5e-dbae-4f41-9dd5-a215384df527",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09072e11-069b-4103-b9b9-230cd547a059",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37fe5e5e-dbae-4f41-9dd5-a215384df527",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "7865b7cc-e29b-4d13-a02d-d92ed6c06248",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "12ac5501-aa4c-4f3e-b06f-53caef5ad93b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7865b7cc-e29b-4d13-a02d-d92ed6c06248",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "371493a9-3b66-4088-bbcc-4549b25997a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7865b7cc-e29b-4d13-a02d-d92ed6c06248",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "cd1b9fd0-cba3-40ce-9704-6b7d4523e51e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "e9e25310-bff7-40f7-a01b-6598c4df19b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd1b9fd0-cba3-40ce-9704-6b7d4523e51e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b8ce36a-afb0-41db-afc2-a0f9ce04f13c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd1b9fd0-cba3-40ce-9704-6b7d4523e51e",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "48eaa828-0b75-4271-a176-327328c98da1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "7b6d8df3-e1f3-40c6-ba3a-bbeeee7cdd1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48eaa828-0b75-4271-a176-327328c98da1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cca3d865-834d-40e7-a258-de8c2ecef3da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48eaa828-0b75-4271-a176-327328c98da1",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "59944e2a-4afe-4113-83c3-0dfe9a45217b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "7b40d431-a4f2-43fd-9b5b-5b6ce68e67e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59944e2a-4afe-4113-83c3-0dfe9a45217b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "078a6e2e-3a28-4e35-b650-1cb704ccdbf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59944e2a-4afe-4113-83c3-0dfe9a45217b",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "6b53dbaf-8113-4979-9d72-98f79fe64bc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "8d035ff4-b6e0-4686-8773-648b0ed26d84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b53dbaf-8113-4979-9d72-98f79fe64bc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f41d571-b6bc-428d-8026-e8345ad87615",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b53dbaf-8113-4979-9d72-98f79fe64bc0",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "05f4cbd1-3b10-4c05-b102-46ec2ba4a09c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "a196cc29-dec4-42a3-a52b-59f3b4c6d557",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05f4cbd1-3b10-4c05-b102-46ec2ba4a09c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92641646-5180-4c8f-a617-5f7f81c7bd0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05f4cbd1-3b10-4c05-b102-46ec2ba4a09c",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "021b87fd-79c3-4b0f-ac16-455b882879d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "52b1e85c-fa75-4f2d-93e9-eba1d83aedd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "021b87fd-79c3-4b0f-ac16-455b882879d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8b59916-edb5-42c0-a69a-bee2e5fecd60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "021b87fd-79c3-4b0f-ac16-455b882879d6",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "b20f51a3-c6ee-4e8d-80c5-d0654d8e5374",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "8631910e-4bd1-46fd-a29e-d5fa90ee8f88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b20f51a3-c6ee-4e8d-80c5-d0654d8e5374",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62975d78-4682-4aaf-9a99-2494820eeb27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b20f51a3-c6ee-4e8d-80c5-d0654d8e5374",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "50f2b91d-cc6b-4fdc-a8b1-9cdad019cb2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "cbad8a54-82a1-4f76-a078-78e2c5dabfd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50f2b91d-cc6b-4fdc-a8b1-9cdad019cb2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37e21275-514e-4924-b726-16e95d781ff7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50f2b91d-cc6b-4fdc-a8b1-9cdad019cb2e",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "21171055-62d4-4fb2-bbd7-bebef2b285db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "92480938-fb36-4565-8abc-5d08c88e4263",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21171055-62d4-4fb2-bbd7-bebef2b285db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3aab9828-ec17-49f0-bc5a-c4a5717f1e3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21171055-62d4-4fb2-bbd7-bebef2b285db",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "a76f65b8-5e32-4a71-92ba-e7b1cad3b295",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "b4223be1-61cd-4751-90f9-6190a302338e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a76f65b8-5e32-4a71-92ba-e7b1cad3b295",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3bddabb-2eab-4e1a-aa37-6f122d24af69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a76f65b8-5e32-4a71-92ba-e7b1cad3b295",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "9b887ada-3019-4d4c-89f1-d5fcc892e9de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "8a1cc801-c49d-40d4-9f4e-3cc8af9a55ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b887ada-3019-4d4c-89f1-d5fcc892e9de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3523e37a-13c7-4868-a2bb-3b3031ee0f6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b887ada-3019-4d4c-89f1-d5fcc892e9de",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "6485e51b-0c27-4aa1-a444-630b28d00bb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "672503a6-fc02-489a-a035-69381308a0d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6485e51b-0c27-4aa1-a444-630b28d00bb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d4c9f39-ddb5-47d8-8430-e6f91a0a9bad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6485e51b-0c27-4aa1-a444-630b28d00bb2",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "865f4a36-5da9-4299-bfb2-22270c320a51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "4c55fe93-f0ea-4fe7-a94a-521b01cd1904",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "865f4a36-5da9-4299-bfb2-22270c320a51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18272364-0055-40ae-b5a4-83f57fa2e7a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "865f4a36-5da9-4299-bfb2-22270c320a51",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "c3cda4c0-fe2e-414a-b43d-0d7e0e7fb04a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "11f39f8e-fbf7-4713-88f2-4fcc4d7a49b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3cda4c0-fe2e-414a-b43d-0d7e0e7fb04a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b527a98-9c78-45dc-a7b7-c22f23f97225",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3cda4c0-fe2e-414a-b43d-0d7e0e7fb04a",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "19d0f044-f223-4b87-b19b-f38e672edb5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "a1844deb-caec-48d2-86a4-60943d71937f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19d0f044-f223-4b87-b19b-f38e672edb5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94e96245-03b0-45b5-a1f8-1a303ff9f878",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19d0f044-f223-4b87-b19b-f38e672edb5a",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "fb25b7da-90a6-49d4-ac39-9f3ecf9c5feb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "f8e1e22c-fe0e-4d18-a53d-984f06e67c52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb25b7da-90a6-49d4-ac39-9f3ecf9c5feb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82935e9c-1c5f-4412-aec1-1435c3a7e5fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb25b7da-90a6-49d4-ac39-9f3ecf9c5feb",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "739f6bac-a037-470c-b948-0d2be8659a1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "ad480756-f720-47ba-a774-cf2afe609165",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "739f6bac-a037-470c-b948-0d2be8659a1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb70074c-f6dc-44ae-b0d7-107c6e936b12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "739f6bac-a037-470c-b948-0d2be8659a1c",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "8cdc030a-c0bf-415c-bcc8-39743529fbb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "24110e82-6e0d-427a-86c7-31a8acfd8c5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cdc030a-c0bf-415c-bcc8-39743529fbb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2b0b2fa-cf9a-485b-b115-8cacec40b617",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cdc030a-c0bf-415c-bcc8-39743529fbb1",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "133d1a91-4584-487d-bbd3-703705e451d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "dfccc8cb-01c5-4d68-8720-5f19a6a644ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "133d1a91-4584-487d-bbd3-703705e451d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c83fbbe-1e19-4352-bdf9-731c58d25b56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "133d1a91-4584-487d-bbd3-703705e451d6",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "16f6272c-1bf8-492d-a597-ecdf91798bed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "bf07bdb5-a1b4-4e42-8295-21156fc56e5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16f6272c-1bf8-492d-a597-ecdf91798bed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93a7d074-d147-4c40-b92e-4bf3d1c818ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16f6272c-1bf8-492d-a597-ecdf91798bed",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "bd0fea91-451c-4604-8187-e4f66fb8f72e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "e13bc56e-77db-4127-a147-b9fff150cea7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd0fea91-451c-4604-8187-e4f66fb8f72e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98d340a6-8d4e-47df-a37e-1c5ce2a4d9c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd0fea91-451c-4604-8187-e4f66fb8f72e",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "522c3b2c-a98e-4224-ad97-bb76c5bd73ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "82efc487-2265-4dfd-b4c4-61af02130e56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "522c3b2c-a98e-4224-ad97-bb76c5bd73ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eba89ce7-56b1-4a2a-a2b4-abb79bf2a2ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "522c3b2c-a98e-4224-ad97-bb76c5bd73ee",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "83ce08c4-2c87-4e28-a861-c7cfbd1bdc88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "20d31d90-6005-4e6b-9793-0f5ad79802dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83ce08c4-2c87-4e28-a861-c7cfbd1bdc88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "853690ee-0b86-40df-8ccf-d944c8ac60d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83ce08c4-2c87-4e28-a861-c7cfbd1bdc88",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "dbddbe71-3865-4eea-a2f9-581bca79ddbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "fc44f077-7a68-49b4-a59d-d865677286fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbddbe71-3865-4eea-a2f9-581bca79ddbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f522f0c3-31b1-4816-a55d-fda54befa45f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbddbe71-3865-4eea-a2f9-581bca79ddbb",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "42be604f-2b85-436f-a57d-9ba6d4dc2f7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "117e1a8e-a9c8-4bed-85dd-0ac7bbb3791f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42be604f-2b85-436f-a57d-9ba6d4dc2f7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af374b3a-0284-47f3-918f-f3083f838181",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42be604f-2b85-436f-a57d-9ba6d4dc2f7e",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "9bcb8e84-b302-46a8-b7db-1be033d4a9bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "2ca99ace-7654-4beb-807a-59c11b53b40f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bcb8e84-b302-46a8-b7db-1be033d4a9bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5598691d-f8eb-4731-b25b-d7566b660d3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bcb8e84-b302-46a8-b7db-1be033d4a9bf",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "41c3d8fc-efe7-4df7-b81a-cb5600f32ce4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "2608562b-ccc9-43c7-9a88-f99a7d19cbd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41c3d8fc-efe7-4df7-b81a-cb5600f32ce4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2548984-74f5-4ed9-af7f-aa0439ac05a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41c3d8fc-efe7-4df7-b81a-cb5600f32ce4",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "1471fd90-926c-41cf-b200-bb5112c2db25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "3302a9df-5505-46cc-b49e-6b5c39843e9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1471fd90-926c-41cf-b200-bb5112c2db25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4f465a0-f0bb-43e0-a378-3dbaa851856a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1471fd90-926c-41cf-b200-bb5112c2db25",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "8e2c6e58-3cd6-4815-9335-3fbbbaccb69a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "e3e58d0f-a245-4b78-856d-2b3fcd9899f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e2c6e58-3cd6-4815-9335-3fbbbaccb69a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "731811f1-ac03-41a0-a3ab-a31cc7959767",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e2c6e58-3cd6-4815-9335-3fbbbaccb69a",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "f1cd44b0-16f6-4905-85f9-2c646def3282",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "3b2938db-5c9c-4009-912a-400bc2c0028f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1cd44b0-16f6-4905-85f9-2c646def3282",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92a0c371-6916-4263-bab6-bbba734202dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1cd44b0-16f6-4905-85f9-2c646def3282",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        },
        {
            "id": "94877585-9343-436e-9b35-96856e94a485",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "compositeImage": {
                "id": "8e1f7ec1-1858-4225-bc8a-af22e92b0378",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94877585-9343-436e-9b35-96856e94a485",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c94d2ead-5f03-4324-ac11-abb107011a3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94877585-9343-436e-9b35-96856e94a485",
                    "LayerId": "e6bf639c-65cf-48c4-9fd2-01614ce31f95"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 148,
    "layers": [
        {
            "id": "e6bf639c-65cf-48c4-9fd2-01614ce31f95",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8a027e01-50ea-4f16-99a7-9735e51e7ac9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 168,
    "xorig": 63,
    "yorig": 87
}