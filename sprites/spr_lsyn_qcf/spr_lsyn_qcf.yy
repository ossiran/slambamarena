{
    "id": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lsyn_qcf",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 46,
    "bbox_right": 93,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d2a0bc43-7c91-4fcc-81a0-36304085a0c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "3da48bb0-c6e7-4ff0-a3dd-266f1152a0a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2a0bc43-7c91-4fcc-81a0-36304085a0c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5f00be4-fe00-433a-b058-3c9e942448ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2a0bc43-7c91-4fcc-81a0-36304085a0c8",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "dfe6aefa-fb3b-45eb-9c77-ab615e26aff9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "cbdc7835-555c-44e5-bf46-326ed09a6c35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfe6aefa-fb3b-45eb-9c77-ab615e26aff9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a959ef45-beae-4910-8e66-ccf2aa9e68d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfe6aefa-fb3b-45eb-9c77-ab615e26aff9",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "52fd0d86-e323-420c-927f-d921b1f38e3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "b9dea9a9-c337-4da9-98f4-8a923e52b273",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52fd0d86-e323-420c-927f-d921b1f38e3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f0fcdb0-0173-41a1-96df-359c75d28ca6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52fd0d86-e323-420c-927f-d921b1f38e3c",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "0bb39968-ae6f-46f6-89f9-010179d1cce0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "173f5178-e154-4dea-b0b5-ed6b30858376",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bb39968-ae6f-46f6-89f9-010179d1cce0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18365845-d751-49ea-97f8-c3e173ca75f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bb39968-ae6f-46f6-89f9-010179d1cce0",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "ec94229f-3b4b-4afb-a9d5-a3da2978de13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "37e0b0df-25cb-4608-87ce-573459186956",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec94229f-3b4b-4afb-a9d5-a3da2978de13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8ec1191-3cd7-446b-8a92-9fedc6000639",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec94229f-3b4b-4afb-a9d5-a3da2978de13",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "d61f1a31-ea10-49e4-9c19-03e1cf7b6abd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "54ec0029-c404-497f-be32-552e8e83869f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d61f1a31-ea10-49e4-9c19-03e1cf7b6abd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5496a442-1c4c-4782-9d2a-8b07c6746435",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d61f1a31-ea10-49e4-9c19-03e1cf7b6abd",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "4de57bd8-f272-487e-bb93-4040f782d888",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "867f5677-7d18-46ab-b71d-8fe999168124",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4de57bd8-f272-487e-bb93-4040f782d888",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "816aaa2f-7fbc-4327-9742-ea4aab744150",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4de57bd8-f272-487e-bb93-4040f782d888",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "3a27d082-67f6-4e88-b5e9-7099e6bb1e5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "9e52a408-af83-45ab-91a3-625a415e905f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a27d082-67f6-4e88-b5e9-7099e6bb1e5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ccf5a4a-91a4-4d5b-9cd1-cdc966afe5b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a27d082-67f6-4e88-b5e9-7099e6bb1e5c",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "764ef230-d035-40f3-a521-e4f78bfa3433",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "11046e9b-f7f8-4c18-8f09-bc9049507205",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "764ef230-d035-40f3-a521-e4f78bfa3433",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c13e274e-c3d3-4fe9-81f7-1a8220422f14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "764ef230-d035-40f3-a521-e4f78bfa3433",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "e4de63b7-f0f3-42dd-9a62-536e04622cb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "02a70960-b1ca-4863-b293-5a6c07659459",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4de63b7-f0f3-42dd-9a62-536e04622cb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b98d339-c28e-426b-9fc0-0d330ce90ada",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4de63b7-f0f3-42dd-9a62-536e04622cb1",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "7237d8fe-a452-4933-97ec-c3f3489e2974",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "100dbde4-1c36-4ded-b903-a1e9e9659b8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7237d8fe-a452-4933-97ec-c3f3489e2974",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc43e4f2-fece-4ef0-9372-4eb1088e326e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7237d8fe-a452-4933-97ec-c3f3489e2974",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "e26d3ef1-d0b5-414a-85c2-675e6ee2344d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "4c46bd37-6d44-4c5e-894c-f99fe1316dc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e26d3ef1-d0b5-414a-85c2-675e6ee2344d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f480075b-8226-4811-9094-6848b66df350",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e26d3ef1-d0b5-414a-85c2-675e6ee2344d",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "8e9989a7-d13e-45b8-9717-715a26e026da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "b4c74d65-d9eb-4ce2-a92d-89ca0fadaa0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e9989a7-d13e-45b8-9717-715a26e026da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5b86d5b-deb8-4335-b27d-b3d0718b73e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e9989a7-d13e-45b8-9717-715a26e026da",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "8f7eb847-8032-4f65-8045-7c49ca541150",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "a94c63b5-37ef-4b04-b08c-707a5fac57f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f7eb847-8032-4f65-8045-7c49ca541150",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3a2032f-675f-4222-9931-d4dcc827875b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f7eb847-8032-4f65-8045-7c49ca541150",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "6306d624-b596-4cf1-b154-a9f1f7e2f170",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "24129e93-11c4-4f95-ba1f-1192ae7dfdf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6306d624-b596-4cf1-b154-a9f1f7e2f170",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dacd4cf2-60d2-423e-a931-a2bc378deabd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6306d624-b596-4cf1-b154-a9f1f7e2f170",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "ec3b50ba-5cf0-4559-a711-fd5200545e3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "be0d8191-731d-46f8-bd43-e9c9c82b3cf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec3b50ba-5cf0-4559-a711-fd5200545e3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29382b6f-97a6-49b4-ae32-e8ee9f437c7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec3b50ba-5cf0-4559-a711-fd5200545e3c",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "24b5f16b-c7cc-46ff-b126-57e3c7ebfe2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "cfd45ebb-1be0-49c4-9762-bcee83fbd014",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24b5f16b-c7cc-46ff-b126-57e3c7ebfe2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc9da489-b83d-49c6-9441-f98b2ab02294",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24b5f16b-c7cc-46ff-b126-57e3c7ebfe2f",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "c1d81148-e4fd-4c30-999b-0df9a44472f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "668e9a87-f134-4b41-9146-c1dea80d0029",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1d81148-e4fd-4c30-999b-0df9a44472f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10a19891-021b-4be0-ab3a-584edde91b0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1d81148-e4fd-4c30-999b-0df9a44472f3",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "305b09f9-fc33-44a3-955a-2545c7075d4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "be584330-44e1-4238-9f23-9f58ab61d469",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "305b09f9-fc33-44a3-955a-2545c7075d4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "001557a0-3de8-4e75-b943-4cfdcbf64fb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "305b09f9-fc33-44a3-955a-2545c7075d4c",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "462ff596-7026-40b3-b925-3ad4df02a2e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "3b509379-fafa-43ba-a539-59e27f88b00c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "462ff596-7026-40b3-b925-3ad4df02a2e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f1bcbbf-6c25-44f8-b795-cf738efd30fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "462ff596-7026-40b3-b925-3ad4df02a2e5",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "d249eb06-73db-425c-bcac-7d940baedf04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "93316590-df1d-491a-bca4-ebb57ce27d15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d249eb06-73db-425c-bcac-7d940baedf04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "958437cd-3946-44ba-8e01-55485ddf5aba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d249eb06-73db-425c-bcac-7d940baedf04",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "22b65367-9977-4510-af1a-1676ab9e60a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "91710fbe-f8ce-4c9d-a6d3-9d40bfb1138b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22b65367-9977-4510-af1a-1676ab9e60a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "619830d2-4137-410d-a5ca-d0e49decc1d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22b65367-9977-4510-af1a-1676ab9e60a1",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "2d0a0234-d307-423c-8941-6e486ea49ae2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "dc127431-9cb1-4ead-a7e3-51c60e9a5d64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d0a0234-d307-423c-8941-6e486ea49ae2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59afb14f-824a-4719-bb60-8769079dbc52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d0a0234-d307-423c-8941-6e486ea49ae2",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "b65a4a5d-6ee3-4e26-85de-03b450b8f20e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "b6f8fb9e-4c84-44f6-bd11-37782cd0e07f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b65a4a5d-6ee3-4e26-85de-03b450b8f20e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f5e4575-7afc-4e5f-919c-21b4e066d10e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b65a4a5d-6ee3-4e26-85de-03b450b8f20e",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "f4d06a91-281b-4583-a01a-dd59d9511749",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "e650ca60-c0b4-4b7d-bcd3-23537a3dd0c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4d06a91-281b-4583-a01a-dd59d9511749",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "981a9143-390b-46e2-a505-a8cf1b0322d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4d06a91-281b-4583-a01a-dd59d9511749",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "28b96287-1bfd-4629-9831-8e80a51a7c87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "f76d052b-073a-4d68-b703-c09d37a1c2f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28b96287-1bfd-4629-9831-8e80a51a7c87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d393ce38-2173-41c8-a59c-5d0aeeee5873",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28b96287-1bfd-4629-9831-8e80a51a7c87",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "5e9afa10-27bc-4530-9126-18ada47f3773",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "d0225d05-defa-4e07-9a40-c6a3b17ad02f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e9afa10-27bc-4530-9126-18ada47f3773",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b354faa-7075-418e-8e44-437f75a9814f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e9afa10-27bc-4530-9126-18ada47f3773",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "4f972e78-105e-4dff-9055-b316038dbdd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "af95c837-1500-4a0e-94aa-8cfb8caf319f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f972e78-105e-4dff-9055-b316038dbdd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d351da81-2567-4e6a-959b-e7b318351f80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f972e78-105e-4dff-9055-b316038dbdd4",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "0dbdb99c-08e9-4537-85a4-2d73df8fb2ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "de64973b-8693-4279-97ae-175688052906",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dbdb99c-08e9-4537-85a4-2d73df8fb2ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b23675fa-6aeb-4526-93ef-9cabe4a0f974",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dbdb99c-08e9-4537-85a4-2d73df8fb2ff",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "01fe1966-7747-4858-a782-0a0a193c93a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "684c6f02-9ef1-4995-9550-5f0c4a997f83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01fe1966-7747-4858-a782-0a0a193c93a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7975438-9fb8-4a28-8a3b-5da18c374b3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01fe1966-7747-4858-a782-0a0a193c93a1",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "d590c2c8-d698-4814-9395-c279a93caab9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "fbe8a462-2ace-4ca4-be7b-5c5f48dce786",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d590c2c8-d698-4814-9395-c279a93caab9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba9337bb-74de-4337-9b0d-12134af78d90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d590c2c8-d698-4814-9395-c279a93caab9",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        },
        {
            "id": "a3ff6344-5b53-4670-b3ea-a39dfc4731eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "compositeImage": {
                "id": "c9acdb75-d44a-4132-8e34-8d360e05bd7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3ff6344-5b53-4670-b3ea-a39dfc4731eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be5ac49d-d03f-43a0-87d3-de0aa8ad15e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3ff6344-5b53-4670-b3ea-a39dfc4731eb",
                    "LayerId": "3f765673-cfbe-4db0-bf93-e21012fcb857"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "3f765673-cfbe-4db0-bf93-e21012fcb857",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1f5c687-ca7f-4a98-bbac-02f1acc886e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}