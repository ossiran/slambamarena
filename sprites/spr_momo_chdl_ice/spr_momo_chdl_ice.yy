{
    "id": "fec0d028-02f2-4e22-a38d-2feee7473b45",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_chdl_ice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 122,
    "bbox_left": 24,
    "bbox_right": 154,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bd0a0dfc-c6d3-4874-a151-5b76f86b8660",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "04891a5e-e26c-41e6-acce-ac5c8bb52793",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd0a0dfc-c6d3-4874-a151-5b76f86b8660",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6e034af-55bc-4b9e-950e-32d2c457f82e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd0a0dfc-c6d3-4874-a151-5b76f86b8660",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "3be7185a-bc22-490c-8ce6-a245c0b4b3ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "94b33944-3e19-49ba-8462-b5228a176acf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3be7185a-bc22-490c-8ce6-a245c0b4b3ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15a816e2-62e9-4e71-89a3-929fcb988133",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3be7185a-bc22-490c-8ce6-a245c0b4b3ec",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "23428223-74ea-4ac3-898a-754f99391f6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "15cc6c2a-de62-40d2-94ea-c9393284bded",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23428223-74ea-4ac3-898a-754f99391f6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3834438-dc15-4990-af49-6ac49f528ea6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23428223-74ea-4ac3-898a-754f99391f6f",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "2d1c5385-e3e9-42c8-b143-702bf39029d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "0b49f00f-173c-470e-9511-25fbbaa6b103",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d1c5385-e3e9-42c8-b143-702bf39029d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ba932fa-ef14-4c19-a13d-38547d7373e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d1c5385-e3e9-42c8-b143-702bf39029d2",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "28f32061-4fba-42df-8972-0dd9b803e98b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "bee5a0aa-1a6d-472c-b159-8e55bc2c46aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28f32061-4fba-42df-8972-0dd9b803e98b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b8cb636-5b1a-4532-83a5-e425b7ed1239",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28f32061-4fba-42df-8972-0dd9b803e98b",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "206f2b25-dded-4c07-a8f2-aedc819be8ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "c10d4fbc-da17-4961-854f-bf02a29aee3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "206f2b25-dded-4c07-a8f2-aedc819be8ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d9ad5c2-9b97-4543-8426-d1b065014420",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "206f2b25-dded-4c07-a8f2-aedc819be8ff",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "e8480824-53d2-4407-a37e-e18acc7e6a5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "3fd64f0a-33c6-4dd1-b671-99cd76aaefed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8480824-53d2-4407-a37e-e18acc7e6a5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a1cc6ed-c2c6-4310-8329-e41198dbf084",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8480824-53d2-4407-a37e-e18acc7e6a5e",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "41d6f79e-309d-4958-ab1a-fb6341b2dfee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "95c529d1-e08a-4216-bc90-fbc7c8ec96d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41d6f79e-309d-4958-ab1a-fb6341b2dfee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69a4ed35-bee1-48a5-818c-a9832c9f45ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41d6f79e-309d-4958-ab1a-fb6341b2dfee",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "c63bd51c-42e4-4ab4-aeef-b4dc3537e6db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "2d24f918-2c3b-4170-9225-deb7f30d8c40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c63bd51c-42e4-4ab4-aeef-b4dc3537e6db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e314bf9-7580-4341-b862-71b812926c5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c63bd51c-42e4-4ab4-aeef-b4dc3537e6db",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "a634e2ab-c737-4ceb-b1f5-6bc219db3efc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "95704905-0ccb-4e09-9c60-5d7133521628",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a634e2ab-c737-4ceb-b1f5-6bc219db3efc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fb161c3-abc9-4d3d-912e-5d6a82f790c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a634e2ab-c737-4ceb-b1f5-6bc219db3efc",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "bb2474cc-d68f-4278-9569-f298cbe2530e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "567db8f8-f3e9-4203-83a1-aff0277e7407",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb2474cc-d68f-4278-9569-f298cbe2530e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "522f99f5-9ed5-4355-b7c0-1a89e149b06e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb2474cc-d68f-4278-9569-f298cbe2530e",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "c0f2dc8d-4a59-46cb-bef0-a53ed0b96b81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "178fa72b-0ddb-4a00-86eb-b7870800e4f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0f2dc8d-4a59-46cb-bef0-a53ed0b96b81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb50f158-5f91-4fd4-a617-a80531c6fa08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0f2dc8d-4a59-46cb-bef0-a53ed0b96b81",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "8741cfe3-bb9a-4d7b-a72c-6020613f3df0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "7a988e8a-9d36-42b9-81e2-8aabf35c0225",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8741cfe3-bb9a-4d7b-a72c-6020613f3df0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e582b34-a17c-4833-93d4-b1dd62aeeaaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8741cfe3-bb9a-4d7b-a72c-6020613f3df0",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "ff0c88cf-cc4f-424a-b785-a3e80deb79bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "8f68c3c5-8231-45a3-a81c-95770775d490",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff0c88cf-cc4f-424a-b785-a3e80deb79bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6302db1-8e26-4f4c-b3c6-13fab0c7a516",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff0c88cf-cc4f-424a-b785-a3e80deb79bf",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "a1213f21-a1c6-40f1-a933-24208ef7439c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "0d734b2d-ec87-476a-b6ba-6bbff0f50a14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1213f21-a1c6-40f1-a933-24208ef7439c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "074b7fc5-ea8c-4a5e-9080-43bcc31ec7d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1213f21-a1c6-40f1-a933-24208ef7439c",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "d71180d5-9b15-413f-b858-b04ec4c1767b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "bd46b649-59e5-4b3d-82ca-17d80246aad7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d71180d5-9b15-413f-b858-b04ec4c1767b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f124add-6c39-4438-9d58-014937348e78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d71180d5-9b15-413f-b858-b04ec4c1767b",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "16afc1b5-8034-4a47-b8ce-3a62087dd281",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "12ed38fa-fdef-4adf-b99d-f7a5b460d125",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16afc1b5-8034-4a47-b8ce-3a62087dd281",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a51a45d9-3739-4f20-b224-7244f8ff86cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16afc1b5-8034-4a47-b8ce-3a62087dd281",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "72a06742-21e9-4c89-aabe-d0a8f151890f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "9fc495ee-c3eb-40dd-8dc5-4f1e0204d078",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72a06742-21e9-4c89-aabe-d0a8f151890f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f829acb0-51a2-4a9d-bee8-027d31abb5cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72a06742-21e9-4c89-aabe-d0a8f151890f",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "5e4b7928-d511-4e54-9fe0-53fd534f3d7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "9fa5746f-a858-4014-8f92-6daded3faf38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e4b7928-d511-4e54-9fe0-53fd534f3d7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a410191-83d1-445b-9245-d3d2eb9690f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e4b7928-d511-4e54-9fe0-53fd534f3d7a",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "9f86184c-ee56-46df-b900-8366fe558fb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "f341053f-2804-4d76-9140-bea5ae15812d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f86184c-ee56-46df-b900-8366fe558fb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d8581f5-a383-4042-8c7b-cd38bc7f3d12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f86184c-ee56-46df-b900-8366fe558fb6",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "748f9745-4675-488f-b777-d537fd17967c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "0ca8058b-a749-41d8-8bca-17de635a6851",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "748f9745-4675-488f-b777-d537fd17967c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01c7e0db-7974-48e0-a7f7-b3fec3d0d25b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "748f9745-4675-488f-b777-d537fd17967c",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "bdc7c9c7-17d5-46b5-89ba-22e7bd48167e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "a20c95fb-6400-4362-bc24-85fc279a4d79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdc7c9c7-17d5-46b5-89ba-22e7bd48167e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8d4c0d0-e5f8-411c-bbef-7a0ae6340bd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdc7c9c7-17d5-46b5-89ba-22e7bd48167e",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "0486a579-d572-4b29-b7f3-305715838b89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "cea23e49-c305-4ffb-bd43-55cb5e0a54a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0486a579-d572-4b29-b7f3-305715838b89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2445a1c-6075-4f3a-8e3b-b01d15bd96fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0486a579-d572-4b29-b7f3-305715838b89",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "bc108f48-a326-44ed-98e6-2c45006445cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "60447653-d19f-44c7-a581-02dd35099157",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc108f48-a326-44ed-98e6-2c45006445cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5430f289-ea6f-4d2f-99f6-671d6b0ee3b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc108f48-a326-44ed-98e6-2c45006445cf",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "da9d3846-e780-4d74-81d8-79ca87f40108",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "2d5d9313-0090-492c-837d-e31ff74e4545",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da9d3846-e780-4d74-81d8-79ca87f40108",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b377a9c-820b-4f6d-a67f-20e0aafb8950",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da9d3846-e780-4d74-81d8-79ca87f40108",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "cce876c0-3372-49b5-9b2a-bc3a8869fef2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "a9de47e3-9b4d-4d45-9116-5f5d43568ee0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cce876c0-3372-49b5-9b2a-bc3a8869fef2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d42830e-2c58-4cb6-9504-022f26f60f5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cce876c0-3372-49b5-9b2a-bc3a8869fef2",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "c0bca8d9-d2d2-4145-b2f9-0bc7626a45f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "06947e98-171b-483c-9a90-5b6ea982bd75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0bca8d9-d2d2-4145-b2f9-0bc7626a45f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6285091a-ce23-44db-890f-66bef1785690",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0bca8d9-d2d2-4145-b2f9-0bc7626a45f0",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "2ce61db6-ae0d-4c0c-9254-ad1af2f3e7c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "dbd0cab6-5256-44e6-b79c-4053b53833c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ce61db6-ae0d-4c0c-9254-ad1af2f3e7c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "323572f1-86d3-4ccc-bfb3-0bc95f9f36a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ce61db6-ae0d-4c0c-9254-ad1af2f3e7c1",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "9db968d5-9586-417d-8c35-f8eddd039983",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "fe91dea5-5ac7-44e3-a200-d7197990e57b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9db968d5-9586-417d-8c35-f8eddd039983",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "351fcf11-1648-4494-ada1-784eda58cc61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9db968d5-9586-417d-8c35-f8eddd039983",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "4e72b873-6c11-4e5f-9bbb-d02966f613f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "77a9316f-8718-4e1e-bedc-e8c69d6c6409",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e72b873-6c11-4e5f-9bbb-d02966f613f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c117661-2471-41ee-adb8-2412620d0f45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e72b873-6c11-4e5f-9bbb-d02966f613f2",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "0c6afb51-3296-4ceb-93f1-b9cc60e92f37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "fc654f2f-4772-49e8-af32-4eb764607ac1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c6afb51-3296-4ceb-93f1-b9cc60e92f37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e15af500-97d8-4a4a-80df-0732b82b4d25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c6afb51-3296-4ceb-93f1-b9cc60e92f37",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "a244324d-cc00-425f-989d-433e6b70f505",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "4a1e2d3f-0395-41da-ba2f-c19c6f61c15d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a244324d-cc00-425f-989d-433e6b70f505",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9b681f6-10fe-4ca6-9886-0a6a1578945e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a244324d-cc00-425f-989d-433e6b70f505",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "7b2af110-4618-49be-9320-14ab5080cae3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "fcdf03ab-f31a-48bd-bbbf-65f8a613025a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b2af110-4618-49be-9320-14ab5080cae3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebb2d805-1134-4e7a-8f73-3785a4b13e27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b2af110-4618-49be-9320-14ab5080cae3",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "8eab44c0-8f79-40ac-895c-dc890df810ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "4ba2d9ae-c676-4ae7-9c46-51baae6cdac9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8eab44c0-8f79-40ac-895c-dc890df810ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f4b268e-3dd7-4550-a6de-fe434a1c82e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8eab44c0-8f79-40ac-895c-dc890df810ff",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "4cd1165a-e01d-468a-b273-71ae7a7e858d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "cc1b3db3-d616-4927-bfff-3e67cfdc6a6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cd1165a-e01d-468a-b273-71ae7a7e858d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "670f7646-33de-4aa0-bfd5-67e6bdf23e24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cd1165a-e01d-468a-b273-71ae7a7e858d",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "d698bd50-3a6f-4d54-b2bc-5d6270377b3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "195e5dc6-2ec6-4264-8cb1-42c7c86e8753",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d698bd50-3a6f-4d54-b2bc-5d6270377b3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e95ca929-b145-4717-94ea-db170c54a383",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d698bd50-3a6f-4d54-b2bc-5d6270377b3f",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "520267e7-49c7-4552-94f1-31df6055c5fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "6f2807e6-81f2-436f-b87b-02024b5f0193",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "520267e7-49c7-4552-94f1-31df6055c5fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed4668c2-164c-45a3-9069-527638c1c79d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "520267e7-49c7-4552-94f1-31df6055c5fc",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "d990fe2b-cbd8-4c41-9355-9f2e62509f80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "2078e731-4224-4b89-a142-915b16e87a2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d990fe2b-cbd8-4c41-9355-9f2e62509f80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da07a4a8-1e01-4b49-99d1-5d1e36ab0bfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d990fe2b-cbd8-4c41-9355-9f2e62509f80",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "c8152de5-8778-451d-8ad6-ff094c104b8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "cc020e40-187b-4b29-9686-014c24559ba4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8152de5-8778-451d-8ad6-ff094c104b8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14c97aed-6fa3-4472-89d0-e83afbef8a17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8152de5-8778-451d-8ad6-ff094c104b8b",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "2a81786f-8661-4a1c-835a-a0511c92366a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "eb2e3a50-fcff-4f1d-b143-189f82b9eea8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a81786f-8661-4a1c-835a-a0511c92366a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "180713ec-ac5e-4fbf-84bb-67c574b82f43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a81786f-8661-4a1c-835a-a0511c92366a",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "7e4f9a55-c051-4f9b-bf95-d8a1a0f08c3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "17e86b0b-3403-4eab-8d61-13b376127662",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e4f9a55-c051-4f9b-bf95-d8a1a0f08c3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "538a0411-9b95-4b11-9933-5814d359994b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e4f9a55-c051-4f9b-bf95-d8a1a0f08c3a",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "007ee5c6-5b64-44ab-bc3f-3ae8dff7b6c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "733cb606-a73d-46dc-b5ab-30df733ac936",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "007ee5c6-5b64-44ab-bc3f-3ae8dff7b6c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35673434-3df2-4bc9-aca9-92b3546f2e6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "007ee5c6-5b64-44ab-bc3f-3ae8dff7b6c7",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        },
        {
            "id": "e9c48d51-6b23-41ef-9bb4-471fe6b74bd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "compositeImage": {
                "id": "1f26b737-3fa4-44a8-bcfe-a8c6c1b82b01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9c48d51-6b23-41ef-9bb4-471fe6b74bd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "331abe11-86f1-4743-bebd-7ed3ec4353a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9c48d51-6b23-41ef-9bb4-471fe6b74bd9",
                    "LayerId": "781b7f20-913b-4128-a90a-1cbe6cc06b8c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 148,
    "layers": [
        {
            "id": "781b7f20-913b-4128-a90a-1cbe6cc06b8c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fec0d028-02f2-4e22-a38d-2feee7473b45",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 168,
    "xorig": 63,
    "yorig": 87
}