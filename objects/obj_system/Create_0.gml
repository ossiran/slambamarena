//state machine
state_machine_init();

state_create("main_menu",state_sys_menu);
state_create("char_select",state_sys_char);
state_create("char_select2",state_sys_char2);
state_create("char_select3",state_sys_char3);
state_create("start_match",state_sys_start_match);
state_create("start_round",state_sys_start_round);
state_create("mid_round",state_sys_mid_round);
state_create("end_round",state_sys_end_round);
state_create("end_match",state_sys_end_match);
state_create("training",state_sys_training);

state_init("main_menu");

live_init(3, "http://127.0.0.1:5100" , "");
//variables

depth = -9999;
player1 = 0;
player2 = 0;

showinvis = 1;

player1_controller = -2;
player2_controller = -3;

player1_char = 0;
player2_char = 0;

controller1 = 0;
controller2 = 0;

players_control = 0;

ready_up = 0;

training = 0;

//message array. 0 is the message string. 1 is the amount of frames the message will be shown for. ticks down every frame;
message_array_1[0,1] = 0;
message_array_2[0,1] = 0;

message_array_count_1 = 0;
message_array_count_2 = 0;

//player1 controls

key_up_1 = gp_padu;
key_down_1 = gp_padd;
key_left_1 = gp_padl;
key_right_1 = gp_padr;

key_light_1 = gp_face3;
key_medium_1 = gp_face4;
key_heavy_1 = gp_face2;
key_essence_1 = gp_face1;

key_dash_1 = gp_shoulderrb;
key_jump_1 = gp_shoulderlb;
key_grab_1 = gp_shoulderr;


//player2 controls

key_up_2 = gp_padu;
key_down_2 = gp_padd;
key_left_2 = gp_padl;
key_right_2 = gp_padr;

key_light_2 = gp_face3;
key_medium_2 = gp_face4;
key_heavy_2 = gp_face2;
key_essence_2 = gp_face1;

key_dash_2 = gp_shoulderrb;
key_jump_2 = gp_shoulderlb;
key_grab_2 = gp_shoulderr;

//keyboard controls

keyboard_up = ord("W");
keyboard_down = ord("S");
keyboard_left = ord("A");
keyboard_right = ord("D");

keyboard_light = vk_numpad7;
keyboard_medium = vk_numpad8;
keyboard_heavy = vk_numpad9;
keyboard_essence = vk_numpad4;

keyboard_dash = ord("L");
keyboard_jump = vk_space;
keyboard_grab = ord(".");

keyboard_start = vk_enter;

//draw prio
draw_priority = 1;
//menu variables
menu_maxx = 0;
menu_maxy = -1;
menu_indexx = 0;
menu_indexy = 0;
//makeshift 3d array
//menu_array[7,7,7] = "";
menu_array[500,7] = "";

image_speed = 0.5;

//pause 

pause = 0;
unpaused_player = -1;
paused_objects[10,6] = -1;

//timer
clock = 0;
//initializing 2d array
for(i = 0; i < 11; i++)
{
    
    for(ii = 0; ii < 7 ; ii++)
    {
        paused_objects[i,ii] = -1;
    }
}

//screen
cam = 0;

window_set_fullscreen(true);


MonitorW=display_get_width();
MonitorH=display_get_height();


display_set_gui_size(1280, 720);


Xoffset=(MonitorW-1920)/2;
Yoffset=(MonitorH-1080)/2;

surface_resize(application_surface,1920,1080);

//colors
ex_color = make_color_rgb(120,120,240);
ex_colorF = make_color_rgb(175,175,255);
ex_color1 = make_color_rgb(70,70,185);
ex_colorF1 = make_color_rgb(125,125,200);
ex_color2 = make_color_rgb(50,50,150);
ex_colorF2 = make_color_rgb(100,100,180);
ex_color3 = make_color_rgb(40,40,120);
ex_colorF3 = make_color_rgb(80,80,150);
ex_colorF4 = make_color_rgb(255,255,255);

hp_colorF = make_color_rgb(240,240,80);

roundstartpos1 = 0;
roundstartpos2 = 0;