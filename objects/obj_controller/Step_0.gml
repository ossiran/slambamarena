/// @description movement
get_controls_1(controller_num);

//movement not working for some reason with atleast keyboard. implement input machine
//left
if(key_left == 1 && counter == 0 && positionx > positionx_min && ready_up == 0)
{
	positionx --;
	counter = 10;
}

//right
if(key_right == 1 && counter == 0 && positionx < positionx_max && ready_up == 0)
{
	positionx ++;
	counter = 10;
}

//up
if(key_up == 1  && counter == 0 && positiony > positiony_min && ready_up == 0)
{
	positiony --;
	counter = 10;
}

//down
if(key_down == 1 && counter == 0 && positiony < positiony_max && ready_up == 0)
{
	positiony ++;
	counter = 10;
}

//counter
if(counter > 0)
{
    counter--;
}
//position in controller select
if(obj_system.state_name ==  "char_select")
{
    
    
    //image index and re ready
    if(ready_up == 1)
    {
    	image_index = 1;
    	//unready
    	if(key_start == 1)
    	{
    	    obj_system.ready_up ++;
    	}
    	if(key_essence == 1)
    	{
    		ready_up = 0;
    		obj_system.ready_up --;
    	}
    }
    else
    {
    	image_index = 0;	
    }
    
    //ready up
    if(key_start == 1 && ready_up == 0 && positionx != 0 && !place_meeting(x,y-200,obj_controller) && !place_meeting(x,y+200,obj_controller))
    {
    	ready_up = 1;
    	obj_system.ready_up ++;
    	
    	if(positionx < 0)
    	{
    	    obj_system.player1_controller = controller_num;
    	}
    	else
    	{
    	    obj_system.player2_controller = controller_num;
    	}
    }

    x = room_width/2 + (positionx * 200);

    y = display_get_gui_height()/2 + 100 * controller_num;
}
else //position in char select
{
    //image index and re ready
    if(ready_up == 1)
    {
    	image_index = 1;
    	if(obj_system.training == 1 && key_start == 1 )
    	{
    	    obj_system.ready_up ++;
    	}
    	//unready
    	if(key_essence == 1)
    	{
    		ready_up = 0;
    		obj_system.ready_up --;
    	}
    }
    else
    {
    	image_index = 0;	
    }
    
    //ready up
    if(key_light == 1 && ready_up == 0 )
    {
    	ready_up = 1;
    	obj_system.ready_up ++;
    }
    
    
    //position
    x = room_width/2 + (-64 * obj_system.menu_maxx/2) + (positionx * 64);

    y = room_height/2 + -floor(obj_system.menu_maxy/2) + (positiony * 64);
}

//player2 skin
if(player_num == 2)
{
sprite_index = spr_controller2;
}

