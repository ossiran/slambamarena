{
    "id": "b907c60a-9fde-4606-a8b6-5e94a89b968f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lsyn_5l",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 45,
    "bbox_right": 99,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5b283f53-34c4-498d-8ece-cf38951467c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b907c60a-9fde-4606-a8b6-5e94a89b968f",
            "compositeImage": {
                "id": "f8a75729-6a0c-4b5d-96eb-31f05cc06186",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b283f53-34c4-498d-8ece-cf38951467c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a23a6b9a-3db2-4bb5-a87d-5d4286a6e906",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b283f53-34c4-498d-8ece-cf38951467c7",
                    "LayerId": "b380eda9-708a-45fb-85bd-4d0f3490f868"
                }
            ]
        },
        {
            "id": "3fa61332-b707-4e0c-86fa-43f7d6f96927",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b907c60a-9fde-4606-a8b6-5e94a89b968f",
            "compositeImage": {
                "id": "b0661569-8b09-420f-b811-552eed2bc7bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fa61332-b707-4e0c-86fa-43f7d6f96927",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d8653ba-a758-4bea-be6c-0059a82d4fb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fa61332-b707-4e0c-86fa-43f7d6f96927",
                    "LayerId": "b380eda9-708a-45fb-85bd-4d0f3490f868"
                }
            ]
        },
        {
            "id": "da363c3c-ad3c-4e00-88c7-b5663a718f0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b907c60a-9fde-4606-a8b6-5e94a89b968f",
            "compositeImage": {
                "id": "b95442bd-bac2-4a0b-86ec-3cbde197e6b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da363c3c-ad3c-4e00-88c7-b5663a718f0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ad8f6ec-95f0-41b2-82c2-8113a83e6ebe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da363c3c-ad3c-4e00-88c7-b5663a718f0a",
                    "LayerId": "b380eda9-708a-45fb-85bd-4d0f3490f868"
                }
            ]
        },
        {
            "id": "1bb73c63-4777-47b2-84bf-8745ad091584",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b907c60a-9fde-4606-a8b6-5e94a89b968f",
            "compositeImage": {
                "id": "a158d74d-0890-4baa-aabd-fe13568b4083",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bb73c63-4777-47b2-84bf-8745ad091584",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c5b307b-6dec-4264-afee-be5e5be62862",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bb73c63-4777-47b2-84bf-8745ad091584",
                    "LayerId": "b380eda9-708a-45fb-85bd-4d0f3490f868"
                }
            ]
        },
        {
            "id": "637b13b9-3e10-4f80-ad10-b143da3e07e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b907c60a-9fde-4606-a8b6-5e94a89b968f",
            "compositeImage": {
                "id": "0f176f2c-f35b-47b7-ac26-27c66c8afcd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "637b13b9-3e10-4f80-ad10-b143da3e07e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46d35f29-06ff-49fa-a9fa-f2a8946b62b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "637b13b9-3e10-4f80-ad10-b143da3e07e5",
                    "LayerId": "b380eda9-708a-45fb-85bd-4d0f3490f868"
                }
            ]
        },
        {
            "id": "c58693fe-3cd9-427c-8d72-32adf428f677",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b907c60a-9fde-4606-a8b6-5e94a89b968f",
            "compositeImage": {
                "id": "b9bfb617-1293-4d63-89c4-fa14d95b068e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c58693fe-3cd9-427c-8d72-32adf428f677",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01b613ca-60c2-4685-8e69-cc0d6d2c1493",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c58693fe-3cd9-427c-8d72-32adf428f677",
                    "LayerId": "b380eda9-708a-45fb-85bd-4d0f3490f868"
                }
            ]
        },
        {
            "id": "7a686722-172d-4ede-84f2-db41eb7dcb1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b907c60a-9fde-4606-a8b6-5e94a89b968f",
            "compositeImage": {
                "id": "b52f98a8-e8c1-46e6-91c9-238e300718f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a686722-172d-4ede-84f2-db41eb7dcb1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "147558f7-7cac-468c-b237-86f645142703",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a686722-172d-4ede-84f2-db41eb7dcb1c",
                    "LayerId": "b380eda9-708a-45fb-85bd-4d0f3490f868"
                }
            ]
        },
        {
            "id": "ad361c2a-4cfb-45ce-bc7a-d5772449f8ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b907c60a-9fde-4606-a8b6-5e94a89b968f",
            "compositeImage": {
                "id": "8f7bf040-50e1-4689-aa5d-d13391be1437",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad361c2a-4cfb-45ce-bc7a-d5772449f8ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08b095db-fb1b-4400-a240-d195a7cd91fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad361c2a-4cfb-45ce-bc7a-d5772449f8ea",
                    "LayerId": "b380eda9-708a-45fb-85bd-4d0f3490f868"
                }
            ]
        },
        {
            "id": "62a8ee7e-15ea-42f5-bc06-30a47e21d510",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b907c60a-9fde-4606-a8b6-5e94a89b968f",
            "compositeImage": {
                "id": "2901eb24-db52-4638-ac52-afedece9e598",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62a8ee7e-15ea-42f5-bc06-30a47e21d510",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47dc7354-6afe-414c-bcdc-de6b4e520af6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62a8ee7e-15ea-42f5-bc06-30a47e21d510",
                    "LayerId": "b380eda9-708a-45fb-85bd-4d0f3490f868"
                }
            ]
        },
        {
            "id": "fa1e84df-6c57-4902-81d6-ce74f3334b55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b907c60a-9fde-4606-a8b6-5e94a89b968f",
            "compositeImage": {
                "id": "0e7b2258-7bea-4f2e-a9c8-def84b0751de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa1e84df-6c57-4902-81d6-ce74f3334b55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9437411-5b8a-47f7-a7c2-893aee990d51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa1e84df-6c57-4902-81d6-ce74f3334b55",
                    "LayerId": "b380eda9-708a-45fb-85bd-4d0f3490f868"
                }
            ]
        },
        {
            "id": "e96e9579-df39-48b1-b8ae-8248db4d2bd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b907c60a-9fde-4606-a8b6-5e94a89b968f",
            "compositeImage": {
                "id": "11e0d9bb-5a9b-4c46-9f5b-91d9f485a82b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e96e9579-df39-48b1-b8ae-8248db4d2bd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e210420-f97e-40fe-9bc2-9d64da2e2de7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e96e9579-df39-48b1-b8ae-8248db4d2bd1",
                    "LayerId": "b380eda9-708a-45fb-85bd-4d0f3490f868"
                }
            ]
        },
        {
            "id": "f6e4dc3a-450a-476b-a01d-261bf106fc2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b907c60a-9fde-4606-a8b6-5e94a89b968f",
            "compositeImage": {
                "id": "f17aa5b1-b13a-4cd3-adf8-26b7886d6c82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6e4dc3a-450a-476b-a01d-261bf106fc2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2d4ff5b-053f-4b82-9493-d07b25208997",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6e4dc3a-450a-476b-a01d-261bf106fc2a",
                    "LayerId": "b380eda9-708a-45fb-85bd-4d0f3490f868"
                }
            ]
        },
        {
            "id": "ae8b7c18-4a4e-4561-ab0c-16cf6ea68c35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b907c60a-9fde-4606-a8b6-5e94a89b968f",
            "compositeImage": {
                "id": "4e56527e-6f41-4e8a-bd2a-e4abc9456b34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae8b7c18-4a4e-4561-ab0c-16cf6ea68c35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b9966d5-1a40-432d-8d96-415990079fa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae8b7c18-4a4e-4561-ab0c-16cf6ea68c35",
                    "LayerId": "b380eda9-708a-45fb-85bd-4d0f3490f868"
                }
            ]
        },
        {
            "id": "5e7bdc1b-839f-4e4c-bf92-ea9094aca8fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b907c60a-9fde-4606-a8b6-5e94a89b968f",
            "compositeImage": {
                "id": "e7f65099-06e0-47a0-af0f-508cc8f7021f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e7bdc1b-839f-4e4c-bf92-ea9094aca8fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29ba1ca1-f680-42c7-bbb4-abc6cb8796a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e7bdc1b-839f-4e4c-bf92-ea9094aca8fe",
                    "LayerId": "b380eda9-708a-45fb-85bd-4d0f3490f868"
                }
            ]
        },
        {
            "id": "1c490a5c-9e51-4dac-8aa1-47c5d7115900",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b907c60a-9fde-4606-a8b6-5e94a89b968f",
            "compositeImage": {
                "id": "0299ec34-0960-4d11-a9ef-d6a5b7124c5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c490a5c-9e51-4dac-8aa1-47c5d7115900",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b0bfa4d-56f0-44e0-b8cc-84c7523aa746",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c490a5c-9e51-4dac-8aa1-47c5d7115900",
                    "LayerId": "b380eda9-708a-45fb-85bd-4d0f3490f868"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "b380eda9-708a-45fb-85bd-4d0f3490f868",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b907c60a-9fde-4606-a8b6-5e94a89b968f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}