{
    "id": "007c21af-3543-4ea2-b0fd-201956b3a135",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dsyn_66m",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2169963-f2a8-4a61-90bd-fa30bf7d20e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "007c21af-3543-4ea2-b0fd-201956b3a135",
            "compositeImage": {
                "id": "c6d244be-9bf2-4713-ab8c-67fe3527ce65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2169963-f2a8-4a61-90bd-fa30bf7d20e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3539d43e-3ca2-4148-835f-44b388d49844",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2169963-f2a8-4a61-90bd-fa30bf7d20e5",
                    "LayerId": "929f0248-8577-49ec-a4c3-6770c0a47e63"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "929f0248-8577-49ec-a4c3-6770c0a47e63",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "007c21af-3543-4ea2-b0fd-201956b3a135",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}