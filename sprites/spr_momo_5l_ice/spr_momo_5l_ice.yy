{
    "id": "8e3270e8-1b88-46fe-baef-f39469f34b38",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_5l_ice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b55f059a-f65c-4c85-b2f8-e48117af4404",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e3270e8-1b88-46fe-baef-f39469f34b38",
            "compositeImage": {
                "id": "f3402d26-a03f-4e47-963b-7fdae97c7027",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b55f059a-f65c-4c85-b2f8-e48117af4404",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b10b558-32b9-4042-90a4-4e2c65295f14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b55f059a-f65c-4c85-b2f8-e48117af4404",
                    "LayerId": "2bbf4a29-6ef4-4ee2-bd60-79396e44ec8a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2bbf4a29-6ef4-4ee2-bd60-79396e44ec8a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e3270e8-1b88-46fe-baef-f39469f34b38",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}