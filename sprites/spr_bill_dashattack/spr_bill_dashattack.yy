{
    "id": "849c4571-5007-4a27-8817-2536a78dfb97",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_dashattack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 26,
    "bbox_right": 111,
    "bbox_top": 66,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24f70c23-185a-45cd-91ad-e959e20990ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "b3b156e9-fca5-42bc-979c-0f4a3b97559b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24f70c23-185a-45cd-91ad-e959e20990ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb0d13bd-0824-41b4-8d57-ee5efcaace62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24f70c23-185a-45cd-91ad-e959e20990ae",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "42b829bd-c48c-4949-ae84-de53b22d4202",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "6f44930a-ef37-47e8-9761-73b8cc035956",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42b829bd-c48c-4949-ae84-de53b22d4202",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe6fb05d-933b-40fe-acfa-05e0c2454504",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42b829bd-c48c-4949-ae84-de53b22d4202",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "85ddb3f7-c6f4-48ed-a70e-334ff9d2d2ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "372b4b62-0b2b-4d61-9e1b-fa8b31ad10a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85ddb3f7-c6f4-48ed-a70e-334ff9d2d2ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "868e9614-ebf8-46a3-9489-28e798d2398c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85ddb3f7-c6f4-48ed-a70e-334ff9d2d2ae",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "d350dcb2-3643-494c-a305-6f122e27ff5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "635c5779-94ed-4aae-958c-363155ba3405",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d350dcb2-3643-494c-a305-6f122e27ff5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa0e62ed-4335-411a-bf62-16218fb44604",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d350dcb2-3643-494c-a305-6f122e27ff5a",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "f0795a01-ea3d-4be6-ac6f-e9fce75e03c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "6c9b0d63-c5de-4951-a050-95f30b6ff275",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0795a01-ea3d-4be6-ac6f-e9fce75e03c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c2ef4cb-5a75-432c-b731-9f32e0777985",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0795a01-ea3d-4be6-ac6f-e9fce75e03c1",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "ff57e737-4501-402f-acd8-70b9509d669b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "18f65d88-b709-460a-8c49-123d42ef4719",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff57e737-4501-402f-acd8-70b9509d669b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e182b2a6-976d-48c1-80e2-54a9396a5a93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff57e737-4501-402f-acd8-70b9509d669b",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "7fa4a897-cfea-4d37-8ae2-40b15689270e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "6ebbc2a5-455d-463e-86d1-a04f0613b9b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fa4a897-cfea-4d37-8ae2-40b15689270e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cca2f37-4f77-469f-878f-49be283c1470",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fa4a897-cfea-4d37-8ae2-40b15689270e",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "f1183d91-f2dc-4185-a6d5-e641abccb64f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "e889225f-6a21-471e-afd5-7701cd688af1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1183d91-f2dc-4185-a6d5-e641abccb64f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66dc7cf5-52bf-4eb3-a1e3-a12ecf84e797",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1183d91-f2dc-4185-a6d5-e641abccb64f",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "df8ce99e-1dbc-4eac-a531-26c45b8c45b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "79f5e4f5-bbac-4c93-a252-af99a351dcdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df8ce99e-1dbc-4eac-a531-26c45b8c45b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdac2bd4-b119-4cf2-8b5c-c1a81d3a6ee5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df8ce99e-1dbc-4eac-a531-26c45b8c45b6",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "41bacc42-c097-4e8e-b362-367d82f8b12d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "2d37f38c-8c77-47d3-bc34-2dac4bfabde3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41bacc42-c097-4e8e-b362-367d82f8b12d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c5cc4c6-d79a-47bf-a18b-49d0044db0ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41bacc42-c097-4e8e-b362-367d82f8b12d",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "1d42f97b-7168-4fcc-9247-3401f986f563",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "69ca55de-1b2e-474a-b038-34dd560e6ae4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d42f97b-7168-4fcc-9247-3401f986f563",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5ea22c3-e769-4bf9-a9fb-494bcf756fd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d42f97b-7168-4fcc-9247-3401f986f563",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "e3770825-95f5-4ae8-bd2a-87732e4c470c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "89c80546-d5d7-4a69-8eb4-4fc69c491efc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3770825-95f5-4ae8-bd2a-87732e4c470c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37998711-512e-403e-9571-2e88d384c650",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3770825-95f5-4ae8-bd2a-87732e4c470c",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "040b2488-e911-4742-80d6-d4f64cd9caf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "40f57b7e-1047-4ae6-b6de-658bd84da770",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "040b2488-e911-4742-80d6-d4f64cd9caf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13b03973-9847-4693-8bbe-27c8d042659e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "040b2488-e911-4742-80d6-d4f64cd9caf1",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "d3089f2a-dac6-4134-95b9-8259741eb760",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "4e892adf-0a63-4372-b149-205f983d2a71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3089f2a-dac6-4134-95b9-8259741eb760",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03969da3-b37b-4b07-9c92-b89e3f625136",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3089f2a-dac6-4134-95b9-8259741eb760",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "70be6f8c-7059-4ea2-b605-7da481fed06a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "6a803294-3292-48fd-a592-d36e7907d47f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70be6f8c-7059-4ea2-b605-7da481fed06a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e138da96-90ae-4c3c-8e26-55f00e02c885",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70be6f8c-7059-4ea2-b605-7da481fed06a",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "645bf392-a858-4809-9301-521607b57d1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "f565a1d5-02f1-42c7-9075-e5bfe940b1ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "645bf392-a858-4809-9301-521607b57d1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e80d746a-08ef-4a01-9adc-18a6939a4c06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "645bf392-a858-4809-9301-521607b57d1a",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "7b377b3f-5866-41fc-98ad-e146a73fdbf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "f8b70970-3d7a-4a29-9227-25dc56a029c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b377b3f-5866-41fc-98ad-e146a73fdbf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6149d2d-7f38-4bd8-aab0-02543893f323",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b377b3f-5866-41fc-98ad-e146a73fdbf0",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "36eddc4e-8295-4477-be04-a20552b94144",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "1513f74e-c51c-4cbe-a5fa-9ce5a338c425",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36eddc4e-8295-4477-be04-a20552b94144",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e2f6e10-077b-4536-9f89-1efda76a8466",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36eddc4e-8295-4477-be04-a20552b94144",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "8b7821d3-ebca-41cc-88e7-379502a1e55d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "6b89472b-2781-4fa3-baff-3cd669477a89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b7821d3-ebca-41cc-88e7-379502a1e55d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "498f86be-a1bf-47b4-90fb-0e42d04de412",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b7821d3-ebca-41cc-88e7-379502a1e55d",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "79aa078d-47ef-4c8a-b6ca-89ae413e83c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "0bdaafbf-ee3b-432a-8dc0-15a75b41b2fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79aa078d-47ef-4c8a-b6ca-89ae413e83c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8e8752b-e540-4acc-9ccc-7602b783e74e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79aa078d-47ef-4c8a-b6ca-89ae413e83c4",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "c1e455dc-60d6-416a-a639-3a24ea54cf2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "409a5780-4593-4398-9ecf-6730c51cb9b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1e455dc-60d6-416a-a639-3a24ea54cf2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7324896a-f603-4006-871b-2b0d40b331a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1e455dc-60d6-416a-a639-3a24ea54cf2a",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "fca2eb69-9130-4af7-ba4b-d8a74f7e0bd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "4b90b27f-7b22-4588-a0e6-4feda1cff359",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fca2eb69-9130-4af7-ba4b-d8a74f7e0bd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee46e5cc-164a-44ff-9a0a-8da87ac21205",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fca2eb69-9130-4af7-ba4b-d8a74f7e0bd8",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "37c4aaed-5780-4867-8c05-65fe96c2f30b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "13b1f7d9-ad53-47f8-a584-33ba09963b69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37c4aaed-5780-4867-8c05-65fe96c2f30b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb9c25ee-9b64-47c2-8fda-6c87af9b4062",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37c4aaed-5780-4867-8c05-65fe96c2f30b",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "9a80c619-179b-4a7c-9fe9-2b475e94cdbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "97849888-32cd-4d7d-bc0d-d18d3d5b95c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a80c619-179b-4a7c-9fe9-2b475e94cdbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83f2569a-da7e-48b3-bee6-44f5f3e37ac0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a80c619-179b-4a7c-9fe9-2b475e94cdbf",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "6ce2973a-5ba5-4fee-af68-1d2ca34e42cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "4c55d7e9-f47e-47bc-9e95-72b0ab5d4ee7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ce2973a-5ba5-4fee-af68-1d2ca34e42cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "148b1fe7-2b66-417e-a8ed-57d291138a29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ce2973a-5ba5-4fee-af68-1d2ca34e42cf",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "68d8e688-20c2-4d16-b5ac-84a9d2d4cee4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "ade2598f-9141-4a86-a300-325d7a957380",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68d8e688-20c2-4d16-b5ac-84a9d2d4cee4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee5fbc17-5a6a-4bf4-8c4b-2a197a8143e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68d8e688-20c2-4d16-b5ac-84a9d2d4cee4",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "22e35958-dd5e-46d8-a2aa-a86372297c5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "388278f3-15f8-4b68-87dc-fca118ffe702",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22e35958-dd5e-46d8-a2aa-a86372297c5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a842d7eb-110c-4c81-96cf-4370e6fd1184",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22e35958-dd5e-46d8-a2aa-a86372297c5e",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "edcc3265-b278-42a1-8652-d950956952d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "54b16b35-ee44-47c8-b414-3cf18953812d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edcc3265-b278-42a1-8652-d950956952d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "118e3d40-f20f-4842-a58e-3591791f4759",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edcc3265-b278-42a1-8652-d950956952d9",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "aa14991c-b1ff-4eed-b718-c5ba656eb26f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "7c870a06-1714-4407-9f67-231e5ef9e3e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa14991c-b1ff-4eed-b718-c5ba656eb26f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f0b7bdd-e155-489b-9250-731e9b56c6d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa14991c-b1ff-4eed-b718-c5ba656eb26f",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "6b509052-8e5d-4556-9ddf-c3c7ba42ecbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "affaa111-b8de-42d9-985f-5117115c462e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b509052-8e5d-4556-9ddf-c3c7ba42ecbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f0d678f-6084-4ceb-931c-167b2160b193",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b509052-8e5d-4556-9ddf-c3c7ba42ecbf",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "eab75233-ac03-4c7d-87da-b085878d1511",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "d98d474c-e6d5-4413-b7a0-1b5c7ac53282",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eab75233-ac03-4c7d-87da-b085878d1511",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd35f2b8-9560-452c-9799-b83ab77b7dce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eab75233-ac03-4c7d-87da-b085878d1511",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "d210e905-2be4-47b2-b506-36d58b6a3945",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "156d23ee-94ff-43a1-a3f8-0e943dcad6d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d210e905-2be4-47b2-b506-36d58b6a3945",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d1fcd3d-14ee-40fe-91b1-713bf5cbadf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d210e905-2be4-47b2-b506-36d58b6a3945",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "afdbf6bb-1d20-4e1d-bcbd-709b612366b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "44f9f34e-2385-4a03-83c3-bf49d3b029a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afdbf6bb-1d20-4e1d-bcbd-709b612366b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "953d28c4-aa40-4375-8706-300d9134ede6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afdbf6bb-1d20-4e1d-bcbd-709b612366b4",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "647ba2c6-d524-4d32-bc79-a4998124b805",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "46d96a07-3bf0-48c6-94c3-3ca12ee515c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "647ba2c6-d524-4d32-bc79-a4998124b805",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ceb68ed2-2c0c-47b0-a611-b4edfa080be0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "647ba2c6-d524-4d32-bc79-a4998124b805",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "632e3ec4-9644-4a62-88c7-f0d5cf8a34e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "140e3996-1eeb-42de-a319-86ed0b921a7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "632e3ec4-9644-4a62-88c7-f0d5cf8a34e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18e14f32-eecd-49bc-b84e-fd5db4dc4fce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "632e3ec4-9644-4a62-88c7-f0d5cf8a34e5",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "6dea1f6e-f2e4-4c5b-8e87-e06368019b09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "c4b80319-5f5d-448e-8e99-3e70196c410e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dea1f6e-f2e4-4c5b-8e87-e06368019b09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b46a31fd-31de-4daa-90c0-48bf4247e4d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dea1f6e-f2e4-4c5b-8e87-e06368019b09",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "b7303c1d-c6d8-4238-8fd2-fca38e3ec1f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "5b2d5125-f5ec-4c5d-84f9-f6ae9fda0a84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7303c1d-c6d8-4238-8fd2-fca38e3ec1f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "905938ee-d838-47e2-9f2d-a09aec60bed8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7303c1d-c6d8-4238-8fd2-fca38e3ec1f1",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "8b0404ac-6328-40ac-882f-4c78f62f3fd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "118f62de-f99c-4186-a415-a19ee5457ea4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b0404ac-6328-40ac-882f-4c78f62f3fd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "639f57f4-51a0-4457-9f54-b0f14b9b7321",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b0404ac-6328-40ac-882f-4c78f62f3fd6",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "9a5a2deb-2558-47cf-9896-0ccb6bd4de36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "703bf4f3-c069-4af9-aef8-61c606583acb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a5a2deb-2558-47cf-9896-0ccb6bd4de36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8186fd0-dfa9-4762-a586-e6ff5c5120f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a5a2deb-2558-47cf-9896-0ccb6bd4de36",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "56718324-38a8-47f7-abc8-e3b352de8942",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "2f50b1e9-97fc-4ffd-9a46-6baf6bebccf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56718324-38a8-47f7-abc8-e3b352de8942",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9616b5fd-d486-49ef-88db-f76927e03a1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56718324-38a8-47f7-abc8-e3b352de8942",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        },
        {
            "id": "a8d86fe3-0fb9-4009-b0cb-bd2b55996637",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "compositeImage": {
                "id": "f2584645-de86-47fd-b23c-bd6648d40b13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8d86fe3-0fb9-4009-b0cb-bd2b55996637",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d71f16b-c358-4761-9c42-8e02a12e6268",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8d86fe3-0fb9-4009-b0cb-bd2b55996637",
                    "LayerId": "95b6a21d-7911-4209-b971-80f3224b0377"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "95b6a21d-7911-4209-b971-80f3224b0377",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "849c4571-5007-4a27-8817-2536a78dfb97",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}