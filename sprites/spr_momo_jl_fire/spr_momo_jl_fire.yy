{
    "id": "0f810894-88af-49bf-9a73-79e038b4c6f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_jl_fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "16a6405d-771d-42a2-85eb-1e3502a0808d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f810894-88af-49bf-9a73-79e038b4c6f4",
            "compositeImage": {
                "id": "bafd3a01-cd84-40a0-abfb-5b0381472830",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16a6405d-771d-42a2-85eb-1e3502a0808d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5c974c1-cc49-4e43-84cc-c81c5d3546c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16a6405d-771d-42a2-85eb-1e3502a0808d",
                    "LayerId": "6ff82135-74f1-48a5-9f0c-5c0a47adb8dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6ff82135-74f1-48a5-9f0c-5c0a47adb8dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f810894-88af-49bf-9a73-79e038b4c6f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}