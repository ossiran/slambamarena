if(state_new == 1)
{
	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -42;
	    xscale  = 26;
	    yscale  = 74;
	    xshift  = -2;
	}
	
			with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 16;
	    image_yscale  = 47;
	    xshift = -2
	}
	
	attackframe0 = 2;
	attackframe1 = 5;
	attackframe2 = 17;
	attackframe3 = 10;
	attackframe4 = 18;
	endstate = 64;
	
	exmove = 1;

	vx = 0;
	vy = 0;
	
	ex += -50;
}

sprite_index = spr_bill_cspecial;

//initial hitbox
if(state_timer == attackframe0)
{
    hitbox0 = create_hitbox(2 ,-55,35,76,4,-12,12,12,14,10,-1,0,3,0,"mid",-1);   
    hitbox0.hitsound = snd_hit;
    hitbox0.scaling = 0.9;
    vx = 4 * image_xscale;
    hitbox0.juggle = 0;
}

//begin movement up
if(state_timer > attackframe1 && state_timer < attackframe2 && otherplayer.hitpause == 0)
{
    vy = -7;
    vx = 3.5  * image_xscale ;
}

//2nd hitbox
if(state_timer == attackframe3 && !hitpausing())
{
    hitbox2 = create_hitbox(4,-55,26,75,4,-7,12,4,12,10,-1,0,13,0,"mid",-1); 
    hitbox2.scaling = 0.9;
    hitbox2.hitsound = snd_hit;
    hitbox2.juggle = 0;
}

//3rd and final hitbox
if(state_timer == attackframe4 && !hitpausing())
{
    hitbox3 = create_hitbox(4,-55,26,75,4,-6,12,4,12,10,-1,0,13,0,"mid",-1); 
    hitbox3.scaling = 0.9;
    hitbox3.hitsound = snd_hit;
    hitbox3.juggle = 0;
}

//attackbase. changes to air if not on ground
if(state_timer > attackframe1 + 4)
{
    attack_base(endstate,1,0,0,1,0);
}
else
{
    attack_base(endstate,0,0,0,1,0);
}

if(state_timer == attackframe2 + 1 && !hitpausing())
{
    vy = 0;
}

if(state_timer > attackframe4 && !onground() && !hitpausing())
{
    vx = 1 * image_xscale;
    vy += -0.25;
}


