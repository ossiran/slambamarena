{
    "id": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lsyn_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 110,
    "bbox_left": 53,
    "bbox_right": 75,
    "bbox_top": 44,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a860550c-5e4f-4db6-84b4-0a3a12e221bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "cf67b215-7c5a-4f72-b4ed-dac699ada6d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a860550c-5e4f-4db6-84b4-0a3a12e221bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5216e3f9-0ceb-4065-bd80-d6c17f3cf727",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a860550c-5e4f-4db6-84b4-0a3a12e221bf",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "bbc1e502-ecc4-436c-a328-93d7e00f85f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "32598be1-44a1-4d5e-b889-39e662719eb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbc1e502-ecc4-436c-a328-93d7e00f85f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1944dfb3-b4f2-4d31-8fdb-3d9422ad66aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbc1e502-ecc4-436c-a328-93d7e00f85f9",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "4f3e463a-6dda-42fd-8164-90afee77d976",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "0351424f-729a-4f58-b917-33edfb70eb29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f3e463a-6dda-42fd-8164-90afee77d976",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "601c90aa-bcab-4080-b107-f39de6865c85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f3e463a-6dda-42fd-8164-90afee77d976",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "8953d280-5b86-4f46-ac31-2ca1481f77cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "271c419a-0b18-46d0-8956-8bfac5f8d928",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8953d280-5b86-4f46-ac31-2ca1481f77cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9517047d-0361-4c7c-95b7-7e82f6b93f1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8953d280-5b86-4f46-ac31-2ca1481f77cf",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "7d80c61f-9f05-4837-9842-d0ef6ddfa733",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "dd513d94-8ff4-46f3-9bc1-3272e3ae37ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d80c61f-9f05-4837-9842-d0ef6ddfa733",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "754e4667-97e6-4983-8f9c-fe783d8eca5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d80c61f-9f05-4837-9842-d0ef6ddfa733",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "99597daa-881b-4156-bbb4-ce8805be84fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "2174c217-ea12-45a5-a9a3-dffbf8445b87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99597daa-881b-4156-bbb4-ce8805be84fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "158d257c-3df9-4a75-952a-bf52823ecf5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99597daa-881b-4156-bbb4-ce8805be84fe",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "fda0d632-46e8-4f73-996b-bca544b219ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "c5dd1baf-cf23-45e3-ae5e-3bb89610c993",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fda0d632-46e8-4f73-996b-bca544b219ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab1678dd-163b-44a9-9ec3-a501183000d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fda0d632-46e8-4f73-996b-bca544b219ef",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "013a8586-c1c8-4e04-9ec7-e2d9956f444e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "4c652803-bf0d-4dc6-89ca-c2dfed9adb44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "013a8586-c1c8-4e04-9ec7-e2d9956f444e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "348cec2e-8133-4d02-a964-18503ee5a906",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "013a8586-c1c8-4e04-9ec7-e2d9956f444e",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "9d91c19c-4363-4740-b407-1e8e5ce94b9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "d473dbaf-c157-444b-915c-5e764cb4f75e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d91c19c-4363-4740-b407-1e8e5ce94b9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "088ab917-5715-4e6f-a5c7-33d21cb36557",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d91c19c-4363-4740-b407-1e8e5ce94b9f",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "a50c04e0-78c0-4531-bc6f-4a2bc6af1506",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "ee6c4f48-3bd2-414c-8db4-8e278fc54894",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a50c04e0-78c0-4531-bc6f-4a2bc6af1506",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "789c0797-c6b5-41ee-8fca-39936e913d70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a50c04e0-78c0-4531-bc6f-4a2bc6af1506",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "f87583f3-eb13-4b41-919a-c3c0f31caf17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "c896f7a0-9b76-4494-8216-f8ac4da861c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f87583f3-eb13-4b41-919a-c3c0f31caf17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f78f5975-cc71-413e-86f6-75052745b8c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f87583f3-eb13-4b41-919a-c3c0f31caf17",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "51b78331-9922-46a9-a540-b54cefb99fec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "903caf89-90d7-4826-9ae8-49790e78c1b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51b78331-9922-46a9-a540-b54cefb99fec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b5907c5-7923-42c3-a85d-d18477b41475",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51b78331-9922-46a9-a540-b54cefb99fec",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "ede1802f-c625-4643-8b16-c1f0c66dcb18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "7d332e3f-986c-4fe4-8af4-b055dd74e2e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ede1802f-c625-4643-8b16-c1f0c66dcb18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81a1e333-5f82-44f3-8c09-c1e514dd5376",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ede1802f-c625-4643-8b16-c1f0c66dcb18",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "3c7842da-0954-4bb4-bec4-2e6d524edae4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "365aa5a4-f313-4df3-9384-09ff2cfe1208",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c7842da-0954-4bb4-bec4-2e6d524edae4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c9d3a7e-3888-45b9-8fff-1c2e648f35fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c7842da-0954-4bb4-bec4-2e6d524edae4",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "a702bc33-f2bf-468c-bd1a-f6a47ce2f2eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "f0662a12-5be9-4a2d-a5ad-81a8ef1559ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a702bc33-f2bf-468c-bd1a-f6a47ce2f2eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e20724e-5a6a-4fc6-a734-91025393aab8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a702bc33-f2bf-468c-bd1a-f6a47ce2f2eb",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "93560d29-599d-48d9-93b7-5140e6d3c196",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "eb4d9950-c1b3-4f6b-a6fb-519cb3246868",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93560d29-599d-48d9-93b7-5140e6d3c196",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bedde89-4eb5-4a54-8778-44ab8319e736",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93560d29-599d-48d9-93b7-5140e6d3c196",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "c85885f2-ec82-4d3a-bb7c-9a6672eb100d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "c9929789-fd76-4b89-a57b-7821fac2aa81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c85885f2-ec82-4d3a-bb7c-9a6672eb100d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3963856-8b03-467f-aa11-679fe0ff1d84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c85885f2-ec82-4d3a-bb7c-9a6672eb100d",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "4ccd1cf6-db25-4fdb-b363-205b4991b22f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "271f9dd8-527d-4135-a49c-18622363e474",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ccd1cf6-db25-4fdb-b363-205b4991b22f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "370ef805-a2b2-49ae-9967-de4646eb93af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ccd1cf6-db25-4fdb-b363-205b4991b22f",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "0ee6cc8e-06ea-4187-9d8a-d40af6c9bf15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "11e0f1b1-2489-4d88-9add-e70236b80a48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ee6cc8e-06ea-4187-9d8a-d40af6c9bf15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af351aa4-73a0-42e4-9790-49d308a29760",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ee6cc8e-06ea-4187-9d8a-d40af6c9bf15",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "7de6bb8a-c049-4cda-8ef7-c6859ea27dc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "34eaa096-311a-4a19-a4fc-c646aaa90309",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7de6bb8a-c049-4cda-8ef7-c6859ea27dc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e185a00e-99d8-462d-b995-87aa8a91ea21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7de6bb8a-c049-4cda-8ef7-c6859ea27dc3",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "3264ee08-b638-42df-93f1-3dbdaa15666c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "681f6dcd-4a11-4422-8abe-278d2c00a202",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3264ee08-b638-42df-93f1-3dbdaa15666c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "deb1e4ce-110e-41ce-b366-eaa4c8326b36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3264ee08-b638-42df-93f1-3dbdaa15666c",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "6b8d5e06-6a66-4ee0-853f-2f4127af188b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "056feb1f-49b0-4a1e-a211-397531448587",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b8d5e06-6a66-4ee0-853f-2f4127af188b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c941017-b61b-4f20-9fc9-a51c54741239",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b8d5e06-6a66-4ee0-853f-2f4127af188b",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "3250c159-d1a7-4aed-927a-709456e5edd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "afbef526-0073-4d05-a636-d02743a32092",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3250c159-d1a7-4aed-927a-709456e5edd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29ad87a7-aeca-4661-af34-a00017ab3356",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3250c159-d1a7-4aed-927a-709456e5edd5",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "85494cc3-61fa-465f-ae90-137b8003724e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "3850e760-be64-491f-a7ea-9a672d40b905",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85494cc3-61fa-465f-ae90-137b8003724e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98926d39-5bca-4ef5-a2d7-0f709cf1d29e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85494cc3-61fa-465f-ae90-137b8003724e",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "b8a03eb9-cc76-4439-ab8b-e2719d36c594",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "a6812cc7-00b0-469c-bdde-906feec6b69d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8a03eb9-cc76-4439-ab8b-e2719d36c594",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91941aa9-8378-439e-b608-127cde95dc4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8a03eb9-cc76-4439-ab8b-e2719d36c594",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "0d93f801-4834-498a-bdd6-96100d1f43e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "23288a73-37d6-4d3e-90ef-c442433de8ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d93f801-4834-498a-bdd6-96100d1f43e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d76ab6c1-f8c0-46e8-89d9-4d318507418e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d93f801-4834-498a-bdd6-96100d1f43e9",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "249d2bc1-029e-4fa3-be42-0570e412a1d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "cb9d55f5-0596-4e13-93a0-312f48341277",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "249d2bc1-029e-4fa3-be42-0570e412a1d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca13476f-52cc-4433-8482-7f4d07464ec6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "249d2bc1-029e-4fa3-be42-0570e412a1d7",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "0e2b0c08-1140-4bda-a1dc-d0cef487c349",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "ba558a8b-522a-4413-af02-270140f0bb39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e2b0c08-1140-4bda-a1dc-d0cef487c349",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47bad494-7ac0-48c4-be6c-9e7744a9ea5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e2b0c08-1140-4bda-a1dc-d0cef487c349",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "4cac7b9a-8506-42d6-a8d2-d6546279b267",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "4e8f2081-6b72-4f16-a1b0-b024664e7c4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cac7b9a-8506-42d6-a8d2-d6546279b267",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20c380e3-35c6-4756-b02a-cffd871d7c99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cac7b9a-8506-42d6-a8d2-d6546279b267",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "2736581d-f4a7-48d7-86b0-08237a58b393",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "8c043892-6a4c-4fb6-8a83-e0fe125960cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2736581d-f4a7-48d7-86b0-08237a58b393",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c55ef4a-0e8b-427f-a9d1-adf1ea494c28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2736581d-f4a7-48d7-86b0-08237a58b393",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "c0ca5dd7-62fb-4bdf-a7fe-969a4cb0d2ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "b0818618-be8d-4bb1-8f3f-7132b759dca0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0ca5dd7-62fb-4bdf-a7fe-969a4cb0d2ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7679d6b3-cacf-40b2-957a-bbd6399353f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0ca5dd7-62fb-4bdf-a7fe-969a4cb0d2ac",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "6e4ecea7-4d35-4e18-82f8-46de5b36a58b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "ebe5b43f-145a-4f0a-9168-39907092865c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e4ecea7-4d35-4e18-82f8-46de5b36a58b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a9fcc78-cc8d-47d5-8239-51e1e3d58d5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e4ecea7-4d35-4e18-82f8-46de5b36a58b",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "76248efb-4ae1-4a0a-af95-18b5cab82591",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "41a85b2a-5709-47aa-8050-0eaf2451074b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76248efb-4ae1-4a0a-af95-18b5cab82591",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9254094a-6ce2-4692-9d6d-4a201293dfbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76248efb-4ae1-4a0a-af95-18b5cab82591",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "ba96710a-1b97-439a-aac7-b4890523a132",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "0aac0395-f638-4fe4-956d-e44ff810d5f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba96710a-1b97-439a-aac7-b4890523a132",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60326d17-8792-4616-acd9-223d3e29a6e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba96710a-1b97-439a-aac7-b4890523a132",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "baebb4c4-1fd6-44c5-8355-4288184a7f02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "a4e2be02-79a0-4027-97a2-6703604bd21c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "baebb4c4-1fd6-44c5-8355-4288184a7f02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86cc3f67-f5dc-4190-b55e-d3aa66bf2eba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "baebb4c4-1fd6-44c5-8355-4288184a7f02",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "d857d2d8-629a-4ce8-946a-b359ab32efea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "f56a7182-99f8-4c56-992a-1d03be3cde34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d857d2d8-629a-4ce8-946a-b359ab32efea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05b41316-1994-45f7-8664-73b1dbd2a1e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d857d2d8-629a-4ce8-946a-b359ab32efea",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "e8ddfdf1-4e55-48ac-8131-5acbc685e088",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "66ae6f05-4021-407c-851f-67537d849bac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8ddfdf1-4e55-48ac-8131-5acbc685e088",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab793bc0-1476-4e77-8844-2122ebb24010",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8ddfdf1-4e55-48ac-8131-5acbc685e088",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "a6891ed8-73c5-4cc5-8b0a-e985fa461a27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "96fcf3a6-611c-471e-b2d0-81a9910aed96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6891ed8-73c5-4cc5-8b0a-e985fa461a27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc79de46-18a8-4d75-80a6-91f3d4fae701",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6891ed8-73c5-4cc5-8b0a-e985fa461a27",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "02fa177d-f26b-431b-a98a-28deab3838cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "cce7c833-7d37-4378-8e43-2229a89f4adc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02fa177d-f26b-431b-a98a-28deab3838cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04007ca7-99cc-43fe-af1d-1b96cfcc7509",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02fa177d-f26b-431b-a98a-28deab3838cc",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "b7602102-423f-48c5-805d-fb5b9e3d9d68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "aa6240eb-180f-4971-88c3-14ca46e8b667",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7602102-423f-48c5-805d-fb5b9e3d9d68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82e9d192-c5f1-4273-9c08-6af1730d0d1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7602102-423f-48c5-805d-fb5b9e3d9d68",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "f606c0da-5c62-468e-b6d2-25c9f59c07b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "95eb877b-75ae-4e5a-9426-e01e367b5100",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f606c0da-5c62-468e-b6d2-25c9f59c07b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76248379-821c-4ac4-9894-7ab5a1e116e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f606c0da-5c62-468e-b6d2-25c9f59c07b1",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "53ef21da-d662-4c53-a939-7a62d69d4d5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "9a977473-8c6c-40e4-8714-3225a10c5942",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53ef21da-d662-4c53-a939-7a62d69d4d5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3264c1b-fc25-4603-9ab3-3d174c721d46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53ef21da-d662-4c53-a939-7a62d69d4d5e",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "ef1a5537-5d15-45c2-b905-cd1008eb7a9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "b4e70c39-e124-41f2-ab1d-57f4c010b279",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef1a5537-5d15-45c2-b905-cd1008eb7a9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67a95f91-075f-4f20-95f9-29e2e2051050",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef1a5537-5d15-45c2-b905-cd1008eb7a9b",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "a776918a-0f9b-4750-af3c-114b5c753d67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "bb9ddea6-5aa3-413b-8b6e-b62c20a545d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a776918a-0f9b-4750-af3c-114b5c753d67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a0ed5e3-189c-4006-a904-966b92b101df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a776918a-0f9b-4750-af3c-114b5c753d67",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "a2416f2c-169a-441b-a328-938e25e2c4bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "b6bf77bb-6454-4cf1-8f46-ac45e2542e6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2416f2c-169a-441b-a328-938e25e2c4bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7c22900-2b66-4b0d-8f5c-e5ea0c5870c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2416f2c-169a-441b-a328-938e25e2c4bf",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "fe3849e1-f259-4bd8-9988-00320e3f05bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "84e1c01a-a5e7-461e-a698-0d4d915e748c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe3849e1-f259-4bd8-9988-00320e3f05bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53487f8e-15ae-4f4c-8539-a944c3d10157",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe3849e1-f259-4bd8-9988-00320e3f05bb",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "00d0a685-d26f-4141-9cde-98d830689fa7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "609643ab-e3cd-4b07-ae1a-c8bd55aabd13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00d0a685-d26f-4141-9cde-98d830689fa7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1626050f-1c69-4e00-97a2-e6b200a12592",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00d0a685-d26f-4141-9cde-98d830689fa7",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "7bbd7124-9053-4700-9ebd-4f549660a679",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "357f85bd-1426-4c70-8efa-848f6a1e4793",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bbd7124-9053-4700-9ebd-4f549660a679",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b6c1d21-5c61-4a66-9460-38e0915d210b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bbd7124-9053-4700-9ebd-4f549660a679",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "1662596a-4a84-4a3c-812d-60a01d77263a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "427b9b1c-98d5-450f-a9ee-c8bd51ec201d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1662596a-4a84-4a3c-812d-60a01d77263a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fed71e6c-734a-4fb4-816e-3a60c0aa551f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1662596a-4a84-4a3c-812d-60a01d77263a",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "fe4583d3-a480-4020-91ac-9cc9cd4ef66b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "484c8e6d-a7bd-4c6c-aaab-0048e92c40e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe4583d3-a480-4020-91ac-9cc9cd4ef66b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fe45f03-e781-41c4-b0dc-bc6739334691",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe4583d3-a480-4020-91ac-9cc9cd4ef66b",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "656e60af-0675-4fef-aa20-a27fecab05f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "0e49525b-9e17-4607-81b2-1124c3e3650b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "656e60af-0675-4fef-aa20-a27fecab05f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5249a567-2fec-4db9-b769-e8113a4ceef9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "656e60af-0675-4fef-aa20-a27fecab05f5",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "971f02d1-d7ee-4bc1-8a73-1e1608e67ab7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "2e73f06a-ebe5-46ad-9722-5ee1f3dc91c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "971f02d1-d7ee-4bc1-8a73-1e1608e67ab7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "391c643f-dc17-4164-b3ad-a53c3cb7eced",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "971f02d1-d7ee-4bc1-8a73-1e1608e67ab7",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "13f86299-3588-4b08-a471-9fc45a26618d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "fae8e6e6-e2da-45a2-a47a-abbc6f310641",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13f86299-3588-4b08-a471-9fc45a26618d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5d3cab4-145b-4077-8ebe-c82800164eb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13f86299-3588-4b08-a471-9fc45a26618d",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "9c93dbbc-f73c-463a-a499-bc3458d3547e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "db5955ab-80e5-471c-a660-3f3da532c1e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c93dbbc-f73c-463a-a499-bc3458d3547e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "123f5038-5688-49fb-9958-1808fea9a62b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c93dbbc-f73c-463a-a499-bc3458d3547e",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        },
        {
            "id": "5d793eea-cb2f-4175-a40b-2fbbc6dbc279",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "compositeImage": {
                "id": "731e7ef7-70e3-4252-a211-25940878c4f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d793eea-cb2f-4175-a40b-2fbbc6dbc279",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afaa93bd-6767-450e-add7-65e10950ca94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d793eea-cb2f-4175-a40b-2fbbc6dbc279",
                    "LayerId": "e82fe4c9-b388-4442-9cb8-d88803fc577d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "e82fe4c9-b388-4442-9cb8-d88803fc577d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}