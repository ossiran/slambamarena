{
    "id": "dad2e451-9e88-45f1-8bf4-0dd685c86f9f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dsyn_airn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 50,
    "bbox_right": 82,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ca6bd2f-d4c1-47c0-959b-5c7a5bc16a2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dad2e451-9e88-45f1-8bf4-0dd685c86f9f",
            "compositeImage": {
                "id": "193ec446-0392-4dd8-9a89-04461b76156d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ca6bd2f-d4c1-47c0-959b-5c7a5bc16a2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47ae19c7-eb96-4130-b940-035345670edc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ca6bd2f-d4c1-47c0-959b-5c7a5bc16a2b",
                    "LayerId": "1dfcefb6-e79c-4899-802e-90db1ee9cb79"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "1dfcefb6-e79c-4899-802e-90db1ee9cb79",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dad2e451-9e88-45f1-8bf4-0dd685c86f9f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}