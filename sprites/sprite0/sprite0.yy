{
    "id": "3f62b208-ca71-4621-a1a1-3226fbe28853",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7d750e1-55c1-40d6-b2df-33474a380b0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f62b208-ca71-4621-a1a1-3226fbe28853",
            "compositeImage": {
                "id": "7ce3dfdf-023d-4dae-ad4b-a6b7dbfa401c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7d750e1-55c1-40d6-b2df-33474a380b0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9ca57be-b75d-4e9e-a3e7-dc162efd8f19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7d750e1-55c1-40d6-b2df-33474a380b0e",
                    "LayerId": "f5af7454-b8df-4ce6-b4c8-a9c8ebd147f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f5af7454-b8df-4ce6-b4c8-a9c8ebd147f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f62b208-ca71-4621-a1a1-3226fbe28853",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}