/// @description Insert description here
// You can write your code in this editor
//hitbox system
state_update();

if(keyboard_check_pressed(vk_numpad1) && player1 !=0 && player2 != 0)
{
    player1.hp = player1.hpmax;
}
if(keyboard_check_pressed(vk_numpad2) && player1 !=0 && player2 != 0)
{
    player2.hp = player2.hpmax;
}

if(keyboard_check_pressed(vk_numpad4) && player1 !=0 && player2 != 0)
{
    player1.ex = 150;
}

if(keyboard_check_pressed(vk_numpad5) && player1 !=0 && player2 != 0)
{
    player2.ex = 150;
}

if(keyboard_check_pressed(vk_numpad3) && player1 !=0 && player2 != 0)
{
    if(showinvis == 1)
    {
        showinvis = 0;
    }
    else
    {
        showinvis = 1;
    }
}

with(obj_player_parent)
{
    var vxNew, vyNew;
    
    /*
    // Handle sub-pixel movement
    cx += vx;
    cy += vy;
    vxNew = round(cx);
    vyNew = round(cy);
    cx -= vxNew;
    cy -= vyNew;
    
    */
    
    /*
    // Vertical
    repeat(abs(vyNew)) {
        if (!place_meeting(x, y + sign(vyNew), obj_solid))
            y += sign(vyNew); 
        else {
            vy = 0;
            break;
        }
    }
    
    // Horizontal
    repeat(abs(vxNew)) {
    
        // Move up slope
        if (place_meeting(x + sign(vxNew), y, obj_solid) && !place_meeting(x + sign(vxNew), y - 1, obj_solid))
            --y;
        
        // Move down slope
        if (!place_meeting(x + sign(vxNew), y, obj_solid) && !place_meeting(x + sign(vxNew), y + 1, obj_solid) && place_meeting(x + sign(vxNew), y + 2, obj_solid))
            ++y; 
    
        if (!place_meeting(x + sign(vxNew), y, obj_solid))
        {
                        x += sign(vxNew); 
        }
        else 
        {
            vx = 0;
            break;
        }
    }
    */
    
    /*
    //bbox variables
    var sprite_bbox_top = sprite_get_bbox_top(mask_index) - sprite_get_yoffset(mask_index);
    var sprite_bbox_left = sprite_get_bbox_left(mask_index) - sprite_get_xoffset(mask_index);
    var sprite_bbox_right = sprite_get_bbox_right(mask_index) - sprite_get_xoffset(mask_index);
    var sprite_bbox_bottom = sprite_get_bbox_bottom(mask_index) - sprite_get_yoffset(mask_index);
    
    
        y += vy;
        //Snap
        if (place_meeting(x,y + sign(vy),obj_solid) && vy > 0)
        {
            var wall1 = instance_place(x,y+sign(vy),obj_solid);
            if (y > 736)
            { //top
                y = (wall1.bbox_top -  1) - sprite_bbox_bottom;
                        vy = 0;
            } 
    
        }
        
        x += vx;
        //Snap
        if (place_meeting(x + sign(vx),y,obj_solid) && !place_meeting(x,y + sign(vy),obj_solid))
        {
            var wall1 = instance_place(x + sign(vx),y,obj_solid);
            if (x > 700)
            { //right
                x = (wall1.bbox_left -  1) - sprite_bbox_right;
                        vx = 0;
            } 
            else if(x < 100)
            { //left
                x = (wall1.bbox_right + 1) - sprite_bbox_left;
                        vx = 0;
            }
    
        }
    */
    
    
    
    
    
    /*
    //collisionbox
    if(collisionbox != -1)
    {
    
        
    
    
    	    with(obj_collisionbox)
        {
    
    		x = owner.x + xoffset + owner.vx;
            y = owner.y + yoffset + owner.vy;	
            
    
                    with(obj_collisionbox)
                    {
                        if(place_meeting(x + sign(owner.vx),y,obj_collisionbox) || place_meeting(x,y,obj_collisionbox))
                        {
                            with(owner)
                            {
    							if(onground())
    							{
    							    with(otherplayer)
    							    {
    							        if(onground())
    							        {
    											if(abs(vx) < abs(other.vx) && other.vy >= 0)
    											{
    											         x += 1 * frontOrBack + otherplayer.vx * 0.5;
    											}
    							        }
    							    }
    							}
    							else
    							{
    							    
        							    with(otherplayer)
        							    {
        							        if(!onground())
    							            {
        		                                x += 1 * frontOrBack + otherplayer.vx * 0.5;
    							            }
        							    }
    							}
    						}
                        }
                    }
        }
    }
    */
    
    
    
    
    
    

    
    repeat(abs(vx))
    {
        //if too far apart
        if(abs(x - otherplayer.x) > 350 && sign(vx) == frontOrBack)
        {
            vx = 0;
        }
           
        //makes sure collisionboxes arent colliding.     
        with(collisionbox)
        {
            if(!place_meeting(x + sign(owner.vx), y, obj_collisionbox))
            {
                with(owner)
                {
                    x += sign(vx);
                }
            }
            else
            {
                with(owner)
                {
                    if(onground())
                    {
                        with(otherplayer)
                        {
                            if(onground() && !place_meeting(x + 1, y, obj_solid) && !place_meeting(x - 1, y, obj_solid))
                            {
                                x += sign(otherplayer.vx);
                                with(otherplayer)
                                {
                                    x += sign(vx);
                                }
                            }
                        }
                    }
                    else
                    {
                        with(otherplayer)
                        {
                            if(onground())
                            {
                                with(other)
                                {
                                    x += sign(vx);
                                }
                            }
                            else
                            {
                                if(!place_meeting(x + 1, y, obj_solid) && !place_meeting(x - 1, y, obj_solid))
                                {
                                    x += sign(other.vx);
                                    with(other)
                                    {
                                        x += sign(vx);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    
    }
    
    //vertical
    repeat(abs(vy))
    {
        //pushes the player if they are above
        with(collisionbox)
        {
            if(!place_meeting(x, y + sign(owner.vy), obj_collisionbox))
            {
                with(owner)
                {
                    y += sign(vy);
                }
            }
            else
            {
                 with(owner)
                {
                    if(!onground() && vy >= 0)
                    {
                        with(otherplayer)
                        {
                            if(onground() )
                            {
                                with(otherplayer)
                                {
                                    y += sign(vy);
                                    x += frontOrBack;
                                }
                            }
                            else if(!onground())
                            {
                                with(otherplayer)
                                {
                                    y += sign(vy);
                                }
                            }
                        }
                    }
                    else
                    {
                        y += sign(vy);
                    }
                }
            }
        }
    }
    
    
    //collisions
    if(onground())
    {
        if(x < 81)
        {
            x = 81;
            vx = 0;
        }
        
        if(x > room_width - 81)
        {
            x = room_width - 81;
            vx = 0;
        }
    }
    else
    {
        if(x < 76)
        {
            x = 76;
            vx = 0;
        }
        
        if(x > room_width - 76)
        {
            x = room_width - 76;
            vx = 0;
        }
    }
    
    //collisions
    if(y > 737)
    {
        y = 737;
        if(vy > 0)
        {
            vy = 0;
        }
        /*
        if(yhit > 0)
        {
            yhit = 0;
        }
        */
    }
    
    if(instance_exists(obj_hurtbox))
    {
        with(obj_hurtbox)
        {
            //hurtbox follow
            if(following == -1)
            {
                x = owner.x + xoffset  + (owner.frontOrBack*xshift);
                y = owner.y + yoffset;
            }
            else
            {
                if(instance_exists(following))
                {
                    x = following.x + xoffset;
                    y = following.y + yoffset;
                }
                else
                {
                    instance_destroy();
                }
            }
            
            
        }
    }
    
    //grabbox
    if(instance_exists(obj_grabbox))
    {
        with(obj_grabbox)
        {
            x = owner.x + xoffset;
            y = owner.y + yoffset;
            
            
                with(obj_hurtbox)
                {
                    if(place_meeting(x,y,other) && other.owner != owner && other.grabbed == 0)
                    {
                        if(owner.y > 735)
                        {
                            if(owner.state_name != "jumpsquat" && owner.state_name != "knockdown")
                            {
                                owner.grabbed = 1;
                            }
                        }
                        else
                        {
                            with(owner.otherplayer)
                            {
                                if(!onground())
                                {
                                    with(otherplayer)
                                    {
                                        owner.grabbed = 1;
                                    }
                                }
                            }
                        }
                    }
                }
            
            
            /*
            with(obj_grabbox)
            {
                if(place_meeting(x,y,other) && other.owner != owner && other.hitted == 0)
                {
                    if(owner.y > 736)
                    {
                        if(owner.state_name != "jumpsquat")
                        {
                            owner.grabbed = -1;
                        }
                    }
                    else
                    {
                        with(otherplayer)
                        {
                            if(!onground())
                            {
                                with(otherplayer)
                                {
                                    owner.grabbed = -1;
                                }
                            }
                        }
                    }
                }
            }
            */
        }
    }
    
    //hitbox
    if(instance_exists(obj_hitbox))
    {
        with(obj_hitbox)
        {
            if(vx == 0 && vy == 0)
            {
                if(following == -1)
                {
                    x = owner.x + xoffset + (owner.frontOrBack*xshift);
                    y = owner.y + yoffset;
                    
                    //proxbox follow
                    prox.x = owner.x + xoffset  + (owner.frontOrBack*xshift);
                    prox.y = owner.y + yoffset;
                }
                else
                {
                    //this causes an issue with following and owner hitting at the same time
                    if(instance_exists(following))
                    {
                        x = following.x + xoffset + (following.owner.frontOrBack*xshift);
                        y = following.y + yoffset;
                        
                        //proxbox follow
                        prox.x = following.x + xoffset  + (following.owner.frontOrBack*xshift);
                        prox.y = following.y + yoffset;
                    }
                    else
                    {
                        instance_destroy();
                        instance_destroy(prox);
                    }
                }
            }
            else
            {
                prox.x = prox.owner.x + vx;
                prox.y = prox.owner.y + vy;
            }
            with(obj_proxbox)
            {
                with(obj_hurtbox)
                {
                    if(place_meeting(x,y,other) && other.owner.owner != owner)
                    {
                        with(owner)
                            {
                                if(onground())
                                {
                                    if(input_check(4,0,0,0,0) && (state_name == "stand" || state_name == "crouch" || state_name == "walk"))
                                    {
                                        blockstun = 4;
                                    }
                                    else if(input_check(1,0,0,0,0) && (state_name == "stand" || state_name == "crouch" || state_name == "walk"))
                                    {
                                        blockstun = 4;
                                    }
                                }
                                else
                                {
                                    if((input_check(4,0,0,0,0) || input_check(1,0,0,0,0) || input_check(7,0,0,0,0)) && state_name == "air")
                                    {
                                        blockstun = 4;
                                    }
                                }
                            }   
                    }
                }
            }
           
            with(obj_hurtbox)
            {
                if(place_meeting(x,y,other) && other.owner != owner && other.hitted == 0)
                {
                    with(owner)
                        {
                            if(onground())
                            {
                                if(input_check(4,0,0,0,0) && (state_name == "stand" || state_name == "crouch" || state_name == "walk"))
                                {
                                    state_switch("sblock");
                                }
                                else if(input_check(1,0,0,0,0) && (state_name == "stand" || state_name == "crouch" || state_name == "walk"))
                                {
                                    state_switch("cblock");
                                }
                            }
                            else
                            {
                                if((input_check(4,0,0,0,0) || input_check(1,0,0,0,0) || input_check(7,0,0,0,0)) && state_name == "air")
                                {
                                    state_switch("ablock");
                                }
                            }
                        }   
                        
                    if(owner.y > 736 && owner.hitair == 0)
                    {
                        //redo
                        
                        if(other.modifier != "low" && other.modifier != "projectilelow" && owner.state_name == "sblock" )
                        {
                                owner.blockstun = other.blockstun;
                                owner.hp += other.chipdamage;
                                owner.xhit  = other.xhit/1.5;
                                owner.otherplayer.vx += -other.xhit/1.5;
                                owner.ex += other.exgive/3;
                                
                                //marking all others in the group
                                    with(other)
                                    {

                                            hitted = 1;
                                        
                                        
                                        var groupmark = group;
                                        with(obj_hitbox)
                                        {
                                            if(modifier != -1 && group == groupmark && hitted = 0)
                                            {
                                                hitted = -1;
                                            }
                                        }
                                    }
                                other.owner.ex += (other.exgive/2.5);
                                owner.hitpause = round(other.hitpause/2);
                                audio_play_sound(snd_blocksound,1,0);
                                
                                if(other.modifier != "projectilemid" && other.modifier != "projectilelow" && other.modifier != "projectileoverhead")
                                {
                                    if(other.following == -1)
                                    {
                                        other.owner.hitting = 1;
                                    }
                                    else
                                    {
                                        other.following.hitting = 1;
                                    }
                                }
                        }
                        else if(other.modifier != "overhead" && other.modifier != "projectileoverhead" && owner.state_name == "cblock")
                        {
                                owner.blockstun = other.blockstun;
                                owner.hp += other.chipdamage;
                                owner.xhit  = other.xhit/1.5;
                                owner.ex += other.exgive/3;
                                
                                //marking all others in the group
                                    with(other)
                                    {

                                        hitted = 1;
                                        
                                        
                                        var groupmark = group;
                                        with(obj_hitbox)
                                        {
                                            if(modifier != -1 && group == groupmark && hitted = 0)
                                            {
                                                hitted = -1;
                                            }
                                        }
                                    }
                                other.owner.ex += (other.exgive/3);
                                owner.hitpause = round(other.hitpause/2);
                                audio_play_sound(snd_blocksound,1,0);
                                
                                if(other.modifier != "projectilemid" && other.modifier != "projectilelow" && other.modifier != "projectileoverhead")
                                {
                                    if(other.following == -1)
                                    {
                                        other.owner.hitting = 1;
                                    }
                                    else
                                    {
                                        other.following.hitting = 1;
                                    }
                                }
                        }
                        else
                        {
                                if(owner.state_name != "knockdown")
                                {
                                    owner.counterhit = 0;
                                    if(owner.state_name != "walk"  && owner.state_name != "stand" && owner.state_name != "fdash" && owner.state_name != "bdash" && owner.state_name != "crouch" && owner.state_name != "hitstun" && owner.state_name != "airdash" && owner.state_name != "air" && owner.state_name != "jumpsquat" && owner.state_name != "hitfall" && owner.state_name != "bounce" && owner.state_name != "grabbed"
                                    && owner.state_timer <= owner.attackframe0 && owner.state_timer <= owner.attackframe1 && owner.state_timer <= owner.attackframe2 && owner.state_timer <= onwer.attackframe3 && owner.state_timer <= owner.attackframe4 && owner.state_timer <= owner.attackframe5)
                                    {
                                        
                                        owner.counterhit = 1;
                                        other.juggle = 0;
                                        
                                        if(owner.player_num == 1)
                                        {
                                            obj_system.message_array_1[obj_system.message_array_count_1, 0] = "COUNTERHIT";
                                            obj_system.message_array_1[obj_system.message_array_count_1, 1] = 60;
                                            obj_system.message_array_count_1 ++;
                                        }
                                        else
                                        {
                                            obj_system.message_array_2[obj_system.message_array_count_2, 0] = "COUNTERHIT";
                                            obj_system.message_array_2[obj_system.message_array_count_2, 1] = 60;
                                            obj_system.message_array_count_2 ++;
                                        }
                                    }
                                    owner.scaling += other.scaling - owner.counterhit;
                                    //make hitstun decay on the ground
                                    owner.hitstun = other.hitstun  + (owner.counterhit * 0.25 * other.hitstun);
                                    owner.hitpause = other.hitpause + (owner.counterhit * 0.25 * other.hitpause);
                                    owner.hitfall = other.hitfall;
                                    owner.stagger = other.stagger + (other.counterstagger * owner.counterhit);
                                    owner.bounce = other.bounce;
                                    owner.xhit = other.xhit;
                                    owner.yhit = other.yhit + (owner.counterhit * 0.25 * other.yhit) + (other.counteryhit * owner.counterhit);
                                    owner.ex += abs(other.exgive) / 1.5;
                                    owner.hit = 1;
                                    owner.lastmod = other.modifier;
                                   
                                    
                                    //marking all others in the group
                                    with(other)
                                    {
  
                                        hitted = 1;
                                        
                                        
                                        
                                        var groupmark = group;
                                        with(obj_hitbox)
                                        {
                                            if(modifier != -1 && group == groupmark && hitted = 0)
                                            {
                                                hitted = -1;
                                            }
                                        }
                                    }
                                    //hitsound
                                    if(other.hitsound != 0)
                                    {
                                        audio_play_sound(other.hitsound,1,0);
                                    }
                                    
                                    /*
                                    //juggle
                                    var juggleadd = other.juggle;
                                       
                                    with(owner)
                                    {
                                        if(yhit < 0)
                                        {
                                           juggle += juggleadd;
                                        }
                                    }
                                    */
                                    
                                    owner.hitcounter += 1;
                                    
                                    if(owner.scaling > 2)
                                    {
                                        
                                        if(1.1 - (owner.scaling * 0.1) > 0.15)
                                        {
                                            owner.hp += (-other.damage * (1.1 - (owner.scaling * 0.1)));
                                            owner.combodamage += (other.damage * (1.1 - (owner.scaling * 0.1)));
                                            //scaling
                                            if(other.exgive > 0)
                                            {
                                            other.owner.ex += other.exgive * (1.2 - (owner.scaling * 0.1));
                                            }
                                        }
                                        else
                                        {
                                            owner.hp += -other.damage * 0.15;
                                            owner.combodamage += other.damage * 0.15;
                                            //scaling
                                            if(other.exgive > 0)
                                            {
                                                other.owner.ex += other.exgive * 0.35;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if(other.exgive > 0)
                                        {
                                            other.owner.ex += other.exgive;
                                        }
                                        owner.hp += -other.damage;
                                        owner.combodamage += other.damage;
                                    }
                                    
                                    
                                if(other.modifier != "projectilemid" && other.modifier != "projectilelow" && other.modifier != "projectileoverhead")
                                {
                                    if(other.following == -1)
                                    {
                                        other.owner.hitting = 1;
                                    }
                                    else
                                    {
                                        other.following.hitting = 1;
                                    }
                                }
                                    
                                    
                                }
                                //Off the ground
                                /*
                                else
                                {
                                    if(other.modifier != "high" && other.modifier != "overhead")
                                    {
                                        owner.hitstun = other.hitstun;
                                        owner.hitpause = other.hitpause;
                                        owner.hitfall = other.hitfall;
                                        owner.stagger = other.stagger;
                                        owner.bounce = other.bounce;
                                        owner.xhit = other.xhit;
                                        owner.yhit = other.yhit;
                                        owner.ex += other.exgive/2;
                                        owner.hit = 1;
                                        
                                        other.hitted = 1;
                                            //marking all others in the group
                                        with(other)
                                        {
                                            with(obj_hitbox)
                                            {
                                                if(modifier != -1 && modifier == other.modifier)
                                                {
                                                    hitted = 1;
                                                }
                                            }
                                        }
                                        other.owner.ex += other.exgive;
                                        //hitsound
                                        if(other.hitsound != 0)
                                        {
                                        audio_play_sound(other.hitsound,1,0);
                                        }
                                        
                                        owner.hitcounter += 1;
                                        //scaling
                                        if(owner.hitcounter > 2)
                                        {
                                            owner.scaling += other.scaling;
                                            
                                            if(1 - (owner.scaling * 0.1) > 0.35)
                                            {
                                                owner.hp += (-other.damage * (1 - (owner.scaling * 0.1)));
                                            }
                                            else
                                            {
                                                owner.hp += -other.damage * 0.35;
                                            }
                                        }
                                        else
                                        {
                                            owner.hp += -other.damage;
                                        }
                                        
                                        
                                        if(other.vx == 0 && other.vy == 0)
                                        {
                                            other.owner.hitting = 1;
                                        }
                                        
                                        owner.juggle += other.juggle;
                                        
                                        if(other.vx == 0 && other.vy == 0)
                                        {
                                            other.owner.hitting = 1;
                                        }
                                    }
                                }
                                */
        
                        }
                    }
                    else
                    {
                        //air hits
                        //air block
                        if(other.modifier != "antiair" && owner.state_name == "ablock")
                        {
                                owner.blockstun = other.blockstun;
                                owner.hp += other.chipdamage;
                                owner.xhit  = other.xhit;
                                
                                //marking all others in the group
                                    with(other)
                                    {

                                        hitted = 1;
                                        
                                        
                                        var groupmark = group;
                                        with(obj_hitbox)
                                        {
                                            if(modifier != -1 && group == groupmark && hitted = 0)
                                            {
                                                hitted = -1;
                                            }
                                        }
                                    }
                                
                                other.owner.ex += (other.exgive/2);
                                owner.hitpause = round(other.hitpause/2);
                                audio_play_sound(snd_blocksound,1,0);
                                
                                if(other.modifier != "projectilemid" && other.modifier != "projectilelow" && other.modifier != "projectileoverhead")
                                {
                                    if(other.following == -1)
                                    {
                                        other.owner.hitting = 1;
                                    }
                                    else
                                    {
                                        other.following.hitting = 1;
                                    }
                                }
                        }
                        else
                        {
                                    //air hit
                                    //scaling
                                    owner.counterhit = 0;
                                    if(owner.state_name != "walk"  && owner.state_name != "stand" && owner.state_name != "fdash" && owner.state_name != "bdash" && owner.state_name != "crouch" && owner.state_name != "hitstun" && owner.state_name != "airdash" && owner.state_name != "air" && owner.state_name != "jumpsquat" && owner.state_name != "hitfall" && owner.state_name != "bounce" && owner.state_name != "grabbed"
                                    && owner.state_timer <= owner.attackframe0 && owner.state_timer <= owner.attackframe1 && owner.state_timer <= owner.attackframe2 && owner.state_timer <= onwer.attackframe3 && owner.state_timer <= owner.attackframe4 && owner.state_timer <= owner.attackframe5)
                                    {
                                        owner.counterhit = 1;
                                        other.juggle = 0;
                                        
                                        if(owner.player_num == 1)
                                        {
                                            obj_system.message_array_1[obj_system.message_array_count_1, 0] = "COUNTERHIT";
                                            obj_system.message_array_1[obj_system.message_array_count_1, 1] = 60;
                                            obj_system.message_array_count_1 ++;
                                        }
                                        else
                                        {
                                            
                                            obj_system.message_array_2[obj_system.message_array_count_2, 0] = "COUNTERHIT";
                                            obj_system.message_array_2[obj_system.message_array_count_2, 1] = 60;
                                            obj_system.message_array_count_2 ++;
                                        }
                                    }
                                    owner.scaling += other.scaling - owner.counterhit;
                                    owner.hitstun = other.hitstun + other.airxtrahitstun + (owner.counterhit * 0.25 * (other.hitstun + other.airxtrahitstun));
                                    owner.hitpause = other.hitpause + (owner.counterhit * 0.25 * other.hitpause);
                                    owner.hitfall = other.hitfall;
                                    owner.stagger = other.stagger + (other.counterstagger * owner.counterhit);
                                    owner.bounce = other.bounce;
                                    
                                    owner.xhit = other.xhit + (other.airxtraxhit * sign(other.owner.image_xscale));
                                    owner.yhit = other.yhit + other.airxtrayhit + (owner.counterhit * 0.25 * (other.yhit + other.airxtrayhit)) + (other.counteryhit * owner.counterhit);
                                    owner.ex += abs(other.exgive) / 1.5;
                                    owner.hit = 1;
                                    owner.lastmod = other.modifier;
                                    owner.juggle += other.juggle;
                                    
                                    
                                    with(owner)
                                    {
                                        if(yhit == 0)
                                        {
                                            /*
                                            if( juggle > 1)
                                            {
                                                xhit = xhit * 0.75;
                                                yhit = -7;
                                                hitstun = floor( hitstun / 3 ) + 1;
                                                //hitfall += 1 + floor(juggle / 5);
                                            }
                                            else
                                            {
                                                xhit = xhit * 0.75;
                                                yhit = -10 ;
                                                //yhit = -13 + juggle * 3.5;
                                                hitstun = floor( hitstun / 3 ) + 1;
                                                //hitfall = 0;
                                            }
                                            */
                                            
                                            xhit = xhit * 0.75;
                                            yhit = -10 ;
                                            hitstun = floor( hitstun / 3 ) + 1;
                                        }
                                        /*
                                        else
                                        {
                                            if(juggle > 2)
                                            {
                                                yhit = yhit + floor(juggle/3);
                                                hitfall += hitfall + floor(juggle / 5);
                                            }
                                        }
                                        */
                                    }
                                    
                                    
                                    //marking all others in the group
                                    with(other)
                                    {

                                        hitted = 1;

                                        
                                        var groupmark = group;
                                        with(obj_hitbox)
                                        {
                                            if(modifier != -1 && group == groupmark && hitted = 0)
                                            {
                                                hitted = -1;
                                            }
                                        }
                                    }
                                    
                                    //hitsound
                                    if(other.hitsound != 0)
                                    {
                                    audio_play_sound(other.hitsound,1,0);
                                    }
                                    
                                    owner.hitcounter += 1;
                                    
                                    if(owner.scaling > 2)
                                    {
                                        
                                        if(1.1 - (owner.scaling * 0.1) > 0.15)
                                        {
                                            owner.hp += (-other.damage * (1.1 - (owner.scaling * 0.1)));
                                            owner.combodamage += (other.damage * (1.1 - (owner.scaling * 0.1)));
                                            //scaling
                                            if(other.exgive > 0)
                                            {
                                            other.owner.ex += other.exgive * (1.2 - (owner.scaling * 0.1));
                                            }
                                        }
                                        else
                                        {
                                            owner.hp += -other.damage * 0.15;
                                            owner.combodamage += other.damage * 0.15;
                                            //scaling
                                            if(other.exgive > 0)
                                            {
                                                other.owner.ex += other.exgive * 0.35;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if(other.exgive > 0)
                                        {
                                            other.owner.ex += other.exgive;
                                        }
                                        owner.hp += -other.damage;
                                        owner.combodamage += other.damage;
                                    }
                                    
                                    
                                    
                                    if(other.modifier != "projectilemid" && other.modifier != "projectilelow" && other.modifier != "projectileoverhead")
                                    {
                                        if(other.following == -1)
                                        {
                                            other.owner.hitting = 1;
                                        }
                                        else
                                        {
                                            other.following.hitting = 1;
                                        }
                                    }
                        }
                    }
                }
            }
        }
    }
    
    //collisionbox
    with(collisionbox)
    {
        while(place_meeting(x, y, obj_collisionbox))
        {
            with(owner)
            {
                with(otherplayer)
                {
                    if(onground())
                    {
                        with(otherplayer)
                        {
                            x += frontOrBack;
                            collisionbox.x += frontOrBack;
                        }
                    }
                    else
                    {
    
                            if(!onground())
                            {
                                with(otherplayer)
                                {
                                    x += frontOrBack;
                                    collisionbox.x += frontOrBack;
                                }
                            }
                            else
                            {
                                with(otherplayer)
                                {
                                    x += frontOrBack;
                                    collisionbox.x += frontOrBack;
                                }
                            }
                    }
                }
            }
            
        }
    }
    
    /*
    //MOVED TO THE TOP BECAUSE IT MAKES HURTBOXES, HITBOXES, ETC NOT FLOAT OFF THE SCREEN IF VX OR VY WOULD PUSH THE PLAYER OFF THE MAP 
    //collisions
    if(onground())
    {
        if(x < 81)
        {
            x = 81;
            vx = 0;
        }
        
        if(x > room_width - 81)
        {
            x = room_width - 81;
            vx = 0;
        }
    }
    else
    {
        if(x < 76)
        {
            x = 76;
            vx = 0;
        }
        
        if(x > room_width - 76)
        {
            x = room_width - 76;
            vx = 0;
        }
    }
    
    //collisions
    if(y > 737)
    {
        y = 737;
        if(vy > 0)
        {
            vy = 0;
        }
    }
    */
    
    if(collisionbox != -1)
    {
        with(obj_collisionbox)
        {
                x = owner.x + xoffset + (owner.frontOrBack *xshift);
                y = owner.y + yoffset;
        }
    }
    
    //damage and ex check
    if(hp < 0)
    {
        hp = 0;
    }
    if(ex > 150)
    {
        ex = 150;
    }
    
    
    //frontOrBack
    if(x < otherplayer.x)
    {
    	frontOrBack = -1;	
    }
    if(x > otherplayer.x)
    {
    	frontOrBack = 1;
    }
    
    xlastframe = x;
    
    //pausing. this has to be done here. it causes GUI issues in the draw phase
    if(other.pause > 0 && other.unpaused_player != player_num && other.paused_objects[0,0] != -1)
    {
        instance_deactivate_object(id);
    }
}