if(state_new == 1)
{
    	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -42;
	    xscale  = 26;
	    yscale  = 74;
	    xshift  = -2;
	}
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 16;
	    image_yscale  = 47;
	    xshift = -2
	}
	
	if(xhit < 2)
	{
	    xhit = 2 * sign(xhit);
	}
}

sprite_index = spr_bill_sblock;

if(onground())
{
    state_switch("sblock");
}

//timer
if(blockstun > 0 && hitpause == 0)
{
blockstun--;
blockstunwas = blockstun;
vx = xhit;
}

//switch out
if(blockstun = 0)
{
    state_switch("air");
}



//hitstun
if(hitstun > 0)
{
    state_switch("hitstun");
    blockstun = 0;
    blockstunwas = 0;
}

if(hitpause > 0 && hitstun == 0)
{
    hitpause --;
}