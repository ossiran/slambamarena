/// @description Insert description here
// You can write your code in this editor

//draw_text(300,400, training);
if(player1 != 0 && player2 != 0)
{
        
    //hp
    var hpVal1 = (player1.hp/player1.hpmax) * -500;
    var redhpVal1 = ((player1.hp + player1.combodamage)/player1.hpmax) * -500;
    var hpVal2 = (player2.hp/player2.hpmax) * 500;
    var redhpVal2 = ((player2.hp + player2.combodamage)/player2.hpmax) * 500;
    
    var barVal1 = floor(player1.ex/25);
    var barVal2 = floor(player2.ex/50);
    var exVal1 = (player1.ex * 12) -(barVal1 * 300);
    var exVal2 = (player2.ex * 6) -(barVal2 * 300);
    
    //hitpause screenshake
    var ranx1 = 0;
    var rany1 = 0;
        
    var ranx2 = 0;
    var rany2 = 0;
    //player1
    if(player1.hitpause > 10 )
    {
        ranx1 = irandom_range(-2,2);
        rany1 = irandom_range(-2,2);
    }
    else if(player1.hitpause > 5 )
    {
        ranx1 = irandom_range(-1,1);
        rany1 = irandom_range(-1,1);
    }
    else if(player1.hitpause > 0)
    {
        //ranx1 = irandom_range(-1,1);
        //rany1 = irandom_range(-1,1);
    }
    //player2
    if(player2.hitpause > 10 )
    {
        ranx2 = irandom_range(-2,2);
        rany2 = irandom_range(-2,2);
    }
    else if(player2.hitpause > 5 )
    {
        ranx2 = irandom_range(-1,1);
        rany2 = irandom_range(-1,1);
    }
    else if(player2.hitpause > 0)
    {
        //ranx2 = irandom_range(-1,1);
        //rany2 = irandom_range(-1,1);
    }
    
    
    //GX
    //player1
    if(player1.gbust == 0)
    {
        draw_sprite_ext(spr_barbox,1,92 + ranx1 + ranx2,100 + rany1 + rany2,(150 * (player1.gx / 900)),8,0,c_lime,1);
    }
    else
    {
        if(player1.gx mod 20 > 10)
        {
            draw_sprite_ext(spr_barbox,1,92 + ranx1 + ranx2,100 + rany1 + rany2,(150 * (player1.gx / 600)),8,0,c_white,1);
        }
        else
        {
            draw_sprite_ext(spr_barbox,1,92 + ranx1 + ranx2,100 + rany1 + rany2,(150 * (player1.gx / 600)),8,0,c_lime,1);
        }
    }
    
    //player2
    draw_sprite_ext(spr_barbox,1,1188+ ranx1 + ranx2,100 + rany1 + rany2,(-150 * (player2.gx / 900)),8,0,c_lime,1);
    
    //LIFEBARS
    draw_sprite_ext(spr_barbox,1,592+ ranx1 + ranx2,40+ rany1 + rany2,redhpVal1,58,0,c_red,1);
    draw_sprite_ext(spr_barbox,1,592+ ranx1 + ranx2,40+ rany1 + rany2,hpVal1 + ranx1,58,0,hp_colorF,1);
    draw_sprite_ext(spr_barbox,1,688+ ranx1 + ranx2,40+ rany1 + rany2,redhpVal2,58,0,c_red,1);
    draw_sprite_ext(spr_barbox,1,688+ ranx1 + ranx2,40+ rany1 + rany2,hpVal2 + ranx2,58,0,hp_colorF,1);
    draw_sprite_ext(spr_lifebar,1,1+ ranx1 + ranx2,14+ rany1 + rany2,1,1,0,c_white,1);
    draw_sprite_ext(spr_lifebar,1,1279+ ranx1 + ranx2,14+ rany1 + rany2,-1,1,0,c_white,1);
    
    //clock
    draw_text(display_get_gui_width()/2, 40, clock);
    //EXBAR2
    
    if(player2.ex == 150)
    {
        draw_sprite_ext(spr_barbox,1,840+ ranx1 + ranx2,670+ rany1 + rany2,301,22,0,ex_colorF,0.95);
    }
    else if(player2.ex > 99.9)
    {
        draw_sprite_ext(spr_barbox,1,840+ ranx1 + ranx2,670+ rany1 + rany2,player2.ex * 2,22,0,ex_colorF3,0.85);
        draw_sprite_ext(spr_barbox,1,840+ ranx1 + ranx2,670+ rany1 + rany2,200,22,0,ex_colorF,0.95);
        
    }
    else if(player2.ex > 49.9)
    {
        draw_sprite_ext(spr_barbox,1,840+ ranx1 + ranx2,670+ rany1 + rany2,player2.ex * 2,22,0,ex_colorF3,0.85);
        draw_sprite_ext(spr_barbox,1,840+ ranx1 + ranx2,670+ rany1 + rany2,100,22,0,ex_colorF,0.95);
        
    }
    else
    {
        draw_sprite_ext(spr_barbox,1,840+ ranx1 + ranx2,670+ rany1 + rany2,player2.ex * 2,22,0,ex_colorF3,0.85);
    }
    
    //EXBAR1
    /*
    //OLD
    //draw_sprite_ext(spr_barbox,1,434,664,-301,32,0,c_dkgray,1);
    draw_sprite_ext(spr_barbox,1,440+ ranx1 + ranx2,670+ rany1 + rany2,player1.ex * 2,22,0,ex_colorF,0.90);
    
    if(player1.ex = 150)
    {
        draw_sprite_ext(spr_barbox,1,440+ ranx1 + ranx2,670+ rany1 + rany2,-301,22,0,ex_colorF3,0.95);
    }
    */
    
    
    if(player1.ex == 150)
    {
        draw_sprite_ext(spr_barbox,1,440+ ranx1 + ranx2,670+ rany1 + rany2,-301,22,0,ex_colorF,0.95);
    }
    else if(player1.ex > 99.9)
    {
        draw_sprite_ext(spr_barbox,1,440+ ranx1 + ranx2,670+ rany1 + rany2,-player1.ex * 2,22,0,ex_colorF3,0.85);
        draw_sprite_ext(spr_barbox,1,440+ ranx1 + ranx2,670+ rany1 + rany2,-200,22,0,ex_colorF,0.95);
        
    }
    else if(player1.ex > 49.9)
    {
        draw_sprite_ext(spr_barbox,1,440+ ranx1 + ranx2,670+ rany1 + rany2,-player1.ex * 2,22,0,ex_colorF3,0.85);
        draw_sprite_ext(spr_barbox,1,440+ ranx1 + ranx2,670+ rany1 + rany2,-100,22,0,ex_colorF,0.95);
        
    }
    else
    {
        draw_sprite_ext(spr_barbox,1,440+ ranx1 + ranx2,670+ rany1 + rany2,-player1.ex * 2,22,0,ex_colorF3,0.85);
    }
    
        
    //hitcounter
    if(player2.hitcounter > 1)
    {
        draw_text_transformed_color(120 - (font_get_size(THEFONT) * (0.05 * player2.hitpause))+ ranx1 + ranx2,260 - (font_get_size(THEFONT) * (0.05 * player2.hitpause))+ rany1 + rany2,string(player2.hitcounter) + " HITS",2 + (0.05 * player2.hitpause),2 + (0.05 * player2.hitpause),1,c_red,c_red,c_yellow,c_yellow,0.95);
    }
    if(player1.hitcounter > 1)
    {
        draw_text_transformed_color(1125 - (font_get_size(THEFONT) * (0.05 * player1.hitpause))+ ranx1 + ranx2,260 - (font_get_size(THEFONT) * (0.05 * player1.hitpause))+ rany1 + rany2,string(player1.hitcounter) + " HITS",2 + (0.05 * player1.hitpause),2 + (0.05 * player1.hitpause),1,c_red,c_red,c_yellow,c_yellow,0.95);
    }
    
    //juggle
    if(player2.juggle > 0)
    {
        draw_text_transformed_color(120+ ranx1 + ranx2 ,340+ rany1 + rany2,"JUGGLE: " + string(player2.juggle) + " /5",1,1,1,c_red,c_red,c_yellow,c_yellow,0.95);
    }
    if(player1.juggle > 0)
    {
        draw_text_transformed_color(1125+ ranx1 + ranx2 ,340+ rany1 + rany2,"JUGGLE: " + string(player1.juggle) + " /5",1,1,1,c_red,c_red,c_yellow,c_yellow,0.95);
    }
    
    
    //message array tickdown1
    var loopcounter = 0;
    repeat(message_array_count_1)
    {
        if(message_array_1[loopcounter,1] > 0)
        {
            message_array_1[loopcounter,1] --;
        }
        else
        {
            //moving down
            message_array_1[loopcounter,0] = 0;
            var i = loopcounter;
            repeat(message_array_count_1 - loopcounter - 1)
            {
                i++;
                message_array_1[i - 1,0] =  message_array_1[i,0];
                message_array_1[i - 1,1] =  message_array_1[i,1];
            }
            message_array_count_1--;
        }
        
        loopcounter ++;
    }
    
    //message array tickdown2
    loopcounter = 0;
    repeat(message_array_count_2)
    {
        if(message_array_2[loopcounter,1] > 0)
        {
            message_array_2[loopcounter,1] --;
        }
        else
        {
            //moving down
            message_array_2[loopcounter,0] = 0;
            var i = loopcounter;
            repeat(message_array_count_2 - loopcounter - 1)
            {
                i++;
                message_array_2[i - 1,0] =  message_array_2[i,0];
                message_array_2[i - 1,1] =  message_array_2[i,1];
            }
            message_array_count_2--;
        }
        
        loopcounter ++;
    }
    
    //message array display
    loopcounter = 0;
    repeat(message_array_count_1)
    {
        draw_text_transformed_color(110+ ranx1 + ranx2,440 + (25 * loopcounter)+ rany1 + rany2,message_array_1[loopcounter, 0],1,1,1,c_red,c_red,c_yellow,c_yellow,message_array_1[loopcounter, 1]);
        loopcounter ++;
    }
    
    loopcounter = 0;
    repeat(message_array_count_2)
    {
        draw_text_transformed_color(1065+ ranx1 + ranx2,440 + (25 * loopcounter)+ rany1 + rany2,message_array_2[loopcounter, 0],1,1,1,c_red,c_red,c_yellow,c_yellow,message_array_2[loopcounter, 1]);
        loopcounter ++;
    }
    
    draw_sprite_ext(spr_exmeter1,0,0+ ranx1 + ranx2,720+ rany1 + rany2,1,1,0,c_white,1);
    draw_sprite_ext(spr_exmeter1,1,1280+ ranx1 + ranx2,720+ rany1 + rany2,-1,1,0,c_white,1);
    
    
    //character specific meters
    if(object_exists(obj_syn))
    {
        with(obj_syn)
        {
            if(player_num == 1)
            {
                with(other)
                {
                    if(player1.synmode > 0)
                    {
                        draw_sprite_ext(spr_barbox,1, 135, 657, 200 * (player1.synmode / 1500), 8, 0, c_white, 0.8);
                        draw_sprite_ext(spr_light, -1, 105, 674, 2, 2, 0 ,c_white, 1);
                    }
                    else if(player1.synmode < 0)
                    {
                        draw_sprite_ext(spr_barbox,1, 135, 657, 200 * (-player1.synmode / 1500), 8, 0, c_black, 0.65);
                        draw_sprite_ext(spr_dark, -1, 105, 674, 2, 2, 0 ,c_white, 1);
                    }
                    else
                    {
                        draw_sprite_ext(spr_dark, -1, 105, 674, 2, 2, 0 ,c_white, 0.5);
                        draw_sprite_ext(spr_light, -1, 105, 674, 2, 2, 0 ,c_white, 0.5);
                    }
                }
            }
            else if(player_num == 2)
            {
                if(player2.synmode > 0)
                    {
                        draw_sprite_ext(spr_barbox,1, 1145, 657, 200 * (-player2.synmode / 1500), 8, 0, c_white, 0.8);
                        draw_sprite_ext(spr_light, -1, 1175, 674, 2, 2, 0 ,c_white, 1);
                    }
                    else if(player2.synmode < 0)
                    {
                        draw_sprite_ext(spr_barbox,1, 1145, 657, 200 * (player2.synmode / 1500), 8, 0, c_black, 0.65);
                        draw_sprite_ext(spr_dark, -1, 1175, 674, 2, 2, 0 ,c_white, 1);
                    }
                    else
                    {
                        draw_sprite_ext(spr_dark, -1, 1175, 674, 2, 2, 0 ,c_white, 0.5);
                        draw_sprite_ext(spr_light, -1, 1175, 674, 2, 2, 0 ,c_white, 0.5);
                    }
            }
        }
    }
    
    
    draw_set_font(THEFONT);
    draw_set_color(c_white);
    
    /*
    //gbust
    if(player1.gbust > 0)
    {
        draw_sprite_ext(spr_barbox,1,126,645,210 * (player1.gbust / player1.gbustmax),14,0,c_white,0.95);
        draw_text_transformed_color(340,638,"TIME",0.75,0.75,1,c_yellow,c_yellow,c_yellow,c_yellow,1);
    }
    
    //gbust
    if(player2.gbust > 0)
    {
        draw_sprite_ext(spr_barbox,1,1155,645,-(player2.gbust / player2.gbustmax) * 210,14,0,c_white,0.95);
        draw_text_transformed_color(900,638,"TIME",0.75,0.75,1,c_yellow,c_yellow,c_yellow,c_yellow,1);
    }
    */
    
    //meter count
    //draw_text_transformed(87+ ranx1 + ranx2,634+ rany1 + rany2,barVal1,2,2,0);
    //draw_text_transformed(1160+ ranx1 + ranx2,634+ rany1 + rany2,barVal2,2,2,0);
    
    
    //draw_text(200,200, paused_objects[0,0]);
}

//menu
if(state_name == "main_menu")
{
    for(var i = 0; i < menu_maxx + 1; i++)
    {
        draw_text(display_get_gui_width()/2, display_get_gui_height()/2.5 + i*30, menu_array[i,0]);
    }
    draw_sprite_ext(spr_hitspark,0,display_get_gui_width()/2 - 15, display_get_gui_height()/2.5 + menu_indexx*30, 1,1, 0, c_white, 1);
}


