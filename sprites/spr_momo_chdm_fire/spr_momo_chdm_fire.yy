{
    "id": "535ce904-6f73-4cdb-858a-083daf53e0d2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_chdm_fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 122,
    "bbox_left": 24,
    "bbox_right": 153,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "506b9df1-085a-4859-af3a-3553a376a2b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "44e90d03-0ead-422d-b0e4-638de008c06d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "506b9df1-085a-4859-af3a-3553a376a2b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a84f6da1-06b4-4bd3-8337-046eef87dc64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "506b9df1-085a-4859-af3a-3553a376a2b9",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "8d9a4c14-363f-4f9b-86e4-b96d25ebefcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "9e75392b-6aad-4afb-9888-2c9713865b9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d9a4c14-363f-4f9b-86e4-b96d25ebefcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e20c55a0-4af6-42b6-83d9-df2203730851",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d9a4c14-363f-4f9b-86e4-b96d25ebefcc",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "c73a197c-5ff5-48f9-85b6-f436648e7ee1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "c8957a99-de4f-45f6-80af-79779b24847e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c73a197c-5ff5-48f9-85b6-f436648e7ee1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7052c2bc-dbb5-470a-abef-906c8c088f3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c73a197c-5ff5-48f9-85b6-f436648e7ee1",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "84f7b967-c829-4294-9976-a08fe8004e04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "92f1018b-3750-44fc-8ccd-75fd62d38da9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84f7b967-c829-4294-9976-a08fe8004e04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60ce6899-2d98-4490-8847-01f87343a39b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84f7b967-c829-4294-9976-a08fe8004e04",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "9f553429-59ff-4d9f-8900-41063105eb37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "9a5be5ec-e5be-4e9a-a60b-bf055c1eb5b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f553429-59ff-4d9f-8900-41063105eb37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a6db8bf-d159-4bfb-9035-9f9cbf602d7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f553429-59ff-4d9f-8900-41063105eb37",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "0dc365a1-a97f-45f6-964d-d1462519ef01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "aa3d5c4a-5b13-4ada-8a1e-be6454687888",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dc365a1-a97f-45f6-964d-d1462519ef01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07f89187-d7db-4668-9c44-8a5c677c807d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dc365a1-a97f-45f6-964d-d1462519ef01",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "797718b5-d86b-4fad-bf3b-fc9463d44846",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "831e25ef-141f-4af9-9ed2-31812b2c40db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "797718b5-d86b-4fad-bf3b-fc9463d44846",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2a8fb99-f107-452d-857e-328ee85ad729",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "797718b5-d86b-4fad-bf3b-fc9463d44846",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "1546c2e8-3672-4ef2-bef1-74d6d0e557a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "04e3fc1e-71a5-4ebb-9eef-bf83a9e3c9a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1546c2e8-3672-4ef2-bef1-74d6d0e557a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1aea818b-af94-4819-bab5-474af643d168",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1546c2e8-3672-4ef2-bef1-74d6d0e557a7",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "69ab97ad-ee9c-4b2c-a80b-e974c5072372",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "0216e058-29ba-43c5-805b-473683627e36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69ab97ad-ee9c-4b2c-a80b-e974c5072372",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2fe086e-9fe2-4e4d-8434-709bbf449756",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69ab97ad-ee9c-4b2c-a80b-e974c5072372",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "a448e987-eb44-42f0-9745-c13a0482bc44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "6b87ec82-5660-4340-bd7e-245df01d6e02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a448e987-eb44-42f0-9745-c13a0482bc44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3aaa8312-c887-4480-8725-a355eb52dc5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a448e987-eb44-42f0-9745-c13a0482bc44",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "fa676c4c-859f-40b1-a158-efa1d2b73cca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "3b0cd410-3619-4b76-9d68-512e8c497b86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa676c4c-859f-40b1-a158-efa1d2b73cca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc7ce257-aee5-4c6a-8ac1-5cc0dbaf6d91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa676c4c-859f-40b1-a158-efa1d2b73cca",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "a99982ba-251b-480e-95e9-f91690fd4cbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "7e6a33be-1036-45cb-ad90-574d39196a8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a99982ba-251b-480e-95e9-f91690fd4cbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96962205-9fc5-4a5e-bbf0-95c7ea77b87d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a99982ba-251b-480e-95e9-f91690fd4cbb",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "7c195c1f-0aee-4248-9327-dea8aecb3ef4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "b10a1982-fcad-49d0-8ba5-3e2a2fa131fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c195c1f-0aee-4248-9327-dea8aecb3ef4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70589a86-8f69-4f1c-a80e-a6eeb1365577",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c195c1f-0aee-4248-9327-dea8aecb3ef4",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "0f226a91-d853-40f5-8507-432a15a597fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "0d5a37de-de39-4381-a73c-bb7bb72e59cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f226a91-d853-40f5-8507-432a15a597fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4f23c94-84b6-47c2-8378-bb000aae3f1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f226a91-d853-40f5-8507-432a15a597fb",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "8cf0dd9f-a910-4d5c-9caa-4345f4db7aec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "1955f074-cddb-4550-860e-23247b6050ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cf0dd9f-a910-4d5c-9caa-4345f4db7aec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "491275f6-03ce-4d52-84be-5c6b2304bdeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cf0dd9f-a910-4d5c-9caa-4345f4db7aec",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "e3f77968-9679-48fd-9827-54a9fcea2c2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "0e19f523-3463-43a0-b240-d3c960ffa2ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3f77968-9679-48fd-9827-54a9fcea2c2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7f0c6ef-d19a-44ee-8afe-73884766b85f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3f77968-9679-48fd-9827-54a9fcea2c2d",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "fad6a962-7f09-452d-bf72-b4e42853a34b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "cfd4082c-599f-4f6b-80c8-fd35c5d8abca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fad6a962-7f09-452d-bf72-b4e42853a34b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d122272f-214b-4161-8482-aa3a17dc0022",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fad6a962-7f09-452d-bf72-b4e42853a34b",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "1dc74884-29e2-4a22-827f-031243962b42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "b07f4149-b213-4393-a229-c5e62913dd78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dc74884-29e2-4a22-827f-031243962b42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34951141-4f48-463c-89a9-fb6670f1ee95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dc74884-29e2-4a22-827f-031243962b42",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "e0a2be84-ddb6-401f-954d-8a8eb5adccfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "a9bec405-b692-46c2-8481-ba5300646a95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0a2be84-ddb6-401f-954d-8a8eb5adccfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b33b2b0f-8842-4805-bd18-0fe3f3c51782",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0a2be84-ddb6-401f-954d-8a8eb5adccfa",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "b54dc309-4da4-4872-b02d-5af8af865628",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "2f3da941-a88e-474d-a840-05cb4eefc2fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b54dc309-4da4-4872-b02d-5af8af865628",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1ff7324-19ae-4785-a765-6817e3bbce30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b54dc309-4da4-4872-b02d-5af8af865628",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "9fafd90c-b61b-4f8e-8fca-6a954a170d61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "0876c3e7-adf6-4887-ac39-612182517999",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fafd90c-b61b-4f8e-8fca-6a954a170d61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da115e3e-d74c-40e8-8212-1aceb629cba6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fafd90c-b61b-4f8e-8fca-6a954a170d61",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "90050fc4-0e78-41bb-8e22-d317e999be04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "ccd434f7-25ff-444a-851e-3a24bd001c40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90050fc4-0e78-41bb-8e22-d317e999be04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07a143c1-9fa8-4636-a3ee-9fcad3453b2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90050fc4-0e78-41bb-8e22-d317e999be04",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "ec301595-a1d8-4bae-864b-3a93650cdd5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "c90a9fb7-a1a8-4928-a01e-7330d74e6791",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec301595-a1d8-4bae-864b-3a93650cdd5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a02d918-d61e-4d02-b71d-76bdf69cbe3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec301595-a1d8-4bae-864b-3a93650cdd5d",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "a2bc1764-81f8-4184-b88b-bffb46a586b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "5ee97f59-d375-45f2-b09b-f334638b94c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2bc1764-81f8-4184-b88b-bffb46a586b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cebda56-2903-417b-960f-24b1833bb00e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2bc1764-81f8-4184-b88b-bffb46a586b9",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "3842e36b-1e8f-4415-84c8-623b7f7ac69e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "0c2d61f2-2254-47c1-a4ee-7eeff30ae188",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3842e36b-1e8f-4415-84c8-623b7f7ac69e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12c30aa9-f717-4c2a-95b6-01bf703eaea0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3842e36b-1e8f-4415-84c8-623b7f7ac69e",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "a5c95f77-13d6-48cd-a662-0cf0f87cde60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "ede73b41-6ff5-4d27-8640-f27ee2a6507b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5c95f77-13d6-48cd-a662-0cf0f87cde60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34572ed0-3495-484c-ba24-2506b48ecd1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5c95f77-13d6-48cd-a662-0cf0f87cde60",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "8f3e0c0c-2e5a-4780-9a60-8a8ca088ab85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "0177a1dd-21ea-4b20-9604-3917218d9ddc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f3e0c0c-2e5a-4780-9a60-8a8ca088ab85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d4664e8-e82f-44b9-9e27-44515d3d3056",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f3e0c0c-2e5a-4780-9a60-8a8ca088ab85",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "9f9fec5c-e2d9-4464-8f19-5a497223cc67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "c6d756b0-4dcf-4763-8950-7f4434eb1d41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f9fec5c-e2d9-4464-8f19-5a497223cc67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cc02d6f-2676-4640-b19f-caf3cfbcea65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f9fec5c-e2d9-4464-8f19-5a497223cc67",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "cdcec1db-036b-4637-9398-b495c195c9e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "f5274104-e316-453b-b95a-beba9553d13c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdcec1db-036b-4637-9398-b495c195c9e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b01becb-80f0-4626-8792-8458acb42a01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdcec1db-036b-4637-9398-b495c195c9e1",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "20feecbd-aeb2-488d-9776-58feb589d28f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "04d4dcc8-ad2d-435c-a85f-5e8fcc7db904",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20feecbd-aeb2-488d-9776-58feb589d28f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ae4bffd-ecc4-43a2-bb03-687c8aa4fa1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20feecbd-aeb2-488d-9776-58feb589d28f",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "483436c6-f9d6-4445-aad1-3bdb1b2684c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "30d532b0-6624-43da-9a26-93acabd1cbdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "483436c6-f9d6-4445-aad1-3bdb1b2684c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dd1c6a4-0090-4d5e-9d03-14d599e0f1e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "483436c6-f9d6-4445-aad1-3bdb1b2684c8",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "570b04ad-7046-4cad-bc3c-d254df3f9cc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "e17ae698-930c-44ff-bdbf-83f67ade2e4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "570b04ad-7046-4cad-bc3c-d254df3f9cc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48179449-d306-4236-957a-4294b5d54383",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "570b04ad-7046-4cad-bc3c-d254df3f9cc8",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "db8acc71-2eae-4b8f-9b10-a60c109991b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "a1164611-8adc-4c53-88f9-19c29b46c1fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db8acc71-2eae-4b8f-9b10-a60c109991b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88a0918d-468e-4f63-b39d-f3736479de2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db8acc71-2eae-4b8f-9b10-a60c109991b4",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "683f366c-8462-4331-be94-96d0527a5584",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "a1d09ac5-35f5-488b-a17d-642dbb67cc83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "683f366c-8462-4331-be94-96d0527a5584",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2b45217-1a10-4bab-9c7d-77605fa1c934",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "683f366c-8462-4331-be94-96d0527a5584",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "2be0c14b-e5fb-4599-9b39-79914a6de5ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "3d08a1d1-9d71-4a4a-bd42-aa54b8f0a3db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2be0c14b-e5fb-4599-9b39-79914a6de5ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3df9effc-450a-4bbf-8f3c-06a3f0285bbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2be0c14b-e5fb-4599-9b39-79914a6de5ed",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "a067e247-28cb-4741-bdd7-e7526fbe6490",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "f4d9d4f3-b15a-4122-8864-b74369389902",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a067e247-28cb-4741-bdd7-e7526fbe6490",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5dedf88-461b-496e-9e8f-18858a3ed06e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a067e247-28cb-4741-bdd7-e7526fbe6490",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "042c33f9-1e06-4841-ab6f-73653d17dcb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "b9892599-7bc8-4092-b511-9b830dc78ced",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "042c33f9-1e06-4841-ab6f-73653d17dcb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "227a2a1c-af46-4020-80e1-140b1261e601",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "042c33f9-1e06-4841-ab6f-73653d17dcb1",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "0648b62c-3a7a-4350-bc4c-fa0b53423034",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "9ba16dfa-045b-4ea5-b0d1-7ee5224f005c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0648b62c-3a7a-4350-bc4c-fa0b53423034",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa920738-ace7-4f7b-9df2-c97e26da72c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0648b62c-3a7a-4350-bc4c-fa0b53423034",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "fc32fc1f-b11d-4e3b-8bd3-0b6bcf46c840",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "fa30b45b-2019-46ad-ad5d-e10ddd192063",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc32fc1f-b11d-4e3b-8bd3-0b6bcf46c840",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a12334b-a216-41bd-8603-3f41460f8ebf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc32fc1f-b11d-4e3b-8bd3-0b6bcf46c840",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "e798b747-93a0-4747-bd8b-d6f459599730",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "080701d2-eded-45e2-94f0-355b9086e33b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e798b747-93a0-4747-bd8b-d6f459599730",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33fc9023-89b7-4d4c-954c-469dd1e4701a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e798b747-93a0-4747-bd8b-d6f459599730",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "4fce796c-14ee-4a00-b553-c15967ce43cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "d3fcad40-4dfc-4973-8225-d8b7fc9cff66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fce796c-14ee-4a00-b553-c15967ce43cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "368def85-fed5-44d9-bbdb-8911988fee3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fce796c-14ee-4a00-b553-c15967ce43cd",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "188ab269-c524-4d35-9c6c-ac5f0068c2d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "7b400bab-46bd-4aaf-9457-d2206ac95924",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "188ab269-c524-4d35-9c6c-ac5f0068c2d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dede6a3-786c-498c-950f-f79307d35911",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "188ab269-c524-4d35-9c6c-ac5f0068c2d3",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        },
        {
            "id": "873460e8-9f1c-489f-b9b3-5f5b588b4223",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "compositeImage": {
                "id": "a7c04a9d-5ff7-4d4e-9e73-4d96ec6ed354",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "873460e8-9f1c-489f-b9b3-5f5b588b4223",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60da84aa-2c72-48a4-bacb-36dad17fc95d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "873460e8-9f1c-489f-b9b3-5f5b588b4223",
                    "LayerId": "56c07701-557a-4de3-936d-17c1a20787a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 148,
    "layers": [
        {
            "id": "56c07701-557a-4de3-936d-17c1a20787a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "535ce904-6f73-4cdb-858a-083daf53e0d2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 168,
    "xorig": 63,
    "yorig": 87
}