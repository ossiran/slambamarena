{
    "id": "5536ef0c-373c-41f0-a126-f71e6ef9eea5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_myu_attack_cjab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 8,
    "bbox_right": 56,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7d38da7-1df3-44cf-8339-097148e1f834",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5536ef0c-373c-41f0-a126-f71e6ef9eea5",
            "compositeImage": {
                "id": "1bbe9925-eb11-4510-b18f-720b791e1cfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7d38da7-1df3-44cf-8339-097148e1f834",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "602ba609-80f9-4717-bda7-82de4e913aab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7d38da7-1df3-44cf-8339-097148e1f834",
                    "LayerId": "4a54ea72-fdb8-42d3-95fd-4d7f912dadbb"
                }
            ]
        },
        {
            "id": "abd7b9e8-d23e-4690-b08d-b217deac53e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5536ef0c-373c-41f0-a126-f71e6ef9eea5",
            "compositeImage": {
                "id": "4ec8dce8-cab0-464f-b6f6-0e06e0f5ab87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abd7b9e8-d23e-4690-b08d-b217deac53e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f023a10-fd67-422a-a772-24ab86d9231b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abd7b9e8-d23e-4690-b08d-b217deac53e2",
                    "LayerId": "4a54ea72-fdb8-42d3-95fd-4d7f912dadbb"
                }
            ]
        },
        {
            "id": "71e18a53-0bb0-4f04-822a-cb7fc30c0981",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5536ef0c-373c-41f0-a126-f71e6ef9eea5",
            "compositeImage": {
                "id": "8e93c118-84e4-4a0a-810c-d0e7b8718083",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71e18a53-0bb0-4f04-822a-cb7fc30c0981",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef5039c4-362b-4eff-bac2-110e73e80b22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71e18a53-0bb0-4f04-822a-cb7fc30c0981",
                    "LayerId": "4a54ea72-fdb8-42d3-95fd-4d7f912dadbb"
                }
            ]
        },
        {
            "id": "58c7ed3f-95c0-4a05-b0a8-2521f1b2bef3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5536ef0c-373c-41f0-a126-f71e6ef9eea5",
            "compositeImage": {
                "id": "75e16e91-c04e-4651-99ca-cacd14f86502",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58c7ed3f-95c0-4a05-b0a8-2521f1b2bef3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15c36c20-e845-4bb2-8514-c8aa9b42e64b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58c7ed3f-95c0-4a05-b0a8-2521f1b2bef3",
                    "LayerId": "4a54ea72-fdb8-42d3-95fd-4d7f912dadbb"
                }
            ]
        },
        {
            "id": "bfe956fb-ffd6-46f1-9500-8f6e7077e9dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5536ef0c-373c-41f0-a126-f71e6ef9eea5",
            "compositeImage": {
                "id": "5709a3c7-bbe6-4edf-a740-6030eb5188b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfe956fb-ffd6-46f1-9500-8f6e7077e9dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8686326f-2e38-4ee9-b4b1-3e1588e16102",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfe956fb-ffd6-46f1-9500-8f6e7077e9dc",
                    "LayerId": "4a54ea72-fdb8-42d3-95fd-4d7f912dadbb"
                }
            ]
        },
        {
            "id": "8d3979c6-07cc-4f4d-a8cd-c1fc475b4ff9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5536ef0c-373c-41f0-a126-f71e6ef9eea5",
            "compositeImage": {
                "id": "6ff0cae0-265e-4c92-a3b9-59c722de6247",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d3979c6-07cc-4f4d-a8cd-c1fc475b4ff9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c15731e0-fc8d-4b08-9586-9ea8788735e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d3979c6-07cc-4f4d-a8cd-c1fc475b4ff9",
                    "LayerId": "4a54ea72-fdb8-42d3-95fd-4d7f912dadbb"
                }
            ]
        },
        {
            "id": "b570fdaa-b72b-4c2c-84c8-73c38dbc06bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5536ef0c-373c-41f0-a126-f71e6ef9eea5",
            "compositeImage": {
                "id": "398e04de-77f3-4e41-861f-f3b1af4e48db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b570fdaa-b72b-4c2c-84c8-73c38dbc06bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c56c6242-8892-4cab-baed-4defb6738d6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b570fdaa-b72b-4c2c-84c8-73c38dbc06bb",
                    "LayerId": "4a54ea72-fdb8-42d3-95fd-4d7f912dadbb"
                }
            ]
        },
        {
            "id": "12f23afb-9e50-43af-9bb8-755e2cb62e8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5536ef0c-373c-41f0-a126-f71e6ef9eea5",
            "compositeImage": {
                "id": "6f014758-edac-4bf4-ab0b-e864ad567bc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12f23afb-9e50-43af-9bb8-755e2cb62e8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4ec0089-9150-4424-ba23-1a5d77a78c4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12f23afb-9e50-43af-9bb8-755e2cb62e8d",
                    "LayerId": "4a54ea72-fdb8-42d3-95fd-4d7f912dadbb"
                }
            ]
        },
        {
            "id": "dfcf0d7e-6430-48dc-959e-700a871be487",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5536ef0c-373c-41f0-a126-f71e6ef9eea5",
            "compositeImage": {
                "id": "9bdfd78a-2cff-4555-9c94-73bf89bc356e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfcf0d7e-6430-48dc-959e-700a871be487",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cb864f6-c8a5-43bd-918d-cfe9817c2e82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfcf0d7e-6430-48dc-959e-700a871be487",
                    "LayerId": "4a54ea72-fdb8-42d3-95fd-4d7f912dadbb"
                }
            ]
        },
        {
            "id": "b763991b-4b1a-4a1f-ad8f-a5c2a8338529",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5536ef0c-373c-41f0-a126-f71e6ef9eea5",
            "compositeImage": {
                "id": "619260f0-8cb4-4734-9937-18ddd0d796bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b763991b-4b1a-4a1f-ad8f-a5c2a8338529",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64578805-a6ad-428a-8b27-9d06003bb0a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b763991b-4b1a-4a1f-ad8f-a5c2a8338529",
                    "LayerId": "4a54ea72-fdb8-42d3-95fd-4d7f912dadbb"
                }
            ]
        },
        {
            "id": "5270bf27-85a3-49c4-a560-1b9f12c2d771",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5536ef0c-373c-41f0-a126-f71e6ef9eea5",
            "compositeImage": {
                "id": "ae4c6e70-f369-4a08-833b-0fa0149ace67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5270bf27-85a3-49c4-a560-1b9f12c2d771",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a828a98-7413-45be-b313-299286a7247f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5270bf27-85a3-49c4-a560-1b9f12c2d771",
                    "LayerId": "4a54ea72-fdb8-42d3-95fd-4d7f912dadbb"
                }
            ]
        },
        {
            "id": "4a35cdb4-b811-434a-a37a-a14c3cd14d9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5536ef0c-373c-41f0-a126-f71e6ef9eea5",
            "compositeImage": {
                "id": "f27bd911-5a1b-474e-995e-2c06e5dce8be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a35cdb4-b811-434a-a37a-a14c3cd14d9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57e9c406-e841-48ac-98be-e321e500e4a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a35cdb4-b811-434a-a37a-a14c3cd14d9e",
                    "LayerId": "4a54ea72-fdb8-42d3-95fd-4d7f912dadbb"
                }
            ]
        },
        {
            "id": "1114b2a2-d28c-4b93-8e24-228b734de5d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5536ef0c-373c-41f0-a126-f71e6ef9eea5",
            "compositeImage": {
                "id": "11888d39-8e23-438a-9bb3-e280a4d23682",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1114b2a2-d28c-4b93-8e24-228b734de5d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ea18d5e-5aae-44e4-9cf1-97851b6fdaa1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1114b2a2-d28c-4b93-8e24-228b734de5d4",
                    "LayerId": "4a54ea72-fdb8-42d3-95fd-4d7f912dadbb"
                }
            ]
        },
        {
            "id": "d601f5f7-56ce-42b5-83c3-c58d16d979e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5536ef0c-373c-41f0-a126-f71e6ef9eea5",
            "compositeImage": {
                "id": "b1302865-50d8-4caf-89ce-c8d1b50dfe78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d601f5f7-56ce-42b5-83c3-c58d16d979e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6733da87-b153-43a3-be94-52a41bb6c986",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d601f5f7-56ce-42b5-83c3-c58d16d979e6",
                    "LayerId": "4a54ea72-fdb8-42d3-95fd-4d7f912dadbb"
                }
            ]
        },
        {
            "id": "847c0e58-4e0b-4056-a13d-46faccd46d80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5536ef0c-373c-41f0-a126-f71e6ef9eea5",
            "compositeImage": {
                "id": "95bbf2ed-ea15-4205-9387-34a9f28e5864",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "847c0e58-4e0b-4056-a13d-46faccd46d80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "769137b7-c837-4f2d-9a3c-5613b02b9ce5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "847c0e58-4e0b-4056-a13d-46faccd46d80",
                    "LayerId": "4a54ea72-fdb8-42d3-95fd-4d7f912dadbb"
                }
            ]
        },
        {
            "id": "07dd77a6-7b43-4088-a148-aa8a874a97cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5536ef0c-373c-41f0-a126-f71e6ef9eea5",
            "compositeImage": {
                "id": "425145a2-9c2f-4293-ad41-00994e6a7bfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07dd77a6-7b43-4088-a148-aa8a874a97cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b2d817d-8c5e-4f4c-ab48-d0f65fc16983",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07dd77a6-7b43-4088-a148-aa8a874a97cc",
                    "LayerId": "4a54ea72-fdb8-42d3-95fd-4d7f912dadbb"
                }
            ]
        },
        {
            "id": "f16cbb07-8256-4546-89c7-0c5e6585a784",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5536ef0c-373c-41f0-a126-f71e6ef9eea5",
            "compositeImage": {
                "id": "8e4762e5-f9cc-485e-b287-fd4351203223",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f16cbb07-8256-4546-89c7-0c5e6585a784",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b0450df-a2a7-4ff3-b3f9-810ff6e41097",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f16cbb07-8256-4546-89c7-0c5e6585a784",
                    "LayerId": "4a54ea72-fdb8-42d3-95fd-4d7f912dadbb"
                }
            ]
        },
        {
            "id": "610ebfc9-792e-4431-96d4-cf8166b233f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5536ef0c-373c-41f0-a126-f71e6ef9eea5",
            "compositeImage": {
                "id": "0691d06c-d69d-4f9a-b83d-d8223209d744",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "610ebfc9-792e-4431-96d4-cf8166b233f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "639296c3-741c-47e7-8f19-c61ee8ef568c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "610ebfc9-792e-4431-96d4-cf8166b233f7",
                    "LayerId": "4a54ea72-fdb8-42d3-95fd-4d7f912dadbb"
                }
            ]
        },
        {
            "id": "c9684d29-5683-4196-ae11-e5ef7d706c56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5536ef0c-373c-41f0-a126-f71e6ef9eea5",
            "compositeImage": {
                "id": "d5afb467-397b-498f-8887-b161cbd426ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9684d29-5683-4196-ae11-e5ef7d706c56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4c73c5b-0f2f-45a0-9183-144c9e69744d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9684d29-5683-4196-ae11-e5ef7d706c56",
                    "LayerId": "4a54ea72-fdb8-42d3-95fd-4d7f912dadbb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4a54ea72-fdb8-42d3-95fd-4d7f912dadbb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5536ef0c-373c-41f0-a126-f71e6ef9eea5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 24,
    "yorig": 32
}