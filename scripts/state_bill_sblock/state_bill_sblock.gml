if(state_new == 1)
{
    	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -42;
	    xscale  = 26;
	    yscale  = 74;
	    xshift  = -2;
	}
	
	if(abs(xhit) < 2)
	{
	    xhit = 3 * sign(xhit);
	}
}

sprite_index = spr_bill_sblock;

//switch to cblock if cblocking
if(blockstun > blockstunwas)
{
    if(key_down == 1)
    {
        state_switch("cblock");
    }
    
    if(abs(xhit) < 2)
	{
	    xhit = 3 * sign(xhit);
	}
}

//timer
if(blockstun > 0 && hitpause == 0)
{
    blockstun--;
    blockstunwas = blockstun;
    vx = xhit/3;
    otherplayer.vx += -xhit/3;
    xhit = xhit * 0.85;
}

//switch out
if(blockstun = 0)
{
    state_switch("stand");
    xhit = 0;
}

blockstunwas =  blockstun;

//hitstun
if(hitstun > 0)
{
    state_switch("hitstun");
    blockstun = 0;
    blockstunwas = 0;
}

if(hitpause > 0 && hitstun == 0)
{
    hitpause --;
    //pushback

}