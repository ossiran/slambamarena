{
    "id": "94e15a3b-5a7a-4169-9350-4b501fe6190e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_system",
    "eventList": [
        {
            "id": "c604e320-e217-4d4a-99aa-37d25066be30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "94e15a3b-5a7a-4169-9350-4b501fe6190e"
        },
        {
            "id": "9f69d24e-e5c8-46c2-9cc7-1dc936e40d5e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "94e15a3b-5a7a-4169-9350-4b501fe6190e"
        },
        {
            "id": "ef19de55-166f-4703-a8aa-2c84ea941b4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "94e15a3b-5a7a-4169-9350-4b501fe6190e"
        },
        {
            "id": "e8fc8500-8988-4b02-bd43-6957968fc394",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "94e15a3b-5a7a-4169-9350-4b501fe6190e"
        },
        {
            "id": "a57ada19-ab58-458b-b046-db5b2f1ab7b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "94e15a3b-5a7a-4169-9350-4b501fe6190e"
        },
        {
            "id": "7e36c24e-0674-49c6-a8e2-cb849f86d781",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "94e15a3b-5a7a-4169-9350-4b501fe6190e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}