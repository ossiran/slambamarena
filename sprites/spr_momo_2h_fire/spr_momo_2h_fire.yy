{
    "id": "5aed1f81-7bb1-4426-8cc6-b7f80ad40771",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_2h_fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ad537d8-8f5c-440f-a700-37b8fa774532",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aed1f81-7bb1-4426-8cc6-b7f80ad40771",
            "compositeImage": {
                "id": "6fdbb882-eef9-4587-9bb5-31d313ec4d2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ad537d8-8f5c-440f-a700-37b8fa774532",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cba2ca0-a1f1-495a-8c9d-118bc81c5104",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ad537d8-8f5c-440f-a700-37b8fa774532",
                    "LayerId": "6b289812-5498-442a-97ba-03cf4440b8fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6b289812-5498-442a-97ba-03cf4440b8fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5aed1f81-7bb1-4426-8cc6-b7f80ad40771",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}