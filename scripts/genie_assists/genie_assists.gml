if(gx > 299)
{
    if(genie = "justice")
    {
        if(input_check(0,0,0,0,1) && qcf_buffer > qcb_buffer && gxcounter == 0)
        {
            genieobj = instance_create_depth(x,760,depth - 1, obj_justice);
            with(genieobj)
            {
                state_switch("qcf");
                owner = other;
                image_xscale = other.image_xscale;
            }
            gx -= 300;
            gxcounter = 60;
        }
        
        if(input_check(0,0,0,0,1) && qcb_buffer > 0 && gxcounter == 0)
        {
            genieobj = instance_create_depth(x,y,depth - 1, obj_justice);
            with(genieobj)
            {
                state_switch("qcb");
                owner = other;
                image_xscale = other.image_xscale;
            }
            gx -= 300;
            gxcounter = 60;
        }
    }
}