{
    "id": "84c0dcf3-ce5d-496d-8b90-2ef4166ff718",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_momo",
    "eventList": [
        {
            "id": "52174468-e59f-489e-a9b5-f345439af587",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "84c0dcf3-ce5d-496d-8b90-2ef4166ff718"
        },
        {
            "id": "b0ddc941-77ab-4a8f-9ed6-c8d15b7aea51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "84c0dcf3-ce5d-496d-8b90-2ef4166ff718"
        },
        {
            "id": "55961927-916c-43d0-bd73-5493a60b1a3e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "84c0dcf3-ce5d-496d-8b90-2ef4166ff718"
        },
        {
            "id": "bb469555-7ff0-4cf1-b262-9058a00a5782",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "84c0dcf3-ce5d-496d-8b90-2ef4166ff718"
        },
        {
            "id": "01023a85-d919-4ed2-982e-88a7561cafaf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "84c0dcf3-ce5d-496d-8b90-2ef4166ff718"
        },
        {
            "id": "cdd8df8f-32e4-4079-b4a2-c4612cf87653",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "84c0dcf3-ce5d-496d-8b90-2ef4166ff718"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "764a5c31-d766-4159-bbb8-1162177a46bb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
    "visible": true
}