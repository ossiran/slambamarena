/// @description Insert description here
// You can write your code in this editor

// Handle sub-pixel movement
var vxNew, vyNew;
    cx += vx;
    cy += vy;
    vxNew = round(cx);
    vyNew = round(cy);
    cx -= vxNew;
    cy -= vyNew;

x += vxNew;
y += vyNew;

with(obj_projectile_parent)
{
    with(other)
    {
        if(place_meeting(x,y, other))
        {
            if(other.owner != owner)
            {
                hits--;
            }
        }
    }
}