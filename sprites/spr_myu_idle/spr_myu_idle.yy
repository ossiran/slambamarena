{
    "id": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_myu_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 12,
    "bbox_right": 35,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb1135c9-7336-45b7-85e8-0d3a229c03c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "2a2ec77d-d975-44b8-9af8-ac639e5af4d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb1135c9-7336-45b7-85e8-0d3a229c03c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e878f7cc-c233-4bda-a30b-538bd292e8a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb1135c9-7336-45b7-85e8-0d3a229c03c7",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "db35cf6e-cf85-42df-9c25-907cef07f92d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "8d70bfcd-e7ef-4c6a-a6ba-782930f7a21e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db35cf6e-cf85-42df-9c25-907cef07f92d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03cbd4d7-0df9-4081-befa-5ba5fc0284f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db35cf6e-cf85-42df-9c25-907cef07f92d",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "8024fca7-0a2b-4f39-a54c-e06d9feab80a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "533ad476-9b29-4ab3-a122-4905b520badc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8024fca7-0a2b-4f39-a54c-e06d9feab80a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d60c7ca-30ef-4233-949f-bd95e4efdf79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8024fca7-0a2b-4f39-a54c-e06d9feab80a",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "9d51b2fc-3c20-4c75-b01b-b05304a5708a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "996c2314-5fb0-4038-822b-c6e7a903d338",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d51b2fc-3c20-4c75-b01b-b05304a5708a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f33b9fc-808f-4405-ba2c-d3c557586383",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d51b2fc-3c20-4c75-b01b-b05304a5708a",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "fd735e87-2863-4409-954f-10f230e588fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "66fbe293-e999-4b74-9e5a-1ef065c4e90a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd735e87-2863-4409-954f-10f230e588fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f73ba5e-3cb5-45de-80fb-6a02c1aeefe6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd735e87-2863-4409-954f-10f230e588fe",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "65551d9d-8731-427b-bde9-193b96fb7dbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "560d6397-6c00-4f4b-a1ae-37ee33abf25c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65551d9d-8731-427b-bde9-193b96fb7dbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fa59f5e-c561-4b0d-876d-fb93433c4a76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65551d9d-8731-427b-bde9-193b96fb7dbc",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "1524bf84-ba95-445f-b1b8-932f14c343fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "249996b4-2473-481b-8864-cc53cb5c9880",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1524bf84-ba95-445f-b1b8-932f14c343fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f323f1e4-543b-4065-9b03-96ced40c7f2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1524bf84-ba95-445f-b1b8-932f14c343fc",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "06e5fc03-fa26-47ca-8f58-f054e0e2fde3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "b87271d0-6113-444a-9ad1-1acac5ffd208",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06e5fc03-fa26-47ca-8f58-f054e0e2fde3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d34f093-16ef-4acf-8a9d-c89a98f50930",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06e5fc03-fa26-47ca-8f58-f054e0e2fde3",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "3a4a7000-01d1-4ce2-9712-6962e74732e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "a25b2155-e34a-4f4b-94f5-a21b450a7a99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a4a7000-01d1-4ce2-9712-6962e74732e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89c42b90-8b6f-4b9e-a9ad-8f92f34c35cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a4a7000-01d1-4ce2-9712-6962e74732e8",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "0ba3ea4e-2fd8-43e1-9d99-f640ecbfb3be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "5469f936-44c9-48e5-81ee-0476a7b654cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ba3ea4e-2fd8-43e1-9d99-f640ecbfb3be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f352694-5ce7-45af-93d9-3645a5a285af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ba3ea4e-2fd8-43e1-9d99-f640ecbfb3be",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "4ee9f87d-9365-427a-a6e6-12bf48420e75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "ae0e18b0-b574-43f7-8914-87bac71cf838",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ee9f87d-9365-427a-a6e6-12bf48420e75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd384b60-c4a1-4535-9730-c041ea99e6b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ee9f87d-9365-427a-a6e6-12bf48420e75",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "3282ad47-f0d7-409a-9f60-fde9694d58be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "21e2477e-f066-4e12-a415-270ad31cd479",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3282ad47-f0d7-409a-9f60-fde9694d58be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1c24270-303c-4611-9e67-744c8747c610",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3282ad47-f0d7-409a-9f60-fde9694d58be",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "43cbda98-7c7e-4802-8955-24a53082ba93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "9790eb52-793f-4258-a5a6-cf0b1e29efb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43cbda98-7c7e-4802-8955-24a53082ba93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47bab7ca-c78a-445f-83ef-a947dfc1e973",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43cbda98-7c7e-4802-8955-24a53082ba93",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "0bbb5f0f-bd31-4daa-b7cf-eeb25f06b736",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "c1eed324-4651-4c65-83d7-b66c5dd77a77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bbb5f0f-bd31-4daa-b7cf-eeb25f06b736",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ba1febe-8134-46d6-81a9-2b08a77f800c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bbb5f0f-bd31-4daa-b7cf-eeb25f06b736",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "673a1fe4-1860-46d4-bd0f-db2e599b9a87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "d3cf0c86-246b-48a0-a0b8-92e0856f955d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "673a1fe4-1860-46d4-bd0f-db2e599b9a87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f268952c-646b-40c0-96c8-54a45510b98d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "673a1fe4-1860-46d4-bd0f-db2e599b9a87",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "9f73524b-713b-4c73-9cab-a5de4b4628ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "5d369093-fbaf-4a70-ba9f-5d49a1e2ad26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f73524b-713b-4c73-9cab-a5de4b4628ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84dbf467-ba4e-49d3-8acc-9e2757e001a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f73524b-713b-4c73-9cab-a5de4b4628ee",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "eab5df81-b82f-4c47-bb2f-2ad9321ac579",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "bec9d338-84c0-488b-a797-449153f64ebd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eab5df81-b82f-4c47-bb2f-2ad9321ac579",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59dd8590-83c9-4d65-bd13-82cce6c42fc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eab5df81-b82f-4c47-bb2f-2ad9321ac579",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "31e15eee-65af-480b-9b2f-092a83499a5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "e0f36059-9bb8-4e9a-89db-9237221388fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31e15eee-65af-480b-9b2f-092a83499a5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cac77e6e-5a59-4565-8fa8-5761ed3a9308",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31e15eee-65af-480b-9b2f-092a83499a5d",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "f8358fd0-b8a9-4c2f-9fc3-aa726608e27b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "46d7be11-e928-4d0a-a5aa-16d48ff25758",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8358fd0-b8a9-4c2f-9fc3-aa726608e27b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "582081e6-8c61-41cb-8de8-56e9e2063375",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8358fd0-b8a9-4c2f-9fc3-aa726608e27b",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "db4bc40e-39b7-40cb-9dde-175f3f254a6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "377840c1-eb76-47ab-a3ea-1898c2b38f96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db4bc40e-39b7-40cb-9dde-175f3f254a6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1aa6117-52c8-48d3-be8f-35c92988f6d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db4bc40e-39b7-40cb-9dde-175f3f254a6c",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "59cb5543-96d1-4780-a4ee-ca6d331ed50b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "035400ff-fced-4da0-be16-cd55274c2749",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59cb5543-96d1-4780-a4ee-ca6d331ed50b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "467e7751-5534-468d-9363-132d2f094415",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59cb5543-96d1-4780-a4ee-ca6d331ed50b",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "75c07298-3aa4-4805-a6cf-ccfbfe18db90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "dee9d78d-1554-4efc-ae21-54996db17590",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75c07298-3aa4-4805-a6cf-ccfbfe18db90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e83dfcd6-668d-4aba-aba4-edf1870f8f70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75c07298-3aa4-4805-a6cf-ccfbfe18db90",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "8307d91c-9096-41e2-bf74-0891e0f0824f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "daa2b83b-f303-4323-a70e-0964e86d4a66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8307d91c-9096-41e2-bf74-0891e0f0824f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a9b4823-b346-4468-b7fb-bdc5f0e6a837",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8307d91c-9096-41e2-bf74-0891e0f0824f",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "a938a1a2-fa81-4bbe-a53c-9cfb7d7b8cb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "ca1670c1-ff1a-45a7-9c2d-794dafcb1c3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a938a1a2-fa81-4bbe-a53c-9cfb7d7b8cb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c165ea98-1221-4f88-ac5c-77cdf14b0eb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a938a1a2-fa81-4bbe-a53c-9cfb7d7b8cb1",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "ed5af487-b271-4628-9d1b-5d07734427e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "3889c014-d0c9-416d-ac36-99f92aa4c667",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed5af487-b271-4628-9d1b-5d07734427e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "190d8749-2a00-42e2-aa95-a51326a02b34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed5af487-b271-4628-9d1b-5d07734427e9",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "1261c78b-b160-4ad7-bd90-c2db3e9696b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "2d32f788-9ee6-4bd2-9fe1-a2e33de8022c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1261c78b-b160-4ad7-bd90-c2db3e9696b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afeb501a-6007-4c7f-8f9d-cbf6b43c0493",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1261c78b-b160-4ad7-bd90-c2db3e9696b0",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "fbb276d4-5201-4a32-8c6f-b1f63cef9c73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "d01ffa3a-988e-4e00-b4df-712d8ebc2217",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbb276d4-5201-4a32-8c6f-b1f63cef9c73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "584d1d89-650d-4b7c-a30d-35c64bb951b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbb276d4-5201-4a32-8c6f-b1f63cef9c73",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "0c4e85a8-ede4-4665-b20e-ca0459f859e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "55904d4f-88a5-4f81-8ede-eab2427706c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c4e85a8-ede4-4665-b20e-ca0459f859e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e871d03-a502-428f-92ad-ca7adff8ae21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c4e85a8-ede4-4665-b20e-ca0459f859e2",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "be240986-4bea-4ec6-a757-67f4dccb3fc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "1d0b38db-7dc2-429a-a896-3f0bb300edff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be240986-4bea-4ec6-a757-67f4dccb3fc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23714a34-6a3b-41fd-90f2-735812a8b73b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be240986-4bea-4ec6-a757-67f4dccb3fc5",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "972b8823-1a12-4e85-aedc-f08455815db3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "4de6a6f8-dadb-470e-99d2-a8f97bf0983b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "972b8823-1a12-4e85-aedc-f08455815db3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2d208b1-429c-475a-9ee7-6d69b08ec2ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "972b8823-1a12-4e85-aedc-f08455815db3",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "bb3c1fc4-a4b7-459b-ba8a-dc06142d822c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "25c39149-cd49-4040-810b-d0fb5f5a1cb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb3c1fc4-a4b7-459b-ba8a-dc06142d822c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f30cb0dc-298a-4f9a-b35a-4f3f9042c83a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb3c1fc4-a4b7-459b-ba8a-dc06142d822c",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "f03af382-9348-4db9-affc-677739572231",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "7f9f12c5-1882-47fc-b671-7a7d60623347",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f03af382-9348-4db9-affc-677739572231",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dfdc20e-3b7d-459a-b6c3-65a6360cecf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f03af382-9348-4db9-affc-677739572231",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "6f57b634-54b8-480f-b221-eafa1c5ddd5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "0c0d2758-cdee-42cc-ba38-91d20257a569",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f57b634-54b8-480f-b221-eafa1c5ddd5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46b0e94d-26f2-49b6-b3c6-f6b98d5bdb3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f57b634-54b8-480f-b221-eafa1c5ddd5a",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "9fc8fbac-92e3-40ae-986d-3afb059b89fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "aec2961d-3305-412c-b402-bd035a8af531",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fc8fbac-92e3-40ae-986d-3afb059b89fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17b1560f-d978-45cf-9166-c833e85e12f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fc8fbac-92e3-40ae-986d-3afb059b89fe",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        },
        {
            "id": "d92858c4-82f0-4dec-afb6-b52cb8b22186",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "compositeImage": {
                "id": "a56b9c8a-5398-438b-9051-b7e037678144",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d92858c4-82f0-4dec-afb6-b52cb8b22186",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b01b3cdf-6c16-429d-bb67-94e2e4b87c6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d92858c4-82f0-4dec-afb6-b52cb8b22186",
                    "LayerId": "3f29b153-7224-47fe-aa95-3a6904ad0ef7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3f29b153-7224-47fe-aa95-3a6904ad0ef7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db6b4dc0-eca1-4eb8-acf8-4fc4942a6bf9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 32
}