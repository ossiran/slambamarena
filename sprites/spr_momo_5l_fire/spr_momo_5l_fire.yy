{
    "id": "1bed668c-12a7-4772-9280-a28c9f2a12fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_5l_fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f058d3ac-d2dc-4e88-a2e9-64334138790c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bed668c-12a7-4772-9280-a28c9f2a12fc",
            "compositeImage": {
                "id": "7884b5bc-6233-409b-ade2-d3bf1d332ee4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f058d3ac-d2dc-4e88-a2e9-64334138790c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71197ea7-43ce-48bd-891b-e74329f2d8af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f058d3ac-d2dc-4e88-a2e9-64334138790c",
                    "LayerId": "479ec16c-e4a3-42d5-a5f1-c8b0a20c24c7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "479ec16c-e4a3-42d5-a5f1-c8b0a20c24c7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1bed668c-12a7-4772-9280-a28c9f2a12fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}