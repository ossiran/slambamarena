{
    "id": "53073de7-414c-4cc9-a60d-4428bd55e8e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_2m_fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 16,
    "bbox_right": 168,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "974ea9e9-fcf3-4e39-b4f5-365ab583a040",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53073de7-414c-4cc9-a60d-4428bd55e8e8",
            "compositeImage": {
                "id": "d24e5c62-4027-4703-8469-9fb69ff87ebb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "974ea9e9-fcf3-4e39-b4f5-365ab583a040",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d8a5920-175e-4544-a2d5-0a28362f1bdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "974ea9e9-fcf3-4e39-b4f5-365ab583a040",
                    "LayerId": "bde07fad-c2ac-4566-a68e-6f8b0f4548d8"
                }
            ]
        },
        {
            "id": "bc386255-be55-40dc-a54a-f8ec13b3c3e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53073de7-414c-4cc9-a60d-4428bd55e8e8",
            "compositeImage": {
                "id": "8e740534-91f5-4893-9d48-f77c09c26c2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc386255-be55-40dc-a54a-f8ec13b3c3e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6de3696-2260-4f44-96e2-503dfa8b0635",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc386255-be55-40dc-a54a-f8ec13b3c3e9",
                    "LayerId": "bde07fad-c2ac-4566-a68e-6f8b0f4548d8"
                }
            ]
        },
        {
            "id": "c2fc09e9-e9e9-4e1b-9438-36e44f91d2ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53073de7-414c-4cc9-a60d-4428bd55e8e8",
            "compositeImage": {
                "id": "0ef9ca22-3ffb-4501-818e-5ce8adaae27f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2fc09e9-e9e9-4e1b-9438-36e44f91d2ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fd86c64-7b3f-47f5-b50d-868944a51741",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2fc09e9-e9e9-4e1b-9438-36e44f91d2ad",
                    "LayerId": "bde07fad-c2ac-4566-a68e-6f8b0f4548d8"
                }
            ]
        },
        {
            "id": "74f42858-c13a-4799-b95f-86f86c204328",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53073de7-414c-4cc9-a60d-4428bd55e8e8",
            "compositeImage": {
                "id": "2986953e-4cc3-4641-a24e-552f06941244",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74f42858-c13a-4799-b95f-86f86c204328",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7273843-7dab-4937-9438-b7dcfdaf1f04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74f42858-c13a-4799-b95f-86f86c204328",
                    "LayerId": "bde07fad-c2ac-4566-a68e-6f8b0f4548d8"
                }
            ]
        },
        {
            "id": "fc8278fa-9974-4588-8a25-c83a94677a87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53073de7-414c-4cc9-a60d-4428bd55e8e8",
            "compositeImage": {
                "id": "64694d34-db45-41ed-8329-246849b2e1b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc8278fa-9974-4588-8a25-c83a94677a87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "858671ec-7873-4ad6-88d3-0b5f05c99e35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc8278fa-9974-4588-8a25-c83a94677a87",
                    "LayerId": "bde07fad-c2ac-4566-a68e-6f8b0f4548d8"
                }
            ]
        },
        {
            "id": "b955a266-7518-46e6-839f-5a88fe1f4c83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53073de7-414c-4cc9-a60d-4428bd55e8e8",
            "compositeImage": {
                "id": "3857dd62-1a7a-42ed-b6fc-1e2215920a6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b955a266-7518-46e6-839f-5a88fe1f4c83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7ca4c77-c39f-4762-b5f0-9b12e8a94548",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b955a266-7518-46e6-839f-5a88fe1f4c83",
                    "LayerId": "bde07fad-c2ac-4566-a68e-6f8b0f4548d8"
                }
            ]
        },
        {
            "id": "d67817ae-e88d-4476-b9b5-60e4010ae86e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53073de7-414c-4cc9-a60d-4428bd55e8e8",
            "compositeImage": {
                "id": "6cb2d851-315b-468f-be25-b2141fe1b5ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d67817ae-e88d-4476-b9b5-60e4010ae86e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b095cb6a-76e9-43c1-8a4c-1b9b8318fa75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d67817ae-e88d-4476-b9b5-60e4010ae86e",
                    "LayerId": "bde07fad-c2ac-4566-a68e-6f8b0f4548d8"
                }
            ]
        },
        {
            "id": "fb9ab6d4-b5e7-4c8e-8483-fbf4864ccf32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53073de7-414c-4cc9-a60d-4428bd55e8e8",
            "compositeImage": {
                "id": "a00a2bea-6c9a-494d-882e-fa444543391c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb9ab6d4-b5e7-4c8e-8483-fbf4864ccf32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54f55330-8429-43b2-97e2-6089be1e4c5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb9ab6d4-b5e7-4c8e-8483-fbf4864ccf32",
                    "LayerId": "bde07fad-c2ac-4566-a68e-6f8b0f4548d8"
                }
            ]
        },
        {
            "id": "f1a9d0b3-cd33-49c1-aab9-ec7d15bae13d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53073de7-414c-4cc9-a60d-4428bd55e8e8",
            "compositeImage": {
                "id": "4fbd95ca-44cf-4656-914c-ff8caf385015",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1a9d0b3-cd33-49c1-aab9-ec7d15bae13d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9633fbe4-81c8-48bf-84fe-efbb7e84574a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1a9d0b3-cd33-49c1-aab9-ec7d15bae13d",
                    "LayerId": "bde07fad-c2ac-4566-a68e-6f8b0f4548d8"
                }
            ]
        },
        {
            "id": "28915d93-2e17-42b1-b71f-ee2d05ca1602",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53073de7-414c-4cc9-a60d-4428bd55e8e8",
            "compositeImage": {
                "id": "931ae9ca-e192-4907-a483-ce30c7be863e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28915d93-2e17-42b1-b71f-ee2d05ca1602",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1053063e-9fd9-4b35-9b00-59c442043772",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28915d93-2e17-42b1-b71f-ee2d05ca1602",
                    "LayerId": "bde07fad-c2ac-4566-a68e-6f8b0f4548d8"
                }
            ]
        },
        {
            "id": "9b503c33-7f0e-4495-b4f4-e02a1376d39a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53073de7-414c-4cc9-a60d-4428bd55e8e8",
            "compositeImage": {
                "id": "f768fe81-28b2-47e5-9be2-9cd52e8bd2c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b503c33-7f0e-4495-b4f4-e02a1376d39a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d20db35-ad72-419b-b678-63cb6b427895",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b503c33-7f0e-4495-b4f4-e02a1376d39a",
                    "LayerId": "bde07fad-c2ac-4566-a68e-6f8b0f4548d8"
                }
            ]
        },
        {
            "id": "bcd14c03-c3e6-4a69-a3c3-585b3bea0db1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53073de7-414c-4cc9-a60d-4428bd55e8e8",
            "compositeImage": {
                "id": "35a7bd9c-ee47-48f5-b68f-7e56b09300c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcd14c03-c3e6-4a69-a3c3-585b3bea0db1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50808bae-f72f-46cc-b9f1-6618c407aab2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcd14c03-c3e6-4a69-a3c3-585b3bea0db1",
                    "LayerId": "bde07fad-c2ac-4566-a68e-6f8b0f4548d8"
                }
            ]
        },
        {
            "id": "4084b0ad-8725-4149-a439-7dfee65e3efa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53073de7-414c-4cc9-a60d-4428bd55e8e8",
            "compositeImage": {
                "id": "210291c0-c27b-49bc-ae67-89516f0f22a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4084b0ad-8725-4149-a439-7dfee65e3efa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12591786-4957-42b6-9733-9253dd18561d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4084b0ad-8725-4149-a439-7dfee65e3efa",
                    "LayerId": "bde07fad-c2ac-4566-a68e-6f8b0f4548d8"
                }
            ]
        },
        {
            "id": "0050e779-a525-4712-9364-933f5bb7f0e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53073de7-414c-4cc9-a60d-4428bd55e8e8",
            "compositeImage": {
                "id": "4314f251-7534-4c45-8bfc-88f37d287fb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0050e779-a525-4712-9364-933f5bb7f0e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d807a51-77b4-49f1-b6de-0dbd19bf29f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0050e779-a525-4712-9364-933f5bb7f0e6",
                    "LayerId": "bde07fad-c2ac-4566-a68e-6f8b0f4548d8"
                }
            ]
        },
        {
            "id": "b799b665-4379-48cf-a30a-b425d0542a2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53073de7-414c-4cc9-a60d-4428bd55e8e8",
            "compositeImage": {
                "id": "8b53d4d7-aab7-4c5b-8622-e262a94c6359",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b799b665-4379-48cf-a30a-b425d0542a2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79f85ec3-6fc1-4383-b825-fd8ac3667329",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b799b665-4379-48cf-a30a-b425d0542a2e",
                    "LayerId": "bde07fad-c2ac-4566-a68e-6f8b0f4548d8"
                }
            ]
        },
        {
            "id": "768b8704-debc-4613-b03f-e03da3e93406",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53073de7-414c-4cc9-a60d-4428bd55e8e8",
            "compositeImage": {
                "id": "a072d10d-72d9-4b1f-bab1-7354b525deea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "768b8704-debc-4613-b03f-e03da3e93406",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "353fe5cd-7a0c-46b6-ace7-98235a662fde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "768b8704-debc-4613-b03f-e03da3e93406",
                    "LayerId": "bde07fad-c2ac-4566-a68e-6f8b0f4548d8"
                }
            ]
        },
        {
            "id": "ad0306be-62c1-4e50-990e-df0fb8c1e6d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53073de7-414c-4cc9-a60d-4428bd55e8e8",
            "compositeImage": {
                "id": "f67ca4a6-7994-4af9-947c-4c5c9a05f4ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad0306be-62c1-4e50-990e-df0fb8c1e6d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d49d4152-ee65-4723-b18a-3417317551c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad0306be-62c1-4e50-990e-df0fb8c1e6d7",
                    "LayerId": "bde07fad-c2ac-4566-a68e-6f8b0f4548d8"
                }
            ]
        },
        {
            "id": "ce70b0d8-04fe-49b0-b8f2-5d8cdde2a2a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53073de7-414c-4cc9-a60d-4428bd55e8e8",
            "compositeImage": {
                "id": "01b78827-050c-472a-b0b4-d5980cc57afa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce70b0d8-04fe-49b0-b8f2-5d8cdde2a2a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f14ebfb1-1372-458f-9c87-e70dc02e0824",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce70b0d8-04fe-49b0-b8f2-5d8cdde2a2a5",
                    "LayerId": "bde07fad-c2ac-4566-a68e-6f8b0f4548d8"
                }
            ]
        },
        {
            "id": "c43a729c-7634-4dda-a966-55f31c65706b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53073de7-414c-4cc9-a60d-4428bd55e8e8",
            "compositeImage": {
                "id": "456ce000-59d6-456a-9523-cd109aab5539",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c43a729c-7634-4dda-a966-55f31c65706b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "501da559-48b2-40a5-beea-bbdb80b1555d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c43a729c-7634-4dda-a966-55f31c65706b",
                    "LayerId": "bde07fad-c2ac-4566-a68e-6f8b0f4548d8"
                }
            ]
        },
        {
            "id": "94e64592-fa74-4e55-b426-176d97d74e53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53073de7-414c-4cc9-a60d-4428bd55e8e8",
            "compositeImage": {
                "id": "154afae0-d168-4088-8cac-90fa5dc7f8db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94e64592-fa74-4e55-b426-176d97d74e53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4c54043-a291-4310-9cb0-e01692bd1223",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94e64592-fa74-4e55-b426-176d97d74e53",
                    "LayerId": "bde07fad-c2ac-4566-a68e-6f8b0f4548d8"
                }
            ]
        },
        {
            "id": "b6a6ab49-e52c-4139-a339-48ba591e0b3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53073de7-414c-4cc9-a60d-4428bd55e8e8",
            "compositeImage": {
                "id": "0a6a39df-01fb-428a-bf2f-f80546e32452",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6a6ab49-e52c-4139-a339-48ba591e0b3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaddb5a2-8918-427b-89b6-53857ff9369b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6a6ab49-e52c-4139-a339-48ba591e0b3a",
                    "LayerId": "bde07fad-c2ac-4566-a68e-6f8b0f4548d8"
                }
            ]
        },
        {
            "id": "e3d38cf0-b058-4d60-bb0e-eaf1056b23ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53073de7-414c-4cc9-a60d-4428bd55e8e8",
            "compositeImage": {
                "id": "48fb05af-c2eb-46b6-aa21-547d4dc059db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3d38cf0-b058-4d60-bb0e-eaf1056b23ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46239d4c-e3bc-4077-a33a-88d2d064ba91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3d38cf0-b058-4d60-bb0e-eaf1056b23ec",
                    "LayerId": "bde07fad-c2ac-4566-a68e-6f8b0f4548d8"
                }
            ]
        },
        {
            "id": "c0f16b04-126e-489c-8fae-94b9ebdf674f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53073de7-414c-4cc9-a60d-4428bd55e8e8",
            "compositeImage": {
                "id": "49e7aad1-a3de-428d-9b4c-79b90ee9c828",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0f16b04-126e-489c-8fae-94b9ebdf674f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f284600b-c416-4f7a-bf0b-2cf1622ab42b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0f16b04-126e-489c-8fae-94b9ebdf674f",
                    "LayerId": "bde07fad-c2ac-4566-a68e-6f8b0f4548d8"
                }
            ]
        },
        {
            "id": "d4a8a86e-56fa-4a51-b949-f6ea7e79b104",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53073de7-414c-4cc9-a60d-4428bd55e8e8",
            "compositeImage": {
                "id": "18286633-97d9-4ba5-bb5a-3f139e7d4f3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4a8a86e-56fa-4a51-b949-f6ea7e79b104",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "309e6cf2-df9f-41f7-93d4-5174f7fbd5bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4a8a86e-56fa-4a51-b949-f6ea7e79b104",
                    "LayerId": "bde07fad-c2ac-4566-a68e-6f8b0f4548d8"
                }
            ]
        },
        {
            "id": "294aa606-f36c-4595-bfef-4feaceb4bf85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53073de7-414c-4cc9-a60d-4428bd55e8e8",
            "compositeImage": {
                "id": "d98d5672-e628-4b1c-812d-f55323511013",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "294aa606-f36c-4595-bfef-4feaceb4bf85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3650bf1-688a-4d02-9704-0a036eb13047",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "294aa606-f36c-4595-bfef-4feaceb4bf85",
                    "LayerId": "bde07fad-c2ac-4566-a68e-6f8b0f4548d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "bde07fad-c2ac-4566-a68e-6f8b0f4548d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53073de7-414c-4cc9-a60d-4428bd55e8e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 180,
    "xorig": 63,
    "yorig": 87
}