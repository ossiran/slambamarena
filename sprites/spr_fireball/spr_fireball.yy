{
    "id": "141ff86b-6ecc-4fff-beb2-23645844fffa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fireball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 2,
    "bbox_right": 26,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b973f008-27a0-4559-99d6-c15edf6454e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "141ff86b-6ecc-4fff-beb2-23645844fffa",
            "compositeImage": {
                "id": "3f32f3a8-59ea-491f-a8db-4a572a83f085",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b973f008-27a0-4559-99d6-c15edf6454e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "954e8d70-14b3-4279-a63b-1163327e5864",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b973f008-27a0-4559-99d6-c15edf6454e0",
                    "LayerId": "faebb1e1-61c9-4fc4-b264-118a2175c90b"
                }
            ]
        },
        {
            "id": "68047115-67bd-4489-8279-e54e3d5821bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "141ff86b-6ecc-4fff-beb2-23645844fffa",
            "compositeImage": {
                "id": "4deedf3f-5055-4fcc-9f77-3332a3f8c151",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68047115-67bd-4489-8279-e54e3d5821bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a62da9c-e08e-4450-ac0a-eff0c40776b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68047115-67bd-4489-8279-e54e3d5821bb",
                    "LayerId": "faebb1e1-61c9-4fc4-b264-118a2175c90b"
                }
            ]
        },
        {
            "id": "278b9b01-b9a0-4fde-a93e-d4f8154de102",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "141ff86b-6ecc-4fff-beb2-23645844fffa",
            "compositeImage": {
                "id": "d489fdf4-3133-4148-842c-28e8f460958f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "278b9b01-b9a0-4fde-a93e-d4f8154de102",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd164b7b-020f-4abe-9e3b-a3fa3d712573",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "278b9b01-b9a0-4fde-a93e-d4f8154de102",
                    "LayerId": "faebb1e1-61c9-4fc4-b264-118a2175c90b"
                }
            ]
        },
        {
            "id": "3ea7d847-1acd-4225-b327-7bbc2f8e65d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "141ff86b-6ecc-4fff-beb2-23645844fffa",
            "compositeImage": {
                "id": "2161a5ad-40a2-43ad-996c-2fe0ae35c1fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ea7d847-1acd-4225-b327-7bbc2f8e65d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e01884ea-935f-4d41-934c-94927cb25df2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ea7d847-1acd-4225-b327-7bbc2f8e65d5",
                    "LayerId": "faebb1e1-61c9-4fc4-b264-118a2175c90b"
                }
            ]
        },
        {
            "id": "62e12294-2ee2-476f-b931-1a60992b985d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "141ff86b-6ecc-4fff-beb2-23645844fffa",
            "compositeImage": {
                "id": "eb01ce04-13c5-483d-946e-a4e199943d08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62e12294-2ee2-476f-b931-1a60992b985d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39bc3e50-85c9-41e3-856a-366517d22051",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62e12294-2ee2-476f-b931-1a60992b985d",
                    "LayerId": "faebb1e1-61c9-4fc4-b264-118a2175c90b"
                }
            ]
        },
        {
            "id": "a377611b-f88b-4282-8ed7-51201427b9a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "141ff86b-6ecc-4fff-beb2-23645844fffa",
            "compositeImage": {
                "id": "ea9912cf-63eb-41e6-a425-9a5fbf30b3b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a377611b-f88b-4282-8ed7-51201427b9a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0e4aa9a-f423-49ef-9a40-d08573dff097",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a377611b-f88b-4282-8ed7-51201427b9a5",
                    "LayerId": "faebb1e1-61c9-4fc4-b264-118a2175c90b"
                }
            ]
        },
        {
            "id": "43a0f57a-4568-4d4d-a792-28a6a0e512e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "141ff86b-6ecc-4fff-beb2-23645844fffa",
            "compositeImage": {
                "id": "43d67c58-cb37-41bb-a74d-a1e4981caf74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43a0f57a-4568-4d4d-a792-28a6a0e512e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4b8552b-c913-44c2-a4ba-29cbc569f7a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43a0f57a-4568-4d4d-a792-28a6a0e512e5",
                    "LayerId": "faebb1e1-61c9-4fc4-b264-118a2175c90b"
                }
            ]
        },
        {
            "id": "7e46adaf-7ec7-4eed-a6ac-2aa1a9901928",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "141ff86b-6ecc-4fff-beb2-23645844fffa",
            "compositeImage": {
                "id": "f326ef5b-1742-4129-8dea-12f7f5652f47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e46adaf-7ec7-4eed-a6ac-2aa1a9901928",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2997b8e5-94e5-4b60-a1fe-482b5948df93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e46adaf-7ec7-4eed-a6ac-2aa1a9901928",
                    "LayerId": "faebb1e1-61c9-4fc4-b264-118a2175c90b"
                }
            ]
        },
        {
            "id": "f9eb5ee4-93b7-4112-8c07-d43ec747628a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "141ff86b-6ecc-4fff-beb2-23645844fffa",
            "compositeImage": {
                "id": "62e09084-830f-483c-b533-d6eb43f6f003",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9eb5ee4-93b7-4112-8c07-d43ec747628a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a335ad4c-d619-43d1-a916-def3b8f2a970",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9eb5ee4-93b7-4112-8c07-d43ec747628a",
                    "LayerId": "faebb1e1-61c9-4fc4-b264-118a2175c90b"
                }
            ]
        },
        {
            "id": "fc424921-8ea8-4a0b-89c9-47c2fc007262",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "141ff86b-6ecc-4fff-beb2-23645844fffa",
            "compositeImage": {
                "id": "f1923400-4951-48d3-a0ea-7d201205b0bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc424921-8ea8-4a0b-89c9-47c2fc007262",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f7cc963-062a-4c37-bfd1-bcbf462c8342",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc424921-8ea8-4a0b-89c9-47c2fc007262",
                    "LayerId": "faebb1e1-61c9-4fc4-b264-118a2175c90b"
                }
            ]
        },
        {
            "id": "94da588c-7875-442d-8c89-49f12f63c36e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "141ff86b-6ecc-4fff-beb2-23645844fffa",
            "compositeImage": {
                "id": "042b142a-beee-4575-8efb-5391e6614910",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94da588c-7875-442d-8c89-49f12f63c36e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db7e6f73-9e19-47bd-9afc-2c3800c68e9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94da588c-7875-442d-8c89-49f12f63c36e",
                    "LayerId": "faebb1e1-61c9-4fc4-b264-118a2175c90b"
                }
            ]
        },
        {
            "id": "f7cb572e-f415-4ebf-9919-ae86695e5b25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "141ff86b-6ecc-4fff-beb2-23645844fffa",
            "compositeImage": {
                "id": "e7be34a1-cf77-48bb-88ac-2ea828773efe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7cb572e-f415-4ebf-9919-ae86695e5b25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e7cd811-51f0-4629-b694-49b82c1911dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7cb572e-f415-4ebf-9919-ae86695e5b25",
                    "LayerId": "faebb1e1-61c9-4fc4-b264-118a2175c90b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "faebb1e1-61c9-4fc4-b264-118a2175c90b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "141ff86b-6ecc-4fff-beb2-23645844fffa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 10
}