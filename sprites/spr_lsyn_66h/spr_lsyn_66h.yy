{
    "id": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lsyn_66h",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 107,
    "bbox_left": 53,
    "bbox_right": 158,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e7b6de48-997c-4cf4-a14d-968b76a40b09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "926bc1fb-db5f-4d96-a04a-8eac6b009dc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7b6de48-997c-4cf4-a14d-968b76a40b09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec785bf9-ea0f-4fac-9aba-1a895ceeadc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7b6de48-997c-4cf4-a14d-968b76a40b09",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "6caa0839-f381-4f93-a7f9-735478589a5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "6fd0cea6-bc3a-4a9e-9f2f-15832a96106c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6caa0839-f381-4f93-a7f9-735478589a5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "435e37f7-3227-4715-aa97-6eecef0f1461",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6caa0839-f381-4f93-a7f9-735478589a5d",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "f154ac7d-1dd5-4634-aa35-3edf20a4292d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "9c135ebe-3ba1-400e-a0ae-92b483193494",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f154ac7d-1dd5-4634-aa35-3edf20a4292d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09f42712-4081-4656-870d-baa84562c235",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f154ac7d-1dd5-4634-aa35-3edf20a4292d",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "d3309694-0667-43d8-bfd3-4bbdafa0fda2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "8877b0b1-3246-428d-be72-28485cadfbc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3309694-0667-43d8-bfd3-4bbdafa0fda2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7ee896e-4cbd-4692-b46e-868b9dc1a484",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3309694-0667-43d8-bfd3-4bbdafa0fda2",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "766f9763-7529-46a8-af29-f61d5b20ec19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "49c3068e-49ed-4a1a-99c6-93f33292172b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "766f9763-7529-46a8-af29-f61d5b20ec19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "685fa998-6ecf-42f7-bfb0-d75cef432072",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "766f9763-7529-46a8-af29-f61d5b20ec19",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "84d091a4-b2fe-4d53-9f8d-d335fd1ded5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "cc696d28-db74-48bf-812d-11ff8d8f635d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84d091a4-b2fe-4d53-9f8d-d335fd1ded5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff9e8a96-a27c-4b1f-bf5e-18d0b8589b75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84d091a4-b2fe-4d53-9f8d-d335fd1ded5b",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "e6770f83-c6a2-4db4-bf87-ea95e3c10a52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "8a1401b2-c0ae-4e60-8d99-1ccdf4542e2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6770f83-c6a2-4db4-bf87-ea95e3c10a52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81160e9d-298f-4e69-9b5a-1b879e92b01e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6770f83-c6a2-4db4-bf87-ea95e3c10a52",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "e7eede7e-f241-4b20-b7ae-a6052decaba5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "0c6f3215-6b3f-41e6-bcf0-3b63e6465d77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7eede7e-f241-4b20-b7ae-a6052decaba5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e123734d-0b18-49b4-9122-88029765c23f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7eede7e-f241-4b20-b7ae-a6052decaba5",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "a3c63ea7-b10e-444c-b043-71468b5232ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "2f058b0d-fcb6-44a1-b5d4-ce5255850439",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3c63ea7-b10e-444c-b043-71468b5232ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84f49a28-690b-4d2e-a019-e084dabb3f73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3c63ea7-b10e-444c-b043-71468b5232ca",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "46ae6034-90bd-489b-b6fc-8470c9bbc8a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "16ce2c9e-3f3b-423c-ab02-df5a91833378",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46ae6034-90bd-489b-b6fc-8470c9bbc8a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8991a60a-6ffd-4078-9192-3efe2e892911",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46ae6034-90bd-489b-b6fc-8470c9bbc8a0",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "0051dd5c-3def-4b42-ac60-fbc36bddeb21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "1e85ec87-10dd-4a80-ab1b-57b31da49b87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0051dd5c-3def-4b42-ac60-fbc36bddeb21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4a5368e-96c3-46fc-92a9-5cfe1dca0079",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0051dd5c-3def-4b42-ac60-fbc36bddeb21",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "5415b45b-fea1-40a5-a8d8-443ce3f61189",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "3370bda3-2df1-458a-8b9f-05526fad1d73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5415b45b-fea1-40a5-a8d8-443ce3f61189",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99a69ad9-52ae-476b-96b7-2384b2fe95e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5415b45b-fea1-40a5-a8d8-443ce3f61189",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "e56c8eba-dbee-4539-be4a-a9949b116417",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "f831b56a-7b5a-4db9-8258-c1f5ad3b3f37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e56c8eba-dbee-4539-be4a-a9949b116417",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e6fcf43-ac6a-4f2d-89b8-93e6268dd11f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e56c8eba-dbee-4539-be4a-a9949b116417",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "db8bc9ea-70bf-4268-9e28-54d3570f6756",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "53c34744-fc17-4e67-942a-f008df5570ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db8bc9ea-70bf-4268-9e28-54d3570f6756",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40a259be-982f-4686-a94b-46ed988b986d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db8bc9ea-70bf-4268-9e28-54d3570f6756",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "949c2ddc-42af-48e3-b79d-3e46a376f4b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "1d0abe36-0e64-4647-903f-c96a4e679d5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "949c2ddc-42af-48e3-b79d-3e46a376f4b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83875949-7345-4127-a96b-1a77dfb814f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "949c2ddc-42af-48e3-b79d-3e46a376f4b6",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "a001e402-2245-480c-b069-0cdc10f4b798",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "37399955-33c7-4ff3-b107-62c6cf039828",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a001e402-2245-480c-b069-0cdc10f4b798",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "640d4acf-85fc-496c-93fa-793d010f6e44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a001e402-2245-480c-b069-0cdc10f4b798",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "c3b437e0-e0ef-44fd-98da-03d5d9a5010f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "5aacdfa6-961a-4e7c-857f-8d30705dd999",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3b437e0-e0ef-44fd-98da-03d5d9a5010f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a52418c-f549-4e25-aa7f-8f7a427a802c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3b437e0-e0ef-44fd-98da-03d5d9a5010f",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "82ea155f-62aa-484e-879b-f9b72b299600",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "e6d9582c-1452-40ac-a6dc-9a209fef50fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82ea155f-62aa-484e-879b-f9b72b299600",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a739eb92-e65f-45ac-ad49-395dc45d00d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82ea155f-62aa-484e-879b-f9b72b299600",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "0d1ff0c6-0380-4658-a630-75aaf1c531f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "2c56500a-bdc3-4477-a4a2-8bccdc2fbf43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d1ff0c6-0380-4658-a630-75aaf1c531f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "418640be-4069-41ca-af63-a08c3d7d50db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d1ff0c6-0380-4658-a630-75aaf1c531f5",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "d108de87-09ea-43ac-ad82-4736dac2f987",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "7c1c926c-a03e-4a99-9378-5a79906eb0d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d108de87-09ea-43ac-ad82-4736dac2f987",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e073c21b-1ecd-43b1-82a3-ba29d6bc8368",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d108de87-09ea-43ac-ad82-4736dac2f987",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "a18f4323-ab1b-4fec-9673-4d2366513da7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "b7e8feb5-3792-4b0b-b7a3-64ed401651ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a18f4323-ab1b-4fec-9673-4d2366513da7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddb69f0e-1472-4082-bee4-4132b7aafd9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a18f4323-ab1b-4fec-9673-4d2366513da7",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "e774a956-cf35-44e1-84fe-d8f2a982919d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "5a24e506-1783-4e24-bd08-3fa0b0171b41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e774a956-cf35-44e1-84fe-d8f2a982919d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7c9ba2a-6c20-4f2c-9773-b13c3826d62b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e774a956-cf35-44e1-84fe-d8f2a982919d",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "6853f52c-26d7-4d1e-8f5c-5f54bc364abb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "3ffb9c99-0ae1-4b29-b177-9b73274f31af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6853f52c-26d7-4d1e-8f5c-5f54bc364abb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecd7e371-908d-4f50-be98-d04b3d48607f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6853f52c-26d7-4d1e-8f5c-5f54bc364abb",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "20231b73-db16-4ada-ad7d-adbf5e9c44d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "6bae0a2b-637d-4ff1-bb50-c1606cd44011",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20231b73-db16-4ada-ad7d-adbf5e9c44d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7da76164-d106-4e12-a1c0-787ae7e71ebb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20231b73-db16-4ada-ad7d-adbf5e9c44d0",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "00c7badd-a426-4da3-9c88-b018df010782",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "4a425296-7898-46d6-afec-4e832bd53d81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00c7badd-a426-4da3-9c88-b018df010782",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6652065e-dd56-4574-929b-db4eaeb59c97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00c7badd-a426-4da3-9c88-b018df010782",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "4a67ed8f-0ceb-4cb6-b29f-521195f2f7c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "cc26b28f-7474-4d34-91b3-497a8175ac7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a67ed8f-0ceb-4cb6-b29f-521195f2f7c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe8542e6-1546-4f3b-929b-574da615412b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a67ed8f-0ceb-4cb6-b29f-521195f2f7c3",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "36ba8165-5b86-4d61-94d9-40a8617cd7e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "23feabdc-1863-44ec-9f0f-025e2cd9b426",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36ba8165-5b86-4d61-94d9-40a8617cd7e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b97a67d6-e539-4120-9dea-ae141e60dabf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36ba8165-5b86-4d61-94d9-40a8617cd7e4",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "0bb5cbb4-2ecb-4377-99d8-4d4ec302d782",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "e19eb507-277b-4cf3-9d82-0106a86f0796",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bb5cbb4-2ecb-4377-99d8-4d4ec302d782",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "895cc100-ede8-4aba-8e93-489b35fe4ab0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bb5cbb4-2ecb-4377-99d8-4d4ec302d782",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "29761195-dae2-4047-9f24-ceadd9815333",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "2a5cbaf9-51d8-45be-bdc3-20e768d56eeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29761195-dae2-4047-9f24-ceadd9815333",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6a456bc-3d6f-4a3f-a9ab-b4bbf7f19ba4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29761195-dae2-4047-9f24-ceadd9815333",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "0ee07895-3e97-43c6-adb0-6975aaae1ed8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "da824099-cfa5-4ba5-8da2-e7e2f6775d16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ee07895-3e97-43c6-adb0-6975aaae1ed8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13951575-0af8-4f9d-b2d9-6fb1344564a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ee07895-3e97-43c6-adb0-6975aaae1ed8",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "6243fe79-aec3-472b-aae2-09a51f3074fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "72c211ee-ef5d-495c-a635-89f57c66bb15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6243fe79-aec3-472b-aae2-09a51f3074fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98e5834d-fdb8-495a-8051-bb6890879d02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6243fe79-aec3-472b-aae2-09a51f3074fe",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "8135ab97-e1a8-4587-9308-8a5fb28977a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "ea3f2ca7-75eb-434a-9441-5f7e251e52a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8135ab97-e1a8-4587-9308-8a5fb28977a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8c62019-c237-445f-8dc1-b7158782b902",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8135ab97-e1a8-4587-9308-8a5fb28977a0",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "f250a6db-dfe0-4103-997d-4339720f40da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "f02af9d4-8278-4ac1-93f9-39cf924ddc88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f250a6db-dfe0-4103-997d-4339720f40da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c066040f-1049-43a1-a5f5-b242207a95ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f250a6db-dfe0-4103-997d-4339720f40da",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "05b06d60-024f-4681-b94f-1840dc3b45b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "79cb030b-0d80-4baf-bc4c-0ba0eda306bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05b06d60-024f-4681-b94f-1840dc3b45b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e13ab3f-bae9-49f1-81d4-789245ba5dc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05b06d60-024f-4681-b94f-1840dc3b45b7",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "46b0a9b5-cef2-4ddc-868d-e47a8574f5db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "de83fbf3-cadc-48f9-9847-40ae573948ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46b0a9b5-cef2-4ddc-868d-e47a8574f5db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27ce9979-89e3-4cb6-86ee-4183df7c1278",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46b0a9b5-cef2-4ddc-868d-e47a8574f5db",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        },
        {
            "id": "365a6e06-1cc0-48a5-a57a-08699dd8fffb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "compositeImage": {
                "id": "3c6c26b3-6b38-4587-ab2b-956e0a94adbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "365a6e06-1cc0-48a5-a57a-08699dd8fffb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "380b3bf5-3955-4a02-b021-ad31e625a21c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "365a6e06-1cc0-48a5-a57a-08699dd8fffb",
                    "LayerId": "b3c27289-96fc-4cb9-a328-6d9367dd3d08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "b3c27289-96fc-4cb9-a328-6d9367dd3d08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "29dea8a1-b505-4b20-b9cc-bf756c5d8b5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 168,
    "xorig": 85,
    "yorig": 80
}