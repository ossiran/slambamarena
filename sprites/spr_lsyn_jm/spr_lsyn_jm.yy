{
    "id": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lsyn_jm",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 47,
    "bbox_right": 112,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b14300f3-4e32-4321-8a78-9374c633834c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "07184737-5c6c-4b06-a5c3-7a20c4d13093",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b14300f3-4e32-4321-8a78-9374c633834c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d65b71a1-d1c0-478f-8ba1-fa647eff9261",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b14300f3-4e32-4321-8a78-9374c633834c",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "d19a82c4-ddb7-451c-854b-d109c952c31b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "3d746950-e6a7-46f3-870a-cfcbf3a6aff1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d19a82c4-ddb7-451c-854b-d109c952c31b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9733c43b-3e86-475d-8b58-ad70c91e49b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d19a82c4-ddb7-451c-854b-d109c952c31b",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "21d178ab-f1cc-47ce-b5b8-47f1d7acb550",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "86e98619-5139-439d-957f-64ce38dc2149",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21d178ab-f1cc-47ce-b5b8-47f1d7acb550",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a7a20cb-dcf3-4e17-996c-8c8f7e80a7fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21d178ab-f1cc-47ce-b5b8-47f1d7acb550",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "b759f6d1-8721-4869-b85b-5a6fc7d9f90b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "f3d14936-f8ee-47f8-a77b-50ec3a4b0cd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b759f6d1-8721-4869-b85b-5a6fc7d9f90b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc450152-710c-4614-97c6-59ecc566db7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b759f6d1-8721-4869-b85b-5a6fc7d9f90b",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "178fcbf7-3ae1-42ba-b402-a8f5ea52eb2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "ac754867-8661-4056-a0a6-06d48baa29de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "178fcbf7-3ae1-42ba-b402-a8f5ea52eb2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0a62a08-9ecd-4410-afda-f803874cb469",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "178fcbf7-3ae1-42ba-b402-a8f5ea52eb2c",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "6395593d-e3f1-46ac-8766-883afe29956d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "e5f02fde-37d8-419c-87ea-a6c58ca32c3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6395593d-e3f1-46ac-8766-883afe29956d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "377294d8-c78c-44af-88b8-0ae82cceefcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6395593d-e3f1-46ac-8766-883afe29956d",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "f0cc2df5-6b35-419d-a108-706474ad3d50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "10f474e8-0127-4503-8f79-b73ee61722bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0cc2df5-6b35-419d-a108-706474ad3d50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0de302e7-9298-44f1-a6d3-4f8bdbd5b86f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0cc2df5-6b35-419d-a108-706474ad3d50",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "72c12e97-e81f-41af-8fc1-c51a211a1ca9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "9e4e0b84-5591-4da7-971a-2aecc31ad503",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72c12e97-e81f-41af-8fc1-c51a211a1ca9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea05067c-ac70-4689-bd25-fbd64038d421",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72c12e97-e81f-41af-8fc1-c51a211a1ca9",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "bc37cbe1-18f4-4486-bdd4-3c3b814ad8a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "00177175-8b5d-4fae-85f2-6f48a05d67c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc37cbe1-18f4-4486-bdd4-3c3b814ad8a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a1939ec-9eb1-4feb-8399-e41833a202cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc37cbe1-18f4-4486-bdd4-3c3b814ad8a5",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "f4b640bb-7ddd-4abc-b2e7-5b6f73597efe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "68a24a58-7b5b-49d9-877b-ee99207fdb6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4b640bb-7ddd-4abc-b2e7-5b6f73597efe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "834624ea-9f09-487c-b401-825b35783938",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4b640bb-7ddd-4abc-b2e7-5b6f73597efe",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "5f17a995-1655-4a40-83c6-6d6e3fed4758",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "10724fba-1244-4a29-a954-49890b5ee2f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f17a995-1655-4a40-83c6-6d6e3fed4758",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f762c5e8-b75c-4f7f-a625-836f2e956cd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f17a995-1655-4a40-83c6-6d6e3fed4758",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "a8956d2c-3ca3-40bc-afb8-301037e290d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "03ee93f0-eae6-4697-94fe-2374035c2ee0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8956d2c-3ca3-40bc-afb8-301037e290d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef5f0cd9-e0d4-4ca3-a6a3-7661f7488306",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8956d2c-3ca3-40bc-afb8-301037e290d0",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "2baa79bc-ef37-48c0-9118-3465e76efacc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "21235328-f3f2-4679-83a0-d406a6d1ef85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2baa79bc-ef37-48c0-9118-3465e76efacc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21e8bb44-f008-491f-9bc9-5e18cd33e0a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2baa79bc-ef37-48c0-9118-3465e76efacc",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "519d3475-9858-42fe-aa57-995abaf36c95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "664750b4-f2ab-427f-bdc4-f4ed74dfd724",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "519d3475-9858-42fe-aa57-995abaf36c95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34b8082d-8fa6-4d9a-9898-df339686d9ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "519d3475-9858-42fe-aa57-995abaf36c95",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "05c68351-01a3-40bd-ae32-9b9bad86fa71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "09646218-1487-4a7d-813e-b15c3f1ed074",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05c68351-01a3-40bd-ae32-9b9bad86fa71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df8b6f35-d1de-40c7-b889-870cdd943e04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05c68351-01a3-40bd-ae32-9b9bad86fa71",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "61ffcd77-9698-4bbc-9ce2-a0f3fa1b3a38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "ccbd3370-e862-42cb-bac0-17d55eb541ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61ffcd77-9698-4bbc-9ce2-a0f3fa1b3a38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0eae7420-533a-46bb-92b8-6d89725ea581",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61ffcd77-9698-4bbc-9ce2-a0f3fa1b3a38",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "46c78437-1c7a-4e75-9613-249e2ea2901c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "9cb1bff5-7e5d-4a5b-bb20-0937edc7cd52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46c78437-1c7a-4e75-9613-249e2ea2901c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f060141-dc83-489a-b089-0704f7fc58df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46c78437-1c7a-4e75-9613-249e2ea2901c",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "a05ae33d-2a68-4dc5-9cc4-4cf267b5d3aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "21cbef8b-2a01-428c-be05-4174984554e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a05ae33d-2a68-4dc5-9cc4-4cf267b5d3aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62b5738c-e49c-4a1c-97e3-c0b5d3b0c5d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a05ae33d-2a68-4dc5-9cc4-4cf267b5d3aa",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "3395cae3-6887-419a-8558-e830cd10b08a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "c73a178b-ff9e-4adf-a6f9-e106c3fbe80f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3395cae3-6887-419a-8558-e830cd10b08a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45fe0ac1-4cf5-4258-9449-e2211bf4ec37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3395cae3-6887-419a-8558-e830cd10b08a",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "faa944ea-7cd0-46af-a76a-62bc3bafa934",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "196eda8c-8695-401c-8820-a4beb46c56cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "faa944ea-7cd0-46af-a76a-62bc3bafa934",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39c960e2-3a5b-423e-beb1-ce16948abd79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "faa944ea-7cd0-46af-a76a-62bc3bafa934",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "c6fbbae3-30d0-4e2d-a12b-39d488f583aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "64cb3817-033a-4997-bfc0-c5c553d1f029",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6fbbae3-30d0-4e2d-a12b-39d488f583aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0465b6f7-4ef0-491e-8687-0f822a7169ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6fbbae3-30d0-4e2d-a12b-39d488f583aa",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "292c851f-5904-44c5-9505-ef88db6ac02a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "c459b6c6-b611-4979-b27f-711ef6dfcdd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "292c851f-5904-44c5-9505-ef88db6ac02a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0029961-6bb1-4ccc-824e-f372200ae811",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "292c851f-5904-44c5-9505-ef88db6ac02a",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "3d3ba218-d292-4801-bfbf-a1c3a21a0564",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "02ce04a5-424b-4f9e-80a9-55856ef29ba9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d3ba218-d292-4801-bfbf-a1c3a21a0564",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1233a48c-a217-4fe8-8b45-166588e302af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d3ba218-d292-4801-bfbf-a1c3a21a0564",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "7aabe2bc-d26d-4230-917e-22e51d9fbbba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "d90fca06-b2e8-4d6e-825f-34365e56988d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7aabe2bc-d26d-4230-917e-22e51d9fbbba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e0639cc-d223-4cf0-98dc-497957c7079f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7aabe2bc-d26d-4230-917e-22e51d9fbbba",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "90d0daeb-d0b3-43d4-846f-b720580f5f5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "306ffd5d-6bed-4db7-97ce-0b8d8a12aceb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90d0daeb-d0b3-43d4-846f-b720580f5f5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6ad8f2d-afb7-4a5c-9b78-5f9db82f6638",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90d0daeb-d0b3-43d4-846f-b720580f5f5d",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "0f68cbf5-67ef-4006-ab2b-806f851addd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "a3ab3ce9-37ad-4209-a5aa-6cb0196c273d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f68cbf5-67ef-4006-ab2b-806f851addd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a78578c-a997-4c80-96e1-219697d996db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f68cbf5-67ef-4006-ab2b-806f851addd9",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "7cb3528a-048e-432c-aad7-42f2388fd05d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "395b224a-de25-4532-bf74-b1614056dc8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cb3528a-048e-432c-aad7-42f2388fd05d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f0dc971-3bcb-4a2c-b03d-0ac5dedb2d66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cb3528a-048e-432c-aad7-42f2388fd05d",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "fa70e8c5-0c54-43d6-b87c-f27f0adcfda7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "347d2199-ff12-478d-8cd9-8db5dc9df929",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa70e8c5-0c54-43d6-b87c-f27f0adcfda7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b808d3c1-81c5-4d40-a579-b4c938fa2d71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa70e8c5-0c54-43d6-b87c-f27f0adcfda7",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "e9f1254d-27f6-4449-b01b-7538cdbbd97b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "7214c6f3-3746-4927-997b-4780d2766304",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9f1254d-27f6-4449-b01b-7538cdbbd97b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8e79ad8-e815-460e-b7e4-72635ecdccdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9f1254d-27f6-4449-b01b-7538cdbbd97b",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "3de0dbc9-22a4-41ec-bb01-d2f2346e79ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "d496b113-ceab-4043-a05f-8bd7e78b8ecc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3de0dbc9-22a4-41ec-bb01-d2f2346e79ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1b920c9-8205-44ef-801d-6f2930916473",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3de0dbc9-22a4-41ec-bb01-d2f2346e79ab",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        },
        {
            "id": "ac3bcb13-27af-4b94-83df-66b62ef90267",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "compositeImage": {
                "id": "1add7e22-6f66-48b2-99da-38be74e62cb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac3bcb13-27af-4b94-83df-66b62ef90267",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63f3c49a-db29-4ac0-acca-74bc4677a526",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac3bcb13-27af-4b94-83df-66b62ef90267",
                    "LayerId": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "8982cac6-be83-47a3-b9e6-cf10f8a1d9c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6e5809cc-7e9c-4b66-b281-60ddc1c6385f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}