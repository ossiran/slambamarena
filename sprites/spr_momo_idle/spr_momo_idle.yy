{
    "id": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 24,
    "bbox_right": 115,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d05d05a-730c-4e27-84cd-33111335fecf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "d2bf7a2e-7cc2-412f-872d-41c610c5a975",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d05d05a-730c-4e27-84cd-33111335fecf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "838aa27f-829b-4417-a514-d202540138ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d05d05a-730c-4e27-84cd-33111335fecf",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "57a793c1-454e-437e-9064-32d0f9efe4d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "9f10b011-51a3-4245-8bf9-62684e2364c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57a793c1-454e-437e-9064-32d0f9efe4d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e77c26e0-2b68-45ed-b130-ed177acab2ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57a793c1-454e-437e-9064-32d0f9efe4d5",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "8a7fb31e-5cf1-4748-afdf-8e713c2a026c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "efeca589-1577-4917-a055-f21e17ebe4ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a7fb31e-5cf1-4748-afdf-8e713c2a026c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c462d273-c118-49d1-8abf-9dfc19305135",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a7fb31e-5cf1-4748-afdf-8e713c2a026c",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "6e7cd443-1fbe-436f-95f0-f7170781f03c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "e823f737-e4d9-4809-a216-8112c83c22ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e7cd443-1fbe-436f-95f0-f7170781f03c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdfd2631-86a0-4b9e-baf3-73a0aa8215fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e7cd443-1fbe-436f-95f0-f7170781f03c",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "f5fff331-12e8-4351-925e-03bf7981b3f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "b550ab6f-9b6c-45a6-b2fe-d29614e02f76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5fff331-12e8-4351-925e-03bf7981b3f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0afd543-07ab-456b-8586-0e008f020701",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5fff331-12e8-4351-925e-03bf7981b3f5",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "434a2810-bad3-46bf-98b6-7de68f22d701",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "0629d8b4-637d-44e0-a63f-7b25d223a154",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "434a2810-bad3-46bf-98b6-7de68f22d701",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5865efd8-a85f-4ad4-af0b-02a94e5e4701",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "434a2810-bad3-46bf-98b6-7de68f22d701",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "614a9814-08e8-4be0-a0ad-990ceadee289",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "b7002a0f-96f5-4cc4-866b-592133a0ccb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "614a9814-08e8-4be0-a0ad-990ceadee289",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad5c56d4-610e-4580-8525-bd485c322b34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "614a9814-08e8-4be0-a0ad-990ceadee289",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "3b4d9f0b-618d-45f9-805b-b1844360f31c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "cc4fb2c6-19f5-4a33-b5f7-277a66734fe0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b4d9f0b-618d-45f9-805b-b1844360f31c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f47fc55b-be43-40e4-9e10-8f70db38593c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b4d9f0b-618d-45f9-805b-b1844360f31c",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "bc19d016-b2bd-4718-9f6a-81e3bbc25dfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "e334702e-20ab-4184-8f12-3183527bd3cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc19d016-b2bd-4718-9f6a-81e3bbc25dfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08744a9a-dbb1-47f0-8531-80407ebc5c82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc19d016-b2bd-4718-9f6a-81e3bbc25dfe",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "ca397d15-682e-4b6c-a62d-2e3f0b6a343c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "9b8ce806-0b44-4f82-8410-297d1021d650",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca397d15-682e-4b6c-a62d-2e3f0b6a343c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae49d97c-04a5-4846-ae61-a165399b5996",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca397d15-682e-4b6c-a62d-2e3f0b6a343c",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "410483b8-4659-4513-95e1-32a34002a9b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "beaee021-1323-4ab8-b2fa-bb993aa53c6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "410483b8-4659-4513-95e1-32a34002a9b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7833dcd-2a0a-4206-9cca-3ed27416f8b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "410483b8-4659-4513-95e1-32a34002a9b8",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "f4552d80-7d7a-4878-89de-f7fd4e3913e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "68c44333-734d-4a7e-84ca-2caec3b2f209",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4552d80-7d7a-4878-89de-f7fd4e3913e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d67297c-d411-49e0-b347-bb02961b9fff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4552d80-7d7a-4878-89de-f7fd4e3913e0",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "3c466796-bd83-4e47-89ae-22a0eafcda59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "8f103041-f85a-483a-82ed-bfae98fb9fd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c466796-bd83-4e47-89ae-22a0eafcda59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06f6344b-dbba-4000-8759-582aac1be978",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c466796-bd83-4e47-89ae-22a0eafcda59",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "ff372c59-2186-469c-b4cd-0bfed783c195",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "b4ad2cfc-444f-4aad-9ac5-1dd4ff88a6ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff372c59-2186-469c-b4cd-0bfed783c195",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fad27dfd-5e8d-4c7e-9db6-feceea1fb652",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff372c59-2186-469c-b4cd-0bfed783c195",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "89bfaf7b-06fe-4244-9f87-0c4c9ac1b569",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "456dcc03-0d11-4cd9-a264-5102b09ed428",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89bfaf7b-06fe-4244-9f87-0c4c9ac1b569",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0153135c-41f8-4bbc-bff2-032d56d99777",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89bfaf7b-06fe-4244-9f87-0c4c9ac1b569",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "1c25b4ae-3cf0-4917-996f-16dca4c5d120",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "0e0e5d15-8e29-4631-b063-682d83c2d638",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c25b4ae-3cf0-4917-996f-16dca4c5d120",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daf26ec1-2081-4318-8ce6-c3fc15502b62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c25b4ae-3cf0-4917-996f-16dca4c5d120",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "ef7689db-b791-4702-9a2d-1eb7966d423a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "d586c91f-551e-45c8-ade7-8e1222a9b018",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef7689db-b791-4702-9a2d-1eb7966d423a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e311b44b-40e5-47b0-a116-c9f2d421d6ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef7689db-b791-4702-9a2d-1eb7966d423a",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "7a4d009c-6bcd-4a92-95c8-9d1bff5a6a1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "6f4ab158-f744-4065-b079-2b6bbaad46b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a4d009c-6bcd-4a92-95c8-9d1bff5a6a1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e99f8b6d-2870-48e0-ab6b-c3bcf289a273",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a4d009c-6bcd-4a92-95c8-9d1bff5a6a1c",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "35998e5a-e2b0-4e98-bc80-c952d6250242",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "224da510-6269-40e6-93bb-b68de582764c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35998e5a-e2b0-4e98-bc80-c952d6250242",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6efa6ba1-2b4f-4102-a0a9-b7d3d60a19f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35998e5a-e2b0-4e98-bc80-c952d6250242",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "d51fd2b6-07b4-40b4-bb51-9509a5ee8368",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "5abc210c-7891-4dbc-a823-2237e1d87a5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d51fd2b6-07b4-40b4-bb51-9509a5ee8368",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02f8cdd8-e88b-493b-968c-92b5c998032b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d51fd2b6-07b4-40b4-bb51-9509a5ee8368",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "7e0b7eed-aae7-41ed-81a4-6f8e0653b8c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "486878c6-b744-4b52-bcc3-17c4c4d0360b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e0b7eed-aae7-41ed-81a4-6f8e0653b8c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad099aec-1d12-4c7d-be5f-bc11e419cb81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e0b7eed-aae7-41ed-81a4-6f8e0653b8c8",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "3a6b1862-d5f4-4e24-9c32-692ba4c39ca8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "15ec798d-9b50-4a74-8956-49bdd001fcc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a6b1862-d5f4-4e24-9c32-692ba4c39ca8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2ecf630-bc03-473f-90f3-8bf7f7677fc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a6b1862-d5f4-4e24-9c32-692ba4c39ca8",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "a16f47b1-138d-400d-9102-069d85b3e2a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "827279d5-1a73-4cfe-a62b-9b75b35c7b7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a16f47b1-138d-400d-9102-069d85b3e2a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bffd902-c9f5-4a7a-9f17-088fc5fb7893",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a16f47b1-138d-400d-9102-069d85b3e2a5",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "34783c4c-0fcc-4e15-aaee-b97e1eabd280",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "2894c20a-822c-479e-8af0-58303bf9dde2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34783c4c-0fcc-4e15-aaee-b97e1eabd280",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fc6a00f-3185-47c5-ac29-ec1381ceacde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34783c4c-0fcc-4e15-aaee-b97e1eabd280",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "40527706-c55d-4721-a71d-acf3bfadc4e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "5a663abb-fcb3-4848-8257-65422526de08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40527706-c55d-4721-a71d-acf3bfadc4e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de71bcbe-6a78-4b86-9ab1-154bf58eaed4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40527706-c55d-4721-a71d-acf3bfadc4e5",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "229f7613-0fd8-4650-af61-877ac696b521",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "6d605246-9e1f-48c6-b949-0d92858d99b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "229f7613-0fd8-4650-af61-877ac696b521",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0057e160-aeec-45c8-aa72-e73ae9b462fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "229f7613-0fd8-4650-af61-877ac696b521",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "9fd0bfc2-d5ed-44a7-ac9e-8cc660e6403d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "3b7da45f-3917-44e7-b225-d8006795638f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fd0bfc2-d5ed-44a7-ac9e-8cc660e6403d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff83770e-e4b8-4c1b-a2f7-93caf439d734",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fd0bfc2-d5ed-44a7-ac9e-8cc660e6403d",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "b721fb9c-e200-4a44-9422-2de17554d117",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "26e3bd14-a867-4d42-9069-abad0cfb17e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b721fb9c-e200-4a44-9422-2de17554d117",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e1708f9-603c-432d-90cc-4ba9eb1b388d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b721fb9c-e200-4a44-9422-2de17554d117",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "4db02377-0f7e-434b-89c4-91035445e504",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "81bf8b00-fb9b-4f8c-b034-338409d8dbfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4db02377-0f7e-434b-89c4-91035445e504",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "737429c5-b31d-4e4e-8626-a49bc05092c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4db02377-0f7e-434b-89c4-91035445e504",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "8e5f14df-df41-4bda-8cb0-2b1221eeebe5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "01ee91d8-c6dd-4f57-b035-6452fb9f0de0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e5f14df-df41-4bda-8cb0-2b1221eeebe5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bb66233-72a5-4401-9ff6-564cb32b7313",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e5f14df-df41-4bda-8cb0-2b1221eeebe5",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "28b71496-f9f5-45e1-92af-a70997119750",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "4630f9f8-8abd-4d32-bc92-b0f1acc36f40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28b71496-f9f5-45e1-92af-a70997119750",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46671a53-efab-41a4-8573-b0c52326ec4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28b71496-f9f5-45e1-92af-a70997119750",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "163ea9b6-2b50-498c-be79-74bbfb82d4c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "0629291e-aedb-4131-a511-61951853e12b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "163ea9b6-2b50-498c-be79-74bbfb82d4c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48f4d8f8-4b97-4d03-a9f6-6c15cb0d5fd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "163ea9b6-2b50-498c-be79-74bbfb82d4c3",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "edb596ad-41dc-4201-b175-41f27f09345a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "e814344f-9746-4767-83c3-3d78ef009854",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edb596ad-41dc-4201-b175-41f27f09345a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bd00f6b-820c-4b8b-abdb-e29a53dd633d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edb596ad-41dc-4201-b175-41f27f09345a",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "2f984986-61c0-4f42-9eac-1e7d1ad88c78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "75fea92f-4e53-4322-813d-bc5ca7a93c18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f984986-61c0-4f42-9eac-1e7d1ad88c78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98aa2e6e-b28d-4bb1-b0fc-da5fd01fa3e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f984986-61c0-4f42-9eac-1e7d1ad88c78",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "5df25d72-d6db-45b4-91a8-a76be3b6d5e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "84a1a9a5-24c8-49fa-b067-86eb3d50ab2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5df25d72-d6db-45b4-91a8-a76be3b6d5e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22370423-bca4-4c6f-b367-a30ad3e1fc66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5df25d72-d6db-45b4-91a8-a76be3b6d5e5",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "d37fc562-20aa-4ee3-bed3-47a05b964b3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "d63e7012-45cb-4472-8940-ba6136d1aa1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d37fc562-20aa-4ee3-bed3-47a05b964b3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2d2fbff-1a68-419d-954f-31cd9e99fa09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d37fc562-20aa-4ee3-bed3-47a05b964b3e",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "bf6eb3aa-546f-46d7-b8bc-a70d9be63bcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "9fb48d80-2a6b-4173-a03a-ecb28cd8534d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf6eb3aa-546f-46d7-b8bc-a70d9be63bcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e58f684-9365-4dbe-b19e-d086d9b5a620",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf6eb3aa-546f-46d7-b8bc-a70d9be63bcc",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "a92120b5-0508-4423-b7d4-4c869deba8af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "92234eae-b888-4df1-bab2-2d1f6436cf34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a92120b5-0508-4423-b7d4-4c869deba8af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a5f9595-b68e-4e0b-8a6b-e0b503783afe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a92120b5-0508-4423-b7d4-4c869deba8af",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "dc992a26-474a-4e8f-a277-d0d384572f73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "087e3655-8150-46d2-9217-261c85a2aae7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc992a26-474a-4e8f-a277-d0d384572f73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "269f2c45-f9d5-41b0-93ea-9e486f0b37fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc992a26-474a-4e8f-a277-d0d384572f73",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "d91b4256-8900-47b4-a071-fb210b163e8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "a2a447da-2d13-42c4-8882-5132c222047e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d91b4256-8900-47b4-a071-fb210b163e8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82fa66a7-f953-4ea9-9862-d719c4b81586",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d91b4256-8900-47b4-a071-fb210b163e8c",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "3815b02e-0d18-4073-bb8d-1787e71b1578",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "254928b6-7a38-4aa0-87ac-4bbbcbad7fd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3815b02e-0d18-4073-bb8d-1787e71b1578",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73bc192b-0c6c-4dd3-bc58-4e723d9fe8a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3815b02e-0d18-4073-bb8d-1787e71b1578",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "77ecd5a7-5484-48a9-bcb2-129ed9ac6450",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "324e7430-c705-450f-8c95-65172cbbebb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77ecd5a7-5484-48a9-bcb2-129ed9ac6450",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76a2fb9a-88b2-4667-b004-08ee55c5ce9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77ecd5a7-5484-48a9-bcb2-129ed9ac6450",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "1e49be48-2fc3-4933-af5f-d2136d294b9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "1627afa7-c2b9-4ef5-bc54-306b0c6c8222",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e49be48-2fc3-4933-af5f-d2136d294b9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e9502b3-c9f6-465d-90b3-d55e21637587",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e49be48-2fc3-4933-af5f-d2136d294b9a",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "3beda0d7-2090-4d1c-a202-af97511147cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "de7c8d29-c352-4eaf-85fb-6edeac624639",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3beda0d7-2090-4d1c-a202-af97511147cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "557cc4dc-ab50-4035-8225-c1454019651f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3beda0d7-2090-4d1c-a202-af97511147cf",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "d172a710-3e87-4b23-99cd-22b94e01e9c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "b9b6ce15-705e-405f-98a9-65570aecd0ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d172a710-3e87-4b23-99cd-22b94e01e9c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e3dda97-6dd0-4a34-8789-9165f69872f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d172a710-3e87-4b23-99cd-22b94e01e9c5",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "7f61f2c0-7c9b-4f2e-b70d-0a508b83d275",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "4cedf93c-871a-46ab-9778-1bd70f4a88ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f61f2c0-7c9b-4f2e-b70d-0a508b83d275",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "536c9b82-6e61-44f4-a9f5-18eb4313c35f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f61f2c0-7c9b-4f2e-b70d-0a508b83d275",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "0724d09b-1bbb-4ade-887e-ddb122516a9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "93b44f5d-14d1-4ee1-9fde-21bde5fa1cbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0724d09b-1bbb-4ade-887e-ddb122516a9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccddf0af-4627-482b-9d98-681631e3091b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0724d09b-1bbb-4ade-887e-ddb122516a9d",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "5d7541e7-aaf0-4312-829c-9d97dbcf72b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "2ce961df-2106-4e52-918e-fbd7c372758d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d7541e7-aaf0-4312-829c-9d97dbcf72b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9367c13a-d702-4b06-a599-38947c2f2359",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d7541e7-aaf0-4312-829c-9d97dbcf72b6",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "54989a17-885b-4d21-a2cc-60332ec0f0f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "c9991f2e-4d11-4400-8034-1f98b7ecf3e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54989a17-885b-4d21-a2cc-60332ec0f0f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d728c0b-8698-4613-a596-5d147e9a7e93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54989a17-885b-4d21-a2cc-60332ec0f0f5",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        },
        {
            "id": "8d86f837-e31e-4937-9e23-b03b5ff8535d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "compositeImage": {
                "id": "1173bcd7-0bd4-4cd1-91ef-5cd9d4d75721",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d86f837-e31e-4937-9e23-b03b5ff8535d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3355f085-d057-42a0-81ac-fb2802197bdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d86f837-e31e-4937-9e23-b03b5ff8535d",
                    "LayerId": "acce72d9-0c17-4869-bddf-1cc27bdfff4a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "acce72d9-0c17-4869-bddf-1cc27bdfff4a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "78342efb-d2ca-45e0-913e-4170fd7f0db6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 63,
    "yorig": 87
}