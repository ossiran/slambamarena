///attack_base(timer)

if(owner.otherplayer.hitpause > 0 && hitting == 1 && state_timer < argument0 - 9)
{
    /*
    if(attackframe0 > (state_timer + animationdelay - 1))
    {
        attackframe0 += 1;
    }
    
    if(attackframe1 > (state_timer + animationdelay - 1))
    {
        attackframe1 += 1;
    }
    
    if(attackframe2 > (state_timer + animationdelay - 1))
    {
        attackframe2 += 1;
    }
    
    if(attackframe3 > (state_timer + animationdelay - 1))
    {
        attackframe3 += 1;
    }
    
    if(attackframe4 > (state_timer + animationdelay - 1))
    {
        attackframe4 += 1;
    }
    
    if(attackframe5 > (state_timer + animationdelay - 1))
    {
        attackframe5 += 1;
    }
    */
    state_timer -= 1;
    //fix this
    
    //fix this. make it only work after the last attack frame
    if(owner.otherplayer.hitpause == 1)
    {
        hitting = 0;
    }
}

if(state_timer < 8)
{
    image_alpha = Approach(image_alpha, .8, 0.1);
}

if(state_timer == argument0 - 8)
{
    with(owner)
    {
        if(state_name == "geniemove")
        {
            if(onground())
            {
                state_switch("stand");
            }
            else
            {
                state_switch("air");
            }
        }
    }
    
    with(hurtbox)
	{
	    xoffset = -13;
	    yoffset = -1000;
	    image_xscale  = 0;
	    image_yscale  = 0;
	    xshift  = -2;
	}
}

if(state_timer > argument0 - 9)
{
    image_alpha = Approach(image_alpha,0,.1);
}

if(argument0 == state_timer)
{
    instance_destroy();
}

image_index = state_timer ;