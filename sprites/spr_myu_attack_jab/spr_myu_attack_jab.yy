{
    "id": "a96ad95f-b870-491f-97bf-ea2398d7938e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_myu_attack_jab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 12,
    "bbox_right": 47,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ebb67b2-494e-48b5-9689-848a28fb16eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a96ad95f-b870-491f-97bf-ea2398d7938e",
            "compositeImage": {
                "id": "31d7d538-8c2a-4218-ad02-2895ba19036f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ebb67b2-494e-48b5-9689-848a28fb16eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d5b2e93-58ef-4eac-8e86-816f8370290e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ebb67b2-494e-48b5-9689-848a28fb16eb",
                    "LayerId": "44742f43-e9b9-4e2e-a4f8-7de5ac3e2890"
                }
            ]
        },
        {
            "id": "3823303f-9a85-4e63-a32d-6db0bd8d7bf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a96ad95f-b870-491f-97bf-ea2398d7938e",
            "compositeImage": {
                "id": "e0185a82-913e-4036-b634-468f834721f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3823303f-9a85-4e63-a32d-6db0bd8d7bf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb16d7d6-7fc4-4177-b7b8-b2a0ab5825c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3823303f-9a85-4e63-a32d-6db0bd8d7bf1",
                    "LayerId": "44742f43-e9b9-4e2e-a4f8-7de5ac3e2890"
                }
            ]
        },
        {
            "id": "82c7dbf7-aa95-4df5-954a-6804b4427ef5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a96ad95f-b870-491f-97bf-ea2398d7938e",
            "compositeImage": {
                "id": "48f6f98f-765b-4063-9546-68ba764cbc49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82c7dbf7-aa95-4df5-954a-6804b4427ef5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e359e26d-4b37-4943-94c0-64268fb5c456",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82c7dbf7-aa95-4df5-954a-6804b4427ef5",
                    "LayerId": "44742f43-e9b9-4e2e-a4f8-7de5ac3e2890"
                }
            ]
        },
        {
            "id": "486834a6-ed47-40f3-94a0-0fecde720da2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a96ad95f-b870-491f-97bf-ea2398d7938e",
            "compositeImage": {
                "id": "be4cdb2f-b474-48f5-8d9d-c61d4d04b561",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "486834a6-ed47-40f3-94a0-0fecde720da2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4d621da-fd86-4574-9292-5747c97ec52d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "486834a6-ed47-40f3-94a0-0fecde720da2",
                    "LayerId": "44742f43-e9b9-4e2e-a4f8-7de5ac3e2890"
                }
            ]
        },
        {
            "id": "8c60a0d5-e9ae-4f40-93b7-79cf5ca9b442",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a96ad95f-b870-491f-97bf-ea2398d7938e",
            "compositeImage": {
                "id": "2b6e9459-a5a4-4d4b-a3ad-c84eccf031ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c60a0d5-e9ae-4f40-93b7-79cf5ca9b442",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57c56df0-7c76-44a0-8616-6a93ae42b8b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c60a0d5-e9ae-4f40-93b7-79cf5ca9b442",
                    "LayerId": "44742f43-e9b9-4e2e-a4f8-7de5ac3e2890"
                }
            ]
        },
        {
            "id": "d2fa17ac-1494-4074-a223-da518afb1d11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a96ad95f-b870-491f-97bf-ea2398d7938e",
            "compositeImage": {
                "id": "9c04b744-2203-4bcd-920c-fccb9cdae9c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2fa17ac-1494-4074-a223-da518afb1d11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "188a9aea-aabd-4f77-919b-76ddcb8539fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2fa17ac-1494-4074-a223-da518afb1d11",
                    "LayerId": "44742f43-e9b9-4e2e-a4f8-7de5ac3e2890"
                }
            ]
        },
        {
            "id": "2af798ec-17b8-478c-bc47-955181936a6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a96ad95f-b870-491f-97bf-ea2398d7938e",
            "compositeImage": {
                "id": "25e6c742-d351-466e-a981-1090bc85a714",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2af798ec-17b8-478c-bc47-955181936a6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "043aabde-b97e-48f2-96c7-eb46d7046418",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2af798ec-17b8-478c-bc47-955181936a6d",
                    "LayerId": "44742f43-e9b9-4e2e-a4f8-7de5ac3e2890"
                }
            ]
        },
        {
            "id": "175d681f-fdb2-43c6-ba7a-e221ea4d3ef2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a96ad95f-b870-491f-97bf-ea2398d7938e",
            "compositeImage": {
                "id": "9213e27e-dc40-42cf-be0a-7cd4e7e69b8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "175d681f-fdb2-43c6-ba7a-e221ea4d3ef2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bc315ac-99d6-4083-9923-e1a8e349b915",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "175d681f-fdb2-43c6-ba7a-e221ea4d3ef2",
                    "LayerId": "44742f43-e9b9-4e2e-a4f8-7de5ac3e2890"
                }
            ]
        },
        {
            "id": "dbb4da08-7fa1-4013-9329-b1d895ba98ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a96ad95f-b870-491f-97bf-ea2398d7938e",
            "compositeImage": {
                "id": "3fdebe5d-bef0-470d-b12a-475abc3cd543",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbb4da08-7fa1-4013-9329-b1d895ba98ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7eee35e0-45f3-4509-bb8f-7146c557b58a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbb4da08-7fa1-4013-9329-b1d895ba98ba",
                    "LayerId": "44742f43-e9b9-4e2e-a4f8-7de5ac3e2890"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "44742f43-e9b9-4e2e-a4f8-7de5ac3e2890",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a96ad95f-b870-491f-97bf-ea2398d7938e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 32
}