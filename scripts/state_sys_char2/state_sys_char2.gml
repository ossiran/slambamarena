if(state_timer == 0)
{
    //resetting menu
    menu_maxx = 0;
    menu_maxy = 2;
    menu_indexx = 0;
    menu_indexy = 0;
    
    menu_array[0 * array_height_2d(menu_array) + 0,0] = obj_myu;
    menu_array[0 * array_height_2d(menu_array) + 0,1] = "Bill";
    menu_array[0 * array_height_2d(menu_array) + 0,2] = spr_bill_idle;
    menu_array[0 * array_height_2d(menu_array) + 0,3] = spr_bill_portrait;

    menu_array[0 * array_height_2d(menu_array) + 1,0] = obj_syn;
    menu_array[0 * array_height_2d(menu_array) + 1,1] = "Syndikate";
    menu_array[0 * array_height_2d(menu_array) + 1,2] = spr_lsyn_idle;
    menu_array[0 * array_height_2d(menu_array) + 1,3] = spr_bill_portrait;
    
    menu_array[0 * array_height_2d(menu_array) + 2,0] = obj_momo;
    menu_array[0 * array_height_2d(menu_array) + 2,1] = "Momo";
    menu_array[0 * array_height_2d(menu_array) + 2,2] = spr_momo_idle;
    menu_array[0 * array_height_2d(menu_array) + 2,3] = spr_bill_portrait;
    
    with(obj_controller)
    {
        if(obj_system.player1_controller != controller_num && obj_system.player2_controller != controller_num)
        {
            instance_destroy();
        }
        
        positionx_max = 0;
        positionx_min = 0;
        positiony_max = 2;
        positiony_min = 0;
        
        positionx = 0;
        positiony = 0;
        
        ready_up = 0;
        
        if(obj_system.player1_controller == controller_num)
        {
            sprite_index = spr_controller1_selector;
        }
        else
        {
            sprite_index = spr_controller2_selector;
        }
    }
    
    //background
    var back_id = layer_background_get_id("Background");
    layer_background_sprite(back_id,spr_charselect);
}

with(obj_controller)
{
    if(ready_up == 0)
    {
            if(key_essence == 1)
            {
                with(other)
                {
                    state_switch("char_select");
                }
            }
    }
}

if(ready_up == 2)
{
    //assigning correct value to player1_char and player2_char
    with(obj_controller)
    {
        if(obj_system.player1_controller == controller_num)
        {
            obj_system.player1_char = obj_system.menu_array[positionx * array_height_2d(obj_system.menu_array) + positiony, 0];
        }
        else
        {
            obj_system.player2_char = obj_system.menu_array[positionx * array_height_2d(obj_system.menu_array) + positiony, 0];
        }
    }
    
    menu_array[0 * array_height_2d(menu_array) + 0,0] = 0;
    menu_array[0 * array_height_2d(menu_array) + 0,1] = 0;
    menu_array[0 * array_height_2d(menu_array) + 0,2] = 0;
    menu_array[0 * array_height_2d(menu_array) + 0,3] = 0;

    menu_array[0 * array_height_2d(menu_array) + 1,0] = 0;
    menu_array[0 * array_height_2d(menu_array) + 1,1] = 0;
    menu_array[0 * array_height_2d(menu_array) + 1,2] = 0;
    menu_array[0 * array_height_2d(menu_array) + 1,3] = 0;
    
    ready_up = 0;
    
    if(player2_char == 0)
    {
        player2_char = obj_myu;
    }
    
    state_switch("char_select3");
}