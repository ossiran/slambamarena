{
    "id": "abc2b166-6eca-423f-8504-1b1b6e02d2ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_5m_ice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "08ed7abf-dadd-446e-ad4d-1612bbbe4c79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abc2b166-6eca-423f-8504-1b1b6e02d2ae",
            "compositeImage": {
                "id": "480d3858-9e2c-49e9-a393-b8fa74a231b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08ed7abf-dadd-446e-ad4d-1612bbbe4c79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f86dfab-376a-4d0e-a754-e1f9f5b30048",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08ed7abf-dadd-446e-ad4d-1612bbbe4c79",
                    "LayerId": "e272bc4a-ee74-4c90-ba7d-431ada7ad97b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e272bc4a-ee74-4c90-ba7d-431ada7ad97b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "abc2b166-6eca-423f-8504-1b1b6e02d2ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}