{
    "id": "749ae08c-6a90-4344-ab6e-33e21fa3bb40",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lsyn_5m",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 43,
    "bbox_right": 114,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f7410635-ab8f-4549-9ec1-2e9ebefcf5cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749ae08c-6a90-4344-ab6e-33e21fa3bb40",
            "compositeImage": {
                "id": "b5492513-e3c2-47d3-a11a-570f64c8eef2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7410635-ab8f-4549-9ec1-2e9ebefcf5cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1864267-7f31-4a6f-84c6-b1a42f2f3f0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7410635-ab8f-4549-9ec1-2e9ebefcf5cd",
                    "LayerId": "50faa3ca-9660-4677-993f-092e13cdccbb"
                }
            ]
        },
        {
            "id": "ddb80b21-8954-46db-995b-2ad07d4f4c1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749ae08c-6a90-4344-ab6e-33e21fa3bb40",
            "compositeImage": {
                "id": "3d7103c1-8915-438f-b60f-673b76b0bcb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddb80b21-8954-46db-995b-2ad07d4f4c1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5ab0d5b-d301-426f-ae9d-1e7e8ab892d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddb80b21-8954-46db-995b-2ad07d4f4c1b",
                    "LayerId": "50faa3ca-9660-4677-993f-092e13cdccbb"
                }
            ]
        },
        {
            "id": "52a491fa-5509-4a78-98b9-63e872098ea9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749ae08c-6a90-4344-ab6e-33e21fa3bb40",
            "compositeImage": {
                "id": "3fc204b5-090b-45f5-9d87-30bed0413be5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52a491fa-5509-4a78-98b9-63e872098ea9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85f17469-226a-4ed0-914f-36798be97e04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52a491fa-5509-4a78-98b9-63e872098ea9",
                    "LayerId": "50faa3ca-9660-4677-993f-092e13cdccbb"
                }
            ]
        },
        {
            "id": "b892a7ba-aeb0-4ada-9404-b8d3c4b6aae0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749ae08c-6a90-4344-ab6e-33e21fa3bb40",
            "compositeImage": {
                "id": "6db74aae-e0b0-461c-871f-31adb0b65cf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b892a7ba-aeb0-4ada-9404-b8d3c4b6aae0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7df69d6d-a2e6-464c-99fc-b0bbb6551792",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b892a7ba-aeb0-4ada-9404-b8d3c4b6aae0",
                    "LayerId": "50faa3ca-9660-4677-993f-092e13cdccbb"
                }
            ]
        },
        {
            "id": "9cefa5c1-6a3b-426a-801d-cbd928a8814e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749ae08c-6a90-4344-ab6e-33e21fa3bb40",
            "compositeImage": {
                "id": "c0b5d5b8-0cd7-4ab3-b8d4-d18ae89f0b68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cefa5c1-6a3b-426a-801d-cbd928a8814e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "192dea35-7ea8-4a28-bccb-3ff635116d88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cefa5c1-6a3b-426a-801d-cbd928a8814e",
                    "LayerId": "50faa3ca-9660-4677-993f-092e13cdccbb"
                }
            ]
        },
        {
            "id": "acede13a-b940-479f-b6d2-2910a86d6589",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749ae08c-6a90-4344-ab6e-33e21fa3bb40",
            "compositeImage": {
                "id": "3e83dee3-51fb-4706-951f-c9204aa85cfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acede13a-b940-479f-b6d2-2910a86d6589",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c810bf3-62e4-463e-b424-fc85e4742e95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acede13a-b940-479f-b6d2-2910a86d6589",
                    "LayerId": "50faa3ca-9660-4677-993f-092e13cdccbb"
                }
            ]
        },
        {
            "id": "17f32ab6-7965-4f99-b676-afd215c7103a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749ae08c-6a90-4344-ab6e-33e21fa3bb40",
            "compositeImage": {
                "id": "b3b59cfc-2bb7-4f30-84eb-e096d546378b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17f32ab6-7965-4f99-b676-afd215c7103a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fbc1c4a-6bba-4b0c-9a57-20fca71eac5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17f32ab6-7965-4f99-b676-afd215c7103a",
                    "LayerId": "50faa3ca-9660-4677-993f-092e13cdccbb"
                }
            ]
        },
        {
            "id": "3e892f31-22a4-439e-a7f6-f135d7c5a398",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749ae08c-6a90-4344-ab6e-33e21fa3bb40",
            "compositeImage": {
                "id": "3c3be5f3-c333-43b7-8541-5f2054b1ccd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e892f31-22a4-439e-a7f6-f135d7c5a398",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f36bee53-912d-492f-996c-041d6452dba4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e892f31-22a4-439e-a7f6-f135d7c5a398",
                    "LayerId": "50faa3ca-9660-4677-993f-092e13cdccbb"
                }
            ]
        },
        {
            "id": "129215a1-4377-4cbc-9bc3-12ce3a04ad7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749ae08c-6a90-4344-ab6e-33e21fa3bb40",
            "compositeImage": {
                "id": "3f7dbece-3391-4740-9311-7467701579c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "129215a1-4377-4cbc-9bc3-12ce3a04ad7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00a21fd9-18a1-4db0-b010-b5510176a347",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "129215a1-4377-4cbc-9bc3-12ce3a04ad7b",
                    "LayerId": "50faa3ca-9660-4677-993f-092e13cdccbb"
                }
            ]
        },
        {
            "id": "f890871a-ad20-4849-a2e7-a92044771b82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749ae08c-6a90-4344-ab6e-33e21fa3bb40",
            "compositeImage": {
                "id": "6ae2bf02-1065-4e1a-a5f7-d3343930032b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f890871a-ad20-4849-a2e7-a92044771b82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "feb5cd19-e8ea-4a0a-8bfd-6288e23acd18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f890871a-ad20-4849-a2e7-a92044771b82",
                    "LayerId": "50faa3ca-9660-4677-993f-092e13cdccbb"
                }
            ]
        },
        {
            "id": "9e932525-4b50-47bd-80a8-b3f234fe552a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749ae08c-6a90-4344-ab6e-33e21fa3bb40",
            "compositeImage": {
                "id": "49f2643b-378f-4615-804b-bbe6b81e8d9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e932525-4b50-47bd-80a8-b3f234fe552a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c8c9231-60de-4971-bf71-2790d5a41fec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e932525-4b50-47bd-80a8-b3f234fe552a",
                    "LayerId": "50faa3ca-9660-4677-993f-092e13cdccbb"
                }
            ]
        },
        {
            "id": "ad65c725-c443-44ea-9045-4943f9ef8874",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749ae08c-6a90-4344-ab6e-33e21fa3bb40",
            "compositeImage": {
                "id": "9ef5ecec-d80f-455f-a2c2-f1019b4d76f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad65c725-c443-44ea-9045-4943f9ef8874",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8580bdc-b407-4b68-a5c1-326556576c2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad65c725-c443-44ea-9045-4943f9ef8874",
                    "LayerId": "50faa3ca-9660-4677-993f-092e13cdccbb"
                }
            ]
        },
        {
            "id": "0491a2de-cfb7-4e70-b4bd-ebf845d20d43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749ae08c-6a90-4344-ab6e-33e21fa3bb40",
            "compositeImage": {
                "id": "87d30c8a-8f13-4b34-a19e-5f0b2bf05dec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0491a2de-cfb7-4e70-b4bd-ebf845d20d43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f626b6a3-ba33-46d5-82ba-4b0f6a35d8fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0491a2de-cfb7-4e70-b4bd-ebf845d20d43",
                    "LayerId": "50faa3ca-9660-4677-993f-092e13cdccbb"
                }
            ]
        },
        {
            "id": "59ce2618-4606-4975-b231-4f8c30253921",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749ae08c-6a90-4344-ab6e-33e21fa3bb40",
            "compositeImage": {
                "id": "60c2816a-424f-40dc-acb8-cd644270a609",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59ce2618-4606-4975-b231-4f8c30253921",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b652205-f36c-4a34-8ddb-984cb44cfc76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59ce2618-4606-4975-b231-4f8c30253921",
                    "LayerId": "50faa3ca-9660-4677-993f-092e13cdccbb"
                }
            ]
        },
        {
            "id": "02cd8b19-6b1c-4b09-8f9e-21673d01965b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749ae08c-6a90-4344-ab6e-33e21fa3bb40",
            "compositeImage": {
                "id": "e8a9b9e5-35f9-4f37-a98a-dddedef9cbbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02cd8b19-6b1c-4b09-8f9e-21673d01965b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad1e131e-0040-4138-9239-407e1be5987e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02cd8b19-6b1c-4b09-8f9e-21673d01965b",
                    "LayerId": "50faa3ca-9660-4677-993f-092e13cdccbb"
                }
            ]
        },
        {
            "id": "5a8124bb-3c93-4bee-b9e6-b1e3d783c9de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749ae08c-6a90-4344-ab6e-33e21fa3bb40",
            "compositeImage": {
                "id": "7d3d919e-240e-45e8-b80b-98d867d5e3e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a8124bb-3c93-4bee-b9e6-b1e3d783c9de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "094f0421-0677-4ee4-815c-15e480041a5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a8124bb-3c93-4bee-b9e6-b1e3d783c9de",
                    "LayerId": "50faa3ca-9660-4677-993f-092e13cdccbb"
                }
            ]
        },
        {
            "id": "83a3b8d7-3b97-4742-bdaf-c297928cde71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749ae08c-6a90-4344-ab6e-33e21fa3bb40",
            "compositeImage": {
                "id": "be5236ea-9cf8-4d32-9c38-f15f1c35813f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83a3b8d7-3b97-4742-bdaf-c297928cde71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c96fac1c-81f7-4e4d-a85e-733566428765",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83a3b8d7-3b97-4742-bdaf-c297928cde71",
                    "LayerId": "50faa3ca-9660-4677-993f-092e13cdccbb"
                }
            ]
        },
        {
            "id": "73a6c48b-6df4-4c51-b73a-577f444fc7e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749ae08c-6a90-4344-ab6e-33e21fa3bb40",
            "compositeImage": {
                "id": "0deeba9d-0698-4c42-ae45-bfb282649f89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73a6c48b-6df4-4c51-b73a-577f444fc7e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "813102b7-7de9-4510-a644-8ab0958cc041",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73a6c48b-6df4-4c51-b73a-577f444fc7e1",
                    "LayerId": "50faa3ca-9660-4677-993f-092e13cdccbb"
                }
            ]
        },
        {
            "id": "8a4699f0-21f5-44df-813b-746581678afb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749ae08c-6a90-4344-ab6e-33e21fa3bb40",
            "compositeImage": {
                "id": "5294eaec-0e91-4709-ba29-91a031bfd204",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a4699f0-21f5-44df-813b-746581678afb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7568ccc7-01a8-4007-91ec-a687e6fc9916",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a4699f0-21f5-44df-813b-746581678afb",
                    "LayerId": "50faa3ca-9660-4677-993f-092e13cdccbb"
                }
            ]
        },
        {
            "id": "4c8fbd56-6a04-4e61-b23d-6189730a73af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749ae08c-6a90-4344-ab6e-33e21fa3bb40",
            "compositeImage": {
                "id": "a11a2767-8c95-44ee-8f48-393153c001f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c8fbd56-6a04-4e61-b23d-6189730a73af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd0d4930-5aa1-47f8-9113-30f4c12b2361",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c8fbd56-6a04-4e61-b23d-6189730a73af",
                    "LayerId": "50faa3ca-9660-4677-993f-092e13cdccbb"
                }
            ]
        },
        {
            "id": "26ca0082-6afb-4f76-99e9-83e7325f6f39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749ae08c-6a90-4344-ab6e-33e21fa3bb40",
            "compositeImage": {
                "id": "ca14856c-d24a-4ad8-a989-2693f00e1a74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26ca0082-6afb-4f76-99e9-83e7325f6f39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b27e217c-c293-4192-9df3-52814530f33d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26ca0082-6afb-4f76-99e9-83e7325f6f39",
                    "LayerId": "50faa3ca-9660-4677-993f-092e13cdccbb"
                }
            ]
        },
        {
            "id": "afbd2864-a14a-4049-a2d4-a35a1bfb7a3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749ae08c-6a90-4344-ab6e-33e21fa3bb40",
            "compositeImage": {
                "id": "aaf689d3-2d4a-45db-b525-03bdfda948d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afbd2864-a14a-4049-a2d4-a35a1bfb7a3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3ae2843-f4e4-4f03-979f-198fb87878e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afbd2864-a14a-4049-a2d4-a35a1bfb7a3f",
                    "LayerId": "50faa3ca-9660-4677-993f-092e13cdccbb"
                }
            ]
        },
        {
            "id": "23184676-eaa1-48d8-9232-ac034c280817",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749ae08c-6a90-4344-ab6e-33e21fa3bb40",
            "compositeImage": {
                "id": "839a28ad-f9a9-4593-825f-7608433f586b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23184676-eaa1-48d8-9232-ac034c280817",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb7b9835-c830-4b66-ac75-7422cb375b1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23184676-eaa1-48d8-9232-ac034c280817",
                    "LayerId": "50faa3ca-9660-4677-993f-092e13cdccbb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "50faa3ca-9660-4677-993f-092e13cdccbb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "749ae08c-6a90-4344-ab6e-33e21fa3bb40",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}