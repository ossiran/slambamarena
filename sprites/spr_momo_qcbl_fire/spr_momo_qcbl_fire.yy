{
    "id": "adf7c11c-c3b0-47c9-8b89-4ab2c7c798c0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_qcbl_fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c08414de-ec21-4981-9512-a734fe19dd57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adf7c11c-c3b0-47c9-8b89-4ab2c7c798c0",
            "compositeImage": {
                "id": "79fa4b3d-ad0d-49ad-944f-2d1bebdefec0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c08414de-ec21-4981-9512-a734fe19dd57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f83a75b0-d810-42b6-bc55-128826564cc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c08414de-ec21-4981-9512-a734fe19dd57",
                    "LayerId": "9bb33d2a-608c-4ce3-bbb7-0876db5baa4f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9bb33d2a-608c-4ce3-bbb7-0876db5baa4f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "adf7c11c-c3b0-47c9-8b89-4ab2c7c798c0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}