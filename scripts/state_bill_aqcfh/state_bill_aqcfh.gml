if(state_new == 1)
{
	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -42;
	    xscale  = 26;
	    yscale  = 74;
	    xshift  = -2;
	}
	
			with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 16;
	    image_yscale  = 47;
	    xshift = -2
	}
	attackframe0 =  16;
	attackframe1 = 25
	endstate = 40;
}



if(state_timer == attackframe0)
{
    hitbox1 = create_hitbox(13, -35,20, 20, 3.65,-5, 19,18,8,12,24,0,330,3.5,"mid","billnspecial");
    hitbox2 = create_hitbox(5, -28,39, 28, 3.65,-5, 18,16,10,12,24,0,1,6,"mid","billnspecial");
	hitbox1.image_alpha = 1;
	hitbox1.hitsound = snd_hit;
	hitbox1.vx = 5 * image_xscale;
	hitbox1.vy = 2.75;
	audio_play_sound(snd_laser,1,0);
	ex += 2;
	
}

if(state_timer == attackframe0)
{
    hitbox3 = create_hitbox(13, -35,20, 20, 3.65,-5, 19,18,8,12,24,0,330,3.5,"mid","billnspecial");
    hitbox4 = create_hitbox(5, -28,39, 28, 3.65,-5, 18,16,10,12,24,0,1,6,"mid","billnspecial");
	hitbox3.image_alpha = 1;
	hitbox3.hitsound = snd_hit;
	hitbox3.vx = 6 * image_xscale;
	hitbox3.vy = 3;
	audio_play_sound(snd_laser,1,0);
	ex += 2;
	
}


sprite_index = spr_bill_fspecial;

attack_base(endstate,1,1,0,1,0);

if(state_new == 1)
{
    vxwas = vx;
	vywas = vy - 1;
    vx = 0;
    vy = 0;
}

if(state_timer < attackframe1)
{
    vy = 0;
}

if(state_timer == attackframe1)
{
    vx = vxwas;
    vy = vywas;
}