///create_collisionbox(xoffset,yoffset,xscale,yscale,xshift)
var collisionbox0 = instance_create_depth(x,y,120,obj_collisionbox);

collisionbox0.owner           =    other;
collisionbox0.image_xscale    =    argument2;
collisionbox0.image_yscale    =    argument3;
collisionbox0.xoffset         =    argument0;
collisionbox0.yoffset         =    argument1;
collisionbox0.xshift          =    argument4;

return collisionbox0;