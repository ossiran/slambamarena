if(state_new == 1)
{
	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -42;
	    xscale  = 26;
	    yscale  = 74;
	    xshift  = -2;
	}
	
			with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 16;
	    image_yscale  = 47;
	    xshift = -2
	}
	attackframe0 =  5;
	attackframe1 =  18;
	endstate = 36;
}


if(state_timer > attackframe0 && state_timer < attackframe1 && hitpause == 0)
{
    vx = 5.5 * image_xscale;
}
else
{
    vx = Approach(vx,0,5);
}

if(state_timer == 9)
{
    vy = -5;
}

if(state_timer == 15)
{
    vy = 9;
}

var hitfalladd = 0;

//juggling correctly
with(otherplayer)
{
    if(!onground() || vy < 0 || hitair = 1)
    {
        hitfalladd = 1;
    }
}

if(state_timer == 19)
{
    hitbox1 = create_hitbox(-4, -20,50, 50, 0, -3, 14,18,5,10,0 + hitfalladd,0,0,3,"overhead",-1);
	hitbox1.hitsound = snd_hit;
	ex += 4;
	hitbox1.scaling = 1.5;
	
}


if(state_timer == 22)
{
    hitbox1 = create_hitbox(-4, -20,50, 50, 2, 5, 14,12,12,9,0 + hitfalladd,1,3,3,"overhead",-1);
	hitbox1.hitsound = snd_hit;
	hitbox1.scaling = 2.5;
}

sprite_index = spr_bill_qcbm;

attack_base(endstate,0,0,0,1,0);
/*
if(state_new == 1)
{
	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -42;
	    xscale  = 26;
	    yscale  = 74;
	    xshift  = -2;
	}
	
			with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 16;
	    image_yscale  = 47;
	    xshift = -2
	}
	attackframe0 =  5;
	attackframe1 =  50;
	attackframe2 =  13;
	attackframe3 =  27;
	attackframe4 =  45;
	endstate = 69;
}


if(state_timer > attackframe0 && state_timer < attackframe1)
{
    vx = 4.75 * image_xscale;
}
else
{
    vx = Approach(vx,0,0.1);
}

if(state_timer == attackframe2)
{
    hitbox1 = create_hitbox(-4,-15,45,35,6,-4,11,20,7,15,0,0,2,2,"mid",-1);
	hitbox1.hitsound = snd_hit;
	hitbox1.juggle = 1;
	ex += 3;
}

if(state_timer == attackframe3)
{
    hitbox2 = create_hitbox(-4,-15,45,35,6,-4,11,20,7,15,0,0,2,2,"mid",-1);
    hitbox2.scaling = 0.5;
	hitbox2.hitsound = snd_hit;
	hitbox2.juggle = 1;
}

if(state_timer == attackframe4)
{
    hitbox4 = create_hitbox(0,-27,52,49,4,4,15,4,7,10,-1,0,2,2,"overhead",-1);
    hitbox4.scaling = 0.5;
	hitbox4.hitsound = snd_hit;
	hitbox4.juggle = 1;
}

sprite_index = spr_bill_bspecial;

attack_base(endstate,0,0,0,1,0);
*/