event_inherited();

state_machine_init();

state_create("stand",state_bill_stand);
state_create("walk",state_bill_walk);
state_create("fdash",state_bill_fdash);
state_create("bdash",state_bill_bdash);
state_create("crouch",state_bill_crouch);
state_create("jumpsquat",state_bill_jumpsquat);
state_create("air",state_bill_air);
state_create("airdash",state_bill_airdash);
state_create("hitstun",state_bill_hitstun);
state_create("hitfall",state_bill_hitfall);
state_create("bounce",state_bill_bounce);
state_create("knockdown",state_bill_knockdown);
state_create("stagger",state_bill_stagger);
state_create("recovery",state_bill_recovery);
state_create("sblock",state_bill_sblock);
state_create("cblock",state_bill_cblock);
state_create("ablock",state_bill_ablock);
state_create("light",state_bill_light);
state_create("clight",state_bill_clight);
state_create("medium",state_bill_medium);
state_create("fmedium",state_bill_fmedium);
state_create("cmedium",state_bill_cmedium);
state_create("heavy",state_bill_heavy);
state_create("cheavy",state_bill_cheavy);
state_create("fheavy",state_bill_fheavy);
state_create("qcfl",state_bill_qcfl);
state_create("qcfm",state_bill_qcfm);
state_create("qcfh",state_bill_qcfh);
state_create("qcbl",state_bill_qcbl);
state_create("qcbm",state_bill_qcbm);
state_create("qcbh",state_bill_qcbh);
state_create("srkl",state_bill_srkl);
state_create("srkm",state_bill_srkm);
state_create("srkh",state_bill_srkh);
state_create("dqcf",state_bill_dqcf);
state_create("grab",state_bill_grab);
state_create("lair",state_bill_lair);
state_create("mair",state_bill_mair);
state_create("hair",state_bill_hair);
state_create("aqcbl",state_bill_aqcbl);
state_create("aqcbm",state_bill_aqcbm);
state_create("aqcbh",state_bill_aqcbh);
state_create("agrab",state_bill_agrab);
state_create("bthrow",state_bill_bthrow);
state_create("fthrow",state_bill_fthrow);
state_create("athrow",state_bill_athrow);
state_create("grabbed",state_bill_grabbed);
state_create("grabtech",state_bill_grabtech);
state_create("geniemove",state_bill_geniemove);
state_create("landing",state_bill_landing);

state_init("stand");

ground_accel = 1;
air_accel = 0.12;
ground_fric = 0.9;
air_fric = 0.5;
grav = 0.75;
jumpheight = 11;
vxMax = 3;
vyMax = 8;
doublejump = 1;
dashdir = 0;

myname = "bill";

hurtbox0 = create_hurtbox(-13,-43,26,75,-1,-2);

collisionbox = create_collisionbox(-5,-12,16,41,-2);

hurt_spriteG = spr_bill_hitstun_g;
hurt_spriteA = spr_bill_hitstun_a;
fall_sprite = spr_bill_hitfall;
knock_sprite = spr_bill_knockdown;
idle_sprite = spr_bill_idle;

hpmax = 200;
hp = hpmax;

qcf_buffer = 0;
dqcf_buffer = 0;
qcb_buffer = 0;
srk_buffer = 0;
ff_buffer = 0;
bb_buffer = 0;

mask_index = spr_bill_idle;
//genie = instance_create_depth(x,y,-100,obj_justice);
//genie.owner = other;