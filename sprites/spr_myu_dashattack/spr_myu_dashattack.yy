{
    "id": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_myu_dashattack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 2,
    "bbox_right": 57,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "20b84765-8eb2-4d11-9c0e-bd22266370b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "072508b0-bff1-44fd-9cba-64308039e9c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20b84765-8eb2-4d11-9c0e-bd22266370b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cbde2b7-cf48-4e28-9128-e8d2c492eb79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20b84765-8eb2-4d11-9c0e-bd22266370b5",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "3aeef92f-97ef-49db-a531-78efb92915a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "7d336f1b-be53-4310-b948-3cbe68d7e6c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3aeef92f-97ef-49db-a531-78efb92915a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2827cc7f-df67-4e86-b86a-9a5165670a5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3aeef92f-97ef-49db-a531-78efb92915a4",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "7a5f449e-95f8-40ad-ac94-29cb486ef447",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "37079d69-8ad9-4269-adf5-20734a620810",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a5f449e-95f8-40ad-ac94-29cb486ef447",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8670cbb8-f225-4480-b72d-db32665f19d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a5f449e-95f8-40ad-ac94-29cb486ef447",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "093dc751-912e-406b-9fa9-b29399e44e89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "eb64b4a5-088c-4805-b0b4-ac85d55341aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "093dc751-912e-406b-9fa9-b29399e44e89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16162299-c306-4ee4-b9b1-42b984c037a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "093dc751-912e-406b-9fa9-b29399e44e89",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "93aa1619-476a-47d5-ae9f-ff2d7c444551",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "e4b923fd-5781-4a52-92ce-490999cefd5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93aa1619-476a-47d5-ae9f-ff2d7c444551",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07486175-bc94-4b2f-aef6-2911e0e97193",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93aa1619-476a-47d5-ae9f-ff2d7c444551",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "fc0aa7d8-266b-49f6-9c7b-ee91d9f1049d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "62b967e7-c121-4c32-9bba-bbc7cfeaf5f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc0aa7d8-266b-49f6-9c7b-ee91d9f1049d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4f186f3-d1fc-418d-9236-8883abf60147",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc0aa7d8-266b-49f6-9c7b-ee91d9f1049d",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "115db31b-8f3f-47b9-951e-9e10fad4961b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "02f72deb-993c-4b06-ae36-5be4bc9e9eeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "115db31b-8f3f-47b9-951e-9e10fad4961b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2d2ccbf-7e01-40c2-96ff-92c027a889ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "115db31b-8f3f-47b9-951e-9e10fad4961b",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "f8ebefba-0c14-4ea1-9618-ad1f88118a34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "8bbb6991-e70f-4820-8f3e-c45bbd833d7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8ebefba-0c14-4ea1-9618-ad1f88118a34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b18034b7-34f2-4ef1-879e-ceabcba2188b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8ebefba-0c14-4ea1-9618-ad1f88118a34",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "3f5ee342-0561-4780-9a60-c558e01f90ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "5003a4c8-830c-4c3b-872e-39f5d125e6f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f5ee342-0561-4780-9a60-c558e01f90ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e66a625-913f-49ca-9554-dc39e6d10f12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f5ee342-0561-4780-9a60-c558e01f90ff",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "03306a16-168c-4213-a7cd-717e886ce368",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "b0f76c92-ec90-4992-8f1e-4e31870f4523",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03306a16-168c-4213-a7cd-717e886ce368",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb09a90a-8b43-4505-a042-15bca2f2a8de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03306a16-168c-4213-a7cd-717e886ce368",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "44508700-ce94-49ff-bfc0-671ae24796aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "78284834-e000-4acf-8a0e-ead297a120f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44508700-ce94-49ff-bfc0-671ae24796aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1654a9cd-f75c-4706-b348-54e49b8e0564",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44508700-ce94-49ff-bfc0-671ae24796aa",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "369002fc-fdcc-4ca0-80a7-06497dde3068",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "c77ac1c7-a0f6-4499-bf92-3e8963ddde46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "369002fc-fdcc-4ca0-80a7-06497dde3068",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "defde97c-69c0-4f9a-b78e-7ae5ec4aae9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "369002fc-fdcc-4ca0-80a7-06497dde3068",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "d46b2d0b-d29a-4c45-84c6-94403319c73f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "2765798a-24fa-4ece-a8d7-e7d082dc6744",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d46b2d0b-d29a-4c45-84c6-94403319c73f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01ab1231-dcb8-4ca2-b39c-a8ba8c4e95ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d46b2d0b-d29a-4c45-84c6-94403319c73f",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "573269f9-0d15-4f2c-a028-14396ee43c36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "73568d96-6bae-48b9-b79d-a104bfbea1f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "573269f9-0d15-4f2c-a028-14396ee43c36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c67c305-fe24-449d-9ba2-2a042950658d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "573269f9-0d15-4f2c-a028-14396ee43c36",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "4194596d-b93c-4fda-9727-b2f8a1de8971",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "e39fb013-c123-4360-821f-9515baa69eb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4194596d-b93c-4fda-9727-b2f8a1de8971",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf26fd53-b6db-41db-ac79-1b9844b16635",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4194596d-b93c-4fda-9727-b2f8a1de8971",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "1941399a-c1ad-46cd-9880-ab562539009c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "d738721c-7c56-4a11-b639-9328d929c729",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1941399a-c1ad-46cd-9880-ab562539009c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bdb249d-d6d4-4d45-8bb9-bea9fb94184b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1941399a-c1ad-46cd-9880-ab562539009c",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "6e31accc-e615-47dd-8617-12c91ac10e24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "02c3674c-267f-4d33-ae3d-2c7b58fe2236",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e31accc-e615-47dd-8617-12c91ac10e24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98d4babf-fdea-44aa-912e-72b7c06bb626",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e31accc-e615-47dd-8617-12c91ac10e24",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "41201bc5-6822-4cca-a7b4-1cd974f6bf44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "afc965f2-a11f-4e84-9761-89d8a0a41a14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41201bc5-6822-4cca-a7b4-1cd974f6bf44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "968dd9d0-4079-4094-9497-03aff84cc389",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41201bc5-6822-4cca-a7b4-1cd974f6bf44",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "af9d21d2-c647-4f7b-864e-c377c3210fc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "b072d3d9-c99d-4aec-9d90-03eed5a36575",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af9d21d2-c647-4f7b-864e-c377c3210fc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d65f0a3-d0cd-4839-97e4-b5f57280c40c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af9d21d2-c647-4f7b-864e-c377c3210fc5",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "ad53a286-8e8e-4971-8794-2ee1f924ba4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "fad6e07f-85d3-4509-87ea-66461984f6a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad53a286-8e8e-4971-8794-2ee1f924ba4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bda6a2de-0a7c-4d59-ae0a-25383956d301",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad53a286-8e8e-4971-8794-2ee1f924ba4f",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "834143c6-e8a9-46ff-a901-8de7634c2867",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "591f1a7f-513e-44ee-8c87-9adc150d05c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "834143c6-e8a9-46ff-a901-8de7634c2867",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d54e6c81-b139-43df-843b-86c962ee89a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "834143c6-e8a9-46ff-a901-8de7634c2867",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "72bfd88b-e48f-48d5-9469-c1b9a5247354",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "8338a31d-fa57-48b7-b9b6-60fd475f4834",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72bfd88b-e48f-48d5-9469-c1b9a5247354",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a27f5b60-51d0-42c3-aa6b-8db493408fcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72bfd88b-e48f-48d5-9469-c1b9a5247354",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "ae9e6828-2915-4729-884e-b867438ee634",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "1d0fda36-fe64-4ca2-a76c-d983c5d4bcf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae9e6828-2915-4729-884e-b867438ee634",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37f5972e-bdba-4512-a617-6185d2af2ddd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae9e6828-2915-4729-884e-b867438ee634",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "42c44a4a-38d1-4f8f-a0a5-968dff559805",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "76b8670f-8792-4429-8e75-0caa52e22589",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42c44a4a-38d1-4f8f-a0a5-968dff559805",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df5b1509-c6d8-41ef-9558-571adb04081e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42c44a4a-38d1-4f8f-a0a5-968dff559805",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "dacdac1a-f743-450b-abb3-2a5a1d9f660d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "ace4b827-912c-4ac6-9e92-cb8134e8fd1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dacdac1a-f743-450b-abb3-2a5a1d9f660d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02f8ffa7-e7cb-4e9b-8bfd-64e13acea84e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dacdac1a-f743-450b-abb3-2a5a1d9f660d",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "27bb8e9b-81be-435b-a34c-83dc4c1a3436",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "c07175c9-b44b-449e-9568-5ff0e99fd8a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27bb8e9b-81be-435b-a34c-83dc4c1a3436",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "054a0a14-dd42-41b9-98ca-8e8802552c76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27bb8e9b-81be-435b-a34c-83dc4c1a3436",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "681223f1-3d82-487c-829a-238d1a351e9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "8884fe67-214b-4876-9562-d07e7b852e84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "681223f1-3d82-487c-829a-238d1a351e9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c08efe7c-0764-4cc2-bbbd-716879b9e568",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "681223f1-3d82-487c-829a-238d1a351e9a",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "1e9fcef8-b5c0-47be-b4e3-b1f520e34065",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "7ead0693-f1d7-4f3f-a9b3-aadd884a9365",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e9fcef8-b5c0-47be-b4e3-b1f520e34065",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebc13bb0-f99b-485b-b2a6-94a1137a25e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e9fcef8-b5c0-47be-b4e3-b1f520e34065",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "551c2871-d438-41a9-beb1-0953a580b1e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "b4b80c3e-6365-42fd-9ff6-dc0afed9e73b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "551c2871-d438-41a9-beb1-0953a580b1e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b0793da-7ae4-4a57-bd01-62237492562a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "551c2871-d438-41a9-beb1-0953a580b1e7",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "47285c31-f603-4e6b-9292-dc9bc454e026",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "d1b7feca-8105-4a77-8e2d-0c56c206b6d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47285c31-f603-4e6b-9292-dc9bc454e026",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33de482a-4736-43a2-aebd-120ba16bc7b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47285c31-f603-4e6b-9292-dc9bc454e026",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "d2f18aa7-543f-4cf3-9fc0-481518ccf5e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "7acdbad4-fda3-4703-9b23-1171c2437a02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2f18aa7-543f-4cf3-9fc0-481518ccf5e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70668af5-57b2-4231-9108-e2c4fb905822",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2f18aa7-543f-4cf3-9fc0-481518ccf5e8",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "7c911bb0-21a4-4c4e-843d-8d9d9ed4361d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "f30d2efe-444b-4f86-b3bb-5aea251c89ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c911bb0-21a4-4c4e-843d-8d9d9ed4361d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58494b7f-835f-455e-9adf-714fa94fbeea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c911bb0-21a4-4c4e-843d-8d9d9ed4361d",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "de41341c-f22a-447c-aab6-3af71e0cf453",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "d1b14d33-024c-4eb9-ac29-11be6564a2b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de41341c-f22a-447c-aab6-3af71e0cf453",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "719ebfae-4053-40b3-9285-dcfb0858f35c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de41341c-f22a-447c-aab6-3af71e0cf453",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "603daf58-bf7a-4664-882f-9199d76cae5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "2ad74560-dd80-4db2-b4b6-a2ec1adce215",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "603daf58-bf7a-4664-882f-9199d76cae5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6081ba7-763d-4d5c-aeea-a5b0557ca2d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "603daf58-bf7a-4664-882f-9199d76cae5b",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "95c6a811-9275-42f2-b9bc-ebb8093686fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "be7b60f5-8647-40c9-828e-fb49e55ca867",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95c6a811-9275-42f2-b9bc-ebb8093686fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91d37133-eb02-4761-a54d-749f271cd2bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95c6a811-9275-42f2-b9bc-ebb8093686fa",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "1ef1d0f7-6a89-4690-a58b-cc3b5b2cbfe7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "19b9cf91-7210-4ea3-ad40-70b682aaf8af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ef1d0f7-6a89-4690-a58b-cc3b5b2cbfe7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "458ce71c-2ab9-4264-aaed-a3b707d48f55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ef1d0f7-6a89-4690-a58b-cc3b5b2cbfe7",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "486284da-0639-4dfe-b54d-a246530baa45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "8af423be-4344-4861-9560-685a9f590546",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "486284da-0639-4dfe-b54d-a246530baa45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f20828b5-b9f5-4bee-b6fa-6594fbff3119",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "486284da-0639-4dfe-b54d-a246530baa45",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "268f6e18-f150-4e00-abff-dd065ea0424b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "841b635f-1b45-4bc6-8be7-c8de25fc745d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "268f6e18-f150-4e00-abff-dd065ea0424b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6dc0a83-c746-4168-b2b5-0eb282dae82e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "268f6e18-f150-4e00-abff-dd065ea0424b",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "71113b69-6329-4159-b0e1-05a1a9a1e3c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "a90cc1f2-df4f-4581-9043-1f21d6b34e5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71113b69-6329-4159-b0e1-05a1a9a1e3c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "749694f1-72a2-4795-afb4-0b4b56fa9a26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71113b69-6329-4159-b0e1-05a1a9a1e3c8",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        },
        {
            "id": "437e3bf3-ba93-4fc8-9ae7-3d66f7905f9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "compositeImage": {
                "id": "2fad80c0-fb04-4132-8f07-4266f731de0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "437e3bf3-ba93-4fc8-9ae7-3d66f7905f9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87819652-0240-49f6-b9d9-5ec76a923f2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "437e3bf3-ba93-4fc8-9ae7-3d66f7905f9b",
                    "LayerId": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a8bb5200-7d02-4204-ba1d-03d5e7a21c8f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a55b61f3-ffd5-4a22-89d0-182591bfa5b8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 24,
    "yorig": 32
}