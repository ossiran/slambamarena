if(state_new == 1)
{
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 16;
	    image_yscale  = 47;
	    xshift = -2
	}

	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -28;
	    xscale  = 26;
	    yscale  = 60;
	    xshift  = -2;
	}
    sprite_index = spr_bill_crouch;
}

image_index = -1;


//friction
vx = Approach(vx,0,ground_fric);
vy = 0;

if(state_timer == 2)
{
    if(input_check(1,0,0,0,0) || input_check(2,0,0,0,0) || input_check(3,0,0,0,0))
    {
        state_switch("crouch");
    }
    else
    {
        state_switch("stand");
    }
}

//hitstun
if(hitstun > 0)
{
    state_switch("hitstun");   
}