{
    "id": "d28c4dbe-1280-4185-89ae-ea2ec4d0977c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lightball",
    "eventList": [
        {
            "id": "8b6f9d56-b29f-4ef7-a2fd-f558fc934fa9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d28c4dbe-1280-4185-89ae-ea2ec4d0977c"
        },
        {
            "id": "5266894e-b0a4-4213-9ef0-adc050c5fdfb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d28c4dbe-1280-4185-89ae-ea2ec4d0977c"
        },
        {
            "id": "1b339ac8-3744-48aa-be39-5b817a0ba195",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "d28c4dbe-1280-4185-89ae-ea2ec4d0977c"
        },
        {
            "id": "3f9ae63c-ea57-44f2-80fb-f78bdd11d5fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d28c4dbe-1280-4185-89ae-ea2ec4d0977c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2132d2fb-2953-4d2d-a20d-f37cdc53310f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "14578850-9511-403f-8bcd-e7f7e417934d",
    "visible": true
}