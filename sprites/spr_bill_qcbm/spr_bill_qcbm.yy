{
    "id": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_qcbm",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 36,
    "bbox_right": 122,
    "bbox_top": 69,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "501ab23d-66af-4032-9516-2828c820ddc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "26946d9e-37c3-400a-a295-fe562b325729",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "501ab23d-66af-4032-9516-2828c820ddc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0072dbc0-a8ef-4cd1-9a04-94ac6c5f626d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "501ab23d-66af-4032-9516-2828c820ddc9",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "c262fe05-cf81-49d1-b9a5-ac0e784e5760",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "bb549938-f61e-467e-a25d-c56b36e3961d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c262fe05-cf81-49d1-b9a5-ac0e784e5760",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a21cfd67-939f-4f96-a7f7-5d2713824723",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c262fe05-cf81-49d1-b9a5-ac0e784e5760",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "a9ecdc88-ad63-4cc7-8e2b-d127056cab0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "23779c90-d70d-4938-9208-7890e0c7139a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9ecdc88-ad63-4cc7-8e2b-d127056cab0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d1b6f7c-b021-4680-b9af-fc9c5d6e1868",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9ecdc88-ad63-4cc7-8e2b-d127056cab0a",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "b49022df-a167-45ee-b3aa-99c5bfbfc37b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "bcced927-e338-407a-ad3d-6ba36ae007d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b49022df-a167-45ee-b3aa-99c5bfbfc37b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "def0c7fa-6402-4096-a070-079dcc930733",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b49022df-a167-45ee-b3aa-99c5bfbfc37b",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "72a506e4-64e5-420f-b519-0453d48f76a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "f966a907-7b83-442d-909a-22979bf87c95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72a506e4-64e5-420f-b519-0453d48f76a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df2f178f-46ec-4b19-a535-f2b856e27ed6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72a506e4-64e5-420f-b519-0453d48f76a7",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "2fda9f20-d9ed-455b-be65-8e2d50ff439e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "c05a0a1d-5b8d-4cf4-8083-164ff22bae95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fda9f20-d9ed-455b-be65-8e2d50ff439e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68273755-6962-4d25-8c6b-00cc0ee9e771",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fda9f20-d9ed-455b-be65-8e2d50ff439e",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "fdf89e9d-8350-45eb-8f51-657f2a32985f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "095c839c-cc06-4488-8142-83ccc6a87766",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdf89e9d-8350-45eb-8f51-657f2a32985f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbbc4fed-0b04-4dc3-884e-42a7314fd2d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdf89e9d-8350-45eb-8f51-657f2a32985f",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "50b31fa4-ca69-4c0c-b597-5e74e41bb371",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "9033f3a3-4f6a-49cf-9f2f-f90637b76e3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50b31fa4-ca69-4c0c-b597-5e74e41bb371",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c2ab193-8be7-43ac-b23c-3bc8bedab17e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50b31fa4-ca69-4c0c-b597-5e74e41bb371",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "161a5cc3-c4f1-489e-9675-ea2b7e35bbca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "99a4ed7e-6cfc-4244-bdc0-52afb0a01922",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "161a5cc3-c4f1-489e-9675-ea2b7e35bbca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85bd493d-e814-401e-b7c9-3e071bb28f18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "161a5cc3-c4f1-489e-9675-ea2b7e35bbca",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "b5f6d83f-f6be-4215-bba9-8a28f7b9d846",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "39dccd91-ad1e-435d-b3af-1f33b5ba5054",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5f6d83f-f6be-4215-bba9-8a28f7b9d846",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a86e810-b534-4f81-b210-491ebc9e6d6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5f6d83f-f6be-4215-bba9-8a28f7b9d846",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "80525e37-895d-4f70-ba3e-7311d79e5204",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "f8e43b23-cdf5-40c0-9ab0-314f21c4ee0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80525e37-895d-4f70-ba3e-7311d79e5204",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e0ce3d9-c959-47dd-b651-ad9bda2b1b3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80525e37-895d-4f70-ba3e-7311d79e5204",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "1d244c5e-ba41-4333-8bdc-121a2b3b3c52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "27ef0ecc-5db6-4608-b688-eeaef9697261",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d244c5e-ba41-4333-8bdc-121a2b3b3c52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0a16910-75ee-431a-b731-53be3d95cae1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d244c5e-ba41-4333-8bdc-121a2b3b3c52",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "a0959cf7-a9f6-48a0-b7f0-7ce8b704cc5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "514d1308-131b-4786-ae34-1a04646ea44b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0959cf7-a9f6-48a0-b7f0-7ce8b704cc5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f6e66de-c6cf-4cc2-8bbc-4efdab6ec992",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0959cf7-a9f6-48a0-b7f0-7ce8b704cc5b",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "0bcd2823-ff15-4a08-a575-53508a340c26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "969485d7-18fc-4407-828a-8f33f73b699c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bcd2823-ff15-4a08-a575-53508a340c26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cf644ec-1dc3-4e23-a3cd-68feb91d5fe2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bcd2823-ff15-4a08-a575-53508a340c26",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "3920e6b7-0ff2-4c5e-b419-f70d01753192",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "bf33c2af-7723-4cc6-8cb5-6350d7ca478c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3920e6b7-0ff2-4c5e-b419-f70d01753192",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b6c42ce-8e29-432c-8eed-2c529a75d41a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3920e6b7-0ff2-4c5e-b419-f70d01753192",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "82223372-02cb-43b0-ac9c-311f646111be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "5d249274-c154-4344-9917-468715ee60dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82223372-02cb-43b0-ac9c-311f646111be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "030a95ce-cdc1-456f-a0b4-0600d789a788",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82223372-02cb-43b0-ac9c-311f646111be",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "22323dd8-f588-4765-87f9-3ea650085d43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "07928a26-399a-49b6-aa5a-24a336951d41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22323dd8-f588-4765-87f9-3ea650085d43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f022fe04-25be-4511-86a5-4dfb0024bec0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22323dd8-f588-4765-87f9-3ea650085d43",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "8cd3a739-75bd-4002-a682-c9c82811a0b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "fc8b8947-8062-4fb7-841d-56e38366649d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cd3a739-75bd-4002-a682-c9c82811a0b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "316ec001-b770-44f4-ad60-32b0436fec42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cd3a739-75bd-4002-a682-c9c82811a0b8",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "2b3d850d-17aa-4fe1-9267-2141ee2120d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "bff0ee77-8dd7-4415-85ca-5e854636b1e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b3d850d-17aa-4fe1-9267-2141ee2120d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54b7937c-7eaf-4b8b-8f9a-5c03179eeb31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b3d850d-17aa-4fe1-9267-2141ee2120d7",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "d0bfd06c-dc67-4e69-b670-63a9bfe4f308",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "8ad3e63e-636b-4441-9676-e289fecff598",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0bfd06c-dc67-4e69-b670-63a9bfe4f308",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44028ff9-3ba9-4e9b-aed2-fa34dd3b3902",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0bfd06c-dc67-4e69-b670-63a9bfe4f308",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "0578912b-ae88-4da5-bb67-0b2d6767b1b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "f0ac7baa-608a-47fe-a98d-ac21cfbab9e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0578912b-ae88-4da5-bb67-0b2d6767b1b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d0c49d8-4760-4b02-88d1-5770f3961af0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0578912b-ae88-4da5-bb67-0b2d6767b1b5",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "9868f1d2-4aa8-462d-a233-d0bff37475c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "fb942090-254a-4e71-87f9-0085b1938dd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9868f1d2-4aa8-462d-a233-d0bff37475c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5579398a-bc9e-4ded-9bab-6315e4f137a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9868f1d2-4aa8-462d-a233-d0bff37475c4",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "c0f769f2-6da9-4217-a399-efc6dee6faf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "f77d8057-59b8-4f1e-bd4f-e3b2feaf3469",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0f769f2-6da9-4217-a399-efc6dee6faf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c28582e-1c79-4ae4-a98d-ddaf952899d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0f769f2-6da9-4217-a399-efc6dee6faf6",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "bd9268c0-dc06-4fb8-b918-5078de8473ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "31ee78c5-0027-4146-bd52-4f57d667a369",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd9268c0-dc06-4fb8-b918-5078de8473ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e340bccf-1a3c-4829-8d3a-35c73ae67942",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd9268c0-dc06-4fb8-b918-5078de8473ef",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "48ae218d-4648-4bac-9a01-ecaa12952e58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "86112af3-af6b-4468-9e0b-2f34717ab6b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48ae218d-4648-4bac-9a01-ecaa12952e58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0d8bf7e-6f1e-4c74-ab8d-d18cf65ea844",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48ae218d-4648-4bac-9a01-ecaa12952e58",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "70c52bc9-6a3e-43f4-9b08-bc3c73f044ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "c0fe6827-f817-4f5a-a721-b5a890abc0b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70c52bc9-6a3e-43f4-9b08-bc3c73f044ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9a78683-f0eb-4e59-9c67-18d33ba98a78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70c52bc9-6a3e-43f4-9b08-bc3c73f044ef",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "30816281-ae0e-4676-a668-c82c56fa6472",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "c17443e1-016f-41dd-a7f2-d3f030b0c6db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30816281-ae0e-4676-a668-c82c56fa6472",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf123439-1a80-44c8-b4fc-2ee6237b718f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30816281-ae0e-4676-a668-c82c56fa6472",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "f459076f-11cc-4a1c-bb7d-ff78374982a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "887fdb25-0f67-4c24-962f-79cc7f5a936d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f459076f-11cc-4a1c-bb7d-ff78374982a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "391e876e-edce-4aa2-b788-7c8ff9f52601",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f459076f-11cc-4a1c-bb7d-ff78374982a2",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "fd7541b4-a95a-4f1a-99f7-34e5b5a20bb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "d51dcee1-6ce7-4ca5-829b-c4cd32568e54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd7541b4-a95a-4f1a-99f7-34e5b5a20bb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16e7d96b-438d-40ea-87d2-227fda51a886",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd7541b4-a95a-4f1a-99f7-34e5b5a20bb0",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "96e988b3-758d-4938-a4c7-064770d0df7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "8455cf9c-e786-4b4e-a1c1-30161fd5d929",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96e988b3-758d-4938-a4c7-064770d0df7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "157b4815-6b56-46fd-b95f-a7b6cca88ee6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96e988b3-758d-4938-a4c7-064770d0df7e",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "413329bf-9f3a-46b8-b836-68b9cf53b2e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "9b7f0db7-4ec4-40cb-b31e-63bb13af1e58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "413329bf-9f3a-46b8-b836-68b9cf53b2e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5bf6070-5222-4e0d-ac82-f67f9afe1275",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "413329bf-9f3a-46b8-b836-68b9cf53b2e2",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "fdff44d9-3e77-4b75-8da5-93b2e25c8a84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "22cde5d4-607a-46f2-a22e-0d9ed9cd7e8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdff44d9-3e77-4b75-8da5-93b2e25c8a84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae88f179-0b82-4354-866d-136e008b92d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdff44d9-3e77-4b75-8da5-93b2e25c8a84",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "7cc504d5-d675-4176-990a-424994549e7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "3e587922-b920-4eb0-b87b-aebe882caca7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cc504d5-d675-4176-990a-424994549e7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c33e4f9-063b-435c-8413-b7fccd7b2281",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cc504d5-d675-4176-990a-424994549e7d",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "a5ce2f4a-ee7d-41a3-9056-93f6bd1c6e4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "04452cd9-b252-4801-85ce-56d9108dd2e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5ce2f4a-ee7d-41a3-9056-93f6bd1c6e4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3ae2151-f473-4afa-b98f-f1475b4dc526",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5ce2f4a-ee7d-41a3-9056-93f6bd1c6e4c",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "f63035a0-2f79-48c4-a5f6-79ddbe67f489",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "9ff40e63-9cb5-45c4-a892-485c16f7e2c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f63035a0-2f79-48c4-a5f6-79ddbe67f489",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6934fc77-5545-4f9f-b04a-903d40899b70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f63035a0-2f79-48c4-a5f6-79ddbe67f489",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "5500521c-2f8c-49d6-b6c1-0fa92e593d55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "b6627fdf-c959-469f-84ba-4ef273b51c4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5500521c-2f8c-49d6-b6c1-0fa92e593d55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4876f22-6059-45b1-a29d-17b6daeb1199",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5500521c-2f8c-49d6-b6c1-0fa92e593d55",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "d387bced-d838-4a51-b656-26942836f62e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "292d60f9-595f-491a-90ea-f5e2e7ee9121",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d387bced-d838-4a51-b656-26942836f62e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4b4c5cb-4ba9-470d-a4d3-0aacc7d1daed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d387bced-d838-4a51-b656-26942836f62e",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        },
        {
            "id": "ef8b7308-35a4-4efc-855b-c669c1301563",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "compositeImage": {
                "id": "4a7447a7-167a-49c0-ac6d-0761ae48f577",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef8b7308-35a4-4efc-855b-c669c1301563",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64b6019b-ed6f-4b72-8670-8191b2867f2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef8b7308-35a4-4efc-855b-c669c1301563",
                    "LayerId": "18af4561-5a49-4195-8ec9-c5a137d6a739"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "18af4561-5a49-4195-8ec9-c5a137d6a739",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "607f5c99-d015-4264-a650-2a1f7b90ecb8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}