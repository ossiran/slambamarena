{
    "id": "b11fd9e0-c40b-4288-9034-ef49604487e7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_myu_landing",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 12,
    "bbox_right": 35,
    "bbox_top": 11,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "635b115d-44a0-41bb-97f7-d02db98c1fd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b11fd9e0-c40b-4288-9034-ef49604487e7",
            "compositeImage": {
                "id": "953de355-ab46-4e5d-a7e6-e365f3c1c8b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "635b115d-44a0-41bb-97f7-d02db98c1fd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df7cbb0a-51fe-4033-92a3-88ca63dfb1ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "635b115d-44a0-41bb-97f7-d02db98c1fd7",
                    "LayerId": "d507d77a-5474-4654-8ad6-c65c5a327b9c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d507d77a-5474-4654-8ad6-c65c5a327b9c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b11fd9e0-c40b-4288-9034-ef49604487e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 32
}