{
    "id": "db6fb90d-5b12-4df7-8581-68bf49acc77f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dark",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 8,
    "bbox_right": 39,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c5eefa8-4da9-4a9f-a0ca-30ae5626f6f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6fb90d-5b12-4df7-8581-68bf49acc77f",
            "compositeImage": {
                "id": "33f84721-ec46-49d0-a798-30d65adc9f92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c5eefa8-4da9-4a9f-a0ca-30ae5626f6f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a0cde4b-2aab-44f2-b6a0-f73e4f7c8060",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c5eefa8-4da9-4a9f-a0ca-30ae5626f6f2",
                    "LayerId": "0d7aee40-ef54-468a-a0f8-ee647955e3a5"
                }
            ]
        },
        {
            "id": "09c5cb9c-7e77-4d80-822a-6e580ef36293",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6fb90d-5b12-4df7-8581-68bf49acc77f",
            "compositeImage": {
                "id": "1ced252d-b2fa-4076-96bc-ac5235498869",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09c5cb9c-7e77-4d80-822a-6e580ef36293",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2af297ab-d001-4c41-9bfe-4364d18c2989",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09c5cb9c-7e77-4d80-822a-6e580ef36293",
                    "LayerId": "0d7aee40-ef54-468a-a0f8-ee647955e3a5"
                }
            ]
        },
        {
            "id": "a5289686-221f-4c2d-a719-332a91096716",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6fb90d-5b12-4df7-8581-68bf49acc77f",
            "compositeImage": {
                "id": "b251bfb6-017e-449e-aa26-a68ef2ca5712",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5289686-221f-4c2d-a719-332a91096716",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe488743-f8c2-479e-b1ba-9b38029fafa0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5289686-221f-4c2d-a719-332a91096716",
                    "LayerId": "0d7aee40-ef54-468a-a0f8-ee647955e3a5"
                }
            ]
        },
        {
            "id": "6e4c65fd-4cf9-4b40-b494-d090f39ca1d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6fb90d-5b12-4df7-8581-68bf49acc77f",
            "compositeImage": {
                "id": "b0d9c823-a5d4-473e-a055-f9808198d36a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e4c65fd-4cf9-4b40-b494-d090f39ca1d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80929fbe-ae9c-4e5e-b919-c3373c085684",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e4c65fd-4cf9-4b40-b494-d090f39ca1d1",
                    "LayerId": "0d7aee40-ef54-468a-a0f8-ee647955e3a5"
                }
            ]
        },
        {
            "id": "89de581e-089c-41e2-9c6e-d248c6cf3699",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6fb90d-5b12-4df7-8581-68bf49acc77f",
            "compositeImage": {
                "id": "5244a6e9-7274-4824-9422-74a42dbe7414",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89de581e-089c-41e2-9c6e-d248c6cf3699",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9f212c5-4fb7-4958-95b3-bdc2e24a2054",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89de581e-089c-41e2-9c6e-d248c6cf3699",
                    "LayerId": "0d7aee40-ef54-468a-a0f8-ee647955e3a5"
                }
            ]
        },
        {
            "id": "8eb8351e-3a46-4bb9-9b3d-03cd79ea621b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6fb90d-5b12-4df7-8581-68bf49acc77f",
            "compositeImage": {
                "id": "afa721b0-059a-4c4d-9ff0-ace928075b23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8eb8351e-3a46-4bb9-9b3d-03cd79ea621b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "334f725d-bda3-4f47-a080-569592d69809",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8eb8351e-3a46-4bb9-9b3d-03cd79ea621b",
                    "LayerId": "0d7aee40-ef54-468a-a0f8-ee647955e3a5"
                }
            ]
        },
        {
            "id": "66970270-f952-41c1-9cc3-721f00f66ec5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6fb90d-5b12-4df7-8581-68bf49acc77f",
            "compositeImage": {
                "id": "161a6bdd-a094-4e92-ada8-fe987b24889b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66970270-f952-41c1-9cc3-721f00f66ec5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63f20671-2a55-497f-a61a-a190ed71cbeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66970270-f952-41c1-9cc3-721f00f66ec5",
                    "LayerId": "0d7aee40-ef54-468a-a0f8-ee647955e3a5"
                }
            ]
        },
        {
            "id": "ca3643b7-42bc-4dc3-9352-070bae10e116",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6fb90d-5b12-4df7-8581-68bf49acc77f",
            "compositeImage": {
                "id": "23a0e267-bd13-4238-a184-546754b3ecdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca3643b7-42bc-4dc3-9352-070bae10e116",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f88a38d1-8ede-4ef7-bee2-09e2b1943361",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca3643b7-42bc-4dc3-9352-070bae10e116",
                    "LayerId": "0d7aee40-ef54-468a-a0f8-ee647955e3a5"
                }
            ]
        },
        {
            "id": "2c9173df-409e-433f-bb7c-5b12681511fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6fb90d-5b12-4df7-8581-68bf49acc77f",
            "compositeImage": {
                "id": "a2ab6697-f074-4323-a31c-ec069c541f9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c9173df-409e-433f-bb7c-5b12681511fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8df3c795-0e32-4a69-bdef-642ff5eb83a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c9173df-409e-433f-bb7c-5b12681511fa",
                    "LayerId": "0d7aee40-ef54-468a-a0f8-ee647955e3a5"
                }
            ]
        },
        {
            "id": "7f7f2c08-0095-4d4d-831c-3b5f17ff0a60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6fb90d-5b12-4df7-8581-68bf49acc77f",
            "compositeImage": {
                "id": "e5ee1e7d-aac2-446a-b1c8-ebdc07e083fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f7f2c08-0095-4d4d-831c-3b5f17ff0a60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0ba1349-2b7b-4508-96fa-393c193910f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f7f2c08-0095-4d4d-831c-3b5f17ff0a60",
                    "LayerId": "0d7aee40-ef54-468a-a0f8-ee647955e3a5"
                }
            ]
        },
        {
            "id": "63ad80f0-8e4b-4b83-be04-5e7579ac3ff4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6fb90d-5b12-4df7-8581-68bf49acc77f",
            "compositeImage": {
                "id": "ac1860a0-d9a3-4f26-a046-97f72748ca18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63ad80f0-8e4b-4b83-be04-5e7579ac3ff4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c770f71d-b643-461e-a123-dddbe6be2156",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63ad80f0-8e4b-4b83-be04-5e7579ac3ff4",
                    "LayerId": "0d7aee40-ef54-468a-a0f8-ee647955e3a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "0d7aee40-ef54-468a-a0f8-ee647955e3a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db6fb90d-5b12-4df7-8581-68bf49acc77f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 24,
    "yorig": 24
}