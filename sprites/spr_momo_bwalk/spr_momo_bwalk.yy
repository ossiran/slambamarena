{
    "id": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_bwalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 30,
    "bbox_right": 111,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "253b8439-c313-4b35-833a-0220d10cac81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "8e1b34df-4430-46a8-9723-3172639587a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "253b8439-c313-4b35-833a-0220d10cac81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82f66c4c-2bad-405c-9974-22f21a7ff4f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "253b8439-c313-4b35-833a-0220d10cac81",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "a6632808-f53f-4294-92ec-2984efb4ca7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "5bf2a668-946f-457d-92ef-1085eb0018a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6632808-f53f-4294-92ec-2984efb4ca7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf54406c-1ebb-423e-99c2-f13e05f2c3a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6632808-f53f-4294-92ec-2984efb4ca7f",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "3f1e4fbd-1171-4881-a02c-c30a477290b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "711257d2-f910-4eb8-9f67-a716ec0df0e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f1e4fbd-1171-4881-a02c-c30a477290b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "567e7da3-5ca2-46a4-9a72-5c1a1b25107a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f1e4fbd-1171-4881-a02c-c30a477290b6",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "b6b6cc13-9d45-4eb8-9e1a-130429d80645",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "33cebb13-4cfc-42b7-98d6-c11a6e25b8f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6b6cc13-9d45-4eb8-9e1a-130429d80645",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd60c9c1-9541-4222-82c4-daecb8a1b3c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6b6cc13-9d45-4eb8-9e1a-130429d80645",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "7f95d002-4640-40cc-bcfe-77022c745a97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "b0e423ea-584e-4c2c-a2b5-881deb2972fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f95d002-4640-40cc-bcfe-77022c745a97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7f7c261-35c4-4cb4-a6f1-de1c91b95dc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f95d002-4640-40cc-bcfe-77022c745a97",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "17bba52f-dc3b-4753-a582-a810c9dbc740",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "beb113f9-1249-458f-a569-7fb438e7444c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17bba52f-dc3b-4753-a582-a810c9dbc740",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74ea7020-158f-401c-9d6f-e2effe2a850b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17bba52f-dc3b-4753-a582-a810c9dbc740",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "e8d66d4c-8ea9-4e29-839a-23a814565892",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "19f610e3-4c5f-45ac-a6ed-1830f9792e71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8d66d4c-8ea9-4e29-839a-23a814565892",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b0e7988-98bb-4e4b-b662-a6da9162fd6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8d66d4c-8ea9-4e29-839a-23a814565892",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "46da588e-2c34-461b-b2ac-16acad1260ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "a7e5de04-8a94-4e85-912a-f891b5b0562c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46da588e-2c34-461b-b2ac-16acad1260ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2df50f7f-af68-4b4e-bc94-27b732302e71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46da588e-2c34-461b-b2ac-16acad1260ab",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "04f8a502-5ee7-4045-9933-b811159b3662",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "9481df0e-e799-4e05-b31e-521faabe34b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04f8a502-5ee7-4045-9933-b811159b3662",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "442fcdaa-3fc5-474d-9974-1b322df0f554",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04f8a502-5ee7-4045-9933-b811159b3662",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "4457c6ac-0b56-46ac-ad77-7e85c5db9f77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "b7547bea-419a-470e-9af3-1e8949fe1258",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4457c6ac-0b56-46ac-ad77-7e85c5db9f77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "040dfe88-289c-4551-8e29-52b76be4a886",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4457c6ac-0b56-46ac-ad77-7e85c5db9f77",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "1f91d50d-328a-4378-b701-a5482dd820c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "ef9fbbd0-6ea7-4b9c-940c-e83d160d1b1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f91d50d-328a-4378-b701-a5482dd820c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54811da8-9dd8-45a6-bcc7-0c80ac3b40b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f91d50d-328a-4378-b701-a5482dd820c6",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "2fffd92e-a03a-4ccd-9490-191115c573ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "03b78f01-269e-424a-8b59-942a456b5d66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fffd92e-a03a-4ccd-9490-191115c573ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0c1587c-0f8b-45c7-809c-63e9a00a2047",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fffd92e-a03a-4ccd-9490-191115c573ce",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "085eadf9-4bba-42e5-80d5-502e16c673bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "37b8438d-0c10-4b34-a55a-1840098cd92f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "085eadf9-4bba-42e5-80d5-502e16c673bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2d16475-ca29-402f-ad56-1f9aae936e17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "085eadf9-4bba-42e5-80d5-502e16c673bb",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "f8272184-dea5-4109-ab07-d2cd9102a08f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "73b0ee6f-446d-4251-be4c-4cc0177ae2a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8272184-dea5-4109-ab07-d2cd9102a08f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74c5e044-b259-4d39-b689-91928da84981",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8272184-dea5-4109-ab07-d2cd9102a08f",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "1a9a2299-bd9c-4e95-98e2-5f97fe6fe362",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "18ea09be-c8f2-4393-ae3b-6e8700af1ef9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a9a2299-bd9c-4e95-98e2-5f97fe6fe362",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca06c82a-5481-4f27-8f1c-6c2b8f36be5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a9a2299-bd9c-4e95-98e2-5f97fe6fe362",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "37eef454-262e-43e7-a0c0-2299afe79ee2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "53d86d5b-ebfc-4763-8a3a-204e886bd608",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37eef454-262e-43e7-a0c0-2299afe79ee2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8deaa9ee-b7e3-48ae-b8f2-219f1f65663a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37eef454-262e-43e7-a0c0-2299afe79ee2",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "6061b981-7eb5-4624-a0e4-a3f30bd2c84c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "ccdbfc76-c50a-4f6d-a9ef-e232c7140ba5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6061b981-7eb5-4624-a0e4-a3f30bd2c84c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaf6bcfe-0890-463d-90f0-dc59b5adb863",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6061b981-7eb5-4624-a0e4-a3f30bd2c84c",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "2e8549cc-711b-413b-ac06-020b363e3f08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "6804d451-ffe6-49e0-8ec3-69fefc0d6004",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e8549cc-711b-413b-ac06-020b363e3f08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84387e27-6198-46a5-8553-07d73dab169f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e8549cc-711b-413b-ac06-020b363e3f08",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "0fde1e8c-cd81-4800-8ed3-ce8984393666",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "3ff2cf55-ccef-4b0a-9ed0-e25abed1b46b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fde1e8c-cd81-4800-8ed3-ce8984393666",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "479fd71a-b94e-42ae-bfd9-ca7f3b25a36f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fde1e8c-cd81-4800-8ed3-ce8984393666",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "b01fa4a0-977b-417a-81f5-eb9f6407323b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "aa8d9cb9-13aa-4b10-b137-385dc6585e05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b01fa4a0-977b-417a-81f5-eb9f6407323b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b311d3c9-bd6a-4758-9311-e7bf446dc71d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b01fa4a0-977b-417a-81f5-eb9f6407323b",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "167f025d-93e7-4771-b224-333b81fde926",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "c28c64fb-5909-4b39-9d51-ece234198357",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "167f025d-93e7-4771-b224-333b81fde926",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49d2eb55-2a16-4e5d-a80e-8f270cdb172f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "167f025d-93e7-4771-b224-333b81fde926",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "21b15ca0-7033-44b0-b2ce-80cb09797ef8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "b29e77c9-a438-4a30-9858-242c7ff7bbc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21b15ca0-7033-44b0-b2ce-80cb09797ef8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da886799-463f-412c-ad94-bcf8a2c8908e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21b15ca0-7033-44b0-b2ce-80cb09797ef8",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "b10a5bb2-6a6a-40c4-9086-d551ded596d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "f71cda6d-918c-44b0-9c73-fa985bdd26db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b10a5bb2-6a6a-40c4-9086-d551ded596d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e34bcda9-64a4-4267-9d78-20b541ee2297",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b10a5bb2-6a6a-40c4-9086-d551ded596d7",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "f26bfbaa-3c5f-44a2-b8ad-57eae92c81e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "3f219375-d38f-48b4-af95-1bff72b209c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f26bfbaa-3c5f-44a2-b8ad-57eae92c81e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77333473-c97e-4a06-86fd-9467b1cf6c2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f26bfbaa-3c5f-44a2-b8ad-57eae92c81e9",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "81b66a0f-803d-4483-a2a9-359c29dbad56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "85bfbd86-aeda-4e7a-91b4-618633c8e931",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81b66a0f-803d-4483-a2a9-359c29dbad56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14b13630-9f2a-4c5b-9aac-26e1be91a830",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81b66a0f-803d-4483-a2a9-359c29dbad56",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "38f1987d-d035-4644-90bb-be3af31aa25c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "bd6c0a29-d468-40fb-af74-62c4cbb69d48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38f1987d-d035-4644-90bb-be3af31aa25c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "764bc1e9-c5c3-4d68-aa3c-97b9a5c01566",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38f1987d-d035-4644-90bb-be3af31aa25c",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "647e5bbe-af40-4c12-8621-e9dde0123dad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "e8eaec93-de5f-4a14-9353-299b242faaac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "647e5bbe-af40-4c12-8621-e9dde0123dad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a1f069c-d684-4b24-b9db-87269c818646",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "647e5bbe-af40-4c12-8621-e9dde0123dad",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "163db437-1529-4d8d-ba27-260516a36be4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "8a6c184b-f307-4ecf-ad5c-a0dba627dcf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "163db437-1529-4d8d-ba27-260516a36be4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96281df5-a0ba-4f2d-b3f7-a2663e962b4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "163db437-1529-4d8d-ba27-260516a36be4",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "e093d478-3521-41c0-b2ca-8176d7e02a60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "53f9735a-9d31-45b0-90b7-e75fbd6ee6dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e093d478-3521-41c0-b2ca-8176d7e02a60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6793b470-1d49-4478-8f75-9608134b4ca6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e093d478-3521-41c0-b2ca-8176d7e02a60",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "4ec6341d-cf06-4bb2-93e9-4577fdcf6345",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "a54cc698-9842-468f-98db-7a23cee22939",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ec6341d-cf06-4bb2-93e9-4577fdcf6345",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2c1a235-9f68-464f-9a17-3437864d6a28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ec6341d-cf06-4bb2-93e9-4577fdcf6345",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "d73d57a9-7568-46a3-82f6-3358e4f2a85c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "6efc0107-9120-4818-8768-b682c806e482",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d73d57a9-7568-46a3-82f6-3358e4f2a85c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8acc933-9c53-4d66-9e22-d5c56bd0301b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d73d57a9-7568-46a3-82f6-3358e4f2a85c",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "77f2c795-07e5-4f9e-913f-cf04b90ce05e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "28b9586a-d0f5-4c6a-ba2b-954bc8cc9a15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77f2c795-07e5-4f9e-913f-cf04b90ce05e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfee1bb8-7048-4251-a87e-b72f58a9c8a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77f2c795-07e5-4f9e-913f-cf04b90ce05e",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "65781de4-2ef0-4360-b831-29d4bc20bed7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "01af059a-8560-4394-80af-3ae3d8750a72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65781de4-2ef0-4360-b831-29d4bc20bed7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd6720c1-7e94-4e40-9b24-0d61576dc3e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65781de4-2ef0-4360-b831-29d4bc20bed7",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "d44a04e4-6068-4249-b0dc-1ab8dc0fcb1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "b11e2850-33eb-4c30-96bd-07ee4153eccd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d44a04e4-6068-4249-b0dc-1ab8dc0fcb1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86dab028-b27a-4135-ae61-0714a733f95b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d44a04e4-6068-4249-b0dc-1ab8dc0fcb1f",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "7b5ab5a2-6530-4bdb-8614-0c1bd4ab3335",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "a1600bb9-66c9-4b5c-9b86-3d6cbec74c6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b5ab5a2-6530-4bdb-8614-0c1bd4ab3335",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ff2f7bc-ce05-4a97-8022-b28a95fca201",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b5ab5a2-6530-4bdb-8614-0c1bd4ab3335",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "c1c7a0cd-a48b-4a79-b598-7099c3bafbc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "0b65db04-c949-4b7d-ba55-d31b392deac2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1c7a0cd-a48b-4a79-b598-7099c3bafbc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4142a6d3-e9ba-4736-a578-9a802efa1439",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1c7a0cd-a48b-4a79-b598-7099c3bafbc5",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "2a8464bd-4274-464d-844f-1c3d02ca64a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "aa1ffab2-54b5-4fd7-82b9-53c39e817b0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a8464bd-4274-464d-844f-1c3d02ca64a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2fdfcb4-1d89-4554-a814-0097b82a536a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a8464bd-4274-464d-844f-1c3d02ca64a3",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "0c1ba490-07e9-4cc2-bb11-36e78e6a748c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "b81a7b1a-adc0-4500-a209-838462647236",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c1ba490-07e9-4cc2-bb11-36e78e6a748c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e228ac2-2865-4eae-bff1-432763dcf08e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c1ba490-07e9-4cc2-bb11-36e78e6a748c",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "2ba87dfc-1c01-4311-aa54-b700937a5b4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "c31e6004-f424-4792-8ba7-073c628bac88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ba87dfc-1c01-4311-aa54-b700937a5b4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "003bca38-0d83-4c06-9a65-f7015487e5a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ba87dfc-1c01-4311-aa54-b700937a5b4d",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "57814a3a-c0c8-40ad-bace-913c2a9cf89b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "80c9a545-09d5-4c4b-a19d-e6258a00eb47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57814a3a-c0c8-40ad-bace-913c2a9cf89b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40150b67-3a6e-4980-8ba0-0bd90a86927c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57814a3a-c0c8-40ad-bace-913c2a9cf89b",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "cb028658-6788-4363-9e70-6cf3751758f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "1c62f17a-6d5d-4bbb-ba51-b6ca79074b9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb028658-6788-4363-9e70-6cf3751758f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfccb6eb-c10f-431f-ae5a-e6d042d4646c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb028658-6788-4363-9e70-6cf3751758f0",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "b96990bc-1a8d-4d72-acea-0825bfb110fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "66385247-d70e-4dda-9d40-6681059725f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b96990bc-1a8d-4d72-acea-0825bfb110fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d457494-e840-48eb-95d9-4df712b2ef99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b96990bc-1a8d-4d72-acea-0825bfb110fe",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "5c53aca5-78ba-45d6-b576-4562a1a7de93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "5b89dde6-0395-45da-8b0d-24e78d74849c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c53aca5-78ba-45d6-b576-4562a1a7de93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28064fe4-d0df-47fa-9f34-74c15d468b77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c53aca5-78ba-45d6-b576-4562a1a7de93",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "e93ba96b-b4e1-49ac-9bab-a816dd02036e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "9afb86f5-7312-41f0-a40c-d46101b19a1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e93ba96b-b4e1-49ac-9bab-a816dd02036e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b630f1f-15a5-4504-a925-c478458414fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e93ba96b-b4e1-49ac-9bab-a816dd02036e",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "d49cdfa8-7df0-4dd6-8a1b-999ee15e536e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "a24456f2-064f-476b-a774-f263a3281d13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d49cdfa8-7df0-4dd6-8a1b-999ee15e536e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70ff2daa-e556-4d5d-8078-172d783504c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d49cdfa8-7df0-4dd6-8a1b-999ee15e536e",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "0c3e3b5a-9084-4a53-861c-2543989dc015",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "3349897f-b6f5-4ff6-9a17-f777318abcd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c3e3b5a-9084-4a53-861c-2543989dc015",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da71b839-9962-4f50-9b76-7cec9590eb69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c3e3b5a-9084-4a53-861c-2543989dc015",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "0011dad9-02f4-4503-8f79-21b827efd962",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "51d5dd70-7d3e-437c-99a4-ca7912434e1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0011dad9-02f4-4503-8f79-21b827efd962",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb54b699-b7d6-4dcb-8e81-5f9c2093b9fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0011dad9-02f4-4503-8f79-21b827efd962",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "5b25589f-4b88-4f03-91da-314399b0fe86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "60a643a3-6667-432f-9717-cef755e6f704",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b25589f-4b88-4f03-91da-314399b0fe86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f97c467d-761f-41c4-b2f5-58a1d7e4487e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b25589f-4b88-4f03-91da-314399b0fe86",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "6ebf32d2-9787-4f4b-a17c-8342d9a1b381",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "cc481e12-fc84-4d03-a0d8-e18132ba0d75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ebf32d2-9787-4f4b-a17c-8342d9a1b381",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3b5be9e-56b7-46b9-90c5-8a9ffb2bd6f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ebf32d2-9787-4f4b-a17c-8342d9a1b381",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "f7886f10-e218-4766-a510-9095b7c4e4d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "f714dd54-69a3-4c94-a7b8-e1135c169aac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7886f10-e218-4766-a510-9095b7c4e4d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e40d8a9-6003-4f2b-944b-3c878d728e79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7886f10-e218-4766-a510-9095b7c4e4d3",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "6d4e2f1d-5a42-4991-b329-8ad7f4cd0377",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "6b86a4db-4a83-4984-ba69-a15a32b0a3ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d4e2f1d-5a42-4991-b329-8ad7f4cd0377",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7573284b-54b3-4149-83bd-7a72b5e86a98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d4e2f1d-5a42-4991-b329-8ad7f4cd0377",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "5ad02e17-578f-4ade-8fd4-7573dac4c3ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "2fe38379-e607-4c93-819d-0989606a3c49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ad02e17-578f-4ade-8fd4-7573dac4c3ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ade82219-c129-4331-84fa-a35c06dbd42b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ad02e17-578f-4ade-8fd4-7573dac4c3ae",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "703298b9-4b7c-434a-a8a1-e4c1bb2f0f3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "bca3a13a-4a6f-4e47-aa19-e63544bb6ba6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "703298b9-4b7c-434a-a8a1-e4c1bb2f0f3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2967842e-b23f-48ac-ae63-5d47f4e48fa1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "703298b9-4b7c-434a-a8a1-e4c1bb2f0f3a",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "d8b907b0-ecae-4ed0-97ac-b8724f2cae2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "2ca5e277-6570-449d-aea7-2b9927bf31c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8b907b0-ecae-4ed0-97ac-b8724f2cae2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6635b732-93b5-4e68-b4ad-e1861337389f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8b907b0-ecae-4ed0-97ac-b8724f2cae2e",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "f248761c-2969-452f-80d3-44551f245c30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "c0d02407-a652-4179-a97a-181e4341461b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f248761c-2969-452f-80d3-44551f245c30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0127605-d27c-4578-81a4-0d3c90371218",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f248761c-2969-452f-80d3-44551f245c30",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "93562a18-e066-4eaf-a93e-53573c442216",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "685b8907-65bc-4bec-9b0f-c26a3202de9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93562a18-e066-4eaf-a93e-53573c442216",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea962ceb-2089-4173-aac8-311566432eec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93562a18-e066-4eaf-a93e-53573c442216",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "a78d39bc-dc16-4cdc-9795-ef180e2388e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "19141bff-1eb5-4c8c-add8-3ae2c4dc2c02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a78d39bc-dc16-4cdc-9795-ef180e2388e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e11d06b-af68-4f33-bbd8-757be6765cf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a78d39bc-dc16-4cdc-9795-ef180e2388e6",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "c676631f-5a82-4a52-ab40-95b8042860e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "fdba2a0d-3038-4092-b0d7-611b86e354ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c676631f-5a82-4a52-ab40-95b8042860e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "969877ed-deed-4bab-83d1-848968cc88aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c676631f-5a82-4a52-ab40-95b8042860e7",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "324347b0-feeb-44b9-bd3e-f9773dacafcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "c8241fff-f7f9-4bfe-9a4b-3825b2ce72fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "324347b0-feeb-44b9-bd3e-f9773dacafcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d156c582-0539-4946-b306-cae4bdeea082",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "324347b0-feeb-44b9-bd3e-f9773dacafcc",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        },
        {
            "id": "9232dbf9-3168-4b30-aa38-64c1e77dd086",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "compositeImage": {
                "id": "f4953747-d8aa-44da-82ef-f85a8646ff1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9232dbf9-3168-4b30-aa38-64c1e77dd086",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4699a110-7ea0-4c8d-8569-94b31ab06e30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9232dbf9-3168-4b30-aa38-64c1e77dd086",
                    "LayerId": "c92a508a-38a3-4154-a739-a492ca3e9d49"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "c92a508a-38a3-4154-a739-a492ca3e9d49",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4aa5fbb0-ce1e-4084-b57f-863449645d81",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 63,
    "yorig": 87
}