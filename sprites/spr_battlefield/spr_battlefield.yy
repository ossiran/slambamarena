{
    "id": "6e93f744-af1a-49a6-9bf6-9d9fb0cffbc2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battlefield",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 10,
    "bbox_right": 54,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e6bf9e86-c291-4052-9efa-5127508b166c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e93f744-af1a-49a6-9bf6-9d9fb0cffbc2",
            "compositeImage": {
                "id": "321503d1-ab1a-4121-9b87-f30549be3dc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6bf9e86-c291-4052-9efa-5127508b166c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cb0078b-286b-463a-b2cc-c8ed0b6a9f6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6bf9e86-c291-4052-9efa-5127508b166c",
                    "LayerId": "cf3c5d67-474f-45b4-8cde-e6d5b93942fc"
                }
            ]
        },
        {
            "id": "9161893b-8cb2-43b4-89cf-5a9ed811f9a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e93f744-af1a-49a6-9bf6-9d9fb0cffbc2",
            "compositeImage": {
                "id": "d16f36f8-a536-4c3b-a851-a27fe8d8dc45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9161893b-8cb2-43b4-89cf-5a9ed811f9a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4076f98-a2e2-4080-8078-53c27df133cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9161893b-8cb2-43b4-89cf-5a9ed811f9a6",
                    "LayerId": "cf3c5d67-474f-45b4-8cde-e6d5b93942fc"
                }
            ]
        },
        {
            "id": "c8ad97f3-7099-4ef2-a124-32175ac4f5ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e93f744-af1a-49a6-9bf6-9d9fb0cffbc2",
            "compositeImage": {
                "id": "5339a0d5-e33e-4225-bb91-4291246b60f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8ad97f3-7099-4ef2-a124-32175ac4f5ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f197eb28-d815-4fb5-8263-6db6ef364cd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8ad97f3-7099-4ef2-a124-32175ac4f5ef",
                    "LayerId": "cf3c5d67-474f-45b4-8cde-e6d5b93942fc"
                }
            ]
        },
        {
            "id": "caff6fa2-65c0-4a01-8e90-2c471f96dd73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e93f744-af1a-49a6-9bf6-9d9fb0cffbc2",
            "compositeImage": {
                "id": "c224e480-1ef8-486c-8dcf-5601180cdabb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caff6fa2-65c0-4a01-8e90-2c471f96dd73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef6644d3-2ff4-46f3-a1d4-b128350aac4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caff6fa2-65c0-4a01-8e90-2c471f96dd73",
                    "LayerId": "cf3c5d67-474f-45b4-8cde-e6d5b93942fc"
                }
            ]
        },
        {
            "id": "b064ea6b-8cf8-4ffa-88cc-f6d35442b171",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e93f744-af1a-49a6-9bf6-9d9fb0cffbc2",
            "compositeImage": {
                "id": "7fe29ecb-c2db-46c1-89b7-3ff56b4c7d7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b064ea6b-8cf8-4ffa-88cc-f6d35442b171",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52faea9c-870d-44ec-b0cc-483924317f0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b064ea6b-8cf8-4ffa-88cc-f6d35442b171",
                    "LayerId": "cf3c5d67-474f-45b4-8cde-e6d5b93942fc"
                }
            ]
        },
        {
            "id": "31721c40-890b-4246-8ae5-0094beacf4a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e93f744-af1a-49a6-9bf6-9d9fb0cffbc2",
            "compositeImage": {
                "id": "19527006-1dc2-4639-959a-c616bf2c5a4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31721c40-890b-4246-8ae5-0094beacf4a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9846aabb-5976-45a5-8082-df9815922d23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31721c40-890b-4246-8ae5-0094beacf4a8",
                    "LayerId": "cf3c5d67-474f-45b4-8cde-e6d5b93942fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "cf3c5d67-474f-45b4-8cde-e6d5b93942fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6e93f744-af1a-49a6-9bf6-9d9fb0cffbc2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 32
}