if(state_new == 1)
{
	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -42;
	    xscale  = 26;
	    yscale  = 74;
	    xshift  = -2;
	}
	
			with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 16;
	    image_yscale  = 47;
	    xshift = -2
	}
	attackframe0 =  10;
	attackframe2 =  16;
	endstate = 34;
}


if(state_timer > attackframe0 && state_timer < attackframe2 && hitpause == 0)
{
    vx = 8 * image_xscale;
}
else
{
    vx = Approach(vx,0,4);
}

/*
if(state_timer > 6 && state_timer < 9)
{
    vy = -4.2;
}


if(state_timer == 15)
{
    vy = 11;
}
*/


if(state_timer == attackframe2)
{
    hitbox1 = create_hitbox(-4, -20,50, 50, 4.5, -11, 14,9,10,20,-1,0,0,3,"mid",-1);
	hitbox1.hitsound = snd_hit;
	ex += 4;
	hitbox1.scaling = 0.9;
	hitbox1.airxtrayhit = 18;
}

sprite_index = spr_bill_qcbl;

attack_base(endstate,0,0,0,1,0);