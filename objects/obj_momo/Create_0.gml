/// @description Insert description here
// You can write your code in this editor
//
event_inherited();

state_machine_init();

state_create("stand",state_momo_stand);
state_create("walk",state_momo_walk);
state_create("fdash",state_momo_fdash);
state_create("bdash",state_momo_bdash);
state_create("crouch",state_momo_crouch);
state_create("jumpsquat",state_momo_jumpsquat);
state_create("air",state_momo_air);
state_create("airdash",state_momo_airdash);
state_create("hitstun",state_momo_hitstun);
state_create("hitfall",state_momo_hitfall);
state_create("bounce",state_momo_bounce);
state_create("knockdown",state_momo_knockdown);
state_create("stagger",state_momo_stagger);
state_create("recovery",state_momo_recovery);
state_create("sblock",state_momo_sblock);
state_create("cblock",state_momo_cblock);
state_create("ablock",state_momo_ablock);
state_create("light",state_momo_light);
state_create("clight",state_momo_clight);
state_create("medium",state_momo_medium);
state_create("fmedium",state_momo_dmedium);
state_create("cmedium",state_momo_cmedium);
state_create("heavy",state_momo_heavy);
state_create("cheavy",state_momo_cheavy);
state_create("fheavy",state_momo_dheavy);
state_create("chbl",state_momo_chbl);
state_create("chbm",state_momo_chbm);
state_create("chbh",state_momo_chbh);
state_create("qcbl",state_momo_qcbl);
state_create("qcbm",state_momo_qcbm);
state_create("qcbh",state_momo_qcbh);
state_create("chdl",state_momo_chdl);
state_create("chdm",state_momo_chdm);
state_create("chdh",state_momo_chdh);
state_create("chul",state_momo_chul);
state_create("chum",state_momo_chum);
state_create("chuh",state_momo_chuh);
state_create("ddl",state_momo_ddl);
state_create("ddm",state_momo_ddl);
state_create("ddh",state_momo_ddh);
state_create("dqcf",state_momo_dqcf);
state_create("grab",state_momo_grab);
state_create("lair",state_momo_lair);
state_create("mair",state_momo_mair);
state_create("hair",state_momo_hair);
state_create("agrab",state_momo_agrab);
state_create("bthrow",state_momo_bthrow);
state_create("fthrow",state_momo_fthrow);
state_create("athrow",state_momo_athrow);
state_create("grabbed",state_momo_grabbed);
state_create("grabtech",state_momo_grabtech);
state_create("geniemove",state_momo_geniemove);
state_create("landing",state_momo_landing);

state_init("stand");

ground_accel = 1;
air_accel = 0.12;
ground_fric = 1;
air_fric = 0.5;
grav = 0.75;
jumpheight = 10;
vxMax = 1;
vyMax = 8;
doublejump = 1;
dashdir = 0;

myname = "momo";

hurtbox0 = create_hurtbox(-29,-36,58,68,-1,-1);
hurtbox1 = create_hurtbox(-15,-61,30,25,-1,-3);

collisionbox = create_collisionbox(-12,-20,24,50,-2);

hurt_spriteG = spr_bill_hitstun_g;
hurt_spriteA = spr_bill_hitstun_a;
fall_sprite = spr_bill_hitfall;
knock_sprite = spr_bill_knockdown;
idle_sprite = spr_momo_idle;

hpmax = 240;
hp = hpmax;


//heat stuff
heat = 0;
momomode = 1;

dqcf_buffer = 0;
qcb_buffer = 0;
ff_buffer = 0;
bb_buffer = 0;
dd_buffer = 0;

mask_index = spr_momo_idle;