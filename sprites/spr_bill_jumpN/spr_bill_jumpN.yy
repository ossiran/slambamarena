{
    "id": "33231476-651b-4f60-9bc8-8b01d452c65e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_jumpN",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 141,
    "bbox_left": 52,
    "bbox_right": 88,
    "bbox_top": 70,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b511e8b4-a0d0-4d7e-a10a-d621b906b59c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33231476-651b-4f60-9bc8-8b01d452c65e",
            "compositeImage": {
                "id": "9c874c63-f6c8-4d8b-b3d9-50c1dacb9c6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b511e8b4-a0d0-4d7e-a10a-d621b906b59c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e119acd0-9536-4641-8bb2-4c4e7b0dc187",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b511e8b4-a0d0-4d7e-a10a-d621b906b59c",
                    "LayerId": "1e5a0b64-138d-46dc-8ab4-6f2c908b82ec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "1e5a0b64-138d-46dc-8ab4-6f2c908b82ec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "33231476-651b-4f60-9bc8-8b01d452c65e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 68,
    "yorig": 118
}