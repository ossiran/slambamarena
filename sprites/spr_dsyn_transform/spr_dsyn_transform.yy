{
    "id": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dsyn_transform",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 108,
    "bbox_left": 53,
    "bbox_right": 109,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2693a6d9-4587-4135-af2a-25b955d1b618",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "aa1c82cc-c550-4454-bf74-2a3f3cbe2a68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2693a6d9-4587-4135-af2a-25b955d1b618",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a510229e-5b75-4866-beec-47a2b111794e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2693a6d9-4587-4135-af2a-25b955d1b618",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "de55be33-ed3f-497d-91c0-9617a933abae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "ad2ef0b5-21cf-4de3-882a-0cd34df0d965",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de55be33-ed3f-497d-91c0-9617a933abae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98c68b84-47f4-4976-bc02-dab702ded919",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de55be33-ed3f-497d-91c0-9617a933abae",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "13c52b12-a20e-4144-80e7-0d9846dfe3fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "d003082d-4956-4eda-a1fb-7aec137b7554",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13c52b12-a20e-4144-80e7-0d9846dfe3fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3c8c771-49c2-405f-99b9-d94c7ba4a0d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13c52b12-a20e-4144-80e7-0d9846dfe3fd",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "a431d439-06ac-471c-8966-ad32068286f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "95ff8761-0b63-4d54-a10c-223f8fd3b2e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a431d439-06ac-471c-8966-ad32068286f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f9676a5-f81f-42a6-b7b1-88b7528381a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a431d439-06ac-471c-8966-ad32068286f0",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "820b0c08-ce0d-4a09-b0d8-56da78607bbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "a6773698-f88c-44da-a30d-2d59698a7bae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "820b0c08-ce0d-4a09-b0d8-56da78607bbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c0e32fb-e81c-415a-8382-f96735ad05f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "820b0c08-ce0d-4a09-b0d8-56da78607bbb",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "59e3068d-c1f6-463a-8308-d58ebdeffc1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "ed947e10-77dd-4c74-81ac-6575f712f8be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59e3068d-c1f6-463a-8308-d58ebdeffc1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "268772a6-113d-4382-88f6-467716c31b9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59e3068d-c1f6-463a-8308-d58ebdeffc1d",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "df7fe23e-5476-41bb-9b7f-3b2f58d2184f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "7a6aec73-0cf6-433c-998b-1513e04b1ab8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df7fe23e-5476-41bb-9b7f-3b2f58d2184f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbd6e677-d25e-467e-bfd9-5411543e3d35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df7fe23e-5476-41bb-9b7f-3b2f58d2184f",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "3e0a8c34-e682-4027-a06b-9520da9f9ed4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "1ce64a93-16e1-4af6-8d5e-9ef9a7c207f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e0a8c34-e682-4027-a06b-9520da9f9ed4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e221a801-f68f-4bdc-9a20-f970c0b832fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e0a8c34-e682-4027-a06b-9520da9f9ed4",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "b2ea07af-2a2e-408f-8cb9-74f1fd898c77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "67d9c67a-724c-445a-9ef4-f01a4cd21bf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2ea07af-2a2e-408f-8cb9-74f1fd898c77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab047ef4-1fda-49a0-9137-dad96b06f33e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2ea07af-2a2e-408f-8cb9-74f1fd898c77",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "f983b3b4-9577-4fa5-ad48-90101b5c1e37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "deb9dbfb-25c1-48f4-803c-9ca253558dca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f983b3b4-9577-4fa5-ad48-90101b5c1e37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78bfef30-e762-4e58-b9cd-6c32097aa5b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f983b3b4-9577-4fa5-ad48-90101b5c1e37",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "68222658-5701-43c5-95dc-691eadc0050a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "43a867e3-9d99-46a7-8355-fa8799ac51fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68222658-5701-43c5-95dc-691eadc0050a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c0a4782-e014-42ab-9534-685fddc00d9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68222658-5701-43c5-95dc-691eadc0050a",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "f1070f8c-f451-443c-9053-02587ca48028",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "ced0a404-bdc8-4158-a5e4-07173daefe44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1070f8c-f451-443c-9053-02587ca48028",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e08ca9c5-8de6-4404-831a-464a7ae9336b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1070f8c-f451-443c-9053-02587ca48028",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "e36dacbe-e10f-4f7a-99c9-9aface086b29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "3e0a7aa0-bbfd-4862-b35f-e2c2a612d156",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e36dacbe-e10f-4f7a-99c9-9aface086b29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75811184-2eb7-433f-8830-c0427e4bf03a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e36dacbe-e10f-4f7a-99c9-9aface086b29",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "3b213aac-59d6-4318-95e7-b3d3b3c4553b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "ee354edd-4724-45c4-80de-894a39d4f8a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b213aac-59d6-4318-95e7-b3d3b3c4553b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11b17914-be1f-4581-b00b-7c4caca5fb95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b213aac-59d6-4318-95e7-b3d3b3c4553b",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "58279107-9c47-4056-b778-461d75a23b06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "f15d1b66-ac2a-4efd-9267-deb11f4e2f5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58279107-9c47-4056-b778-461d75a23b06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5cf704b-94a4-45a8-a8e1-2b239106872b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58279107-9c47-4056-b778-461d75a23b06",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "ca58d4d0-ce5e-4558-b7fd-b7c94456e4a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "3b069cd5-7d1b-43ad-9d58-11590fce19de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca58d4d0-ce5e-4558-b7fd-b7c94456e4a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2bf235e-18da-41ac-b0eb-2a50103d5817",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca58d4d0-ce5e-4558-b7fd-b7c94456e4a5",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "c5d2000d-3b2d-46b5-8dbf-ba8038253ed2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "4ec275bc-e5e6-4b91-834b-a1bf431257c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5d2000d-3b2d-46b5-8dbf-ba8038253ed2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fb4a9d9-328a-4ba5-a3c7-74e43aad4420",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5d2000d-3b2d-46b5-8dbf-ba8038253ed2",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "07f63d65-1e44-4891-bc5c-0d4795c7dc07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "c939a562-8c92-43db-ba98-faa4f2871cdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07f63d65-1e44-4891-bc5c-0d4795c7dc07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10c451af-ed9c-437f-ada8-f507a028ec73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07f63d65-1e44-4891-bc5c-0d4795c7dc07",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "656de78e-e870-49c8-b8c9-ed068953c646",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "7958f4b6-e57e-437e-8612-e522a3641249",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "656de78e-e870-49c8-b8c9-ed068953c646",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "659a30ec-3ae7-4502-aa88-6e7cf961a276",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "656de78e-e870-49c8-b8c9-ed068953c646",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "b388c791-63ab-4028-95f8-aed060a93f07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "6b349997-6e93-4e32-80eb-d140dc503c63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b388c791-63ab-4028-95f8-aed060a93f07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb2337ca-8199-4fea-bd2a-d442b81aaf1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b388c791-63ab-4028-95f8-aed060a93f07",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "c3ef10bc-018f-4c15-af79-e551f355081b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "884fd52d-40e4-49ad-a053-816dd98c11ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3ef10bc-018f-4c15-af79-e551f355081b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a900bed1-e24a-406f-84bd-8f416a43fa9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3ef10bc-018f-4c15-af79-e551f355081b",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "6e5c8499-9d0c-4d2c-8cd3-ccecf918c9f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "b412a7c4-7844-4922-af72-25afc75991c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e5c8499-9d0c-4d2c-8cd3-ccecf918c9f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15a2510a-a090-4e15-a462-c95529e09a1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e5c8499-9d0c-4d2c-8cd3-ccecf918c9f7",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "31f291eb-9f70-491b-8582-612206b1b6e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "debfc1ca-755d-41b7-83cf-0529fe86a36a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31f291eb-9f70-491b-8582-612206b1b6e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "097689ed-05a9-42ff-83b4-5b3d7aec7e62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31f291eb-9f70-491b-8582-612206b1b6e0",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "005c88cc-9aa5-4518-b00d-5548c6c67650",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "26d76f6e-18fc-4f2c-a0db-59d0bb19832f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "005c88cc-9aa5-4518-b00d-5548c6c67650",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4bc17b8-8400-484d-9804-4096fa398822",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "005c88cc-9aa5-4518-b00d-5548c6c67650",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "003294bc-d3b8-4194-9d2a-0940ab1d278e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "1748d0a4-9282-47c8-bc8e-65b3887b16c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "003294bc-d3b8-4194-9d2a-0940ab1d278e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fd5a49b-ac5f-4a0b-a657-b6d7b109a363",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "003294bc-d3b8-4194-9d2a-0940ab1d278e",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "d891cecf-d18d-45b4-9c6b-1733acd1e879",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "f5b109f0-a9c8-41d8-b976-6f03e52ed44e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d891cecf-d18d-45b4-9c6b-1733acd1e879",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe690b81-cf72-42ab-849c-af0820ff8630",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d891cecf-d18d-45b4-9c6b-1733acd1e879",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "97fb5101-b06c-406b-b241-2dae3058cc47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "a1154a8d-821f-4aa7-bccf-380facf3526d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97fb5101-b06c-406b-b241-2dae3058cc47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d60b0b68-a72c-4075-a4e2-d7d55d09e4e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97fb5101-b06c-406b-b241-2dae3058cc47",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "eef29f9e-74b9-41da-8eef-54ee721de48d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "23d8e393-4085-4411-8b1a-999de4c90754",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eef29f9e-74b9-41da-8eef-54ee721de48d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ddc43d1-389c-4d59-beea-ac0b7dd10f27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eef29f9e-74b9-41da-8eef-54ee721de48d",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "1b524d54-27a1-44e3-9e40-6d6a9a34aac0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "a2e50099-537d-4217-8896-ceb0e8aec334",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b524d54-27a1-44e3-9e40-6d6a9a34aac0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a8507fe-8584-4fd4-8e2e-e9eed4fd1be8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b524d54-27a1-44e3-9e40-6d6a9a34aac0",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "a7b02e25-a395-49d0-8a15-0552ba9e9f02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "9f373b8b-fee8-4a1c-91e7-b0d04ce4a420",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7b02e25-a395-49d0-8a15-0552ba9e9f02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acc900c8-1228-4667-8530-6dd9402558cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7b02e25-a395-49d0-8a15-0552ba9e9f02",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "748c2697-bf99-44b6-9821-887aec4e3038",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "ab3132c5-77cd-4153-8b0c-61858f6b3ddf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "748c2697-bf99-44b6-9821-887aec4e3038",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "216033e4-5bc8-4b1e-87b1-cbdda6b0ed0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "748c2697-bf99-44b6-9821-887aec4e3038",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "c79590fe-39f6-494d-95af-148253f704f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "47880d09-b1c7-4dbf-b423-2630e0e51533",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c79590fe-39f6-494d-95af-148253f704f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d5a080d-d895-490d-a667-1fdeeb7529e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c79590fe-39f6-494d-95af-148253f704f2",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "d93cf5a9-e5cf-4528-837b-e57ae271eb4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "d485b03c-c1aa-4f73-9418-33e4d7821753",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d93cf5a9-e5cf-4528-837b-e57ae271eb4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8901b44f-2744-49b2-b667-5bac9a114bf5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d93cf5a9-e5cf-4528-837b-e57ae271eb4b",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "5f6836b6-db56-4f04-82da-4ebe0158d823",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "373ca221-a690-42fa-bca5-dcc8cc320896",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f6836b6-db56-4f04-82da-4ebe0158d823",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47078d10-dcdc-4f66-b8b4-1da5add30b37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f6836b6-db56-4f04-82da-4ebe0158d823",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        },
        {
            "id": "5010e423-bdd8-4bb6-a60b-1be48b0fc8a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "compositeImage": {
                "id": "9459c20b-7162-4434-8d57-5e707a3f7804",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5010e423-bdd8-4bb6-a60b-1be48b0fc8a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "936b9448-80ff-4afa-834f-2e8d3ef00f78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5010e423-bdd8-4bb6-a60b-1be48b0fc8a5",
                    "LayerId": "1391911b-efbc-4912-ad28-ed8bb6a418e3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "1391911b-efbc-4912-ad28-ed8bb6a418e3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7b802783-0269-4f5c-8ab3-20bf605c2ea1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 168,
    "xorig": 85,
    "yorig": 80
}