{
    "id": "bcd358ba-f7d1-4489-9053-5e0d4c00456a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_myu_falling",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 12,
    "bbox_right": 35,
    "bbox_top": 11,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c13cba21-3d7c-465e-ba92-7fdba9a0b7e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcd358ba-f7d1-4489-9053-5e0d4c00456a",
            "compositeImage": {
                "id": "07d185d3-d0ce-4db7-95ff-d516fcce12d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c13cba21-3d7c-465e-ba92-7fdba9a0b7e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e65423e-f0e5-43f0-9359-e9a942867600",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c13cba21-3d7c-465e-ba92-7fdba9a0b7e4",
                    "LayerId": "1c44ba65-39a9-44fd-b4a6-d6284ee76096"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1c44ba65-39a9-44fd-b4a6-d6284ee76096",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bcd358ba-f7d1-4489-9053-5e0d4c00456a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 32
}