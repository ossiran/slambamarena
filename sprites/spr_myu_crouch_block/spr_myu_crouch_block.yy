{
    "id": "c5138a52-1048-45cb-91aa-19145bb6b809",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_myu_crouch_block",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 14,
    "bbox_right": 42,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ce940b3-bb18-4fb0-832d-29ad38cc178e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5138a52-1048-45cb-91aa-19145bb6b809",
            "compositeImage": {
                "id": "35a3ce96-856f-4827-a4ca-fa82a03d5652",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ce940b3-bb18-4fb0-832d-29ad38cc178e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d41d6ac-8e5d-44c4-a07b-fe5881ac95dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ce940b3-bb18-4fb0-832d-29ad38cc178e",
                    "LayerId": "517e3bab-009a-4a24-ad6f-02c02966bb07"
                }
            ]
        },
        {
            "id": "faadc24a-08f6-4cfd-929c-286958963663",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5138a52-1048-45cb-91aa-19145bb6b809",
            "compositeImage": {
                "id": "00dbc62f-d70b-4e07-8826-4c741138031b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "faadc24a-08f6-4cfd-929c-286958963663",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "407d167f-23e3-4b80-a2aa-073857321e41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "faadc24a-08f6-4cfd-929c-286958963663",
                    "LayerId": "517e3bab-009a-4a24-ad6f-02c02966bb07"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "517e3bab-009a-4a24-ad6f-02c02966bb07",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c5138a52-1048-45cb-91aa-19145bb6b809",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 32
}