live_call();

if(state_new == 1)
{
    with(hurtbox0)
	{
	    xoffset = -12;
	    yoffset = -13;
	    image_xscale  = 24;
	    image_yscale  = 40;
	    xshift  = 0;
	}
	
	with(hurtbox1)
	{
	    xoffset = -8;
	    yoffset = -31;
	    image_xscale  = 16;
	    image_yscale  = 18;
	    xshift = 0;
	}
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -13;
	    image_xscale  = 16;
	    image_yscale  = 40;
	    xshift = 0;
	}
}




image_xscale = -frontOrBack;

if(synmode < 0)
{
    //right
    if(input_check(6,0,0,0,0))
    {
    	vx = Approach(vx,vxMax * image_xscale,ground_accel);
    }
    
    //left
    if(input_check(4,0,0,0,0))
    {
    	vx = Approach(vx,-vxMax * image_xscale + 1 * image_xscale,ground_accel);
    }
    
    if(sign(vx) == frontOrBack)
    {
        sprite_index = spr_dsyn_bwalk;
        hurtbox0.xshift = 3;
        hurtbox1.xshift = 3;
        collisionbox.xshift = 3;
    }
    else
    {
        sprite_index = spr_dsyn_fwalk;
        hurtbox0.xshift = -3;
        hurtbox1.xshift = -3;
        collisionbox.xshift = -3;
    }
}
else
{
    //right
    if(input_check(6,0,0,0,0))
    {
    	vx = Approach(vx,vxMax * image_xscale + 1 * image_xscale,ground_accel);
    }
    
    //left
    if(input_check(4,0,0,0,0))
    {
    	vx = Approach(vx,-vxMax * image_xscale,ground_accel);
    }
    
    if(sign(vx) == frontOrBack)
    {
        sprite_index = spr_lsyn_bwalk;
        hurtbox0.xshift = 3;
        hurtbox1.xshift = 3;
        collisionbox.xshift = 3;
    }
    else
    {
        sprite_index = spr_lsyn_fwalk;
        hurtbox0.xshift = -3;
        hurtbox1.xshift = -3;
        collisionbox.xshift = -3;
    }
}

//friction
if((key_left == 0 && key_right == 0)||(key_left == 1 && key_right == 1))
{
	vx = Approach(vx,0,ground_fric);
	state_switch("stand");
}

//jab
if(input_check(0,1,0,0,0))
{
    state_switch("light");
}

//clight
if(input_check(2,1,0,0,0) || input_check(1,1,0,0,0) || input_check(3,1,0,0,0))
{
    state_switch("clight");
}

//medium
if(input_check(0,0,1,0,0))
{
    state_switch("medium");
}

//cmedium
if(input_check(2,0,1,0,0) || input_check(1,0,1,0,0) || input_check(3,0,1,0,0))
{
    state_switch("cmedium");
}

//heavy
if(input_check(0,0,0,1,0))
{
    state_switch("heavy");
}


//cheavy
if(input_check(2,0,0,1,0))
{
    state_switch("cheavy");
}

//qcf
if(qcf_buffer > 0)
{
    if(input_check(0,0,0,1,0))
    {
        if(ex > 24.9)
        {
            state_switch("qcfh");
        }
        else
        {
            state_switch("qcfm");
        }
    }
    else if(input_check(0,0,1,0,0))
    {
        state_switch("qcfm");
    }
    else if(input_check(0,1,0,0,0))
    {
        state_switch("qcfl");
    }
}


//qcb
if(qcb_buffer > 0)
{
    if(input_check(0,0,0,1,0))
    {
        if(ex > 24.9)
        {
            state_switch("qcbh");
        }
        else
        {
            state_switch("qcbm");
        }
    }
    else if(input_check(0,0,1,0,0))
    {
        state_switch("qcbm");
    }
    else if(input_check(0,1,0,0,0))
    {
        state_switch("qcbl");
    }
}

//srk
if(srk_buffer > 0)
{
    if(input_check(0,0,0,1,0))
    {
        if(ex > 24.9)
        {
            state_switch("srkh");
        }
        else
        {
            state_switch("srkm");
        }
    }
    else if(input_check(0,0,1,0,0))
    {
        state_switch("srkm");
    }
    else if(input_check(0,1,0,0,0))
    {
        state_switch("srkl");
    }
}

//grab
if(input_check(0,1,0,0,1))
{
    state_switch("grab");
}

//dash
//fdash
if(ff_buffer > 0 || (input_check(6, 1, 1, 0, 0)) || (input_check(5, 1, 1, 0, 0)))
{
        state_switch("fdash");
}
//bdash
if(bb_buffer > 0 || (input_check(4, 1, 1, 0, 0)))
{
        state_switch("bdash");
}

//jump
if(input_check(7,0,0,0,0) || input_check(8,0,0,0,0) || input_check(9,0,0,0,0))
{
	state_switch("jumpsquat");
}

//crouch
if(input_check(1,0,0,0,0) || input_check(2,0,0,0,0) || input_check(3,0,0,0,0))
{
     state_switch("crouch");   
}


//stand
if(input_check(5,0,0,0,0))
{
	state_switch("stand");
}

//transform
if(synmode < 1 && synmode > -1)
{
    state_switch("transform");
}
//block
if(blockstun > 0)
{
	state_switch("sblock");	
}

//hitstun
if(hitstun > 0)
{
    state_switch("hitstun");   
}

if(grabbed == 1)
{
 state_switch("grabbed");   
}