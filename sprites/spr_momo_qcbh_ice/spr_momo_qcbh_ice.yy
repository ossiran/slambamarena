{
    "id": "9bd690c7-7f3f-4627-b10b-b38ccda92c2f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_qcbh_ice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "25e61687-90dd-46ee-9d81-5eed7ef8d307",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9bd690c7-7f3f-4627-b10b-b38ccda92c2f",
            "compositeImage": {
                "id": "aff203e2-9943-4191-8a4b-02604796700e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25e61687-90dd-46ee-9d81-5eed7ef8d307",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "351268d4-17cc-4f9c-913c-70e993d2cd15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25e61687-90dd-46ee-9d81-5eed7ef8d307",
                    "LayerId": "7257b7f3-ac1a-49e8-96d8-568d5164e5eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7257b7f3-ac1a-49e8-96d8-568d5164e5eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9bd690c7-7f3f-4627-b10b-b38ccda92c2f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}