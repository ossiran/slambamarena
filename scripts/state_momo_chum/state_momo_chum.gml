//
//
if(momomode == 1)
{
    if(state_new == 1)
    {
        with(hurtbox0)
        {
            xoffset = -12;
            yoffset = -13;
            image_xscale  = 24;
            image_yscale  = 40;
            xshift  = 0;
        }
    	
    	with(hurtbox1)
    	{
    	    xoffset = -8;
    	    yoffset = -31;
    	    image_xscale  = 16;
    	    image_yscale  = 18;
    	    xshift = 0;
    	}
    	
    	with(collisionbox)
    	{
    	    xoffset = -8;
    	    yoffset = -13;
    	    image_xscale  = 16;
    	    image_yscale  = 40;
    	    xshift = 0;
    	}
    	
    	
    	attackframe0 =  5;
    	endstate = 26;
    }
        
        
    
    if(state_timer == attackframe0)
    {
        var missle = instance_create_depth(x + 46 * image_xscale, y + 5, 1, obj_missle);
        missle.image_xscale = image_xscale;
        missle.mode = "fireum";
        missle.owner = self;
        missle.vxMax = 5;
        missle.vyMax = 6;
        missle.hits = 2;
        
        heat += 20;
        ex += 3;
        vx = -2.5 * image_xscale;
        vy = -4;
    }
        
        
    sprite_index = spr_momo_chum_fire;
        
    attack_base(endstate,1,1,0,1,0);

    if(state_timer < attackframe0)
    {
        vx = 0;
        vy = 0;
    }
}
else
{
    if(state_new == 1)
    {
        with(hurtbox0)
        {
            xoffset = -12;
            yoffset = -13;
            image_xscale  = 24;
            image_yscale  = 40;
            xshift  = 0;
        }
    	
    	with(hurtbox1)
    	{
    	    xoffset = -8;
    	    yoffset = -31;
    	    image_xscale  = 16;
    	    image_yscale  = 18;
    	    xshift = 0;
    	}
    	
    	with(collisionbox)
    	{
    	    xoffset = -8;
    	    yoffset = -13;
    	    image_xscale  = 16;
    	    image_yscale  = 40;
    	    xshift = 0;
    	}
    	
    	
    	attackframe0 =  5;
    	endstate = 26;
    }
        
    
    if(state_timer == attackframe0)
    {
        var missle = instance_create_depth(x + 46 * image_xscale, y + 5, 1, obj_missle);
        missle.image_xscale = image_xscale;
        missle.mode = "iceum";
        missle.owner = self;
        missle.vxMax = 5;
        missle.vyMax = 6;
        missle.hits = 1;
        
        heat += -20;
        ex += 3;
        vx = -2.5 * image_xscale;
        vy = -4;
    }
        
        
    sprite_index = spr_momo_chum_ice;
        
    attack_base(endstate,1,1,0,1,0);

    if(state_timer < attackframe0)
    {
        vx = 0;
        vy = 0;
    }
}