{
    "id": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lsyn_fwalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 108,
    "bbox_left": 49,
    "bbox_right": 76,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a8a79316-6c73-47ef-beb2-c2141f6ff9eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "ba1d5153-0d20-45d8-9142-c1f92663f023",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8a79316-6c73-47ef-beb2-c2141f6ff9eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63b8a276-0536-487a-9bbc-6714afc52b7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8a79316-6c73-47ef-beb2-c2141f6ff9eb",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "5eb62f05-8730-4961-8e1e-c0628144de53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "59a7feb4-6c1c-4f63-922a-452c41d84a61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5eb62f05-8730-4961-8e1e-c0628144de53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d08d07d2-5e16-46ce-86de-b8dba1865120",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5eb62f05-8730-4961-8e1e-c0628144de53",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "ec7d64c6-7795-42e3-8d40-e99cc1225aa8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "7240dac5-f8c7-4956-80a9-5c3b7feacd19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec7d64c6-7795-42e3-8d40-e99cc1225aa8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd8754df-0e61-4cdb-be4f-b8a37ee4f0ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec7d64c6-7795-42e3-8d40-e99cc1225aa8",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "7c1fdfe0-0f15-44de-8409-a98e3f1893d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "952b5120-b8a3-4bbd-82fd-a906be00c7dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c1fdfe0-0f15-44de-8409-a98e3f1893d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e946517-ebab-4e81-bd7f-8bc807291735",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c1fdfe0-0f15-44de-8409-a98e3f1893d0",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "b7180cc7-8e05-47e5-b6b1-d534ecceb4e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "55ff0559-860b-4642-95b5-b6f099d992f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7180cc7-8e05-47e5-b6b1-d534ecceb4e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cadabd84-f17b-4c21-801e-c049f7111755",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7180cc7-8e05-47e5-b6b1-d534ecceb4e2",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "aabb861b-7c23-4aca-b2a5-5bbec84b42df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "0d11d896-3e3f-4b42-b1e7-01acb429f422",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aabb861b-7c23-4aca-b2a5-5bbec84b42df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbcffbd3-baa8-44e1-875c-5bca3c2377bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aabb861b-7c23-4aca-b2a5-5bbec84b42df",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "25efd94b-1f20-41b9-acbe-4422b80fff8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "2826921a-17f8-4c34-b8ba-dcd42cda451e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25efd94b-1f20-41b9-acbe-4422b80fff8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d111b448-f656-42cb-8c09-231b70e0387b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25efd94b-1f20-41b9-acbe-4422b80fff8a",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "c19b08db-8178-44fc-962b-0bac4ff54a8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "8c8cc932-c4dd-4163-8e95-2a71b2dcdbc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c19b08db-8178-44fc-962b-0bac4ff54a8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5273290c-9c02-48c8-93d7-7f7b4e6f04a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c19b08db-8178-44fc-962b-0bac4ff54a8f",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "7ae77eb6-783a-44a1-b812-f5337446c679",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "391e8d60-1b36-440a-b3b8-e20750c84e77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ae77eb6-783a-44a1-b812-f5337446c679",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5982a7b5-a7f9-416d-b676-84f9045d5e13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ae77eb6-783a-44a1-b812-f5337446c679",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "b5b70d1a-ed8a-4ebe-a26c-31e5cdbb49bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "0f0861e7-f670-4000-bcf5-805316957d1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5b70d1a-ed8a-4ebe-a26c-31e5cdbb49bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6a54ecc-a61d-4f9f-86c7-c0e45a75a720",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5b70d1a-ed8a-4ebe-a26c-31e5cdbb49bb",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "38543653-4eff-4bcd-8ef2-0b7186a6948c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "67325c25-5e3e-4144-8e14-2346715fc717",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38543653-4eff-4bcd-8ef2-0b7186a6948c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec16bbb3-b65a-4037-82fa-718ef57d9b9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38543653-4eff-4bcd-8ef2-0b7186a6948c",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "95b878b7-f2f8-4e48-8939-f8b86f94847f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "77b5dac2-d863-4a46-9c52-23d99367ca36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95b878b7-f2f8-4e48-8939-f8b86f94847f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4731f94-087e-4e7a-b4e0-9f5074dbc8ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95b878b7-f2f8-4e48-8939-f8b86f94847f",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "3a68cb0a-7e78-4f19-82e8-0ebe08ac2ccd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "c530f2da-a462-438a-8e0f-20532dcb13d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a68cb0a-7e78-4f19-82e8-0ebe08ac2ccd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "585d17c6-da51-48fc-a2be-8862e31b02fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a68cb0a-7e78-4f19-82e8-0ebe08ac2ccd",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "a07f53a2-ae6c-46ca-bf2f-b0d6a761d4c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "a11727d7-a4cc-4f6b-8467-dede88595719",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a07f53a2-ae6c-46ca-bf2f-b0d6a761d4c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7eeb2b7-1f39-45a4-aed4-5cd4836b7f37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a07f53a2-ae6c-46ca-bf2f-b0d6a761d4c4",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "6e8d7029-6860-44fc-9234-a5d1de0205e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "06be7330-f795-4148-86a0-a10432164810",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e8d7029-6860-44fc-9234-a5d1de0205e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ba5e600-e43f-4b0b-8eb6-40a3c7885f10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e8d7029-6860-44fc-9234-a5d1de0205e8",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "55276f20-981d-4ec9-9ed7-6ebefe13fa3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "e7c1e862-d679-4b24-8467-cc3f400031c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55276f20-981d-4ec9-9ed7-6ebefe13fa3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a169604-d543-4fe3-8df0-760821d43243",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55276f20-981d-4ec9-9ed7-6ebefe13fa3b",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "909d11fb-48a3-4d9d-950e-fcf2e1eae598",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "7d83b59a-4be1-4507-8500-caf914433eef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "909d11fb-48a3-4d9d-950e-fcf2e1eae598",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99a6598a-4f64-42a4-b09d-8b1cd95d02fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "909d11fb-48a3-4d9d-950e-fcf2e1eae598",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "ed8abc44-4c14-428a-acaf-c4724a477241",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "dc62cfb5-a69c-4637-aebb-b73a27301467",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed8abc44-4c14-428a-acaf-c4724a477241",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ff6438b-afcf-4bf4-83ca-3558ecd5241f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed8abc44-4c14-428a-acaf-c4724a477241",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "7669e37a-4878-4a2a-b531-7298a6139aca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "edb91893-5f22-4378-8322-420a6e4dfa81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7669e37a-4878-4a2a-b531-7298a6139aca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c6f5be6-efc1-4ba6-895c-345ebe6800a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7669e37a-4878-4a2a-b531-7298a6139aca",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "892a7cac-1592-4b8f-9c6e-a0ba084dca42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "0bdec554-a71a-43f3-845a-8f1f9e9d90b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "892a7cac-1592-4b8f-9c6e-a0ba084dca42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaef9945-33ca-427d-b09b-d00edf7a41a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "892a7cac-1592-4b8f-9c6e-a0ba084dca42",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "091453f1-3574-44ed-93b8-61b281a7349a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "df949fed-5378-46ad-92c3-df918c521d9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "091453f1-3574-44ed-93b8-61b281a7349a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cb723d7-ff34-44cd-b7ac-6ac74bd869e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "091453f1-3574-44ed-93b8-61b281a7349a",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "a8b5790c-f3ae-488f-90d6-0da73d8a5a16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "ddf33c7f-1519-4342-bc9c-420e04057242",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8b5790c-f3ae-488f-90d6-0da73d8a5a16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a5febf5-5916-4942-8ac1-c4e2e107b35a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8b5790c-f3ae-488f-90d6-0da73d8a5a16",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "601c69bb-e40f-4cc8-b69e-7c7103e9a771",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "bec74ada-ed79-4e3f-8b57-983e4165ed36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "601c69bb-e40f-4cc8-b69e-7c7103e9a771",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9d0ffb4-4e96-4c48-a37b-7bdbe7332b23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "601c69bb-e40f-4cc8-b69e-7c7103e9a771",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "5ac50f44-eb19-4651-b205-975b020de63d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "4d6eee60-b4c4-403f-82fc-fef7738a3dbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ac50f44-eb19-4651-b205-975b020de63d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c219ef66-c225-487a-bb94-8e59af94ff9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ac50f44-eb19-4651-b205-975b020de63d",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "efb006a1-6773-4d78-8c54-b4df28227ace",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "0954fb75-b13c-43d7-9218-1b7e23f395d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efb006a1-6773-4d78-8c54-b4df28227ace",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e28cbd9-0f4b-4389-9bbb-aeda5dfddd8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efb006a1-6773-4d78-8c54-b4df28227ace",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "14bc57fd-323a-481e-9a4a-9a6718b3ac15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "31f48bce-072c-4729-9ee9-3c6df82125d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14bc57fd-323a-481e-9a4a-9a6718b3ac15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f0fbb53-2894-4ac0-91b2-336548655e04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14bc57fd-323a-481e-9a4a-9a6718b3ac15",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "f039162a-b9d7-4a77-af2c-2512a2a226bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "424b0695-81b3-40e9-aafe-aa21c4dc80ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f039162a-b9d7-4a77-af2c-2512a2a226bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad81182e-efe8-4f22-b93d-9002a8f4d10a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f039162a-b9d7-4a77-af2c-2512a2a226bd",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "c61e9984-457e-4eb4-9986-62f26fa23c83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "41130656-e1d5-404a-983c-28bb9c99bd60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c61e9984-457e-4eb4-9986-62f26fa23c83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "477f8719-b6c9-4565-863c-e4164fb3a63b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c61e9984-457e-4eb4-9986-62f26fa23c83",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "4dede730-edb2-4a1c-a1aa-1907131b4804",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "7c2eabec-a504-432b-8096-f1ed426c179d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dede730-edb2-4a1c-a1aa-1907131b4804",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95a6d2bc-f4c1-4868-8827-42d196b559f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dede730-edb2-4a1c-a1aa-1907131b4804",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "48700e24-72d3-4160-9d76-1dbd9512ee5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "9f6ce3bc-903c-4f95-858e-2472eab60c56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48700e24-72d3-4160-9d76-1dbd9512ee5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4223f405-fc73-4696-b77f-b5235bad6947",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48700e24-72d3-4160-9d76-1dbd9512ee5e",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "4905de88-295f-4d9e-acd7-ab4000ea1c85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "7fef0ef7-f7d5-4d03-a4b9-1bf7db758fa5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4905de88-295f-4d9e-acd7-ab4000ea1c85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e62bebd-33de-4249-86fb-9e21fd32de1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4905de88-295f-4d9e-acd7-ab4000ea1c85",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "ed138d35-b1da-43fd-8ba9-2175e2a9a6d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "9941ac94-03b2-44dc-a693-10cf5b300752",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed138d35-b1da-43fd-8ba9-2175e2a9a6d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4873603-b8e7-4903-bc0b-1f3e7a84a9d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed138d35-b1da-43fd-8ba9-2175e2a9a6d2",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "d1806fc6-d578-499a-855f-6d82d8de3418",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "68bfec3e-50c8-46a2-ad85-b4baea72ebe8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1806fc6-d578-499a-855f-6d82d8de3418",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b48dd1b-93f8-43ed-b829-c771b0ffac81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1806fc6-d578-499a-855f-6d82d8de3418",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "2bcb9e00-cb0e-42f3-8842-7565dd08262c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "2f3c6b69-0b33-4742-8c9b-402a6660387a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bcb9e00-cb0e-42f3-8842-7565dd08262c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6db55c6a-eb25-4569-ba9b-7bfe2b93eb1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bcb9e00-cb0e-42f3-8842-7565dd08262c",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "674ef786-10b3-4c34-8aca-5b1df38f39d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "4017a5f1-3f6b-4f09-8c64-44b0e19891ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "674ef786-10b3-4c34-8aca-5b1df38f39d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "724fc2e3-3109-4731-a183-bf77d1aad4c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "674ef786-10b3-4c34-8aca-5b1df38f39d4",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "299de185-ee6a-47d2-b523-8f2bb841c1cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "8d387413-bb85-4439-93e3-611245b51610",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "299de185-ee6a-47d2-b523-8f2bb841c1cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69a8ef8b-54ad-47fd-839d-60ad05f069bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "299de185-ee6a-47d2-b523-8f2bb841c1cd",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "fc55b17c-b77c-420f-9eea-022e84c76bb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "9a966e43-6c93-425e-88c4-454ff13acc20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc55b17c-b77c-420f-9eea-022e84c76bb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccbedba5-9d76-4b90-b0e3-cc29dc68149c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc55b17c-b77c-420f-9eea-022e84c76bb5",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "5ab1ec37-c473-42b9-8dd0-efe526b84a87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "2689e801-6829-4282-a11b-6de60db3072f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ab1ec37-c473-42b9-8dd0-efe526b84a87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a88739da-9e4f-4314-9077-2773e1a7723c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ab1ec37-c473-42b9-8dd0-efe526b84a87",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "1a197b04-2105-4c67-b57b-8a110e6aab47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "753d0248-9fe8-4fc4-8a2c-98580343656b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a197b04-2105-4c67-b57b-8a110e6aab47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5c1d8d1-6fce-4b52-b267-54ff16148a7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a197b04-2105-4c67-b57b-8a110e6aab47",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        },
        {
            "id": "c320c37c-8c1c-4d7b-aa62-0f74472a5d9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "compositeImage": {
                "id": "5e1033e1-287c-47a3-8550-d67be7375304",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c320c37c-8c1c-4d7b-aa62-0f74472a5d9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9e609d0-1e21-44c6-b076-9d769b2ab536",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c320c37c-8c1c-4d7b-aa62-0f74472a5d9d",
                    "LayerId": "97a1b99f-6e36-4170-a85f-5070077a2516"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "97a1b99f-6e36-4170-a85f-5070077a2516",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e2265c6b-b8df-4bce-8f0d-c35d26c26cb8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}