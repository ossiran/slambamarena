live_call();

if(state_new == 1)
{
	
	with(hurtbox0)
	{
	    xoffset = 0;
	    yoffset = -1000;
	    image_xscale  = 0;
	    image_yscale  = 0;
	    xshift  = 0;
	}
	
	with(hurtbox1)
	{
	    xoffset = 0;
	    yoffset = -1000;
	    image_xscale  = 0;
	    image_yscale  = 0;
	    xshift = 0;
	}
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -13;
	    image_xscale  = 16;
	    image_yscale  = 40;
	    xshift = 0;
	}
	

    dtap_right = 0;
    dtap_left = 0;
    
	//sprite_index = spr_lsyn_bdash;
	image_index = 1;
}

if(image_xscale == -1 && state_timer > 1 && state_timer < 8)
{
    vx = Approach(vx, 6, 4);
}

if(image_xscale == 1  && state_timer > 1 && state_timer < 8)
{
    vx = Approach(vx, -6, 4);
}

if(state_timer > 9)
{
     vx = Approach(vx, 0, 2);
    
    with(hurtbox0)
	{
	    xoffset = -12;
	    yoffset = -13;
	    image_xscale  = 24;
	    image_yscale  = 40;
	    xshift  = other.image_xscale * 3;
	}
	
	with(hurtbox1)
	{
	    xoffset = -8;
	    yoffset = -31;
	    image_xscale  = 16;
	    image_yscale  = 18;
	    xshift = other.image_xscale * 3;
	}
}

if(synmode > 0)
{
    sprite_index = spr_lsyn_bwalk;
}
else if(synmode < 0)
{
    sprite_index = spr_dsyn_bwalk;
}

//air
if(!onground())
{
	state_switch("air");	
}

if(state_timer == 15)
{
    state_switch("stand");
}