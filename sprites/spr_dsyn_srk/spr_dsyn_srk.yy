{
    "id": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dsyn_srk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 49,
    "bbox_right": 86,
    "bbox_top": 38,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "098fe9ab-6631-4e7f-9491-7b34506de13d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "573b0876-c93c-4c36-8eb9-2f6d64388635",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "098fe9ab-6631-4e7f-9491-7b34506de13d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dff91312-20b9-49b1-b4dc-30b5ea28efd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "098fe9ab-6631-4e7f-9491-7b34506de13d",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "06c16d98-b9b6-4e46-92f2-2588a21bcaa1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "1bac8ea6-a3d1-43a2-ac20-2780cf639e89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06c16d98-b9b6-4e46-92f2-2588a21bcaa1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "378eaad5-3a4e-4bf2-96b1-f1b7ffb067fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06c16d98-b9b6-4e46-92f2-2588a21bcaa1",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "a07620a2-2525-488f-ac29-8380fec0e8f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "e2186161-c4f6-49fc-87da-23a7753daae3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a07620a2-2525-488f-ac29-8380fec0e8f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8537bbc-ad7c-4929-941d-ef1a32e7fab7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a07620a2-2525-488f-ac29-8380fec0e8f3",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "a226307f-e45f-47da-ad4e-b87916fce9e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "187b6f98-dfb5-49fe-91cd-6f071d5c7c13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a226307f-e45f-47da-ad4e-b87916fce9e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c875dd34-fc3c-4e32-84bc-02aee1198b9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a226307f-e45f-47da-ad4e-b87916fce9e1",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "ee0c271b-4b38-4b71-96b6-888b2c6f51ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "9247f0ee-1138-4c2f-a856-0732373edde1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee0c271b-4b38-4b71-96b6-888b2c6f51ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad6bb8b6-fdc3-40e1-b947-0c0ac0f94661",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee0c271b-4b38-4b71-96b6-888b2c6f51ff",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "f0c19939-f845-4529-bf22-3e2e86f40ee9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "d31b197b-236e-42ee-aee6-771cd5a92980",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0c19939-f845-4529-bf22-3e2e86f40ee9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "214a3615-2275-4bc5-8ebe-3fb686eec4d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0c19939-f845-4529-bf22-3e2e86f40ee9",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "f4a06915-f2fd-4241-b522-969b377ff836",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "ee9a1cb1-2b4b-4669-9542-c62b98fd04cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4a06915-f2fd-4241-b522-969b377ff836",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a7ea169-0946-420f-b31e-b5dbf9b4014a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4a06915-f2fd-4241-b522-969b377ff836",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "97148c88-a8fd-4d5f-b6f0-247d9d8a4f8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "976a9f86-9e19-4d2c-9c92-65c2c6139949",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97148c88-a8fd-4d5f-b6f0-247d9d8a4f8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9673db1-093f-46dd-9a71-efdf62471e29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97148c88-a8fd-4d5f-b6f0-247d9d8a4f8a",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "094da193-e662-4fb0-8dcb-e49eb7ef46f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "7de8b03a-90a1-431e-b9b2-09a357af67e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "094da193-e662-4fb0-8dcb-e49eb7ef46f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cfaad07-9db5-4e71-bd05-cb0c39cc1a2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "094da193-e662-4fb0-8dcb-e49eb7ef46f5",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "d22543a3-00e5-4724-917a-660fc32e06c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "c856d639-2451-443a-b1d4-c60bd3f3c1a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d22543a3-00e5-4724-917a-660fc32e06c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01d4cef4-ac58-44ae-92d0-77235eedc974",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d22543a3-00e5-4724-917a-660fc32e06c1",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "b3ab8a2e-849f-4148-964e-0178f7f33773",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "77be9c67-f685-4860-9b8f-bd8c8bc7269f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3ab8a2e-849f-4148-964e-0178f7f33773",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58f13f71-af67-4d14-b9fc-b7b70f368d21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3ab8a2e-849f-4148-964e-0178f7f33773",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "ecc3371c-5f1d-4cda-8cb5-fdad267f6e85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "87c8db7c-214c-48e5-b333-03fd73a0148c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecc3371c-5f1d-4cda-8cb5-fdad267f6e85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09069dc0-3c6b-49f2-9a5f-2751114bea52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecc3371c-5f1d-4cda-8cb5-fdad267f6e85",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "dd48de66-0447-4771-8aff-acde2cdbbb97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "1d9fe613-42be-43b7-8e8b-1ad0d295d48f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd48de66-0447-4771-8aff-acde2cdbbb97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93c2511a-e1f3-44cd-84fd-454d633daa26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd48de66-0447-4771-8aff-acde2cdbbb97",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "ee3c0b51-0e3e-4942-a930-8ddf1e0a44da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "936a2239-ba77-4756-8ae9-41a4a24cd222",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee3c0b51-0e3e-4942-a930-8ddf1e0a44da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1dc81ef-b79f-4086-9e27-119c1616892d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee3c0b51-0e3e-4942-a930-8ddf1e0a44da",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "2e66694b-e532-4045-a612-0cbe31649a97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "80280963-0aed-4707-a236-30f13c7db959",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e66694b-e532-4045-a612-0cbe31649a97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2efbdc5-5e58-4722-b401-34a02d486d3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e66694b-e532-4045-a612-0cbe31649a97",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "61a7f38f-2dc4-4af5-b760-65610b1d1849",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "4190d10d-55a6-49e1-a8dc-0a5af761230d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61a7f38f-2dc4-4af5-b760-65610b1d1849",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c291817-c825-48f7-8fab-853cc8340d27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61a7f38f-2dc4-4af5-b760-65610b1d1849",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "a4a5f1c1-7576-4e35-a1c2-6d87b10c33cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "07906f4f-7876-425d-aff3-d3ddadc94909",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4a5f1c1-7576-4e35-a1c2-6d87b10c33cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9f9ac09-40ba-481d-8326-df998109d833",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4a5f1c1-7576-4e35-a1c2-6d87b10c33cc",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "83584612-9bbb-4ce8-91ab-61a84241b4f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "f8ee6794-fd0c-4cf6-9157-03f67cfd118e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83584612-9bbb-4ce8-91ab-61a84241b4f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28515279-2023-474f-b098-6d4ffd03f562",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83584612-9bbb-4ce8-91ab-61a84241b4f9",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "c64d4c29-134b-4f4c-bd42-3f861afc8580",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "370e86a3-bdca-421e-9e37-6819f80884ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c64d4c29-134b-4f4c-bd42-3f861afc8580",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a442e85-c75b-4252-a137-1c68fb916d00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c64d4c29-134b-4f4c-bd42-3f861afc8580",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "aefdd883-1765-46f1-982b-873efdfecc52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "aeec2578-d571-4a21-b583-4682586c7953",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aefdd883-1765-46f1-982b-873efdfecc52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2044ee6-8c56-43fe-bc1b-9ba19c091c3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aefdd883-1765-46f1-982b-873efdfecc52",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "bf6d97b5-e241-4ddc-8532-cbfb439ef2f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "5eedb193-b2dc-42f9-af48-2c8e3d0278d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf6d97b5-e241-4ddc-8532-cbfb439ef2f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73cba4d4-0749-40bb-903d-6241c153f36c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf6d97b5-e241-4ddc-8532-cbfb439ef2f4",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "e4e9bccd-dfb4-4558-8d5b-28de0d13268b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "54fb7ac9-3c14-45ff-8578-5e169cd31cfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4e9bccd-dfb4-4558-8d5b-28de0d13268b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7f0202e-c158-41aa-85db-1d55e94638d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4e9bccd-dfb4-4558-8d5b-28de0d13268b",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "2a10bf8e-64e2-4edb-b5fc-8100a833d3b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "79a3fd72-ee9b-4ad2-afe9-91627d68a6f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a10bf8e-64e2-4edb-b5fc-8100a833d3b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ba579ed-5ac6-4eca-9516-7edbbf389560",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a10bf8e-64e2-4edb-b5fc-8100a833d3b5",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "567d70e1-a1e8-4c73-9c13-ce2f472abbc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "777ab736-eac0-4eff-85e7-c86363893a22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "567d70e1-a1e8-4c73-9c13-ce2f472abbc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "047c9d60-7581-413d-8925-37078c35df66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "567d70e1-a1e8-4c73-9c13-ce2f472abbc6",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "15089f0c-0d42-4e39-bb70-364828f191ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "6b389042-48d9-4b4a-a626-8059da9a1caa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15089f0c-0d42-4e39-bb70-364828f191ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "038ba94a-6d89-4e3c-a64c-fc8d3ecc811d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15089f0c-0d42-4e39-bb70-364828f191ef",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "f3849c87-db1a-4866-9f77-150077b00b3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "c9b8ca74-3008-4fd0-8f17-8aa0f3733199",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3849c87-db1a-4866-9f77-150077b00b3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f356f0fb-e769-4816-b4f0-298b80414f31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3849c87-db1a-4866-9f77-150077b00b3e",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "90616f83-71c0-454c-8081-8d55dc7312d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "89a184f6-0c2c-4bc7-bfd2-1518184f929a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90616f83-71c0-454c-8081-8d55dc7312d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e0645c3-f966-497b-9f71-7b061213be95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90616f83-71c0-454c-8081-8d55dc7312d2",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "ee75de24-4f6e-49c7-a6ff-8782b1624ff9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "48343235-f18d-4ded-a7a9-0adfd1489bc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee75de24-4f6e-49c7-a6ff-8782b1624ff9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3cfb1a0-d568-46b8-b121-0e080a66af7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee75de24-4f6e-49c7-a6ff-8782b1624ff9",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "a23be13b-292f-419d-89c4-9ee43def23b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "ba6cfb4d-99ce-451a-a174-fade0ee90497",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a23be13b-292f-419d-89c4-9ee43def23b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce6e7246-ffa5-49e4-9d0d-3d48f1993aed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a23be13b-292f-419d-89c4-9ee43def23b6",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "7b631eb7-e5dd-416f-8c66-0b74ee7902b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "202aa562-9037-4a51-b775-8fbe7547941b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b631eb7-e5dd-416f-8c66-0b74ee7902b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e6f4c77-40c2-4238-b608-0ab9ce4ade74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b631eb7-e5dd-416f-8c66-0b74ee7902b2",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "1df70f1f-7e48-46d6-905c-c808c83b4de6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "bed51c8b-d32d-47f0-ba6e-df0292ec92f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1df70f1f-7e48-46d6-905c-c808c83b4de6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2031edd9-ac74-4f59-965d-24bd8cb7d9cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1df70f1f-7e48-46d6-905c-c808c83b4de6",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "1378c266-6970-421f-b23f-70fb401da407",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "a9e662b6-e291-40ff-aaf6-1e107e6b4743",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1378c266-6970-421f-b23f-70fb401da407",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8230ceab-e134-4f05-86f7-8aa4dfe5f946",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1378c266-6970-421f-b23f-70fb401da407",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "8ee1d5d5-f2d6-49f5-b22b-5bfc64fe1354",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "8aff2188-08b9-4602-a8e9-9b7329403baf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ee1d5d5-f2d6-49f5-b22b-5bfc64fe1354",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4433608-4cee-41da-b49c-ef7069f1cfa5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ee1d5d5-f2d6-49f5-b22b-5bfc64fe1354",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "044ae449-c9c2-4651-8157-e3d015a020cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "71034db2-b5c1-4885-b0f4-6ab14b131f42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "044ae449-c9c2-4651-8157-e3d015a020cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b13772b-b4e4-45e2-a339-f4de9385809b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "044ae449-c9c2-4651-8157-e3d015a020cf",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "ecc46d20-f952-46fd-80d9-6eba2f880e9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "9784dd5d-cc2c-4d0c-8afa-4c00e7a62c77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecc46d20-f952-46fd-80d9-6eba2f880e9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68106ed3-8c3b-4439-8692-782350e1bc9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecc46d20-f952-46fd-80d9-6eba2f880e9f",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        },
        {
            "id": "40e128b4-9823-4ecc-ba42-41e461acb771",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "compositeImage": {
                "id": "979fb6f9-7df4-4562-a5ae-70909179e017",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40e128b4-9823-4ecc-ba42-41e461acb771",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6c253c2-d236-4261-843d-b2a7038def12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40e128b4-9823-4ecc-ba42-41e461acb771",
                    "LayerId": "d9366cd9-b5be-4baf-8bb0-93447170e57d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d9366cd9-b5be-4baf-8bb0-93447170e57d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a9d0e91-f48b-4937-9985-a5ca71eedb9c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 76
}