{
    "id": "3564c6ec-4860-4519-992d-b8f4439fbacd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_2m_ice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 16,
    "bbox_right": 166,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a1acd218-3dd2-4162-86d7-0eeac38ce8e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3564c6ec-4860-4519-992d-b8f4439fbacd",
            "compositeImage": {
                "id": "e32a86aa-7d18-43c7-ab34-283b561ac2e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1acd218-3dd2-4162-86d7-0eeac38ce8e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6267794-0e93-4d9d-82be-9ab9a4158a03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1acd218-3dd2-4162-86d7-0eeac38ce8e3",
                    "LayerId": "14578189-185f-40fd-a475-6e5cc9ab929e"
                }
            ]
        },
        {
            "id": "39c24e1f-4736-4d9f-8f13-c6857da7211e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3564c6ec-4860-4519-992d-b8f4439fbacd",
            "compositeImage": {
                "id": "bb7fb5d7-d0f3-46be-8b09-d2b38fc9fb9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39c24e1f-4736-4d9f-8f13-c6857da7211e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "830ed047-ebd7-4bb5-917a-f78eb926ac45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39c24e1f-4736-4d9f-8f13-c6857da7211e",
                    "LayerId": "14578189-185f-40fd-a475-6e5cc9ab929e"
                }
            ]
        },
        {
            "id": "e109a91e-b609-4a15-a10f-51dc93bee9d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3564c6ec-4860-4519-992d-b8f4439fbacd",
            "compositeImage": {
                "id": "aea55524-1673-46bc-a9de-ba8e52bfeccc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e109a91e-b609-4a15-a10f-51dc93bee9d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ee4a4cd-efe3-44b1-b63f-307a49d43de9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e109a91e-b609-4a15-a10f-51dc93bee9d3",
                    "LayerId": "14578189-185f-40fd-a475-6e5cc9ab929e"
                }
            ]
        },
        {
            "id": "9afbab4f-211f-43a2-a6ad-016b3ab309c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3564c6ec-4860-4519-992d-b8f4439fbacd",
            "compositeImage": {
                "id": "ce5e057b-d77f-4979-8123-07d31f8d9248",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9afbab4f-211f-43a2-a6ad-016b3ab309c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11d1a0ea-2111-42a4-a62c-77c01cc1757f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9afbab4f-211f-43a2-a6ad-016b3ab309c6",
                    "LayerId": "14578189-185f-40fd-a475-6e5cc9ab929e"
                }
            ]
        },
        {
            "id": "9de29167-5f74-4ea4-8986-5c1b73a9b6bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3564c6ec-4860-4519-992d-b8f4439fbacd",
            "compositeImage": {
                "id": "ce174f99-3b62-421e-b30b-21489822bbc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9de29167-5f74-4ea4-8986-5c1b73a9b6bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6b5f8a3-9734-480b-905a-3bf14760a8f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9de29167-5f74-4ea4-8986-5c1b73a9b6bc",
                    "LayerId": "14578189-185f-40fd-a475-6e5cc9ab929e"
                }
            ]
        },
        {
            "id": "d49dea4a-d85c-4f6c-b7b2-ec386ecefd05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3564c6ec-4860-4519-992d-b8f4439fbacd",
            "compositeImage": {
                "id": "a4885ed0-32b4-432d-9e2d-dd54eb4dbf65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d49dea4a-d85c-4f6c-b7b2-ec386ecefd05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71d78759-ca07-4fbc-9ad5-6c96cad95b44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d49dea4a-d85c-4f6c-b7b2-ec386ecefd05",
                    "LayerId": "14578189-185f-40fd-a475-6e5cc9ab929e"
                }
            ]
        },
        {
            "id": "3cfcb33d-744f-4fa6-9dcf-fe3fc27e7c27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3564c6ec-4860-4519-992d-b8f4439fbacd",
            "compositeImage": {
                "id": "aa5f13fe-6a20-4b12-9264-f71ee8e4cf77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cfcb33d-744f-4fa6-9dcf-fe3fc27e7c27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfebd8aa-d9e7-4e67-a78f-18b5e23a42f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cfcb33d-744f-4fa6-9dcf-fe3fc27e7c27",
                    "LayerId": "14578189-185f-40fd-a475-6e5cc9ab929e"
                }
            ]
        },
        {
            "id": "b69ce07e-d9c9-451f-b895-6d8c3a1a3e48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3564c6ec-4860-4519-992d-b8f4439fbacd",
            "compositeImage": {
                "id": "143ad1c5-07ac-403e-81c7-116503099412",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b69ce07e-d9c9-451f-b895-6d8c3a1a3e48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca7c8adc-9f85-4603-99aa-c5e7a8d7ca63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b69ce07e-d9c9-451f-b895-6d8c3a1a3e48",
                    "LayerId": "14578189-185f-40fd-a475-6e5cc9ab929e"
                }
            ]
        },
        {
            "id": "b6b30f4c-aa49-4f35-b2d0-4fe38fff87d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3564c6ec-4860-4519-992d-b8f4439fbacd",
            "compositeImage": {
                "id": "807b3d60-d355-4080-b4e7-e36c183deca2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6b30f4c-aa49-4f35-b2d0-4fe38fff87d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "553ec0ef-6345-4a9e-bf0c-2924da25d3df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6b30f4c-aa49-4f35-b2d0-4fe38fff87d6",
                    "LayerId": "14578189-185f-40fd-a475-6e5cc9ab929e"
                }
            ]
        },
        {
            "id": "7a2dc3f6-8c9c-45a0-9b65-d37e6a835116",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3564c6ec-4860-4519-992d-b8f4439fbacd",
            "compositeImage": {
                "id": "b8c97943-442d-4db1-b8c3-3aa28c6789f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a2dc3f6-8c9c-45a0-9b65-d37e6a835116",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63efee25-717c-45af-8d43-7f9a64f9fc2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a2dc3f6-8c9c-45a0-9b65-d37e6a835116",
                    "LayerId": "14578189-185f-40fd-a475-6e5cc9ab929e"
                }
            ]
        },
        {
            "id": "ab637ac4-052a-44e5-9bb9-1a03e3b8d86e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3564c6ec-4860-4519-992d-b8f4439fbacd",
            "compositeImage": {
                "id": "9ff6898f-8fa2-4874-b3bc-3f9b3dae79fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab637ac4-052a-44e5-9bb9-1a03e3b8d86e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "228eb0e5-60b3-4597-8772-07b117eb205e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab637ac4-052a-44e5-9bb9-1a03e3b8d86e",
                    "LayerId": "14578189-185f-40fd-a475-6e5cc9ab929e"
                }
            ]
        },
        {
            "id": "03035d77-8243-4d89-a41c-9add0f3b0a11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3564c6ec-4860-4519-992d-b8f4439fbacd",
            "compositeImage": {
                "id": "bfa682ba-8ccb-43aa-9ebd-a2ad004672f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03035d77-8243-4d89-a41c-9add0f3b0a11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98fc10f5-a1dd-4993-98fa-0d1a7013466f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03035d77-8243-4d89-a41c-9add0f3b0a11",
                    "LayerId": "14578189-185f-40fd-a475-6e5cc9ab929e"
                }
            ]
        },
        {
            "id": "64575714-b59e-437f-83c9-c444a9b353ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3564c6ec-4860-4519-992d-b8f4439fbacd",
            "compositeImage": {
                "id": "477baa7c-6565-419f-8ff5-36e29e02b21e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64575714-b59e-437f-83c9-c444a9b353ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95c32fe7-291f-4377-a2c1-bd1bdd64de43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64575714-b59e-437f-83c9-c444a9b353ad",
                    "LayerId": "14578189-185f-40fd-a475-6e5cc9ab929e"
                }
            ]
        },
        {
            "id": "2f13e499-bde2-4d4a-adfb-ee64e78c5e42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3564c6ec-4860-4519-992d-b8f4439fbacd",
            "compositeImage": {
                "id": "368049e7-5a71-4562-b2f1-aeca4f5ba540",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f13e499-bde2-4d4a-adfb-ee64e78c5e42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac4f6683-513e-468a-a102-553f3206ac71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f13e499-bde2-4d4a-adfb-ee64e78c5e42",
                    "LayerId": "14578189-185f-40fd-a475-6e5cc9ab929e"
                }
            ]
        },
        {
            "id": "0416bc66-6f01-47f8-a298-0617e218d502",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3564c6ec-4860-4519-992d-b8f4439fbacd",
            "compositeImage": {
                "id": "bd9e5b6c-ffd0-4c3e-9ee8-3baf81c22f04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0416bc66-6f01-47f8-a298-0617e218d502",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ad90ce1-5581-45e9-8d26-e8560766637c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0416bc66-6f01-47f8-a298-0617e218d502",
                    "LayerId": "14578189-185f-40fd-a475-6e5cc9ab929e"
                }
            ]
        },
        {
            "id": "a0311cf2-fa77-4e7d-9e80-94363034c319",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3564c6ec-4860-4519-992d-b8f4439fbacd",
            "compositeImage": {
                "id": "e07e8478-2aeb-42a1-85f3-4fa7473178f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0311cf2-fa77-4e7d-9e80-94363034c319",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70e6a910-c72b-4b3b-aa81-8d24931ea628",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0311cf2-fa77-4e7d-9e80-94363034c319",
                    "LayerId": "14578189-185f-40fd-a475-6e5cc9ab929e"
                }
            ]
        },
        {
            "id": "80a422dc-9ce4-4fad-b25d-470410975f43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3564c6ec-4860-4519-992d-b8f4439fbacd",
            "compositeImage": {
                "id": "9bf3eaf6-978d-4323-b7fc-51909a178917",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80a422dc-9ce4-4fad-b25d-470410975f43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24171c35-37b1-4826-b277-99f61ea15947",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80a422dc-9ce4-4fad-b25d-470410975f43",
                    "LayerId": "14578189-185f-40fd-a475-6e5cc9ab929e"
                }
            ]
        },
        {
            "id": "6ef8d6ec-6d27-431c-b783-66a8af4a0907",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3564c6ec-4860-4519-992d-b8f4439fbacd",
            "compositeImage": {
                "id": "2a7e3a20-c82e-4a76-8720-0c2195e3e767",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ef8d6ec-6d27-431c-b783-66a8af4a0907",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0367019-0c48-492d-87e7-e0d4387d4d2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ef8d6ec-6d27-431c-b783-66a8af4a0907",
                    "LayerId": "14578189-185f-40fd-a475-6e5cc9ab929e"
                }
            ]
        },
        {
            "id": "61743d8b-d1db-4acc-a1c3-3e557f5894d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3564c6ec-4860-4519-992d-b8f4439fbacd",
            "compositeImage": {
                "id": "6dbfc0a1-f6ef-420f-8382-c18ecbd4158b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61743d8b-d1db-4acc-a1c3-3e557f5894d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8d60bae-5448-4688-8eb8-7a19fb6675d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61743d8b-d1db-4acc-a1c3-3e557f5894d6",
                    "LayerId": "14578189-185f-40fd-a475-6e5cc9ab929e"
                }
            ]
        },
        {
            "id": "1dc0d283-9e3c-4d60-92c6-41cec5be4ea7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3564c6ec-4860-4519-992d-b8f4439fbacd",
            "compositeImage": {
                "id": "68c412f7-28da-44b8-b685-06c7b040a6f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dc0d283-9e3c-4d60-92c6-41cec5be4ea7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45a5c577-1c74-46a2-ac13-dcb52643998c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dc0d283-9e3c-4d60-92c6-41cec5be4ea7",
                    "LayerId": "14578189-185f-40fd-a475-6e5cc9ab929e"
                }
            ]
        },
        {
            "id": "701cde83-837c-4c95-be04-a84a085df275",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3564c6ec-4860-4519-992d-b8f4439fbacd",
            "compositeImage": {
                "id": "1a4f4b33-7a14-4eab-bfab-06a29df26c7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "701cde83-837c-4c95-be04-a84a085df275",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "236fb760-61f6-4fde-8b5b-8ba55000ef6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "701cde83-837c-4c95-be04-a84a085df275",
                    "LayerId": "14578189-185f-40fd-a475-6e5cc9ab929e"
                }
            ]
        },
        {
            "id": "c8d5c41d-8140-4aca-880f-7a987aa5937f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3564c6ec-4860-4519-992d-b8f4439fbacd",
            "compositeImage": {
                "id": "da80fdbc-cffb-4c85-9feb-a4e341a08865",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8d5c41d-8140-4aca-880f-7a987aa5937f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95274f29-b630-4ad8-b4fd-712f967ea018",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8d5c41d-8140-4aca-880f-7a987aa5937f",
                    "LayerId": "14578189-185f-40fd-a475-6e5cc9ab929e"
                }
            ]
        },
        {
            "id": "2328e581-ba74-49d7-b279-c4bbec96e0c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3564c6ec-4860-4519-992d-b8f4439fbacd",
            "compositeImage": {
                "id": "00b1595c-14cd-4696-90d3-21f91c1afb8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2328e581-ba74-49d7-b279-c4bbec96e0c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0328698b-c475-43ff-94ff-7187c700af0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2328e581-ba74-49d7-b279-c4bbec96e0c0",
                    "LayerId": "14578189-185f-40fd-a475-6e5cc9ab929e"
                }
            ]
        },
        {
            "id": "eb3fb0bd-5d55-483b-bdc5-8825f0f5c5c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3564c6ec-4860-4519-992d-b8f4439fbacd",
            "compositeImage": {
                "id": "a2e008c5-e061-4d11-9483-982c5a762559",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb3fb0bd-5d55-483b-bdc5-8825f0f5c5c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acfe2f09-c0de-48ba-bc24-fd1cd2945db2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb3fb0bd-5d55-483b-bdc5-8825f0f5c5c6",
                    "LayerId": "14578189-185f-40fd-a475-6e5cc9ab929e"
                }
            ]
        },
        {
            "id": "6eeda721-15cf-4c7b-9e39-554a4cfe8beb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3564c6ec-4860-4519-992d-b8f4439fbacd",
            "compositeImage": {
                "id": "8d8e6fa1-cb3d-4a0a-8b6a-a89941276b63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6eeda721-15cf-4c7b-9e39-554a4cfe8beb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e56a02c1-5a06-4fb0-ba69-abfa0be49c37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6eeda721-15cf-4c7b-9e39-554a4cfe8beb",
                    "LayerId": "14578189-185f-40fd-a475-6e5cc9ab929e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "14578189-185f-40fd-a475-6e5cc9ab929e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3564c6ec-4860-4519-992d-b8f4439fbacd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 180,
    "xorig": 63,
    "yorig": 87
}