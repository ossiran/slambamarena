{
    "id": "ffde1fbe-11c4-4fc6-9d89-3f65c189449f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dsyn_5m",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 45,
    "bbox_right": 103,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a630f618-30bb-4cec-9d17-e6c9ed830534",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffde1fbe-11c4-4fc6-9d89-3f65c189449f",
            "compositeImage": {
                "id": "3e2aaf37-8ccd-4061-9d82-31aa6b9270ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a630f618-30bb-4cec-9d17-e6c9ed830534",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e9a0bce-2eb9-4b02-a601-8acb8f3ab545",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a630f618-30bb-4cec-9d17-e6c9ed830534",
                    "LayerId": "6b336890-2d77-486d-95ab-68756e9696da"
                }
            ]
        },
        {
            "id": "0abc5687-a368-4f8c-b267-e8c09ba7ad35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffde1fbe-11c4-4fc6-9d89-3f65c189449f",
            "compositeImage": {
                "id": "63bc777f-cb07-467c-b1f4-cabf1113d9dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0abc5687-a368-4f8c-b267-e8c09ba7ad35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dac9b71-4c13-4d7a-adef-a941d7655571",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0abc5687-a368-4f8c-b267-e8c09ba7ad35",
                    "LayerId": "6b336890-2d77-486d-95ab-68756e9696da"
                }
            ]
        },
        {
            "id": "df3eb20d-6f16-40be-8ffb-51f6a7c6b864",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffde1fbe-11c4-4fc6-9d89-3f65c189449f",
            "compositeImage": {
                "id": "71269464-df3e-48d6-b8c6-8cfbbc5113a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df3eb20d-6f16-40be-8ffb-51f6a7c6b864",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad1878f8-eecf-451b-94e1-ce7cf5725a71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df3eb20d-6f16-40be-8ffb-51f6a7c6b864",
                    "LayerId": "6b336890-2d77-486d-95ab-68756e9696da"
                }
            ]
        },
        {
            "id": "be0996ca-7356-4ca1-8e35-9d7d071c7929",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffde1fbe-11c4-4fc6-9d89-3f65c189449f",
            "compositeImage": {
                "id": "f1d2e1b2-82e3-45a4-9358-cd36532d2cc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be0996ca-7356-4ca1-8e35-9d7d071c7929",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6db46a25-c70e-41ec-af61-b485eba6d590",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be0996ca-7356-4ca1-8e35-9d7d071c7929",
                    "LayerId": "6b336890-2d77-486d-95ab-68756e9696da"
                }
            ]
        },
        {
            "id": "c742dcfc-b954-4408-896f-94b0b2033948",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffde1fbe-11c4-4fc6-9d89-3f65c189449f",
            "compositeImage": {
                "id": "a7c18244-1a68-4173-8191-ed5bf7ff859c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c742dcfc-b954-4408-896f-94b0b2033948",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba516df7-94eb-4afb-bbd4-ba024d774521",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c742dcfc-b954-4408-896f-94b0b2033948",
                    "LayerId": "6b336890-2d77-486d-95ab-68756e9696da"
                }
            ]
        },
        {
            "id": "22015a76-9d6d-43e0-a102-dd2f37606a7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffde1fbe-11c4-4fc6-9d89-3f65c189449f",
            "compositeImage": {
                "id": "2515dbc2-7ffa-4f64-93b4-431389c51bfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22015a76-9d6d-43e0-a102-dd2f37606a7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05ec8c67-9b04-43c1-85e0-58be01afaf3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22015a76-9d6d-43e0-a102-dd2f37606a7d",
                    "LayerId": "6b336890-2d77-486d-95ab-68756e9696da"
                }
            ]
        },
        {
            "id": "085a023f-e097-4d63-8ef4-25cb5b271c8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffde1fbe-11c4-4fc6-9d89-3f65c189449f",
            "compositeImage": {
                "id": "845d7d75-c542-45b1-8812-c41712039fca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "085a023f-e097-4d63-8ef4-25cb5b271c8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f372f783-212b-4c5c-92f7-8463e0860d13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "085a023f-e097-4d63-8ef4-25cb5b271c8e",
                    "LayerId": "6b336890-2d77-486d-95ab-68756e9696da"
                }
            ]
        },
        {
            "id": "561d5d55-2a2c-49f3-b28a-4eaf80c7d6da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffde1fbe-11c4-4fc6-9d89-3f65c189449f",
            "compositeImage": {
                "id": "178e052d-c1ef-4a09-a58c-ce03e0cf98a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "561d5d55-2a2c-49f3-b28a-4eaf80c7d6da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbe7ba9b-2eb7-472a-bd02-3f6905d377fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "561d5d55-2a2c-49f3-b28a-4eaf80c7d6da",
                    "LayerId": "6b336890-2d77-486d-95ab-68756e9696da"
                }
            ]
        },
        {
            "id": "9f73643d-c136-4bd8-98e1-41b75cf84942",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffde1fbe-11c4-4fc6-9d89-3f65c189449f",
            "compositeImage": {
                "id": "258ad846-ff47-4772-85e0-e7222662065c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f73643d-c136-4bd8-98e1-41b75cf84942",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ec99771-ebe7-48ab-9837-46477361e684",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f73643d-c136-4bd8-98e1-41b75cf84942",
                    "LayerId": "6b336890-2d77-486d-95ab-68756e9696da"
                }
            ]
        },
        {
            "id": "cbbcde66-78a4-4265-8064-effc9a6202ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffde1fbe-11c4-4fc6-9d89-3f65c189449f",
            "compositeImage": {
                "id": "75e1bb4c-1f00-443f-85a3-15d5a56f0d60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbbcde66-78a4-4265-8064-effc9a6202ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69505044-d58c-476b-974b-f668d5497e72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbbcde66-78a4-4265-8064-effc9a6202ed",
                    "LayerId": "6b336890-2d77-486d-95ab-68756e9696da"
                }
            ]
        },
        {
            "id": "501eaf00-4b5e-4414-9cc6-b0b3f6c90cfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffde1fbe-11c4-4fc6-9d89-3f65c189449f",
            "compositeImage": {
                "id": "05917003-a6e5-4b6c-9272-85305f85d77b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "501eaf00-4b5e-4414-9cc6-b0b3f6c90cfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b2a78e1-83a1-4ffe-9c51-54a5674b6fc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "501eaf00-4b5e-4414-9cc6-b0b3f6c90cfb",
                    "LayerId": "6b336890-2d77-486d-95ab-68756e9696da"
                }
            ]
        },
        {
            "id": "8e9d0135-54e1-4250-b3a6-fb92fcde007a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffde1fbe-11c4-4fc6-9d89-3f65c189449f",
            "compositeImage": {
                "id": "4e95d9ab-b67c-4851-be75-ed5642e5e056",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e9d0135-54e1-4250-b3a6-fb92fcde007a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe253a0f-09cb-45f9-9ca4-119d052987e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e9d0135-54e1-4250-b3a6-fb92fcde007a",
                    "LayerId": "6b336890-2d77-486d-95ab-68756e9696da"
                }
            ]
        },
        {
            "id": "60b765c3-e39f-43ea-8bd5-dbe046eab6d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffde1fbe-11c4-4fc6-9d89-3f65c189449f",
            "compositeImage": {
                "id": "867f5dcd-0844-4253-b200-1e5bea756aa3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60b765c3-e39f-43ea-8bd5-dbe046eab6d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1bfa295-6126-4e6b-ac76-7060f1f68757",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60b765c3-e39f-43ea-8bd5-dbe046eab6d5",
                    "LayerId": "6b336890-2d77-486d-95ab-68756e9696da"
                }
            ]
        },
        {
            "id": "b0d6a8ef-fcef-4692-84a0-33c05bb2a065",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffde1fbe-11c4-4fc6-9d89-3f65c189449f",
            "compositeImage": {
                "id": "993b0099-bd34-4233-8c2d-d9e35c805e22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0d6a8ef-fcef-4692-84a0-33c05bb2a065",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8eb8182c-809f-45fb-8c80-5a1caac23d51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0d6a8ef-fcef-4692-84a0-33c05bb2a065",
                    "LayerId": "6b336890-2d77-486d-95ab-68756e9696da"
                }
            ]
        },
        {
            "id": "793e19e0-8ea9-4b04-9099-5da98772a146",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffde1fbe-11c4-4fc6-9d89-3f65c189449f",
            "compositeImage": {
                "id": "79dccc5d-472b-49c6-8d45-6dd1cd8144ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "793e19e0-8ea9-4b04-9099-5da98772a146",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f5ec795-c6fd-43eb-b5b2-e0eeb22bed6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "793e19e0-8ea9-4b04-9099-5da98772a146",
                    "LayerId": "6b336890-2d77-486d-95ab-68756e9696da"
                }
            ]
        },
        {
            "id": "be1c9a1c-1769-4264-97a3-2cd3867a5213",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffde1fbe-11c4-4fc6-9d89-3f65c189449f",
            "compositeImage": {
                "id": "43533993-3fd9-44cb-bd50-f7d96966b072",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be1c9a1c-1769-4264-97a3-2cd3867a5213",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcffab91-563e-48b9-8f9f-671b99512cb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be1c9a1c-1769-4264-97a3-2cd3867a5213",
                    "LayerId": "6b336890-2d77-486d-95ab-68756e9696da"
                }
            ]
        },
        {
            "id": "14b7e025-3056-42ee-af2f-5169f75b843d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffde1fbe-11c4-4fc6-9d89-3f65c189449f",
            "compositeImage": {
                "id": "b4de2740-59cf-448c-a96f-e905d7867989",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14b7e025-3056-42ee-af2f-5169f75b843d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a31ea419-7ea5-42f7-b192-55ddd1df79e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14b7e025-3056-42ee-af2f-5169f75b843d",
                    "LayerId": "6b336890-2d77-486d-95ab-68756e9696da"
                }
            ]
        },
        {
            "id": "fff000f2-7b8a-47d2-a083-88787b80d7b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffde1fbe-11c4-4fc6-9d89-3f65c189449f",
            "compositeImage": {
                "id": "776a1a2c-9a1d-4e63-ab61-2cb955944ed6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fff000f2-7b8a-47d2-a083-88787b80d7b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5198967-8fba-41f8-b81f-7a66e66acfca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fff000f2-7b8a-47d2-a083-88787b80d7b4",
                    "LayerId": "6b336890-2d77-486d-95ab-68756e9696da"
                }
            ]
        },
        {
            "id": "eb08f53c-df50-492f-b486-6690e854fdb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffde1fbe-11c4-4fc6-9d89-3f65c189449f",
            "compositeImage": {
                "id": "2a9951b3-1144-454a-9806-f0d367abe2b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb08f53c-df50-492f-b486-6690e854fdb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "737d7b94-4d13-4d33-a7f1-64915fff6a9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb08f53c-df50-492f-b486-6690e854fdb8",
                    "LayerId": "6b336890-2d77-486d-95ab-68756e9696da"
                }
            ]
        },
        {
            "id": "3ccc5b8f-f5d0-4018-bcab-51977efe224d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffde1fbe-11c4-4fc6-9d89-3f65c189449f",
            "compositeImage": {
                "id": "5e63ff19-5b10-4bf7-b028-cbbd69fb2b10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ccc5b8f-f5d0-4018-bcab-51977efe224d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0855e82e-04e0-4d13-9d60-89fdb8f35f87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ccc5b8f-f5d0-4018-bcab-51977efe224d",
                    "LayerId": "6b336890-2d77-486d-95ab-68756e9696da"
                }
            ]
        },
        {
            "id": "009599ab-2813-4429-b021-0bbbdd976dbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffde1fbe-11c4-4fc6-9d89-3f65c189449f",
            "compositeImage": {
                "id": "4188287b-1945-4e2a-841e-11bc3fc5555f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "009599ab-2813-4429-b021-0bbbdd976dbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a2345d0-485d-4f12-b88c-443132d3db2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "009599ab-2813-4429-b021-0bbbdd976dbe",
                    "LayerId": "6b336890-2d77-486d-95ab-68756e9696da"
                }
            ]
        },
        {
            "id": "4b14e39c-0c1f-47f7-882d-000fcfb3f8f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffde1fbe-11c4-4fc6-9d89-3f65c189449f",
            "compositeImage": {
                "id": "83c06765-8b91-47aa-b0ca-de70ec3afbb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b14e39c-0c1f-47f7-882d-000fcfb3f8f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86a4bd93-76a1-489f-ada9-11bb02ecc77a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b14e39c-0c1f-47f7-882d-000fcfb3f8f4",
                    "LayerId": "6b336890-2d77-486d-95ab-68756e9696da"
                }
            ]
        },
        {
            "id": "2c7c737b-8b97-46a8-98f3-378d836f917f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffde1fbe-11c4-4fc6-9d89-3f65c189449f",
            "compositeImage": {
                "id": "66654115-708b-49ca-ab13-cfe5296734cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c7c737b-8b97-46a8-98f3-378d836f917f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5b44ea1-3228-4c75-8627-0d78b38fb14f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c7c737b-8b97-46a8-98f3-378d836f917f",
                    "LayerId": "6b336890-2d77-486d-95ab-68756e9696da"
                }
            ]
        },
        {
            "id": "5cecfdf9-d1c9-4010-b2af-916ff757bb83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffde1fbe-11c4-4fc6-9d89-3f65c189449f",
            "compositeImage": {
                "id": "6bb67797-9a7d-4a87-8c0f-8b42165d18d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cecfdf9-d1c9-4010-b2af-916ff757bb83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c293bbd-2db6-411a-8bef-eb35ebe365ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cecfdf9-d1c9-4010-b2af-916ff757bb83",
                    "LayerId": "6b336890-2d77-486d-95ab-68756e9696da"
                }
            ]
        },
        {
            "id": "01cbe3de-b669-4110-860b-6818acf93620",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffde1fbe-11c4-4fc6-9d89-3f65c189449f",
            "compositeImage": {
                "id": "b1e4c503-8769-4d94-953d-4a6b4107caac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01cbe3de-b669-4110-860b-6818acf93620",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f9747b2-59f1-4aef-b732-15b68e33ac48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01cbe3de-b669-4110-860b-6818acf93620",
                    "LayerId": "6b336890-2d77-486d-95ab-68756e9696da"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "6b336890-2d77-486d-95ab-68756e9696da",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ffde1fbe-11c4-4fc6-9d89-3f65c189449f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}