{
    "id": "9c32c586-efe1-4080-a1ba-98bff199a04b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_5h_fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 16,
    "bbox_right": 175,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1ed3959e-76f1-4714-b9c1-38780e4d939f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "3b012987-5089-4bc3-8b71-3e28dcc49d0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ed3959e-76f1-4714-b9c1-38780e4d939f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a28b8870-1e79-4668-9912-b426a3f4ac98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ed3959e-76f1-4714-b9c1-38780e4d939f",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "1e4b3c77-342b-4da7-9422-869c5c0ee862",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "0f8607cf-ff95-4b3d-b990-32fffa48c795",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e4b3c77-342b-4da7-9422-869c5c0ee862",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a484198-1a5d-4bf6-a2eb-6035518fb7eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e4b3c77-342b-4da7-9422-869c5c0ee862",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "0ca35477-9673-4735-863f-f6709421fb6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "deff87b6-b71f-4c81-8bc3-057f9781339c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ca35477-9673-4735-863f-f6709421fb6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "804dbc12-9442-4a14-83d6-365cd3a2d316",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ca35477-9673-4735-863f-f6709421fb6b",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "2c43c12d-ff08-409e-87b0-a7fe400ffe42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "c6914319-2753-4565-8ed7-d5b94db583fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c43c12d-ff08-409e-87b0-a7fe400ffe42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "594d47d3-d43b-4d8f-89bc-c37f92042b92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c43c12d-ff08-409e-87b0-a7fe400ffe42",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "b33f9881-3d98-4b9b-8837-755915219fbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "b8d947ed-af65-4fb9-bdc3-3e70a9f2b650",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b33f9881-3d98-4b9b-8837-755915219fbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e4fd26c-9793-46d1-bd1a-c22330218151",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b33f9881-3d98-4b9b-8837-755915219fbb",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "5ebfab43-28ed-4076-9b6c-2f0e96713ee9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "34a1500b-9afa-4634-9d35-1098afc63e95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ebfab43-28ed-4076-9b6c-2f0e96713ee9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d576cace-416a-4c36-88f6-437d51331e68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ebfab43-28ed-4076-9b6c-2f0e96713ee9",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "95ea3880-b70d-4796-951c-3510d8c9648c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "7acad1cb-a0a6-47c3-b212-338dd0636ba1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95ea3880-b70d-4796-951c-3510d8c9648c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a37e3dd-c216-4c68-a6be-d7eb94a72078",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95ea3880-b70d-4796-951c-3510d8c9648c",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "2b265a0b-8296-4d70-84d9-1b822723c6d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "4deedb36-58c3-4ae0-b3d9-4c196256d68f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b265a0b-8296-4d70-84d9-1b822723c6d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dcead8d-2e11-4cb9-93cf-27c68a556d4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b265a0b-8296-4d70-84d9-1b822723c6d2",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "3556d1cd-2c19-414b-910a-9e047442ce80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "6a7ba3ec-b771-4de8-8cf9-d2faa861f417",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3556d1cd-2c19-414b-910a-9e047442ce80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fb9bd14-1849-4c11-83ce-e28ea8dd6cae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3556d1cd-2c19-414b-910a-9e047442ce80",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "d583761e-14bc-4996-b892-601c4ca2368a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "e09f14f7-6423-48ae-9367-54ad2ac8173b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d583761e-14bc-4996-b892-601c4ca2368a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f3153d3-2594-460c-b5d9-a6d1fcdfefd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d583761e-14bc-4996-b892-601c4ca2368a",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "66b436e1-12b9-4136-a545-7df808f25c9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "1a5d923e-5713-4353-8b60-801a5e1cc1f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66b436e1-12b9-4136-a545-7df808f25c9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad132cee-da47-491f-b4d0-a97a7dccabc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66b436e1-12b9-4136-a545-7df808f25c9c",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "dda61352-25ca-4531-a592-68b331c44bbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "d47c2e0e-b2db-47d1-a859-255f14134c36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dda61352-25ca-4531-a592-68b331c44bbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfba7e97-fb26-46f9-be0e-c47866f9632c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dda61352-25ca-4531-a592-68b331c44bbf",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "fc82d666-a4d8-4243-a553-b513af79051b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "ec0e605b-4e76-41e5-8092-409d807ff310",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc82d666-a4d8-4243-a553-b513af79051b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a92875d1-36bb-4f5f-b4e9-7ea4e42e2761",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc82d666-a4d8-4243-a553-b513af79051b",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "eace5620-c152-422f-ade3-80df41060675",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "6e32699a-95cb-4d24-9690-26d4f05c215f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eace5620-c152-422f-ade3-80df41060675",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef635f9a-06fe-4378-82cf-042c66e61232",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eace5620-c152-422f-ade3-80df41060675",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "c2fda84c-2440-4195-ab9c-d510788200d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "32b01f8e-e576-45b6-b302-23ff5f4ba161",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2fda84c-2440-4195-ab9c-d510788200d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "472f99d5-15bd-40dd-8512-eba9e856b894",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2fda84c-2440-4195-ab9c-d510788200d7",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "cf7a7199-635a-4fe7-8c9b-0a0c16d3be1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "889f0df6-657b-420f-8f3f-1e79f22cd555",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf7a7199-635a-4fe7-8c9b-0a0c16d3be1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b46762f-bc05-42f4-ab22-95f5f18b4ae1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf7a7199-635a-4fe7-8c9b-0a0c16d3be1d",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "b23a8d5c-7dd7-42c8-9fa7-895cdedb631c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "1740022d-a86f-453e-b14d-5c32e478fd12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b23a8d5c-7dd7-42c8-9fa7-895cdedb631c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89de9942-09b6-481c-998f-a8e0ee23dd3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b23a8d5c-7dd7-42c8-9fa7-895cdedb631c",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "3b8d46de-56ca-4398-8651-9c95d11fb3f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "44c716b7-cc43-4bb1-bff3-c17e20136718",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b8d46de-56ca-4398-8651-9c95d11fb3f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbda5b27-728d-47a2-93a8-5cfb7bf1e758",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b8d46de-56ca-4398-8651-9c95d11fb3f7",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "1369f25b-2728-4acd-9690-a07220573100",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "688d0efa-db65-442e-af6b-755434757753",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1369f25b-2728-4acd-9690-a07220573100",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4dbfe1e-d2dd-442e-8d7c-e68f06267b4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1369f25b-2728-4acd-9690-a07220573100",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "e05019fb-d088-4469-bebb-9bb41cbfe218",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "244d099c-7ed4-4686-8079-93328df52bdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e05019fb-d088-4469-bebb-9bb41cbfe218",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73818a87-82ad-4710-8f9a-6a039ddde415",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e05019fb-d088-4469-bebb-9bb41cbfe218",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "e25a2fe6-989e-4100-bed7-be82fabbf325",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "542a54a6-7abb-4807-b965-c0509d2a827b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e25a2fe6-989e-4100-bed7-be82fabbf325",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28203d5c-92c9-4c5f-945e-6be26d3f4098",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e25a2fe6-989e-4100-bed7-be82fabbf325",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "cd238eac-7118-47c0-992c-8aa75b0dc826",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "b6d3f3b5-a38a-4b5b-af54-6596b6554f81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd238eac-7118-47c0-992c-8aa75b0dc826",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f2b33a3-211f-4d98-ab24-4dfe32f71a43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd238eac-7118-47c0-992c-8aa75b0dc826",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "79ab6f6f-c591-4ba0-9dac-e8137819071c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "c5288fdc-4b32-4a43-a3b9-109241dd2543",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79ab6f6f-c591-4ba0-9dac-e8137819071c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e63577b9-191d-4e5a-96f3-698e2e8a5e89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79ab6f6f-c591-4ba0-9dac-e8137819071c",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "121848b0-16a1-4134-9d90-b865794ed4ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "56574ae2-441d-4142-9371-533fffc6f4e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "121848b0-16a1-4134-9d90-b865794ed4ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3b46c08-6787-433d-bad3-249345b4e551",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "121848b0-16a1-4134-9d90-b865794ed4ef",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "03e9b409-8c4e-471d-86f1-b66788645f7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "ed7cec8f-e9ac-44d8-88ed-4ff47cf96b67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03e9b409-8c4e-471d-86f1-b66788645f7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a130705-fcee-4bff-abaf-5a7cf9f575df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03e9b409-8c4e-471d-86f1-b66788645f7c",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "3cd48d65-fd77-49b6-aa72-d0ef8e7bbefb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "2e910cc7-f52b-4ee8-bd64-0945d4910459",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cd48d65-fd77-49b6-aa72-d0ef8e7bbefb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e66263c5-fbe2-4428-b7e6-da67d0d04fd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cd48d65-fd77-49b6-aa72-d0ef8e7bbefb",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "f223b396-89b4-4c13-b430-e4769f378f0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "24a9e9a0-4c0c-4f4c-b2a2-caccd3c4f53b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f223b396-89b4-4c13-b430-e4769f378f0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93e9f53a-46fb-4d82-ab62-80bb2a75e7a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f223b396-89b4-4c13-b430-e4769f378f0b",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "88c3a539-ee66-4b7f-848c-573982537c2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "9c2f2d0c-2d34-4740-9d57-d9d88abc45f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88c3a539-ee66-4b7f-848c-573982537c2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75d5fb66-9a17-41d4-965e-10d3004e9d73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88c3a539-ee66-4b7f-848c-573982537c2d",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "a41025b2-d4a9-46e8-a0b5-97124d7c5f70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "76ca60c5-c831-4bad-874d-756cf3a137cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a41025b2-d4a9-46e8-a0b5-97124d7c5f70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d632e82e-8a2f-4243-aaf9-6e29f58adf77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a41025b2-d4a9-46e8-a0b5-97124d7c5f70",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "8ff05bb8-751b-451e-af43-385eed4fb1b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "346e4945-aa3e-4f69-a9a2-9b065dad69cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ff05bb8-751b-451e-af43-385eed4fb1b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6bde48f-2e19-4db0-ba99-4a9510101c13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ff05bb8-751b-451e-af43-385eed4fb1b7",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "449a5ef0-e880-48a2-9194-9b8a44455efe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "7ea091b8-b1f3-41a2-a9ca-0557cf12c63c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "449a5ef0-e880-48a2-9194-9b8a44455efe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2971b90a-e9d6-40d9-a74b-bda4644703c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "449a5ef0-e880-48a2-9194-9b8a44455efe",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "1d784faf-726d-41f8-bbb7-ada465f9292f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "2425e4c9-c255-4ca3-8ddd-d83c31ca8d15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d784faf-726d-41f8-bbb7-ada465f9292f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62e421c5-dea9-490f-9653-d01af4046d6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d784faf-726d-41f8-bbb7-ada465f9292f",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        },
        {
            "id": "e894e35e-d239-42e0-81ba-bf87b9abd64b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "compositeImage": {
                "id": "5052152d-9a67-4066-9e99-38d4d6911f61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e894e35e-d239-42e0-81ba-bf87b9abd64b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d87f161a-7e5b-4bae-8efc-d3fe3a7448bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e894e35e-d239-42e0-81ba-bf87b9abd64b",
                    "LayerId": "3aea5963-6b57-4690-ac94-2d46966082dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "3aea5963-6b57-4690-ac94-2d46966082dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9c32c586-efe1-4080-a1ba-98bff199a04b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 190,
    "xorig": 63,
    "yorig": 87
}