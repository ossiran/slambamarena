{
    "id": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_fthrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 50,
    "bbox_right": 109,
    "bbox_top": 75,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9ea96d3-db13-4893-9ec5-fae9908085df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "b34e8511-44ec-48ef-82b4-14acdf324a99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9ea96d3-db13-4893-9ec5-fae9908085df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ba64551-1b39-4f44-8f04-6cb66e9bb288",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9ea96d3-db13-4893-9ec5-fae9908085df",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "027b74be-7272-4470-ae08-8ea3ad02f4e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "7bb28365-978f-4960-9b56-9315ce011eba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "027b74be-7272-4470-ae08-8ea3ad02f4e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffcaea79-de92-48ec-b028-ceb1f7435c42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "027b74be-7272-4470-ae08-8ea3ad02f4e5",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "42020fb9-75c2-42fd-9315-e14c779cba1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "00af1037-e34e-4175-a17b-3b4f45ad5667",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42020fb9-75c2-42fd-9315-e14c779cba1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78a9544a-2232-4d4c-b186-0f88ac0a7053",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42020fb9-75c2-42fd-9315-e14c779cba1f",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "1c40c2b6-44d8-45ed-ad4b-31f58c1570d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "90aaf4f7-1868-400a-9371-c5d161d8f9fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c40c2b6-44d8-45ed-ad4b-31f58c1570d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30120896-a49b-47d5-a992-6a3662bdc50d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c40c2b6-44d8-45ed-ad4b-31f58c1570d9",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "0f3b7dff-76f9-446e-8a23-b936f34d52c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "221eca19-7357-4024-b729-f98529b95505",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f3b7dff-76f9-446e-8a23-b936f34d52c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01d48cb8-79d1-4792-b9ea-24a7c81659ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f3b7dff-76f9-446e-8a23-b936f34d52c8",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "9196e3ba-2f54-4eef-8c82-c8b845118347",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "eb699b13-9ea0-4e58-b400-b5b7157285eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9196e3ba-2f54-4eef-8c82-c8b845118347",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a335d6f-9625-4a90-ab9e-ae00d7754747",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9196e3ba-2f54-4eef-8c82-c8b845118347",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "782e15f2-d2bf-41b3-bcd1-4996e2618f19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "202e7d55-2c7e-40fa-9c46-afedee0271b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "782e15f2-d2bf-41b3-bcd1-4996e2618f19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "113540d4-19be-42c6-adca-b8e2eec99451",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "782e15f2-d2bf-41b3-bcd1-4996e2618f19",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "63301aff-e7d8-4c8e-83c5-2c7454541c0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "70e315ae-0846-43a3-9c7c-ada56725c1cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63301aff-e7d8-4c8e-83c5-2c7454541c0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2277940-042f-49ce-9f95-9c8065f95bb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63301aff-e7d8-4c8e-83c5-2c7454541c0e",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "bb7797da-e0d3-4217-9c65-eb3e6a391c3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "f1bad28e-89d7-4ed7-b566-1dda68fbbbd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb7797da-e0d3-4217-9c65-eb3e6a391c3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bae876b-7d0d-4dad-9067-e651ce1dfbab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb7797da-e0d3-4217-9c65-eb3e6a391c3e",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "fc496282-4778-4c0d-a4bf-bb5defbe9f3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "75dc28b5-043c-49b3-ab24-bdccb4df4a84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc496282-4778-4c0d-a4bf-bb5defbe9f3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "277c72f2-0bb8-4825-a3e3-62d071ea8c68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc496282-4778-4c0d-a4bf-bb5defbe9f3d",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "06f2efdd-955a-473a-849c-477047566ce4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "376edc1d-54b0-48da-afac-9bea71cf64f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06f2efdd-955a-473a-849c-477047566ce4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab95d96d-e6ac-48ba-bce4-7c22c8e87b24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06f2efdd-955a-473a-849c-477047566ce4",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "171315e3-ad4f-4d03-b7a9-4b1f82b4fae0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "72fd79c2-e2f1-4533-8b92-75eff25c9db2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "171315e3-ad4f-4d03-b7a9-4b1f82b4fae0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2c92bca-d8d8-4f47-b9ed-9381b9cb7d60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "171315e3-ad4f-4d03-b7a9-4b1f82b4fae0",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "905d5269-fcc3-41c4-8c41-269ae447b7d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "c92324bb-9da6-4675-b3c0-39c546a8edb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "905d5269-fcc3-41c4-8c41-269ae447b7d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d0f3a23-03d5-4902-bd31-1c2b2250addc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "905d5269-fcc3-41c4-8c41-269ae447b7d1",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "df795564-7331-4ba8-837e-06c5b552ed19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "28930388-acf9-42f7-a04d-d669d3b6cca7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df795564-7331-4ba8-837e-06c5b552ed19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6853a053-d932-4e02-9374-4820b0212d39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df795564-7331-4ba8-837e-06c5b552ed19",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "952ab92d-43e8-41e7-8014-682a7effabf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "1cc53f27-51bc-4a71-88a3-7d4504e00042",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "952ab92d-43e8-41e7-8014-682a7effabf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b53733e9-3997-4a1e-b422-9c3de64ff312",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "952ab92d-43e8-41e7-8014-682a7effabf1",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "35b44031-1076-4bdc-9148-a70dc13786df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "aba5c8bc-22fc-4394-a774-04c46c4101dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35b44031-1076-4bdc-9148-a70dc13786df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cdc776f-18d2-4942-aa03-7e822f8e6f76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35b44031-1076-4bdc-9148-a70dc13786df",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "a4ee2a4c-19fd-436f-8571-d7484c7bfe0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "871d254d-7e4d-4f65-97ab-0755d78ac706",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4ee2a4c-19fd-436f-8571-d7484c7bfe0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b3a6db1-ec5e-4ca8-9df4-2f178aa67616",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4ee2a4c-19fd-436f-8571-d7484c7bfe0b",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "80de6ccf-6f56-4e48-91e0-4f0df065ef90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "03f71d3d-72a5-46e5-ae75-68b3338bc712",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80de6ccf-6f56-4e48-91e0-4f0df065ef90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "435d463e-ef61-4d4a-b5b2-f49b1b44a536",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80de6ccf-6f56-4e48-91e0-4f0df065ef90",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "2e880492-7d54-426d-b7d3-a34084b191b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "70f280b7-e01e-4920-b08b-b68128d2e033",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e880492-7d54-426d-b7d3-a34084b191b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6442ceb-da9b-4ca5-a13b-e90addff1b3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e880492-7d54-426d-b7d3-a34084b191b7",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "a7600701-2eb9-4042-af78-70881b72bf15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "79a1242e-6924-4556-8379-c5a923861818",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7600701-2eb9-4042-af78-70881b72bf15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "650994be-cf68-4a14-a9f2-68b341270789",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7600701-2eb9-4042-af78-70881b72bf15",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "51325101-b772-4224-9690-c83542f92de7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "a47dae05-d383-4e9a-92d1-072c87b01bca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51325101-b772-4224-9690-c83542f92de7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84605da6-75ed-4c57-bc56-e3589b7a7aff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51325101-b772-4224-9690-c83542f92de7",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "da2960c1-bee2-4731-97d9-230032a0ecea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "1a7bec9c-ae84-406d-bdf2-d74963d165e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da2960c1-bee2-4731-97d9-230032a0ecea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd31a930-62df-4db9-a0d7-46eb79dd0c2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da2960c1-bee2-4731-97d9-230032a0ecea",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "b4b4b162-2c06-4596-8fbc-fec71a353a36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "e0b8a77e-a080-4bd9-81fa-a7a4bded114a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4b4b162-2c06-4596-8fbc-fec71a353a36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b69360c-5fa3-4e74-9c2d-2f0d7d280907",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4b4b162-2c06-4596-8fbc-fec71a353a36",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "b994d574-4034-4ddb-9f5a-f2190844cd35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "685b11b8-185a-4510-8aef-01d596ebd330",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b994d574-4034-4ddb-9f5a-f2190844cd35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfcb66e6-a2ad-4314-95cb-702ed070ee1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b994d574-4034-4ddb-9f5a-f2190844cd35",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "1b10b527-0bce-45bc-bb25-25889aab246a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "a3fbf112-236d-442a-80e4-bfc862114e93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b10b527-0bce-45bc-bb25-25889aab246a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28a9c8f2-acf1-4ef0-81ef-8f26082ca6d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b10b527-0bce-45bc-bb25-25889aab246a",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "e89aaa74-2888-4078-b7a5-f6dfa358dd25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "fc655584-4780-41ca-9f15-a2c191839927",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e89aaa74-2888-4078-b7a5-f6dfa358dd25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59e423a6-1119-4098-81df-508a75d8610e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e89aaa74-2888-4078-b7a5-f6dfa358dd25",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "ad725c3f-a608-401e-9929-c8e781041320",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "8eed38da-edc8-4087-9aa6-281a3a50f551",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad725c3f-a608-401e-9929-c8e781041320",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abb272b1-3b9a-4c88-9b95-9b95a84a118c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad725c3f-a608-401e-9929-c8e781041320",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "67bec831-c53d-44e2-8942-2cb3e9fd3a2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "f79221e5-90ff-4226-95e3-2280adc35033",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67bec831-c53d-44e2-8942-2cb3e9fd3a2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "188a7600-b396-4a51-9711-f6c793f2f7ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67bec831-c53d-44e2-8942-2cb3e9fd3a2d",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "78398293-91c2-4aea-ab0d-c8822c63a16a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "566b6ecc-2943-44e9-b8a7-53e48a9c4a8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78398293-91c2-4aea-ab0d-c8822c63a16a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b47d1254-2fe8-483b-b2ca-1d3efbd3d4ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78398293-91c2-4aea-ab0d-c8822c63a16a",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "fb92cf93-a688-4b5b-82e3-2427820701dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "2217f62c-f632-4d35-8ab8-44de2571611b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb92cf93-a688-4b5b-82e3-2427820701dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d07613bf-10b6-489b-bc68-bac9a241d692",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb92cf93-a688-4b5b-82e3-2427820701dd",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "00892172-1957-4dc7-9d83-4866aee66f28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "516c38f4-0d20-478f-b084-9a834982d8bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00892172-1957-4dc7-9d83-4866aee66f28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00c1466e-fe65-43a7-875a-d7fa78858534",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00892172-1957-4dc7-9d83-4866aee66f28",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "5f56de09-22dd-4f36-a562-d9eb5bbdcccd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "9a52bebf-385b-439a-bc75-2b8b0bb36579",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f56de09-22dd-4f36-a562-d9eb5bbdcccd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "098627e2-fc33-4719-8082-99fa627face4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f56de09-22dd-4f36-a562-d9eb5bbdcccd",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "184b89b7-667e-43af-a1e6-195791029f62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "715daf4a-efdb-4187-9de8-bc6cc3c6da3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "184b89b7-667e-43af-a1e6-195791029f62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5840e68-53c9-491b-bab8-bbd57a28e08a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "184b89b7-667e-43af-a1e6-195791029f62",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "054e17ca-3484-4ddf-8f1f-aa81bcb9c074",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "2128a61e-aa8d-4096-843f-46052a569503",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "054e17ca-3484-4ddf-8f1f-aa81bcb9c074",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab6f42ab-1d66-4be8-b797-a39b8d3a5bee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "054e17ca-3484-4ddf-8f1f-aa81bcb9c074",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "5cd21a54-42b4-46ec-bc53-c564bfc5eed9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "f1da900a-596c-40ce-b8db-9c986f9924d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cd21a54-42b4-46ec-bc53-c564bfc5eed9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5937edc1-ee3b-4cfc-b35c-68f156550e5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cd21a54-42b4-46ec-bc53-c564bfc5eed9",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "d72ce12c-fdbe-4ba6-9c20-d81308320058",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "9d025555-1952-49fe-862b-d02d5e88a606",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d72ce12c-fdbe-4ba6-9c20-d81308320058",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2778b482-4adc-4376-9e8f-139f7d117b64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d72ce12c-fdbe-4ba6-9c20-d81308320058",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "31112bb7-5e1c-45ac-a74b-9d2fab6f1d5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "7796183c-c440-43d8-ad9f-d05178e06bdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31112bb7-5e1c-45ac-a74b-9d2fab6f1d5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c0fcffd-a19a-4324-bc6c-6897798a7926",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31112bb7-5e1c-45ac-a74b-9d2fab6f1d5d",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "390e85bb-17c2-40eb-9710-1959a93a9e10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "f3c659ae-f97d-44ed-b052-ab59665cced8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "390e85bb-17c2-40eb-9710-1959a93a9e10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54800e75-2049-4cec-b547-77f09d90942f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "390e85bb-17c2-40eb-9710-1959a93a9e10",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "1eb4aeb9-b2fb-4701-b703-e6871a5bdeaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "6a54b14d-d03c-45de-b815-74305627365b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1eb4aeb9-b2fb-4701-b703-e6871a5bdeaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1996b688-ca93-4411-9c8c-5f2298fc6bf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1eb4aeb9-b2fb-4701-b703-e6871a5bdeaa",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "6f86e0bb-ef26-40c8-b1e3-269008e14f31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "4715df58-fb84-4b2b-97ae-730b5341e3b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f86e0bb-ef26-40c8-b1e3-269008e14f31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88fe7b67-2f19-4971-8eb5-443312baf960",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f86e0bb-ef26-40c8-b1e3-269008e14f31",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "1a49e7e2-8d04-4b24-8c9b-1506672d453b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "7c873692-2622-4ddd-b158-41dcebed6602",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a49e7e2-8d04-4b24-8c9b-1506672d453b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcc60e32-909c-4d79-bb25-8481ae29ab80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a49e7e2-8d04-4b24-8c9b-1506672d453b",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "7cfb1bec-abe2-4fb3-99b9-c3c0b14f3cca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "53528846-3d60-416c-afe0-d5be3b453966",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cfb1bec-abe2-4fb3-99b9-c3c0b14f3cca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7dd8833-d9da-41d7-91d4-a50dc8d22fb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cfb1bec-abe2-4fb3-99b9-c3c0b14f3cca",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "dd80d625-6fee-4a86-9716-24b33efbbba6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "dbe740f6-742b-4411-8713-6f5f2e797ba1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd80d625-6fee-4a86-9716-24b33efbbba6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34a25a12-1cc4-4e55-b114-4142d8bf2fa2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd80d625-6fee-4a86-9716-24b33efbbba6",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "e5cc69c3-665e-4e75-86fd-9208b6855966",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "bacc9c6b-08d8-4c86-bdf1-0789f75f940e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5cc69c3-665e-4e75-86fd-9208b6855966",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "721cf141-164b-4c41-9be3-2597c576af62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5cc69c3-665e-4e75-86fd-9208b6855966",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        },
        {
            "id": "2260e374-2d1a-4fd8-817a-ae813c883aac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "compositeImage": {
                "id": "f244c32e-78f0-46e8-9d5e-1425cf4c0f41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2260e374-2d1a-4fd8-817a-ae813c883aac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62eef539-6581-4dad-b908-448c73ccb474",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2260e374-2d1a-4fd8-817a-ae813c883aac",
                    "LayerId": "d7c82086-beb9-4069-9c60-9cfea1345d47"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "d7c82086-beb9-4069-9c60-9cfea1345d47",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a182ead8-777e-4bf7-b8e7-fa78873a7995",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}