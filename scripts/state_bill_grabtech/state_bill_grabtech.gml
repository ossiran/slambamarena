if(state_new == 1)
{
	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -43;
	    image_xscale  = 26;
	    image_yscale  = 75;
	    xshift  = -2;
	}
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 0;
	    image_yscale  = 0;
	    xshift = -2
	}
}

vx = 3 * frontOrBack;
vy = 0;

if(state_timer > 11)
{
    if(onground())
    {
        state_switch("stand");
    }
    else
    {
        state_switch("air");
    }
}