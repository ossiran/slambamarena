live_call();

if(state_new == 1)
{
    with(hurtbox0)
	{
	    xoffset = -12;
	    yoffset = -13;
	    image_xscale  = 24;
	    image_yscale  = 40;
	    xshift  = 0;
	}
	
	with(hurtbox1)
	{
	    xoffset = -8;
	    yoffset = -31;
	    image_xscale  = 16;
	    image_yscale  = 18;
	    xshift = 0;
	}
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -13;
	    image_xscale  = 16;
	    image_yscale  = 40;
	    xshift = 0;
	}
	
	chainmove = 0;
	attackframe0 = 5;
	endstate = 30;
}

//animation
if(synmode > 0)
{
    sprite_index = spr_lsyn_jm;
}
else if(synmode < 0)
{
    sprite_index = spr_dsyn_jm;
}



//attack
if(state_timer == attackframe0)
{
    hitbox0 = create_hitbox(-12,-24,60,30,7.25,0,14,20,11,15,0,0,3,4,"mid",-1);
    attackhurtbox0 = create_hurtbox(8 * image_xscale, -7,28 * image_xscale,16,3,-2);
    hitbox0.hitsound = snd_hitsound1;
    hitbox0.scaling = 1;
    hitbox0.juggle = 0;
}

//chainmove
if(state_timer > 3)
{
        
    if(input_check(0,0,0,1,0))
    {
        chainmove = "hair"
    }
}

attack_base(endstate,1,0,1,1,0);