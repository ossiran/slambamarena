{
    "id": "079461ac-310e-4281-910e-51a12fb04448",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_chdm_ice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 122,
    "bbox_left": 24,
    "bbox_right": 154,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ccc5386c-ced9-4a85-9213-c2380b74edfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "4b36d80b-63be-4ab3-8218-2f138dbd4622",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccc5386c-ced9-4a85-9213-c2380b74edfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12d320e4-93db-48a0-a561-d3b7c1085c03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccc5386c-ced9-4a85-9213-c2380b74edfb",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "e3d208fa-b745-4ab4-ae01-77bf5441c289",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "9d1884d8-53a2-4925-a61d-8b26f6c3facd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3d208fa-b745-4ab4-ae01-77bf5441c289",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58f09874-133e-49c0-bf26-fbf1800e538e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3d208fa-b745-4ab4-ae01-77bf5441c289",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "2c6a0e31-6766-42ab-9832-a90262da663b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "721da3bb-4008-49fb-8b53-fc0ff4609746",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c6a0e31-6766-42ab-9832-a90262da663b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f164dd91-b426-4c6f-a4e4-89adcc1a6365",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c6a0e31-6766-42ab-9832-a90262da663b",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "3d9fb1d5-3ddc-44f5-ad4c-ad7aa2858860",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "30a635f2-a3a8-46d4-81e7-f1cee2b5aee3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d9fb1d5-3ddc-44f5-ad4c-ad7aa2858860",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bd973cb-71ea-4183-8be4-f7229acef561",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d9fb1d5-3ddc-44f5-ad4c-ad7aa2858860",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "f30b78de-f350-450f-9214-5934baf6ca02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "ebc37b37-49a1-4dd4-a8dc-2a98402cc454",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f30b78de-f350-450f-9214-5934baf6ca02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cea6e00b-97cb-4d72-82cd-e2546935110a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f30b78de-f350-450f-9214-5934baf6ca02",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "a7278424-b234-4f9a-9141-66eba3b5c0c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "bdfc746d-5ae3-494e-a31b-554595647a98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7278424-b234-4f9a-9141-66eba3b5c0c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff177e1b-077b-4d6e-a98e-fd79048c9b5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7278424-b234-4f9a-9141-66eba3b5c0c4",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "d626d6e4-f75f-47de-b820-46daaf84789b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "55482d2a-82ad-4f86-ab6b-a57319bae2a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d626d6e4-f75f-47de-b820-46daaf84789b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59210492-f585-4e66-ac48-ff5015f1fe84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d626d6e4-f75f-47de-b820-46daaf84789b",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "e49a2e8b-248d-4169-a832-4ffed7673b31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "203ba8b6-1286-44f4-b624-c3af6cf7d939",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e49a2e8b-248d-4169-a832-4ffed7673b31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36f44a07-c89b-4bb5-8d22-01d9eeb30f4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e49a2e8b-248d-4169-a832-4ffed7673b31",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "b839a6c0-82a7-496e-8047-fb3823c6049f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "d444813f-b366-4fc9-a6e3-35d98d5e1d9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b839a6c0-82a7-496e-8047-fb3823c6049f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "454cb4ef-d54b-4d1e-85bf-acdc4ed1e10b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b839a6c0-82a7-496e-8047-fb3823c6049f",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "729a6e01-10b7-4abe-b52b-7382c7cbd9c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "d57b4d64-8204-4c05-9c36-d0f36d7e5f88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "729a6e01-10b7-4abe-b52b-7382c7cbd9c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84dd062f-3d90-4eab-bc49-36cfc1e89d35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "729a6e01-10b7-4abe-b52b-7382c7cbd9c8",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "7a561cce-2a33-4298-8348-3376ded687ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "d03eeab6-3189-4591-a697-ef3fea040546",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a561cce-2a33-4298-8348-3376ded687ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ee96447-5c91-4ec2-b3a2-9a96f9418b56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a561cce-2a33-4298-8348-3376ded687ab",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "1a145299-d1c6-46b1-90c7-b1b87db5faac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "0506aa21-8330-4567-8503-5f67ab148296",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a145299-d1c6-46b1-90c7-b1b87db5faac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5992f4f1-dbd6-467c-9467-d759964dae67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a145299-d1c6-46b1-90c7-b1b87db5faac",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "006a9592-8ee3-469e-9b1d-595518dc2e16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "3883eb48-1bbf-481c-854c-5d76d626697a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "006a9592-8ee3-469e-9b1d-595518dc2e16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9e2507b-3821-4be4-be26-3be56d6e2f2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "006a9592-8ee3-469e-9b1d-595518dc2e16",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "76b963c2-73fa-4843-bac0-88d5360d1454",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "a49a79ee-d365-4612-9ac6-91ccac68ef2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76b963c2-73fa-4843-bac0-88d5360d1454",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58663867-2b44-42da-92d1-555f06baa49d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76b963c2-73fa-4843-bac0-88d5360d1454",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "de1e25bd-9f8b-4b2f-83f0-a8f9befc7647",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "0585d05e-becb-41c2-9628-7faefb59fd02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de1e25bd-9f8b-4b2f-83f0-a8f9befc7647",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab4aecdf-05e7-4ad6-b082-adaa753a6631",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de1e25bd-9f8b-4b2f-83f0-a8f9befc7647",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "a18c0ec5-2f08-465b-8d37-5d62ef204975",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "0096e75b-470d-4c96-90af-eb98987b1004",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a18c0ec5-2f08-465b-8d37-5d62ef204975",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cf01e98-8727-4ddf-89bc-7e51eb3e7b62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a18c0ec5-2f08-465b-8d37-5d62ef204975",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "37f1d3d2-cbd9-412f-9d6f-a7827fe11752",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "3d5a4107-93b4-4b39-ad56-fc0cb5d4819e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37f1d3d2-cbd9-412f-9d6f-a7827fe11752",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01fe9118-07f1-4faa-aa20-58a3f3183d8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37f1d3d2-cbd9-412f-9d6f-a7827fe11752",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "9a311740-0856-4924-8f77-821c6993c3ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "f480e6cd-b488-4066-98c3-f39e1b82baf2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a311740-0856-4924-8f77-821c6993c3ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5617ad7c-348e-4f6b-8ef3-58db534f2f0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a311740-0856-4924-8f77-821c6993c3ff",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "c2188610-5e06-475e-ad1d-00742f8582b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "6fa58167-7ca1-4804-93c5-6581422ac77b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2188610-5e06-475e-ad1d-00742f8582b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90424130-f68c-4aa8-82a1-712064cb02e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2188610-5e06-475e-ad1d-00742f8582b5",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "e92d79c1-cae2-4968-b576-6d2638876656",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "b7fdc979-57a6-49bd-9a7c-0d53a16736d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e92d79c1-cae2-4968-b576-6d2638876656",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c930c3f-2de8-4d57-ab9a-5ba3b019af56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e92d79c1-cae2-4968-b576-6d2638876656",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "08b75bbf-e180-4a8f-bc56-310a6e696dec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "6c662f53-8842-45bd-be5f-66fd630eb961",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08b75bbf-e180-4a8f-bc56-310a6e696dec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37984b34-e98b-487c-a3e2-6dadc2ef50b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08b75bbf-e180-4a8f-bc56-310a6e696dec",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "2968d626-c428-472f-bbf6-1ee07714ba0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "d42c85c6-0eb9-46cb-a1f9-e3653be7e8b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2968d626-c428-472f-bbf6-1ee07714ba0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a41892c0-82c9-4494-8b51-fe1658263d74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2968d626-c428-472f-bbf6-1ee07714ba0a",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "72cc34e9-d79d-40f0-bcd7-30cda360bea5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "05aa2c74-a06d-41c4-9017-f9ef01f69289",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72cc34e9-d79d-40f0-bcd7-30cda360bea5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af56338d-5137-46ac-8288-2fd799e6ca24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72cc34e9-d79d-40f0-bcd7-30cda360bea5",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "e4b22dae-53cf-4d35-9572-e8b5fd3f26f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "04e6606f-a169-4a33-8b02-d404fa034a97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4b22dae-53cf-4d35-9572-e8b5fd3f26f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6540b868-4c2a-44b7-a75a-3701e66dc198",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4b22dae-53cf-4d35-9572-e8b5fd3f26f8",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "518a5c09-55c1-47be-aab7-f6d7944b7c2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "f0e51ce1-474b-4655-bd4f-87f3d2afe713",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "518a5c09-55c1-47be-aab7-f6d7944b7c2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b934f63-1139-4e6d-a467-5cd7d6ade701",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "518a5c09-55c1-47be-aab7-f6d7944b7c2e",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "05e6f55e-9549-4c61-9b62-4ed1a1a9e557",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "01b89c6d-4b56-4151-a339-4a5852d9434d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05e6f55e-9549-4c61-9b62-4ed1a1a9e557",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76f17dcf-9947-4b42-8f38-9310f102129a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05e6f55e-9549-4c61-9b62-4ed1a1a9e557",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "dd2d147a-dc78-46b5-8f6f-5eb389a18d42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "8ea562ff-acce-4ef0-b2fa-b81e372f3705",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd2d147a-dc78-46b5-8f6f-5eb389a18d42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10af83e0-7feb-4358-b9a7-816e31b7a762",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd2d147a-dc78-46b5-8f6f-5eb389a18d42",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "f0905e39-718e-4f37-ba23-99d5a6957dac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "2d9363c1-8791-4cd4-b017-63e686a6bb93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0905e39-718e-4f37-ba23-99d5a6957dac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "686be60e-9fad-4f2a-a624-71bf0a994584",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0905e39-718e-4f37-ba23-99d5a6957dac",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "be7bf7ba-1187-4099-a952-a044121e72c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "2f536673-fa9b-4fb1-b293-e75faf77dda1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be7bf7ba-1187-4099-a952-a044121e72c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b661908e-457b-4f46-89f4-e9c091dc14f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be7bf7ba-1187-4099-a952-a044121e72c4",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "ec7320e4-bff1-4677-bbb2-fef636327fe4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "830fa145-ed9d-457e-8d35-1d47e5b13f99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec7320e4-bff1-4677-bbb2-fef636327fe4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb864c93-060a-463c-b5a0-0e70ee62ace9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec7320e4-bff1-4677-bbb2-fef636327fe4",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "cd96014c-4148-4281-a1d2-fc70cd6c8613",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "0f075cd6-3614-49d7-8003-04028ac1bc95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd96014c-4148-4281-a1d2-fc70cd6c8613",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8dc9bd9-220f-4da5-8ea2-53b315fee470",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd96014c-4148-4281-a1d2-fc70cd6c8613",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "6dad742f-728e-4312-a83e-6e844d9ea2ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "2af5306f-02a3-4ff9-9876-e7b8a15154c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dad742f-728e-4312-a83e-6e844d9ea2ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2841dd6-bc7a-4016-b7e4-00e5bf5517ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dad742f-728e-4312-a83e-6e844d9ea2ea",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "c06da336-0cb3-4aa0-91b8-86d6eb12de21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "a74d9d59-d762-4b81-8803-6b8658bb76a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c06da336-0cb3-4aa0-91b8-86d6eb12de21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8271b8d-ebe1-484e-9d17-dab0eed381ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c06da336-0cb3-4aa0-91b8-86d6eb12de21",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "a766f303-e4b0-4cb1-a63f-e64168bea735",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "58cb15e8-85a6-44f2-957a-0505c3cbc933",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a766f303-e4b0-4cb1-a63f-e64168bea735",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3060674-85b2-4d94-8433-95a0145de5d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a766f303-e4b0-4cb1-a63f-e64168bea735",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "f8618b28-09f3-4ad6-8bf0-c42693972a8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "dd152a25-f0c1-48f2-a361-fc3dcb68efca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8618b28-09f3-4ad6-8bf0-c42693972a8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4aae02e5-1779-46d2-8d74-0af8128e987f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8618b28-09f3-4ad6-8bf0-c42693972a8c",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "3c21c0ef-fdbd-4ac8-8d8c-fcfd6ca869af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "eb5d5aa7-4239-49bf-87fc-7650ddbc8a13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c21c0ef-fdbd-4ac8-8d8c-fcfd6ca869af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c92cd73-5d85-4e32-bc9b-dc8b2e7f07f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c21c0ef-fdbd-4ac8-8d8c-fcfd6ca869af",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "c358bb6c-0174-478a-8c35-61a47fb50e8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "71b67b2b-107d-4bda-b8a3-0f4e303c29eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c358bb6c-0174-478a-8c35-61a47fb50e8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72484ba4-c94a-4b3a-a635-7b3f2d5e53d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c358bb6c-0174-478a-8c35-61a47fb50e8e",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "3cce8f39-e476-4831-806c-4ff47eb182ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "9dba15fa-4054-4248-a9ef-b0dad52e940f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cce8f39-e476-4831-806c-4ff47eb182ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38e5d2bd-83ea-4f84-ad73-1068e91a58b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cce8f39-e476-4831-806c-4ff47eb182ad",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "89020fca-0101-473c-b5ed-43da2bbbeafd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "a535320a-2a9e-4999-b612-17290fba4bed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89020fca-0101-473c-b5ed-43da2bbbeafd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4a6bf38-1e5d-4643-9ae3-1fa9b986d419",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89020fca-0101-473c-b5ed-43da2bbbeafd",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "d912ac7a-4f9b-4fc6-a9b9-68d1d88a81f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "ed86e986-f3f5-47f7-af3e-d5f52425f574",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d912ac7a-4f9b-4fc6-a9b9-68d1d88a81f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87063efb-3a7a-4b69-896e-d9d6e3c20dcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d912ac7a-4f9b-4fc6-a9b9-68d1d88a81f0",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "2c0d9a6c-0011-45b2-bcf7-a752897c891d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "71cb3e9c-54d2-4315-afcb-d65c5b930fd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c0d9a6c-0011-45b2-bcf7-a752897c891d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf9c5c04-8412-45f8-b552-cc6aa47891dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c0d9a6c-0011-45b2-bcf7-a752897c891d",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "b7143329-1b38-44c4-be77-c4bd16c60bc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "d75d9e90-2eee-43cb-ba6d-134dc3a61c29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7143329-1b38-44c4-be77-c4bd16c60bc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ce2284d-7390-4df1-bda7-c0eb1512b52f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7143329-1b38-44c4-be77-c4bd16c60bc3",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        },
        {
            "id": "07210b99-e752-442b-bdd2-137eb6cecc96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "compositeImage": {
                "id": "aca772ca-ce37-4d3d-8d88-c085f1131f1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07210b99-e752-442b-bdd2-137eb6cecc96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1df703d8-a35b-4f97-94f5-e915c584d531",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07210b99-e752-442b-bdd2-137eb6cecc96",
                    "LayerId": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 148,
    "layers": [
        {
            "id": "3080eac8-7c5a-44f6-85c0-99abc09bbeb9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "079461ac-310e-4281-910e-51a12fb04448",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 168,
    "xorig": 63,
    "yorig": 87
}