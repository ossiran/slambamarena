if(state_new == 1)
{
    with(hurtbox0)
	{
	    xoffset = -12;
	    yoffset = -13;
	    image_xscale  = 24;
	    image_yscale  = 40;
	    xshift  = 0;
	}
	
	with(hurtbox1)
	{
	    xoffset = -8;
	    yoffset = -31;
	    image_xscale  = 16;
	    image_yscale  = 18;
	    xshift = 0;
	}
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -13;
	    image_xscale  = 16;
	    image_yscale  = 40;
	    xshift = 0;
	}
	
	if(synmode > 0)
    {
        sprite_index = spr_lsyn_transform;
    }
    else
    {
        sprite_index = spr_dsyn_transform;
    }
    image_index = 0;
    
    
}

image_index = state_timer;

if(state_timer == 34)
{
    state_switch("stand");
    
    if(sprite_index == spr_lsyn_transform)
    {
        synmode = -1800;
    }
    else
    {
        synmode = 1800;
    }
}

//hitstun
if(hitstun > 0)
{
    state_switch("hitstun");   
}

if(grabbed == 1)
{
 state_switch("grabbed");   
}