{
    "id": "dd81ce73-6569-4056-9828-11775596cb1b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dsyn_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 49,
    "bbox_right": 74,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d7cc82c-cf00-420a-82fe-8c03bfd1dc61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "937f7465-53ca-413e-a918-ac31f0b2e62c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d7cc82c-cf00-420a-82fe-8c03bfd1dc61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d31d0696-548b-4095-a57c-c3b00a4912a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d7cc82c-cf00-420a-82fe-8c03bfd1dc61",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "93a21b8a-29aa-493c-91f9-0be44c8bb4d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "3ca99678-79c3-463b-a24a-9e322db5a575",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93a21b8a-29aa-493c-91f9-0be44c8bb4d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fff5f9c3-e276-403c-97e2-f2c385300efe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93a21b8a-29aa-493c-91f9-0be44c8bb4d9",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "1a9038f4-3637-42f6-80e6-7b9bca610bba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "4d7af155-eb50-4413-ab0d-94a2bf6675a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a9038f4-3637-42f6-80e6-7b9bca610bba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5879cdea-d14a-4c66-a4d1-db8be156aa4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a9038f4-3637-42f6-80e6-7b9bca610bba",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "8ef713fc-21cb-42a1-80d8-464cc66acc87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "f7d9aa63-5fed-4020-8118-a61912c34f40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ef713fc-21cb-42a1-80d8-464cc66acc87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58cb9ca2-30f2-4f0d-9536-8105b938e50d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ef713fc-21cb-42a1-80d8-464cc66acc87",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "d1af0e6b-8e57-47f6-8adb-112adf0b4b32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "a3133dee-d93f-4fbf-ba63-a28e80f7b216",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1af0e6b-8e57-47f6-8adb-112adf0b4b32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75786e60-247b-4c33-ab89-8a63789a7034",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1af0e6b-8e57-47f6-8adb-112adf0b4b32",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "6cfd28eb-a7e3-446b-9629-c81fc209a991",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "5e925ef2-82c0-481b-86be-19a494a38ed1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cfd28eb-a7e3-446b-9629-c81fc209a991",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67d84304-9322-493d-b42e-8ee1e0ba6025",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cfd28eb-a7e3-446b-9629-c81fc209a991",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "ddaec0ac-a5a3-4e10-a940-b87f814d6dac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "27e87d08-56e2-499e-9328-7f0f9816be37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddaec0ac-a5a3-4e10-a940-b87f814d6dac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc7c775c-9a63-40fc-9a75-667990805248",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddaec0ac-a5a3-4e10-a940-b87f814d6dac",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "4532913d-7868-4ee7-abba-47e7719c8cdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "c0ab1681-2a0c-481a-ae3d-652e01eeb01a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4532913d-7868-4ee7-abba-47e7719c8cdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "956c561b-e79f-47d6-9436-7a930989cfc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4532913d-7868-4ee7-abba-47e7719c8cdf",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "ad54fff6-6d57-4578-b4fa-00911bdc0e2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "bb53ee1e-4ad1-44c6-ac35-c43586b08160",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad54fff6-6d57-4578-b4fa-00911bdc0e2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "415c0fbc-5424-4a86-a564-378296f1aea9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad54fff6-6d57-4578-b4fa-00911bdc0e2a",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "1dc5dfae-0251-41d3-b601-8ba5dfb2b02b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "65414e75-618d-4ffe-b8f9-a002f4f9fd31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dc5dfae-0251-41d3-b601-8ba5dfb2b02b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06ecd467-38de-4da1-86b6-3fd24c779b72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dc5dfae-0251-41d3-b601-8ba5dfb2b02b",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "1ba67202-9e25-46b2-ab00-86e0b4ab564a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "aa0c53bc-7efd-4c00-a577-70326f0ce024",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ba67202-9e25-46b2-ab00-86e0b4ab564a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e01838e-12e0-4a0b-9c9c-ef27d76198a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ba67202-9e25-46b2-ab00-86e0b4ab564a",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "dce34778-fde3-4ef5-af73-3d53cbf39fd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "8f89601c-5eec-4df5-926f-9cc790c7b59b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dce34778-fde3-4ef5-af73-3d53cbf39fd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f60c6860-68f7-49d2-9a9b-437a302dcbd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dce34778-fde3-4ef5-af73-3d53cbf39fd5",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "5ca5a3eb-5069-490c-a97f-0685d7877e7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "51efb8fa-2104-4634-9076-2b6d8f21126c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ca5a3eb-5069-490c-a97f-0685d7877e7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05ec73ab-57d9-4366-a173-41d27f6154bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ca5a3eb-5069-490c-a97f-0685d7877e7a",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "13a69679-b237-4bcc-9d89-07b25b334184",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "daf866a4-74a6-436b-8423-e536365e30f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13a69679-b237-4bcc-9d89-07b25b334184",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7253b054-ea8b-42e7-8be1-15f3e8ab0c38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13a69679-b237-4bcc-9d89-07b25b334184",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "1591a383-9376-4102-8f72-42dc3acde524",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "4695a929-c265-4ca3-950b-5edb1251fbaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1591a383-9376-4102-8f72-42dc3acde524",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ba82643-c202-4f0e-b261-bdb625c9104b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1591a383-9376-4102-8f72-42dc3acde524",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "edc4b615-19ed-4545-9451-ca0f124e6234",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "48ea817e-6e57-4b47-a298-d34f9f1580b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edc4b615-19ed-4545-9451-ca0f124e6234",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca6ec8d9-7be6-4d8b-a847-4be3780a1fb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edc4b615-19ed-4545-9451-ca0f124e6234",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "2b512926-8b40-446f-a8c6-f0814e829322",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "c0975e71-afa1-44a1-b51a-419127312b7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b512926-8b40-446f-a8c6-f0814e829322",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04cf41bc-8738-4163-8400-43911c602cae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b512926-8b40-446f-a8c6-f0814e829322",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "d1b7687e-162f-4e79-b97d-2ca4c44b9fa8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "72fac6ac-c158-4eac-b19c-fd35a58ade3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1b7687e-162f-4e79-b97d-2ca4c44b9fa8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a81f83a1-0adb-4646-a219-ccc817eacc23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1b7687e-162f-4e79-b97d-2ca4c44b9fa8",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "ce72203a-0d84-4930-a6f5-f06afdc212f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "04bd551e-4400-4a66-a35b-6c857ddbdd07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce72203a-0d84-4930-a6f5-f06afdc212f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2465b8a-16d3-41dd-af01-076cde141195",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce72203a-0d84-4930-a6f5-f06afdc212f7",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "764894e4-bb43-4415-b699-040dfb7477fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "93fc1555-bc5b-4ddb-b773-03718e1ac764",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "764894e4-bb43-4415-b699-040dfb7477fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69c815a8-7028-42cd-a065-764fde72df20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "764894e4-bb43-4415-b699-040dfb7477fa",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "e8738b6a-4b66-4603-bfc5-16815b73301e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "348d2372-2a6a-4be6-bf98-6b635956b194",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8738b6a-4b66-4603-bfc5-16815b73301e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d9fe99b-c017-4bce-8086-d4d349ddd197",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8738b6a-4b66-4603-bfc5-16815b73301e",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "60ad18b1-0e29-47a4-8ad7-a3605d17c1a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "ad3c66e0-27dd-4feb-92f2-d6447fe2026f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60ad18b1-0e29-47a4-8ad7-a3605d17c1a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ea80d20-616e-4878-9412-33ece343d01e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60ad18b1-0e29-47a4-8ad7-a3605d17c1a0",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "f66324e3-d263-4614-bdeb-80a23f2654ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "f85513ad-fda7-4381-9b10-5c7698af1cb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f66324e3-d263-4614-bdeb-80a23f2654ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0302d9d-cf29-4ede-87b4-ec291d052603",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f66324e3-d263-4614-bdeb-80a23f2654ed",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "f1fb25ab-ed2c-42b9-bee8-e2912766dfb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "652a1c6f-7601-4a00-8ef6-48d38654e198",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1fb25ab-ed2c-42b9-bee8-e2912766dfb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "206ee5ea-3c21-41ab-8dfb-e85ad8a0ae3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1fb25ab-ed2c-42b9-bee8-e2912766dfb5",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "a3c7f600-201e-4416-b1ca-6ef92d20137a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "fcce6d12-aafe-4309-a5a6-48e18e88d943",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3c7f600-201e-4416-b1ca-6ef92d20137a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3887adb1-439f-4ddb-bdf6-4a2cb7b61f01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3c7f600-201e-4416-b1ca-6ef92d20137a",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "85553a09-7f7e-40e9-b37f-60360decb8a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "85811caf-cae3-4088-a01e-05872e400fa2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85553a09-7f7e-40e9-b37f-60360decb8a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17a29ed4-4d2f-475b-824d-c5f496d1bc6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85553a09-7f7e-40e9-b37f-60360decb8a7",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "43f76313-5de1-44c2-ae53-b5061a999ac2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "9738705e-1ea2-4bb8-83cc-5fa226c602f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43f76313-5de1-44c2-ae53-b5061a999ac2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e8cbf34-e8eb-4d1d-806e-611e7bf3f309",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43f76313-5de1-44c2-ae53-b5061a999ac2",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "7d65d6bf-d2af-46a2-9cd2-d6def84630ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "b7c45e92-c082-453d-8d19-8a63ffb3b633",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d65d6bf-d2af-46a2-9cd2-d6def84630ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b57410a-b595-455b-a1cd-c2a3bea154b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d65d6bf-d2af-46a2-9cd2-d6def84630ee",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "c9385f41-b68e-4a36-830a-3bd226e6a838",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "d06578ba-3411-44bb-a7df-48299c84d25a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9385f41-b68e-4a36-830a-3bd226e6a838",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9b0fb1e-7a1f-4ffc-b3ca-12545b09de61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9385f41-b68e-4a36-830a-3bd226e6a838",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "04be5ed7-7c47-4258-9772-8521738994bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "6d974187-735f-447f-9290-77cd4b1ad85b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04be5ed7-7c47-4258-9772-8521738994bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac86ad24-5a82-495f-ad3f-6a10627cff1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04be5ed7-7c47-4258-9772-8521738994bf",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "2bcec784-a7fc-409a-b465-67a5525faa04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "3614c21f-6667-4d04-83f0-a00ced082aba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bcec784-a7fc-409a-b465-67a5525faa04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e15f233-c4b6-4874-82da-1bade1ad9222",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bcec784-a7fc-409a-b465-67a5525faa04",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "a31db05c-6706-49eb-b928-441e95923ed3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "5940bd3a-68d5-4d1a-87b3-2f1b43975977",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a31db05c-6706-49eb-b928-441e95923ed3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a92b9cf0-1ac2-453c-87c8-a5171b858471",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a31db05c-6706-49eb-b928-441e95923ed3",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "7c47794e-c6d2-455d-8b2d-45eca21699e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "3c30ad3c-9f52-49b5-94c4-4a4ca154d6a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c47794e-c6d2-455d-8b2d-45eca21699e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b376a62d-4109-48b1-846e-0d873db4c9b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c47794e-c6d2-455d-8b2d-45eca21699e2",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "65ec7980-7db8-4704-b9e9-d61ce8c2b487",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "ba44ad85-caff-408e-83e5-9b153ce4fd11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65ec7980-7db8-4704-b9e9-d61ce8c2b487",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31a81f50-caef-44f2-b833-8f474e92b520",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65ec7980-7db8-4704-b9e9-d61ce8c2b487",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "1ea883ce-23cf-4ef3-a4a6-b394c65c1e83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "85234564-8d64-4f76-8c04-1d5ffdfa3a82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ea883ce-23cf-4ef3-a4a6-b394c65c1e83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0ea47c5-502e-40cf-a71f-74efb56bf191",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ea883ce-23cf-4ef3-a4a6-b394c65c1e83",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "8807bd60-898c-431f-ad86-caa9d141a389",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "b5facafb-c5a9-4574-88ec-71e36e12aabb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8807bd60-898c-431f-ad86-caa9d141a389",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b0b6188-ee54-4275-bcb0-2855bc15dbc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8807bd60-898c-431f-ad86-caa9d141a389",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "8ce1f79a-6ba7-42f8-8aeb-bb5a84226dc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "d112a131-ffe5-4188-8e34-6d7f507f25d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ce1f79a-6ba7-42f8-8aeb-bb5a84226dc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f41d2b36-cc28-4eda-9c78-d69f35367d4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ce1f79a-6ba7-42f8-8aeb-bb5a84226dc4",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "8676f2e1-8073-4d93-aade-103a0a15eb90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "c8313025-c72a-42bc-bedc-8729d2bbfa71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8676f2e1-8073-4d93-aade-103a0a15eb90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb678201-4f56-4ec0-8713-1125ebdabda2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8676f2e1-8073-4d93-aade-103a0a15eb90",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "54afd2f1-9b25-42b6-b4f2-94e8f0442a81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "3a150bf6-7990-46de-9dda-5627573fde56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54afd2f1-9b25-42b6-b4f2-94e8f0442a81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fedaab7-b6b3-4629-ac35-4e32ee9f1cb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54afd2f1-9b25-42b6-b4f2-94e8f0442a81",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "af689246-ee30-46b1-b3c0-326a0a6bcccc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "64599aa0-ae79-444c-9355-762b77d36581",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af689246-ee30-46b1-b3c0-326a0a6bcccc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06793778-1c38-46b6-a989-6c9f6c061ab5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af689246-ee30-46b1-b3c0-326a0a6bcccc",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "ef945b56-de44-498f-a433-44dfe38e5023",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "5374029a-a293-4c54-9a2e-008c62b8d53b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef945b56-de44-498f-a433-44dfe38e5023",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65b49e37-6e6e-46c8-95d5-331b316eb594",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef945b56-de44-498f-a433-44dfe38e5023",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "f8848ed3-6f94-499a-858e-46454c446c2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "0b11dcc9-8638-47d6-acb4-82be8bb4a2a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8848ed3-6f94-499a-858e-46454c446c2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c068a357-1d82-4f26-b04e-730e82f6c540",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8848ed3-6f94-499a-858e-46454c446c2f",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "b193c36b-98f4-4d9c-85d0-b21f1f3a670d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "048c8629-7785-45c8-86ad-ef9300e5dfa6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b193c36b-98f4-4d9c-85d0-b21f1f3a670d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8513845-6619-4e39-bf0c-d9cc3ce263eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b193c36b-98f4-4d9c-85d0-b21f1f3a670d",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "c51f7bcc-13ff-407d-81e4-4650232522d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "c87b4327-b92e-448d-bc28-36d84d2a48b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c51f7bcc-13ff-407d-81e4-4650232522d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4a947a9-6eed-487e-9a52-f5fef5eed52c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c51f7bcc-13ff-407d-81e4-4650232522d3",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "80f03861-7cea-41a4-9ef7-99377a3964c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "06b6c95e-bad2-4ed3-957a-53cc28dc915d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80f03861-7cea-41a4-9ef7-99377a3964c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50067eb4-55fc-4b5b-9814-4e166c60bb1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80f03861-7cea-41a4-9ef7-99377a3964c1",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "f34a07dc-f770-414d-bfb4-f3657bb0030c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "587e5e8b-1206-4aff-a976-567362be588f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f34a07dc-f770-414d-bfb4-f3657bb0030c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4e7bc96-e6e6-45f9-b673-78748977af9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f34a07dc-f770-414d-bfb4-f3657bb0030c",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "fb2ec6a6-25c7-4c7d-ab5f-af5bb22d3f94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "ddd0d304-1c83-486f-8789-02475820412d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb2ec6a6-25c7-4c7d-ab5f-af5bb22d3f94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9308aaa-c96b-4ae9-add5-927acac62383",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb2ec6a6-25c7-4c7d-ab5f-af5bb22d3f94",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "985b1adf-eb09-47d8-af10-91c5b1044044",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "88bf3479-14b2-4388-b7d3-daa67d649878",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "985b1adf-eb09-47d8-af10-91c5b1044044",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98fe41c5-c8e3-4507-9db1-2663fd4643d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "985b1adf-eb09-47d8-af10-91c5b1044044",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "53345af0-957d-41c3-b682-9ccd3331abdc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "e253befb-ae80-48a2-8b9f-a69ff1caf362",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53345af0-957d-41c3-b682-9ccd3331abdc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "699f45bd-dde0-4146-a2e5-17aa51db9cfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53345af0-957d-41c3-b682-9ccd3331abdc",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "b71dcd1b-e955-4668-94cb-ff3f2cbbdca8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "f3b76e35-3e63-4dbd-8e91-d284e9cb2d22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b71dcd1b-e955-4668-94cb-ff3f2cbbdca8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "294951a2-05d7-4c40-abb2-6b2bec120cdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b71dcd1b-e955-4668-94cb-ff3f2cbbdca8",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "7ef30f5e-755c-4b93-8bad-5f1c58ea629c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "397cdd52-e632-45d5-a38e-1cad23cdcc5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ef30f5e-755c-4b93-8bad-5f1c58ea629c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afb002e6-cee2-4d4c-9f6a-adc81d0645d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ef30f5e-755c-4b93-8bad-5f1c58ea629c",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "22d85753-bcee-4716-9117-6bd665dfec6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "87682a48-5787-4a8b-824e-8a6d6ae1b15a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22d85753-bcee-4716-9117-6bd665dfec6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1515078f-af9c-4a51-90d9-c46c9d45322e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22d85753-bcee-4716-9117-6bd665dfec6e",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "0ab11e63-4b20-4b61-8a60-18a8c0ce8451",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "a9709a70-2710-4f05-aa02-96a9c8f0a2e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ab11e63-4b20-4b61-8a60-18a8c0ce8451",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fb8f6fd-ff0e-4035-ac68-cbd34f2f3cdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ab11e63-4b20-4b61-8a60-18a8c0ce8451",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "080eff1e-b2fd-4c61-8113-a3fa084a0ed2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "1de277a6-5739-4ed9-9fbe-2e5704fbd5ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "080eff1e-b2fd-4c61-8113-a3fa084a0ed2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d3a7269-4676-4543-9b74-4669b6a7f9d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "080eff1e-b2fd-4c61-8113-a3fa084a0ed2",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "e33c1638-fd66-4315-95ba-d62a38255f1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "ff2c2b00-baf4-4eee-ac76-99764cf00893",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e33c1638-fd66-4315-95ba-d62a38255f1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5943a3ce-d920-4ce4-8328-442e18cd5f57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e33c1638-fd66-4315-95ba-d62a38255f1f",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "7a34b5c7-87a6-4ad0-a569-76024dd77385",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "2903517b-0a2f-4b7a-905a-1b19b40e11ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a34b5c7-87a6-4ad0-a569-76024dd77385",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbfe791e-9f82-408c-bf97-99f487a81a29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a34b5c7-87a6-4ad0-a569-76024dd77385",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "c2291f89-dbac-429a-adaf-733ebcdfc679",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "e19921dd-031b-4207-ad4e-2ca32fa1f7e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2291f89-dbac-429a-adaf-733ebcdfc679",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "795c73fa-e74f-45b3-bcdd-4da2406ee042",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2291f89-dbac-429a-adaf-733ebcdfc679",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "d6b75170-d081-48f4-acc6-e4292682a50c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "d23a9356-afb1-407a-8026-ef2abefc05c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6b75170-d081-48f4-acc6-e4292682a50c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6750433-35b5-4aa4-86f9-cfccc589ba58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6b75170-d081-48f4-acc6-e4292682a50c",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "71f94457-0908-450b-aebb-4299f5ced057",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "d2f27c73-93a5-41d1-bafc-0a712c09778c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71f94457-0908-450b-aebb-4299f5ced057",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a47519cd-044b-4d3b-b2ce-1c760c438010",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71f94457-0908-450b-aebb-4299f5ced057",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "e86b1aec-8756-42ae-ae7c-31ff2b02c312",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "2a1a102e-834c-4057-b57c-7eaa41107f95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e86b1aec-8756-42ae-ae7c-31ff2b02c312",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21530b7d-ee4f-4310-84b3-73b2538c2927",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e86b1aec-8756-42ae-ae7c-31ff2b02c312",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "abbc5fdd-2948-4f03-b726-5486f53f1474",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "19387302-9a2d-428d-8db4-8a1ec67cca66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abbc5fdd-2948-4f03-b726-5486f53f1474",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6ed4348-619c-4000-a172-18699a50fa24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abbc5fdd-2948-4f03-b726-5486f53f1474",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "3c8fa623-bd55-4628-bd95-37472a5b40de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "3ebc67f3-4072-45a8-bdef-b72ead0c1f0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c8fa623-bd55-4628-bd95-37472a5b40de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d661c96-052a-4057-97d6-096f47dfb69a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c8fa623-bd55-4628-bd95-37472a5b40de",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "269e7e0c-0cc0-446f-aee5-c0faafa75206",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "291b58e7-7401-4cc9-8c3b-2d93ca297b5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "269e7e0c-0cc0-446f-aee5-c0faafa75206",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2619fd30-5b01-46af-ab96-0d7d99832dbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "269e7e0c-0cc0-446f-aee5-c0faafa75206",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        },
        {
            "id": "c34471dc-98f3-4a0b-ab8e-709f2f54c440",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "compositeImage": {
                "id": "5a21a249-4268-414f-b33e-a4fcb6063031",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c34471dc-98f3-4a0b-ab8e-709f2f54c440",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c5f3e3e-a7ec-4aa1-8222-7176c2751ac5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c34471dc-98f3-4a0b-ab8e-709f2f54c440",
                    "LayerId": "73973050-67bf-49b5-877b-99c375fab68c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "73973050-67bf-49b5-877b-99c375fab68c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd81ce73-6569-4056-9828-11775596cb1b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}