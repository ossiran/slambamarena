{
    "id": "c2266898-e0a1-4458-bedb-cc0d99659cea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dsyn_66h",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c128cc50-c3ca-4b79-9799-8a6605f2f539",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2266898-e0a1-4458-bedb-cc0d99659cea",
            "compositeImage": {
                "id": "e7c2bc4c-a5f2-4bc3-88c6-8f720613375c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c128cc50-c3ca-4b79-9799-8a6605f2f539",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c30f8571-3a01-4258-ba0c-5aecbb27738b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c128cc50-c3ca-4b79-9799-8a6605f2f539",
                    "LayerId": "f65d3b06-6078-4e8f-9fa3-fa7053846000"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f65d3b06-6078-4e8f-9fa3-fa7053846000",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2266898-e0a1-4458-bedb-cc0d99659cea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}