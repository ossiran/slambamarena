///attack_base(timer,air,projectile,special cancel, super cancel, jump cancel)
if(state_new == 1)
{
    animationdelay = 0;
}

if(argument1 == 1 )
{
    if(!hitpausing())
    {
    	vy = Approach(vy,vyMax,grav);
    }
}
else
{
    if(!hitpausing())
    {
        vx = Approach(vx,0,ground_fric);
    }
}

//animating
image_index = state_timer - animationdelay;
    
if( hitpausing())
{
    state_timer -= 1;
    
    if(vxwas == 0 && vywas == 0)
    {
        vxwas = vx / 1.2;
        vywas = vy;
    }
    
    vx = 0;
    vy = 0;
}
else
{
    if((vxwas != 0 || vywas != 0 ) && argument2 == 0)
    {
        vx = vxwas;
        vy = vywas;
        
        vxwas = 0;
        vywas = 0;
    }
}




if(state_timer >= argument0)
{
    if(onground())
    {
        
        vy = 0;
        if(key_down == 1)
        {
            state_switch("crouch");
            end_move();
        }
        else
        {
            state_switch("stand");
            end_move();
        }
        
        hitting = 0;
        if(argument2 == 0)
        {
            with(obj_hitbox)
                {
                    if(owner.id == other && projectile = 0)
                    {
                        instance_destroy()
                    }
                }
        }
    }
    else
    {
        state_switch("air");
        end_move();
    }
    vxwas = 0;
    vywas = 0;
}




if(hitting == 1)
{
    //dash cancel
    /*
    if(argument4 == 1 && otherplayer.hitpause > 0 && chainmove == 0)
    {
        if(onground())
        {
            //dash
            //fdash
            if((ff_buffer > 0 || input_check(6, 1, 1, 0, 0) || input_check(5, 1, 1, 0, 0)) && ex > 50)
            {
                //excost
                ex += -50;
                //cancel sound
				audio_play_sound(snd_parry,1,0);
				//trail
				trailduration = 16;
				vxwas = 0;
                vywas = 0;
                    state_switch("fdash");
                    end_move();
            }
            //bdash
            if((bb_buffer > 0  || input_check(4, 1, 1, 0, 0)) && ex > 50)
            {
                //ex cost
                ex += -50;
                //cancel sound
				audio_play_sound(snd_parry,1,0);
				//trail
				trailduration = 16;
                    state_switch("bdash");
                    end_move(); 
                    
            }
            
            if(state_exists("dqcf") == 1 && dqcf_buffer > 0 && ex > 99.9)
            {
                if(input_check(0,0,0,1,0))
                {
                    state_switch("dqcf");
                }
                else if(input_check(0,0,1,0,0))
                {
                    state_switch("dqcf");
                }
                else if(input_check(0,1,0,0,0))
                {
                    state_switch("dqcf");
                }
            }
        }
        else
        {
            //airdash
            if((ff_buffer > 0 || input_check(6, 1, 1, 0, 0) || input_check(5, 1, 1, 0, 0)) && airdash == 1 && ex > 50)
            {
				audio_play_sound(snd_parry,1,0);
                state_switch("airdash");
                dashdir = image_xscale;
                airdash = 0;
                ex += -50;
                end_move();
                //trail
				trailduration = 16;
            }
            if((bb_buffer > 0  || input_check(4, 1, 1, 0, 0)) && airdash == 1 && ex > 50)
            {
				audio_play_sound(snd_parry,1,0);
                state_switch("airdash");
                dashdir = -image_xscale;
                airdash = 0;
                ex += -50;
                //trail
				trailduration = 16;
                
                    
                end_move();
            }
        }
    }
    */
               
    
    //jump cancel
    if(argument5 == 1 && otherplayer.hitpause < 6 && hitpausing())
    {
        if(onground())
        {
            if(input_check(7,0,0,0,0) || input_check(8,0,0,0,0) || input_check(9,0,0,0,0))
            {
                state_switch("jumpsquat");
                end_move();
            }
        }
        else
        {               
                    /*
                    if((tap(7) || tap(8) || tap(9)) && doublejump > 0)
                    {   
                        //double jump	
                        if(tap(7) && doublejump == 1)
                        {
                        		doublejump --;
                        		vy = -jumpheight * 0.85;
                        		vx = -4 * image_xscale;
                        		audio_play_sound(snd_bill_rocketjump,1,0);
                        }
                        
                        //double jump	
                        if(tap(8) && doublejump == 1)
                        {
                        		doublejump --;
                        		vx = 0;
                        		vy = -jumpheight;
                        		audio_play_sound(snd_bill_rocketjump,1,0);
                        }
                        
                        //double jump	
                        if(tap(9) && doublejump == 1)
                        {
                        		doublejump --;
                        		vy = -jumpheight * 0.85;
                        		vx = 4 * image_xscale;
                        		audio_play_sound(snd_bill_rocketjump,1,0);
                        }
                		doublejump = 0;
                		audio_play_sound(snd_bill_rocketjump,1,0);
                		/*
                		if(dtap_right >= 2)
                		{
                			dtap_right --;
                		}
                		if(dtap_left >= 2)
                		{
                			dtap_left --;
        		        }
        		        */
                        
                        state_switch("air");
                        if(argument2 == 0)
                        {
                            with(obj_hitbox)
                            {
                                if(owner.id == other && projectile = 0)
                                {
                                    instance_destroy()
                                }
                            }
                        }
                        
                        end_move();
                    
        }
    }
    
    //special cancels
    if(argument3 == 1 && otherplayer.hitpause > 0 && cancelmove == 0)
    {
       posspecials();
    }
    
    
    //special to special cancels
    if(argument4 == 1 && argument3 == 0 && otherplayer.hitpause > 0 && cancelmove == 0)
    {
        posspecialscancel();
        //dcf
        if(argument3 = 0 && argument4 == 1 && state_exists("dqcf") == 1 && dqcf_buffer > 0 && chainmove != dqcf && ex > 49.9 && onground() == 1)
        {
            
            if(input_check(0,0,0,1,0))
            {
                chainmove = "dqcf";
            }
            else if(input_check(0,0,1,0,0))
            {
                chainmove = "dqcf";
            }
            else if(input_check(0,1,0,0,0))
            {
                chainmove = "dqcf";
            }
        }
    }
}
else if(argument2 == 1)
{
            /*
            if(argument4 == 1)
            {
                if(state_timer > attackframe0)
                {
                    if(onground())
                    {
                        //dash
                        //fdash
                        
                        if(ff_buffer > 0 || (input_check(6, 1, 1, 0, 0)) && ex > 50)
                        {
                            ex += -50;
    						audio_play_sound(snd_parry,1,0);
    						//trail
				            trailduration = 16;
                            end_move();
                        }
                        //bdash
                        if(bb_buffer > 0 || (input_check(4, 1, 1, 0, 0)) && ex > 50)
                        {
                            ex += -50;
    						audio_play_sound(snd_parry,1,0);
                            //trail
				            trailduration = 16;
                            end_move();
                        }
                        
                        
                    }
                }
                else
                {
                    //airdash
                    if((ff_buffer > 0 || (dash_buffer > 0 && (key_right == 1))) && airdash == 1 && ex > 50)
                    {
                        state_switch("airdash");
                        dashdir = image_xscale * image_xscale;
                        airdash = 0;
                        ex += -50;
                        //trail
				        trailduration = 16;
						audio_play_sound(snd_parry,1,0);

						
                    }
                    if((bb_buffer > 0 || (dash_buffer > 0  && (key_left == 1))) && airdash == 1 && ex > 50)
                    {
                        state_switch("airdash");
                        dashdir = -image_xscale * image_xscale;;
                        airdash = 0;
                        ex += -50;
                        //trail
				        trailduration = 16;
						audio_play_sound(snd_parry,1,0);
                    }
                }
            }
            */
    
    if(argument4 == 1 && state_exists("dqcf") == 1 && dqcf_buffer > 0 && ex > 49.9 && onground() && state_timer > attackframe0)
    {
        if(input_check(0,0,0,1,0))
        {
            state_switch("dqcf");
            end_move();
        }
        else if(input_check(0,0,1,0,0))
        {
            state_switch("dqcf");
            end_move();
        }
        else if(input_check(0,1,0,0,0))
        {
            state_switch("dqcf");
            end_move();
        }
    }
    
    if(state_timer > attackframe0 && state_timer < attackframe0 + 9 && argument3 == 0)
    {
        posspecialscancel();
    }
}

//cancels. after hitpause
if(otherplayer.hitpause == 0 && (chainmove != 0 || cancelmove != 0) && (hitting == 1 || (hitting == 0 && argument2 == 1)))
{
    
    if(argument4 == 1 && argument3 == 0 && chainmove != "dqcf")
    {
        
        if(player_num == 1)
        {
            
            obj_system.message_array_1[obj_system.message_array_count_1, 0] = "SPECIALER CANCEL";
            obj_system.message_array_1[obj_system.message_array_count_1, 1] = 45;
            obj_system.message_array_count_1 ++;
        }
        else
        {
            
            obj_system.message_array_2[obj_system.message_array_count_2, 0] = "SPECIALER CANCEL";
            obj_system.message_array_2[obj_system.message_array_count_2, 1] = 45;
            obj_system.message_array_count_2 ++;
        }
    }
    
    if(cancelmove != 0)
    {
        state_switch(cancelmove);
        if(argument4 == 1 && argument3 == 0)
        {
            ex += -50;
        }
    }
    else
    {
        state_switch(chainmove);
    }
    end_move();
}

if(otherplayer.hitpause == 0)
{
    chainmove = 0;
}


if(argument1 == 1 && onground())
{
    state_switch("landing");
    audio_play_sound(snd_landing,1,0);
    end_move();
}

//hitstun
if(hitstun > 0)
{
    
    
    if(argument1 == 1)
    {
        if(yhit == 0)
        {
        yhit = -5.25;
        xhit = xhit * 1.25;
        hitstun = round(hitstun/3);
        }
        hitfall = -1;
        hit = 0;
    }
    
    state_switch("hitstun");
    
    /*
    //counterhit. too slow moving to the system object
    hitstun = round(hitstun * 1.25);
    if(hitfall > 0)
    {
        hitfall = round(hitfall * 1.25);
    }
    xhit =  xhit * .80;
    yhit =  yhit * .80;
    
    if(state_timer < attackframe0 || hitbox1 != -1 || hitbox2 != -1 || hitbox3 != -1 || hitbox4 != -1)
    {
        counterhit = 1;
    }
    */
    
    end_move();
}

//genie_assists();
//plinkdash leniency

if(state_timer < 3)
{
    if(state_name == "light" || state_name == "medium")
    {
        //dash
        //fdash
        if((input_check(6, 1, 1, 0, 0)) || (input_check(5, 1, 1, 0, 0)))
        {
                state_switch("fdash");
                end_move();
        }
        //bdash
        if((input_check(4, 1, 1, 0, 0)))
        {
                state_switch("bdash");
                end_move();
        }
    }
    
    if(state_name == "light")
    {
        //grab
        if(input_check(0,1,0,0,1))
        {
            state_switch("grab");
            end_move();
        }
    }
}
