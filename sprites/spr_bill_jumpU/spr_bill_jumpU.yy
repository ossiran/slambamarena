{
    "id": "14455a6e-c09e-4983-aa01-73618fcd3337",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_jumpU",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 52,
    "bbox_right": 88,
    "bbox_top": 70,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "619d3f7a-d8bd-414e-b12d-996bf4f9c3f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14455a6e-c09e-4983-aa01-73618fcd3337",
            "compositeImage": {
                "id": "e871f4fb-7131-45b8-9a5c-a79550ceae8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "619d3f7a-d8bd-414e-b12d-996bf4f9c3f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c72c287c-0d06-4d2a-9cda-5c2f21a242b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "619d3f7a-d8bd-414e-b12d-996bf4f9c3f9",
                    "LayerId": "990e53df-2bfb-4879-ac38-a118e62c45a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "990e53df-2bfb-4879-ac38-a118e62c45a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14455a6e-c09e-4983-aa01-73618fcd3337",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 68,
    "yorig": 116
}