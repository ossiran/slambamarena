{
    "id": "3fea898e-6cb4-4c75-8ed5-404226e9e14c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_grabbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "658fd03c-7b00-443d-88ed-cdedc91e0877",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3fea898e-6cb4-4c75-8ed5-404226e9e14c",
            "compositeImage": {
                "id": "33deec87-e628-4c5c-8bfd-f2b4f034d19f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "658fd03c-7b00-443d-88ed-cdedc91e0877",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3424d18-a524-4afe-a268-24c7428e2e5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "658fd03c-7b00-443d-88ed-cdedc91e0877",
                    "LayerId": "9409dbb5-cfe6-49b8-ab33-8e012db8fde3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "9409dbb5-cfe6-49b8-ab33-8e012db8fde3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3fea898e-6cb4-4c75-8ed5-404226e9e14c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}