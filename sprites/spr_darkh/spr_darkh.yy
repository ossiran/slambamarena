{
    "id": "6abb60b9-c0aa-4dcd-8d01-b29cbd0d3727",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_darkh",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 6,
    "bbox_right": 130,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b056a346-7da8-42a8-94fb-8a8edbbe448c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6abb60b9-c0aa-4dcd-8d01-b29cbd0d3727",
            "compositeImage": {
                "id": "594f6ebb-5e57-49bd-8bbf-5eff9bb98c0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b056a346-7da8-42a8-94fb-8a8edbbe448c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d21ab6d1-23d4-43a3-ae69-7f21beaf9be1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b056a346-7da8-42a8-94fb-8a8edbbe448c",
                    "LayerId": "334608ed-74db-433c-917a-f223377b1661"
                }
            ]
        },
        {
            "id": "37c93299-f9d8-4523-8f5b-b0ad8ab3fb72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6abb60b9-c0aa-4dcd-8d01-b29cbd0d3727",
            "compositeImage": {
                "id": "d5b1a3ba-19b1-49c5-9566-326a325c368e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37c93299-f9d8-4523-8f5b-b0ad8ab3fb72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c2f1c77-cddc-4830-8701-44ebdadf9aad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37c93299-f9d8-4523-8f5b-b0ad8ab3fb72",
                    "LayerId": "334608ed-74db-433c-917a-f223377b1661"
                }
            ]
        },
        {
            "id": "aea3032d-dd80-4a09-8f94-b55759fa0a59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6abb60b9-c0aa-4dcd-8d01-b29cbd0d3727",
            "compositeImage": {
                "id": "431eb6aa-47d9-473e-8c73-ddcc78432f91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aea3032d-dd80-4a09-8f94-b55759fa0a59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "145e385c-ff2b-4a62-a595-5f38f7a75e80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aea3032d-dd80-4a09-8f94-b55759fa0a59",
                    "LayerId": "334608ed-74db-433c-917a-f223377b1661"
                }
            ]
        },
        {
            "id": "7fbab4c3-0d21-41e2-ac73-e72afc697175",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6abb60b9-c0aa-4dcd-8d01-b29cbd0d3727",
            "compositeImage": {
                "id": "6a8277c5-1ee5-462f-8358-6218a4f1bc11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fbab4c3-0d21-41e2-ac73-e72afc697175",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fd96fe3-f3cd-4552-8fe9-ba96cdaaf980",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fbab4c3-0d21-41e2-ac73-e72afc697175",
                    "LayerId": "334608ed-74db-433c-917a-f223377b1661"
                }
            ]
        },
        {
            "id": "adf5aa67-f54d-4ca5-8171-030cd6873328",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6abb60b9-c0aa-4dcd-8d01-b29cbd0d3727",
            "compositeImage": {
                "id": "73db2dea-7332-4500-a64e-74e3b43bf681",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adf5aa67-f54d-4ca5-8171-030cd6873328",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04028bda-8580-4922-89cf-05300b397222",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adf5aa67-f54d-4ca5-8171-030cd6873328",
                    "LayerId": "334608ed-74db-433c-917a-f223377b1661"
                }
            ]
        },
        {
            "id": "a3f3af66-acd2-42c9-81d1-e30207008a29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6abb60b9-c0aa-4dcd-8d01-b29cbd0d3727",
            "compositeImage": {
                "id": "bebdb226-f446-466d-ada0-420e348b4856",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3f3af66-acd2-42c9-81d1-e30207008a29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36f68cd9-1d70-4a35-a039-a40cc10565da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3f3af66-acd2-42c9-81d1-e30207008a29",
                    "LayerId": "334608ed-74db-433c-917a-f223377b1661"
                }
            ]
        },
        {
            "id": "b874e975-f4d6-44df-b995-075b01a78cc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6abb60b9-c0aa-4dcd-8d01-b29cbd0d3727",
            "compositeImage": {
                "id": "a9916aa0-571c-49d9-9916-592be6d91a19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b874e975-f4d6-44df-b995-075b01a78cc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18615e23-fa94-437a-b39e-41581669ebc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b874e975-f4d6-44df-b995-075b01a78cc8",
                    "LayerId": "334608ed-74db-433c-917a-f223377b1661"
                }
            ]
        },
        {
            "id": "362a2111-ae4c-48fb-b1df-8a0855a6dc3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6abb60b9-c0aa-4dcd-8d01-b29cbd0d3727",
            "compositeImage": {
                "id": "4c6deb05-c628-44d4-be1c-c172fe11888c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "362a2111-ae4c-48fb-b1df-8a0855a6dc3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cddba2b-8bb8-4bf3-8829-489a172430a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "362a2111-ae4c-48fb-b1df-8a0855a6dc3b",
                    "LayerId": "334608ed-74db-433c-917a-f223377b1661"
                }
            ]
        },
        {
            "id": "cd537888-0af1-462b-891e-36bf1f546155",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6abb60b9-c0aa-4dcd-8d01-b29cbd0d3727",
            "compositeImage": {
                "id": "d7c807d1-6a08-48de-9758-26337eeb0dc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd537888-0af1-462b-891e-36bf1f546155",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d0dcd94-6dfd-4e18-a5eb-332efacdf6c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd537888-0af1-462b-891e-36bf1f546155",
                    "LayerId": "334608ed-74db-433c-917a-f223377b1661"
                }
            ]
        },
        {
            "id": "7ee31405-6c84-4998-92d0-0a4d26b240ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6abb60b9-c0aa-4dcd-8d01-b29cbd0d3727",
            "compositeImage": {
                "id": "2830c671-8797-40b8-8e4d-3f1fc9d1f9b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ee31405-6c84-4998-92d0-0a4d26b240ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a099c120-39a0-421e-ae9f-e2ea52351940",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ee31405-6c84-4998-92d0-0a4d26b240ca",
                    "LayerId": "334608ed-74db-433c-917a-f223377b1661"
                }
            ]
        },
        {
            "id": "2f536221-876d-44b6-95ef-50fc9d7701d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6abb60b9-c0aa-4dcd-8d01-b29cbd0d3727",
            "compositeImage": {
                "id": "212a081c-edec-41bc-a93c-6e70e9dd53a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f536221-876d-44b6-95ef-50fc9d7701d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bcd8604-75f4-4c2f-bd69-984960be8a41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f536221-876d-44b6-95ef-50fc9d7701d9",
                    "LayerId": "334608ed-74db-433c-917a-f223377b1661"
                }
            ]
        },
        {
            "id": "a7260ea1-01c8-4979-92d0-88bc9e1c9e3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6abb60b9-c0aa-4dcd-8d01-b29cbd0d3727",
            "compositeImage": {
                "id": "a5fa4ee1-5d96-416a-b110-7d792aa43c73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7260ea1-01c8-4979-92d0-88bc9e1c9e3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cadf6cce-cd74-4e46-af96-258de47ab295",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7260ea1-01c8-4979-92d0-88bc9e1c9e3c",
                    "LayerId": "334608ed-74db-433c-917a-f223377b1661"
                }
            ]
        },
        {
            "id": "a2ae6930-5372-4df4-bcc8-aaba24b6d6b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6abb60b9-c0aa-4dcd-8d01-b29cbd0d3727",
            "compositeImage": {
                "id": "70807242-17f4-43e8-8003-47d2d6b0c5d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2ae6930-5372-4df4-bcc8-aaba24b6d6b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b34303b4-5d5d-4b47-9efe-59666cfec410",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2ae6930-5372-4df4-bcc8-aaba24b6d6b7",
                    "LayerId": "334608ed-74db-433c-917a-f223377b1661"
                }
            ]
        },
        {
            "id": "af534e9f-1bfb-478b-8b36-681e9a3c8dca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6abb60b9-c0aa-4dcd-8d01-b29cbd0d3727",
            "compositeImage": {
                "id": "5ea88968-700d-4df4-a3c9-055ee9068e9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af534e9f-1bfb-478b-8b36-681e9a3c8dca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfb46310-d932-4c4d-8909-485a5d67a3fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af534e9f-1bfb-478b-8b36-681e9a3c8dca",
                    "LayerId": "334608ed-74db-433c-917a-f223377b1661"
                }
            ]
        },
        {
            "id": "026f33d6-a377-4049-a99a-56d4d236fcdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6abb60b9-c0aa-4dcd-8d01-b29cbd0d3727",
            "compositeImage": {
                "id": "41ca8455-cb61-4a49-afda-96ee1c070273",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "026f33d6-a377-4049-a99a-56d4d236fcdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d219b701-1093-4ffb-9b7b-7c32d6b495bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "026f33d6-a377-4049-a99a-56d4d236fcdd",
                    "LayerId": "334608ed-74db-433c-917a-f223377b1661"
                }
            ]
        },
        {
            "id": "215a4484-578c-4377-bcf0-9712ad9510a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6abb60b9-c0aa-4dcd-8d01-b29cbd0d3727",
            "compositeImage": {
                "id": "c0695bd8-f4af-4564-9a52-536817deb098",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "215a4484-578c-4377-bcf0-9712ad9510a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73b1b468-21d4-4c6f-b074-0f4f2f91eb7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "215a4484-578c-4377-bcf0-9712ad9510a7",
                    "LayerId": "334608ed-74db-433c-917a-f223377b1661"
                }
            ]
        },
        {
            "id": "8db79a94-0dea-476f-bfc7-b595a76de464",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6abb60b9-c0aa-4dcd-8d01-b29cbd0d3727",
            "compositeImage": {
                "id": "b7bcd96b-ad4c-4e8e-a968-49237d07d2d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8db79a94-0dea-476f-bfc7-b595a76de464",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e09f93ff-55fb-4a3d-ab8f-def281cbd374",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8db79a94-0dea-476f-bfc7-b595a76de464",
                    "LayerId": "334608ed-74db-433c-917a-f223377b1661"
                }
            ]
        },
        {
            "id": "815db071-1fab-435a-9b26-226d9ddca77c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6abb60b9-c0aa-4dcd-8d01-b29cbd0d3727",
            "compositeImage": {
                "id": "bb28a37d-593b-497b-a978-239467c55089",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "815db071-1fab-435a-9b26-226d9ddca77c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28ffb373-49ae-42ed-91c8-094230d871ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "815db071-1fab-435a-9b26-226d9ddca77c",
                    "LayerId": "334608ed-74db-433c-917a-f223377b1661"
                }
            ]
        },
        {
            "id": "43da38ad-0016-4837-8640-4f149e038537",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6abb60b9-c0aa-4dcd-8d01-b29cbd0d3727",
            "compositeImage": {
                "id": "4889b644-bf7d-464a-87a3-0f7cc3759357",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43da38ad-0016-4837-8640-4f149e038537",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77398f94-e3ea-4a6b-a8e6-d07181078bd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43da38ad-0016-4837-8640-4f149e038537",
                    "LayerId": "334608ed-74db-433c-917a-f223377b1661"
                }
            ]
        },
        {
            "id": "26e831ed-664e-4e03-84e1-d252fc7a16ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6abb60b9-c0aa-4dcd-8d01-b29cbd0d3727",
            "compositeImage": {
                "id": "aabb1a8f-28e1-4427-abaf-5d4289091962",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26e831ed-664e-4e03-84e1-d252fc7a16ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9424ff8e-a353-4d07-98fd-029917efcca1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26e831ed-664e-4e03-84e1-d252fc7a16ee",
                    "LayerId": "334608ed-74db-433c-917a-f223377b1661"
                }
            ]
        },
        {
            "id": "47256598-5a26-424b-88e4-903e5d3a2618",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6abb60b9-c0aa-4dcd-8d01-b29cbd0d3727",
            "compositeImage": {
                "id": "d42df423-3667-4862-b216-0aa72df7fcb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47256598-5a26-424b-88e4-903e5d3a2618",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d123b09-4cd9-4073-ba7c-20a7a92e5ca7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47256598-5a26-424b-88e4-903e5d3a2618",
                    "LayerId": "334608ed-74db-433c-917a-f223377b1661"
                }
            ]
        },
        {
            "id": "43a6a116-83e7-4116-9b7f-38563b42ffb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6abb60b9-c0aa-4dcd-8d01-b29cbd0d3727",
            "compositeImage": {
                "id": "9eeb4ff4-d587-47de-9f87-234e88693026",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43a6a116-83e7-4116-9b7f-38563b42ffb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d38c7a6-d657-4915-958f-2133850898ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43a6a116-83e7-4116-9b7f-38563b42ffb2",
                    "LayerId": "334608ed-74db-433c-917a-f223377b1661"
                }
            ]
        },
        {
            "id": "54c49ee6-e046-49f9-95e9-634b641012f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6abb60b9-c0aa-4dcd-8d01-b29cbd0d3727",
            "compositeImage": {
                "id": "2ab181ee-5112-482f-afa2-eeb51cbd01f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54c49ee6-e046-49f9-95e9-634b641012f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d6747ac-0253-4021-83a2-0d1739d2b2c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54c49ee6-e046-49f9-95e9-634b641012f1",
                    "LayerId": "334608ed-74db-433c-917a-f223377b1661"
                }
            ]
        },
        {
            "id": "fb11b909-d1be-4786-a0f9-ce43fd139879",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6abb60b9-c0aa-4dcd-8d01-b29cbd0d3727",
            "compositeImage": {
                "id": "83ea0281-f274-4513-9652-8bb3ada640ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb11b909-d1be-4786-a0f9-ce43fd139879",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e120f92c-0bfe-4080-ac12-f4f773dcd8b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb11b909-d1be-4786-a0f9-ce43fd139879",
                    "LayerId": "334608ed-74db-433c-917a-f223377b1661"
                }
            ]
        },
        {
            "id": "bd1bc08b-4344-4aab-ae57-357a0948e977",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6abb60b9-c0aa-4dcd-8d01-b29cbd0d3727",
            "compositeImage": {
                "id": "18d35cbf-7f10-4034-88b7-c71398bec1b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd1bc08b-4344-4aab-ae57-357a0948e977",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "672c195b-e3ea-49db-88a3-29c957eb79e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd1bc08b-4344-4aab-ae57-357a0948e977",
                    "LayerId": "334608ed-74db-433c-917a-f223377b1661"
                }
            ]
        },
        {
            "id": "96e8f59a-b9c3-4c4d-b445-7eeb28b35cae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6abb60b9-c0aa-4dcd-8d01-b29cbd0d3727",
            "compositeImage": {
                "id": "1e9b1cac-de93-421a-90fb-95a639a6c2d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96e8f59a-b9c3-4c4d-b445-7eeb28b35cae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd6df10d-32eb-4881-99da-e0a3798b9fcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96e8f59a-b9c3-4c4d-b445-7eeb28b35cae",
                    "LayerId": "334608ed-74db-433c-917a-f223377b1661"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "334608ed-74db-433c-917a-f223377b1661",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6abb60b9-c0aa-4dcd-8d01-b29cbd0d3727",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 145,
    "xorig": 24,
    "yorig": 24
}