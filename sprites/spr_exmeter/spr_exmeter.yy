{
    "id": "9ff96531-032a-4747-a0ef-519aab5fb29e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_exmeter",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 96,
    "bbox_left": 93,
    "bbox_right": 460,
    "bbox_top": 38,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "09906fc8-112c-4471-9c5f-03508b43dcd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ff96531-032a-4747-a0ef-519aab5fb29e",
            "compositeImage": {
                "id": "ada44a9e-5094-4258-aca8-d9f672c7bba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09906fc8-112c-4471-9c5f-03508b43dcd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8661f72c-1e3f-4451-a6f0-0fcf37e04fb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09906fc8-112c-4471-9c5f-03508b43dcd8",
                    "LayerId": "cbaf6a00-a0ec-48f6-b0c6-7a55eba0b950"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "cbaf6a00-a0ec-48f6-b0c6-7a55eba0b950",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9ff96531-032a-4747-a0ef-519aab5fb29e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 15,
    "yorig": 120
}