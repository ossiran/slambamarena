live_call();
if (heat > 50)
{
    if(state_new == 1)
    {
        with(hurtbox0)
    	{
    	    xoffset = -29;
    	    yoffset = -36;
    	    image_xscale  = 58;
    	    image_yscale  = 68;
    	    xshift  = -1;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -15;
    	    yoffset = -61;
    	    image_xscale  = 30;
    	    image_yscale  = 25;
    	    xshift = -3;
    	}
    	
    	
    	with(collisionbox)
    	{
    	    xoffset = -20;
    	    yoffset = -22;
    	    image_xscale  = 40;
    	    image_yscale  = 54;
    	    xshift = 0;
    	}
    	
    	chainmove = 0;
    	attackframe0 = 3;
    	endstate = 13;
    }
    
    //animation
    sprite_index = spr_momo_5l_fire;
    
    //attack
    if(state_timer == attackframe0)
    {
        hitbox1 = create_hitbox(10, -24,29, 24, 5.75,0, 8,14,8,10,0,0,2,3.5,"mid",1);
        attackhurtbox0 = create_hurtbox(8 * image_xscale,-26,28 * image_xscale,24,2,-2);
        hitbox1.scaling = 1.2;
        hitbox1.hitsound = snd_hitsound1;
        hitbox1.juggle = 2;
        
    }
    
    //chainmove
    if( state_timer > 2)
    {
        if(input_check(4,1,0,0,0) || input_check(5,1,0,0,0) || input_check(6,1,0,0,0))
        {
            chainmove = "light";
        }
        
        if(input_check(2,1,0,0,0) || input_check(1,1,0,0,0) || input_check(3,1,0,0,0))
        {
            chainmove = "clight";
        }
    
        if(input_check(2,1,0,0,0) || input_check(1,1,0,0,0) || input_check(3,1,0,0,0))
        {
            chainmove = "clight";
        }
        
        if(input_check(4,0,1,0,0) || input_check(5,0,1,0,0) || input_check(6,0,1,0,0))
        {
            chainmove = "medium";
        }
        
        if(input_check(2,0,1,0,0) || input_check(1,0,1,0,0) || input_check(3,0,1,0,0))
        {
            chainmove = "cmedium";
        }
        
        if(input_check(4,0,0,1,0) || input_check(5,0,0,1,0) || input_check(6,0,0,1,0))
        {
            chainmove = "heavy";
        }
        
        if(input_check(2,0,0,1,0) || input_check(1,0,0,1,0))
        {
            chainmove = "cheavy";
        }
    }
    
    
    attack_base(endstate,0,0,1,1,0);
    
    
    if(hitting == 1 && otherplayer.hitpause == 0 && otherplayer.hitstun < 12 && ((input_check(4,1,0,0,0) || input_check(5,1,0,0,0) || input_check(6,1,0,0,0)) || chainmove == "light"))
    {
        state_timer = 0;
        if(hitbox1 != -1)
        {
            instance_destroy(hitbox1);
        }
    }
}
else if(heat < -50)
{
    if(state_new == 1)
    {
        with(hurtbox0)
    	{
    	    xoffset = -29;
    	    yoffset = -36;
    	    image_xscale  = 58;
    	    image_yscale  = 68;
    	    xshift  = -1;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -15;
    	    yoffset = -61;
    	    image_xscale  = 30;
    	    image_yscale  = 25;
    	    xshift = -3;
    	}
    	
    	
    	with(collisionbox)
    	{
    	    xoffset = -20;
    	    yoffset = -22;
    	    image_xscale  = 40;
    	    image_yscale  = 54;
    	    xshift = 0;
    	}
    	
    	chainmove = 0;
    	attackframe0 = 3;
    	endstate = 13;
    }
    
    //animation
    sprite_index = spr_momo_5l_ice;
    
    //attack
    if(state_timer == attackframe0)
    {
        hitbox1 = create_hitbox(10, -24,29, 24, 5.75,0, 8,15,8,10,0,0,2,3.5,"mid",1);
        attackhurtbox0 = create_hurtbox(8 * image_xscale,-26,28 * image_xscale,24,2,-2);
        hitbox1.scaling = 1.2;
        hitbox1.hitsound = snd_hitsound1;
        hitbox1.juggle = 2;
        
    }
    
    //chainmove
    if( state_timer > 2)
    {
        if(input_check(4,1,0,0,0) || input_check(5,1,0,0,0) || input_check(6,1,0,0,0))
        {
            chainmove = "light";
        }
        
        if(input_check(2,1,0,0,0) || input_check(1,1,0,0,0) || input_check(3,1,0,0,0))
        {
            chainmove = "clight";
        }
    
        if(input_check(2,1,0,0,0) || input_check(1,1,0,0,0) || input_check(3,1,0,0,0))
        {
            chainmove = "clight";
        }
        
        if(input_check(4,0,1,0,0) || input_check(5,0,1,0,0) || input_check(6,0,1,0,0))
        {
            chainmove = "medium";
        }
        
        if(input_check(2,0,1,0,0) || input_check(1,0,1,0,0) || input_check(3,0,1,0,0))
        {
            chainmove = "cmedium";
        }
        
        if(input_check(4,0,0,1,0) || input_check(5,0,0,1,0) || input_check(6,0,0,1,0))
        {
            chainmove = "heavy";
        }
        
        if(input_check(2,0,0,1,0) || input_check(1,0,0,1,0))
        {
            chainmove = "cheavy";
        }
    }
    
    attack_base(endstate,0,0,1,1,0);
    
    
    if(hitting == 1 && otherplayer.hitpause == 0 && otherplayer.hitstun < 12 && ((input_check(4,1,0,0,0) || input_check(5,1,0,0,0) || input_check(6,1,0,0,0)) || chainmove == "light"))
    {
        state_timer = 0;
        if(hitbox1 != -1)
        {
            instance_destroy(hitbox1);
        }
    }
}
else
{
    if(state_new == 1)
    {
        with(hurtbox0)
    	{
    	    xoffset = -29;
    	    yoffset = -36;
    	    image_xscale  = 58;
    	    image_yscale  = 68;
    	    xshift  = -1;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -15;
    	    yoffset = -61;
    	    image_xscale  = 30;
    	    image_yscale  = 25;
    	    xshift = -3;
    	}
    	
    	
    	with(collisionbox)
    	{
    	    xoffset = -20;
    	    yoffset = -22;
    	    image_xscale  = 40;
    	    image_yscale  = 54;
    	    xshift = 0;
    	}
    	
    	chainmove = 0;
    	attackframe0 = 3;
    	endstate = 13;
    }
    
    //animation
    sprite_index = spr_momo_5l;
    
    //attack
    if(state_timer == attackframe0)
    {
        hitbox1 = create_hitbox(28, -24,47, 28, 5.75,0, 8,16,8,13,0,0,2,3.5,"mid",1);
        attackhurtbox0 = create_hurtbox(24 * image_xscale,-20,46 * image_xscale,32,3,-2);
        hitbox1.scaling = 1.2;
        hitbox1.hitsound = snd_hitsound1;
        hitbox1.juggle = 2;
        
    }
    
    //chainmove
    if( state_timer > 2)
    {
        if(input_check(4,1,0,0,0) || input_check(5,1,0,0,0) || input_check(6,1,0,0,0))
        {
            chainmove = "light";
        }
        
        if(input_check(2,1,0,0,0) || input_check(1,1,0,0,0) || input_check(3,1,0,0,0))
        {
            chainmove = "clight";
        }
    
        if(input_check(2,1,0,0,0) || input_check(1,1,0,0,0) || input_check(3,1,0,0,0))
        {
            chainmove = "clight";
        }
        
        if(input_check(4,0,1,0,0) || input_check(5,0,1,0,0) || input_check(6,0,1,0,0))
        {
            chainmove = "medium";
        }
        
        if(input_check(2,0,1,0,0) || input_check(1,0,1,0,0) || input_check(3,0,1,0,0))
        {
            chainmove = "cmedium";
        }
        
        if(input_check(4,0,0,1,0) || input_check(5,0,0,1,0) || input_check(6,0,0,1,0))
        {
            chainmove = "heavy";
        }
        
        if(input_check(2,0,0,1,0) || input_check(1,0,0,1,0))
        {
            chainmove = "cheavy";
        }
    }
    
    attack_base(endstate,0,0,1,1,0);
    
    
    if(hitting == 1 && otherplayer.hitpause == 0 && otherplayer.hitstun < 12 && ((input_check(4,1,0,0,0) || input_check(5,1,0,0,0) || input_check(6,1,0,0,0)) || chainmove == "light"))
    {
        state_timer = 0;
        if(hitbox1 != -1)
        {
            instance_destroy(hitbox1);
        }
    }
}
