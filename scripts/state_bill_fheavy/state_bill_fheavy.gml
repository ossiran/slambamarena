if(state_new == 1)
{
	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -43;
	    image_xscale  = 26;
	    image_yscale  = 75;
	    xshift  = -2;
	}
	
			with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 16;
	    image_yscale  = 47;
	    xshift = -2
	}
	chainmove = 0;
	attackframe0 = 0;
	attackframe1 = 9;
	attackframe2 = 6;
	endstate = 28;
}

if(state_timer > attackframe0 && state_timer < attackframe1)
{
    vx = 9.5 * image_xscale;
}



if(state_timer == attackframe1)
{
    with(otherplayer)
    {
        var launchpower = 0;
        if(!onground() && hitcounter > 0)
        {
            launchpower = 4;
        }
    }

    hitbox1 = create_hitbox(8,-42,29,38,4.85,-23 + launchpower,24,10,13,13,-1,0,3,6,"mid",-1);
    hitbox1.scaling = 1.25;
}

if(state_timer > attackframe2)
{
    vx = Approach(vx,0,0.5);
}

sprite_index = spr_bill_dashattack;

attack_base(endstate,0,0,1,1,0);