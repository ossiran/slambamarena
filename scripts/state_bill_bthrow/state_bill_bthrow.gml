if(state_new == 1)
{
	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -43;
	    image_xscale  = 26;
	    image_yscale  = 75;
	    xshift  = -2;
	}
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -1000;
	    image_xscale  = 0
	    image_yscale  = 0;
	    xshift = -2
	}
	
	otherplayer.x = x + 25 * image_xscale;
}

if(otherplayer.grabbed == -1 )
{
    state_switch("grabtech");
}

if(state_timer > 5 && state_timer < 12)
{
    otherplayer.x = Approach(otherplayer.x,x - 50 * image_xscale,7);
}

if(state_timer == 12 )
{
    with(otherplayer)
    {
        hitstun = 10;
        hitpause = 3;
        xhit = 5 * frontOrBack;
        yhit = -5;
        hp -= 25;
        hitfall = -1;
        grabbed = 0;
    }
}

if(state_timer == 35)
{
    state_switch("stand");
}