{
    "id": "2e378a18-70f8-4039-8044-361db2eba935",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_jab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 47,
    "bbox_right": 106,
    "bbox_top": 75,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4d6da5f3-6c4d-4691-bd8f-fe2ad4a98d0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e378a18-70f8-4039-8044-361db2eba935",
            "compositeImage": {
                "id": "dfd12ab3-e7c6-407d-ae38-2627621c6f47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d6da5f3-6c4d-4691-bd8f-fe2ad4a98d0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25b5684c-100a-4e75-aa94-edab948fcb59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d6da5f3-6c4d-4691-bd8f-fe2ad4a98d0d",
                    "LayerId": "d6aea311-eaba-49f0-a046-e5062d0f55d5"
                }
            ]
        },
        {
            "id": "24723276-8138-4630-8c81-b4a2644829a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e378a18-70f8-4039-8044-361db2eba935",
            "compositeImage": {
                "id": "f14d80c4-baba-4033-a247-8ad32557c5d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24723276-8138-4630-8c81-b4a2644829a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3c414a8-8a8c-482a-826b-c5a4e612274e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24723276-8138-4630-8c81-b4a2644829a1",
                    "LayerId": "d6aea311-eaba-49f0-a046-e5062d0f55d5"
                }
            ]
        },
        {
            "id": "14530d1f-95a7-475e-a54a-cc90fd7e4aff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e378a18-70f8-4039-8044-361db2eba935",
            "compositeImage": {
                "id": "97bebb5f-2c69-4681-b003-ac741b3cd41d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14530d1f-95a7-475e-a54a-cc90fd7e4aff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70b5afe5-c5ac-4536-bb73-6e7cdc8136e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14530d1f-95a7-475e-a54a-cc90fd7e4aff",
                    "LayerId": "d6aea311-eaba-49f0-a046-e5062d0f55d5"
                }
            ]
        },
        {
            "id": "f63c030e-83ac-417e-b594-7e1d6524df46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e378a18-70f8-4039-8044-361db2eba935",
            "compositeImage": {
                "id": "a683e811-eeb7-4d8f-8863-24e04d9b39e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f63c030e-83ac-417e-b594-7e1d6524df46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88979dfa-27b1-48f5-941f-d6238c0bda21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f63c030e-83ac-417e-b594-7e1d6524df46",
                    "LayerId": "d6aea311-eaba-49f0-a046-e5062d0f55d5"
                }
            ]
        },
        {
            "id": "f1f5c265-d0d0-40be-8b72-479a5917febc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e378a18-70f8-4039-8044-361db2eba935",
            "compositeImage": {
                "id": "215c6a90-32cc-4207-bcfb-f761ce993882",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1f5c265-d0d0-40be-8b72-479a5917febc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83de093f-1a2d-4fca-9a6e-f4db6cb79020",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1f5c265-d0d0-40be-8b72-479a5917febc",
                    "LayerId": "d6aea311-eaba-49f0-a046-e5062d0f55d5"
                }
            ]
        },
        {
            "id": "8da32dc0-3548-4b03-95ab-5b8d7b0cb13a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e378a18-70f8-4039-8044-361db2eba935",
            "compositeImage": {
                "id": "e11ee699-e7a0-4322-9a7e-6cba83bb1655",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8da32dc0-3548-4b03-95ab-5b8d7b0cb13a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a6ecb9d-1ea1-4041-a6ca-861b199bbe1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8da32dc0-3548-4b03-95ab-5b8d7b0cb13a",
                    "LayerId": "d6aea311-eaba-49f0-a046-e5062d0f55d5"
                }
            ]
        },
        {
            "id": "132a8a92-38b8-46a4-8956-6f5166e6274a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e378a18-70f8-4039-8044-361db2eba935",
            "compositeImage": {
                "id": "ed317f8c-367c-4b91-94a7-eaef66ef705a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "132a8a92-38b8-46a4-8956-6f5166e6274a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "217e951f-802c-4b47-9b92-85c1376d46f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "132a8a92-38b8-46a4-8956-6f5166e6274a",
                    "LayerId": "d6aea311-eaba-49f0-a046-e5062d0f55d5"
                }
            ]
        },
        {
            "id": "55bfcce7-5d88-48a7-b299-2e9731cbcb02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e378a18-70f8-4039-8044-361db2eba935",
            "compositeImage": {
                "id": "24c89bf3-59a3-489d-8486-103df86a01bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55bfcce7-5d88-48a7-b299-2e9731cbcb02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ac6ef20-e9d7-4337-960c-d37fb85a4fb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55bfcce7-5d88-48a7-b299-2e9731cbcb02",
                    "LayerId": "d6aea311-eaba-49f0-a046-e5062d0f55d5"
                }
            ]
        },
        {
            "id": "044d90f2-69c8-4e23-a8df-a98bda481db6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e378a18-70f8-4039-8044-361db2eba935",
            "compositeImage": {
                "id": "006de9b3-5fc7-4c3f-9387-570def69b34e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "044d90f2-69c8-4e23-a8df-a98bda481db6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f89ef409-74ca-4252-8858-8d52a628f0a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "044d90f2-69c8-4e23-a8df-a98bda481db6",
                    "LayerId": "d6aea311-eaba-49f0-a046-e5062d0f55d5"
                }
            ]
        },
        {
            "id": "473df2a9-e82a-4f65-97b9-65c55e9ea349",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e378a18-70f8-4039-8044-361db2eba935",
            "compositeImage": {
                "id": "1eebd6c2-fcd6-469d-a726-e833ccb91b57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "473df2a9-e82a-4f65-97b9-65c55e9ea349",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51735308-e3e7-4151-aaa8-71eca4b53aca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "473df2a9-e82a-4f65-97b9-65c55e9ea349",
                    "LayerId": "d6aea311-eaba-49f0-a046-e5062d0f55d5"
                }
            ]
        },
        {
            "id": "83218e96-fcb5-4026-84a5-2572fc1cd6f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e378a18-70f8-4039-8044-361db2eba935",
            "compositeImage": {
                "id": "9c4826ff-6f2e-4e00-a43a-0a748e6d7bc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83218e96-fcb5-4026-84a5-2572fc1cd6f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bcb6653-b552-4ab2-a5ff-4699c74a6955",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83218e96-fcb5-4026-84a5-2572fc1cd6f6",
                    "LayerId": "d6aea311-eaba-49f0-a046-e5062d0f55d5"
                }
            ]
        },
        {
            "id": "fe0267d4-b9e9-43e6-b415-c299f3dc98cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e378a18-70f8-4039-8044-361db2eba935",
            "compositeImage": {
                "id": "1debd991-e032-4901-af62-b0d2c5050804",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe0267d4-b9e9-43e6-b415-c299f3dc98cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f7e0b41-6534-48ca-82e2-1c27c912ee50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe0267d4-b9e9-43e6-b415-c299f3dc98cd",
                    "LayerId": "d6aea311-eaba-49f0-a046-e5062d0f55d5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "d6aea311-eaba-49f0-a046-e5062d0f55d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e378a18-70f8-4039-8044-361db2eba935",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}