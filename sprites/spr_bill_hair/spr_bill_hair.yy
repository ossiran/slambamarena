{
    "id": "3e98b8db-620f-48e2-b138-2b2acb6be225",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_hair",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 148,
    "bbox_left": 33,
    "bbox_right": 114,
    "bbox_top": 70,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7bed3172-7d3d-400b-aa83-a70e6dd21848",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "compositeImage": {
                "id": "0c23430a-52d8-4ae2-b825-5129afe1c7d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bed3172-7d3d-400b-aa83-a70e6dd21848",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8efd80c0-48aa-40c0-8c52-8c35483e79c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bed3172-7d3d-400b-aa83-a70e6dd21848",
                    "LayerId": "7c7c284a-8e80-4067-934a-38922a3c4128"
                }
            ]
        },
        {
            "id": "fd10a6a0-ce1b-4cad-9cbf-581f4c54c830",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "compositeImage": {
                "id": "38a52dbc-3ec5-4f57-a18f-2ab3a3dd755a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd10a6a0-ce1b-4cad-9cbf-581f4c54c830",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "271af678-4cfa-4249-976a-379448e844cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd10a6a0-ce1b-4cad-9cbf-581f4c54c830",
                    "LayerId": "7c7c284a-8e80-4067-934a-38922a3c4128"
                }
            ]
        },
        {
            "id": "cb3a4c05-70f0-4887-b470-31aaca329cf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "compositeImage": {
                "id": "7b20255f-a17a-44d0-8d20-71a5ea73ca2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb3a4c05-70f0-4887-b470-31aaca329cf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7ff1eea-a394-4133-ad22-169987ccffaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb3a4c05-70f0-4887-b470-31aaca329cf1",
                    "LayerId": "7c7c284a-8e80-4067-934a-38922a3c4128"
                }
            ]
        },
        {
            "id": "cd0ccc5b-e366-4026-b5c8-a4e75b68096f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "compositeImage": {
                "id": "5e58514a-c2bf-428e-9663-0f94be15636b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd0ccc5b-e366-4026-b5c8-a4e75b68096f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a8aa268-37ee-419b-9bfe-58baa753e8ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd0ccc5b-e366-4026-b5c8-a4e75b68096f",
                    "LayerId": "7c7c284a-8e80-4067-934a-38922a3c4128"
                }
            ]
        },
        {
            "id": "d7d6febe-957f-4ec1-98e8-432739a33357",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "compositeImage": {
                "id": "96dc64ba-95cc-461e-90c2-68fd1708dd56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7d6febe-957f-4ec1-98e8-432739a33357",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1185444-ace2-44ed-aabb-16451517409a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7d6febe-957f-4ec1-98e8-432739a33357",
                    "LayerId": "7c7c284a-8e80-4067-934a-38922a3c4128"
                }
            ]
        },
        {
            "id": "8ec6174e-c27d-4f58-abb1-4520627f92f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "compositeImage": {
                "id": "3f7c5b96-68f3-4c5e-845e-99678c923a21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ec6174e-c27d-4f58-abb1-4520627f92f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a53661fb-4141-46f9-b6de-45bb858abfba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ec6174e-c27d-4f58-abb1-4520627f92f4",
                    "LayerId": "7c7c284a-8e80-4067-934a-38922a3c4128"
                }
            ]
        },
        {
            "id": "e8718111-8efd-4f22-9dfa-d93656828734",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "compositeImage": {
                "id": "5dcbdcf5-d7f0-4f9e-8245-cd7e888963db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8718111-8efd-4f22-9dfa-d93656828734",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac9fcc78-0874-4538-8eec-f73326e87ae0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8718111-8efd-4f22-9dfa-d93656828734",
                    "LayerId": "7c7c284a-8e80-4067-934a-38922a3c4128"
                }
            ]
        },
        {
            "id": "37f0cf60-c256-4d9d-8875-f743885b296d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "compositeImage": {
                "id": "12619faa-cd0b-4c4c-84c5-b3a21f5df098",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37f0cf60-c256-4d9d-8875-f743885b296d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a205e298-5292-4aef-ab2d-02b0726e344d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37f0cf60-c256-4d9d-8875-f743885b296d",
                    "LayerId": "7c7c284a-8e80-4067-934a-38922a3c4128"
                }
            ]
        },
        {
            "id": "486be7c9-990b-4bda-b116-ae2fb2e87adf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "compositeImage": {
                "id": "4029283d-4c56-4218-b079-bd7b52e56850",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "486be7c9-990b-4bda-b116-ae2fb2e87adf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f9d8986-3ad3-4289-9a66-c9e2c4e865ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "486be7c9-990b-4bda-b116-ae2fb2e87adf",
                    "LayerId": "7c7c284a-8e80-4067-934a-38922a3c4128"
                }
            ]
        },
        {
            "id": "905d5b76-e047-4ee3-b1f9-008d1ee517fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "compositeImage": {
                "id": "49d25cac-c5c1-4c5d-8b57-c75e1a3db3cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "905d5b76-e047-4ee3-b1f9-008d1ee517fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "164608b0-013f-402b-9183-531303c6fbf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "905d5b76-e047-4ee3-b1f9-008d1ee517fc",
                    "LayerId": "7c7c284a-8e80-4067-934a-38922a3c4128"
                }
            ]
        },
        {
            "id": "2cd52cc0-36c1-43fb-8590-f3b83cb3d2b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "compositeImage": {
                "id": "a5194564-126f-4835-8213-3878d40782cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cd52cc0-36c1-43fb-8590-f3b83cb3d2b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9c0075d-1293-4112-a472-7d28328aa361",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cd52cc0-36c1-43fb-8590-f3b83cb3d2b2",
                    "LayerId": "7c7c284a-8e80-4067-934a-38922a3c4128"
                }
            ]
        },
        {
            "id": "db2a6f43-cd80-44b9-b575-6dff7790633e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "compositeImage": {
                "id": "f62517f9-6433-475f-be82-efbf46a18b9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db2a6f43-cd80-44b9-b575-6dff7790633e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2742ab0c-e1eb-46a9-bc8b-49412304c3dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db2a6f43-cd80-44b9-b575-6dff7790633e",
                    "LayerId": "7c7c284a-8e80-4067-934a-38922a3c4128"
                }
            ]
        },
        {
            "id": "bbd01367-b6ff-46f4-b58e-9f7c7c8619e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "compositeImage": {
                "id": "87a45189-5a16-4f77-b665-51ec135e9d45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbd01367-b6ff-46f4-b58e-9f7c7c8619e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "defb6c88-7152-4893-99a2-21e129bbd57a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbd01367-b6ff-46f4-b58e-9f7c7c8619e3",
                    "LayerId": "7c7c284a-8e80-4067-934a-38922a3c4128"
                }
            ]
        },
        {
            "id": "7220e0a0-318f-4a16-83e0-43d33a5922b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "compositeImage": {
                "id": "d0e4c9e3-23b5-44b8-b44c-65a7a0b89e8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7220e0a0-318f-4a16-83e0-43d33a5922b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "471a7f39-2a17-475a-b54a-6a0e762d8846",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7220e0a0-318f-4a16-83e0-43d33a5922b5",
                    "LayerId": "7c7c284a-8e80-4067-934a-38922a3c4128"
                }
            ]
        },
        {
            "id": "ad24d6a0-9c0c-485c-8263-3c49ae3ef9bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "compositeImage": {
                "id": "7a3ff6ca-0366-437d-a4b3-c0674f884612",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad24d6a0-9c0c-485c-8263-3c49ae3ef9bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc95b977-30aa-49a7-b37f-2e78007e6af3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad24d6a0-9c0c-485c-8263-3c49ae3ef9bd",
                    "LayerId": "7c7c284a-8e80-4067-934a-38922a3c4128"
                }
            ]
        },
        {
            "id": "4b370295-52d8-4dc5-8a3e-bf9dd4a51a4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "compositeImage": {
                "id": "9f2aa6bb-5f03-4642-9329-637f0cdfb13b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b370295-52d8-4dc5-8a3e-bf9dd4a51a4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81f5fde7-6841-469c-b356-61d9e533f13f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b370295-52d8-4dc5-8a3e-bf9dd4a51a4f",
                    "LayerId": "7c7c284a-8e80-4067-934a-38922a3c4128"
                }
            ]
        },
        {
            "id": "a608d147-f7a3-433d-a1b2-12c3f2773a07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "compositeImage": {
                "id": "da7eb439-8135-4ff3-ae8e-7efaf85d3abc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a608d147-f7a3-433d-a1b2-12c3f2773a07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9bcaf4b-bef8-472e-8d47-9a688d990e7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a608d147-f7a3-433d-a1b2-12c3f2773a07",
                    "LayerId": "7c7c284a-8e80-4067-934a-38922a3c4128"
                }
            ]
        },
        {
            "id": "49114108-948d-4b0f-801c-f2306a28a661",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "compositeImage": {
                "id": "f23ea98d-f639-4d90-823f-585cf0e82671",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49114108-948d-4b0f-801c-f2306a28a661",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38f3c743-b869-4897-9900-eff2c5c9c1ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49114108-948d-4b0f-801c-f2306a28a661",
                    "LayerId": "7c7c284a-8e80-4067-934a-38922a3c4128"
                }
            ]
        },
        {
            "id": "03f2dc2c-0ce1-4026-9f8c-eb1aacdac186",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "compositeImage": {
                "id": "b7d450f9-9685-49da-ad53-bb74a987b267",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03f2dc2c-0ce1-4026-9f8c-eb1aacdac186",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a62b1b5-bfca-43cd-ad0f-caa1b3cd0266",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03f2dc2c-0ce1-4026-9f8c-eb1aacdac186",
                    "LayerId": "7c7c284a-8e80-4067-934a-38922a3c4128"
                }
            ]
        },
        {
            "id": "a211ba33-bfdb-41d6-ac34-a48020c8fa05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "compositeImage": {
                "id": "28759e85-50f1-4f4d-b55c-a248f650b1c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a211ba33-bfdb-41d6-ac34-a48020c8fa05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9333a4a8-f232-4ca0-bc5c-21bb4cf3eaec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a211ba33-bfdb-41d6-ac34-a48020c8fa05",
                    "LayerId": "7c7c284a-8e80-4067-934a-38922a3c4128"
                }
            ]
        },
        {
            "id": "95b93763-95eb-4779-91f8-83cb7a33cf71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "compositeImage": {
                "id": "9e238eec-956b-4cce-9d5b-63a8e02b7fb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95b93763-95eb-4779-91f8-83cb7a33cf71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5a449a1-f64d-404d-a897-d0bb79180fde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95b93763-95eb-4779-91f8-83cb7a33cf71",
                    "LayerId": "7c7c284a-8e80-4067-934a-38922a3c4128"
                }
            ]
        },
        {
            "id": "bba25ef1-8962-4e51-807f-4227512ecceb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "compositeImage": {
                "id": "c66bbb7a-39b1-4df4-bc28-358de6fd57cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bba25ef1-8962-4e51-807f-4227512ecceb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b9fb6a4-1bae-41a3-b173-7d4921e4e137",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bba25ef1-8962-4e51-807f-4227512ecceb",
                    "LayerId": "7c7c284a-8e80-4067-934a-38922a3c4128"
                }
            ]
        },
        {
            "id": "4b8f56b6-dd30-4745-a38e-fe7b64ab78a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "compositeImage": {
                "id": "b18e2e2a-c436-4b84-b398-af484e24d494",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b8f56b6-dd30-4745-a38e-fe7b64ab78a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9115425a-15fa-4f27-a1d8-c34c12786dcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b8f56b6-dd30-4745-a38e-fe7b64ab78a7",
                    "LayerId": "7c7c284a-8e80-4067-934a-38922a3c4128"
                }
            ]
        },
        {
            "id": "ff71b212-1820-4c12-9917-9e6355a48cc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "compositeImage": {
                "id": "fefad91e-841a-4711-97b4-b146b544a563",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff71b212-1820-4c12-9917-9e6355a48cc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f3d8ce2-1edb-4148-ab47-6877f9e45a28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff71b212-1820-4c12-9917-9e6355a48cc2",
                    "LayerId": "7c7c284a-8e80-4067-934a-38922a3c4128"
                }
            ]
        },
        {
            "id": "f833a2e5-5688-4f17-837f-9f0faf327983",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "compositeImage": {
                "id": "98518f13-8393-4bf2-9cd4-15dfe5307ca0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f833a2e5-5688-4f17-837f-9f0faf327983",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36a9467b-edf8-4d35-94da-f0fa9560289f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f833a2e5-5688-4f17-837f-9f0faf327983",
                    "LayerId": "7c7c284a-8e80-4067-934a-38922a3c4128"
                }
            ]
        },
        {
            "id": "73267b1f-bcb9-4324-b4ca-692685709948",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "compositeImage": {
                "id": "c0aa2307-8f8c-42de-93f0-f8740924e381",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73267b1f-bcb9-4324-b4ca-692685709948",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce2225e1-3c55-45cc-9feb-664cf197da7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73267b1f-bcb9-4324-b4ca-692685709948",
                    "LayerId": "7c7c284a-8e80-4067-934a-38922a3c4128"
                }
            ]
        },
        {
            "id": "bb079f7f-61e2-4c27-a802-732c16e241ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "compositeImage": {
                "id": "38181aaa-d150-49d5-bc3c-f7fcadf006ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb079f7f-61e2-4c27-a802-732c16e241ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a25624d8-1b44-4c64-b25c-ccf8d978fa7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb079f7f-61e2-4c27-a802-732c16e241ea",
                    "LayerId": "7c7c284a-8e80-4067-934a-38922a3c4128"
                }
            ]
        },
        {
            "id": "a517a3ac-4b92-401b-a4e8-64823ed6cf8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "compositeImage": {
                "id": "df4f3294-89d3-4a6b-bdaa-0b1c75a7b107",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a517a3ac-4b92-401b-a4e8-64823ed6cf8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fee0c7b-4b6e-4a32-bf75-cc7497d0aa08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a517a3ac-4b92-401b-a4e8-64823ed6cf8f",
                    "LayerId": "7c7c284a-8e80-4067-934a-38922a3c4128"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "7c7c284a-8e80-4067-934a-38922a3c4128",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e98b8db-620f-48e2-b138-2b2acb6be225",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}