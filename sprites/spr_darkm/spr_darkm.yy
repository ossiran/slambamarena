{
    "id": "50c1f1f4-b5a0-47dc-a7ae-156a13445662",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_darkm",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 7,
    "bbox_right": 127,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7cbe8c78-f1af-4516-a123-f0f2aacba772",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c1f1f4-b5a0-47dc-a7ae-156a13445662",
            "compositeImage": {
                "id": "6c307754-f009-4786-8627-8d42c772cb60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cbe8c78-f1af-4516-a123-f0f2aacba772",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97d5f81b-c913-4801-b559-6f79c5970076",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cbe8c78-f1af-4516-a123-f0f2aacba772",
                    "LayerId": "7aab7e45-1ec8-4b56-a1bb-bee5f5c3d880"
                }
            ]
        },
        {
            "id": "41ae4ec0-21f8-4197-8f52-5f7d3155c414",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c1f1f4-b5a0-47dc-a7ae-156a13445662",
            "compositeImage": {
                "id": "c8ecb30d-77f6-4403-a5c8-cccb14716565",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41ae4ec0-21f8-4197-8f52-5f7d3155c414",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7369e49e-3c96-43fa-a839-11cef569ebdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41ae4ec0-21f8-4197-8f52-5f7d3155c414",
                    "LayerId": "7aab7e45-1ec8-4b56-a1bb-bee5f5c3d880"
                }
            ]
        },
        {
            "id": "fdeceedc-7336-4835-9b2d-858f8602927b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c1f1f4-b5a0-47dc-a7ae-156a13445662",
            "compositeImage": {
                "id": "d691ee7a-3d75-4752-96c2-eeb91985be4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdeceedc-7336-4835-9b2d-858f8602927b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f31a70a8-898f-4a27-a81e-96589ead16f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdeceedc-7336-4835-9b2d-858f8602927b",
                    "LayerId": "7aab7e45-1ec8-4b56-a1bb-bee5f5c3d880"
                }
            ]
        },
        {
            "id": "0d40bdf5-543f-46da-809d-0d1978c880a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c1f1f4-b5a0-47dc-a7ae-156a13445662",
            "compositeImage": {
                "id": "335de1e1-aae9-4a63-9b11-2a147fd1d530",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d40bdf5-543f-46da-809d-0d1978c880a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "821c4bc3-0bc7-43e7-bffb-ce80e95bcae4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d40bdf5-543f-46da-809d-0d1978c880a2",
                    "LayerId": "7aab7e45-1ec8-4b56-a1bb-bee5f5c3d880"
                }
            ]
        },
        {
            "id": "8d120817-5426-453c-9807-6a89d8d2eeb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c1f1f4-b5a0-47dc-a7ae-156a13445662",
            "compositeImage": {
                "id": "92d7648d-0628-4fd0-91ce-b347efd45b92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d120817-5426-453c-9807-6a89d8d2eeb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1383595-c125-493b-af76-b43f3975b0d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d120817-5426-453c-9807-6a89d8d2eeb9",
                    "LayerId": "7aab7e45-1ec8-4b56-a1bb-bee5f5c3d880"
                }
            ]
        },
        {
            "id": "ff7f0da6-154c-4103-a93a-fb3b74c57e26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c1f1f4-b5a0-47dc-a7ae-156a13445662",
            "compositeImage": {
                "id": "bde269aa-90c3-4581-8965-abc10cf020c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff7f0da6-154c-4103-a93a-fb3b74c57e26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba46e286-7b5d-4f3a-a9f7-30bbd115639b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff7f0da6-154c-4103-a93a-fb3b74c57e26",
                    "LayerId": "7aab7e45-1ec8-4b56-a1bb-bee5f5c3d880"
                }
            ]
        },
        {
            "id": "4e54897b-b968-458c-9112-5b0f4dda3cb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c1f1f4-b5a0-47dc-a7ae-156a13445662",
            "compositeImage": {
                "id": "5cd2ee9c-752b-4b95-98c1-52bca8a6819d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e54897b-b968-458c-9112-5b0f4dda3cb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65d2416e-3993-4433-ae9d-c1ff96e97c2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e54897b-b968-458c-9112-5b0f4dda3cb4",
                    "LayerId": "7aab7e45-1ec8-4b56-a1bb-bee5f5c3d880"
                }
            ]
        },
        {
            "id": "aa6dd345-fe05-4877-8117-21b4ba56d438",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c1f1f4-b5a0-47dc-a7ae-156a13445662",
            "compositeImage": {
                "id": "f2daac02-f98d-4719-96c8-bcee7696c93c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa6dd345-fe05-4877-8117-21b4ba56d438",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81d90553-f3ad-4018-ab18-ecadde12ad3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa6dd345-fe05-4877-8117-21b4ba56d438",
                    "LayerId": "7aab7e45-1ec8-4b56-a1bb-bee5f5c3d880"
                }
            ]
        },
        {
            "id": "b874e64f-9f32-4383-9f8c-133342c06bcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c1f1f4-b5a0-47dc-a7ae-156a13445662",
            "compositeImage": {
                "id": "88ac625d-6d58-44fc-8b1c-9cd1a3837479",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b874e64f-9f32-4383-9f8c-133342c06bcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae73afef-767b-4ea9-abc1-ff0de2b71f86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b874e64f-9f32-4383-9f8c-133342c06bcd",
                    "LayerId": "7aab7e45-1ec8-4b56-a1bb-bee5f5c3d880"
                }
            ]
        },
        {
            "id": "78b2cdc2-c280-4899-bf1b-1b766bc3289b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c1f1f4-b5a0-47dc-a7ae-156a13445662",
            "compositeImage": {
                "id": "ffa01352-48b2-437a-9c15-9f4e337b13d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78b2cdc2-c280-4899-bf1b-1b766bc3289b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d54e2dd-63f7-4dde-9a46-7acb0f9eb2ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78b2cdc2-c280-4899-bf1b-1b766bc3289b",
                    "LayerId": "7aab7e45-1ec8-4b56-a1bb-bee5f5c3d880"
                }
            ]
        },
        {
            "id": "3762306c-ac5a-419d-8402-8e178b1b8bd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c1f1f4-b5a0-47dc-a7ae-156a13445662",
            "compositeImage": {
                "id": "622b665b-d587-41c1-8a02-1e0eb0b9aa91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3762306c-ac5a-419d-8402-8e178b1b8bd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02dc88e8-662b-4451-9d0e-b837e44b52a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3762306c-ac5a-419d-8402-8e178b1b8bd4",
                    "LayerId": "7aab7e45-1ec8-4b56-a1bb-bee5f5c3d880"
                }
            ]
        },
        {
            "id": "fb2eefad-b433-400f-91b3-ded50baada2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c1f1f4-b5a0-47dc-a7ae-156a13445662",
            "compositeImage": {
                "id": "5d39e193-b0f0-4965-92a9-6ee8829732eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb2eefad-b433-400f-91b3-ded50baada2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54c9f215-b8a5-4120-9bb8-48e56a034eb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb2eefad-b433-400f-91b3-ded50baada2a",
                    "LayerId": "7aab7e45-1ec8-4b56-a1bb-bee5f5c3d880"
                }
            ]
        },
        {
            "id": "2daa6b1f-40cd-4330-91e2-6562d24c4127",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c1f1f4-b5a0-47dc-a7ae-156a13445662",
            "compositeImage": {
                "id": "dae40cd5-40fb-4564-a0b4-8390519d6462",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2daa6b1f-40cd-4330-91e2-6562d24c4127",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9a94685-937d-40d6-a768-04d634fbebde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2daa6b1f-40cd-4330-91e2-6562d24c4127",
                    "LayerId": "7aab7e45-1ec8-4b56-a1bb-bee5f5c3d880"
                }
            ]
        },
        {
            "id": "a30d78f4-f1df-46ac-bbe1-d07fa2e71a4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c1f1f4-b5a0-47dc-a7ae-156a13445662",
            "compositeImage": {
                "id": "3449cd8e-3950-4fa3-8080-9d4eb64d1753",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a30d78f4-f1df-46ac-bbe1-d07fa2e71a4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "489344f3-a441-4617-a745-ba43af9fbe34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a30d78f4-f1df-46ac-bbe1-d07fa2e71a4d",
                    "LayerId": "7aab7e45-1ec8-4b56-a1bb-bee5f5c3d880"
                }
            ]
        },
        {
            "id": "e67a6329-0cd3-4b61-91e2-c4801d5b6a16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c1f1f4-b5a0-47dc-a7ae-156a13445662",
            "compositeImage": {
                "id": "b1d3b7f0-ce16-443e-91fb-5532d5ccd735",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e67a6329-0cd3-4b61-91e2-c4801d5b6a16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3e24af5-ff5e-4f0b-916f-4fb5ce4e89b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e67a6329-0cd3-4b61-91e2-c4801d5b6a16",
                    "LayerId": "7aab7e45-1ec8-4b56-a1bb-bee5f5c3d880"
                }
            ]
        },
        {
            "id": "91aa4f08-e8c4-401f-a40c-94cd91f6820e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c1f1f4-b5a0-47dc-a7ae-156a13445662",
            "compositeImage": {
                "id": "99c52c06-ce57-4202-8219-ecd7ed39e62a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91aa4f08-e8c4-401f-a40c-94cd91f6820e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "649c9b61-ecbc-49e7-ab41-567b90f4622a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91aa4f08-e8c4-401f-a40c-94cd91f6820e",
                    "LayerId": "7aab7e45-1ec8-4b56-a1bb-bee5f5c3d880"
                }
            ]
        },
        {
            "id": "f9627a95-2cc6-480c-8ecb-1027a38b5689",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c1f1f4-b5a0-47dc-a7ae-156a13445662",
            "compositeImage": {
                "id": "6737f983-7c51-4d07-b830-ea7aec9abaed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9627a95-2cc6-480c-8ecb-1027a38b5689",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fa5d19e-2dae-41a4-9c76-b966a9e18f62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9627a95-2cc6-480c-8ecb-1027a38b5689",
                    "LayerId": "7aab7e45-1ec8-4b56-a1bb-bee5f5c3d880"
                }
            ]
        },
        {
            "id": "78fa6b42-c9ff-45cc-ab9c-7ab0657941b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c1f1f4-b5a0-47dc-a7ae-156a13445662",
            "compositeImage": {
                "id": "a3332d40-8e79-49cc-9e28-982fba698b89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78fa6b42-c9ff-45cc-ab9c-7ab0657941b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f11e483b-a3fb-4a5e-b179-f50e2d31644d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78fa6b42-c9ff-45cc-ab9c-7ab0657941b6",
                    "LayerId": "7aab7e45-1ec8-4b56-a1bb-bee5f5c3d880"
                }
            ]
        },
        {
            "id": "127eb4a2-adb7-437f-890d-35c1a8ffac99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c1f1f4-b5a0-47dc-a7ae-156a13445662",
            "compositeImage": {
                "id": "ca502eef-9ab8-4bd5-bbfb-51f6e1cc36bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "127eb4a2-adb7-437f-890d-35c1a8ffac99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "367fbd0d-43bf-4f8b-addd-533706bd860b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "127eb4a2-adb7-437f-890d-35c1a8ffac99",
                    "LayerId": "7aab7e45-1ec8-4b56-a1bb-bee5f5c3d880"
                }
            ]
        },
        {
            "id": "82786c18-29a2-48ea-b73d-d5edc23235be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c1f1f4-b5a0-47dc-a7ae-156a13445662",
            "compositeImage": {
                "id": "9522c727-e86f-459c-8771-d4167492657b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82786c18-29a2-48ea-b73d-d5edc23235be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40919bda-addb-40aa-b26a-cbf0d1d19a26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82786c18-29a2-48ea-b73d-d5edc23235be",
                    "LayerId": "7aab7e45-1ec8-4b56-a1bb-bee5f5c3d880"
                }
            ]
        },
        {
            "id": "6fc420c6-3b8f-401e-9e7f-8ad01bce36f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c1f1f4-b5a0-47dc-a7ae-156a13445662",
            "compositeImage": {
                "id": "c28c334d-9994-494d-8e30-09f73ce153f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fc420c6-3b8f-401e-9e7f-8ad01bce36f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7313353-934d-4d80-9b24-c79332baf655",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fc420c6-3b8f-401e-9e7f-8ad01bce36f0",
                    "LayerId": "7aab7e45-1ec8-4b56-a1bb-bee5f5c3d880"
                }
            ]
        },
        {
            "id": "87b6d89b-c59b-4cc5-b7ad-fcf916b7f548",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c1f1f4-b5a0-47dc-a7ae-156a13445662",
            "compositeImage": {
                "id": "5fe28b64-0295-470a-9019-60af08f1c4be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87b6d89b-c59b-4cc5-b7ad-fcf916b7f548",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f3741c5-bf95-415a-9217-cd0d6973f1d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87b6d89b-c59b-4cc5-b7ad-fcf916b7f548",
                    "LayerId": "7aab7e45-1ec8-4b56-a1bb-bee5f5c3d880"
                }
            ]
        },
        {
            "id": "f0fd3db4-f1f9-4d16-adcc-6c63aa5f4d19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c1f1f4-b5a0-47dc-a7ae-156a13445662",
            "compositeImage": {
                "id": "59aa2bed-11cb-459d-96b0-ee12a797830b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0fd3db4-f1f9-4d16-adcc-6c63aa5f4d19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8112f3b-20ba-4351-8056-1bb3b8ff695f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0fd3db4-f1f9-4d16-adcc-6c63aa5f4d19",
                    "LayerId": "7aab7e45-1ec8-4b56-a1bb-bee5f5c3d880"
                }
            ]
        },
        {
            "id": "b17a7cff-7757-43f5-ac6e-84d9c4862e19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c1f1f4-b5a0-47dc-a7ae-156a13445662",
            "compositeImage": {
                "id": "dd2b1414-a46f-4c21-9ba1-b998fe373401",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b17a7cff-7757-43f5-ac6e-84d9c4862e19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c8988d7-2db9-487f-9b5a-ec3594fc1cd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b17a7cff-7757-43f5-ac6e-84d9c4862e19",
                    "LayerId": "7aab7e45-1ec8-4b56-a1bb-bee5f5c3d880"
                }
            ]
        },
        {
            "id": "cf9414c3-07b9-4652-97f6-42f056f990d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c1f1f4-b5a0-47dc-a7ae-156a13445662",
            "compositeImage": {
                "id": "06109dad-0edf-4565-8582-6c3729a509be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf9414c3-07b9-4652-97f6-42f056f990d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "177ad212-f598-4328-9cd5-84dc3dd789ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf9414c3-07b9-4652-97f6-42f056f990d2",
                    "LayerId": "7aab7e45-1ec8-4b56-a1bb-bee5f5c3d880"
                }
            ]
        },
        {
            "id": "5f8bfe18-1922-4f01-9516-d4e0e5680584",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c1f1f4-b5a0-47dc-a7ae-156a13445662",
            "compositeImage": {
                "id": "9dac7baf-ae72-4cd6-b4c5-35570775c9d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f8bfe18-1922-4f01-9516-d4e0e5680584",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f067fea-19f2-4049-b7c7-a52bd0b8deb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f8bfe18-1922-4f01-9516-d4e0e5680584",
                    "LayerId": "7aab7e45-1ec8-4b56-a1bb-bee5f5c3d880"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "7aab7e45-1ec8-4b56-a1bb-bee5f5c3d880",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50c1f1f4-b5a0-47dc-a7ae-156a13445662",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 145,
    "xorig": 24,
    "yorig": 24
}