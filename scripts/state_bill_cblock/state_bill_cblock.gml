if(state_new == 1)
{
    	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -28;
	    xscale  = 26;
	    yscale  = 60;
	    xshift  = -2;
	}
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 16;
	    image_yscale  = 47;
	    xshift = -2
	}
	
	if(abs(xhit) < 2)
	{
	    xhit = 3 * sign(xhit);
	}
}
	

//switch to cblock if cblocking
if(blockstun > blockstunwas)
{
    if(key_down == 0)
    {
        state_switch("sblock");
    }
    
    if(abs(xhit) < 2)
	{
	    xhit = 3 * sign(xhit);
	}
}


sprite_index = spr_bill_cblock;

//timer
if(blockstun > 0 && hitpause == 0)
{
    blockstun--;
    blockstunwas = blockstun;
    vx = xhit/2;
    otherplayer.vx += -xhit/3;
    xhit = xhit * 0.85;
}

if(blockstun = 0)
{
    state_switch("crouch");
    xhit = 0;
}

blockstunwas =  blockstun;

//hitstun
if(hitstun > 0)
{
    state_switch("hitstun");
    blockstun = 0;
    blockstunwas = 0;
}

if(hitpause > 0 && hitstun == 0)
{
    hitpause --;
}