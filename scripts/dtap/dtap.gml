/// @param dircheckdir
///dtap(direction)

var dircheckdir = argument0;
    
    if(sign(image_xscale) = -1)
    {
        switch (dircheckdir)
        {
            case 5:
                break;
            case 8:
                break;
            case 2:
                break;
            case 4:
                dircheckdir = 6;
                break;
            case 6:
                dircheckdir = 4;
                break;
            case 1:
                dircheckdir = 3;
                break;
            case 3:
                dircheckdir = 1;
                break;
            case 7:
                dircheckdir = 9;
                break;
            case 9:
                dircheckdir = 7;
                break;
        }
    }


// May need to make it better and not only check for 5. could pose a problem for keyboard inputs that dont pass through 5
for(i = 12; i > 0; i -= 1)
{
    if(input[wrap(inputcounter - i, 0, 300), 0] == 5 )
    {
        for(i2 = 0; i2 < i; i2 += 1)
        {
            if(input[wrap(inputcounter - i + i2, 0, 300), 0] == dircheckdir)
            {
                for(i3 = 0; i3 < i - i2; i3 += 1)
                {
                    if(input[wrap(inputcounter - i + i2 + i3,0 , 300), 0] == 5)
                    {
                        for(i4 = 0; i4 < i - i2 - i3; i4 += 1)
                        {
                            if(input[wrap(inputcounter - i + i2 + i3 + i4,0 , 300), 0] != dircheckdir && input[wrap(inputcounter - i + i2 + i3,0 , 300), 0] != 5)
                            {
                                return 0;
        						break;
                            }
                            
                            if(input[wrap(inputcounter - i + i2 + i3 + i4,0 , 300), 0] == dircheckdir)
                            {
                                return 1;
        						break;
                            }
                        }
                    }
                }
            }
        }
    }
}