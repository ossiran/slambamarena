{
    "id": "6f710e96-e0e2-4cb7-acc2-9e9b5ce326a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_jumpD",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 52,
    "bbox_right": 88,
    "bbox_top": 65,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "62360fcd-bf37-4080-88c5-bd1dbabfc82c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f710e96-e0e2-4cb7-acc2-9e9b5ce326a4",
            "compositeImage": {
                "id": "76731ac8-0c3f-4168-8a30-75a302ef18e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62360fcd-bf37-4080-88c5-bd1dbabfc82c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fa6593e-103a-4b5f-bf0f-05f203ecc8f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62360fcd-bf37-4080-88c5-bd1dbabfc82c",
                    "LayerId": "13f5ebcb-a7af-43f0-911b-6ecac3c9a1cd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "13f5ebcb-a7af-43f0-911b-6ecac3c9a1cd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f710e96-e0e2-4cb7-acc2-9e9b5ce326a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 68,
    "yorig": 116
}