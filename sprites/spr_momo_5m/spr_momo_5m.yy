{
    "id": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_5m",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 16,
    "bbox_right": 151,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e9a1eda6-c466-4f02-b6a1-bc1b006be73f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "compositeImage": {
                "id": "b8fb8814-84a6-48c2-bccf-1d009b40606d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9a1eda6-c466-4f02-b6a1-bc1b006be73f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8179c94b-6b42-435c-a6a2-454c3e0e5f95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9a1eda6-c466-4f02-b6a1-bc1b006be73f",
                    "LayerId": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d"
                }
            ]
        },
        {
            "id": "619853b9-c655-4aec-922a-6fb3c99260df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "compositeImage": {
                "id": "5b8d3cfd-2824-484f-b9b0-a40e6b604d71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "619853b9-c655-4aec-922a-6fb3c99260df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b797f3b-32d4-4331-be46-8ddcce9e5e48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "619853b9-c655-4aec-922a-6fb3c99260df",
                    "LayerId": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d"
                }
            ]
        },
        {
            "id": "a4ad3105-73f6-4b58-a571-ffe55c7ac81b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "compositeImage": {
                "id": "544450a0-9282-47a1-a562-8ee1b747de14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4ad3105-73f6-4b58-a571-ffe55c7ac81b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bb10e91-773b-45c2-a5c3-2c8b95787723",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4ad3105-73f6-4b58-a571-ffe55c7ac81b",
                    "LayerId": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d"
                }
            ]
        },
        {
            "id": "bb7ee83a-a471-44af-b374-f1519fd70662",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "compositeImage": {
                "id": "27a8e579-5391-4899-bb7e-8d0bd48515b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb7ee83a-a471-44af-b374-f1519fd70662",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a11d01f9-4a34-4804-8c52-45ef0235fb37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb7ee83a-a471-44af-b374-f1519fd70662",
                    "LayerId": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d"
                }
            ]
        },
        {
            "id": "8b1715e6-919b-449e-93e0-1dc167351610",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "compositeImage": {
                "id": "6da586ae-fd8f-4fda-80a8-924904003f3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b1715e6-919b-449e-93e0-1dc167351610",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "785465b4-bff3-46d9-aa0b-267216d04715",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b1715e6-919b-449e-93e0-1dc167351610",
                    "LayerId": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d"
                }
            ]
        },
        {
            "id": "8c131bb4-dd27-4337-b936-70a4474b2de7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "compositeImage": {
                "id": "f571fc7b-d8fc-4877-88e9-9f9cdf8f499e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c131bb4-dd27-4337-b936-70a4474b2de7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d56ce8b-d36b-4a4d-8467-13938791f053",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c131bb4-dd27-4337-b936-70a4474b2de7",
                    "LayerId": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d"
                }
            ]
        },
        {
            "id": "bd8e4300-6b32-4c15-b31f-36b143eb6348",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "compositeImage": {
                "id": "6254366b-16fe-48a4-b89e-602b29949c15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd8e4300-6b32-4c15-b31f-36b143eb6348",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ad9715d-7af4-4d6e-afc1-fd44e8b1dc2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd8e4300-6b32-4c15-b31f-36b143eb6348",
                    "LayerId": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d"
                }
            ]
        },
        {
            "id": "8c763c59-bbc8-4458-81f1-49e3d0dec6f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "compositeImage": {
                "id": "d7b9e366-63f2-4064-8b00-55b8a72e6869",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c763c59-bbc8-4458-81f1-49e3d0dec6f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78da622c-dddb-4e9c-b343-e6bb3574b79c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c763c59-bbc8-4458-81f1-49e3d0dec6f5",
                    "LayerId": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d"
                }
            ]
        },
        {
            "id": "b82fb747-00aa-47fb-a502-bd8e07a4fda2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "compositeImage": {
                "id": "abbcca56-f563-4abe-9f5c-19ef36260e41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b82fb747-00aa-47fb-a502-bd8e07a4fda2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95b764e3-f14b-4751-89aa-0913b1903a40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b82fb747-00aa-47fb-a502-bd8e07a4fda2",
                    "LayerId": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d"
                }
            ]
        },
        {
            "id": "8ab2e651-8af7-4022-840d-063d25bb37c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "compositeImage": {
                "id": "f11c784c-1188-40d6-a90c-af607514a7a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ab2e651-8af7-4022-840d-063d25bb37c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "170d260a-c85e-4dbb-b785-ec9489e21d9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ab2e651-8af7-4022-840d-063d25bb37c5",
                    "LayerId": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d"
                }
            ]
        },
        {
            "id": "d5c2e83f-1128-4355-b3dd-8544931f2080",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "compositeImage": {
                "id": "b2d81917-0218-48a2-85aa-59f39c37b890",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5c2e83f-1128-4355-b3dd-8544931f2080",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5e65c4d-679a-48ee-9c83-b02858ec39cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5c2e83f-1128-4355-b3dd-8544931f2080",
                    "LayerId": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d"
                }
            ]
        },
        {
            "id": "27c7e5ee-b8c7-4dba-85f0-de90395b4c32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "compositeImage": {
                "id": "b377da9b-12e5-4496-8a59-6a3f7d3944cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27c7e5ee-b8c7-4dba-85f0-de90395b4c32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9026971e-d520-4d3a-8dc9-7ac7801d8054",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27c7e5ee-b8c7-4dba-85f0-de90395b4c32",
                    "LayerId": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d"
                }
            ]
        },
        {
            "id": "961bc733-3c37-4f51-bef4-566971446473",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "compositeImage": {
                "id": "09c3a318-2e85-433c-91d0-eb42efb7aab1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "961bc733-3c37-4f51-bef4-566971446473",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "025cb5a1-c80d-44f5-9bb5-0cf4008a41d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "961bc733-3c37-4f51-bef4-566971446473",
                    "LayerId": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d"
                }
            ]
        },
        {
            "id": "98d4a348-81fb-4f5a-b62a-1a72982a3cc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "compositeImage": {
                "id": "a61c169a-41bf-4fd1-ac57-62ce8bd756bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98d4a348-81fb-4f5a-b62a-1a72982a3cc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b72c7033-652b-4a30-911e-c0eb609aaf22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98d4a348-81fb-4f5a-b62a-1a72982a3cc1",
                    "LayerId": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d"
                }
            ]
        },
        {
            "id": "d12073fa-01b9-4f3f-9341-6fdf29053e50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "compositeImage": {
                "id": "ab428aa3-9228-42e3-a807-8141d922466b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d12073fa-01b9-4f3f-9341-6fdf29053e50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91f4cdae-9510-4e07-aaa3-8d685ed129ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d12073fa-01b9-4f3f-9341-6fdf29053e50",
                    "LayerId": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d"
                }
            ]
        },
        {
            "id": "4bbc4828-d061-493e-b15d-82cd075287c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "compositeImage": {
                "id": "73f72554-d285-46af-9af5-0885a8c80dde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bbc4828-d061-493e-b15d-82cd075287c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d99bce45-c58e-4432-84b7-17f2758cb8ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bbc4828-d061-493e-b15d-82cd075287c6",
                    "LayerId": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d"
                }
            ]
        },
        {
            "id": "a4cc9feb-5830-4949-aa16-348d353d1e09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "compositeImage": {
                "id": "b8d15629-fdbc-460c-9f9e-fb5978560899",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4cc9feb-5830-4949-aa16-348d353d1e09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aed7cf87-cb84-4f9f-a771-2ebeff2ed365",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4cc9feb-5830-4949-aa16-348d353d1e09",
                    "LayerId": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d"
                }
            ]
        },
        {
            "id": "57679736-8124-4237-853b-be94a0f5a899",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "compositeImage": {
                "id": "2f892881-53b9-4ae2-8daf-acbc81a11990",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57679736-8124-4237-853b-be94a0f5a899",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b72c4768-71f9-4f9a-8cc5-0ea6f1ae6695",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57679736-8124-4237-853b-be94a0f5a899",
                    "LayerId": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d"
                }
            ]
        },
        {
            "id": "f261b9d8-dedf-49d0-85e0-726f773015c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "compositeImage": {
                "id": "2a824e8d-4aa4-4000-8b3f-38ba55d60483",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f261b9d8-dedf-49d0-85e0-726f773015c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02d2dcf5-ffaf-4b28-b98a-d773e1cb9565",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f261b9d8-dedf-49d0-85e0-726f773015c0",
                    "LayerId": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d"
                }
            ]
        },
        {
            "id": "5e797e35-17a3-4484-a1d2-85c952834798",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "compositeImage": {
                "id": "51799222-265f-4452-8fd7-50f570ff84c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e797e35-17a3-4484-a1d2-85c952834798",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9112c8c-b6e1-4d23-9370-d42d8753c97d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e797e35-17a3-4484-a1d2-85c952834798",
                    "LayerId": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d"
                }
            ]
        },
        {
            "id": "501dd2ad-96e7-41f2-96c8-27cafaf5e3dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "compositeImage": {
                "id": "742a7a81-f754-404a-89a4-34acd215709e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "501dd2ad-96e7-41f2-96c8-27cafaf5e3dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11edc19a-2ead-42a1-bcbd-20538db73089",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "501dd2ad-96e7-41f2-96c8-27cafaf5e3dc",
                    "LayerId": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d"
                }
            ]
        },
        {
            "id": "a41cbb0f-3328-4bc0-9fc3-80f455ecae53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "compositeImage": {
                "id": "8f063952-84ba-420b-a6e9-84fc22a26633",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a41cbb0f-3328-4bc0-9fc3-80f455ecae53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a369cfa-1758-40ce-b2c3-196cdcf3ff1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a41cbb0f-3328-4bc0-9fc3-80f455ecae53",
                    "LayerId": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d"
                }
            ]
        },
        {
            "id": "e1b9dca6-a288-4422-861f-559feadc4ccc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "compositeImage": {
                "id": "b62bf5bd-0bb3-4b57-a120-5d8c76995737",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1b9dca6-a288-4422-861f-559feadc4ccc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2d59562-5cc4-43b5-a68e-47be49709771",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1b9dca6-a288-4422-861f-559feadc4ccc",
                    "LayerId": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d"
                }
            ]
        },
        {
            "id": "09ffc195-acb6-45cb-b999-9dca230d592c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "compositeImage": {
                "id": "26257184-61b5-41b9-8ee3-4c8409069bee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09ffc195-acb6-45cb-b999-9dca230d592c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b7cbfc9-3e26-46f7-b302-53fc8016c42b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09ffc195-acb6-45cb-b999-9dca230d592c",
                    "LayerId": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d"
                }
            ]
        },
        {
            "id": "5b3425ff-3b00-42f3-aadc-62ceefe359d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "compositeImage": {
                "id": "4a299aad-6e93-4a80-af68-7e727c275016",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b3425ff-3b00-42f3-aadc-62ceefe359d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8591ccd5-4e53-41c2-b8f1-5f9b2d0ab91e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b3425ff-3b00-42f3-aadc-62ceefe359d5",
                    "LayerId": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d"
                }
            ]
        },
        {
            "id": "d338b2db-8a02-4986-be70-834646a5f27e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "compositeImage": {
                "id": "5ee80297-f4e8-41f4-a5db-0e677f4dff99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d338b2db-8a02-4986-be70-834646a5f27e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7b92622-7917-4d2e-882e-651eb5024457",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d338b2db-8a02-4986-be70-834646a5f27e",
                    "LayerId": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d"
                }
            ]
        },
        {
            "id": "457d03b4-08aa-4742-b35a-ad1d0c5365d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "compositeImage": {
                "id": "392dcfe7-7cac-430d-b8b6-5af1f2c40992",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "457d03b4-08aa-4742-b35a-ad1d0c5365d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "072057e4-8d45-4fc8-a39a-35db6c7b9103",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "457d03b4-08aa-4742-b35a-ad1d0c5365d2",
                    "LayerId": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d"
                }
            ]
        },
        {
            "id": "91236343-90f1-46a7-a431-225232fa9d5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "compositeImage": {
                "id": "38330aeb-b0d5-47b1-b681-d187633ad930",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91236343-90f1-46a7-a431-225232fa9d5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de2d80b4-a0bc-4705-9273-f499d98a8cd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91236343-90f1-46a7-a431-225232fa9d5d",
                    "LayerId": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "6742e2c7-6a5a-420e-acb3-04e6ce028b4d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f0ea1e3-54d0-486c-bfe1-5ac8e396d694",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 63,
    "yorig": 87
}