{
    "id": "52a429ee-8396-4e32-bf4e-554ef967190a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_cheavy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 48,
    "bbox_right": 113,
    "bbox_top": 91,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4c2ab64-69af-43c2-adaf-d09346a9c0f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "compositeImage": {
                "id": "3905683a-7b89-41b4-9f15-677551929f63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4c2ab64-69af-43c2-adaf-d09346a9c0f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8691393e-2d14-440a-b6ab-c3c134d4a62c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4c2ab64-69af-43c2-adaf-d09346a9c0f7",
                    "LayerId": "be6f14da-cbe0-43c0-ac04-a4450afbea61"
                }
            ]
        },
        {
            "id": "ea0f1930-2323-4b50-8a17-76b9e296e3e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "compositeImage": {
                "id": "ebaede5f-01df-4833-b623-fb80dd0eb0ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea0f1930-2323-4b50-8a17-76b9e296e3e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1f9104e-822c-4b87-82ab-90b42735d026",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea0f1930-2323-4b50-8a17-76b9e296e3e2",
                    "LayerId": "be6f14da-cbe0-43c0-ac04-a4450afbea61"
                }
            ]
        },
        {
            "id": "6272dffb-c006-48ee-80fe-fbc693de5673",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "compositeImage": {
                "id": "817dad2c-41e3-4f01-9366-8104d7e51299",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6272dffb-c006-48ee-80fe-fbc693de5673",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "892a6816-9f79-4b31-a6d5-eab2beb8ec76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6272dffb-c006-48ee-80fe-fbc693de5673",
                    "LayerId": "be6f14da-cbe0-43c0-ac04-a4450afbea61"
                }
            ]
        },
        {
            "id": "dd6cff5b-7cc5-4d7f-be88-b9ec688c7f71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "compositeImage": {
                "id": "27541735-5af4-4cb8-b170-ef3796a52c8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd6cff5b-7cc5-4d7f-be88-b9ec688c7f71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb881b11-dac2-4a02-9a71-33cd0f8b6e6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd6cff5b-7cc5-4d7f-be88-b9ec688c7f71",
                    "LayerId": "be6f14da-cbe0-43c0-ac04-a4450afbea61"
                }
            ]
        },
        {
            "id": "fe23489b-e2b3-4d7c-8f82-d9eee477fc10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "compositeImage": {
                "id": "3a16533d-39f6-462a-b28f-96d13006d6fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe23489b-e2b3-4d7c-8f82-d9eee477fc10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13225543-a5b0-4625-9a0a-3ba844b49f45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe23489b-e2b3-4d7c-8f82-d9eee477fc10",
                    "LayerId": "be6f14da-cbe0-43c0-ac04-a4450afbea61"
                }
            ]
        },
        {
            "id": "af8fe008-b5be-4af9-8267-281d847e4491",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "compositeImage": {
                "id": "7f5ca5c2-bbe9-4f59-969d-9783bb948932",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af8fe008-b5be-4af9-8267-281d847e4491",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "615c2a7a-3834-4bf0-a409-1b7924c67936",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af8fe008-b5be-4af9-8267-281d847e4491",
                    "LayerId": "be6f14da-cbe0-43c0-ac04-a4450afbea61"
                }
            ]
        },
        {
            "id": "a55de6a1-f074-4d60-b265-ce69b0758857",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "compositeImage": {
                "id": "614ae3ad-65b8-49cd-87d7-b904d6df0b79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a55de6a1-f074-4d60-b265-ce69b0758857",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "caee353f-7808-4bdb-9337-cd2aad289600",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a55de6a1-f074-4d60-b265-ce69b0758857",
                    "LayerId": "be6f14da-cbe0-43c0-ac04-a4450afbea61"
                }
            ]
        },
        {
            "id": "65f59048-e16d-463e-9071-0539aadbde88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "compositeImage": {
                "id": "3a510ec7-dd24-42bd-9d9d-d932fa142de8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65f59048-e16d-463e-9071-0539aadbde88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaf5d488-ece7-4d4f-8a47-825fd9e8d9c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65f59048-e16d-463e-9071-0539aadbde88",
                    "LayerId": "be6f14da-cbe0-43c0-ac04-a4450afbea61"
                }
            ]
        },
        {
            "id": "6c112232-fd8e-48c6-89b7-70b8dc3a0342",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "compositeImage": {
                "id": "9725566c-4f61-498c-ad80-6a477be74174",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c112232-fd8e-48c6-89b7-70b8dc3a0342",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3af2bef7-de59-435a-b4fb-4461412dec3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c112232-fd8e-48c6-89b7-70b8dc3a0342",
                    "LayerId": "be6f14da-cbe0-43c0-ac04-a4450afbea61"
                }
            ]
        },
        {
            "id": "6d0e0a37-27ea-4c0d-9550-be1ce9072d62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "compositeImage": {
                "id": "e48bbfe9-a708-4cca-a1ea-d2464d9d1277",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d0e0a37-27ea-4c0d-9550-be1ce9072d62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9446ded-04b1-4279-bfec-1c3f01d2d634",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d0e0a37-27ea-4c0d-9550-be1ce9072d62",
                    "LayerId": "be6f14da-cbe0-43c0-ac04-a4450afbea61"
                }
            ]
        },
        {
            "id": "ea961ff8-16dd-4343-a908-e7dfd8211b47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "compositeImage": {
                "id": "5a431f7e-5afd-4ea2-98ad-b4ef1680b85b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea961ff8-16dd-4343-a908-e7dfd8211b47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05af2fcc-5ad7-4a41-9fe0-4beb984b6929",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea961ff8-16dd-4343-a908-e7dfd8211b47",
                    "LayerId": "be6f14da-cbe0-43c0-ac04-a4450afbea61"
                }
            ]
        },
        {
            "id": "15bc20b1-8346-4a2c-ba4b-e0aa60d02f39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "compositeImage": {
                "id": "dcdff6a0-40b5-490a-ba09-4693ce338533",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15bc20b1-8346-4a2c-ba4b-e0aa60d02f39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3061f5a6-4320-41d5-a007-8c46c4d31154",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15bc20b1-8346-4a2c-ba4b-e0aa60d02f39",
                    "LayerId": "be6f14da-cbe0-43c0-ac04-a4450afbea61"
                }
            ]
        },
        {
            "id": "a6ed6adb-61e1-41f0-af80-e878e4cb395b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "compositeImage": {
                "id": "ca8afe7e-d7f3-4e19-a06c-7686c9d0adab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6ed6adb-61e1-41f0-af80-e878e4cb395b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54ccef2a-4ff3-4baf-bde2-fa64faf78f11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6ed6adb-61e1-41f0-af80-e878e4cb395b",
                    "LayerId": "be6f14da-cbe0-43c0-ac04-a4450afbea61"
                }
            ]
        },
        {
            "id": "98d0b224-8e02-4ca9-8705-2adfbb0ed53a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "compositeImage": {
                "id": "d23fbad1-1415-45e7-9f15-6260de2c336c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98d0b224-8e02-4ca9-8705-2adfbb0ed53a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b620cfb3-82ef-4a1d-80c1-190e62a42836",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98d0b224-8e02-4ca9-8705-2adfbb0ed53a",
                    "LayerId": "be6f14da-cbe0-43c0-ac04-a4450afbea61"
                }
            ]
        },
        {
            "id": "1b9df56b-1500-4e4e-aadb-63474cffca6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "compositeImage": {
                "id": "9ab96f9c-e915-45ad-affe-6c8d92bd1616",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b9df56b-1500-4e4e-aadb-63474cffca6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a9c5d4b-fa9f-4080-ae4e-a416e652f830",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b9df56b-1500-4e4e-aadb-63474cffca6c",
                    "LayerId": "be6f14da-cbe0-43c0-ac04-a4450afbea61"
                }
            ]
        },
        {
            "id": "569d39e8-3c55-417c-8104-f16670d54d18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "compositeImage": {
                "id": "38bf6b87-0e1b-4d47-87c0-ac97c171103f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "569d39e8-3c55-417c-8104-f16670d54d18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e0cdadc-8883-46a8-ac76-7696d6b1998c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "569d39e8-3c55-417c-8104-f16670d54d18",
                    "LayerId": "be6f14da-cbe0-43c0-ac04-a4450afbea61"
                }
            ]
        },
        {
            "id": "3d86ea61-e752-4ede-b920-836519c3c683",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "compositeImage": {
                "id": "67b33e03-4c28-47f7-bf0d-b6c69f7f4133",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d86ea61-e752-4ede-b920-836519c3c683",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5c52323-13f6-4bf2-9f89-0bfeab060ac3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d86ea61-e752-4ede-b920-836519c3c683",
                    "LayerId": "be6f14da-cbe0-43c0-ac04-a4450afbea61"
                }
            ]
        },
        {
            "id": "1fea1271-b7c2-40cb-849e-80587289ecac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "compositeImage": {
                "id": "d84fb75c-0fe2-4762-95d8-2d2ed50615f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fea1271-b7c2-40cb-849e-80587289ecac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc3a746f-4108-4f61-88d6-2cd8829cd2b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fea1271-b7c2-40cb-849e-80587289ecac",
                    "LayerId": "be6f14da-cbe0-43c0-ac04-a4450afbea61"
                }
            ]
        },
        {
            "id": "fc75a60e-4601-493f-9a59-8de84a1bef73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "compositeImage": {
                "id": "b3d771ef-0eee-4177-8de2-7f90c06e07ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc75a60e-4601-493f-9a59-8de84a1bef73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ba8f972-d043-4f78-9b1f-a55e5c8a977e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc75a60e-4601-493f-9a59-8de84a1bef73",
                    "LayerId": "be6f14da-cbe0-43c0-ac04-a4450afbea61"
                }
            ]
        },
        {
            "id": "075125cd-eeb8-4dfa-8849-26a88b2ef0e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "compositeImage": {
                "id": "239ac5cc-7831-4a06-9146-9e9442d90bb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "075125cd-eeb8-4dfa-8849-26a88b2ef0e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a372b59-ac4d-4afe-8637-745f36ba304d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "075125cd-eeb8-4dfa-8849-26a88b2ef0e1",
                    "LayerId": "be6f14da-cbe0-43c0-ac04-a4450afbea61"
                }
            ]
        },
        {
            "id": "33889a48-cd85-49d4-8c89-d35c2149c5ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "compositeImage": {
                "id": "2e4822b7-e9bd-45f3-bc1b-116bb33a4527",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33889a48-cd85-49d4-8c89-d35c2149c5ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9e930ab-4aac-4948-96ab-d51984bc19dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33889a48-cd85-49d4-8c89-d35c2149c5ef",
                    "LayerId": "be6f14da-cbe0-43c0-ac04-a4450afbea61"
                }
            ]
        },
        {
            "id": "248e3c55-996e-4e14-bc65-5266fe3ead9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "compositeImage": {
                "id": "ad8ba2d0-176d-4674-b6e3-37f72a2dde3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "248e3c55-996e-4e14-bc65-5266fe3ead9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c616fd26-717a-4188-8c9a-21f8b8d91654",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "248e3c55-996e-4e14-bc65-5266fe3ead9c",
                    "LayerId": "be6f14da-cbe0-43c0-ac04-a4450afbea61"
                }
            ]
        },
        {
            "id": "544dfa1f-a0d9-4065-9241-affd937c4ad7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "compositeImage": {
                "id": "604350ac-076f-41fa-9811-89bec2b0f2a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "544dfa1f-a0d9-4065-9241-affd937c4ad7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1f4f111-ca28-4c6a-b184-b58116473878",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "544dfa1f-a0d9-4065-9241-affd937c4ad7",
                    "LayerId": "be6f14da-cbe0-43c0-ac04-a4450afbea61"
                }
            ]
        },
        {
            "id": "07cc035f-3fe0-451e-b709-045f855935fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "compositeImage": {
                "id": "4f3b8f5f-3091-4182-a9a3-ef28c6e9af36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07cc035f-3fe0-451e-b709-045f855935fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "520c6e68-c72a-4847-a6c2-bbde2f2ad5cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07cc035f-3fe0-451e-b709-045f855935fa",
                    "LayerId": "be6f14da-cbe0-43c0-ac04-a4450afbea61"
                }
            ]
        },
        {
            "id": "bbdd9f82-2d91-4351-81ca-a05a63e22616",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "compositeImage": {
                "id": "dd331027-2d0a-42bd-a2c5-321f4559f56b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbdd9f82-2d91-4351-81ca-a05a63e22616",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f09f9207-667f-4b92-b950-9ec15a62c882",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbdd9f82-2d91-4351-81ca-a05a63e22616",
                    "LayerId": "be6f14da-cbe0-43c0-ac04-a4450afbea61"
                }
            ]
        },
        {
            "id": "5ec92689-770d-40f6-aa28-522d03ee6eac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "compositeImage": {
                "id": "9be9744c-9484-4c32-b5bd-922cf1c5b11f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ec92689-770d-40f6-aa28-522d03ee6eac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05c8e774-b294-41bf-ab19-965571ca0178",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ec92689-770d-40f6-aa28-522d03ee6eac",
                    "LayerId": "be6f14da-cbe0-43c0-ac04-a4450afbea61"
                }
            ]
        },
        {
            "id": "8283d137-4c3a-49b2-b8e9-3b8c817296fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "compositeImage": {
                "id": "7a52084b-2211-4e7c-84b9-f2c4415cc422",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8283d137-4c3a-49b2-b8e9-3b8c817296fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b91f8cb7-db20-4bfd-9938-c5b402b8441f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8283d137-4c3a-49b2-b8e9-3b8c817296fa",
                    "LayerId": "be6f14da-cbe0-43c0-ac04-a4450afbea61"
                }
            ]
        },
        {
            "id": "cc83193a-094b-4563-bbba-2a93bfffb1e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "compositeImage": {
                "id": "c82de9fa-cae3-43e0-9e70-c8fdd2135cc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc83193a-094b-4563-bbba-2a93bfffb1e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f27ecd38-069e-4978-a9bc-7393fede9d74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc83193a-094b-4563-bbba-2a93bfffb1e2",
                    "LayerId": "be6f14da-cbe0-43c0-ac04-a4450afbea61"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "be6f14da-cbe0-43c0-ac04-a4450afbea61",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "52a429ee-8396-4e32-bf4e-554ef967190a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}