{
    "id": "de081805-da42-41ff-bb6b-ac11da7aae5b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_justice",
    "eventList": [
        {
            "id": "5d1583bd-9ff9-4e0f-804d-beca995eea2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "de081805-da42-41ff-bb6b-ac11da7aae5b"
        },
        {
            "id": "25b07f47-d315-46ad-b251-ca58853cd024",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "de081805-da42-41ff-bb6b-ac11da7aae5b"
        },
        {
            "id": "f303e287-6287-4c98-a2a3-d38ff83837e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "de081805-da42-41ff-bb6b-ac11da7aae5b"
        },
        {
            "id": "0b167e3b-392c-4805-91a6-5426dedf8317",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "de081805-da42-41ff-bb6b-ac11da7aae5b"
        },
        {
            "id": "9e20d0bf-2e54-4716-a7ed-227f8100b726",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "de081805-da42-41ff-bb6b-ac11da7aae5b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bbaabfed-20d2-47b4-9d52-ed9c255cd27b",
    "visible": true
}