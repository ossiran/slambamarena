{
    "id": "e132cb65-c969-477a-8df9-f266bfb6e70b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lsyn_5h",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 108,
    "bbox_left": 32,
    "bbox_right": 117,
    "bbox_top": 43,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bdfef978-4544-4619-b9bd-d284ff407e9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "compositeImage": {
                "id": "d1128bf3-7315-45b4-b2a1-92068aebcb4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdfef978-4544-4619-b9bd-d284ff407e9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b09b2a32-ace0-4188-a192-92de2ed0a8b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdfef978-4544-4619-b9bd-d284ff407e9a",
                    "LayerId": "d78ecf70-4575-4779-8140-6c4166e277fc"
                }
            ]
        },
        {
            "id": "86ada2e9-df43-451c-a080-722bddcc869f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "compositeImage": {
                "id": "bd5fe962-d134-4996-8afc-dfc281af73a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86ada2e9-df43-451c-a080-722bddcc869f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43131ac7-10bc-4746-aeed-180d83fefa70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86ada2e9-df43-451c-a080-722bddcc869f",
                    "LayerId": "d78ecf70-4575-4779-8140-6c4166e277fc"
                }
            ]
        },
        {
            "id": "8f982ed5-401a-4a9e-b8e0-96c8bbfc231c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "compositeImage": {
                "id": "b3b5994b-c1fb-4bdb-8ac8-56eee6587446",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f982ed5-401a-4a9e-b8e0-96c8bbfc231c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cc687b0-73af-4fbc-9123-ad9b11e64b5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f982ed5-401a-4a9e-b8e0-96c8bbfc231c",
                    "LayerId": "d78ecf70-4575-4779-8140-6c4166e277fc"
                }
            ]
        },
        {
            "id": "1ab13f83-1fa0-41a0-a07d-de12f55883e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "compositeImage": {
                "id": "8911b98c-2c63-4602-b799-3b2acde16d5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ab13f83-1fa0-41a0-a07d-de12f55883e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cd93a0c-4a87-433e-af6b-2f306807853f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ab13f83-1fa0-41a0-a07d-de12f55883e8",
                    "LayerId": "d78ecf70-4575-4779-8140-6c4166e277fc"
                }
            ]
        },
        {
            "id": "9441db0e-412b-41bb-8651-00b34c76cb3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "compositeImage": {
                "id": "84ca1f29-b1c0-4b5d-86fd-029c529cdfd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9441db0e-412b-41bb-8651-00b34c76cb3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8cbb664-2a49-4f75-ac92-f596a5663c2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9441db0e-412b-41bb-8651-00b34c76cb3a",
                    "LayerId": "d78ecf70-4575-4779-8140-6c4166e277fc"
                }
            ]
        },
        {
            "id": "34201361-8b28-43c4-ab2a-e617d7bb6f7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "compositeImage": {
                "id": "544c5405-0c45-4286-bdb6-ab5b17e663fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34201361-8b28-43c4-ab2a-e617d7bb6f7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "778c3afd-8bb0-469c-81cd-2522117342cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34201361-8b28-43c4-ab2a-e617d7bb6f7f",
                    "LayerId": "d78ecf70-4575-4779-8140-6c4166e277fc"
                }
            ]
        },
        {
            "id": "ea2fb9a7-87d6-4e1d-a7d0-b03ba100213f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "compositeImage": {
                "id": "5999b0fb-a8e0-4041-8739-66473129fb18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea2fb9a7-87d6-4e1d-a7d0-b03ba100213f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ec44cf2-a453-4cbe-aff7-0b3212aca25f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea2fb9a7-87d6-4e1d-a7d0-b03ba100213f",
                    "LayerId": "d78ecf70-4575-4779-8140-6c4166e277fc"
                }
            ]
        },
        {
            "id": "8b3fe2aa-ded7-4145-a7da-0f6712de7918",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "compositeImage": {
                "id": "e92c46b9-c76a-4e30-bba2-e03aad2557d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b3fe2aa-ded7-4145-a7da-0f6712de7918",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4eed7dd-d8b7-4b66-bc3e-60767818667b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b3fe2aa-ded7-4145-a7da-0f6712de7918",
                    "LayerId": "d78ecf70-4575-4779-8140-6c4166e277fc"
                }
            ]
        },
        {
            "id": "c24e28ac-b449-4ffc-8415-cd5975d7e925",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "compositeImage": {
                "id": "aa672069-b2a1-44c8-abdd-2d1b4f6e5ba1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c24e28ac-b449-4ffc-8415-cd5975d7e925",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a6fdee8-3456-43cd-b7ee-a84aa008e6b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c24e28ac-b449-4ffc-8415-cd5975d7e925",
                    "LayerId": "d78ecf70-4575-4779-8140-6c4166e277fc"
                }
            ]
        },
        {
            "id": "675dec1d-1003-4b73-b7b4-29b3e8ee5e5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "compositeImage": {
                "id": "378c8bd3-59ee-4f88-a9fe-8d46c64ebd4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "675dec1d-1003-4b73-b7b4-29b3e8ee5e5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42e0e272-9c9d-47a1-b0ef-2f6cfa7d14be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "675dec1d-1003-4b73-b7b4-29b3e8ee5e5f",
                    "LayerId": "d78ecf70-4575-4779-8140-6c4166e277fc"
                }
            ]
        },
        {
            "id": "f526cc37-cb60-4d57-b695-a10623f3a612",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "compositeImage": {
                "id": "ecbcea8c-5b01-4181-99cf-ad3e20e772b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f526cc37-cb60-4d57-b695-a10623f3a612",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6e75353-560d-4888-bb65-dee9cfa1dc46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f526cc37-cb60-4d57-b695-a10623f3a612",
                    "LayerId": "d78ecf70-4575-4779-8140-6c4166e277fc"
                }
            ]
        },
        {
            "id": "e4ce7df9-7982-4547-b49e-57b5c3478a9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "compositeImage": {
                "id": "ddfba4ed-06d7-4131-8394-01dc3b3a9a7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4ce7df9-7982-4547-b49e-57b5c3478a9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28035cbb-c99c-4815-8fa6-6f3f9a2bcdac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4ce7df9-7982-4547-b49e-57b5c3478a9b",
                    "LayerId": "d78ecf70-4575-4779-8140-6c4166e277fc"
                }
            ]
        },
        {
            "id": "b8f77c10-3661-4d9b-9763-7a16c43b56c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "compositeImage": {
                "id": "ac92be07-81c5-4bf3-97ba-7d2d186c2925",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8f77c10-3661-4d9b-9763-7a16c43b56c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4902455-c4ba-44d7-b484-d7e602c431c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8f77c10-3661-4d9b-9763-7a16c43b56c5",
                    "LayerId": "d78ecf70-4575-4779-8140-6c4166e277fc"
                }
            ]
        },
        {
            "id": "f2007d4b-6094-4b69-8db5-73babf1d9fbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "compositeImage": {
                "id": "5c88f963-8e73-44e2-abd8-d834c3792a2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2007d4b-6094-4b69-8db5-73babf1d9fbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e4d6a20-9cba-485a-af8f-616ed2d5393f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2007d4b-6094-4b69-8db5-73babf1d9fbb",
                    "LayerId": "d78ecf70-4575-4779-8140-6c4166e277fc"
                }
            ]
        },
        {
            "id": "a38fa9ce-2d6c-4de2-bbbe-69e659fa36e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "compositeImage": {
                "id": "045d210b-4cf9-4701-8dce-fde32b412e58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a38fa9ce-2d6c-4de2-bbbe-69e659fa36e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d72b0f41-d20c-42e7-bc2b-0957beaadeb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a38fa9ce-2d6c-4de2-bbbe-69e659fa36e6",
                    "LayerId": "d78ecf70-4575-4779-8140-6c4166e277fc"
                }
            ]
        },
        {
            "id": "7a884c83-ec8d-4fed-9b0b-e6eac2e53a52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "compositeImage": {
                "id": "0e78cb8c-9ebe-4aae-b83a-4a6be9760402",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a884c83-ec8d-4fed-9b0b-e6eac2e53a52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e85bc0e7-bd4d-4df2-b98b-64b8d733eda1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a884c83-ec8d-4fed-9b0b-e6eac2e53a52",
                    "LayerId": "d78ecf70-4575-4779-8140-6c4166e277fc"
                }
            ]
        },
        {
            "id": "19bdb2d3-f218-4abb-87c1-490350eba48c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "compositeImage": {
                "id": "96bfbe50-b5e2-430c-87ff-f21579460738",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19bdb2d3-f218-4abb-87c1-490350eba48c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce7a02f0-0591-4c34-a6eb-d1f5250cee37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19bdb2d3-f218-4abb-87c1-490350eba48c",
                    "LayerId": "d78ecf70-4575-4779-8140-6c4166e277fc"
                }
            ]
        },
        {
            "id": "559e420a-bd30-4396-9e87-92a41bdc887c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "compositeImage": {
                "id": "364a54c9-2dcd-4b16-b9ba-6019caa24699",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "559e420a-bd30-4396-9e87-92a41bdc887c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b14ac29-03e8-496a-bcf5-72e94644d7d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "559e420a-bd30-4396-9e87-92a41bdc887c",
                    "LayerId": "d78ecf70-4575-4779-8140-6c4166e277fc"
                }
            ]
        },
        {
            "id": "596f7c65-ea52-4827-804d-5eae4e542c4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "compositeImage": {
                "id": "45c3d693-987d-4130-b5ae-f984cf0e8d02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "596f7c65-ea52-4827-804d-5eae4e542c4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f40ded7-bb68-4ab8-8430-95a158ea9d86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "596f7c65-ea52-4827-804d-5eae4e542c4f",
                    "LayerId": "d78ecf70-4575-4779-8140-6c4166e277fc"
                }
            ]
        },
        {
            "id": "8855b534-de90-4383-93e9-11fd6a8ea7cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "compositeImage": {
                "id": "39cfbc28-620b-4d2c-8cbe-6d65e3e8da29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8855b534-de90-4383-93e9-11fd6a8ea7cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9f4528b-77b5-4f61-a3d0-1b1a3a5d5747",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8855b534-de90-4383-93e9-11fd6a8ea7cd",
                    "LayerId": "d78ecf70-4575-4779-8140-6c4166e277fc"
                }
            ]
        },
        {
            "id": "b7305841-2913-403a-b76e-1b6336300cff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "compositeImage": {
                "id": "cc6c6cfa-eda8-4d07-9fd1-7bae8628669a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7305841-2913-403a-b76e-1b6336300cff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc8cec4a-e28a-420b-a891-2e44895c6870",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7305841-2913-403a-b76e-1b6336300cff",
                    "LayerId": "d78ecf70-4575-4779-8140-6c4166e277fc"
                }
            ]
        },
        {
            "id": "3808778a-cd64-46cd-adb7-26fff7b1d7de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "compositeImage": {
                "id": "f494eb12-ff77-4759-a0c2-e2b510b89ef3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3808778a-cd64-46cd-adb7-26fff7b1d7de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0e1cf5f-b7a1-4987-922b-841c3052bd82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3808778a-cd64-46cd-adb7-26fff7b1d7de",
                    "LayerId": "d78ecf70-4575-4779-8140-6c4166e277fc"
                }
            ]
        },
        {
            "id": "e323e0ef-c107-42b9-a1b6-f68f74459fc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "compositeImage": {
                "id": "aaeec659-222b-456b-b790-ea615d7943f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e323e0ef-c107-42b9-a1b6-f68f74459fc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e43a925-4ca6-4f27-911a-e8cf1016b864",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e323e0ef-c107-42b9-a1b6-f68f74459fc9",
                    "LayerId": "d78ecf70-4575-4779-8140-6c4166e277fc"
                }
            ]
        },
        {
            "id": "f4861bf4-ee4d-44be-8a0e-5d31539f5c32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "compositeImage": {
                "id": "29eed9a9-ef89-42a0-93a2-3b2c31130ff1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4861bf4-ee4d-44be-8a0e-5d31539f5c32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26c8f755-0320-47f6-b36a-c8f744058ac9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4861bf4-ee4d-44be-8a0e-5d31539f5c32",
                    "LayerId": "d78ecf70-4575-4779-8140-6c4166e277fc"
                }
            ]
        },
        {
            "id": "8059fa9f-21b0-44d1-9116-b9ee9287929e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "compositeImage": {
                "id": "91a8714b-20e4-433a-bcc7-68afa4861399",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8059fa9f-21b0-44d1-9116-b9ee9287929e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bec1fc1e-dfb1-4e1f-bc05-94c48e39da83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8059fa9f-21b0-44d1-9116-b9ee9287929e",
                    "LayerId": "d78ecf70-4575-4779-8140-6c4166e277fc"
                }
            ]
        },
        {
            "id": "d649b275-42e4-4e4a-b72d-4e650c289c1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "compositeImage": {
                "id": "8235dd3e-686e-41a6-9c64-7650a1a6c427",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d649b275-42e4-4e4a-b72d-4e650c289c1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f018a388-6c37-4a12-a552-163167a9f00d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d649b275-42e4-4e4a-b72d-4e650c289c1e",
                    "LayerId": "d78ecf70-4575-4779-8140-6c4166e277fc"
                }
            ]
        },
        {
            "id": "b1e1f59b-f313-4d9f-8879-78b85f7fe33c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "compositeImage": {
                "id": "3228a4d4-1332-413b-884c-8627de999aa8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1e1f59b-f313-4d9f-8879-78b85f7fe33c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bcc027d-5e39-46c6-a9ed-11febd583acb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1e1f59b-f313-4d9f-8879-78b85f7fe33c",
                    "LayerId": "d78ecf70-4575-4779-8140-6c4166e277fc"
                }
            ]
        },
        {
            "id": "e0c4e57b-345c-45ae-9cb8-9368a547326c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "compositeImage": {
                "id": "1d532090-24bc-4788-82c2-9f50e2e0defb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0c4e57b-345c-45ae-9cb8-9368a547326c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30422537-73b8-465f-a352-c7ad91596f93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0c4e57b-345c-45ae-9cb8-9368a547326c",
                    "LayerId": "d78ecf70-4575-4779-8140-6c4166e277fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d78ecf70-4575-4779-8140-6c4166e277fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e132cb65-c969-477a-8df9-f266bfb6e70b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}