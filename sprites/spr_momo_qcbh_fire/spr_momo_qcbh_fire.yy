{
    "id": "50f930b9-366e-4f74-8a36-1e31ef54df96",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_qcbh_fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "37dbc321-002e-40e2-a30d-ed9b759333b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50f930b9-366e-4f74-8a36-1e31ef54df96",
            "compositeImage": {
                "id": "63676d13-2743-46bb-b028-da023ff9fdac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37dbc321-002e-40e2-a30d-ed9b759333b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ca7c871-c814-4609-a92b-81aea4959500",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37dbc321-002e-40e2-a30d-ed9b759333b9",
                    "LayerId": "28c17301-4bea-4f13-a356-dd280658c78e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "28c17301-4bea-4f13-a356-dd280658c78e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50f930b9-366e-4f74-8a36-1e31ef54df96",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}