{
    "id": "d4beaf2c-ea82-409a-bb8f-ec28e07208a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_myu_crouch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 12,
    "bbox_right": 35,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc1abd90-dcde-424e-89da-7d52a43475dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4beaf2c-ea82-409a-bb8f-ec28e07208a1",
            "compositeImage": {
                "id": "ad2bd0ec-ee94-428b-af53-021a136e9b7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc1abd90-dcde-424e-89da-7d52a43475dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5b591ba-ce9a-47c4-8807-ac7798e78c1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc1abd90-dcde-424e-89da-7d52a43475dd",
                    "LayerId": "35d4bc8d-146b-4c4c-bf13-8a9de2d09a74"
                }
            ]
        },
        {
            "id": "ae0d2929-77b7-46d6-a511-9c7b06874713",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4beaf2c-ea82-409a-bb8f-ec28e07208a1",
            "compositeImage": {
                "id": "41ac7a80-1bc0-4ca1-aeb1-acab3c271faf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae0d2929-77b7-46d6-a511-9c7b06874713",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e89db09f-7393-4ce8-8b25-77d8a4a4b387",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae0d2929-77b7-46d6-a511-9c7b06874713",
                    "LayerId": "35d4bc8d-146b-4c4c-bf13-8a9de2d09a74"
                }
            ]
        },
        {
            "id": "7d8e9671-c950-408c-bdb7-94523ea49800",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4beaf2c-ea82-409a-bb8f-ec28e07208a1",
            "compositeImage": {
                "id": "f7cfcbe7-9631-4c6f-9325-23fed641ca13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d8e9671-c950-408c-bdb7-94523ea49800",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47b201cb-adf4-4e0a-984b-36f038dad8f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d8e9671-c950-408c-bdb7-94523ea49800",
                    "LayerId": "35d4bc8d-146b-4c4c-bf13-8a9de2d09a74"
                }
            ]
        },
        {
            "id": "8b6836e0-17d5-4937-ad5a-9ef43db8a53e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4beaf2c-ea82-409a-bb8f-ec28e07208a1",
            "compositeImage": {
                "id": "572d2728-878a-40d8-8612-96c83d362fe8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b6836e0-17d5-4937-ad5a-9ef43db8a53e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdbc607a-d856-425a-9e38-055aa877a9d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b6836e0-17d5-4937-ad5a-9ef43db8a53e",
                    "LayerId": "35d4bc8d-146b-4c4c-bf13-8a9de2d09a74"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "35d4bc8d-146b-4c4c-bf13-8a9de2d09a74",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d4beaf2c-ea82-409a-bb8f-ec28e07208a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 32
}