if(state_new == 1)
{
	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -42;
	    xscale  = 26;
	    yscale  = 74;
	    xshift  = -2;
	}
	
			with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 16;
	    image_yscale  = 47;
	    xshift = -2
	}
	
	attackframe0 = 2;
	attackframe1 = 5;
	attackframe2 = 17;
	attackframe3 = 10;
	attackframe4 = 18;
	endstate = 65;
	

	vx = 0;
	vy = 0;
}

sprite_index = spr_bill_cspecial;

//initial hitbox
if(state_timer == attackframe0 && !hitpausing())
{
    hitbox0 = create_hitbox(2 ,-55,36,76,5,-14,13,12,10,10,-1,0,3,3,"mid",-1);   
    hitbox0.hitsound = snd_hit;
    hitbox0.scaling = 1;
    //hitbox0.juggle = 1;
    hitbox0.chipdamage = 5;
    ex += 3;
}

//begin movement up
if(state_timer > attackframe1 && state_timer < attackframe2 && !hitpausing())
{
    vy = -8;
    vx = 5 * image_xscale ;
}

//3rd and final hitbox
if(state_timer == attackframe3 && !hitpausing())
{
    hitbox0 = create_hitbox(4,-55,26,76,4,-18.25 + 6 * hitting,14,4,7,10,-1,0,13,3,"mid",-1); 
    hitbox0.scaling = 1;
    hitbox0.hitsound = snd_hit;
    hitbox0.chipdamage = 5;
}

//attackbase. changes to air if not on ground
if(state_timer > attackframe2)
{
    attack_base(endstate,1,0,0,1,0);
}
else
{
    attack_base(endstate,0,0,0,1,0);
}

if(state_timer == attackframe2 + 1 && !hitpausing())
{
    vy = 0;
}

if(state_timer > attackframe4 && !onground() && !hitpausing())
{
    vx = 1 * image_xscale;
}