{
    "id": "a25cfeaa-7807-487e-8378-1a00735ac263",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_exmeter1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 153,
    "bbox_right": 456,
    "bbox_top": 64,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fb5db550-9e55-4f28-b7ab-e907ec647d9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a25cfeaa-7807-487e-8378-1a00735ac263",
            "compositeImage": {
                "id": "c271f44d-6eb4-4d91-bbe8-e991f6623f24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb5db550-9e55-4f28-b7ab-e907ec647d9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "306948c1-5343-4ba3-9875-c72bf74362d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb5db550-9e55-4f28-b7ab-e907ec647d9e",
                    "LayerId": "9c09205f-a32c-4f05-ac2d-848b470cad0e"
                }
            ]
        },
        {
            "id": "c7105dd6-5cb1-46ad-a110-f9f248721dfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a25cfeaa-7807-487e-8378-1a00735ac263",
            "compositeImage": {
                "id": "61306650-2610-4149-b502-6291c83fd858",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7105dd6-5cb1-46ad-a110-f9f248721dfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aedae9d5-0359-4205-aa64-b9168b19f893",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7105dd6-5cb1-46ad-a110-f9f248721dfb",
                    "LayerId": "9c09205f-a32c-4f05-ac2d-848b470cad0e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "9c09205f-a32c-4f05-ac2d-848b470cad0e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a25cfeaa-7807-487e-8378-1a00735ac263",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 15,
    "yorig": 115
}