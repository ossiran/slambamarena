{
    "id": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dsyn_fwalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 108,
    "bbox_left": 49,
    "bbox_right": 76,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e527e34c-25f3-4fdb-b525-188e38ec99b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "714d567b-d74f-45e7-9897-c1b5baa7a309",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e527e34c-25f3-4fdb-b525-188e38ec99b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f962be66-08dc-4964-9d2e-34ce7aeae826",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e527e34c-25f3-4fdb-b525-188e38ec99b8",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "5760a9c9-8072-4e9d-bee6-8b3275ce98a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "e6111b29-6c55-4e1a-94d5-b1b7b24ad43b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5760a9c9-8072-4e9d-bee6-8b3275ce98a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e226ba35-5c76-4bd6-8e0e-3647fb5982d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5760a9c9-8072-4e9d-bee6-8b3275ce98a6",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "b36685b9-cb44-4e78-a19e-0dd21061eac6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "ec45ad46-c4b8-44ef-a5f0-bf401d1f4e93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b36685b9-cb44-4e78-a19e-0dd21061eac6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3552b30-cac8-4bae-aa57-b66e0a888da4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b36685b9-cb44-4e78-a19e-0dd21061eac6",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "4552287c-7609-43e0-a715-12616c8b495d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "fb74d914-014e-4f8f-92fb-56558a4f0822",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4552287c-7609-43e0-a715-12616c8b495d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ea4d3a2-f36f-406b-a0dd-2256b14244d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4552287c-7609-43e0-a715-12616c8b495d",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "2b2d0f1c-8ecc-48a0-85d3-6a977dac8fcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "dc6334f1-8e3c-4685-941f-6bed5ca88b9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b2d0f1c-8ecc-48a0-85d3-6a977dac8fcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99e1262a-12d0-430b-95f4-535bdca547af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b2d0f1c-8ecc-48a0-85d3-6a977dac8fcb",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "ba0ed097-bcb4-492d-a656-b5718958ab04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "39237246-195d-4391-a84a-a5a507fa84d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba0ed097-bcb4-492d-a656-b5718958ab04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4f8306d-17a4-48ad-80b2-cf8a63b51cf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba0ed097-bcb4-492d-a656-b5718958ab04",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "3074efb3-ea9c-4f83-8a00-e75419c3d908",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "edc13775-7afe-4554-9c5f-b17a3d3853b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3074efb3-ea9c-4f83-8a00-e75419c3d908",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a09858c9-04f1-42ab-b436-6c2348387619",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3074efb3-ea9c-4f83-8a00-e75419c3d908",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "660b77c3-1cfe-47e3-95b8-a4677b0aade3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "7e86f93e-6fb1-4653-b3dc-3abd0a0eb6ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "660b77c3-1cfe-47e3-95b8-a4677b0aade3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53e94310-1ecb-46db-8799-c7a902787891",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "660b77c3-1cfe-47e3-95b8-a4677b0aade3",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "e92b83a9-d3c8-41d3-9313-5b4b2e6c13d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "050bfc7c-e76b-48ec-8d52-586e02173570",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e92b83a9-d3c8-41d3-9313-5b4b2e6c13d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "887eafd7-7e62-47cc-aeb6-ea9025dc8a43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e92b83a9-d3c8-41d3-9313-5b4b2e6c13d0",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "89b55ca8-fe33-481e-9039-5c5856cb3686",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "31f4dffb-14ad-4620-9f3c-cfd5c60fe701",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89b55ca8-fe33-481e-9039-5c5856cb3686",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6b55b14-6dbb-44ea-a583-d005122901b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89b55ca8-fe33-481e-9039-5c5856cb3686",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "cd561433-f5c6-484d-88ab-5bf155e76b1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "41e2f0e0-a086-43ec-adbc-df14f42525b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd561433-f5c6-484d-88ab-5bf155e76b1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a64b72cf-a889-4d83-80e3-8b094c223a35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd561433-f5c6-484d-88ab-5bf155e76b1e",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "fe3ed977-7503-4eba-b82a-cc0a4bd7ec39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "942516c2-405c-454c-9842-6a2e5fc1f259",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe3ed977-7503-4eba-b82a-cc0a4bd7ec39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09fe2720-b3cc-4232-af42-440299a6df25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe3ed977-7503-4eba-b82a-cc0a4bd7ec39",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "2f318687-a072-42a4-9d6c-c9eced8c5547",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "6c758b0d-6f1c-41a3-9670-7b040faac0b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f318687-a072-42a4-9d6c-c9eced8c5547",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaeaf0a3-9e58-405c-b81c-bf9d2864f767",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f318687-a072-42a4-9d6c-c9eced8c5547",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "d6fa3fc6-e1f7-49ad-8d8f-0a198ba64795",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "20f79212-b16b-4671-a5c2-2e2faebf23a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6fa3fc6-e1f7-49ad-8d8f-0a198ba64795",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "629d5a27-977e-4542-b734-3a4359a9be57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6fa3fc6-e1f7-49ad-8d8f-0a198ba64795",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "5c5bbd15-274c-4bfa-b30e-9e4c04c371ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "ecfc0485-a132-4c16-bc7b-8e61d831f63b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c5bbd15-274c-4bfa-b30e-9e4c04c371ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11f85b97-5ded-4902-b907-73ac69d1f12f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c5bbd15-274c-4bfa-b30e-9e4c04c371ab",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "da539216-3f46-4a84-b4b8-c084d610db64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "cb3301f6-ae33-41e2-b344-2777be99fd33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da539216-3f46-4a84-b4b8-c084d610db64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa23106b-a893-4766-a0f6-38df64210d75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da539216-3f46-4a84-b4b8-c084d610db64",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "27e438de-fa69-4e50-b22b-853ee55a1c0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "696ea3bb-92e5-4c64-acee-3d7dfa437f68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27e438de-fa69-4e50-b22b-853ee55a1c0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9e2701e-ac8e-44a5-b134-460a460b7774",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27e438de-fa69-4e50-b22b-853ee55a1c0e",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "a18165cb-2617-49c4-8627-a1bd358d5e06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "98bd90f3-3de1-4ed6-81be-2c0e0d4f0985",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a18165cb-2617-49c4-8627-a1bd358d5e06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e845bec6-1c7f-407a-9ecc-220f68ab230c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a18165cb-2617-49c4-8627-a1bd358d5e06",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "6ffa591a-4a39-4c0e-b64a-368201843668",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "184a0897-0ee0-4726-b5b2-52c39974b15e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ffa591a-4a39-4c0e-b64a-368201843668",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab8bb31d-e711-434f-a8ea-e401df32b829",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ffa591a-4a39-4c0e-b64a-368201843668",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "4cc7a7e4-89f7-4759-86a5-9dc018e40efe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "9fa9cd0c-c049-4e24-8e74-a66395d4e743",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cc7a7e4-89f7-4759-86a5-9dc018e40efe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2720c2d-0ddb-4637-92fb-55e4b22f80e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cc7a7e4-89f7-4759-86a5-9dc018e40efe",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "6a662279-cbe4-4016-a9e9-5a5fac3f0304",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "3002bc8c-9cc4-48ed-abfa-94b37c54d43e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a662279-cbe4-4016-a9e9-5a5fac3f0304",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e15c259-2fce-4749-b702-77890b4c44b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a662279-cbe4-4016-a9e9-5a5fac3f0304",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "a59bf5b0-579d-458f-b3f4-0304f234a3b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "19624ae3-9f51-4f44-b268-54d980d06ece",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a59bf5b0-579d-458f-b3f4-0304f234a3b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dba97836-410d-4ad7-ac63-188bbbe6f75f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a59bf5b0-579d-458f-b3f4-0304f234a3b0",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "7f3237d8-85b7-4450-932b-e8535904d0d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "5eaa410b-47b8-4750-ae0d-fcbef96313cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f3237d8-85b7-4450-932b-e8535904d0d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff20aa92-9297-4424-8be1-defa7c36796e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f3237d8-85b7-4450-932b-e8535904d0d3",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "bbb69a4a-8e88-42ea-b3fd-924a443ccfe1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "c3222fac-5a86-4492-9227-480e5cd2199a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbb69a4a-8e88-42ea-b3fd-924a443ccfe1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "581917a0-1ba9-4d9a-aa88-ce622dd8afef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbb69a4a-8e88-42ea-b3fd-924a443ccfe1",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "1235ad67-1fd9-4484-983c-e45146a83190",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "bcc1c570-2491-4a23-8d66-20aab349f6f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1235ad67-1fd9-4484-983c-e45146a83190",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd664ea3-530b-4785-850f-f6d9ab8afad3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1235ad67-1fd9-4484-983c-e45146a83190",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "c8fd9cee-799b-4d89-9352-8fa2f3249772",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "de8dd8fa-9415-4426-a468-0ab45c5c3faf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8fd9cee-799b-4d89-9352-8fa2f3249772",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cab6c46a-32e6-40a4-88ab-b25c0d14955c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8fd9cee-799b-4d89-9352-8fa2f3249772",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "12bc70bf-7502-44f0-906d-9be20bc1ae21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "2b3df070-1579-4d1c-a0cd-69a24a90100f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12bc70bf-7502-44f0-906d-9be20bc1ae21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f13dabe-1c4b-42c4-bb2a-085e20e42e54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12bc70bf-7502-44f0-906d-9be20bc1ae21",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "6ea65902-bd65-4463-8a4a-7a7be30e4658",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "913e70ec-d85e-45a3-ad8c-e760dfa9385e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ea65902-bd65-4463-8a4a-7a7be30e4658",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "100d42cc-9a29-485b-a53d-169c0951bead",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ea65902-bd65-4463-8a4a-7a7be30e4658",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "262c8f30-dca6-4ba1-801a-3a2ec3783f09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "bf40d278-4912-455b-87a2-e0d78c66ad49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "262c8f30-dca6-4ba1-801a-3a2ec3783f09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72994c3c-5117-42c5-9058-704ba773fd03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "262c8f30-dca6-4ba1-801a-3a2ec3783f09",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "d454b1a1-49d4-4373-b464-414eaee30b6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "3f37be6c-ad4b-43cb-b9ab-4332ab05af70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d454b1a1-49d4-4373-b464-414eaee30b6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a82b918-0140-47ae-8e18-28d06263aa56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d454b1a1-49d4-4373-b464-414eaee30b6b",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "22764b90-2469-40e9-a9e5-e9d128e66afb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "1f154f2d-5017-4116-a405-1c787aaf74b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22764b90-2469-40e9-a9e5-e9d128e66afb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b01322d-50e5-458d-83b0-f0e87fada64e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22764b90-2469-40e9-a9e5-e9d128e66afb",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "728b311f-d66e-41bd-a636-860ead2cbc14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "fb1616b5-ea3a-4b50-8615-c6043978bb22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "728b311f-d66e-41bd-a636-860ead2cbc14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8798f215-a506-44aa-9540-dbd7768fe2af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "728b311f-d66e-41bd-a636-860ead2cbc14",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "6db05153-a51a-4379-92c0-a762a646832b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "a9615e13-3120-4a82-bec7-7437c10db112",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6db05153-a51a-4379-92c0-a762a646832b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfc1a6bc-f20b-4596-b84b-88910c9fa701",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6db05153-a51a-4379-92c0-a762a646832b",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "2194c286-a036-4be4-892b-2d1c9924a5b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "5a288851-fcd5-43bd-8132-0f3d7cfc071a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2194c286-a036-4be4-892b-2d1c9924a5b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03e5ba94-8567-4301-a051-df4963d79c05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2194c286-a036-4be4-892b-2d1c9924a5b9",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "e7b66aae-c0ff-44d7-9cb9-6a5b86918066",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "ed261d38-89b7-4758-b9fc-8151af1f70de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7b66aae-c0ff-44d7-9cb9-6a5b86918066",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6daf97b3-24ae-473a-988f-46744b151099",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7b66aae-c0ff-44d7-9cb9-6a5b86918066",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "a1e657b3-4290-4196-af25-73d5b49220dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "7042f6d5-e5a1-486c-bc3d-d7cbb0be115c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1e657b3-4290-4196-af25-73d5b49220dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94a9d493-db56-4ef0-b9dd-bc0caf518f74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1e657b3-4290-4196-af25-73d5b49220dd",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "d29312ab-6c10-4dd7-84e3-5088caeeba21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "f12381e9-cabb-47a5-89d1-5ad2956209f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d29312ab-6c10-4dd7-84e3-5088caeeba21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb526185-2f84-42ed-b3ab-cb8026a4aa0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d29312ab-6c10-4dd7-84e3-5088caeeba21",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "6ae05850-d7e4-4191-b301-742e85dfce7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "52a6d633-cff6-40b0-81ee-c7de699e1b89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ae05850-d7e4-4191-b301-742e85dfce7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8ee9c9e-670b-4d7a-b96c-fb1ebcf88fb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ae05850-d7e4-4191-b301-742e85dfce7b",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "77934f28-e435-42b2-9487-c15171c3393b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "ebb6cf56-653a-456c-a94e-7567957c3a9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77934f28-e435-42b2-9487-c15171c3393b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4995f311-15b8-4c6d-9778-bd1e4b51d137",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77934f28-e435-42b2-9487-c15171c3393b",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        },
        {
            "id": "225743ce-f680-4f1c-a72c-f13196d8eaeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "compositeImage": {
                "id": "f0d016c4-7781-4b68-9690-37e83213099d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "225743ce-f680-4f1c-a72c-f13196d8eaeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11779b85-58b0-4de6-b6d7-274cd1c1fca1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "225743ce-f680-4f1c-a72c-f13196d8eaeb",
                    "LayerId": "d87057ae-24aa-4ece-b81a-0dda8e245987"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d87057ae-24aa-4ece-b81a-0dda8e245987",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6bfe06ca-9dd2-4d3c-9016-49674334ccc0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}