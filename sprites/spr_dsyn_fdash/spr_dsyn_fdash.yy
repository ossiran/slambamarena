{
    "id": "ebe1472a-69af-40d1-93c1-f61fbed00889",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dsyn_fdash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 45,
    "bbox_right": 76,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b44ae5b-9e30-4693-a105-157619640c64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebe1472a-69af-40d1-93c1-f61fbed00889",
            "compositeImage": {
                "id": "12a6646b-7447-4f11-844c-012c087ab913",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b44ae5b-9e30-4693-a105-157619640c64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f9652bb-f914-4b1e-b8bc-02a4d7c07a39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b44ae5b-9e30-4693-a105-157619640c64",
                    "LayerId": "2a834005-b578-4570-b173-af44bfe96232"
                }
            ]
        },
        {
            "id": "6f43b726-990f-43d5-948b-145e1164b2ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebe1472a-69af-40d1-93c1-f61fbed00889",
            "compositeImage": {
                "id": "b9a4ee22-e33a-4eff-8885-b7f9f3cf5473",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f43b726-990f-43d5-948b-145e1164b2ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd5d5989-2a8c-48fc-be40-2b34b5a94d76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f43b726-990f-43d5-948b-145e1164b2ae",
                    "LayerId": "2a834005-b578-4570-b173-af44bfe96232"
                }
            ]
        },
        {
            "id": "81642a1a-1724-45df-86c0-d69d80bca6e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebe1472a-69af-40d1-93c1-f61fbed00889",
            "compositeImage": {
                "id": "e0cc9208-9479-4685-9675-bca223ec8932",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81642a1a-1724-45df-86c0-d69d80bca6e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c39c6153-b82d-4a6d-b2d2-0c7eed2f2891",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81642a1a-1724-45df-86c0-d69d80bca6e0",
                    "LayerId": "2a834005-b578-4570-b173-af44bfe96232"
                }
            ]
        },
        {
            "id": "e733e473-bef3-419b-ad79-0f999d1d479f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebe1472a-69af-40d1-93c1-f61fbed00889",
            "compositeImage": {
                "id": "70b58202-1397-481b-94fd-29c06c1a99a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e733e473-bef3-419b-ad79-0f999d1d479f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b589a2d-a7cf-4629-8252-3a173fafb79e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e733e473-bef3-419b-ad79-0f999d1d479f",
                    "LayerId": "2a834005-b578-4570-b173-af44bfe96232"
                }
            ]
        },
        {
            "id": "4844aa48-f27f-4f98-aaad-75ccad9175c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebe1472a-69af-40d1-93c1-f61fbed00889",
            "compositeImage": {
                "id": "f4583919-dcd1-47d9-b41a-ba85281f4912",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4844aa48-f27f-4f98-aaad-75ccad9175c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2211366-0089-4c90-8eb7-573e54afb044",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4844aa48-f27f-4f98-aaad-75ccad9175c5",
                    "LayerId": "2a834005-b578-4570-b173-af44bfe96232"
                }
            ]
        },
        {
            "id": "832b2fb8-a86f-47a4-9c8a-28bdf1c6345a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebe1472a-69af-40d1-93c1-f61fbed00889",
            "compositeImage": {
                "id": "7f37e407-f1fe-48a1-95d7-ee7a5175c6e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "832b2fb8-a86f-47a4-9c8a-28bdf1c6345a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82ac77c5-2e73-4b64-b043-d3a7ed9d7ea8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "832b2fb8-a86f-47a4-9c8a-28bdf1c6345a",
                    "LayerId": "2a834005-b578-4570-b173-af44bfe96232"
                }
            ]
        },
        {
            "id": "8edd4e91-3791-4885-8880-ce755199a744",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebe1472a-69af-40d1-93c1-f61fbed00889",
            "compositeImage": {
                "id": "dcf1c102-3b28-4bd2-ae0c-ee8e58b72381",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8edd4e91-3791-4885-8880-ce755199a744",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de6bd4c7-6719-4082-a6e3-190429a062d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8edd4e91-3791-4885-8880-ce755199a744",
                    "LayerId": "2a834005-b578-4570-b173-af44bfe96232"
                }
            ]
        },
        {
            "id": "e62879bf-3d96-4d19-aef9-1b71022cd154",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebe1472a-69af-40d1-93c1-f61fbed00889",
            "compositeImage": {
                "id": "b0b3d8ae-147f-4a9f-8d68-492a3c1a31ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e62879bf-3d96-4d19-aef9-1b71022cd154",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44e9b452-18c6-43ff-9ca0-aa889d7861d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e62879bf-3d96-4d19-aef9-1b71022cd154",
                    "LayerId": "2a834005-b578-4570-b173-af44bfe96232"
                }
            ]
        },
        {
            "id": "b182935b-4745-4181-8a6b-be34348ac51b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebe1472a-69af-40d1-93c1-f61fbed00889",
            "compositeImage": {
                "id": "2608f92d-3329-4cde-9a27-751fd7e6fdf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b182935b-4745-4181-8a6b-be34348ac51b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bde9b7e5-7926-4b47-be04-f1114e42ec83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b182935b-4745-4181-8a6b-be34348ac51b",
                    "LayerId": "2a834005-b578-4570-b173-af44bfe96232"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "2a834005-b578-4570-b173-af44bfe96232",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ebe1472a-69af-40d1-93c1-f61fbed00889",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}