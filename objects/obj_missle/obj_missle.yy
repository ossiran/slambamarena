{
    "id": "32b98697-02c6-46e7-a6c6-4ef30d6677c8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_missle",
    "eventList": [
        {
            "id": "5caf7c93-3d8c-4b7f-bd4c-e463c4d562b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "32b98697-02c6-46e7-a6c6-4ef30d6677c8"
        },
        {
            "id": "c63fc29b-2322-40f9-8a80-51c1fd109e5b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "32b98697-02c6-46e7-a6c6-4ef30d6677c8"
        },
        {
            "id": "e2735a57-59e2-413e-a359-3a578609845c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "32b98697-02c6-46e7-a6c6-4ef30d6677c8"
        },
        {
            "id": "2aee67b8-224b-42bf-bc6e-1e9435adba18",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "32b98697-02c6-46e7-a6c6-4ef30d6677c8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2132d2fb-2953-4d2d-a20d-f37cdc53310f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "141ff86b-6ecc-4fff-beb2-23645844fffa",
    "visible": true
}