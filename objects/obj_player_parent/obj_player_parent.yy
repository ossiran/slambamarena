{
    "id": "764a5c31-d766-4159-bbb8-1162177a46bb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_parent",
    "eventList": [
        {
            "id": "79ac8bbd-522f-4cd1-b28c-a37fcf71f597",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "764a5c31-d766-4159-bbb8-1162177a46bb"
        },
        {
            "id": "c8bc2eb8-59fc-4099-a0c9-f8f2646eac83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "764a5c31-d766-4159-bbb8-1162177a46bb"
        },
        {
            "id": "55bf82a1-5ab4-4de5-9d3e-bc533aa36ff7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "764a5c31-d766-4159-bbb8-1162177a46bb"
        },
        {
            "id": "d1f0451a-66e6-4f89-950f-2f97ae0cacc1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "764a5c31-d766-4159-bbb8-1162177a46bb"
        },
        {
            "id": "ce73efb0-c48c-4201-af9a-44e9c0edbfff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "764a5c31-d766-4159-bbb8-1162177a46bb"
        },
        {
            "id": "aad2d9cb-9163-4ad2-a535-6d53a36edd50",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "764a5c31-d766-4159-bbb8-1162177a46bb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}