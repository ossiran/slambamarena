{
    "id": "c0fce84c-7816-4a88-b247-fa8d37f53891",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_myu_knocked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 4,
    "bbox_right": 55,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6126013e-067c-417e-b1a1-3cd61eff5bb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fce84c-7816-4a88-b247-fa8d37f53891",
            "compositeImage": {
                "id": "430832cd-9bdb-4cd6-b949-bdfed6314c78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6126013e-067c-417e-b1a1-3cd61eff5bb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a0c2bfe-764f-415a-bd05-ca5d9a36725f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6126013e-067c-417e-b1a1-3cd61eff5bb8",
                    "LayerId": "c1199677-51ae-4790-84c0-9badd9f15149"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c1199677-51ae-4790-84c0-9badd9f15149",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0fce84c-7816-4a88-b247-fa8d37f53891",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 6
}