if(state_new == 1)
{
	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -43;
	    image_xscale  = 26;
	    image_yscale  = 55;
	    xshift  = -2;
	}
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 16;
	    image_yscale  = 30;
	    xshift = -2
	}
	chainmove = 0;
	attackframe0 = 6;
	endstate = 24;
}

//animation
sprite_index = spr_bill_mair;

var hitfalladd = 0;

//juggling correctly
with(otherplayer)
{
    if(!onground() || vy < 0)
    {
        hitfalladd = 1;
    }
}

//attack
if(state_timer == attackframe0)
{
    hitbox1 = create_hitbox(-12, -14,49, 28, 2.75,0, 18,19,9,8,0 + hitfalladd,0,10,3.5,"overhead", -1);
    attackhurtbox0 = create_hurtbox(-13 * image_xscale, -14,54 * image_xscale,25,22,-2);
    hitbox1.scaling = 0.75;
    hitbox1.hitsound = snd_hitsound1;
    hitbox1.juggle = 1;
}

//chainmove
if(state_timer > 3)
{
    if(input_check(0,0,0,1,0))
    {
        chainmove = "hair"
    }
}

attack_base(endstate,1,0,1,1,0);

if(state_timer > 0)
{
    image_index = 1;
}

/*
//chainmove
if((hitting == 0 || otherplayer.hitpause > 0) && state_timer > 3)
{
    if(input_check(0,0,0,1,0))
    {
        chainmove = "hair"
    }
}

if(hitting == 1 && !hitpausing() && (input_check(0,0,0,1,0) || chainmove == "hair") && vxwas == 0 && vywas == 0 && vy != 0)
{
    state_switch("hair");
}
*/
