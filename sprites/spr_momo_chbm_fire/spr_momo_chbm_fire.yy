{
    "id": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_chbm_fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 26,
    "bbox_right": 139,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "927b20f9-f5f1-4271-8c3e-f7957b86048e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "a4540477-5103-45ab-84f1-5740457938d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "927b20f9-f5f1-4271-8c3e-f7957b86048e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b98a5e17-0e71-4c49-876b-81e8cc873124",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "927b20f9-f5f1-4271-8c3e-f7957b86048e",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "c66519b9-1e72-4315-add7-22c88742391f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "45635933-bac4-435c-b1f2-31b56799bb10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c66519b9-1e72-4315-add7-22c88742391f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7a3724a-17a6-4967-a3b2-627799fa0b39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c66519b9-1e72-4315-add7-22c88742391f",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "24c7f79b-ddd0-46b5-a57e-f8a4a3e5420d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "67b4c114-6f8b-456d-9128-906c4578d277",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24c7f79b-ddd0-46b5-a57e-f8a4a3e5420d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dbc29d1-60d7-44f6-936d-bc4222fa0684",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24c7f79b-ddd0-46b5-a57e-f8a4a3e5420d",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "2f0f088c-f503-42d9-9fa1-c1e5bc88950b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "4643fa63-363c-4109-a131-a156a756a0d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f0f088c-f503-42d9-9fa1-c1e5bc88950b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a20b204-f445-429a-9336-40307ea30b47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f0f088c-f503-42d9-9fa1-c1e5bc88950b",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "3ba4ab42-548f-4c61-89de-cbc18678b254",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "df0a75de-1ad5-4a69-8dd2-ddb23af68e8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ba4ab42-548f-4c61-89de-cbc18678b254",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e178c1b-102c-432c-98cc-85217a374e2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ba4ab42-548f-4c61-89de-cbc18678b254",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "035417f7-f679-490c-b3de-080a51c84930",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "b45187ea-7aa0-4a6c-a7d0-19e255572dd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "035417f7-f679-490c-b3de-080a51c84930",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2297b2df-4125-4716-b09c-757a3d747576",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "035417f7-f679-490c-b3de-080a51c84930",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "06ee6725-40fc-44e4-af74-ea74cb3d5bdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "ee8db96a-cdcc-4f26-8d4b-bf93503888b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06ee6725-40fc-44e4-af74-ea74cb3d5bdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1472ca7a-8e1c-46e8-bf4d-943d314b5ce3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06ee6725-40fc-44e4-af74-ea74cb3d5bdb",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "1a0a200b-19fc-49bf-a877-d6fbb441f82f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "ed810627-1bd0-4c28-b0d1-ab51149803ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a0a200b-19fc-49bf-a877-d6fbb441f82f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a20a49d2-58df-4501-a7b4-65457e5d4519",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a0a200b-19fc-49bf-a877-d6fbb441f82f",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "24a39906-8bd0-4eef-9187-5ba4e364064b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "10181e1d-ef5e-4e23-9562-ca07bc24753e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24a39906-8bd0-4eef-9187-5ba4e364064b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed418acd-8aac-456c-965b-4e1e563264b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24a39906-8bd0-4eef-9187-5ba4e364064b",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "c9908708-0beb-4294-99a0-f90f96445847",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "ed29bbe3-1611-4d29-b775-37e7135acb7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9908708-0beb-4294-99a0-f90f96445847",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f082029c-20c6-4468-b76e-8b348a2134ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9908708-0beb-4294-99a0-f90f96445847",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "c9ebf3cd-04e2-4b9b-98e2-d74d9609c5a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "fc009898-8b8f-4b0f-9b27-cb44fb22c992",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9ebf3cd-04e2-4b9b-98e2-d74d9609c5a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52bed3ce-a957-4fb6-9dbe-41e445773024",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9ebf3cd-04e2-4b9b-98e2-d74d9609c5a6",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "15b5df2f-366d-4f40-8494-cd63901c88e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "4d8d91d3-03f4-4b3d-aeb3-1d7159250d73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15b5df2f-366d-4f40-8494-cd63901c88e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b41b4025-f110-47ce-af39-77e68f7ae5f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15b5df2f-366d-4f40-8494-cd63901c88e4",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "0f17248c-b64f-4e5f-ab96-66a0d39d8bd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "27edcb9d-1e67-4d3f-99c7-6c1d4eb468b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f17248c-b64f-4e5f-ab96-66a0d39d8bd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8bb1580-02c3-4cc5-97ed-0f177a75340e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f17248c-b64f-4e5f-ab96-66a0d39d8bd7",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "4e57e9ae-63e9-47aa-bc72-4c752e9c43ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "d6cf17c7-6bf0-4789-9781-f6dba5b835ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e57e9ae-63e9-47aa-bc72-4c752e9c43ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c7794bc-3c31-4bff-ba9d-241ac84ccaff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e57e9ae-63e9-47aa-bc72-4c752e9c43ef",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "b34ebe03-8d30-4319-9026-f7869950885d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "50167c4a-2bbb-4aa4-b4df-520c2a449ebb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b34ebe03-8d30-4319-9026-f7869950885d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "254ca4fe-971e-4041-8aa8-f2e3bc63f8bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b34ebe03-8d30-4319-9026-f7869950885d",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "57dfce98-0e76-43f4-81be-e32706f90c48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "1b5a8de4-36e5-4da0-bd42-7354d542142f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57dfce98-0e76-43f4-81be-e32706f90c48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4458634b-1ff7-42ca-9d6a-99379e74f87b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57dfce98-0e76-43f4-81be-e32706f90c48",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "bc2cd748-3381-470e-9af7-20db05408779",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "aed184bf-018a-4f65-93b8-7da38eadb0df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc2cd748-3381-470e-9af7-20db05408779",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4511450-7a24-4f7a-9eaf-3c70b676eb40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc2cd748-3381-470e-9af7-20db05408779",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "2e9f3b87-a5ed-41a0-8fe3-d5f885b9fc7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "4475d05c-cae8-483d-9767-6afa552d49a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e9f3b87-a5ed-41a0-8fe3-d5f885b9fc7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "662ea22d-45e7-469c-ae97-964f809a735d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e9f3b87-a5ed-41a0-8fe3-d5f885b9fc7a",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "3be7eaa5-4d7b-4b1b-9f48-faf0a21958d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "a3f7ceec-fcd2-4968-bc86-1da68be29064",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3be7eaa5-4d7b-4b1b-9f48-faf0a21958d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "532ec60d-8afb-4598-a5d5-ba319a1fbef6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3be7eaa5-4d7b-4b1b-9f48-faf0a21958d2",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "00b9e146-d0e3-4448-a37a-835773c986ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "ef51d453-7988-4d91-844b-7a2659f67d5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00b9e146-d0e3-4448-a37a-835773c986ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98507949-b263-4f47-9081-2873a875600e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00b9e146-d0e3-4448-a37a-835773c986ad",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "daed958d-104e-49d9-a808-2213f661996e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "1667d454-fbdf-465f-a1b7-dc3e559c1478",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "daed958d-104e-49d9-a808-2213f661996e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdbf7f57-62a9-47e2-acda-7801be1f68e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daed958d-104e-49d9-a808-2213f661996e",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "533a622a-ee8c-4cb2-b003-e5a38251870a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "f14d9327-adb8-4895-81aa-cf25b253c2bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "533a622a-ee8c-4cb2-b003-e5a38251870a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ceef63ae-d3b7-4a57-a5ba-954bda8c379f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "533a622a-ee8c-4cb2-b003-e5a38251870a",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "5cbb9c50-f889-4743-b652-e34855984908",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "83587134-9533-4dc0-beac-7052186cdec0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cbb9c50-f889-4743-b652-e34855984908",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56b47224-57fc-435c-af6a-896187870025",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cbb9c50-f889-4743-b652-e34855984908",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "8a6fde1a-3cb3-4703-a4e5-40fa5f83d752",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "3351f041-0035-4976-b682-8e237ad3a837",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a6fde1a-3cb3-4703-a4e5-40fa5f83d752",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71cc2c67-fd1a-4f28-8b3b-cedb31cd79b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a6fde1a-3cb3-4703-a4e5-40fa5f83d752",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "77aecf12-0168-419e-b64a-bcf83b202282",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "b5960b6e-aa16-4210-888c-877c51d8818e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77aecf12-0168-419e-b64a-bcf83b202282",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "072705ce-7240-4108-bf27-2f749d9be07a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77aecf12-0168-419e-b64a-bcf83b202282",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "03d718fb-1340-4619-9e7e-c457ca8e400f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "65493a7e-aa01-4b0d-b054-b97e1eb36918",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03d718fb-1340-4619-9e7e-c457ca8e400f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4adb6e8e-c9ef-472e-ae0f-e8d0d2b4a5b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03d718fb-1340-4619-9e7e-c457ca8e400f",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "dfcb2c50-772b-4287-b671-b91327cb0345",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "771a6fbe-fb52-4600-8df7-80fd70fb497a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfcb2c50-772b-4287-b671-b91327cb0345",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0abc4696-6928-44ad-9118-21e873f16e2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfcb2c50-772b-4287-b671-b91327cb0345",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "517c68d6-18c6-4ac9-8122-0458a3ee8d0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "755a785a-bc86-4a25-a699-295d91051ee2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "517c68d6-18c6-4ac9-8122-0458a3ee8d0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08bf8e2d-82aa-4964-8dfe-0b9c92a6cf3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "517c68d6-18c6-4ac9-8122-0458a3ee8d0a",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "154dbb03-75d8-46e7-9972-13ec90b45a9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "67787f74-8502-4e71-9ded-40a7e2b332a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "154dbb03-75d8-46e7-9972-13ec90b45a9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9e5af57-740b-42e2-aa1d-debb1494c775",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "154dbb03-75d8-46e7-9972-13ec90b45a9e",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "183aca67-db54-4e3f-b60c-1e2ec08e7174",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "485ee665-17f0-4b8e-b714-aec823ce09f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "183aca67-db54-4e3f-b60c-1e2ec08e7174",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c29da28-6bae-4eeb-9d79-a89beb5948c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "183aca67-db54-4e3f-b60c-1e2ec08e7174",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "d2c27eec-8724-4e6b-9d9a-c7699eb5e1b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "79c9996e-80b1-4315-ac32-14497bcf0673",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2c27eec-8724-4e6b-9d9a-c7699eb5e1b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de581fd2-3ac0-4e83-83c6-8a561753d9a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2c27eec-8724-4e6b-9d9a-c7699eb5e1b3",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "4e6ac3b4-f354-44d5-98c6-2de38aff381c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "fd247cf8-1eb1-4795-a686-b17a1837be36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e6ac3b4-f354-44d5-98c6-2de38aff381c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e614a3d-84ed-495b-80c4-16a74ca0ab9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e6ac3b4-f354-44d5-98c6-2de38aff381c",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "1ca1eda4-2c86-4bce-92fb-923f96fdfb2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "da14b6e2-780b-4992-b31a-3e3518c632bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ca1eda4-2c86-4bce-92fb-923f96fdfb2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e3f5da1-c5b4-402a-8c48-24ccc0ef8456",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ca1eda4-2c86-4bce-92fb-923f96fdfb2d",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        },
        {
            "id": "c532c011-9bfc-4338-b062-2808e630fef6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "compositeImage": {
                "id": "a894d658-6594-4d74-b1d4-21a036be5fbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c532c011-9bfc-4338-b062-2808e630fef6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d67ad13a-aa5b-48f6-9d98-be37c3df7899",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c532c011-9bfc-4338-b062-2808e630fef6",
                    "LayerId": "f78e1052-46d1-4d3d-93e3-12d626adf326"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "f78e1052-46d1-4d3d-93e3-12d626adf326",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de7f7457-85ba-42bc-9b53-4a4706c368fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 175,
    "xorig": 63,
    "yorig": 87
}