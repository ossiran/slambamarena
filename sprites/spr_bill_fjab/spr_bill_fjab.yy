{
    "id": "7e319187-9b91-4112-b76a-36ff5880e038",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_fjab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 48,
    "bbox_right": 103,
    "bbox_top": 75,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e1042dd-e453-49b1-b318-227305330fed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "23755436-69cd-4c9f-9fa5-ce5e9341a5ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e1042dd-e453-49b1-b318-227305330fed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a6e45e9-a2e8-44a4-8e04-72077edd7eba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e1042dd-e453-49b1-b318-227305330fed",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "def32862-bc28-4e19-8e5f-2be084364ceb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "784259d0-47d5-454c-9a2a-15e16c198ea1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "def32862-bc28-4e19-8e5f-2be084364ceb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62f7ddb9-0c98-429f-ac9e-6fe80e5e6bcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "def32862-bc28-4e19-8e5f-2be084364ceb",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "dca59a1b-7313-442e-b4db-269f994c258e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "36db917f-31b1-4faa-b9d7-3ce3a4d54b20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dca59a1b-7313-442e-b4db-269f994c258e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a26746b-0815-4ed4-bcbf-7e6655591d8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dca59a1b-7313-442e-b4db-269f994c258e",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "bc1c23b4-b5e8-468e-bd87-8644798521a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "d9abb78d-a3a1-40b2-a630-e3d0ac9ae676",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc1c23b4-b5e8-468e-bd87-8644798521a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "921021d0-82d7-426b-abc7-75352ba4c957",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc1c23b4-b5e8-468e-bd87-8644798521a9",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "41adf762-8b8f-4a40-bc6d-ce097dfb5f59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "37a78934-32d6-4d23-be77-99bc5182edb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41adf762-8b8f-4a40-bc6d-ce097dfb5f59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd8d22a6-e79e-4400-89e0-2215971dde2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41adf762-8b8f-4a40-bc6d-ce097dfb5f59",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "68c47aca-e1a4-4429-9207-4156866eb9aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "f93eb210-6cda-4aa9-98bd-766b02f52ba0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68c47aca-e1a4-4429-9207-4156866eb9aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b734895-38a3-40f5-9654-e965bb0ed053",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68c47aca-e1a4-4429-9207-4156866eb9aa",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "88843095-4d91-4058-945c-b263783ecd97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "0635a0bc-41ba-4d22-b3a5-32662a655554",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88843095-4d91-4058-945c-b263783ecd97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8915b80c-5bbf-49dd-a47e-75e86e7ebc86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88843095-4d91-4058-945c-b263783ecd97",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "f89c9f4e-0be2-4d2b-9e88-eaa8e92bbbea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "6c51ddbf-da44-498d-8d34-e081e4812c21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f89c9f4e-0be2-4d2b-9e88-eaa8e92bbbea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd529723-fdc3-448b-9fc6-d274c79ac6fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f89c9f4e-0be2-4d2b-9e88-eaa8e92bbbea",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "ecf83d2e-ce70-4a82-86a0-774a8334fa6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "d60f4ff4-6330-4502-afa9-1174bd004d69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecf83d2e-ce70-4a82-86a0-774a8334fa6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49c7922a-9e69-498b-8267-ba8814b48c93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecf83d2e-ce70-4a82-86a0-774a8334fa6b",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "e4e1db6e-c0e8-42cc-9952-9710ee2ba761",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "f9ae3233-cb16-4f6b-a9b2-57bd3f508b0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4e1db6e-c0e8-42cc-9952-9710ee2ba761",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bb1425d-6bec-4151-aad7-d52807b68cd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4e1db6e-c0e8-42cc-9952-9710ee2ba761",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "ca21d7d1-344c-4546-8f17-87791f1b0443",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "d95dce21-43c1-4018-bb23-f322198e1e5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca21d7d1-344c-4546-8f17-87791f1b0443",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40debf2e-121a-4de1-b244-99c779b132b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca21d7d1-344c-4546-8f17-87791f1b0443",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "540995c0-8cd2-4173-9c7a-0b64d8d8ecc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "431a53d6-820d-498c-a9d5-6e3bc4195d8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "540995c0-8cd2-4173-9c7a-0b64d8d8ecc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2485774-0d20-4d6f-bc74-e648323aefac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "540995c0-8cd2-4173-9c7a-0b64d8d8ecc3",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "826afcb9-ec96-4136-815e-80155c28006f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "19eb40f3-0ab2-4da4-a0ee-52b65cc9b232",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "826afcb9-ec96-4136-815e-80155c28006f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09ff08ec-35e2-4a38-929f-4731bbdd0658",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "826afcb9-ec96-4136-815e-80155c28006f",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "fdc686af-a54e-4040-8794-e073db2d2e29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "23fbde52-d275-457f-b94a-4a7794e8035b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdc686af-a54e-4040-8794-e073db2d2e29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba78a119-1b98-4f55-90b8-5376c40ab56e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdc686af-a54e-4040-8794-e073db2d2e29",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "eaa231c5-476c-4f06-b5aa-4144cd3c1ad1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "544f8dbf-7417-45d4-9558-d95eb4e36ed6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eaa231c5-476c-4f06-b5aa-4144cd3c1ad1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c31d375-fd3e-4ede-be6e-13dbc34b6a6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eaa231c5-476c-4f06-b5aa-4144cd3c1ad1",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "612502fc-bdac-42b0-874e-4e6f1a877191",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "5b0446cd-405f-4778-8768-76c5ac933457",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "612502fc-bdac-42b0-874e-4e6f1a877191",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25bb5463-8302-4fce-adc2-146add860330",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "612502fc-bdac-42b0-874e-4e6f1a877191",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "e14f1a33-3fb0-4539-b896-f4c83af85e65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "b42de153-56db-4b5b-8ef6-0d535b9b1bfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e14f1a33-3fb0-4539-b896-f4c83af85e65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17494923-ef48-4892-a7f7-c60999ba6bb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e14f1a33-3fb0-4539-b896-f4c83af85e65",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "cf130f0a-e236-4aee-8990-227c5209569e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "8d41f0ab-a1d2-47d2-aad2-666d54198d1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf130f0a-e236-4aee-8990-227c5209569e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6b74044-8ee9-4c25-8161-17eada081cac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf130f0a-e236-4aee-8990-227c5209569e",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "6fc5c2ca-f680-4020-bb19-27b52a64ed8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "30b9d587-b355-477b-90fa-34951983fb1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fc5c2ca-f680-4020-bb19-27b52a64ed8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95aac503-a75b-41a6-b0fb-9fb425be868e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fc5c2ca-f680-4020-bb19-27b52a64ed8f",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "f5df208d-735b-42ee-b085-28da2375ff3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "b509466a-7e1f-4166-9aed-ea469b202cb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5df208d-735b-42ee-b085-28da2375ff3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ac25b96-a573-49d9-a76d-ada6e342f6c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5df208d-735b-42ee-b085-28da2375ff3c",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "6bbc8088-5c93-4ec0-a7d5-5687a9dc2699",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "14232f08-c48f-4071-9a3e-78294c08b4ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bbc8088-5c93-4ec0-a7d5-5687a9dc2699",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0121483f-9ebc-40ef-8176-cf1d3ae45022",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bbc8088-5c93-4ec0-a7d5-5687a9dc2699",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "599088fa-9b71-499f-90fd-84e26dfc3a8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "27a02273-bb92-4b3c-90c2-cf18ec690af0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "599088fa-9b71-499f-90fd-84e26dfc3a8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a45a7381-3890-4326-8618-f7b94fb0914b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "599088fa-9b71-499f-90fd-84e26dfc3a8b",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "1438e08b-b12d-45c9-ae11-774b9f9661b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "97bfe641-d665-42df-ab31-6f5e2c4efcdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1438e08b-b12d-45c9-ae11-774b9f9661b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4871cea8-2d41-4363-93df-b343e699b978",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1438e08b-b12d-45c9-ae11-774b9f9661b6",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "577f10b0-2629-45df-9462-8df0f6a690cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "5c153719-0eef-4d5e-bccc-8d3ddc5d0987",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "577f10b0-2629-45df-9462-8df0f6a690cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84916ea3-204f-4dca-b50d-15532ec85a2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "577f10b0-2629-45df-9462-8df0f6a690cf",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "992f5849-f0c9-44b2-ba20-0bdcaf225859",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "ea787598-4a96-4903-ad90-5f60f864f0f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "992f5849-f0c9-44b2-ba20-0bdcaf225859",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e4ba8db-df3a-4b5c-9b2b-76ecd71d8caa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "992f5849-f0c9-44b2-ba20-0bdcaf225859",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "0984ee32-8a3e-4bfa-ac76-4263479cbb23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "860d2bd6-a1fb-46b6-a382-c0058c23efa7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0984ee32-8a3e-4bfa-ac76-4263479cbb23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b794fbae-9406-4f7d-83f6-9ad1cea86831",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0984ee32-8a3e-4bfa-ac76-4263479cbb23",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "66570e4b-daa9-4313-ad9c-334c6f366cdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "b2753618-7094-4f52-bba7-d57dec86fb5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66570e4b-daa9-4313-ad9c-334c6f366cdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5433bfc0-e4aa-41b7-a67d-4967650c9cc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66570e4b-daa9-4313-ad9c-334c6f366cdd",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "ade6b48d-124b-40c9-a1ba-73e0f37d5c1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "369e3a27-1a74-4f95-86c2-1aa23b473a8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ade6b48d-124b-40c9-a1ba-73e0f37d5c1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40833c80-a4f6-4d19-b658-1662ce03ac7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ade6b48d-124b-40c9-a1ba-73e0f37d5c1e",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "e11fe9c9-1551-4ea1-a480-93b88ff5b4e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "88b491e9-68d6-4583-97f0-e96b72a0d806",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e11fe9c9-1551-4ea1-a480-93b88ff5b4e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee7c6b61-1090-4a64-93b4-d30d7e874f0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e11fe9c9-1551-4ea1-a480-93b88ff5b4e4",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "44e156f7-ae33-44a2-82d4-f6f29b2e25f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "f130a7b3-fc37-4c04-b0e0-6d5e7c2c166f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44e156f7-ae33-44a2-82d4-f6f29b2e25f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4ce76bb-34eb-4695-a355-d384825e6b61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44e156f7-ae33-44a2-82d4-f6f29b2e25f4",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        },
        {
            "id": "abca87c6-59d3-4b25-a277-df7cf6ceec0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "compositeImage": {
                "id": "6649b246-239f-4b05-bb6e-3f092cb96fa5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abca87c6-59d3-4b25-a277-df7cf6ceec0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ad9293c-7ddc-48f0-9304-a5ffe1791726",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abca87c6-59d3-4b25-a277-df7cf6ceec0d",
                    "LayerId": "3873be95-f3aa-48fb-9fa1-663cb6a0daec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "3873be95-f3aa-48fb-9fa1-663cb6a0daec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7e319187-9b91-4112-b76a-36ff5880e038",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}