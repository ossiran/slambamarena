{
    "id": "fa9842d8-30ad-4fd8-8970-28b3ccf47aa4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_rising",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 120,
    "bbox_left": 33,
    "bbox_right": 109,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "abbc06b5-1ef1-4674-9bd7-a5b46f6c78d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa9842d8-30ad-4fd8-8970-28b3ccf47aa4",
            "compositeImage": {
                "id": "9a9e6d26-a5e6-48bd-8799-90eb14552b1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abbc06b5-1ef1-4674-9bd7-a5b46f6c78d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a62556f6-dfc3-4039-a3be-a290db4c87f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abbc06b5-1ef1-4674-9bd7-a5b46f6c78d5",
                    "LayerId": "54ea5fb3-5718-4f01-8f07-fdf08612dc5a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "54ea5fb3-5718-4f01-8f07-fdf08612dc5a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa9842d8-30ad-4fd8-8970-28b3ccf47aa4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 63,
    "yorig": 87
}