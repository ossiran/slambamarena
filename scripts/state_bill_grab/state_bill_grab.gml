if(state_new == 1)
{
	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -43;
	    image_xscale  = 26;
	    image_yscale  = 75;
	    xshift  = -2;
	}
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 16;
	    image_yscale  = 47;
	    xshift = -2
	}
	
	attackframe0 = 3;
	endstate = 15;
}
vx = 0;
//animation
sprite_index = spr_bill_sgrab;

//attack
if(state_timer == attackframe0)
{
    create_grabbox(6, -25, 30, 18, 2, -2);
    attackhurtbox0 = create_hurtbox(12 * image_xscale, -30,24 * image_xscale,22,2,-2);
}

attack_base(endstate,0,0,1,1,0);

if(otherplayer.grabbed == 1 && otherplayer.state_name  == "grabbed")
{
    if(input_check(4,0,0,0,0))
    {
        state_switch("bthrow");
    }
    else
    {
        state_switch("fthrow");
    }
}


if(otherplayer.grabbed == -1)
{
    state_switch("grabtech");
}
