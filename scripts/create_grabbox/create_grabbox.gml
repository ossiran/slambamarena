///create_hurtbox(xoffset,yoffset,xscale,yscale,duration,xshift)

var grabbox0 = instance_create_depth(x,y,120,obj_grabbox);

grabbox0.owner           =    other;
grabbox0.image_xscale    =    argument2 * image_xscale;
grabbox0.image_yscale    =    argument3;
grabbox0.xoffset         =    argument0 * image_xscale;
grabbox0.yoffset         =    argument1;
grabbox0.duration        =    argument4;
grabbox0.xshift          =    argument5;

return grabbox0;