{
    "id": "548b25f9-6723-40f0-af0f-1c3141346ccc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hurtbox",
    "eventList": [
        {
            "id": "571b1d1b-b5ce-406c-8fbe-972d0f34f309",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "548b25f9-6723-40f0-af0f-1c3141346ccc"
        },
        {
            "id": "09986103-c3de-43d1-b319-b85f4cf21773",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "548b25f9-6723-40f0-af0f-1c3141346ccc"
        },
        {
            "id": "804ca747-afe5-4454-a3a2-4bb31997eb72",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "548b25f9-6723-40f0-af0f-1c3141346ccc"
        },
        {
            "id": "793c6036-0784-4b6a-aa09-2883044e6c29",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "548b25f9-6723-40f0-af0f-1c3141346ccc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4df172bc-8b0c-49e1-b80b-75b9b48af3be",
    "visible": true
}