{
    "id": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dsyn_qcfl",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 43,
    "bbox_right": 122,
    "bbox_top": 43,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aac1f14d-1d5f-4020-9b41-020c808bd342",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "f566ae30-7853-438f-b668-1ff10073ecc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aac1f14d-1d5f-4020-9b41-020c808bd342",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "008e307f-68c1-44b7-a3b2-d9430b9b28cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aac1f14d-1d5f-4020-9b41-020c808bd342",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "2ca47fea-6db3-4681-ba3d-e77354b2de1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "1e0e947f-1130-4999-b0f1-3dfcc42bc83b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ca47fea-6db3-4681-ba3d-e77354b2de1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6be2a3c9-a984-47ce-97a9-f170a03d98eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ca47fea-6db3-4681-ba3d-e77354b2de1c",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "9790f349-6cab-4f56-9cb7-26c0d2e70886",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "f4e123a3-b7ac-476f-8cb6-d0699b592c84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9790f349-6cab-4f56-9cb7-26c0d2e70886",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9afc183-5704-482f-b8ac-40ba64df177a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9790f349-6cab-4f56-9cb7-26c0d2e70886",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "3d54481c-e041-406a-83f7-0e5c6b65aca5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "1c032f75-67b2-4280-b9c8-d3245835f3a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d54481c-e041-406a-83f7-0e5c6b65aca5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18ac2e16-bcb5-40e0-aa0a-2e1df21e8386",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d54481c-e041-406a-83f7-0e5c6b65aca5",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "16515803-22e9-4d48-9cd0-ab571360d5c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "ae74ec85-b7b1-4f1e-a88f-c16bf5943781",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16515803-22e9-4d48-9cd0-ab571360d5c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "239806cd-27f3-4b2a-b02d-fd0beaff58b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16515803-22e9-4d48-9cd0-ab571360d5c0",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "0fa7c986-b2ba-40e1-be60-50db85d90d8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "549d60ca-98db-41ef-b16c-90abb5cf50d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fa7c986-b2ba-40e1-be60-50db85d90d8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cbdffb0-9046-4e3d-a14e-f8f2def4b27b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fa7c986-b2ba-40e1-be60-50db85d90d8c",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "5e8dcafa-f93b-4965-8d02-87d353d232a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "33fc68d9-8644-4ab5-9e08-47015b49b76d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e8dcafa-f93b-4965-8d02-87d353d232a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44ece29c-5f92-4982-b123-8e0726118bcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e8dcafa-f93b-4965-8d02-87d353d232a0",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "d9df9e00-66bd-4b5f-939a-10b4947161a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "9641d3e0-c893-48d9-b2dd-81c05668517d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9df9e00-66bd-4b5f-939a-10b4947161a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c60f8dd-40a4-489f-bc49-056c706e0e55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9df9e00-66bd-4b5f-939a-10b4947161a4",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "b14b37d7-536c-4072-ab10-cc9ed21c3966",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "2b0e1b2e-c412-46b7-a3dc-77c17c9b33fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b14b37d7-536c-4072-ab10-cc9ed21c3966",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "287983e6-407b-4325-a2c7-00595242b4b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b14b37d7-536c-4072-ab10-cc9ed21c3966",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "f7c44e06-d2c3-4c41-b447-d6733c4866fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "31c0d4f4-1001-48dc-a864-256760da96f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7c44e06-d2c3-4c41-b447-d6733c4866fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd7a4376-d27c-44a9-8f0b-c25b53a5e9e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7c44e06-d2c3-4c41-b447-d6733c4866fc",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "ed68f6ad-c045-4e82-8096-9d64a42c4eb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "f674eddd-cf3a-45c3-bb80-e7bb9cb67f08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed68f6ad-c045-4e82-8096-9d64a42c4eb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "288d5685-27dc-420f-8fb3-f107905fc872",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed68f6ad-c045-4e82-8096-9d64a42c4eb8",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "f0d08448-d070-4707-b988-c799ac878e59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "f304835b-69fa-4731-858c-2c6dcf22e359",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0d08448-d070-4707-b988-c799ac878e59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80eca02a-1db6-416f-a330-6bd09a95ae78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0d08448-d070-4707-b988-c799ac878e59",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "d8a89b02-6509-424a-a6c9-1700bdfb3d1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "d0592f29-6bef-4a24-9e68-60035a5b468e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8a89b02-6509-424a-a6c9-1700bdfb3d1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29753fbf-a7d2-46e2-b09d-2adb0c590edd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8a89b02-6509-424a-a6c9-1700bdfb3d1a",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "09799b41-9010-4506-978b-b5bacc200a33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "fab42763-0004-4179-8001-bb682df1925b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09799b41-9010-4506-978b-b5bacc200a33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e38aecc-91af-4c41-9633-8715d42a2cbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09799b41-9010-4506-978b-b5bacc200a33",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "4f2823b6-b26c-490f-b575-59e689e5c677",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "14b6f4be-a4e9-4b48-ab21-5e96de09bdb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f2823b6-b26c-490f-b575-59e689e5c677",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "702827d4-883c-47b3-be8a-d421c873b120",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f2823b6-b26c-490f-b575-59e689e5c677",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "f0b5ff24-304c-4b97-99aa-836fde666bb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "876c2803-0a46-443c-8421-3da0ecf510f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0b5ff24-304c-4b97-99aa-836fde666bb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "153e2df6-bf92-45a7-b9b5-85c53ca9e472",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0b5ff24-304c-4b97-99aa-836fde666bb3",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "c42d5f52-1ffb-4052-b3a8-7b02c3bc8182",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "173c8aa4-cae5-4af1-9061-1b320b035ecf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c42d5f52-1ffb-4052-b3a8-7b02c3bc8182",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fae8e2fb-6c5f-4cf2-a29b-0b06ca76346d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c42d5f52-1ffb-4052-b3a8-7b02c3bc8182",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "1fdac4c5-0551-4fb6-8860-63532dea1262",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "321ef5a3-3dde-4d6f-b75d-c04330ac4da5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fdac4c5-0551-4fb6-8860-63532dea1262",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25b9cb92-d84e-47ba-8099-c1774bb39491",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fdac4c5-0551-4fb6-8860-63532dea1262",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "c05662ad-f2da-4603-88aa-43b8055b8192",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "26b3d1c9-845b-41f1-8a27-dd21e932449d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c05662ad-f2da-4603-88aa-43b8055b8192",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc861575-ba9e-423b-a41c-5b90fc0cf828",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c05662ad-f2da-4603-88aa-43b8055b8192",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "805f85b6-0d03-4563-bdfd-4a63a310e317",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "59ef097d-81be-40bb-a7fc-eca20b9e2303",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "805f85b6-0d03-4563-bdfd-4a63a310e317",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2eca470-4c88-403b-951e-afb0a8b22b75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "805f85b6-0d03-4563-bdfd-4a63a310e317",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "237f4f3a-e4cc-4926-b0ba-c5580b8a3568",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "ce9adf6f-68dc-463c-b2c9-db8a8e7760b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "237f4f3a-e4cc-4926-b0ba-c5580b8a3568",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84701005-0378-46f8-8d10-4f7fbd84bbb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "237f4f3a-e4cc-4926-b0ba-c5580b8a3568",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "2760c55f-d1ab-4bc3-805f-49e2b089af67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "823e1feb-02ec-4b92-a64c-3c2b0a3ec7b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2760c55f-d1ab-4bc3-805f-49e2b089af67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e677a20b-a313-4230-9745-5c9e9d41b9b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2760c55f-d1ab-4bc3-805f-49e2b089af67",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "85e3cca2-747d-4703-bec7-978aede3a307",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "5e38f4e0-c24d-4dd8-933a-aadc10085e21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85e3cca2-747d-4703-bec7-978aede3a307",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22cd970a-f502-4365-b361-a74a211d587d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85e3cca2-747d-4703-bec7-978aede3a307",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "0cd3d2ff-b8c1-45ef-a6e0-88484fd30c74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "7beec220-603f-4418-b989-5ff3851717be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cd3d2ff-b8c1-45ef-a6e0-88484fd30c74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a800870-3226-4c08-b0d0-fd883253e7b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cd3d2ff-b8c1-45ef-a6e0-88484fd30c74",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "b936b8b4-e550-4499-bebb-e6a4c7cc8152",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "b43d9f92-629c-47dc-9818-fe712a055dba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b936b8b4-e550-4499-bebb-e6a4c7cc8152",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5121d8dc-1b76-4764-808b-171501dc0c19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b936b8b4-e550-4499-bebb-e6a4c7cc8152",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "c8161864-4170-4744-8d5e-d7f2a5789396",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "f79806bf-f036-4a55-a1a2-7ba0c3aee7eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8161864-4170-4744-8d5e-d7f2a5789396",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6211f51-a616-4575-aa52-e5c5695d18bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8161864-4170-4744-8d5e-d7f2a5789396",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "3d5d093d-3419-4de2-8229-7e27cbbb2e14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "99776957-74df-4a2c-a118-6d051dae7545",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d5d093d-3419-4de2-8229-7e27cbbb2e14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b297c7a3-a6f6-49c2-8f06-c1fab4d82d1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d5d093d-3419-4de2-8229-7e27cbbb2e14",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "1b8db29e-2fce-455e-bc3e-c37b73657069",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "ea82672d-f433-4a28-a6ab-2c1485cb1002",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b8db29e-2fce-455e-bc3e-c37b73657069",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48fa35f9-6a27-4a8f-a79e-e2970fd994ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b8db29e-2fce-455e-bc3e-c37b73657069",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "0e285a07-c32d-4fe4-8dd1-0b5dfabbe168",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "feb2d8be-6cc6-40b4-999e-5765dd9c531e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e285a07-c32d-4fe4-8dd1-0b5dfabbe168",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbc0f763-c409-457f-a004-ebe94c26b6de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e285a07-c32d-4fe4-8dd1-0b5dfabbe168",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "26b5ffed-8035-4900-b4d8-06585f0dbe31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "d550ddaf-cf86-4b43-ae51-69540e6c1006",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26b5ffed-8035-4900-b4d8-06585f0dbe31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d130370b-d38c-4d59-9e12-e9c7d1fc0478",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26b5ffed-8035-4900-b4d8-06585f0dbe31",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "7c1e1c4b-68f7-4a4a-bd8f-7257f6ffd470",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "91e4bff5-1d2e-43ae-803d-273b9c2cc8cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c1e1c4b-68f7-4a4a-bd8f-7257f6ffd470",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ccb7778-0a72-4da7-b806-cb15af5f99b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c1e1c4b-68f7-4a4a-bd8f-7257f6ffd470",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "4dc4110b-5d1d-400a-a536-9d13b0343708",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "f1f83443-e08c-443b-aeee-0679fc3c0be4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dc4110b-5d1d-400a-a536-9d13b0343708",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a882611-b58f-47dd-8703-476fa86c7cda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dc4110b-5d1d-400a-a536-9d13b0343708",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "6ed6659d-f3cb-4fa9-980e-f10555a42fde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "9fe6b544-b5df-4465-a077-7d604ec1aebc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ed6659d-f3cb-4fa9-980e-f10555a42fde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8e905df-43c8-4ce0-8c3e-ebc74f206491",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ed6659d-f3cb-4fa9-980e-f10555a42fde",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "3a5c2157-9d0e-4748-920e-bca88da7e575",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "8951b9c8-ee93-4bc5-96c9-55fecaa0dd3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a5c2157-9d0e-4748-920e-bca88da7e575",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0e76a0c-c1ab-43b9-a675-791723b71e41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a5c2157-9d0e-4748-920e-bca88da7e575",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "fea78530-ea30-4bde-909b-5de4fec084dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "50f1bbf1-feac-4b42-8dfa-d6c17362ef8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fea78530-ea30-4bde-909b-5de4fec084dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8333041a-f02c-4375-99b0-858fb7f2b9b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fea78530-ea30-4bde-909b-5de4fec084dc",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "d9926828-5339-4500-b542-f44acb190a41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "32d1d5c3-3c24-4043-b25f-213a46fe990d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9926828-5339-4500-b542-f44acb190a41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9725473-d636-4329-97a2-28ab3eefaa1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9926828-5339-4500-b542-f44acb190a41",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "1cdb36ef-8010-4aeb-bdb8-d5999abdd348",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "a4a17d16-3c98-4d77-b8b5-e05662d93a52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cdb36ef-8010-4aeb-bdb8-d5999abdd348",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6787a5a1-90cb-450f-8198-017ddcac3d98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cdb36ef-8010-4aeb-bdb8-d5999abdd348",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "693b5a7c-e630-4c99-acd2-509140598ffd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "3f7afd2b-0679-4e5f-b3ff-ce33392b1ee3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "693b5a7c-e630-4c99-acd2-509140598ffd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56ab8651-192c-45ef-b895-18b5b80e9b37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "693b5a7c-e630-4c99-acd2-509140598ffd",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "668ad215-9bde-4a67-a8b4-0c674ac350f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "75c5d9c2-1486-4749-8943-f33be703565d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "668ad215-9bde-4a67-a8b4-0c674ac350f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a84b61f-3109-4e00-88dd-0659b85a41a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "668ad215-9bde-4a67-a8b4-0c674ac350f3",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        },
        {
            "id": "a735db8d-8f75-41be-a93b-0d7a1740126d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "compositeImage": {
                "id": "9d3c209e-8619-4f5b-b2f4-3d1d9e66190d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a735db8d-8f75-41be-a93b-0d7a1740126d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c212c50-49f3-45f5-b67d-64b65032d7ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a735db8d-8f75-41be-a93b-0d7a1740126d",
                    "LayerId": "e5050ef1-3e85-4ec2-9851-f025596a3a9b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "e5050ef1-3e85-4ec2-9851-f025596a3a9b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "09e685c1-3802-42d4-84d6-e8d4d67177a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}