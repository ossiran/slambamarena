if(state_new == 1)
{
	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -42;
	    xscale  = 26;
	    yscale  = 74;
	    xshift  = -2;
	}
	
			with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 16;
	    image_yscale  = 47;
	    xshift = -2;
	}
	attackframe0 =  27;
	endstate = 75;
	
    ex += -50;
    
    obj_system.pause = 23;
    obj_system.unpaused_player = player_num;
    /*
    otherplayersprites[0] = otherplayer.sprite_index;
    otherplayersprites[1] = otherplayer.image_index;
    otherplayersprites[2] = otherplayer.x;
    otherplayersprites[3] = otherplayer.y;
    otherplayersprites[4] = otherplayer.image_xscale;

    instance_deactivate_object(otherplayer);
    */
}

//redo super pause 

vx = 0;
vy = 0;

if(state_timer == attackframe0)
{
    hitbox2 = create_hitbox(5, -36,2, 16, 3.65,0, 10 + otherplayer.scaling/3,16,8,12,0,0,1,-11,"projectilemid","billnspecial");
    with(hitbox2)
    {
        while(!place_meeting(x,y,obj_solid))
        {
            image_xscale += 1 * sign(image_xscale);
        }
    }
	hitbox2.scaling = 1;
	hitbox2.chipdamage = 3;
	audio_play_sound(snd_laser,1,0);
}

if(state_timer == attackframe0 + 3)
{
    hitbox2 = create_hitbox(5, -36,2, 16, 3.65,0, 10 + otherplayer.scaling/3,16,8,12,0,0,1,-11,"projectilemid","billnspecial");
    with(hitbox2)
    {
        while(!place_meeting(x,y,obj_solid))
        {
            image_xscale += 1 * sign(image_xscale);
        }
    }
	hitbox2.scaling = 1;
	hitbox2.chipdamage = 3;
	hitbox2.image_alpha = 0.3;
}

if(state_timer == attackframe0 + 6)
{
    hitbox2 = create_hitbox(5, -36,2, 16, 3.65,0, 10 + otherplayer.scaling/3,16,8,12,0,0,1,-11,"projectilemid","billnspecial");
    with(hitbox2)
    {
        while(!place_meeting(x,y,obj_solid))
        {
            image_xscale += 1 * sign(image_xscale);
        }
    }
	hitbox2.scaling = 1;
	hitbox2.chipdamage = 3;
	hitbox2.image_alpha = 0.3;
}

if(state_timer == attackframe0 + 9)
{
    hitbox2 = create_hitbox(5, -36,2, 16, 3.65,0, 10 + + otherplayer.scaling/3,16,8,12,0,0,1,-10,"projectilemid","billnspecial");
    with(hitbox2)
    {
        while(!place_meeting(x,y,obj_solid))
        {
            image_xscale += 1 * sign(image_xscale);
        }
    }
	hitbox2.scaling = 1;
	hitbox2.chipdamage = 3;
	hitbox2.image_alpha = 0.3;
}

if(state_timer == attackframe0 + 12)
{
    hitbox2 = create_hitbox(5, -36,2, 16, 3.65,0, 10 + otherplayer.scaling/3,16,8,12,0,0,1,-10,"projectilemid","billnspecial");
    with(hitbox2)
    {
        while(!place_meeting(x,y,obj_solid))
        {
            image_xscale += 1 * sign(image_xscale);
        }
    }
	hitbox2.scaling = 1;
	hitbox2.chipdamage = 3;
	hitbox2.image_alpha = 0.3;
}

if(state_timer == attackframe0 + 15)
{
    hitbox2 = create_hitbox(5, -36,2, 16, 8,-7, 10 + otherplayer.scaling,8,10,12,-1,0,1,-10,"projectilemid","billnspecial");
    with(hitbox2)
    {
        while(!place_meeting(x,y,obj_solid))
        {
            image_xscale += 1 * sign(image_xscale);
        }
    }
	hitbox2.scaling = 1;
	hitbox2.chipdamage = 3;
	hitbox2.image_alpha = 0.3;
}

sprite_index = spr_bill_fspecial;

attack_base(endstate,0,1,0,0,0);