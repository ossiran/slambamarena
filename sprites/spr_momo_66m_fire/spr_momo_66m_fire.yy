{
    "id": "3e2b5253-4f2e-4764-a614-2ec0df8cd1a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_66m_fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3f06c4af-0f4d-43d1-96cf-e47145c4280c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e2b5253-4f2e-4764-a614-2ec0df8cd1a1",
            "compositeImage": {
                "id": "2a91b122-ca46-4ce0-8102-577ab8959487",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f06c4af-0f4d-43d1-96cf-e47145c4280c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edcc0cc5-09e9-42c8-bcf4-f7427cb02681",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f06c4af-0f4d-43d1-96cf-e47145c4280c",
                    "LayerId": "a79e2a32-a6ec-49c8-93f4-48789b253eb0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a79e2a32-a6ec-49c8-93f4-48789b253eb0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e2b5253-4f2e-4764-a614-2ec0df8cd1a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}