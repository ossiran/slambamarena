{
    "id": "ae25fd70-f531-4401-8a1b-292a800e7f55",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_fairdash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 145,
    "bbox_left": 31,
    "bbox_right": 88,
    "bbox_top": 75,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e016ffb-f75a-4eb9-8945-476761db8750",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae25fd70-f531-4401-8a1b-292a800e7f55",
            "compositeImage": {
                "id": "5196173a-9f6c-4c41-ada8-200996208b77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e016ffb-f75a-4eb9-8945-476761db8750",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "695700ea-29f7-4905-b808-8397f38522d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e016ffb-f75a-4eb9-8945-476761db8750",
                    "LayerId": "a2df1320-a3e3-42b1-ba0f-2ee5c5490ac0"
                }
            ]
        },
        {
            "id": "d0bd6a57-dbc4-4d77-8d38-16059814a4e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae25fd70-f531-4401-8a1b-292a800e7f55",
            "compositeImage": {
                "id": "76991c45-f1c4-44d2-9892-c9e7d6ef0899",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0bd6a57-dbc4-4d77-8d38-16059814a4e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb984f2e-da17-42f0-a305-b4ea70d3f733",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0bd6a57-dbc4-4d77-8d38-16059814a4e7",
                    "LayerId": "a2df1320-a3e3-42b1-ba0f-2ee5c5490ac0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "a2df1320-a3e3-42b1-ba0f-2ee5c5490ac0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae25fd70-f531-4401-8a1b-292a800e7f55",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}