{
    "id": "04e0ff6d-2d84-42cb-ab21-df2b302027dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_cmair",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 144,
    "bbox_left": 43,
    "bbox_right": 97,
    "bbox_top": 70,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "27348abc-82dd-4ef8-8ee0-092188a5cb94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04e0ff6d-2d84-42cb-ab21-df2b302027dc",
            "compositeImage": {
                "id": "667eeac4-13ee-4e40-9ba3-4128660880fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27348abc-82dd-4ef8-8ee0-092188a5cb94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8576a88c-be42-4a93-bb19-33a623831a3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27348abc-82dd-4ef8-8ee0-092188a5cb94",
                    "LayerId": "5e861a97-e66d-4222-9fbb-0462b3bf3710"
                }
            ]
        },
        {
            "id": "f49e3cb0-3909-44f6-bf3f-ca91948c9bfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04e0ff6d-2d84-42cb-ab21-df2b302027dc",
            "compositeImage": {
                "id": "68988ee9-bf65-406f-b22f-0c0cb0779a59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f49e3cb0-3909-44f6-bf3f-ca91948c9bfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11f035e8-c774-4fc0-a832-e7c320a4b868",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f49e3cb0-3909-44f6-bf3f-ca91948c9bfa",
                    "LayerId": "5e861a97-e66d-4222-9fbb-0462b3bf3710"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "5e861a97-e66d-4222-9fbb-0462b3bf3710",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "04e0ff6d-2d84-42cb-ab21-df2b302027dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}