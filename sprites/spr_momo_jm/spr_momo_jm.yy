{
    "id": "a84ed018-1459-4f67-bbfa-1d777b5e17a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_jm",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "845289cf-2f76-4280-a63c-dfe65c232423",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a84ed018-1459-4f67-bbfa-1d777b5e17a8",
            "compositeImage": {
                "id": "26e0d3ad-ef5e-405e-af89-47630785dd15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "845289cf-2f76-4280-a63c-dfe65c232423",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49b4ea36-d56f-443d-9a94-97a1038482d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "845289cf-2f76-4280-a63c-dfe65c232423",
                    "LayerId": "07e7f3f3-a209-453a-905a-8035fb276a77"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "07e7f3f3-a209-453a-905a-8035fb276a77",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a84ed018-1459-4f67-bbfa-1d777b5e17a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}