/// @description Insert description here
// You can write your code in this editor

image_alpha  =   obj_system.showinvis * 0.3;
owner        =   0;
xoffset      =   0;
yoffset      =   0;
xhit         =   0;
yhit         =   0;
airxtraxhit  =   0;
airxtrayhit  =   0;
counterxhit  =   0;
counteryhit  =   0;
damage       =   0;
chipdamage   =   0;
hitstun      =   0;
airxtrahitstun=  0;
hitpause     =   0;
blockstun    =   0;
hitfall      =   0;
bounce       =   0;
duration     =   0;
exgive       =   0;
group        =   -1;
modifier     =   -1;
hitsound     =   0;
stagger      =   0;
counterstagger = 0;
scaling     = 1;
xshift       =   0;
juggle = 0;

projectile = 0;

hitted = 0;
hitsparkcounter = 0;

following = -1;

counter = 1;

extrasprite = -1;

vx = 0;
vy = 0;

prox = 0;