{
    "id": "efc517ba-16ba-4927-93fa-07a132f4031e",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "THEFONT",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Impact",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "88f80d41-bf2d-4748-902a-d05c69773334",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 37,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 247,
                "y": 197
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "20155d36-fa0a-4f4c-a72d-c324cdaf80f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 37,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 192,
                "y": 197
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "c3a6fe21-692b-44b3-8e9d-5bbf470b1a87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 37,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 217,
                "y": 158
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "ccedba9e-b61d-48a8-8c69-0816d7e21b12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 19,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a1e3d504-3c8a-45be-b430-e51978b4abcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "30b56ad7-2fe0-4f7b-bff0-c472cb21d0c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 21,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "191c85d5-b376-4366-b268-0350a674d8c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 18,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "b327dce7-8c78-456c-9f4a-0c2ad3a850c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 37,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 210,
                "y": 197
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b2433448-c89d-4ae4-b086-215a89e3d0b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 37,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 88,
                "y": 197
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "5674f0b1-6f82-42e3-bf77-9b27f3a569d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 37,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 99,
                "y": 197
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "17d410f1-fcdf-415f-b34d-1fd6012a8207",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 77,
                "y": 197
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "4c2a227f-eba8-4a36-80c8-ec3ba9a867e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 2,
                "y": 80
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "95d84a39-226b-46d7-938b-a7cea44374ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 37,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 226,
                "y": 197
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "038ae4ce-f785-46e2-bac9-f1a2e76d36ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 37,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 110,
                "y": 197
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "9323ca5f-1d11-4777-86e1-6f708face526",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 37,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 218,
                "y": 197
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "b15cb4f6-c8b2-4515-84a4-51defe899b6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 37,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 133,
                "y": 158
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "042111c9-2f7a-45b8-8df1-788baa177b83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 172,
                "y": 119
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "da1ba05f-8218-4b12-9b46-61aed30417fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 37,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 204,
                "y": 158
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "b47ced89-1f69-4841-85de-7cd78b793f99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 53,
                "y": 158
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "ad3aedde-2028-486b-9eb1-2d2fcd76b9bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 16,
                "x": 237,
                "y": 41
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "8d1b8b27-43c0-4192-bd91-e5f6c0d851e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 219,
                "y": 41
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "47292400-bce2-4535-ac86-543812dca24a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 19,
                "y": 158
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "bc2edfe7-f973-4e46-88b2-26525cbb5e90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 155,
                "y": 119
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "bc59a578-1e4e-482f-8d21-a7f21a95407c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 37,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 190,
                "y": 158
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "3bed4d88-b652-4c32-9db4-a43c283e7d85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 36,
                "y": 158
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "78ef185c-b110-438d-bc43-77aa07fc8a52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 189,
                "y": 119
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "ab54ff0d-80ac-4b0b-ac3d-ede5141b3c75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 37,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 233,
                "y": 197
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "5f5cbc2d-dd1a-4b6e-bbd8-ff394fe72818",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 37,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 240,
                "y": 197
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "6169233d-6036-4169-b565-b7d280027226",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 165,
                "y": 41
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "b87cf761-c817-4643-be7d-8de79abcd597",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 147,
                "y": 41
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "50fb1d74-7fb6-4bb3-8790-3c44b7e7ad61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 129,
                "y": 41
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "79694e0d-e041-4fa2-91b6-ddfd490f54d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 138,
                "y": 119
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "199c187d-642b-4177-941a-78ca2c416772",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 37,
                "offset": 1,
                "shift": 24,
                "w": 24,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "e21e282d-c3a3-42a9-a973-f22f94d4d718",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 2,
                "y": 41
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b890dcd1-4f1c-4b3c-9766-06820644217e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 75,
                "y": 41
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "39a86a64-bed3-446f-9f25-5a20119079de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 57,
                "y": 41
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "837df4cf-0757-48b4-8902-6eb0e2666a5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 183,
                "y": 41
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "71c1bcc0-1efe-47c1-8add-4c1ee053e929",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 148,
                "y": 158
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "bec8df64-437c-4a16-af14-46e75a5053cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 37,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 162,
                "y": 158
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "7ce3bb03-fa41-45c3-ac34-f7b3d86cf8d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 2,
                "y": 158
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "bfb41698-aa99-4e26-951d-61174510c1c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 223,
                "y": 119
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "820b641b-49c4-4455-8041-fd04d70508fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 37,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 201,
                "y": 197
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "f9444c75-1368-4a81-aad1-5850f9b138e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 37,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 53,
                "y": 197
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "ddcee408-0c71-4546-a45c-8448852fbfc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 93,
                "y": 41
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "db7a74ff-73f3-4815-9980-bf3155341932",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 37,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 15,
                "y": 197
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "06f90d2a-80f4-4997-855d-9aaa61d6e237",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 37,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 127,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "8964e83a-f0b7-47b9-9ad5-b03e4c33fa5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 206,
                "y": 119
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "0645b2df-7472-4ec0-8d63-5950634abaf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 121,
                "y": 119
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "c76a13db-1be5-496b-8424-d812c2b6c76b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 102,
                "y": 158
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "185d71d2-ebb4-4a31-bd1c-627da51857a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 87,
                "y": 119
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "8a11cecf-ef99-49a3-b0ca-e5d8c89b6091",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 54,
                "y": 80
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "02dbb3b1-4989-4537-b9fa-165f911d0fdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 16,
                "x": 21,
                "y": 41
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "773e3fd3-59af-4ba4-84c9-955732212515",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 88,
                "y": 80
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "fc99b272-840b-416f-8c50-dfa3b135d7c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 105,
                "y": 80
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "cf82a542-ecaa-4199-bef1-43a7207ef218",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 18,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "9af3fa32-047c-4ae8-bca9-1373a145877f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 37,
                "offset": 0,
                "shift": 25,
                "w": 26,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "b087ea7f-80fd-4b3e-b95d-eb99b33fa362",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 111,
                "y": 41
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "5561101d-d189-41af-8492-101350f39d84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 39,
                "y": 41
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "508e4501-5bc3-4ad2-a4c8-be697e108232",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 37,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 176,
                "y": 158
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "68708f51-e6b9-4b95-a459-3ddddb47d2c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 37,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 164,
                "y": 197
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "a6a5d0e4-c712-4c86-849b-d54d506ea1d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 37,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 118,
                "y": 158
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "c7ce50e8-4667-4662-8908-30008a76b704",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 37,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 154,
                "y": 197
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b2fab623-9eae-4476-a11f-8d9e8e913f93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 36,
                "y": 119
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "6a458a79-b6b4-48cb-9cc9-69982b48d5d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 19,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "56e886ea-e381-4522-9373-25db76ece9cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 37,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 143,
                "y": 197
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d69b8256-c13c-4963-888d-afd581962211",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 70,
                "y": 119
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "c4b0275d-d6ea-47c7-a5ae-668aa561a059",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 20,
                "y": 80
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "93f6e301-b5a7-449a-8891-b98cb470ad9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 15,
                "x": 104,
                "y": 119
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "e4d3d280-0591-444b-90b5-2c4a0c038708",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 53,
                "y": 119
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "01d17ce3-486d-48d0-8455-18e7bb9da0c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 19,
                "y": 119
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "19e497da-2d58-4d02-8bea-3b33f0f05171",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 121,
                "y": 197
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "39b9fec0-19d5-4a2e-859a-bf9dde9a8ff1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 2,
                "y": 119
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "e1b8ea9f-eac0-4cc0-8959-15f8d41835af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 224,
                "y": 80
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "26ea9a19-ef38-4e41-a123-4f81685ba4f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 37,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 183,
                "y": 197
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "7db56373-81b8-4775-96a5-f553b5e16919",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 132,
                "y": 197
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "a3625eda-c0a0-4bba-ace5-08eeb9f4c1f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 86,
                "y": 158
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "a92fd6c1-b633-45bb-8ce8-7205d2508c46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 37,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 174,
                "y": 197
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c152dad9-cbc3-4d4f-b7f1-b91282086d71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 37,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a40d4db5-afe3-495e-9002-6612ff2ad4cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 207,
                "y": 80
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "8e933fc9-db8c-4277-acd7-163de5dfcfdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 190,
                "y": 80
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "af873147-6b4f-4295-9c22-ea4cfdfdb531",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 173,
                "y": 80
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "3b2d0400-98eb-477e-9c98-6ee6b7463a84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 156,
                "y": 80
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "c70c053b-36e8-4c5b-b510-24c959f3f345",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 37,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 41,
                "y": 197
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "dd1fd867-319e-459c-9849-2955d321f4b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 15,
                "x": 139,
                "y": 80
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "ae47d225-06d0-411d-a9e9-7b96d8eb6a18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 65,
                "y": 197
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "13a498b7-376c-4e36-8ce6-94487a48e8fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 122,
                "y": 80
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "ecb0327b-e402-4d4b-a6b9-3c42c9fb73be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 71,
                "y": 80
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "22f25e7c-f3b9-44db-8430-7676b32455c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 37,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "6500c01a-7d6f-409b-90bc-5e61ae104c4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 37,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 70,
                "y": 158
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "ce514ad2-4d78-4a8b-b0dc-dfb39923dac5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 37,
                "y": 80
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "cd9e8e52-1c2a-4c78-928c-007beea677e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 37,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 230,
                "y": 158
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "c829e1c2-ea2b-470c-8fd0-41a3f174039c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 37,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 197
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "c03b3af8-5a8a-4e35-bfe3-71c788c5f95c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 37,
                "offset": 3,
                "shift": 8,
                "w": 4,
                "x": 2,
                "y": 236
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "29b9987f-ce00-473a-9ea0-949f5872fbd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 37,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 28,
                "y": 197
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "e4538912-f360-44f3-8cd2-f1a83c8f0579",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 16,
                "x": 201,
                "y": 41
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "3a7476d8-cca2-4378-8202-c204238367a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 40,
            "second": 65
        },
        {
            "id": "762431f7-ae60-41b5-93c6-597c8df14233",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 40,
            "second": 192
        },
        {
            "id": "f2d5cfbd-ceeb-4930-b9cf-841bfcabe458",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 40,
            "second": 193
        },
        {
            "id": "7891804c-2075-42a1-ac63-3af141212fb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 40,
            "second": 194
        },
        {
            "id": "4a9433e5-6aea-45e1-abf8-15c044675179",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 40,
            "second": 195
        },
        {
            "id": "c476a490-e6fc-4fc1-88a5-1acd2bc2642e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 40,
            "second": 196
        },
        {
            "id": "18dcb00e-60b0-40ad-9b9e-71cde9ec0a56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 40,
            "second": 197
        },
        {
            "id": "5c05c583-b20f-46d2-b3df-45fa957b4ad2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 40,
            "second": 198
        },
        {
            "id": "7571a5a4-07d5-4653-b437-eac8a66cd8c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 52
        },
        {
            "id": "b31809e0-00c5-4c0c-905b-84c850f83365",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 49
        },
        {
            "id": "9e416f03-6c66-4741-b111-2f1cd21bed9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 55
        },
        {
            "id": "cd8ceb6c-0b0d-4bf9-802d-ce76ef73fff9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 49
        },
        {
            "id": "0597ca0c-f1ae-40bc-ae7a-ec0ecd3b3e03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 50
        },
        {
            "id": "89f08405-16f7-430b-8699-1467d5cb4da6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 55
        },
        {
            "id": "032db71e-0454-4314-b1d3-bffb6f49e95b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 49
        },
        {
            "id": "d0ea6ccf-60e3-4acf-9da4-0f0aa8b7c0e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 52
        },
        {
            "id": "ebcdced3-c177-4836-9122-8806a6f269e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 55
        },
        {
            "id": "7c54d923-7186-4839-a57f-5b3b43c51434",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 41
        },
        {
            "id": "22e706dc-0c09-44bd-afbe-63470fbdf8f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 44
        },
        {
            "id": "a3c50b47-c219-4c90-b195-debce076a531",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 46
        },
        {
            "id": "c214055a-6a7c-42d6-b478-eb51c9177710",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 47
        },
        {
            "id": "438cf43d-7db6-49fe-b472-2b2e8e528955",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 65
        },
        {
            "id": "002decaa-d6ba-4174-9af1-4ec4f20c26aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 74
        },
        {
            "id": "5a6559e2-34ec-421d-aca8-8f6118f68869",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "ee58e95f-73bd-40b0-b714-db163346a699",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "da08fcf8-3a75-4031-a8b0-04666a1e485f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "5a39a3c9-a8e0-41d5-ab37-435b4e0d32fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 88
        },
        {
            "id": "a0c4a619-ab19-4507-9e2f-59ef06b0c46a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "0db8b06b-6040-4288-abe9-2b93fff9ebe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 90
        },
        {
            "id": "41ca966e-19ce-426b-b07b-99067d4c6fef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 93
        },
        {
            "id": "cd6d4592-4924-48d8-854d-59102f055c5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 120
        },
        {
            "id": "c4fe99bd-b499-4bbd-a3c3-1c368def7569",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 122
        },
        {
            "id": "bbe0d072-4d7d-41ad-87a4-0c3dff0a4cc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 125
        },
        {
            "id": "4ab52bd7-3839-46e8-ac72-5e5b6dbde676",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 192
        },
        {
            "id": "c712800b-873f-4e38-a3b6-221444e44685",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 193
        },
        {
            "id": "d6b64c3b-ba42-47c2-847f-02954bc8175f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 194
        },
        {
            "id": "12ad75f3-5c96-4e7a-8695-7a7b898ccd2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 195
        },
        {
            "id": "a2eb24be-c34f-4f0f-9a4d-b2d23aab494c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 196
        },
        {
            "id": "c570ac39-6726-4dcd-93a3-1b4006224ead",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 197
        },
        {
            "id": "66b007c0-e36d-48e0-b3cc-18a66016b382",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 198
        },
        {
            "id": "8ec3b039-9d0f-46e9-ad72-b548d52264e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 376
        },
        {
            "id": "546358b8-f024-41b2-b291-e7d2d8a7c08d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 8230
        },
        {
            "id": "2b80b59f-aed9-4ffd-9bf7-b7be7a1ff654",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "b74e3337-cf40-4982-8b13-fd3d94dccd08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "a25b87c2-1a01-4609-9921-f3a537c0d47b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "77d5a8a6-67c6-4424-83c5-a5ae6368e035",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 86
        },
        {
            "id": "86d59872-0e5d-4733-af33-f34ae030a75c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 87
        },
        {
            "id": "37a26b70-551f-43a3-b808-18a52c8d2f4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 89
        },
        {
            "id": "f832f684-6f2a-4511-8c23-ebe5f80c6e17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 192
        },
        {
            "id": "95ffaa90-959e-47e3-bf17-c6f3c4c848b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 193
        },
        {
            "id": "791d2817-caaf-4464-aade-9e8ffc8dca3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 194
        },
        {
            "id": "f8ccaa67-78ef-473c-816a-b77f2fa75611",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 195
        },
        {
            "id": "f1310c39-725f-40ba-8e4b-5cd110e96946",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 196
        },
        {
            "id": "9a4bfeb3-8e69-4446-a61c-859af79a21e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 197
        },
        {
            "id": "0289aca3-91b8-432d-b3db-d8286245d6ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 198
        },
        {
            "id": "2a59e31d-4e92-41be-a472-379ca179c0a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 376
        },
        {
            "id": "de6e38fc-10d3-469a-8107-c99f7e1e475e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8230
        },
        {
            "id": "03ca93ce-ed0e-4f15-addc-ae40afcc55bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 44
        },
        {
            "id": "234780e3-42b9-4ae3-9b99-4688e7e4d48c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 46
        },
        {
            "id": "cdb03361-db43-4527-9662-66a3d6474903",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 65
        },
        {
            "id": "a09a873e-18a9-42a7-847e-6eecb3496159",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 67
        },
        {
            "id": "ca0f21c1-d315-4f2e-8d05-913d23e93158",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 79
        },
        {
            "id": "78c5ffb3-60dd-4b36-9bb1-9bf73eddf9c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 81
        },
        {
            "id": "818bcfca-0217-45ba-9f59-616fb9d952c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 90
        },
        {
            "id": "2d6d663c-7d6b-4848-8c43-686f140a4c29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 192
        },
        {
            "id": "23d31fed-b767-4ced-808a-5ec5dea09624",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 193
        },
        {
            "id": "53351e40-7cb2-4783-a55a-f3ff1b424a8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 194
        },
        {
            "id": "e4a6658b-c711-40c6-945a-a7b7c6de0917",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 195
        },
        {
            "id": "ba4ba74d-2f4c-44f1-9e5c-b8fb8173566c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 196
        },
        {
            "id": "45d412d1-ed16-4eca-831e-ec6b49e2ac08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 197
        },
        {
            "id": "7fffe26d-9efc-48da-b99b-a9b0c0b8f47c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 198
        },
        {
            "id": "83712489-59a9-46d7-add5-aaf66fbadfa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 199
        },
        {
            "id": "71c616e8-d378-4ee4-a494-1b64694b31e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 211
        },
        {
            "id": "2d6c5653-2a94-4942-9cf2-7a0dcf005d04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 213
        },
        {
            "id": "e9dbc348-044a-453b-bdc1-1c5ecf97aae4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 214
        },
        {
            "id": "bbccd276-8179-4fc6-ace1-cda82cec5d42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 338
        },
        {
            "id": "a3b1edfb-6417-42a4-b4dd-e690ff573c96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 8230
        },
        {
            "id": "6e83cbab-0a3b-427b-b823-6b7b1b299e60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 44
        },
        {
            "id": "c320e797-85b5-43a8-b1b4-d6ccd8053aed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 46
        },
        {
            "id": "6ab166e9-a70a-44b3-8143-e789781c6daa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 65
        },
        {
            "id": "20974121-6226-49bd-b04e-d0811216c5f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "de1066c5-0f8e-4abc-ae70-44fc047da8ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "3e8ac3df-08ac-4441-918e-e76f24366751",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "b2e0445c-5c9f-46d0-8940-ad8fa5a2c287",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "0271f516-2e69-4d88-a214-92c84eaec859",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "1d0804fe-5b58-427e-bb47-bc2f8ae9c0fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 192
        },
        {
            "id": "9fe1d905-6b21-4c84-ba6a-4dab08a963d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 193
        },
        {
            "id": "811cd91d-ef6e-4269-8234-2c03251ae9ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 194
        },
        {
            "id": "2bdf77f7-6ebb-45cc-935c-41bdbb2a2430",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 195
        },
        {
            "id": "19bc934a-08ab-41e9-9963-e408cdb75de4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 196
        },
        {
            "id": "21c0f1d8-b247-4f75-9a70-9f2d331f2baf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 197
        },
        {
            "id": "c4460707-047d-4bbe-aed1-22ba61a3fd97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 198
        },
        {
            "id": "0e975eaa-5499-4be2-b3a5-fa65ad216030",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 376
        },
        {
            "id": "5c532afb-ee47-4a7a-bac0-8a0409d0723c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 8230
        },
        {
            "id": "a83eee36-5a3c-4888-8f31-b54369f88635",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 44
        },
        {
            "id": "1794d22a-c866-470d-9fb0-14eb463e4d66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 46
        },
        {
            "id": "4a8f770e-4e50-414e-a351-b84693ec39be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 47
        },
        {
            "id": "4083fea1-a218-42af-a485-ef4b1a2ea4ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "0c7c25d5-498d-4486-8fa1-86735f7f0e6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 74
        },
        {
            "id": "2777be0b-aeb6-486c-8be2-3a1715dcb509",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 192
        },
        {
            "id": "f4798c0b-4fb3-42e8-8f44-5481838e8052",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 193
        },
        {
            "id": "e4b79ea6-8818-4afd-80db-8d859d3ebfd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 194
        },
        {
            "id": "4c0e224e-b3ae-4639-894f-92b603e704db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 195
        },
        {
            "id": "aab1e9b1-5d11-4e8e-bdb4-c01967608eb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 196
        },
        {
            "id": "9c1fe6fa-3631-4dda-a4f8-690f12515edb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 197
        },
        {
            "id": "0ae69dc6-ac72-4730-8d31-6dd73f6909a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 198
        },
        {
            "id": "9f48b4f2-0dce-4615-8b57-f6866fe74f2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 8230
        },
        {
            "id": "b0cd8004-08d7-4afa-a8af-9a4363e6f13b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 44
        },
        {
            "id": "b8c288bd-8137-4859-bfe1-4ad906b1bcf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 45
        },
        {
            "id": "3c862d1b-7ad4-4a02-84a5-4482892047ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 46
        },
        {
            "id": "8032f65a-dba5-4c01-96fb-2c66cd483f7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 47
        },
        {
            "id": "ae04b4aa-9dbc-416b-9568-acf3d48ad946",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "fa3ab024-ae9f-44ce-9503-433220495314",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 74
        },
        {
            "id": "ced899ef-2c05-4332-965e-37ea1627e5b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 86
        },
        {
            "id": "9c71b61c-5c15-4ec0-8a65-de8b71a25e57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 87
        },
        {
            "id": "5f380f71-188b-4d6d-8691-9a6d8620c08f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 89
        },
        {
            "id": "c8979e07-36ad-419d-bc2b-3625afa8066b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "ad8bc18a-7828-4e56-b852-75d13e48f19e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "2e376e99-cd4f-4ffb-bedb-99c4945ed115",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "fbfae0f1-bf35-4d97-9e81-5dcac975c79b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 118
        },
        {
            "id": "50015b65-37f5-49a0-a584-281783c0016a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 119
        },
        {
            "id": "2b2cfd30-e9ef-4300-9323-f23fd3d5579f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 121
        },
        {
            "id": "056407a2-56b2-418e-baf6-3b553b752049",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 173
        },
        {
            "id": "43210173-93fa-46fa-a244-aeb01dd59643",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 192
        },
        {
            "id": "70e82152-f0a8-42ac-a2f1-4f81165684a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 193
        },
        {
            "id": "39ee04b9-aaba-4135-8bc3-8e499c5263ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 194
        },
        {
            "id": "887af099-f7ba-4c54-8eec-7c09e8c68b80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 195
        },
        {
            "id": "65b56007-0674-4982-b263-4b1a4540c633",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 196
        },
        {
            "id": "72a1f99f-97ee-4011-8f92-734823d8a49a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 197
        },
        {
            "id": "a7aaa019-cbef-43df-a0f6-f2b644d7db5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 198
        },
        {
            "id": "656487ce-9299-4942-9238-3156d2bf9cf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 230
        },
        {
            "id": "0681ee24-aee0-4c82-93de-502c78d9833c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 231
        },
        {
            "id": "8d7f00d1-6e7f-4e78-aeac-d68594e84783",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 376
        },
        {
            "id": "d4f3cb38-2d28-41db-a71e-27ecc955f5df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8230
        },
        {
            "id": "784e99a2-c139-46c2-a035-e19e7f30238e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "a168886c-5298-40ad-91f4-33210165f50e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "5e1bf8b8-a18b-4b28-9239-2acb88facdbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 47
        },
        {
            "id": "2f6045f3-e722-425b-b9da-6603c85e4c16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 74
        },
        {
            "id": "e2078f60-e1cf-42f7-8a23-8d76ccaa31fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 89
        },
        {
            "id": "e5e37aff-c968-49aa-8e1f-e9431538aaa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 121
        },
        {
            "id": "dba6cd9a-8e4a-42ea-90da-2d5175acf904",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 376
        },
        {
            "id": "afbc7c47-e95a-41c8-b93f-d88b1439348e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8230
        },
        {
            "id": "3b86df4e-1c9a-4534-847e-4fda677ec9ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "98a3f814-7f59-4f3f-8582-a079ea3a065c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "34827c7b-3443-49aa-b000-572c21550cde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 47
        },
        {
            "id": "736cea4b-5300-456e-889c-8d77784fc88d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 74
        },
        {
            "id": "137cc15d-98ac-404e-b4df-22480f51aeb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 89
        },
        {
            "id": "79fb8a91-707e-444a-823e-8d3d7a326d87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 376
        },
        {
            "id": "7b27a229-f0b7-4fd1-ae4d-90c4b552cdc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8230
        },
        {
            "id": "ea89d4ed-2b6d-4f7a-b08b-61f100e4fcea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 44
        },
        {
            "id": "747dce90-850c-44eb-ab33-f8bd8b815bfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 46
        },
        {
            "id": "fe2a9981-63c5-4b9d-9592-9482f5bc8623",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 65
        },
        {
            "id": "6027f9c6-202b-47a9-8c0d-027ac1e3a601",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 74
        },
        {
            "id": "49c98c71-5785-475f-a0dc-0174e7524681",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 192
        },
        {
            "id": "9f334555-f31f-47d7-b008-bb08f4f37aa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 193
        },
        {
            "id": "d84cfebb-ed75-4ad7-9eec-42dde9bb70e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 194
        },
        {
            "id": "23073821-c6a1-494b-8ff1-aed64994a173",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 195
        },
        {
            "id": "f8656c5b-f3c1-4147-aee1-66d997020cb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 196
        },
        {
            "id": "06d257bc-a63a-4951-907a-7364fe18600f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 197
        },
        {
            "id": "cb41e97d-10ae-4158-bef6-926368ac5191",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 198
        },
        {
            "id": "00e4f3a7-8460-49ba-bffb-a0b4cdd3d607",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 8230
        },
        {
            "id": "38425a7d-17a2-4f52-a37f-5e7e93913a13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 44
        },
        {
            "id": "7a08a6f5-81a6-4dde-85df-6a6d9ce3f24a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 46
        },
        {
            "id": "f369e3c7-c1db-4474-a52b-2655c824eac2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 47
        },
        {
            "id": "cb60cfab-308c-46dd-a79b-b7c2f1ee5c83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "41cdbadc-7d48-441f-8029-a0d88f4f2c0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 74
        },
        {
            "id": "e6f9869a-7906-40ca-95a0-ea7351202f2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "758ef5c4-14f8-488b-93a5-7aaf73a42b27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 99
        },
        {
            "id": "d10588af-6d86-4897-93eb-27b1027cbdd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 100
        },
        {
            "id": "2c949ec1-2d07-4983-a61d-88a5e4ed1cd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "b863c3ed-f90e-4cd5-9836-cfc323a84f3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 103
        },
        {
            "id": "6d7cf2db-ce87-4447-8ab9-135c0e9d12cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "2ad7c527-9efd-4dad-aef2-9afc9dc07992",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "8c7575d3-6b42-4001-8b08-5ae28e2d7272",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 115
        },
        {
            "id": "139b0d29-00d3-4bdd-91d9-0cd03a975abe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 192
        },
        {
            "id": "fd41a9c2-f01a-40a1-ad85-aed32374010c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 193
        },
        {
            "id": "7ce943c2-ee41-4b02-9f1f-9ea5447b9e17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 194
        },
        {
            "id": "6724edc0-0515-4b78-b7b6-54a58d2d4598",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 195
        },
        {
            "id": "cd4e8840-8ab1-4060-afdc-49ebb406cda8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 196
        },
        {
            "id": "34bb3c1b-060a-4473-9b83-2a43de17df6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 197
        },
        {
            "id": "bdc71757-91bd-4aee-b516-4263cef03416",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 198
        },
        {
            "id": "a6073bd0-38ff-4868-85b2-348c8170770c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 230
        },
        {
            "id": "03797d7b-639c-47c0-b6f9-efd1e2bb940f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 231
        },
        {
            "id": "f773c254-5b0b-428c-a9ff-76977de3d944",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 339
        },
        {
            "id": "bfa6b6be-b186-4084-ac08-f1ef45c20750",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 8230
        },
        {
            "id": "cf9dfa3f-a070-4d29-bc33-68c96dbf0040",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 44
        },
        {
            "id": "02d95304-f1d2-4aca-b858-0c7d4abfc6bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 46
        },
        {
            "id": "e590aa7f-6aad-4484-bb80-309a32b0af59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 65
        },
        {
            "id": "d7e844fa-78bc-44e7-99a3-adfbbc549408",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 121
        },
        {
            "id": "f43ba5fe-4cb9-4d08-811e-1d078bdfe3c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 192
        },
        {
            "id": "dc217e14-709d-467b-b81f-148b9e2d5ce1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 193
        },
        {
            "id": "b3dfcbfe-9f73-4785-81e7-1de50f54b85c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 194
        },
        {
            "id": "e9b2dff2-e30c-49ff-bd1f-5365a4b78825",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 195
        },
        {
            "id": "a3ad22b8-c75a-4cac-9692-7c20e90e5574",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 196
        },
        {
            "id": "491fd383-e43f-4147-a756-e2b441306dc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 197
        },
        {
            "id": "164b14e6-14c2-4b1b-a0eb-de01f735135f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 198
        },
        {
            "id": "a600b718-f8c3-45d8-a801-0adcde16c063",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 8230
        },
        {
            "id": "3c01d620-7bab-4276-b1bd-3db28030f8d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 91,
            "second": 65
        },
        {
            "id": "6668b077-06c5-41fd-ad2d-25a97744092a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 91,
            "second": 192
        },
        {
            "id": "8bd0d6b0-0e99-4a9e-8a1b-3f48caefda7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 91,
            "second": 193
        },
        {
            "id": "03f49b75-91e7-40fb-a7cf-75ff2a039656",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 91,
            "second": 194
        },
        {
            "id": "89bf2710-e217-42e5-bd2f-5e3853ce1eeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 91,
            "second": 195
        },
        {
            "id": "dd12e45f-a002-4619-8345-0831fca4e907",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 91,
            "second": 196
        },
        {
            "id": "58471b5b-3a26-4dbe-900f-2f67113a0561",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 91,
            "second": 197
        },
        {
            "id": "30854256-dbf2-4996-bdf4-71836187a146",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 91,
            "second": 198
        },
        {
            "id": "d8a14921-c818-435a-a920-9d6e54dbc80e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 118
        },
        {
            "id": "b6ada0d4-f8ba-4bae-a26c-d46d3a17403a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 119
        },
        {
            "id": "c29264c8-36c5-4cf5-89b4-22214a139f26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 121
        },
        {
            "id": "1adbe9a2-e267-405a-867a-25b885b51b22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 44
        },
        {
            "id": "d7a66353-98f5-4034-b6f4-0c118405b56e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 46
        },
        {
            "id": "8e516397-2025-4cb7-916a-4fc289d65463",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 102
        },
        {
            "id": "cd20f7a6-0b8d-4b11-950a-7f4726f7ed58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 116
        },
        {
            "id": "649a3204-7776-4269-8360-92823a11e8dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 118
        },
        {
            "id": "b19ce3f6-bd58-40b1-af5a-7c4b4d4d4d4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 119
        },
        {
            "id": "4e8f9e80-d9db-4e9a-939c-54cc5dcfd2b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 121
        },
        {
            "id": "c9e3cc51-9a0c-4729-bd1d-9cc05523174e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 8230
        },
        {
            "id": "e3c2d6a0-a0a6-48fe-8d15-76d0c94d27a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 64257
        },
        {
            "id": "08b5a44b-28f5-4bc0-857c-eb2201a880a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 64258
        },
        {
            "id": "9ae7086e-5f72-4711-b61c-9e98d37f761b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 44
        },
        {
            "id": "fbff9788-489d-4fc5-9c1d-38514adc5834",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 46
        },
        {
            "id": "3661a39b-0fbd-44e3-87e5-830c1bc1d7c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 118
        },
        {
            "id": "04850861-8417-41eb-8b14-b4009d2556a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 121
        },
        {
            "id": "8db62db1-fa7d-4f37-aba0-6b67b954c4d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 122
        },
        {
            "id": "80d1dd08-0901-4b83-8ecd-69728762c5bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 8230
        },
        {
            "id": "400d17d7-0432-4087-acaf-423b09507b00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "41d3894b-72f6-45a8-ba02-62e180912804",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "e7984108-e905-453b-abec-998e4610d850",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 102
        },
        {
            "id": "2e39d7dc-54dd-469b-bbb6-85701e1102a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 116
        },
        {
            "id": "5693dd22-dfee-4795-93f1-77e82bb6cd9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 121
        },
        {
            "id": "1fcd2527-3abe-4bdc-9722-2f6b9767bf1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 8230
        },
        {
            "id": "58cae1a6-c79a-4dc7-8579-033566a132dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 64257
        },
        {
            "id": "1e0e2d8c-636a-4c92-afc6-c7d13a5152e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 64258
        },
        {
            "id": "83796089-ba2d-46f7-b4dd-6612668ee74d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 102
        },
        {
            "id": "b95a820e-4fe8-4c1b-8d79-4ccdd41057cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 121
        },
        {
            "id": "2fd45813-a6cb-4024-97fd-3b35d20744e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 64257
        },
        {
            "id": "4b227379-0a70-4559-a5aa-895659329a3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 64258
        },
        {
            "id": "8c5e4bcb-462e-4a45-b18c-a8178decb14e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 44
        },
        {
            "id": "dfb5126d-27fc-458a-a149-c51cf35196bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 46
        },
        {
            "id": "983f84d8-4a6d-4a61-8e4f-2f3e19854c28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 8230
        },
        {
            "id": "ae8e6cf5-067e-4eb7-b052-d59f20c64899",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "850392e2-1ef7-4f0a-89e2-d465b16fd011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        },
        {
            "id": "97484cf1-7545-4389-a710-4f9c5ec01b14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 102
        },
        {
            "id": "e82435ae-6243-4218-8f9b-1b41cab3a54a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 116
        },
        {
            "id": "4abe2947-47eb-42d8-b86c-b8f1c3283310",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 118
        },
        {
            "id": "35afc5b3-1feb-417e-bd17-1169ed9222eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 120
        },
        {
            "id": "0f745491-a3c1-46b4-bf69-97a8797466cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 8230
        },
        {
            "id": "d44bc636-f591-434a-8dba-e1b018758c97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 64257
        },
        {
            "id": "16f2928c-498c-40d7-8f0f-21ea40e814c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 64258
        },
        {
            "id": "8d49161a-1e70-4691-98e6-9ac31a72e9a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 44
        },
        {
            "id": "3043486b-3f7c-4016-85e0-bee9445fa433",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 46
        },
        {
            "id": "25c5a71c-845a-4c4b-8d77-21b4a1eb78dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 118
        },
        {
            "id": "a7e80a9b-e627-4621-bb98-1e5fa91a7518",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 119
        },
        {
            "id": "59a63914-a682-43a2-8662-7b67ddfe9287",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 121
        },
        {
            "id": "36a01c23-5149-4200-9f6c-4922355b11aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 122
        },
        {
            "id": "b8138829-d99a-4a46-8e9b-5e4abc44d1a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 8230
        },
        {
            "id": "e6706027-1a69-4dff-9132-27cb0a9d5814",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 123,
            "second": 65
        },
        {
            "id": "25ec44fd-0d1d-44c0-b408-368485490563",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 123,
            "second": 192
        },
        {
            "id": "f19c73df-9234-4d63-8554-88b04d94313d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 123,
            "second": 193
        },
        {
            "id": "3b63d239-b671-4545-a069-4e64e363088d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 123,
            "second": 194
        },
        {
            "id": "d5628fc5-adbd-4075-824e-5588b9456b39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 123,
            "second": 195
        },
        {
            "id": "efcaa0d2-a59b-46e0-872e-efeadc87986e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 123,
            "second": 196
        },
        {
            "id": "70102c8e-b016-4e6c-a56f-29aad9f45de5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 123,
            "second": 197
        },
        {
            "id": "5701dc22-ed4c-4f87-8da5-e5329bf33827",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 123,
            "second": 198
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 23,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}