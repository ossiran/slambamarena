{
    "id": "c2232d27-6dc1-412c-a41a-4ed9c1741e5b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_66m",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3387d020-d124-4de8-9cee-70b1096eb3dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2232d27-6dc1-412c-a41a-4ed9c1741e5b",
            "compositeImage": {
                "id": "6dae54b5-f107-49e4-987f-e54ce26c2ace",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3387d020-d124-4de8-9cee-70b1096eb3dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3a6af58-a10b-4dc7-9b00-25d006480fa2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3387d020-d124-4de8-9cee-70b1096eb3dc",
                    "LayerId": "a19751e0-1ec8-475e-b94b-0a792bcb9966"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a19751e0-1ec8-475e-b94b-0a792bcb9966",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2232d27-6dc1-412c-a41a-4ed9c1741e5b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}