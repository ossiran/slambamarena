{
    "id": "59a445ca-3052-4b93-aef2-1580eb04f264",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_5h_ice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 16,
    "bbox_right": 173,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "78e953b5-b60c-4de0-88eb-b21f75ae0936",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "46da982c-c5c5-4d9a-8896-70f6a52bf42d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78e953b5-b60c-4de0-88eb-b21f75ae0936",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26cc6801-ecab-42bf-a48d-1553ad5af62d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78e953b5-b60c-4de0-88eb-b21f75ae0936",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "d84ee269-cfde-4efd-9ef6-5ecd60db7867",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "40aab8e0-f1b4-4303-afda-83a9443477e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d84ee269-cfde-4efd-9ef6-5ecd60db7867",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bcbcb39-9b90-41c7-83a8-ee9edf5b4f26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d84ee269-cfde-4efd-9ef6-5ecd60db7867",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "d7ce7355-c8e3-4947-9d67-5d7f5ca2e36d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "180f907a-1750-47df-8d0e-8c1bb8bfe1be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7ce7355-c8e3-4947-9d67-5d7f5ca2e36d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "938b4c6e-73c4-4a2b-a199-8372093ad767",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7ce7355-c8e3-4947-9d67-5d7f5ca2e36d",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "2e016700-1ba0-4eaf-8007-75992b21b7ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "62008da2-c21a-4594-b7a7-a626b364be00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e016700-1ba0-4eaf-8007-75992b21b7ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0ece582-af22-492a-8e5e-dd5ad4b8fb64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e016700-1ba0-4eaf-8007-75992b21b7ff",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "cc2a019d-db19-49c4-80fd-172e712f1a0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "e617a47b-4480-49b6-a468-65a54d8d066b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc2a019d-db19-49c4-80fd-172e712f1a0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72746a8e-8f81-4eb7-80e1-f094001f8fcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc2a019d-db19-49c4-80fd-172e712f1a0a",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "5c0e928f-1812-4534-9f48-f80a4afe52a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "b478283b-cf02-4d35-9020-088de37ef331",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c0e928f-1812-4534-9f48-f80a4afe52a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d69796d-7c8d-41a1-b3a1-9150a94f315d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c0e928f-1812-4534-9f48-f80a4afe52a7",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "6fb0035f-8d3b-4a70-9bcf-6ec287f2131e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "cf2c8528-f923-4aef-b8af-81216fb8fa7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fb0035f-8d3b-4a70-9bcf-6ec287f2131e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f595362-baad-41f7-ad71-dc8238c29104",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fb0035f-8d3b-4a70-9bcf-6ec287f2131e",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "f9cebe04-ba25-447d-a6fc-a4ffa24f6f45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "ed3470ae-82a5-4500-9e77-9a655dc97abc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9cebe04-ba25-447d-a6fc-a4ffa24f6f45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00d8d6ab-04a2-4398-b6a1-31a616407623",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9cebe04-ba25-447d-a6fc-a4ffa24f6f45",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "32023c2c-44e8-4ca7-bf3b-e8eeb751135c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "e7158c50-d495-49ea-b591-3e983624809d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32023c2c-44e8-4ca7-bf3b-e8eeb751135c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d9bc00d-d09b-471e-808e-f5823861ac0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32023c2c-44e8-4ca7-bf3b-e8eeb751135c",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "e3c5f586-9ca2-44cc-98c5-4baf9786905b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "a122b054-9826-4d12-ad32-7608034b172c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3c5f586-9ca2-44cc-98c5-4baf9786905b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64ff3719-4293-4025-b4e1-8deb7248aeba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3c5f586-9ca2-44cc-98c5-4baf9786905b",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "d7c83142-de8b-43c0-ac06-b5cfbc446c89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "2f2032a6-994f-4762-9a42-b5e20e9c51da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7c83142-de8b-43c0-ac06-b5cfbc446c89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93e115e5-1485-4fab-9d0b-83ee40b74090",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7c83142-de8b-43c0-ac06-b5cfbc446c89",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "8846ad67-e02a-4e0c-aa4f-262685877172",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "43ab7590-5edf-49c5-81b0-25aafda55acb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8846ad67-e02a-4e0c-aa4f-262685877172",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2d3982d-3d0b-4671-8e69-5ecf84602261",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8846ad67-e02a-4e0c-aa4f-262685877172",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "6be7e758-112a-415d-bab2-2d55f5676943",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "42c9ca1f-9570-4ea8-8079-6fe2c5a6ee7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6be7e758-112a-415d-bab2-2d55f5676943",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4ed6569-b119-4d42-9a2a-2384f7c4519e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6be7e758-112a-415d-bab2-2d55f5676943",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "8edbc523-2210-45ff-8301-ef2ea84e7893",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "eb07e860-7722-464a-b1ad-d7cd06287557",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8edbc523-2210-45ff-8301-ef2ea84e7893",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c997d29d-5a82-42ed-bf80-cdf75ef8ff81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8edbc523-2210-45ff-8301-ef2ea84e7893",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "6970a9b0-8cc0-4953-84c9-6f5b07ffbeac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "db50e289-764e-4175-b64a-2ffe4deadb4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6970a9b0-8cc0-4953-84c9-6f5b07ffbeac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7abc4e73-2e7c-4603-9e66-1dcbba8a2a5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6970a9b0-8cc0-4953-84c9-6f5b07ffbeac",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "5b21b186-1e8e-4430-b7c0-65a7c9e4edbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "089ac811-0939-4380-a565-667bce9ffb9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b21b186-1e8e-4430-b7c0-65a7c9e4edbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b0f93b9-2ef6-4fd7-9a37-9183196046ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b21b186-1e8e-4430-b7c0-65a7c9e4edbf",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "cc23db51-6017-4c72-9e69-ee493c13b53d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "1ea331b1-ff5d-4399-a26f-bb0de8e3616a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc23db51-6017-4c72-9e69-ee493c13b53d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba0dc64a-ac8a-440a-9397-59a522dafe62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc23db51-6017-4c72-9e69-ee493c13b53d",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "80a46d49-8980-4e96-81bd-42b53d031ed4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "563f7939-463c-434f-9d09-37a3eb71d527",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80a46d49-8980-4e96-81bd-42b53d031ed4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05fc1f6c-4da4-4708-ace9-8f6847cff108",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80a46d49-8980-4e96-81bd-42b53d031ed4",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "3d67240a-b8ee-4f4c-90d1-7997456da122",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "a50faff0-8515-4518-9541-a198a0a372a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d67240a-b8ee-4f4c-90d1-7997456da122",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d37f8db4-15f8-4044-b80d-90b3be1a7215",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d67240a-b8ee-4f4c-90d1-7997456da122",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "b2233833-52a4-4d87-93af-3c32361f4386",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "75df4412-3778-4e01-b46e-84d2f964cb50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2233833-52a4-4d87-93af-3c32361f4386",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b672a0a0-8b12-4ad4-830a-35a3d0a1c0dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2233833-52a4-4d87-93af-3c32361f4386",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "53545681-0ace-456e-8db0-c01316d1a108",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "a459f7ae-facf-4e71-bd25-deabec6e9ab7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53545681-0ace-456e-8db0-c01316d1a108",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef9d8f35-1139-4ad1-9960-ad73a6b52dcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53545681-0ace-456e-8db0-c01316d1a108",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "57abfc38-3ff8-41f9-9aef-b3f204ce812b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "085b3bca-8371-4463-a9f5-fea93fc8f308",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57abfc38-3ff8-41f9-9aef-b3f204ce812b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e779677-ecb0-46c2-a86e-32bbed77ad2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57abfc38-3ff8-41f9-9aef-b3f204ce812b",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "15031b52-ccf1-4aed-9b6e-e726f816903e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "5e667650-705c-4020-8d15-a9d61058b40c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15031b52-ccf1-4aed-9b6e-e726f816903e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "badfbca7-3cae-44d7-a9ab-fcbb0074d758",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15031b52-ccf1-4aed-9b6e-e726f816903e",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "87969496-afdd-446e-8fe6-bcf533de8c1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "5f845e33-5b81-4f19-98eb-1502346caa2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87969496-afdd-446e-8fe6-bcf533de8c1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed75a929-7955-408c-80b8-5371779de458",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87969496-afdd-446e-8fe6-bcf533de8c1a",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "7e547e3f-fa2e-4abc-9da5-a79034d19684",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "f8e6d237-f36c-4c51-81d2-295afc3da282",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e547e3f-fa2e-4abc-9da5-a79034d19684",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "673cad5f-69e1-4cae-8330-f6c7de17f50f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e547e3f-fa2e-4abc-9da5-a79034d19684",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "c846b11c-ab2e-4975-b67c-36360650d9bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "0c6db72e-bd61-4283-b7ac-b77ed266340c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c846b11c-ab2e-4975-b67c-36360650d9bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9298c4a6-83d0-416d-a66f-bf1d23b110ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c846b11c-ab2e-4975-b67c-36360650d9bb",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "4e266667-c22f-4ef8-8653-862101f549ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "0f1d3acd-965f-4598-9d9e-a34f3ed1e9f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e266667-c22f-4ef8-8653-862101f549ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "589a4ea5-5f67-445d-a3af-de876852942e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e266667-c22f-4ef8-8653-862101f549ce",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "d3528662-1b96-4055-bf21-4cfea2ec486a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "1a086935-a3aa-4c1e-a1c1-87e7e6ead3d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3528662-1b96-4055-bf21-4cfea2ec486a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99f9d049-f578-452d-aa50-d9a82fe79456",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3528662-1b96-4055-bf21-4cfea2ec486a",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "3ade2fc2-15ee-4b75-a71a-b42a4b54f01e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "ba2e544b-8594-43fa-8741-8d5ec13e225b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ade2fc2-15ee-4b75-a71a-b42a4b54f01e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4300eb3-6210-4704-b6bd-18b6146c7695",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ade2fc2-15ee-4b75-a71a-b42a4b54f01e",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "0737ebbe-2079-4890-bcb3-16296ffe13d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "d451ab8f-af02-430b-90b5-ab6ee926831d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0737ebbe-2079-4890-bcb3-16296ffe13d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "268d9587-bb4a-4ecf-b84e-a284113acb86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0737ebbe-2079-4890-bcb3-16296ffe13d2",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        },
        {
            "id": "b942e1eb-4e22-48fd-b9c7-9a6f91b2c617",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "compositeImage": {
                "id": "22aedad4-0792-4cf4-a4dc-17cf8f7e1494",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b942e1eb-4e22-48fd-b9c7-9a6f91b2c617",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae4f4425-d4b9-4645-9757-fa0e4167902c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b942e1eb-4e22-48fd-b9c7-9a6f91b2c617",
                    "LayerId": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "543fbc41-6d6e-4e52-bc8a-57b93ac5f804",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "59a445ca-3052-4b93-aef2-1580eb04f264",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 190,
    "xorig": 63,
    "yorig": 87
}