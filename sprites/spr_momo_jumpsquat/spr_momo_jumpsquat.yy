{
    "id": "0acced68-b295-42b4-9c3d-71c5363fe555",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_jumpsquat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 24,
    "bbox_right": 115,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b7b7fec-9042-49f1-b6c9-4a4e046ce5ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0acced68-b295-42b4-9c3d-71c5363fe555",
            "compositeImage": {
                "id": "cd325035-42a2-4d6b-93f5-6e255363e52d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b7b7fec-9042-49f1-b6c9-4a4e046ce5ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc6c34d4-b1a4-4523-b524-450b1d8b808d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b7b7fec-9042-49f1-b6c9-4a4e046ce5ae",
                    "LayerId": "0cb53a7b-10ee-4eb8-b139-e265df1e9794"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "0cb53a7b-10ee-4eb8-b139-e265df1e9794",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0acced68-b295-42b4-9c3d-71c5363fe555",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 63,
    "yorig": 87
}