{
    "id": "4df172bc-8b0c-49e1-b80b-75b9b48af3be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hurtbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5c15b89-64d3-4db9-94a9-649688db98ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4df172bc-8b0c-49e1-b80b-75b9b48af3be",
            "compositeImage": {
                "id": "96bb0947-9450-4364-a784-88f331d5d343",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5c15b89-64d3-4db9-94a9-649688db98ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f088436-95ef-498a-b3b7-5a017960318f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5c15b89-64d3-4db9-94a9-649688db98ca",
                    "LayerId": "3c52a35a-9e64-4df3-80b1-dad68d3082e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "3c52a35a-9e64-4df3-80b1-dad68d3082e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4df172bc-8b0c-49e1-b80b-75b9b48af3be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}