///create_hurtbox(xoffset,yoffset,xscale,yscale,duration,xshift)

var hurtbox0 = instance_create_depth(x,y,120,obj_hurtbox);

hurtbox0.owner           =    other;
hurtbox0.image_xscale    =    argument2;
hurtbox0.image_yscale    =    argument3;
hurtbox0.xoffset         =    argument0;
hurtbox0.yoffset         =    argument1;
hurtbox0.duration        =    argument4;
hurtbox0.xshift          =    argument5;

return hurtbox0;