live_call();

if(state_new == 1)
{
    with(hurtbox0)
	{
	    xoffset = -12;
	    yoffset = -13;
	    image_xscale  = 24;
	    image_yscale  = 40;
	    xshift  = 0;
	}
	
	with(hurtbox1)
	{
	    xoffset = -8;
	    yoffset = -31;
	    image_xscale  = 16;
	    image_yscale  = 18;
	    xshift = 0;
	}
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -13;
	    image_xscale  = 16;
	    image_yscale  = 40;
	    xshift = 0;
	}
	
	chainmove = 0;
	attackframe0 = 8;
	endstate = 36;
}


//animation
if(synmode > 0)
{
    sprite_index = spr_lsyn_jh;
}
else if(synmode < 0)
{
    sprite_index = spr_dsyn_jh;
}


//attack
if(state_timer == attackframe0)
{
    hitbox0 = create_hitbox(-20,-50,63,65,8.25,0,14,20,11,15,0,0,2,3,"mid",-1);
    hitbox1 = create_hitbox(-25,15,50,22,8.25,0,14,20,11,15,0,0,2,3,"mid",-1);
    hitbox2 = create_hitbox(25,15,12,16,8.25,0,14,20,11,15,0,0,2,3,"mid",-1);
    hitbox0.hitsound = snd_hitsound1;
    hitbox0.scaling = 1;
    hitbox0.juggle = 0;
    hitbox1.hitsound = snd_hitsound1;
    hitbox1.scaling = 1;
    hitbox1.juggle = 0;
    hitbox2.hitsound = snd_hitsound1;
    hitbox2.scaling = 1;
    hitbox2.juggle = 0;
}

attack_base(endstate,1,0,1,1,0);