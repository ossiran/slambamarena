{
    "id": "ff155fac-a8b6-497e-b8bd-c3a645ffa770",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_syn",
    "eventList": [
        {
            "id": "5d734a89-b949-409f-b66d-f3ea65a0e4a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ff155fac-a8b6-497e-b8bd-c3a645ffa770"
        },
        {
            "id": "4be2942c-0e97-4f11-aa1f-765cd4caf79c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ff155fac-a8b6-497e-b8bd-c3a645ffa770"
        },
        {
            "id": "80fa227d-f249-4040-be81-99ea629b0e84",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "ff155fac-a8b6-497e-b8bd-c3a645ffa770"
        },
        {
            "id": "4441805e-764c-4b1b-8a63-e7542bb9d4d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "ff155fac-a8b6-497e-b8bd-c3a645ffa770"
        },
        {
            "id": "8a1a4cf0-3fab-4208-85c5-d7771e16a5e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ff155fac-a8b6-497e-b8bd-c3a645ffa770"
        },
        {
            "id": "123d2c8c-af69-4de9-8c34-7d7ea815dca3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "ff155fac-a8b6-497e-b8bd-c3a645ffa770"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "764a5c31-d766-4159-bbb8-1162177a46bb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "61f797a7-afe2-4fd2-9465-0f49fe3fc6a1",
    "visible": true
}