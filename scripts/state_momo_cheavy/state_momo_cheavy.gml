live_call();

if(heat > 50)
{
    if(state_new == 1)
    {
        with(hurtbox0)
    	{
    	    xoffset = -29;
    	    yoffset = -31;
    	    image_xscale  = 58;
    	    image_yscale  = 63;
    	    xshift  = -1;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -15;
    	    yoffset = -56;
    	    image_xscale  = 30;
    	    image_yscale  = 25;
    	    xshift = -3;
    	}
    	
    	
    	with(collisionbox)
    	{
    	    xoffset = -20;
    	    yoffset = -22;
    	    image_xscale  = 40;
    	    image_yscale  = 54;
    	    xshift = 0;
    	}
    	
    	attackframe0 = 9;
    	endstate = 27;
    	
    }
    
    //animation
    sprite_index = spr_momo_2h_fire;
    
    if(state_timer == attackframe0 - 1)
    {
        vx = 6 * image_xscale;
    }
    
    //attack
    if(state_timer == attackframe0)
    {
        hitbox0 = create_hitbox(-6,-10,50,35,4.25,-18,16,6,11,12,-1,0,3,4,"low",1);
        hitbox1 = create_hitbox(-6,-25,48,15,4.25,-18,16,6,11,12,-1,0,3,4,"low",1);
        hitbox2 = create_hitbox(-21,-33,52,8,4.25,-18,16,6,11,12,-1,0,3,4,"low",1);
        hitbox3 = create_hitbox(-25,-46,49,13,4.25,-18,16,6,11,12,-1,0,3,4,"low",1);
        hitbox4 = create_hitbox(-35,-54,40,13,4.25,-18,16,6,11,12,-1,0,3,4,"low",1);
        attackhurtbox0 = create_hurtbox(8 * image_xscale, -17,22 * image_xscale,16,6,-2);
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 1;
        hitbox0.juggle = 2;
        hitbox1.hitsound = snd_hitsound1;
        hitbox1.scaling = 1;
        hitbox1.juggle = 2;
        hitbox2.hitsound = snd_hitsound1;
        hitbox2.scaling = 1;
        hitbox2.juggle = 2;
        hitbox3.hitsound = snd_hitsound1;
        hitbox3.scaling = 1;
        hitbox3.juggle = 2;
        hitbox4.hitsound = snd_hitsound1;
        hitbox4.scaling = 1;
        hitbox4.juggle = 2;
        hitbox0.airxtrayhit = 1;
        hitbox1.airxtrayhit = 1;
        hitbox2.airxtrayhit = 1;
        hitbox3.airxtrayhit = 1;
        hitbox4.airxtrayhit = 1;
        
        
    }
    
    attack_base(endstate,0,0,1,1,1);
}
else if(heat < -50)
{
    if(state_new == 1)
    {
        with(hurtbox0)
    	{
    	    xoffset = -29;
    	    yoffset = -31;
    	    image_xscale  = 58;
    	    image_yscale  = 63;
    	    xshift  = -1;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -15;
    	    yoffset = -56;
    	    image_xscale  = 30;
    	    image_yscale  = 25;
    	    xshift = -3;
    	}
    	
    	
    	with(collisionbox)
    	{
    	    xoffset = -20;
    	    yoffset = -22;
    	    image_xscale  = 40;
    	    image_yscale  = 54;
    	    xshift = 0;
    	}
    	
    	attackframe0 = 8;
    	endstate = 26;
    	
    }
    
    //animation
    sprite_index = spr_momo_2h_ice;
    
    if(state_timer == attackframe0 - 1)
    {
        vx = 6 * image_xscale;
    }
    
    //attack
    if(state_timer == attackframe0)
    {
        hitbox0 = create_hitbox(-6,-10,50,35,6.25,-18,16,6,11,12,-1,0,3,4,"low",1);
        
        attackhurtbox0 = create_hurtbox(8 * image_xscale, -17,22 * image_xscale,16,6,-2);
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 0.9;
        hitbox0.juggle = 1;
        hitbox0.airxtrayhit = -2.5;
       
        
        
    }
    
    attack_base(endstate,0,0,1,1,1);
}
else
{
    if(state_new == 1)
    {
        with(hurtbox0)
    	{
    	    xoffset = -29;
    	    yoffset = -31;
    	    image_xscale  = 58;
    	    image_yscale  = 63;
    	    xshift  = -1;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -15;
    	    yoffset = -56;
    	    image_xscale  = 30;
    	    image_yscale  = 25;
    	    xshift = -3;
    	}
    	
    	
    	with(collisionbox)
    	{
    	    xoffset = -20;
    	    yoffset = -22;
    	    image_xscale  = 40;
    	    image_yscale  = 54;
    	    xshift = 0;
    	}
    	
    	attackframe0 = 10;
    	endstate = 33;
    	
    }
    
    //animation
    sprite_index = spr_momo_2h;
    
    if(state_timer == attackframe0 - 1)
    {
        vx = 3 * image_xscale;
    }
    
    //attack
    if(state_timer == attackframe0)
    {
        hitbox0 = create_hitbox(5,0,90,31,6.25,-7,19,6,11,12,-1,0,3,4,"low",1);
        
        attackhurtbox0 = create_hurtbox(8 * image_xscale, -17,22 * image_xscale,16,6,-2);
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 0.9;
        hitbox0.juggle = 1;
        hitbox0.airxtrayhit = -2.5;
       
        
        
    }
    
    attack_base(endstate,0,0,1,1,1);
}
