{
    "id": "6b05ec4d-95b7-4742-8311-da6f1996a83b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_sblock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 52,
    "bbox_right": 88,
    "bbox_top": 75,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "77356b93-5cd5-440f-86ec-b783ef7968ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b05ec4d-95b7-4742-8311-da6f1996a83b",
            "compositeImage": {
                "id": "6ec2fdcc-1fae-49f9-baa2-3dd70d88ce28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77356b93-5cd5-440f-86ec-b783ef7968ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b18428be-f3c1-4caa-8498-c2a55e1b6c44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77356b93-5cd5-440f-86ec-b783ef7968ae",
                    "LayerId": "3147c272-f617-4586-b3fb-55dff82560ca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "3147c272-f617-4586-b3fb-55dff82560ca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b05ec4d-95b7-4742-8311-da6f1996a83b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}