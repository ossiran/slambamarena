if(state_new == 1)
{
	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -43;
	    image_xscale  = 26;
	    image_yscale  = 55;
	    xshift  = -2;
	}
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 16;
	    image_yscale  = 40;
	    xshift = -2
	}
	
	attackframe0 = 10;
	
	endstate = 125;
	
	c = 0;
}

//animation
sprite_index = spr_bill_cmair;

var hitfalladd = 0;

//juggling correctly
with(otherplayer)
{
    if(!onground() || vy < 0)
    {
        hitfalladd = 1;
    }
}


//attack
if(state_timer == attackframe0)
{
    hitbox1 = create_hitbox(-5, 0,20, 20, 2.75,-5, 15,15,9,4,0 + hitfalladd,0,120,3.5,"mid", "billdair" + string(player_num));
    hitbox2 = create_hitbox(11,12 ,14, 15, 2.75,-5, 15,15,9,4,0 + hitfalladd,0,120,3.5,"mid", "billdair" + string(player_num));
    hitbox3 = create_hitbox(20,17 ,14, 15, 2.75,-5, 15,15,9,4,0 + hitfalladd,0,120,3.5,"mid", "billdair" + string(player_num));
    attackhurtbox0 = create_hurtbox(12 * image_xscale, 2,24 * image_xscale,22,2,-2);
    hitbox1.scaling = 1.5;
    hitbox1.hitsound = snd_hitsound1;
    hitbox2.scaling = 1.5;
    hitbox2.hitsound = snd_hitsound1;
    hitbox3.scaling = 1.5;
    hitbox3.hitsound = snd_hitsound1;
}

/*
//attack
if(state_timer > attackframe0 && hitbox1 == -1)
{
    hitbox1 = create_hitbox(-5, 0,20, 20, 2.75,0, 15,15,9,4,0,0,120,3.5,"mid", "billdair" + string(player_num));
    hitbox2 = create_hitbox(11,12 ,14, 15, 2.75,0, 15,15,9,4,0,0,120,3.5,"mid", "billdair" + string(player_num));
    hitbox3 = create_hitbox(20,17 ,14, 15, 2.75,0, 15,15,9,4,0,0,120,3.5,"mid", "billdair" + string(player_num));
    attackhurtbox0 = create_hurtbox(12 * image_xscale, 2,24 * image_xscale,22,2,-2);
    hitbox1.scaling = 1.5;
    hitbox1.hitsound = snd_hitsound1;
    hitbox2.scaling = 1.5;
    hitbox2.hitsound = snd_hitsound1;
    hitbox3.scaling = 1.5;
    hitbox3.hitsound = snd_hitsound1;
}
*/

attack_base(endstate,1,0,0,1,0);

if(otherplayer.hitpause == 0)
{
    if(state_timer < attackframe0)
    {
        vx = 1 * image_xscale;
        vy = -2.75;
    }
    else
    {
        vx = 7.5 * image_xscale;
        vy = 7.25;
    }
}

if(state_timer < 8)
{
    image_index = 0;
}
else
{
    image_index = 1;
}