if(state_new == 1)
{
    dtap_right = 0;
    dtap_left = 0;
    light_buffer = 0;
    medium_buffer = 0;
}

sprite_index = spr_bill_fdash;

vx = Approach(vx, 7 * image_xscale, ground_accel*2);


//crouch
if(key_down == 1 && state_timer > 4)
{
    state_switch("crouch");
}

//fmedium
if(input_check(6,0,1,0,0) && state_timer > 1)
{
    chainmove =  "fmedium";
}

//fheavy
if(input_check(6,0,0,1,0) && state_timer > 1)
{
    chainmove =  "fheavy";
}

if(state_timer > 4)
{
            //jab
        if(input_check(0,1,0,0,0))
        {
            state_switch("light");
        }
        
        //clight
        if(input_check(2,1,0,0,0))
        {
            state_switch("clight");
        }
        
        //medium
        if(input_check(0,0,1,0,0))
        {
            state_switch("medium");
        }
        
        //cmedium
        if(input_check(1,0,1,0,0) || input_check(2,0,1,0,0) || input_check(3,0,1,0,0))
        {
            state_switch("cmedium");
        }
        
        
        //heavy
        if(input_check(0,0,0,1,0))
        {
            state_switch("heavy");
        }
        
        //grab
        if(input_check(0,1,0,0,1))
        {
            state_switch("grab");
        }
        
        //qcf
        if(qcf_buffer > 0)
        {
            if(input_check(0,0,0,1,0))
            {
                if(ex > 49.9)
                {
                    state_switch("qcfh");
                }
                else
                {
                    state_switch("qcfm");
                }
            }
            else if(input_check(0,0,1,0,0))
            {
                state_switch("qcfm");
            }
            else if(input_check(0,1,0,0,0))
            {
                state_switch("qcfl");
            }
        }
        
        //qcb
        if(qcb_buffer > 0)
        {
            if(input_check(0,0,0,1,0))
            {
                if(ex > 49.9)
                {
                    state_switch("qcbh");
                }
                else
                {
                    state_switch("qcbm");
                }
            }
            else if(input_check(0,0,1,0,0))
            {
                state_switch("qcbm");
            }
            else if(input_check(0,1,0,0,0))
            {
                state_switch("qcbl");
            }
        }
        
        //srk
        if(srk_buffer > 0)
        {
            if(input_check(0,0,0,1,0))
            {
                if(ex > 49.9)
                {
                    state_switch("srkh");
                }
                else
                {
                    state_switch("srkm");
                }
            }
            else if(input_check(0,0,1,0,0))
            {
                state_switch("srkm");
            }
            else if(input_check(0,1,0,0,0))
            {
                state_switch("srkl");
            }
        }
        
        if(dqcf_buffer > 0 && ex > 99.9)
        {
            if(input_check(0,0,0,1,0))
            {
                state_switch("dqcf");
            }
            else if(input_check(0,0,1,0,0))
            {
                state_switch("dqcf");
            }
            else if(input_check(0,1,0,0,0))
            {
                state_switch("dqcf");
            }
        }
        
        //fmedium
        if(input_check(6,0,1,0,0) || chainmove = "fmedium")
        {
            state_switch("fmedium");
        }
        
        //fheavy
        if(input_check(6,0,0,1,0) || chainmove = "fheavy")
        {
            state_switch("fheavy");
        }
}



//jump
if((key_up == 1 || key_jump == 1) && state_timer > 4)
{
	state_switch("jumpsquat")
	vx += 3.5 * image_xscale;
}

//air
if(!onground())
{
	state_switch("air");	
}

if(state_timer > 15)
{
    state_switch("stand");
}
//hitstun
if(hitstun > 0)
{
    state_switch("hitstun");   
}

