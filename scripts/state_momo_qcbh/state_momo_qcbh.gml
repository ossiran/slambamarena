live_call();

live_call();
if (synmode > 0)
{
    if(state_new == 1)
    {
    	with(hurtbox0)
    	{
    	    xoffset = -12;
    	    yoffset = -13;
    	    image_xscale  = 24;
    	    image_yscale  = 40;
    	    xshift  = 0;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -8;
    	    yoffset = -31;
    	    image_xscale  = 16;
    	    image_yscale  = 18;
    	    xshift = 0;
    	}
    	
    	with(collisionbox)
    	{
    	    xoffset = -8;
    	    yoffset = -13;
    	    image_xscale  = 16;
    	    image_yscale  = 40;
    	    xshift = 0;
    	}
    	
    	
    	attackframe0 =  16;
    	endstate = 38;
    }
    
    vx = 0;
    vy = 0;
    
    
    
    if(state_timer == attackframe0)
    {
        /*
        //deleting previous instances
        if(instance_exists(obj_darkball))
        {
            with(obj_darkball)
            {
                if(owner == other && mode == "light")
                {
                    instance_destroy();
                }
            }
        }
        
        if(instance_exists(obj_lightball))
        {
            with(obj_lightball)
            {
                if(owner == other && onplayer == 0 && mode == "light")
                {
                    instance_destroy();
                }
            }
        }
        */
        
        var lightball = instance_create_depth(x + (25 * image_xscale), y - 10, depth - 2, obj_lightball);
        with(lightball)
        {
            duration = 8;
            vx = 0.75 * other.image_xscale;
            owner = other;
            vxMax = 2.5;
            mode = "heavy";
            image_xscale = other.image_xscale * 2;
            image_yscale = 2;
        }
    }
    
    
    if(state_timer > attackframe0 && object_exists(obj_lightball) && state_timer < attackframe0 + 10)
    {
            with(obj_lightball)
            {
                if(owner == other)
                {
                    with(other)
                    {
                        if(key_light_hold == 1 && other.movementdone == 0)
                        {
                            endstate ++;
                            animationdelay ++;
                        }
                    }
                }
            }
    }
    
    
    sprite_index = spr_lsyn_qcf;
    
    attack_base(endstate,0,1,0,1,0);
}
else
{
    if(state_new == 1)
    {
    	with(hurtbox0)
    	{
    	    xoffset = -12;
    	    yoffset = -13;
    	    image_xscale  = 24;
    	    image_yscale  = 40;
    	    xshift  = 0;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -8;
    	    yoffset = -31;
    	    image_xscale  = 16;
    	    image_yscale  = 18;
    	    xshift = 0;
    	}
    	
    	with(collisionbox)
    	{
    	    xoffset = -8;
    	    yoffset = -13;
    	    image_xscale  = 16;
    	    image_yscale  = 40;
    	    xshift = 0;
    	}
    	
    	
    	attackframe0 =  15;
    	endstate = 38;
    }
    
    vx = 0;
    vy = 0;
    
    
    
    if(state_timer == attackframe0)
    {
        /*
        if(instance_exists(obj_darkball))
        {
            with(obj_darkball)
            {
                if(owner == other && mode == "he")
                {
                    instance_destroy();
                }
            }
        }
        
        if(instance_exists(obj_lightball))
        {
            with(obj_lightball)
            {
                if(owner == other && onplayer == 0 && mode == "light")
                {
                    instance_destroy();
                }
            }
        }
        */
        
        var darkball = instance_create_depth(x - (35 * image_xscale), y - 22, depth - 2, obj_darkball);
        with(darkball)
        {
            duration = 9;
            owner = other;
            image_xscale = other.image_xscale;
            mode = "heavy";
        }
    }
    
    /*
    if(state_timer > attackframe0 && object_exists(obj_lightball) && state_timer < attackframe0 + 21)
    {
            with(obj_darkball)
            {
                if(owner == other)
                {
                    with(other)
                    {
                        if(key_light_hold == 1 && other.movementdone == 0)
                        {
                            endstate ++;
                            animationdelay ++;
                        }
                    }
                }
            }
    }
    */
    
    sprite_index = spr_lsyn_qcf;
    
    attack_base(endstate,0,1,0,1,0);
}