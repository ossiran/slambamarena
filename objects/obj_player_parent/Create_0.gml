vx = 0;
vy = 0;
cx = 0;
cy = 0;

vxwas = 0;
vywas = 0;

ground_accel = 0;
air_accel = 0;
ground_fric = 0;
air_fric = 0;
grav = 0.75;
jumpheight = 0;
vxMax = 0;
dashdir = 0;

doublejump = 0;
airdash = 0;


xlastframe = 0;

rounds = 0;

//control variables
key_up = 0;
key_down = 0;
key_left = 0;
key_right = 0;
key_tapleft = 0;
key_tapright = 0;

key_light = 0;
key_light_hold = 0;
key_medium = 0;
key_medium_hold = 0;
key_heavy = 0;
key_heavy_hold = 0;
key_essence = 0;
key_essence_hold = 0;
key_dash = 0;
key_grab = 0;
key_jump = 0;
key_tapjump = 0;

key_start = 0;

player_num = 0;
controller_num = 0;
otherplayer = 0;
frontOrBack = 0;

/*
//dash variables
tapleft_counter = 0;
tapright_counter = 0;
dtap_right = 0;
dtap_left = 0;
dashcontinue = 0;
*/

//buffer variables
light_buffer = 0;
medium_buffer = 0;
heavy_buffer = 0;
dash_buffer = 0;
essence_buffer = 0;
chainmove = 0;
cancelmove = 0;

//attack variables

hurtbox0 = -1;
hurtbox1 = -1;
hurtbox2 = -1;
attackhurtbox0 = -1;
attackhurtbox1 = -1;
hitbox0 = -1;
hitbox1 = -1;
hitbox2 = -1;
hitbox3 = -1;
hitbox4 = -1;
collisionbox = -1;

hitstun = 0;
hit = 0;
hitpause = 0;
blockstun = 0;
blockstunwas = 0;
hitfall = 0;
launched = 0;
bounce = 0;
xhit = 0;
yhit = 0;
hitting = 0;
lastmod = 0;
stagger = 0;
grabbed = 0;
hitair = 0;
quickgetup = 0;
juggle = 0;

hitcounter = 0;
scaling = 0;
counterhit = 0;
combodamage = 0;

//genie
genie = "justice";
geniecounter = 0;
genieobj = -1;

//attack state variables
endstate = 0;
attackframe0 = 0;
attackframe1 = 0;
attackframe2 = 0;
attackframe3 = 0;
attackframe4 = 0;
attackframe5 = 0;
animationdelay = 1;

//damage and meter
hpmax = 0;
hp = 0;
ex = 0;
gx = 900;
gxrecover = 0;

gsprite = 0;
gbust = 0;
gbustrate = 0;

hurt_spriteA = 0;
hurt_spriteG = 0;
idle_sprite = 0;

//effects
trailduration = 0;
trailarray[4,4] = 0;
for(i = 0; i < 5; i++)
{
    for(i2 = 0; i2 < 5; i2++)
    {
        trailarray[i,i2] = 0;
    }
}
trailcounter = 0;

/*
//for super move freeze
otherplayersprites[4] = 0;
for(i = 0; i < 5; i++)
{
    otherplayersprites[i] = 0;
}
*/


exmove = 0;

input_machine_init();