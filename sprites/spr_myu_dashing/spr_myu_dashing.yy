{
    "id": "0de3b259-e7b3-4937-8ac2-ec5de673cf22",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_myu_dashing",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 12,
    "bbox_right": 35,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0314a42a-a7a7-4117-b61e-0fc5e62df423",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0de3b259-e7b3-4937-8ac2-ec5de673cf22",
            "compositeImage": {
                "id": "e53e7c91-2b37-48ae-a6ef-0979a1883421",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0314a42a-a7a7-4117-b61e-0fc5e62df423",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "006da02b-61cd-422c-ba2a-e2dda08a5db5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0314a42a-a7a7-4117-b61e-0fc5e62df423",
                    "LayerId": "daae6003-f69e-4bfb-a750-7f2b0cf3545e"
                }
            ]
        },
        {
            "id": "99100ed5-db0c-4e79-94c5-dd2bf9346d2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0de3b259-e7b3-4937-8ac2-ec5de673cf22",
            "compositeImage": {
                "id": "129cb040-9c1c-4ef7-9bcc-dbc856728624",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99100ed5-db0c-4e79-94c5-dd2bf9346d2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d67354b-4c1f-4bdc-bc88-d18c3f925db1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99100ed5-db0c-4e79-94c5-dd2bf9346d2a",
                    "LayerId": "daae6003-f69e-4bfb-a750-7f2b0cf3545e"
                }
            ]
        },
        {
            "id": "62a7783d-855b-428a-bca6-7a1c4f1a78ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0de3b259-e7b3-4937-8ac2-ec5de673cf22",
            "compositeImage": {
                "id": "d6744b40-ca82-41e0-93d6-3db81f51a72b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62a7783d-855b-428a-bca6-7a1c4f1a78ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8f523c3-5346-4a39-896b-6ee05b8cae96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62a7783d-855b-428a-bca6-7a1c4f1a78ea",
                    "LayerId": "daae6003-f69e-4bfb-a750-7f2b0cf3545e"
                }
            ]
        },
        {
            "id": "c865961d-5e38-4820-84db-4fb0e6c65795",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0de3b259-e7b3-4937-8ac2-ec5de673cf22",
            "compositeImage": {
                "id": "d7adcf8c-85e9-4482-b7a4-7ee3c871aa26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c865961d-5e38-4820-84db-4fb0e6c65795",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcbec94a-ca79-4e96-b0d3-a0a98378bd28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c865961d-5e38-4820-84db-4fb0e6c65795",
                    "LayerId": "daae6003-f69e-4bfb-a750-7f2b0cf3545e"
                }
            ]
        },
        {
            "id": "4a5450d1-d396-469a-aa0d-d77883f9a995",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0de3b259-e7b3-4937-8ac2-ec5de673cf22",
            "compositeImage": {
                "id": "c88787a4-083f-4ac7-8ac5-8344788f3039",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a5450d1-d396-469a-aa0d-d77883f9a995",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4be7553d-3ce5-47f8-bb72-f36754f519ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a5450d1-d396-469a-aa0d-d77883f9a995",
                    "LayerId": "daae6003-f69e-4bfb-a750-7f2b0cf3545e"
                }
            ]
        },
        {
            "id": "51e48707-478f-4add-a72c-3702010cbf3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0de3b259-e7b3-4937-8ac2-ec5de673cf22",
            "compositeImage": {
                "id": "6989bdd2-22ff-4dc7-94e0-c9f935016be5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51e48707-478f-4add-a72c-3702010cbf3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "382a96bb-0572-4249-a285-82064e68bea8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51e48707-478f-4add-a72c-3702010cbf3a",
                    "LayerId": "daae6003-f69e-4bfb-a750-7f2b0cf3545e"
                }
            ]
        },
        {
            "id": "0288f6a7-fe49-48d3-99a1-f022feb25dd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0de3b259-e7b3-4937-8ac2-ec5de673cf22",
            "compositeImage": {
                "id": "496e3632-6295-4e14-a6a6-b89398b40c17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0288f6a7-fe49-48d3-99a1-f022feb25dd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d13630a-378e-45c7-a3e6-f0df6c5b71ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0288f6a7-fe49-48d3-99a1-f022feb25dd3",
                    "LayerId": "daae6003-f69e-4bfb-a750-7f2b0cf3545e"
                }
            ]
        },
        {
            "id": "b0d60757-ea3e-44b4-bd6f-069df7dfd879",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0de3b259-e7b3-4937-8ac2-ec5de673cf22",
            "compositeImage": {
                "id": "1b442aa9-d6fd-4e2a-a2b1-243fba8392ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0d60757-ea3e-44b4-bd6f-069df7dfd879",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db227e94-451c-4a0e-bc77-fbbce06632aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0d60757-ea3e-44b4-bd6f-069df7dfd879",
                    "LayerId": "daae6003-f69e-4bfb-a750-7f2b0cf3545e"
                }
            ]
        },
        {
            "id": "471f436f-431a-4ae8-a9a4-72fd19ea5738",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0de3b259-e7b3-4937-8ac2-ec5de673cf22",
            "compositeImage": {
                "id": "523f95c7-aa75-478e-955f-7501b716dc93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "471f436f-431a-4ae8-a9a4-72fd19ea5738",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbfdb82d-192d-4672-88b6-0283efcda503",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "471f436f-431a-4ae8-a9a4-72fd19ea5738",
                    "LayerId": "daae6003-f69e-4bfb-a750-7f2b0cf3545e"
                }
            ]
        },
        {
            "id": "57dd9200-b244-48c3-8698-f63506df06b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0de3b259-e7b3-4937-8ac2-ec5de673cf22",
            "compositeImage": {
                "id": "d93f7b51-3bf8-4697-a28d-7c7ccaba9a6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57dd9200-b244-48c3-8698-f63506df06b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e548c3ec-7ce7-4fbb-b692-351c3573067e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57dd9200-b244-48c3-8698-f63506df06b0",
                    "LayerId": "daae6003-f69e-4bfb-a750-7f2b0cf3545e"
                }
            ]
        },
        {
            "id": "0aafd62d-7468-49a3-9f58-3d320ed95f54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0de3b259-e7b3-4937-8ac2-ec5de673cf22",
            "compositeImage": {
                "id": "98580f1e-8da0-40de-9d01-dbe0995eaec1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0aafd62d-7468-49a3-9f58-3d320ed95f54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "893483cf-533b-4d61-b07c-74de2a4493a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0aafd62d-7468-49a3-9f58-3d320ed95f54",
                    "LayerId": "daae6003-f69e-4bfb-a750-7f2b0cf3545e"
                }
            ]
        },
        {
            "id": "cffda4f3-99bb-4219-8ba5-e1d971224c45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0de3b259-e7b3-4937-8ac2-ec5de673cf22",
            "compositeImage": {
                "id": "0f0dd584-e2e0-4f25-ab56-2f636bf22c8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cffda4f3-99bb-4219-8ba5-e1d971224c45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b072e9a7-2548-42a6-b3fd-67c70a3dd0ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cffda4f3-99bb-4219-8ba5-e1d971224c45",
                    "LayerId": "daae6003-f69e-4bfb-a750-7f2b0cf3545e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "daae6003-f69e-4bfb-a750-7f2b0cf3545e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0de3b259-e7b3-4937-8ac2-ec5de673cf22",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 32
}