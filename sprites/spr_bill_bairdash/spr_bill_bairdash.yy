{
    "id": "15c3b0e2-40fa-4afa-aa25-3718d1d20b8a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_bairdash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 147,
    "bbox_left": 54,
    "bbox_right": 105,
    "bbox_top": 75,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e7d8d5e-dee7-419f-b49e-a97e8bb77bad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15c3b0e2-40fa-4afa-aa25-3718d1d20b8a",
            "compositeImage": {
                "id": "facc85e7-42aa-45d6-8e48-4b10873afd70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e7d8d5e-dee7-419f-b49e-a97e8bb77bad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5545aae7-867e-40a2-b7c6-bfb08b80c61e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e7d8d5e-dee7-419f-b49e-a97e8bb77bad",
                    "LayerId": "5f7b11a0-619c-44cf-81c7-646a870353bb"
                }
            ]
        },
        {
            "id": "eb6f3d91-752d-4560-a277-ff9f9344357d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15c3b0e2-40fa-4afa-aa25-3718d1d20b8a",
            "compositeImage": {
                "id": "cd28cdc3-da97-4afe-b697-77703ae708b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb6f3d91-752d-4560-a277-ff9f9344357d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d03e2cc3-d171-4c63-8e59-cf7c9204f2cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb6f3d91-752d-4560-a277-ff9f9344357d",
                    "LayerId": "5f7b11a0-619c-44cf-81c7-646a870353bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "5f7b11a0-619c-44cf-81c7-646a870353bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "15c3b0e2-40fa-4afa-aa25-3718d1d20b8a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}