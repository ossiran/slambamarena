{
    "id": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dsyn_5h",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 43,
    "bbox_right": 107,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "df76a204-9b02-458c-9ca1-e8dbd72a9d1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
            "compositeImage": {
                "id": "a27bcd0c-6208-42ab-9d43-88606be55ebd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df76a204-9b02-458c-9ca1-e8dbd72a9d1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10cfc2af-bdbf-4b7c-9d78-6f66e4342178",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df76a204-9b02-458c-9ca1-e8dbd72a9d1c",
                    "LayerId": "0927a4e7-56ea-4736-b376-a17b20f0c3a6"
                }
            ]
        },
        {
            "id": "55aedf7f-d94c-4c52-8f64-4df8f092328d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
            "compositeImage": {
                "id": "95ce2e9c-9e74-418a-afc7-929d13088023",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55aedf7f-d94c-4c52-8f64-4df8f092328d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd646315-2fea-4bbc-b7cd-e9e96b91c25f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55aedf7f-d94c-4c52-8f64-4df8f092328d",
                    "LayerId": "0927a4e7-56ea-4736-b376-a17b20f0c3a6"
                }
            ]
        },
        {
            "id": "3c8b2517-f1c5-4079-b690-51e5985a65a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
            "compositeImage": {
                "id": "fe4ee42a-f29e-49cb-8afa-dac190e41b74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c8b2517-f1c5-4079-b690-51e5985a65a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5528049-e5d6-49a1-baca-6ab236cd506e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c8b2517-f1c5-4079-b690-51e5985a65a4",
                    "LayerId": "0927a4e7-56ea-4736-b376-a17b20f0c3a6"
                }
            ]
        },
        {
            "id": "2d70af96-cdf9-4f73-bb49-140d96627bc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
            "compositeImage": {
                "id": "41623d09-2c1b-4d1f-912f-56a44f8b8929",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d70af96-cdf9-4f73-bb49-140d96627bc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd1b79a3-344b-4158-b40f-2fce256e7d87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d70af96-cdf9-4f73-bb49-140d96627bc0",
                    "LayerId": "0927a4e7-56ea-4736-b376-a17b20f0c3a6"
                }
            ]
        },
        {
            "id": "25ab9d94-2798-45ca-a817-db9a2297ec4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
            "compositeImage": {
                "id": "9f654e92-f09f-495d-95d8-ae2a818f15a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25ab9d94-2798-45ca-a817-db9a2297ec4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "074c8bb7-bf7e-4e4a-8669-bc31a2c20a08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25ab9d94-2798-45ca-a817-db9a2297ec4e",
                    "LayerId": "0927a4e7-56ea-4736-b376-a17b20f0c3a6"
                }
            ]
        },
        {
            "id": "8adb95b5-0e34-4b5f-b825-848636543751",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
            "compositeImage": {
                "id": "573eccb6-20d4-4e9d-9495-2a74d533bcdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8adb95b5-0e34-4b5f-b825-848636543751",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "718088c2-3ca3-4fcc-a9e5-073a1743c898",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8adb95b5-0e34-4b5f-b825-848636543751",
                    "LayerId": "0927a4e7-56ea-4736-b376-a17b20f0c3a6"
                }
            ]
        },
        {
            "id": "dfda6780-72b1-4a43-9664-bac15dbf5fc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
            "compositeImage": {
                "id": "02d6e1fe-13cc-4957-95e6-c67d11397403",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfda6780-72b1-4a43-9664-bac15dbf5fc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7161dd2-b946-437b-8181-af8842df977f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfda6780-72b1-4a43-9664-bac15dbf5fc1",
                    "LayerId": "0927a4e7-56ea-4736-b376-a17b20f0c3a6"
                }
            ]
        },
        {
            "id": "25658689-d0f3-4701-a17a-70c13bf57467",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
            "compositeImage": {
                "id": "c343fbf0-c85f-43b4-a659-d8c7c729ac85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25658689-d0f3-4701-a17a-70c13bf57467",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8fa1a10-46f4-4ba0-b9c8-204080aed237",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25658689-d0f3-4701-a17a-70c13bf57467",
                    "LayerId": "0927a4e7-56ea-4736-b376-a17b20f0c3a6"
                }
            ]
        },
        {
            "id": "f77e3250-33a8-4737-932e-85ac789b9284",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
            "compositeImage": {
                "id": "e86be138-63c4-4dd9-b744-3fdeb153784b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f77e3250-33a8-4737-932e-85ac789b9284",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "847d6e0f-9ce5-46ba-8f2d-156064128cfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f77e3250-33a8-4737-932e-85ac789b9284",
                    "LayerId": "0927a4e7-56ea-4736-b376-a17b20f0c3a6"
                }
            ]
        },
        {
            "id": "069bece5-7285-4114-a655-b0d229e4928e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
            "compositeImage": {
                "id": "34a33f54-5531-472d-83da-b36cf1b9067e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "069bece5-7285-4114-a655-b0d229e4928e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4effe03-de21-44a1-aceb-3293c94fb06d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "069bece5-7285-4114-a655-b0d229e4928e",
                    "LayerId": "0927a4e7-56ea-4736-b376-a17b20f0c3a6"
                }
            ]
        },
        {
            "id": "b6609835-0f22-434e-a4f7-93bd38178d31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
            "compositeImage": {
                "id": "0e18e102-56a4-44d7-8dd6-c1ece36d45c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6609835-0f22-434e-a4f7-93bd38178d31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45800595-a252-43d9-a12c-065347cd6972",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6609835-0f22-434e-a4f7-93bd38178d31",
                    "LayerId": "0927a4e7-56ea-4736-b376-a17b20f0c3a6"
                }
            ]
        },
        {
            "id": "a672b32b-15e6-45cc-89b1-3adbf691e24a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
            "compositeImage": {
                "id": "ae10fbab-0489-4b35-b69d-89de2fda86d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a672b32b-15e6-45cc-89b1-3adbf691e24a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b63dfb9-4548-4d1e-b7df-b8d6c897f570",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a672b32b-15e6-45cc-89b1-3adbf691e24a",
                    "LayerId": "0927a4e7-56ea-4736-b376-a17b20f0c3a6"
                }
            ]
        },
        {
            "id": "da245674-3d21-4a3a-8129-5c0b2707d90a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
            "compositeImage": {
                "id": "665cae48-08fe-4900-9e9f-7852ec2b5f52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da245674-3d21-4a3a-8129-5c0b2707d90a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "595df927-50d7-4da9-882d-825cce7d01cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da245674-3d21-4a3a-8129-5c0b2707d90a",
                    "LayerId": "0927a4e7-56ea-4736-b376-a17b20f0c3a6"
                }
            ]
        },
        {
            "id": "e27dbcc2-b60b-453b-beff-2323828cae56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
            "compositeImage": {
                "id": "c9a5f91e-fd9f-46b4-a34c-ae919bc02989",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e27dbcc2-b60b-453b-beff-2323828cae56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e8b5ab9-41b3-4ed8-8abc-19cbd438d859",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e27dbcc2-b60b-453b-beff-2323828cae56",
                    "LayerId": "0927a4e7-56ea-4736-b376-a17b20f0c3a6"
                }
            ]
        },
        {
            "id": "346ea977-c587-4863-a927-be1f07ea64a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
            "compositeImage": {
                "id": "2ffa6ae7-d4d5-448b-b74f-609dafeb1658",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "346ea977-c587-4863-a927-be1f07ea64a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5d9fc94-6644-495b-bba3-8995aedcbcab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "346ea977-c587-4863-a927-be1f07ea64a1",
                    "LayerId": "0927a4e7-56ea-4736-b376-a17b20f0c3a6"
                }
            ]
        },
        {
            "id": "be67999c-57c1-4886-99f0-9f839c045439",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
            "compositeImage": {
                "id": "1c793c61-6204-4cf7-a204-5b71c7e829e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be67999c-57c1-4886-99f0-9f839c045439",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c7a3051-2fad-4ace-984d-86742e249a1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be67999c-57c1-4886-99f0-9f839c045439",
                    "LayerId": "0927a4e7-56ea-4736-b376-a17b20f0c3a6"
                }
            ]
        },
        {
            "id": "3a226f5b-5786-499b-923c-f3c4e3b161ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
            "compositeImage": {
                "id": "a9931455-036f-440d-a67d-2517ac27d511",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a226f5b-5786-499b-923c-f3c4e3b161ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c27f6031-14b2-46d8-bd3f-56739a6f4293",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a226f5b-5786-499b-923c-f3c4e3b161ef",
                    "LayerId": "0927a4e7-56ea-4736-b376-a17b20f0c3a6"
                }
            ]
        },
        {
            "id": "913ae510-4dfe-4c1f-b42a-8ab7c396b82e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
            "compositeImage": {
                "id": "125c6c4f-bff5-4bab-ad02-8fa880559ac9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "913ae510-4dfe-4c1f-b42a-8ab7c396b82e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1e20bb1-37aa-41d6-9d11-5a9854325305",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "913ae510-4dfe-4c1f-b42a-8ab7c396b82e",
                    "LayerId": "0927a4e7-56ea-4736-b376-a17b20f0c3a6"
                }
            ]
        },
        {
            "id": "4ba8cc1e-367c-4acc-8793-6e52149316f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
            "compositeImage": {
                "id": "63b75aca-c134-4cc6-a082-7f00557c9d32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ba8cc1e-367c-4acc-8793-6e52149316f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec705fc7-4877-42e8-aaa8-73b3965716f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ba8cc1e-367c-4acc-8793-6e52149316f4",
                    "LayerId": "0927a4e7-56ea-4736-b376-a17b20f0c3a6"
                }
            ]
        },
        {
            "id": "cc13d67e-bdb1-4123-8dc0-8703d4dfff02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
            "compositeImage": {
                "id": "d60c04a4-d069-4138-92e4-758ba457cb44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc13d67e-bdb1-4123-8dc0-8703d4dfff02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3c0191a-72b5-458f-8a34-5d974ee73ed1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc13d67e-bdb1-4123-8dc0-8703d4dfff02",
                    "LayerId": "0927a4e7-56ea-4736-b376-a17b20f0c3a6"
                }
            ]
        },
        {
            "id": "d7c99f05-c908-4b4e-9d12-1fd898680234",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
            "compositeImage": {
                "id": "6f47d998-b645-4645-88c9-973b56f62e67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7c99f05-c908-4b4e-9d12-1fd898680234",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a4a38e7-9874-4070-a587-20c13d716c24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7c99f05-c908-4b4e-9d12-1fd898680234",
                    "LayerId": "0927a4e7-56ea-4736-b376-a17b20f0c3a6"
                }
            ]
        },
        {
            "id": "21816527-9aaf-4715-ad19-d31540aa3c39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
            "compositeImage": {
                "id": "2c0ea31c-bfaa-43ed-84ab-6e76565f4589",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21816527-9aaf-4715-ad19-d31540aa3c39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98487f63-7e43-4e45-84d3-cb8207ab83fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21816527-9aaf-4715-ad19-d31540aa3c39",
                    "LayerId": "0927a4e7-56ea-4736-b376-a17b20f0c3a6"
                }
            ]
        },
        {
            "id": "8f3c3302-8a8a-4232-87f5-65f03c5fbe9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
            "compositeImage": {
                "id": "38a6b6ab-d717-4510-a3e4-21d3e452a14c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f3c3302-8a8a-4232-87f5-65f03c5fbe9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41549ba1-2491-4bed-b7dd-83d2e3def666",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f3c3302-8a8a-4232-87f5-65f03c5fbe9c",
                    "LayerId": "0927a4e7-56ea-4736-b376-a17b20f0c3a6"
                }
            ]
        },
        {
            "id": "e3285f59-2e1b-48f9-bf79-d0393d9651c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
            "compositeImage": {
                "id": "413b1f31-e13e-49c8-a8a3-236e87ea5411",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3285f59-2e1b-48f9-bf79-d0393d9651c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03fe85e4-5daa-4bee-99c7-d83a1a36f57d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3285f59-2e1b-48f9-bf79-d0393d9651c6",
                    "LayerId": "0927a4e7-56ea-4736-b376-a17b20f0c3a6"
                }
            ]
        },
        {
            "id": "297ef9a1-3b11-4a8d-8ded-88e0a82f3c35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
            "compositeImage": {
                "id": "72a78bb7-7038-4659-b5bf-2c73e1518472",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "297ef9a1-3b11-4a8d-8ded-88e0a82f3c35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1b802a3-115a-4878-b209-ff415b6da837",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "297ef9a1-3b11-4a8d-8ded-88e0a82f3c35",
                    "LayerId": "0927a4e7-56ea-4736-b376-a17b20f0c3a6"
                }
            ]
        },
        {
            "id": "9fc3a14d-dbdc-4b0a-9209-75625242d9af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
            "compositeImage": {
                "id": "d72894eb-344f-4490-a0d6-e6ba8273ebc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fc3a14d-dbdc-4b0a-9209-75625242d9af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0300c97-9f5f-4936-896c-9485d89b0955",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fc3a14d-dbdc-4b0a-9209-75625242d9af",
                    "LayerId": "0927a4e7-56ea-4736-b376-a17b20f0c3a6"
                }
            ]
        },
        {
            "id": "4a3950c9-a004-4209-806b-10640832fc48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
            "compositeImage": {
                "id": "a560c368-46a8-4d04-a137-c72eceffc0d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a3950c9-a004-4209-806b-10640832fc48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19646495-a215-4e36-bda1-a532edf2858a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a3950c9-a004-4209-806b-10640832fc48",
                    "LayerId": "0927a4e7-56ea-4736-b376-a17b20f0c3a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "0927a4e7-56ea-4736-b376-a17b20f0c3a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1abff4aa-efd0-4e64-8826-0c0ebd16502b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}