{
    "id": "133d58ad-41c6-4ff1-9d5f-79cba5725591",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lsyn_2m",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 107,
    "bbox_left": 30,
    "bbox_right": 95,
    "bbox_top": 52,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c2300bb2-9e71-4d3c-a455-ebc6a7b265d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "133d58ad-41c6-4ff1-9d5f-79cba5725591",
            "compositeImage": {
                "id": "41fd2c76-c214-48fe-8c57-43730e50d267",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2300bb2-9e71-4d3c-a455-ebc6a7b265d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9a29797-22b5-4dca-8f36-d6bed7dda88f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2300bb2-9e71-4d3c-a455-ebc6a7b265d8",
                    "LayerId": "0bbc090e-d7c5-4ced-9caf-1ea42ed235f0"
                }
            ]
        },
        {
            "id": "8a85e4c5-527b-4c3f-885e-56aa987649d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "133d58ad-41c6-4ff1-9d5f-79cba5725591",
            "compositeImage": {
                "id": "0b4b8666-cdf9-4be5-9b4d-f3d0b20294b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a85e4c5-527b-4c3f-885e-56aa987649d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "047a8359-291b-475f-890c-2f6a70ad1412",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a85e4c5-527b-4c3f-885e-56aa987649d6",
                    "LayerId": "0bbc090e-d7c5-4ced-9caf-1ea42ed235f0"
                }
            ]
        },
        {
            "id": "2b357b82-efb2-4276-9a2d-f9c0cddcb7d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "133d58ad-41c6-4ff1-9d5f-79cba5725591",
            "compositeImage": {
                "id": "660057ce-c29a-4ac4-8cc2-10211774e50d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b357b82-efb2-4276-9a2d-f9c0cddcb7d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17cd6e82-a609-4a16-bd42-470c2a331562",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b357b82-efb2-4276-9a2d-f9c0cddcb7d9",
                    "LayerId": "0bbc090e-d7c5-4ced-9caf-1ea42ed235f0"
                }
            ]
        },
        {
            "id": "5c46b462-a382-4670-ab32-5a49b8b5a410",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "133d58ad-41c6-4ff1-9d5f-79cba5725591",
            "compositeImage": {
                "id": "da84499e-7e2f-4111-b61a-528a8152634c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c46b462-a382-4670-ab32-5a49b8b5a410",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2488cfa-e951-4893-9e2e-5b4d565ceb6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c46b462-a382-4670-ab32-5a49b8b5a410",
                    "LayerId": "0bbc090e-d7c5-4ced-9caf-1ea42ed235f0"
                }
            ]
        },
        {
            "id": "66c9aab8-b7be-4c80-87c4-666374cbb05e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "133d58ad-41c6-4ff1-9d5f-79cba5725591",
            "compositeImage": {
                "id": "f6fccb51-57c3-48cb-b4a0-779ae34afba8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66c9aab8-b7be-4c80-87c4-666374cbb05e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "180dc8ae-58e8-49ce-a707-4b01339e5ca8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66c9aab8-b7be-4c80-87c4-666374cbb05e",
                    "LayerId": "0bbc090e-d7c5-4ced-9caf-1ea42ed235f0"
                }
            ]
        },
        {
            "id": "475ec9d9-1e0f-4d25-aca1-2cda8deac988",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "133d58ad-41c6-4ff1-9d5f-79cba5725591",
            "compositeImage": {
                "id": "eae7f759-6fc1-4e5e-962b-25f9bda678ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "475ec9d9-1e0f-4d25-aca1-2cda8deac988",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c5743c8-08b1-4978-b8d0-319c053c61be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "475ec9d9-1e0f-4d25-aca1-2cda8deac988",
                    "LayerId": "0bbc090e-d7c5-4ced-9caf-1ea42ed235f0"
                }
            ]
        },
        {
            "id": "788772ae-0bef-495e-a41d-fb03b6096f09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "133d58ad-41c6-4ff1-9d5f-79cba5725591",
            "compositeImage": {
                "id": "4f5e73c6-a4b7-4fbe-93e6-970e1074783b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "788772ae-0bef-495e-a41d-fb03b6096f09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed073514-5242-4671-a249-f672af0ca630",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "788772ae-0bef-495e-a41d-fb03b6096f09",
                    "LayerId": "0bbc090e-d7c5-4ced-9caf-1ea42ed235f0"
                }
            ]
        },
        {
            "id": "8df18d80-9013-4593-9584-f3170e0030fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "133d58ad-41c6-4ff1-9d5f-79cba5725591",
            "compositeImage": {
                "id": "72222d3b-bed3-4657-a8d8-34643fff2367",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8df18d80-9013-4593-9584-f3170e0030fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "253d3739-4169-44b0-84aa-f8979af182de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8df18d80-9013-4593-9584-f3170e0030fb",
                    "LayerId": "0bbc090e-d7c5-4ced-9caf-1ea42ed235f0"
                }
            ]
        },
        {
            "id": "1f3c700f-9c02-4956-a26d-62c602e641b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "133d58ad-41c6-4ff1-9d5f-79cba5725591",
            "compositeImage": {
                "id": "76a34bae-af66-450d-ba2e-f75dbaec1d15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f3c700f-9c02-4956-a26d-62c602e641b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cfdd81d-0364-4b4b-ba23-e6992fc4ec3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f3c700f-9c02-4956-a26d-62c602e641b8",
                    "LayerId": "0bbc090e-d7c5-4ced-9caf-1ea42ed235f0"
                }
            ]
        },
        {
            "id": "3fab11b9-a2f6-47c7-814f-eab9c404c4d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "133d58ad-41c6-4ff1-9d5f-79cba5725591",
            "compositeImage": {
                "id": "dd5f4b44-b590-4e42-a3fb-e057d3d55f0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fab11b9-a2f6-47c7-814f-eab9c404c4d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fd1a332-9059-490b-9fab-9da99ec68f3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fab11b9-a2f6-47c7-814f-eab9c404c4d7",
                    "LayerId": "0bbc090e-d7c5-4ced-9caf-1ea42ed235f0"
                }
            ]
        },
        {
            "id": "54d4fdf5-ee59-4d1e-9c42-87cb661493ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "133d58ad-41c6-4ff1-9d5f-79cba5725591",
            "compositeImage": {
                "id": "a203200c-fc08-4f0b-a9b3-40b4bde2b009",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54d4fdf5-ee59-4d1e-9c42-87cb661493ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb33abf5-7106-4112-8365-9067f0919a40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54d4fdf5-ee59-4d1e-9c42-87cb661493ff",
                    "LayerId": "0bbc090e-d7c5-4ced-9caf-1ea42ed235f0"
                }
            ]
        },
        {
            "id": "a875b2fa-d556-4a19-b2eb-1f93f716ef16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "133d58ad-41c6-4ff1-9d5f-79cba5725591",
            "compositeImage": {
                "id": "fe197540-532a-4b54-94c8-d8f5608404dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a875b2fa-d556-4a19-b2eb-1f93f716ef16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f13bf03e-813e-4c4f-8cc2-7f62353c420c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a875b2fa-d556-4a19-b2eb-1f93f716ef16",
                    "LayerId": "0bbc090e-d7c5-4ced-9caf-1ea42ed235f0"
                }
            ]
        },
        {
            "id": "71208854-c07a-4e3b-ad82-9806f1679b8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "133d58ad-41c6-4ff1-9d5f-79cba5725591",
            "compositeImage": {
                "id": "9b15adc4-0072-454e-9775-5133209b4409",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71208854-c07a-4e3b-ad82-9806f1679b8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cac4c12-3525-453f-8904-bf548445f0c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71208854-c07a-4e3b-ad82-9806f1679b8e",
                    "LayerId": "0bbc090e-d7c5-4ced-9caf-1ea42ed235f0"
                }
            ]
        },
        {
            "id": "1946b4f6-6f66-4b28-858c-5509874d4445",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "133d58ad-41c6-4ff1-9d5f-79cba5725591",
            "compositeImage": {
                "id": "c685b082-230b-4308-8502-d4a4961d6ab0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1946b4f6-6f66-4b28-858c-5509874d4445",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f325f3b4-38bc-4524-bed7-e5b391b8bdc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1946b4f6-6f66-4b28-858c-5509874d4445",
                    "LayerId": "0bbc090e-d7c5-4ced-9caf-1ea42ed235f0"
                }
            ]
        },
        {
            "id": "30a2640f-c21a-4105-9581-19334fadeadc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "133d58ad-41c6-4ff1-9d5f-79cba5725591",
            "compositeImage": {
                "id": "4615abd9-36c4-41fc-8e02-eef52550a010",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30a2640f-c21a-4105-9581-19334fadeadc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a165977b-5543-4129-9262-379fe384a929",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30a2640f-c21a-4105-9581-19334fadeadc",
                    "LayerId": "0bbc090e-d7c5-4ced-9caf-1ea42ed235f0"
                }
            ]
        },
        {
            "id": "36e7ed76-f54a-4c10-abaf-f8b12ba8a163",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "133d58ad-41c6-4ff1-9d5f-79cba5725591",
            "compositeImage": {
                "id": "4bba6db9-00e0-4053-bd43-5a86590f63fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36e7ed76-f54a-4c10-abaf-f8b12ba8a163",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e931bf7-109d-482e-9318-cf8b74f72ae4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36e7ed76-f54a-4c10-abaf-f8b12ba8a163",
                    "LayerId": "0bbc090e-d7c5-4ced-9caf-1ea42ed235f0"
                }
            ]
        },
        {
            "id": "0ea5fd69-78f3-439b-b5dc-ba7cdc079dd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "133d58ad-41c6-4ff1-9d5f-79cba5725591",
            "compositeImage": {
                "id": "7f35848a-8bb8-4d85-9450-16f5bed38e60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ea5fd69-78f3-439b-b5dc-ba7cdc079dd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c8d108b-f9a7-4779-9a8a-aafc73f48dcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ea5fd69-78f3-439b-b5dc-ba7cdc079dd7",
                    "LayerId": "0bbc090e-d7c5-4ced-9caf-1ea42ed235f0"
                }
            ]
        },
        {
            "id": "aa57e07e-5214-4076-a294-576959eb64c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "133d58ad-41c6-4ff1-9d5f-79cba5725591",
            "compositeImage": {
                "id": "c430d5d3-0c0c-4eba-bc78-b0c30dc19c10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa57e07e-5214-4076-a294-576959eb64c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b546851-d50c-4210-85af-39442525c182",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa57e07e-5214-4076-a294-576959eb64c6",
                    "LayerId": "0bbc090e-d7c5-4ced-9caf-1ea42ed235f0"
                }
            ]
        },
        {
            "id": "39b8adf7-e77c-411d-ae33-01000838462f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "133d58ad-41c6-4ff1-9d5f-79cba5725591",
            "compositeImage": {
                "id": "fb09d295-71c4-4f12-9834-7fef0f2126b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39b8adf7-e77c-411d-ae33-01000838462f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef976b67-355c-425a-bc35-69f0d00eb36e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39b8adf7-e77c-411d-ae33-01000838462f",
                    "LayerId": "0bbc090e-d7c5-4ced-9caf-1ea42ed235f0"
                }
            ]
        },
        {
            "id": "7dbcf4a4-8afd-459b-b207-fd1699a14102",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "133d58ad-41c6-4ff1-9d5f-79cba5725591",
            "compositeImage": {
                "id": "99a8ab5f-8b23-439d-9422-4555592d8c6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dbcf4a4-8afd-459b-b207-fd1699a14102",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f97bd90-5c01-47cb-9161-5487a4bcdc97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dbcf4a4-8afd-459b-b207-fd1699a14102",
                    "LayerId": "0bbc090e-d7c5-4ced-9caf-1ea42ed235f0"
                }
            ]
        },
        {
            "id": "cb4bef1a-1857-419d-80db-6ee773037cd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "133d58ad-41c6-4ff1-9d5f-79cba5725591",
            "compositeImage": {
                "id": "b022700b-1bca-4769-a8e3-0a880f843d73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb4bef1a-1857-419d-80db-6ee773037cd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88106ac6-c984-4d5e-9b73-b0ee1b549223",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb4bef1a-1857-419d-80db-6ee773037cd8",
                    "LayerId": "0bbc090e-d7c5-4ced-9caf-1ea42ed235f0"
                }
            ]
        },
        {
            "id": "5c77c7a1-2c20-4e11-bad4-b6b984f19132",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "133d58ad-41c6-4ff1-9d5f-79cba5725591",
            "compositeImage": {
                "id": "34f863d9-ca17-457b-835a-cb4c913c5e1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c77c7a1-2c20-4e11-bad4-b6b984f19132",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daee962d-4d9e-4743-80ef-894f1b8797db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c77c7a1-2c20-4e11-bad4-b6b984f19132",
                    "LayerId": "0bbc090e-d7c5-4ced-9caf-1ea42ed235f0"
                }
            ]
        },
        {
            "id": "ac910f14-2fdc-40b8-835d-1068a9ce22ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "133d58ad-41c6-4ff1-9d5f-79cba5725591",
            "compositeImage": {
                "id": "bc665432-59fc-44d7-82cb-5fdc8cba0403",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac910f14-2fdc-40b8-835d-1068a9ce22ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02e06824-f07a-4e53-a0d2-cae79a794f11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac910f14-2fdc-40b8-835d-1068a9ce22ff",
                    "LayerId": "0bbc090e-d7c5-4ced-9caf-1ea42ed235f0"
                }
            ]
        },
        {
            "id": "d777277b-9b57-476b-a44d-457701095937",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "133d58ad-41c6-4ff1-9d5f-79cba5725591",
            "compositeImage": {
                "id": "bc8cda58-572a-44ff-8fd5-6f265da95210",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d777277b-9b57-476b-a44d-457701095937",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a122878-4028-4571-8c52-abfe7514e002",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d777277b-9b57-476b-a44d-457701095937",
                    "LayerId": "0bbc090e-d7c5-4ced-9caf-1ea42ed235f0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "0bbc090e-d7c5-4ced-9caf-1ea42ed235f0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "133d58ad-41c6-4ff1-9d5f-79cba5725591",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 76
}