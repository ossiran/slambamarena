{
    "id": "0536946f-9058-4c5a-820b-834c30b60fc2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_collisionbox",
    "eventList": [
        {
            "id": "4a6a4b39-30e5-4115-8dc8-eb3f5664a821",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0536946f-9058-4c5a-820b-834c30b60fc2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ea2076d6-5f22-4401-82c7-0114a69d5795",
    "visible": true
}