live_call();
if (synmode > 0)
{
    if(state_new == 1)
    {
        with(hurtbox0)
    	{
    	    xoffset = -12;
    	    yoffset = -13;
    	    image_xscale  = 24;
    	    image_yscale  = 40;
    	    xshift  = 0;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -8;
    	    yoffset = -31;
    	    image_xscale  = 16;
    	    image_yscale  = 18;
    	    xshift = 0;
    	}
    	
    	with(collisionbox)
    	{
    	    xoffset = -8;
    	    yoffset = -13;
    	    image_xscale  = 16;
    	    image_yscale  = 40;
    	    xshift = 0;
    	}
    	
    	chainmove = 0;
    	attackframe0 = 5;
    	endstate = 26;
    }
    
    //animation
    sprite_index = spr_lsyn_srkx;
    
    //attack
    if(state_timer == attackframe0)
    {
        hitbox0 = create_hitbox(17,31,45,-220,4.25,-12,21,7,12,15,-1,0,4,5,"mid",-1);
        attackhurtbox0 = create_hurtbox(8 * image_xscale, -17,22 * image_xscale,16,2,-2);
        hitbox0.chipdamage = 9;
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 1;
        hitbox0.juggle = 1;
        hitbox0.airxtrahitstun = -5;
        hitbox0.airxtrayhit = -4;
        ex += 4;
    }
    
    attack_base(endstate,0,0,0,1,0);
}
else
{
    if(state_new == 1)
    {
        with(hurtbox0)
    	{
    	    xoffset = -12;
    	    yoffset = -13;
    	    image_xscale  = 24;
    	    image_yscale  = 40;
    	    xshift  = 0;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -8;
    	    yoffset = -31;
    	    image_xscale  = 16;
    	    image_yscale  = 18;
    	    xshift = 0;
    	}
    	
    	with(collisionbox)
    	{
    	    xoffset = -8;
    	    yoffset = -13;
    	    image_xscale  = 16;
    	    image_yscale  = 40;
    	    xshift = 0;
    	}
    	
    	chainmove = 0;
    	attackframe0 = 17;
    	attackframe1 = 19;
    	endstate = 34;
    }
    
    //animation
    sprite_index = spr_dsyn_srk;
    
    if(state_timer > 1 && state_timer < 9)
    {
        vx = Approach(vx, 6 * image_xscale, 5);
        vy = Approach(vy, -10, 10);
    }
    
    if(state_timer > 8 && state_timer < 16)
    {
        vx = 0;
        vy = -2;
    }
    
    if(state_timer == attackframe0 - 2 && !hitpausing())
    {
        vy = 18;
    }
    
    if(state_timer == attackframe0)
    {
        hitbox0 = create_hitbox(-15, -15, 40, 50, 1, -5, 15, 6, 11, 16, -1, 0, 0, 2, "overhead", 2);
        hitbox0.chipdamage = 5;
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 0.5;
        hitbox0.airxtrayhit = 7;
        ex += 5;
        if(otherplayer.y < y -5)
        {
            hitbox0.airxtrayhit = 8;
        }
    }
    
    if(state_timer == attackframe1)
    {
        hitbox0 = create_hitbox(-15, -45, 40, 80, 1, 10, 18, 6, 11, 9, -1, 0, 0, 2, "overhead", 2);
        hitbox0.chipdamage = 8;
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 0.5;
        hitbox0.airxtrahitstun = 3;
        hitbox0.airxtrayhit = 5;
    }
    
    attack_base(endstate,0,0,0,1,0);
}