{
    "id": "ea2076d6-5f22-4401-82c7-0114a69d5795",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_collisionbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "525650a2-a204-40e5-a376-4baa37c385d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea2076d6-5f22-4401-82c7-0114a69d5795",
            "compositeImage": {
                "id": "94aa944a-4d79-4c92-a6a5-7466525f406c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "525650a2-a204-40e5-a376-4baa37c385d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d553a53f-fa71-47ce-85d5-6029689c7935",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "525650a2-a204-40e5-a376-4baa37c385d7",
                    "LayerId": "48ca3fd7-2587-4ba9-83a9-5afb82598ec1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "48ca3fd7-2587-4ba9-83a9-5afb82598ec1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea2076d6-5f22-4401-82c7-0114a69d5795",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}