/// @description Insert description here

//pausing for mid match pause menu
if(state_name == "start_match" || state_name == "start_round" || state_name == "mid_round" || state_name == "end_round")
{
    if(pause != 0)
    {
        if(pause > 0)
        {
            pause --;
        }
        
        //first frame of pause. Capturing all active objects that need pausing
        if(paused_objects[0,0] == -1)
        {
            //player objects
            if( object_exists(obj_player_parent))
            {
                with(obj_player_parent)
                {
                    if(other.unpaused_player != player_num)
                    {
                        for(i = 0; i < 11; i++)
                        {
                            if(other.paused_objects[i,0] == -1)
                            {
                                other.paused_objects[i,0] = sprite_index; 
                                other.paused_objects[i,1] = image_index;
                                other.paused_objects[i,2] = x;
                                other.paused_objects[i,3] = y;
                                other.paused_objects[i,4] = image_xscale;
                                other.paused_objects[i,5] = image_yscale;
                                other.paused_objects[i,6] = image_angle;
                                other.paused_objects[i,7] = image_alpha;
                                
                                break;
                            }
                        }
                    }
                }
            }
            
            //projectiles
            if(object_exists(obj_projectile_parent))
            {
                with(obj_projectile_parent)
                {
                    for(i = 0; i < 11; i++)
                    {
                        if(other.paused_objects[i,0] == -1)
                        {
                                other.paused_objects[i,0] = sprite_index; 
                                other.paused_objects[i,1] = image_index;
                                other.paused_objects[i,2] = x;
                                other.paused_objects[i,3] = y;
                                other.paused_objects[i,4] = image_xscale;
                                other.paused_objects[i,5] = image_yscale;
                                other.paused_objects[i,6] = image_angle;
                                other.paused_objects[i,7] = image_alpha;
                                
                                break;
                        }
                    }
                }
                
            }
        }
        
        //drawing from the paused objects array
        for(i = 0; i < 11; i++)
        {
            if(paused_objects[i,0] != -1)
            {
                draw_sprite_ext(paused_objects[i,0],paused_objects[i,1],paused_objects[i,2],paused_objects[i,3],paused_objects[i,4],paused_objects[i,5],paused_objects[i,6],c_white,paused_objects[i,7]);
                draw_set_alpha(0.75);
                draw_rectangle_color(0,0,room_width,room_height, c_black, c_black, c_black, c_black, 0);
                draw_set_alpha(1);
            }
        }
        
        /*
        if(pause == 0)
        {
            //clearing array
            for(i = 0; i < 11; i++)
            {
                
                for(ii = 0; ii < 7 ; ii++)
                {
                    paused_objects[i,ii] = -1;
                }
            }
            
            instance_activate_all();
        }
        */
    }
    else
    {
        //if unpausing with the pause button
        if(paused_objects[0,0] != -1)
        {
            //clearing array
            for(i = 0; i < 11; i++)
            {
                
                for(ii = 0; ii < 7 ; ii++)
                {
                    paused_objects[i,ii] = -1;
                }
            }
            unpaused_player = -1;
            instance_activate_all();
        }
    }
}

// You can write your code in this editor
if(player1 != 0 && player2 != 0)
{
    var ranx1 = 0;
    var rany1 = 0;
        
    var ranx2 = 0;
    var rany2 = 0;
    
    if(player1.hitpause > 0 || player2.hitpause > 0)
    {
        var ranx1 = irandom_range(-3,3);
        var rany1 = irandom_range(-3,3);
            
        var ranx2 = irandom_range(-1,1);
        var rany2 = irandom_range(-1,1);
    }
        
        with(obj_player_parent)
        {
            //trail
            if(trailduration > 0 && hitstun == 0)
            {
                for(i = 0; i < 5; i++)
                {
                    if(trailarray[i,0] != 0)
                    {
                        draw_sprite_ext(trailarray[i,0],trailarray[i,1],trailarray[i,2],trailarray[i,3],trailarray[i,4],1,1,c_yellow,0.75);
                    }
                }
                
                trailcounter++;
                if(trailcounter > 4)
                {
                    trailcounter = 0;
                }
            }
            
            /*
            //superflash
            if(otherplayersprites[0] != 0)
            {
                draw_sprite_ext(spr_barbox,1,0,0,800,800,1,c_black,0.55);
                draw_sprite_ext(otherplayersprites[0],otherplayersprites[1],otherplayersprites[2],otherplayersprites[3],otherplayersprites[4],1,1,c_white,1);
            }
            */
            /*
            if(hitpause > 0)
            {
                draw_sprite_ext(sprite_index,image_index,x+ranx2,y+rany2,-frontOrBack,1,1,c_white,1);
            }
            if(hitstun > 0)
            {
                draw_sprite_ext(sprite_index,image_index,x+ranx1,y+rany1,-frontOrBack,1,1,c_white,1);
            }
            else
            {
                draw_self();
            }
            */
            /*
            //hitpause and hitstun shaking. draws attacker first
            if(hitpause > 0)
            {
                draw_sprite_ext(sprite_index,image_index,x+ranx1,y+rany1,-frontOrBack,1,1,c_white,1);
                with(other.player2)
                {
                    if(hitpause > 0)
                    {
                        draw_sprite_ext(sprite_index,image_index,x+ranx1,y+rany1,-frontOrBack,1,1,c_white,1);
                    }
                    else if(hitstun > 0)
                    {
                        draw_sprite_ext(sprite_index,image_index,x+ranx2,y+rany2,-frontOrBack,1,1,c_white,1);
                    }
                    else
                    {
                        draw_self();
                    }
                }
            }
            else if(hitstun > 0)
            {
                draw_sprite_ext(sprite_index,image_index,x+ranx2,y+rany2,-frontOrBack,1,1,c_white,1);
                with(other.player2)
                {
                    if(hitpause > 0)
                    {
                        draw_sprite_ext(sprite_index,image_index,x+ranx1,y+rany1,-frontOrBack,1,1,c_white,1);
                    }
                    else if(hitstun > 0)
                    {
                        draw_sprite_ext(sprite_index,image_index,x+ranx2,y+rany2,-frontOrBack,1,1,c_white,1);
                    }
                    else
                    {
                        draw_self();
                    }
                }
            }
            else
            {
                with(other.player2)
                {
                    if(hitpause > 0)
                    {
                        draw_sprite_ext(sprite_index,image_index,x+ranx1,y+rany1,-frontOrBack,1,1,c_white,1);
                    }
                    else if(hitstun > 0)
                    {
                        draw_sprite_ext(sprite_index,image_index,x+ranx2,y+rany2,-frontOrBack,1,1,c_white,1);
                    }
                    else
                    {
                        draw_self();
                    }
                }
                draw_self();
            }
            */
            
            if(otherplayer != 0 && (hitting == 1 || vx > otherplayer.vx))
            {
                other.draw_priority = player_num;
            }
        }
        
        if(draw_priority = 1)
        {
            with(player2)
            {
                draw_sprite_ext(sprite_index,image_index,x+(ranx1 * sign(hitpause))+(ranx2 * sign(hitstun)) , y+(rany1 * sign(hitpause))+(rany2 * sign(hitstun)),image_xscale,image_yscale,0,c_white,1);
            }
            with(player1)
            {
                draw_sprite_ext(sprite_index,image_index,x+(ranx1 * sign(hitpause))+(ranx2 * sign(hitstun)) , y+(rany1 * sign(hitpause))+(rany2 * sign(hitstun)),image_xscale,image_yscale,0,c_white,1);
            }
        }
        else
        {
            with(player1)
            {
                draw_sprite_ext(sprite_index,image_index,x+(ranx1 * sign(hitpause))+(ranx2 * sign(hitstun)),y+(rany1 * sign(hitpause))+(rany2 * sign(hitstun)),image_xscale,image_yscale,0,c_white,1);
            }
            with(player2)
            {
                draw_sprite_ext(sprite_index,image_index,x+(ranx1 * sign(hitpause))+(ranx2 * sign(hitstun)),y+(rany1 * sign(hitpause))+(rany2 * sign(hitstun)),image_xscale,image_yscale,0,c_white,1);
            }
        }
}



//char2
if(state_name == "char_select2" && menu_maxy > 0)
{
    //draw grid with character portraits
    for(var i = 0; i < menu_maxx + 1; i++)
    {
        for(var i2 = 0; i2 < menu_maxy + 1; i2++)
        {
            //character sprite
            draw_sprite_ext(menu_array[i * array_height_2d(menu_array) + i2, 3],0, room_width/2 + (-64 * menu_maxx/2) + (i * 64), room_height/2 + -floor(menu_maxy/2) + (i2 * 64),2,2,0,c_white,1);
            
            
        }
    }
    with(obj_controller)
    {
        
        //drawing the hovered sprite
        
        //player 1
         if(obj_system.player1_controller == controller_num)
        {
            draw_sprite_ext(obj_system.menu_array[positionx * array_height_2d(obj_system.menu_array) + positiony, 2], obj_system.state_timer, room_width/4, 350, 3,3,0,c_white,1);
        }
        else
        {
            draw_sprite_ext(obj_system.menu_array[positionx * array_height_2d(obj_system.menu_array) + positiony, 2], obj_system.state_timer, 3 * room_width/4 , 350, -3,3,0,c_white,1);
        }
    }
}

//char3
if(state_name == "char_select3")
{
    //draw grid with stage portraits
    for(var i = 0; i < menu_maxx + 1; i++)
    {
        draw_sprite_ext(menu_array[i,1], 1, room_width/2 - menu_maxx/2 + 64 * i, room_height/2, 1, 1, 0, c_white, 1);
    }
}
