{
    "id": "223e6c7b-a1ba-4e44-a4e5-3b6e1272ffce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "justice_crouch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 136,
    "bbox_left": 54,
    "bbox_right": 105,
    "bbox_top": 74,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dbe43df4-2a71-449d-9e92-8e83ec50446b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e6c7b-a1ba-4e44-a4e5-3b6e1272ffce",
            "compositeImage": {
                "id": "53260ecb-67de-4bb2-9a85-15d034a4b76d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbe43df4-2a71-449d-9e92-8e83ec50446b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a90638d-356b-4d06-903f-348811e2f00d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbe43df4-2a71-449d-9e92-8e83ec50446b",
                    "LayerId": "0b0f2dc6-b77c-4605-aec4-b8c939b157a8"
                }
            ]
        },
        {
            "id": "d2914114-b6af-4fbf-9645-6574abbab36d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e6c7b-a1ba-4e44-a4e5-3b6e1272ffce",
            "compositeImage": {
                "id": "b807d39a-ee46-4b50-b4bb-31fa86165085",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2914114-b6af-4fbf-9645-6574abbab36d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79533ee2-8f65-403a-a6d0-f789dd04ddfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2914114-b6af-4fbf-9645-6574abbab36d",
                    "LayerId": "0b0f2dc6-b77c-4605-aec4-b8c939b157a8"
                }
            ]
        },
        {
            "id": "146bec14-df50-46dc-9bac-91dea49e4174",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e6c7b-a1ba-4e44-a4e5-3b6e1272ffce",
            "compositeImage": {
                "id": "8a8b5838-3e9c-4630-92b5-3d7ef9d45fb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "146bec14-df50-46dc-9bac-91dea49e4174",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "443989ba-7b55-438f-9b7c-201fae4c875d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "146bec14-df50-46dc-9bac-91dea49e4174",
                    "LayerId": "0b0f2dc6-b77c-4605-aec4-b8c939b157a8"
                }
            ]
        },
        {
            "id": "c1370488-8ce3-442c-8778-bea044a42bba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e6c7b-a1ba-4e44-a4e5-3b6e1272ffce",
            "compositeImage": {
                "id": "0fb83ba5-4b99-4f41-b4ec-29233718146a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1370488-8ce3-442c-8778-bea044a42bba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11799025-5f3a-4f01-85ac-041a52ea8392",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1370488-8ce3-442c-8778-bea044a42bba",
                    "LayerId": "0b0f2dc6-b77c-4605-aec4-b8c939b157a8"
                }
            ]
        },
        {
            "id": "ff176697-e092-4034-acdb-e3419ae6fe22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e6c7b-a1ba-4e44-a4e5-3b6e1272ffce",
            "compositeImage": {
                "id": "5cb61ce6-1353-4953-b55e-69d169981a80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff176697-e092-4034-acdb-e3419ae6fe22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6181926e-8d63-477a-852f-a6bb414aabb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff176697-e092-4034-acdb-e3419ae6fe22",
                    "LayerId": "0b0f2dc6-b77c-4605-aec4-b8c939b157a8"
                }
            ]
        },
        {
            "id": "ff8e73d7-a343-4fd2-a41e-b6861f880574",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e6c7b-a1ba-4e44-a4e5-3b6e1272ffce",
            "compositeImage": {
                "id": "6d41a6fd-f6df-4bfa-9817-9876859ff653",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff8e73d7-a343-4fd2-a41e-b6861f880574",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "444e6f8f-749a-48be-b85c-eec2a1d2cbce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff8e73d7-a343-4fd2-a41e-b6861f880574",
                    "LayerId": "0b0f2dc6-b77c-4605-aec4-b8c939b157a8"
                }
            ]
        },
        {
            "id": "79dc7348-92bc-42db-9f9d-c375124a4aec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e6c7b-a1ba-4e44-a4e5-3b6e1272ffce",
            "compositeImage": {
                "id": "515c8c2c-ab72-4a2b-b5a5-26dc7084abe5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79dc7348-92bc-42db-9f9d-c375124a4aec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65ba9dac-f40d-47e2-a5be-c1a7777a64b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79dc7348-92bc-42db-9f9d-c375124a4aec",
                    "LayerId": "0b0f2dc6-b77c-4605-aec4-b8c939b157a8"
                }
            ]
        },
        {
            "id": "0f391373-68bd-46b1-926f-b08100cbd2dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e6c7b-a1ba-4e44-a4e5-3b6e1272ffce",
            "compositeImage": {
                "id": "5989874a-f0c6-4fda-9920-cc8bd7c88b14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f391373-68bd-46b1-926f-b08100cbd2dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fac99c6-a9f2-445a-a913-1e83482c36c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f391373-68bd-46b1-926f-b08100cbd2dc",
                    "LayerId": "0b0f2dc6-b77c-4605-aec4-b8c939b157a8"
                }
            ]
        },
        {
            "id": "c45588a2-3c9c-4ed0-8dac-5138764771bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e6c7b-a1ba-4e44-a4e5-3b6e1272ffce",
            "compositeImage": {
                "id": "b6228d0d-0571-4c47-b025-4ecaa982fe3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c45588a2-3c9c-4ed0-8dac-5138764771bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "129853c1-1a3d-4cee-bc72-cfd27e04a343",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c45588a2-3c9c-4ed0-8dac-5138764771bf",
                    "LayerId": "0b0f2dc6-b77c-4605-aec4-b8c939b157a8"
                }
            ]
        },
        {
            "id": "0f7a6fbd-98e8-4df3-b841-5c80f227f465",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e6c7b-a1ba-4e44-a4e5-3b6e1272ffce",
            "compositeImage": {
                "id": "c39493e3-6ded-486a-b364-c4136e447ee7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f7a6fbd-98e8-4df3-b841-5c80f227f465",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd812ad7-109e-47f6-b0b5-4eb500f46d69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f7a6fbd-98e8-4df3-b841-5c80f227f465",
                    "LayerId": "0b0f2dc6-b77c-4605-aec4-b8c939b157a8"
                }
            ]
        },
        {
            "id": "733874d8-4ea9-4a84-b252-6b8bc76a010b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e6c7b-a1ba-4e44-a4e5-3b6e1272ffce",
            "compositeImage": {
                "id": "ba319059-a95b-4691-8573-366954bdb0e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "733874d8-4ea9-4a84-b252-6b8bc76a010b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5794482c-5c2d-4817-bbb9-8b722db4882d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "733874d8-4ea9-4a84-b252-6b8bc76a010b",
                    "LayerId": "0b0f2dc6-b77c-4605-aec4-b8c939b157a8"
                }
            ]
        },
        {
            "id": "62890b2e-d1a4-4cea-a0d6-57fc12eb95d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e6c7b-a1ba-4e44-a4e5-3b6e1272ffce",
            "compositeImage": {
                "id": "63013436-ab9a-49a1-9bee-a7692fb48cc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62890b2e-d1a4-4cea-a0d6-57fc12eb95d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf1b5b86-ab4b-4ffa-bdcb-28aa69de8ba5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62890b2e-d1a4-4cea-a0d6-57fc12eb95d2",
                    "LayerId": "0b0f2dc6-b77c-4605-aec4-b8c939b157a8"
                }
            ]
        },
        {
            "id": "b1201a20-29d6-44b3-bd4f-cfe1b28aef43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e6c7b-a1ba-4e44-a4e5-3b6e1272ffce",
            "compositeImage": {
                "id": "af7005c6-792b-48ce-b60f-4f4c1bfbc134",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1201a20-29d6-44b3-bd4f-cfe1b28aef43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f7f4cc2-7b89-4b17-b2b9-06a3bee75708",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1201a20-29d6-44b3-bd4f-cfe1b28aef43",
                    "LayerId": "0b0f2dc6-b77c-4605-aec4-b8c939b157a8"
                }
            ]
        },
        {
            "id": "ce71209b-8167-4ee1-803d-b584f8ddfbd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e6c7b-a1ba-4e44-a4e5-3b6e1272ffce",
            "compositeImage": {
                "id": "6d78c42c-4293-4d8c-acc2-5090989ffdcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce71209b-8167-4ee1-803d-b584f8ddfbd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97341e45-164b-4f09-96d1-7069fe7e1f35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce71209b-8167-4ee1-803d-b584f8ddfbd6",
                    "LayerId": "0b0f2dc6-b77c-4605-aec4-b8c939b157a8"
                }
            ]
        },
        {
            "id": "ea1bf682-898d-4d6b-b946-8f57eeab44ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e6c7b-a1ba-4e44-a4e5-3b6e1272ffce",
            "compositeImage": {
                "id": "15f32b4c-121d-4057-b6b1-9f00ea8f1573",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea1bf682-898d-4d6b-b946-8f57eeab44ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0d01c0d-3da0-4bb9-8101-6858018aff60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea1bf682-898d-4d6b-b946-8f57eeab44ba",
                    "LayerId": "0b0f2dc6-b77c-4605-aec4-b8c939b157a8"
                }
            ]
        },
        {
            "id": "9f4b0a26-c967-47fd-bbec-e320102ba2ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e6c7b-a1ba-4e44-a4e5-3b6e1272ffce",
            "compositeImage": {
                "id": "3ac9d8d7-6d66-4588-b00b-765fd216bc0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f4b0a26-c967-47fd-bbec-e320102ba2ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7450824c-3c22-4206-a408-d1f5cc6ca252",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f4b0a26-c967-47fd-bbec-e320102ba2ec",
                    "LayerId": "0b0f2dc6-b77c-4605-aec4-b8c939b157a8"
                }
            ]
        },
        {
            "id": "b3564c00-3658-4414-a8eb-d38a9807c679",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e6c7b-a1ba-4e44-a4e5-3b6e1272ffce",
            "compositeImage": {
                "id": "c36675a1-01d2-47d9-b9ea-b2f91d6bd67e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3564c00-3658-4414-a8eb-d38a9807c679",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e69ede82-5faf-4940-8251-2e4f4e05b216",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3564c00-3658-4414-a8eb-d38a9807c679",
                    "LayerId": "0b0f2dc6-b77c-4605-aec4-b8c939b157a8"
                }
            ]
        },
        {
            "id": "6cb60406-f645-427e-a524-0441709a11e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e6c7b-a1ba-4e44-a4e5-3b6e1272ffce",
            "compositeImage": {
                "id": "8d8f4176-093c-4c30-a247-106a7d5888bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cb60406-f645-427e-a524-0441709a11e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1beb9c21-6173-4e6d-8dd9-6024a861c967",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cb60406-f645-427e-a524-0441709a11e2",
                    "LayerId": "0b0f2dc6-b77c-4605-aec4-b8c939b157a8"
                }
            ]
        },
        {
            "id": "af3027f2-4397-49fd-a761-d75915cbb614",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e6c7b-a1ba-4e44-a4e5-3b6e1272ffce",
            "compositeImage": {
                "id": "6019cfe9-2740-4cb7-909f-ce20319aa347",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af3027f2-4397-49fd-a761-d75915cbb614",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b52dec4-bb84-461f-adaf-60367a4fa818",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af3027f2-4397-49fd-a761-d75915cbb614",
                    "LayerId": "0b0f2dc6-b77c-4605-aec4-b8c939b157a8"
                }
            ]
        },
        {
            "id": "e0194b9e-35d9-4e2a-a9d8-f83c2043d365",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e6c7b-a1ba-4e44-a4e5-3b6e1272ffce",
            "compositeImage": {
                "id": "087981d7-c7cd-4d23-be27-bd54c2e7cadb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0194b9e-35d9-4e2a-a9d8-f83c2043d365",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f83454f-3a84-4ba3-a1f6-ef642544ec4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0194b9e-35d9-4e2a-a9d8-f83c2043d365",
                    "LayerId": "0b0f2dc6-b77c-4605-aec4-b8c939b157a8"
                }
            ]
        },
        {
            "id": "c5bc0df8-4303-4b4f-b496-c44846837909",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e6c7b-a1ba-4e44-a4e5-3b6e1272ffce",
            "compositeImage": {
                "id": "77396fcc-c280-4f5a-9857-256422ebaf2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5bc0df8-4303-4b4f-b496-c44846837909",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da2d83d1-ddea-4d48-a01e-8ae220e71cea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5bc0df8-4303-4b4f-b496-c44846837909",
                    "LayerId": "0b0f2dc6-b77c-4605-aec4-b8c939b157a8"
                }
            ]
        },
        {
            "id": "a79caa93-65d6-4b63-8025-00538d2952f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e6c7b-a1ba-4e44-a4e5-3b6e1272ffce",
            "compositeImage": {
                "id": "d1e788e6-5aef-4032-8876-b8c5e2a3cb8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a79caa93-65d6-4b63-8025-00538d2952f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5041e42c-2fd2-44c3-b8e2-af21eb491217",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a79caa93-65d6-4b63-8025-00538d2952f2",
                    "LayerId": "0b0f2dc6-b77c-4605-aec4-b8c939b157a8"
                }
            ]
        },
        {
            "id": "d11d06b5-6c30-4c10-8dbb-325a79f401ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e6c7b-a1ba-4e44-a4e5-3b6e1272ffce",
            "compositeImage": {
                "id": "30587141-82e9-400c-b181-8996516e1dc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d11d06b5-6c30-4c10-8dbb-325a79f401ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08bf5c0d-59b7-4047-8e17-465cdd36aa7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d11d06b5-6c30-4c10-8dbb-325a79f401ab",
                    "LayerId": "0b0f2dc6-b77c-4605-aec4-b8c939b157a8"
                }
            ]
        },
        {
            "id": "1eccabcd-a317-469c-8aac-9d85e5c0770f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e6c7b-a1ba-4e44-a4e5-3b6e1272ffce",
            "compositeImage": {
                "id": "53aa3a30-6db4-4ad5-9d39-a9819ed04913",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1eccabcd-a317-469c-8aac-9d85e5c0770f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32b1d42f-1833-492d-ba05-006f0112c80b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1eccabcd-a317-469c-8aac-9d85e5c0770f",
                    "LayerId": "0b0f2dc6-b77c-4605-aec4-b8c939b157a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "0b0f2dc6-b77c-4605-aec4-b8c939b157a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "223e6c7b-a1ba-4e44-a4e5-3b6e1272ffce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 79,
    "yorig": 121
}