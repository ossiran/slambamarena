{
    "id": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_qcfl",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 49,
    "bbox_right": 105,
    "bbox_top": 75,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0af022cb-eb28-46c0-8d07-719d8052d3fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "2ca047f1-feb3-40cd-be42-969b1149e97b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0af022cb-eb28-46c0-8d07-719d8052d3fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c4cc619-15ab-4e17-b39c-160bade4758a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0af022cb-eb28-46c0-8d07-719d8052d3fe",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "0c5c8273-8548-45b2-84b3-196a9ac243ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "7d00327f-3794-4a5b-849d-0517b76f5bc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c5c8273-8548-45b2-84b3-196a9ac243ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6db07541-83d9-4d97-9b48-631777ecbe1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c5c8273-8548-45b2-84b3-196a9ac243ff",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "76b3da4f-93fd-4ec1-8147-f125dfb3fd66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "d55411f4-981e-45df-91f8-a2a1fb872fbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76b3da4f-93fd-4ec1-8147-f125dfb3fd66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e34f84d-8030-4c67-b1b8-241d50b32243",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76b3da4f-93fd-4ec1-8147-f125dfb3fd66",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "e59f8a13-b833-4b7b-9437-270338f362d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "8610e124-9e5f-4df6-bb36-fc79af40cfc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e59f8a13-b833-4b7b-9437-270338f362d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9967822-422b-4348-9bbe-d6802c69eab8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e59f8a13-b833-4b7b-9437-270338f362d8",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "994173b1-326a-436d-8ff5-d7ec7c979a3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "a754f4ba-b891-4db7-971d-c21f8bb12c4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "994173b1-326a-436d-8ff5-d7ec7c979a3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4133b3b1-9855-4767-a930-cc89ec608d9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "994173b1-326a-436d-8ff5-d7ec7c979a3e",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "fa55d220-0b98-4c9b-a341-842e7c7a7342",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "3ec59eea-9810-4c81-bc09-8bac540cc261",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa55d220-0b98-4c9b-a341-842e7c7a7342",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01c879f1-05bc-4dfc-b42d-000fbabd9e9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa55d220-0b98-4c9b-a341-842e7c7a7342",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "4d92181a-6a32-47ec-97ca-1a9cd6ae8745",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "be263f0c-000b-454d-b6fa-07016907ec40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d92181a-6a32-47ec-97ca-1a9cd6ae8745",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11f47a2e-16f3-4605-9393-ee2438c6c2aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d92181a-6a32-47ec-97ca-1a9cd6ae8745",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "eea95710-7b02-44b5-9be9-6a5656f2a9ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "03dda120-49fc-4988-acf7-d9d5fd8b68ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eea95710-7b02-44b5-9be9-6a5656f2a9ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dea743e4-1de3-4e23-bf10-0809d82b1323",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eea95710-7b02-44b5-9be9-6a5656f2a9ca",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "c86bae88-6ec9-442f-9842-fa44412f4474",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "20a207c6-8201-4de5-8641-7dc3a9974cf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c86bae88-6ec9-442f-9842-fa44412f4474",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e260b7e4-5fc2-479e-b65e-2a3bbc3a1ddf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c86bae88-6ec9-442f-9842-fa44412f4474",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "f05cf398-b384-4e8e-a07f-b8b66d984a25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "f09594f7-54d0-40ae-9be9-6eeab5b58468",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f05cf398-b384-4e8e-a07f-b8b66d984a25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02615cf3-713c-48a1-b436-68953ec46695",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f05cf398-b384-4e8e-a07f-b8b66d984a25",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "13d4b749-53e2-496f-be13-959784dcf9a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "1629233d-a8ef-4c9a-96b5-f15ea5c10690",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13d4b749-53e2-496f-be13-959784dcf9a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1491c72d-5b12-4fc0-b5b0-7b85218a78a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13d4b749-53e2-496f-be13-959784dcf9a4",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "15cc5bb2-893c-40cb-9758-ffb9c5507c44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "cbf00fdd-c607-4f0f-8a24-74df32c1adfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15cc5bb2-893c-40cb-9758-ffb9c5507c44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e13bcab0-8710-48aa-82e6-6d0c4c643b85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15cc5bb2-893c-40cb-9758-ffb9c5507c44",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "ce4b98d8-e003-40bd-841f-dd0a90ad4969",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "d8b206e5-325a-477f-81e2-d5508ab1bb64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce4b98d8-e003-40bd-841f-dd0a90ad4969",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25646526-982e-44e1-83e2-b6f274103025",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce4b98d8-e003-40bd-841f-dd0a90ad4969",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "eb961abd-1bd9-4174-a50b-bc9a28c95e70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "e42b68bb-fc69-498e-b5e0-3c0db2dcd532",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb961abd-1bd9-4174-a50b-bc9a28c95e70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8dc30b2-d214-427e-a3f2-1d01df5ffe32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb961abd-1bd9-4174-a50b-bc9a28c95e70",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "6f998ccf-1ec7-40e7-b6f7-4722a1951f3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "e55fc9c7-3673-4a55-ba6b-53e634884f4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f998ccf-1ec7-40e7-b6f7-4722a1951f3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d9a68bb-a77e-4d54-8a58-79f2fa7b5001",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f998ccf-1ec7-40e7-b6f7-4722a1951f3a",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "70147446-745b-41b6-8ee7-2a12b915eb72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "a6344b74-c540-47f5-a496-e4ab79b6ed2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70147446-745b-41b6-8ee7-2a12b915eb72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fcd99e3-cf84-4aae-b748-8b5ec0bce697",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70147446-745b-41b6-8ee7-2a12b915eb72",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "c8653c58-fee7-4d91-a248-5b0de1c7f43c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "868fa6d6-55d4-4a8c-a124-e6031c8bc355",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8653c58-fee7-4d91-a248-5b0de1c7f43c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "845bcb4c-ea8e-4da2-a515-828a4c7f1cf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8653c58-fee7-4d91-a248-5b0de1c7f43c",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "3902fcc2-1082-4bb1-a56e-1935cc6b2c92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "6fff22cd-fc9c-4d28-b863-625862178d77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3902fcc2-1082-4bb1-a56e-1935cc6b2c92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c733d56c-4926-4817-95bb-b794f0a9fc53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3902fcc2-1082-4bb1-a56e-1935cc6b2c92",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "b68e2284-f169-40d9-b4c4-3b949fa665c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "f3313229-a758-4513-897d-e95331b4f1e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b68e2284-f169-40d9-b4c4-3b949fa665c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b62aac3-fae6-478f-a061-19124679a914",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b68e2284-f169-40d9-b4c4-3b949fa665c7",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "5828346f-8eda-45ed-8b83-6a52ab842416",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "e6ad185c-6198-48d4-9cc4-48773911fc47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5828346f-8eda-45ed-8b83-6a52ab842416",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c12460e-6869-4b57-a973-43d19f2417a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5828346f-8eda-45ed-8b83-6a52ab842416",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "0e6b9f7e-5e4b-4c63-87d6-aadf933af7b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "08032991-547d-495c-b35c-945c7d09a8f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e6b9f7e-5e4b-4c63-87d6-aadf933af7b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99dea267-8e4a-45bc-807d-7787a2c400d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e6b9f7e-5e4b-4c63-87d6-aadf933af7b4",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "105502e1-7e75-44cd-a3eb-19e4503e79c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "656b2adf-0cd9-459a-ab06-d6f34b668006",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "105502e1-7e75-44cd-a3eb-19e4503e79c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45120859-bee9-4567-a527-e624078c479d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "105502e1-7e75-44cd-a3eb-19e4503e79c8",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "77e046b2-2244-4de2-9561-5eb3583a7455",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "0dd4a130-fa3b-475b-a343-5292879e5c21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77e046b2-2244-4de2-9561-5eb3583a7455",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e489851-6bcc-4b00-b9d0-e1c5a8619b6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77e046b2-2244-4de2-9561-5eb3583a7455",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "2c346526-0990-41db-8af1-e22747bdc92e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "36dbc311-8886-440f-8267-94cadcb4e34a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c346526-0990-41db-8af1-e22747bdc92e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7158cab7-f1b2-48ec-8bfe-fd004fc2ede4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c346526-0990-41db-8af1-e22747bdc92e",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "b366131f-1756-4ec1-bdb5-b57bac6f3b40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "919acc5f-3bea-4658-a758-38febb8dcf82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b366131f-1756-4ec1-bdb5-b57bac6f3b40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47fb9eaf-c9eb-4a73-a573-0bcd11bcada5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b366131f-1756-4ec1-bdb5-b57bac6f3b40",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "00b77fb2-4c58-4758-8a41-013587cf35ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "b5d897cf-7962-4e0d-a31f-cb5d582a8d30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00b77fb2-4c58-4758-8a41-013587cf35ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0f898ed-20aa-4247-b785-e87f242cfc40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00b77fb2-4c58-4758-8a41-013587cf35ea",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "ec288c68-aab0-4d91-a28f-2d273be901f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "dff37d66-8db3-4b57-9c5b-909766dc3163",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec288c68-aab0-4d91-a28f-2d273be901f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2dbd0b0-71cb-4551-abab-9ad7c7f7c638",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec288c68-aab0-4d91-a28f-2d273be901f0",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "603a4f68-a822-47e6-97bc-63211f5cf415",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "c5ad804c-e0e3-41be-878f-337b9510e2cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "603a4f68-a822-47e6-97bc-63211f5cf415",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c83fabfe-3474-4940-8049-da346566695d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "603a4f68-a822-47e6-97bc-63211f5cf415",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "3702bf43-82a9-4b20-95c2-f625705ee6e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "c7941214-5ef3-483c-bf9a-cc3c359f02ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3702bf43-82a9-4b20-95c2-f625705ee6e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84fa4d1a-564b-47d0-b6de-de4c24a39a7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3702bf43-82a9-4b20-95c2-f625705ee6e5",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "da4979ea-9f86-4fbd-883d-fe97551c9744",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "57f5d80d-316e-43a6-836d-8ddc87cd2e76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da4979ea-9f86-4fbd-883d-fe97551c9744",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6058d83a-b681-40a9-a492-3f4383c8e784",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da4979ea-9f86-4fbd-883d-fe97551c9744",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "b764edaf-e98c-4804-9526-4c6452c99c1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "6ca507fc-255e-4466-ba68-053b9298cbb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b764edaf-e98c-4804-9526-4c6452c99c1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58b422f2-541a-4146-bb48-df3acd980c0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b764edaf-e98c-4804-9526-4c6452c99c1d",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "0bf3a3cd-e58e-42dd-a7a9-a8cf4a889e76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "d3011434-b0a8-488c-804b-24452e262fda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bf3a3cd-e58e-42dd-a7a9-a8cf4a889e76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b442aea-d1af-4bfe-94e2-648b1f743f53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bf3a3cd-e58e-42dd-a7a9-a8cf4a889e76",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "0c7a3d5a-5839-47c4-8e54-7b4f069faa44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "9f025a69-cd16-4228-aa43-b2268523ceaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c7a3d5a-5839-47c4-8e54-7b4f069faa44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e13d2191-2ad3-474a-8772-79c2ab0e7911",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c7a3d5a-5839-47c4-8e54-7b4f069faa44",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "d0dcc9a0-1363-4c70-8e9d-ba4dc906c6e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "5d77ef24-05cb-49e3-bbf1-1aa930081d98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0dcc9a0-1363-4c70-8e9d-ba4dc906c6e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4b6f113-69fb-4447-ac31-af93a49d5d62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0dcc9a0-1363-4c70-8e9d-ba4dc906c6e3",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        },
        {
            "id": "49d7ab50-31fa-40b8-a8d5-a50100300f41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "compositeImage": {
                "id": "a1b3bb97-9d03-4172-93d4-da814a648897",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49d7ab50-31fa-40b8-a8d5-a50100300f41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7fe1ee7-4793-44c2-8950-19efb95baf95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49d7ab50-31fa-40b8-a8d5-a50100300f41",
                    "LayerId": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "c8f17be2-9e9d-4eaa-85d2-e742d4cce26a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9459e133-624f-4aad-8da7-0fb3fcfd5b47",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}