{
    "id": "e263b98e-9868-4a29-84c5-0ff9cb31fdf1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_jumpsquat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 49,
    "bbox_right": 97,
    "bbox_top": 85,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "12278c3a-d252-4447-9d8d-ac79b855da15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e263b98e-9868-4a29-84c5-0ff9cb31fdf1",
            "compositeImage": {
                "id": "775b1a06-4a58-40e0-a1d9-e64b449ea7b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12278c3a-d252-4447-9d8d-ac79b855da15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c910d6ca-cddc-41cc-bf62-89c7330688b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12278c3a-d252-4447-9d8d-ac79b855da15",
                    "LayerId": "ca35500e-3703-4fd8-871b-f3938a3ecf1b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "ca35500e-3703-4fd8-871b-f3938a3ecf1b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e263b98e-9868-4a29-84c5-0ff9cb31fdf1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}