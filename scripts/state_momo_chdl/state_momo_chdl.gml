live_call();
if (momomode == 1)
{
    if(state_new == 1)
    {
        with(hurtbox0)
    	{
    	    xoffset = -29;
    	    yoffset = -36;
    	    image_xscale  = 58;
    	    image_yscale  = 68;
    	    xshift  = -1;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -15;
    	    yoffset = -61;
    	    image_xscale  = 30;
    	    image_yscale  = 25;
    	    xshift = -3;
    	}
    	
    	
    	with(collisionbox)
    	{
    	    xoffset = -20;
    	    yoffset = -22;
    	    image_xscale  = 40;
    	    image_yscale  = 54;
    	    xshift = 0;
    	}
    	

    	attackframe0 = 3;
    	endstate = 50;
    	
    	//animation
        sprite_index = spr_momo_chdl_fire;
    }
    
    if(!hitpausing() && !onground())
    {
    	vy = Approach(vy,vyMax,grav);
    }
    
    //attack
    if(state_timer == attackframe0)
    {
        hitbox0 = create_hitbox(5,-50,72,80,4.25,-21,15,7,12,12,-1,0,4,5,"mid",-1);
        attackhurtbox0 = create_hurtbox(8 * image_xscale, -17,22 * image_xscale,16,2,-2);
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 1;
        hitbox0.juggle = 1;
        hitbox0.airxtrahitstun = -3;
        hitbox0.airxtrayhit = 7;
        ex += 4;
    }
    if(state_timer == attackframe0 + 5)
    {
        heat += 20;
    }
    
    if(state_timer >= attackframe0 && state_timer < 8 && !hitpausing())
    {
        vy = -6;
    }
    
    if(state_timer > 8 && !hitpausing())
    {
        vx = 0;
    }
    
    if(onground() && state_timer > attackframe0)
    {
        if(sprite_index != spr_momo_crouch)
        {
            sprite_index = spr_momo_crouch
            endstate = state_timer + 4;
        }
	}
	
    attack_base(endstate,0,0,0,1,0);
}
else
{
    if(state_new == 1)
    {
        with(hurtbox0)
    	{
    	    xoffset = -29;
    	    yoffset = -36;
    	    image_xscale  = 58;
    	    image_yscale  = 68;
    	    xshift  = -1;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -15;
    	    yoffset = -61;
    	    image_xscale  = 30;
    	    image_yscale  = 25;
    	    xshift = -3;
    	}
    	
    	
    	with(collisionbox)
    	{
    	    xoffset = -20;
    	    yoffset = -22;
    	    image_xscale  = 40;
    	    image_yscale  = 54;
    	    xshift = 0;
    	}
    	

    	attackframe0 = 3;
    	endstate = 50;
    	
    	//animation
        sprite_index = spr_momo_chdl_ice;
    }
    
    if(!hitpausing() && !onground())
    {
    	vy = Approach(vy,vyMax,grav);
    }
    
    //attack
    if(state_timer == attackframe0)
    {
        hitbox0 = create_hitbox(5,-50,72,80,4.25,-19,15,7,12,12,-1,0,4,5,"mid",-1);
        attackhurtbox0 = create_hurtbox(8 * image_xscale, -17,22 * image_xscale,16,2,-2);
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 1;
        hitbox0.juggle = 1;
        hitbox0.airxtrahitstun = -3;
        hitbox0.airxtrayhit = 8;
        ex += 4;
    }
    if(state_timer == attackframe0 + 5)
    {
        heat += -20;
    }
    
    
    if(state_timer >= attackframe0 && state_timer < 8 && !hitpausing())
    {
        vy = -6;
        vx = 3 * image_xscale;
    }
    
    if(state_timer > 8 && !hitpausing())
    {
        vx = 0;
    }
    
    if(onground() && state_timer > attackframe0)
    {
        if(sprite_index != spr_momo_crouch)
        {
            sprite_index = spr_momo_crouch
            endstate = state_timer + 4;
        }
	}
	
    attack_base(endstate,0,0,0,1,0);
}