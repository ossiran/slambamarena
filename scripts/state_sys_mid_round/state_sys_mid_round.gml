if(state_new == 1)
{
    players_control = 2;
}

//position
var playerdistx = abs(player1.x - player2.x);
var playerdisty = abs(player1.y - player2.y);
var cameradistx = (player1.x + player2.x) /2;
var cameradisty = (player1.y + player2.y) /2;

//screen shake
var ranx1 = 0;
var rany1 = 0;

if(player1.hitpause > 10 || player2.hitpause > 10)
{
    ranx1 = irandom_range(-1,1);
    rany1 = irandom_range(-1,1);
}


if(state_timer % 60 == 0)
{
    clock --;
}

var came = view_camera[0];

if(cameradistx > 212 && cameradistx < room_width - 212)
{
    x = Approach(x,cameradistx,5) + ranx1;
}
else
{
    if(cameradistx < 212)
    {
        x = Approach(x,212,5) + ranx1;
    }
    if(cameradistx > room_width - 212)
    {
        x = Approach(x,room_width - 212,5) + ranx1;
    }
}


if(cameradisty < 650)
{
    y = Approach(y, 640, 2) + rany1;
}
else if(cameradisty < 698)
{
    y = Approach(y, 685, 2) + rany1;
}
else
{
    y = Approach(y,699,2) + rany1;
}

if(player1.hp == 0 || player2.hp == 0)
{
    state_switch("end_round");
    
}