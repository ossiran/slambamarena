//
if(state_new == 1)
{
    with(hurtbox0)
	{
	    xoffset = -29;
	    yoffset = -36;
	    image_xscale  = 58;
	    image_yscale  = 68;
	    xshift  = -1;
	}
	
	with(hurtbox1)
	{
	    xoffset = -15;
	    yoffset = -61;
	    image_xscale  = 30;
	    image_yscale  = 25;
	    xshift = -3;
	}
	
	
	with(collisionbox)
	{
	    xoffset = -20;
	    yoffset = -22;
	    image_xscale  = 40;
	    image_yscale  = 54;
	    xshift = 0;
	}
    	
    	
    attackframe0 =  5;
    endstate = 15;
    
    sprite_index = spr_momo_crouch;
    momomode = -momomode;
}

if(!onground() && vwxas == 0 && vywas == 0)
{
    vxwas = vx;
    vywas = vy;
    vx = 0;
    vy = 0;
}

if(state_timer == endstate - 1 && vxwas != 0 && vywas != 0)
{
    vx = vxwas;
    vy = vywas;
}
attack_base(endstate,onground(),1,0,1,0);