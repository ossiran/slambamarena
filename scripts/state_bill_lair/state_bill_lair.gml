if(state_new == 1)
{
	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -43;
	    image_xscale  = 26;
	    image_yscale  = 55;
	    xshift  = -2;
	}
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 16;
	    image_yscale  = 37;
	    xshift = -2
	}
	chainmove = 0;
	attackframe0 = 3;
	endstate = 25;
}

//animation
sprite_index = spr_bill_lair;

var hitfalladd = 0;

//juggling correctly
with(otherplayer)
{
    if(!onground() || vy < 0)
    {
        hitfalladd = 1;
    }
}

//attack
if(state_timer == attackframe0)
{
    hitbox1 = create_hitbox(6, -28,22, 23, 3,0, 10,10,8,8,0 - hitfalladd,0,15,3.5,"overhead",-1);
    attackhurtbox0 = create_hurtbox(6* image_xscale, -29,28 * image_xscale,22,15,-2);
    hitbox1.scaling = 1.5;
    hitbox1.hitsound = snd_hitsound1;
    hitbox1.juggle = 2;
}

//chainmove
if(state_timer > 3)
{
    if(input_check(0,0,1,0,0))
    {
        chainmove = "mair"
    }
        
    if(input_check(0,0,0,1,0))
    {
        chainmove = "hair"
    }
}

attack_base(endstate,1,0,1,1,0);

if(state_timer > 2)
{
    image_index = 1;
}

/*
//chainmove
if((hitting == 0 || otherplayer.hitpause > 0) && state_timer > 3)
{
    if(input_check(0,0,1,0,0))
    {
        chainmove = "mair";
    }
    
    if(input_check(0,0,0,1,0))
    {
        chainmove = "hair"
    }
}

//chain
if(hitting == 1 && otherplayer.hitpause == 0 && (input_check(0,0,1,0,0) || chainmove == "mair") && vxwas == 0 && vywas == 0)
{
    state_switch("mair");
}

if(hitting == 1 && otherplayer.hitpause == 0 && (input_check(0,0,0,1,0) || chainmove == "hair") && vxwas == 0 && vywas == 0)
{
    state_switch("hair");
}
*/