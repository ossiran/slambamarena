if(state_new == 1)
{
    players_control = 1;
    roundstartpos1 = player1.x;
    roundstartpos2 = player2.x
    clock = 90;
}
var came = view_camera[0];

//position
var playerdistx = abs(player1.x - player2.x);
var playerdisty = abs(player1.y - player2.y);
var cameradistx = (player1.x + player2.x) /2;
var cameradisty = (player1.y + player2.y) /2;


var ranx1 = 0;
var rany1 = 0;

if(player1.hitpause > 13 || player2.hitpause > 13)
{
    var ranx1 = irandom_range(-1,1);
    var rany1 = irandom_range(-1,1);
}


var came = view_camera[0];

if(cameradistx > 212 && cameradistx < room_width - 212)
{
    x = Approach(x,cameradistx,5) + ranx1;
}
else
{
    if(cameradistx < 212)
    {
        x = Approach(x,212,5) + ranx1;
    }
    if(cameradistx > room_width - 212)
    {
        x = Approach(x,room_width - 212,5) + ranx1;
    }
}


if(cameradisty < 650)
{
    y = Approach(y, 640, 2) + rany1;
}
else if(cameradisty < 698)
{
    y = Approach(y, 685, 2) + rany1;
}
else
{
    y = Approach(y,699,2) + rany1;
}

/*
if(playerdistx > 280 && playerdistx < 520)
{
    camera_set_view_size(came,Approach(camera_get_view_width(came),(playerdistx+140),16),Approach(camera_get_view_height(came),(camera_get_view_width(came)* 0.5625),9))
}
*/

camera_set_view_size(came,320,180);

//refilling player hp
player1.hp = Approach(player1.hp, player1.hpmax, player1.hpmax/90);
player2.hp = Approach(player2.hp, player2.hpmax, player2.hpmax/90);

/*
//moving back to starting positions
if(player1.rounds > 0 || player2.rounds > 0)
{
    player1.x = Approach(player1.x,300,abs(300 - roundstartpos1)/70);
    
    
    player2.x = Approach(player2.x,500,abs(500 - roundstartpos2)/70);
    
    if(state_timer < 35)
    {
        player1.y = Approach(player1.y,700, 7);
        player2.y = Approach(player2.y,700, 7);
    }
    else
    {
        player1.y = Approach(player1.y,737, abs(player1.y - 700)/35 + player1.grav);
        player2.y = Approach(player2.y,737, abs(player2.y - 700)/35 + player1.grav);
    }
}
*/

//leaving state
if(state_timer == 100)
{
    state_switch("mid_round");
}