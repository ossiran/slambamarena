if(state_new == 1)
{
	audio_play_sound(snd_bill_airdash,1,0);	
}	

if(dashdir = 1)
{

    if(image_xscale = 1)
    {
        vx = 7;
        sprite_index = spr_bill_fairdash;
    }
    else
    {
        vx = 7;
        sprite_index = spr_bill_bairdash;
    }
}

if(dashdir = -1)
{

    if(image_xscale = 1)
    {
        vx = -7;
        sprite_index = spr_bill_bairdash;
    }
    else
    {
        vx = -7;
        sprite_index = spr_bill_fairdash;
    }
}
vy = 0;

if(state_timer > 3)
{
    //double jump	
    if(tap(7) && doublejump == 1)
    {
    		doublejump --;
    		vy = -jumpheight * 0.85;
    		vx = -4 * image_xscale;
    		audio_play_sound(snd_bill_rocketjump,1,0);
    }
    
    //double jump	
    if(tap(8) && doublejump == 1)
    {
    		doublejump --;
    		vx = 0;
    		vy = -jumpheight;
    		audio_play_sound(snd_bill_rocketjump,1,0);
    }
    
    //double jump	
    if(tap(9) && doublejump == 1)
    {
    		doublejump --;
    		vy = -jumpheight * 0.85;
    		vx = 4 * image_xscale;
    		audio_play_sound(snd_bill_rocketjump,1,0);
    }
    
    //lair
    if(input_check(0,1,0,0,0))
    {
        state_switch("lair");
    }
    
    
    //lair
    if(input_check(0,0,1,0,0))
    {
        state_switch("mair");
    }
    
    //hair
    if(input_check(0,0,0,1,0))
    {
        state_switch("hair");
    }
    
    //qcb
    if(qcb_buffer > 0)
    {
        if(input_check(0,0,0,1,0))
        {
            state_switch("aqcbh");
        }
        else if(input_check(0,0,1,0,0))
        {
            state_switch("aqcbm");
        }
        else if(input_check(0,1,0,0,0))
        {
            state_switch("aqcbl");
        }
    }
    
    //g moves
    if(input_check(0,0,0,0,1))
    {
        state_switch("ag");
    }
    
    if(state_name != "airdash")
    {
        vx = vx/1.5;
        dashdir = 0;
    }
}
if(state_timer = 11)
{
    vx = vx/2;
    state_switch("air");
    dashdir = 0;
}