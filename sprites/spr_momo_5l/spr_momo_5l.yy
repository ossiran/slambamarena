{
    "id": "0369c88d-344c-4dfc-a0ba-1380ce90c1e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_5l",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 16,
    "bbox_right": 137,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a93d92aa-8cf1-4f85-9efa-0c82645bd47a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0369c88d-344c-4dfc-a0ba-1380ce90c1e6",
            "compositeImage": {
                "id": "9390a9d0-4598-474e-bd4d-241a244ba592",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a93d92aa-8cf1-4f85-9efa-0c82645bd47a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aef59dc9-a337-449c-9780-8329af35bcb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a93d92aa-8cf1-4f85-9efa-0c82645bd47a",
                    "LayerId": "6147a4ad-880a-44af-84aa-b2c50c5c6658"
                }
            ]
        },
        {
            "id": "782ea5c1-470f-46be-b107-759ffb6f25e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0369c88d-344c-4dfc-a0ba-1380ce90c1e6",
            "compositeImage": {
                "id": "b689c1a1-8f34-497f-92ef-da53307ccc16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "782ea5c1-470f-46be-b107-759ffb6f25e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab46991e-5654-47c4-96b7-1106e9e9d905",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "782ea5c1-470f-46be-b107-759ffb6f25e4",
                    "LayerId": "6147a4ad-880a-44af-84aa-b2c50c5c6658"
                }
            ]
        },
        {
            "id": "3184fdd0-2070-4fd3-95e5-812f52722866",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0369c88d-344c-4dfc-a0ba-1380ce90c1e6",
            "compositeImage": {
                "id": "a518c21e-48ac-4417-b042-1e491ecb8245",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3184fdd0-2070-4fd3-95e5-812f52722866",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1a3cbc3-3e03-4ee4-a730-68648ab763b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3184fdd0-2070-4fd3-95e5-812f52722866",
                    "LayerId": "6147a4ad-880a-44af-84aa-b2c50c5c6658"
                }
            ]
        },
        {
            "id": "5b7fc0f1-e390-40f4-924f-845d85bc8f73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0369c88d-344c-4dfc-a0ba-1380ce90c1e6",
            "compositeImage": {
                "id": "13e344f9-b608-4a68-a81d-d7f4b518da2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b7fc0f1-e390-40f4-924f-845d85bc8f73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f6094ed-7d4d-4d4f-b3c1-604fef8138b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b7fc0f1-e390-40f4-924f-845d85bc8f73",
                    "LayerId": "6147a4ad-880a-44af-84aa-b2c50c5c6658"
                }
            ]
        },
        {
            "id": "2672fbb9-4e3c-4a43-88e1-f0ea5b3f0574",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0369c88d-344c-4dfc-a0ba-1380ce90c1e6",
            "compositeImage": {
                "id": "c0e929ed-432d-4ad7-b724-8fa39b480683",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2672fbb9-4e3c-4a43-88e1-f0ea5b3f0574",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14000487-64d4-4bd9-82ab-2e0ff71d5638",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2672fbb9-4e3c-4a43-88e1-f0ea5b3f0574",
                    "LayerId": "6147a4ad-880a-44af-84aa-b2c50c5c6658"
                }
            ]
        },
        {
            "id": "2ebddbe6-a17e-4757-b04d-d426b5878896",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0369c88d-344c-4dfc-a0ba-1380ce90c1e6",
            "compositeImage": {
                "id": "75cb7f0d-96ff-45a2-80b4-1ab147241d5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ebddbe6-a17e-4757-b04d-d426b5878896",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec5f4ae2-d476-47be-9567-dd0b1fc5ae71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ebddbe6-a17e-4757-b04d-d426b5878896",
                    "LayerId": "6147a4ad-880a-44af-84aa-b2c50c5c6658"
                }
            ]
        },
        {
            "id": "751190f5-b06f-4a29-a50d-fc5042652788",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0369c88d-344c-4dfc-a0ba-1380ce90c1e6",
            "compositeImage": {
                "id": "0537b62d-c024-4174-baad-93ad31b21668",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "751190f5-b06f-4a29-a50d-fc5042652788",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18d628db-c790-4b01-b1b4-bf20a950ffba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "751190f5-b06f-4a29-a50d-fc5042652788",
                    "LayerId": "6147a4ad-880a-44af-84aa-b2c50c5c6658"
                }
            ]
        },
        {
            "id": "de347c1c-df6d-45d8-bd27-bae8794ff6cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0369c88d-344c-4dfc-a0ba-1380ce90c1e6",
            "compositeImage": {
                "id": "76cc9186-0b13-4c67-b4ea-68c8562f5812",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de347c1c-df6d-45d8-bd27-bae8794ff6cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2b92927-2657-4e62-8c6f-b7f5e74f82c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de347c1c-df6d-45d8-bd27-bae8794ff6cb",
                    "LayerId": "6147a4ad-880a-44af-84aa-b2c50c5c6658"
                }
            ]
        },
        {
            "id": "bdd9cb7b-1ffb-4f8a-8c01-3f5aa611a88e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0369c88d-344c-4dfc-a0ba-1380ce90c1e6",
            "compositeImage": {
                "id": "10774d83-8511-464b-854a-1c7a1cb6c08a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdd9cb7b-1ffb-4f8a-8c01-3f5aa611a88e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cb11e9d-5d00-4e0a-9e35-9f3aad4e20e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdd9cb7b-1ffb-4f8a-8c01-3f5aa611a88e",
                    "LayerId": "6147a4ad-880a-44af-84aa-b2c50c5c6658"
                }
            ]
        },
        {
            "id": "e6692567-87fc-4a49-9aed-ef622dbf8bc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0369c88d-344c-4dfc-a0ba-1380ce90c1e6",
            "compositeImage": {
                "id": "f11f7c03-0968-4681-8245-6f3eff18ef33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6692567-87fc-4a49-9aed-ef622dbf8bc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "303b5d0c-a3a7-4829-ba0b-1635f499ba5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6692567-87fc-4a49-9aed-ef622dbf8bc5",
                    "LayerId": "6147a4ad-880a-44af-84aa-b2c50c5c6658"
                }
            ]
        },
        {
            "id": "92fd3feb-180c-4834-b3d0-b3bbb81a6c2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0369c88d-344c-4dfc-a0ba-1380ce90c1e6",
            "compositeImage": {
                "id": "adbaf813-2598-43a2-8ec1-71b4f0c0113a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92fd3feb-180c-4834-b3d0-b3bbb81a6c2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ecdb4b1-6ea7-4ec1-b2e4-4dc816f7e924",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92fd3feb-180c-4834-b3d0-b3bbb81a6c2c",
                    "LayerId": "6147a4ad-880a-44af-84aa-b2c50c5c6658"
                }
            ]
        },
        {
            "id": "f83a4a16-55bd-4348-8d16-680206d6a429",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0369c88d-344c-4dfc-a0ba-1380ce90c1e6",
            "compositeImage": {
                "id": "4e783929-8058-4147-9233-1c42f88ca217",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f83a4a16-55bd-4348-8d16-680206d6a429",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ccbd02b-49d5-40bc-b095-c1975df42af2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f83a4a16-55bd-4348-8d16-680206d6a429",
                    "LayerId": "6147a4ad-880a-44af-84aa-b2c50c5c6658"
                }
            ]
        },
        {
            "id": "b682220c-dcec-4157-b17f-90dda779dc0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0369c88d-344c-4dfc-a0ba-1380ce90c1e6",
            "compositeImage": {
                "id": "4adcad4b-1237-4ebb-9ed7-05e51b2ca7e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b682220c-dcec-4157-b17f-90dda779dc0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1522d927-b271-4d83-9103-47c92a7d85ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b682220c-dcec-4157-b17f-90dda779dc0d",
                    "LayerId": "6147a4ad-880a-44af-84aa-b2c50c5c6658"
                }
            ]
        },
        {
            "id": "0b205880-fba6-4888-88d3-26849c8124db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0369c88d-344c-4dfc-a0ba-1380ce90c1e6",
            "compositeImage": {
                "id": "ae24560c-5ab6-4c46-83dd-d463bfb223ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b205880-fba6-4888-88d3-26849c8124db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5944689d-8cd2-424f-a207-71af3b2f1069",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b205880-fba6-4888-88d3-26849c8124db",
                    "LayerId": "6147a4ad-880a-44af-84aa-b2c50c5c6658"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "6147a4ad-880a-44af-84aa-b2c50c5c6658",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0369c88d-344c-4dfc-a0ba-1380ce90c1e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 63,
    "yorig": 87
}