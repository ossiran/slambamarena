if(state_new == 1)
{
	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -42;
	    xscale  = 26;
	    yscale  = 74;
	    xshift  = -2;
	}
	
			with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 16;
	    image_yscale  = 47;
	    xshift = -2
	}
	attackframe0 =  5;
	attackframe1 =  18;
	attackframe2 =  23;
	endstate = 37;
}


if(state_timer > attackframe0 && state_timer < attackframe1 && hitpause == 0)
{
    vx = 5.5 * image_xscale;
}
else
{
    vx = Approach(vx,0,5);
}

if(state_timer == 9)
{
    vy = -5;
}

if(state_timer == 16)
{
    vy = 9;
}

if(state_timer == attackframe2 - 3)
{
    hitbox1 = create_hitbox(-4, -20,50, 50, 2, 0, 14,18,5,10,-1,0,0,3,"overhead",-1);
	hitbox1.hitsound = snd_hit;
	ex += 4;
	hitbox1.scaling = 1;
	hitbox1.counteryhit = -5;
	hitbox1.airxtrayhit = -1;
}


if(state_timer == attackframe2)
{
    hitbox1 = create_hitbox(-4, -20,50, 50, 2, 10, 14,17,12,9,-1,1,0,3,"overhead",-1);
	hitbox1.hitsound = snd_hit;
	hitbox1.scaling = 0.5;
}

sprite_index = spr_bill_qcbm;

attack_base(endstate,0,0,0,1,0);