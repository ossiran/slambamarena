if(state_new == 1)
{
    dtap_right = 0;
    dtap_left = 0;
    light_buffer = 0;
    medium_buffer = 0;
	sprite_index = spr_bill_bdash;
	image_index = 1;
}

if(image_xscale == -1 && state_timer > 2 && state_timer < 10)
{
    vx = Approach(vx, 5, 3);
}

if(image_xscale == 1  && state_timer > 2 && state_timer < 10)
{
    vx = Approach(vx, -5, 3);
}

if(state_timer > 11)
{
     vx = Approach(vx, 0, 3);
}

//air
if(!onground())
{
	state_switch("air");	
}

if(state_timer == 18)
{
    state_switch("stand");
}