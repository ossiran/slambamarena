{
    "id": "a7e96491-2ad1-4a07-8292-69622c92e931",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dsyn_qcfm",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 68,
    "bbox_right": 169,
    "bbox_top": 43,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ce483297-5474-4b88-9339-9cc181db722d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "a10eb210-bede-456e-9fb3-27adf73b7dfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce483297-5474-4b88-9339-9cc181db722d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f4c8451-b744-4820-81e9-f9c31c04cf02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce483297-5474-4b88-9339-9cc181db722d",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "94ebd587-8bdb-42a2-aa94-98bb8d230eeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "1ddea274-eccd-4a4e-826c-2823a9200a02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94ebd587-8bdb-42a2-aa94-98bb8d230eeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5cdc8c2-c920-4943-85df-89d6cafa8173",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94ebd587-8bdb-42a2-aa94-98bb8d230eeb",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "e9a06407-afc0-499b-a50b-4b1295820e32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "c3ce7c56-501c-4da1-9af8-c03bfe1a5bd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9a06407-afc0-499b-a50b-4b1295820e32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8320bca6-6347-491e-903c-2edef15e27f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9a06407-afc0-499b-a50b-4b1295820e32",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "116f6cf9-244c-4f9f-8c5b-3dd346e70641",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "844935e9-7264-48f2-84e3-b099d4a39873",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "116f6cf9-244c-4f9f-8c5b-3dd346e70641",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20f1f287-2e02-4ae4-bf4c-56c205875099",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "116f6cf9-244c-4f9f-8c5b-3dd346e70641",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "276889b2-d338-4a89-9847-a2d298ce85a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "272ee073-4635-4f95-b869-9bcf4c73450a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "276889b2-d338-4a89-9847-a2d298ce85a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca6ef6be-edd8-4f7a-8994-ae4150898a80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "276889b2-d338-4a89-9847-a2d298ce85a6",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "389e3501-f475-486e-b43c-fc14d11d4790",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "3459740e-89f5-4fbb-8192-9dec8977e9ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "389e3501-f475-486e-b43c-fc14d11d4790",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa71ce1f-5afd-49fa-8636-7f93664bd115",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "389e3501-f475-486e-b43c-fc14d11d4790",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "70e3ab43-6a4b-4be2-abdb-9a918f15e8d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "c1ba7224-c3a0-489c-ae26-1e34f62b6c24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70e3ab43-6a4b-4be2-abdb-9a918f15e8d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20a1da9c-3d69-46ee-a459-1d58c40f5b2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70e3ab43-6a4b-4be2-abdb-9a918f15e8d0",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "a855f4d3-aac4-448c-8f00-2e37abe2b67a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "c9a4e52a-d551-4f7a-baa2-0796529c3691",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a855f4d3-aac4-448c-8f00-2e37abe2b67a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7e6c090-d1f9-4405-8e5d-5a71eeff6333",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a855f4d3-aac4-448c-8f00-2e37abe2b67a",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "7554c397-8184-44c4-bb3b-f48e09a229f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "68385b5d-e921-4378-bee9-4f2a282d872e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7554c397-8184-44c4-bb3b-f48e09a229f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f93dd99-02d8-4fbd-b5f2-409ac0d78458",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7554c397-8184-44c4-bb3b-f48e09a229f0",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "60c3ef25-b1c7-4015-a7d0-19a1c96ac7f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "923fa31d-e236-4a69-9197-c490632a9b2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60c3ef25-b1c7-4015-a7d0-19a1c96ac7f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bef20ba0-528e-47dc-b047-26a8f7b2207b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60c3ef25-b1c7-4015-a7d0-19a1c96ac7f2",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "11c52f75-e701-464c-84a1-f65631445b57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "8baaf801-b55d-4db3-adf0-654cbe24116e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11c52f75-e701-464c-84a1-f65631445b57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1de6c132-06bc-40c9-bc2a-cf851e261945",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11c52f75-e701-464c-84a1-f65631445b57",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "b7d80955-a71e-4d43-91a3-07151f58f241",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "c9b2df35-a38d-4ee8-a8ac-f4fdf6a766b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7d80955-a71e-4d43-91a3-07151f58f241",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11d2895b-8347-4034-bdb7-efad43818e6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7d80955-a71e-4d43-91a3-07151f58f241",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "0dca1a4a-fcab-41a1-ad21-c7b1912532de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "eeeb90f0-cbd6-4f55-8177-bcab9d279aad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dca1a4a-fcab-41a1-ad21-c7b1912532de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6d53df7-3553-4eef-897a-891132bf183f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dca1a4a-fcab-41a1-ad21-c7b1912532de",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "03789301-75ce-47d8-a470-3a52fc473f59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "d9dbc8b1-cb93-4610-bcac-aba80d5c77d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03789301-75ce-47d8-a470-3a52fc473f59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc95a515-22a6-4366-956d-dd63d2c92b24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03789301-75ce-47d8-a470-3a52fc473f59",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "a99014d9-c2de-4bcb-ba13-25d0c6811da6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "1aa4fea1-ea63-4a65-a12b-35e0ded1a264",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a99014d9-c2de-4bcb-ba13-25d0c6811da6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d659f3f-0dce-48c3-81a5-46abc6aef00b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a99014d9-c2de-4bcb-ba13-25d0c6811da6",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "62700c51-71e3-4744-90a8-393f395d7da5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "5e9789a0-2675-4754-8e20-1c3b86fed187",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62700c51-71e3-4744-90a8-393f395d7da5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "944a611e-7380-4dca-b546-9a1c137fa072",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62700c51-71e3-4744-90a8-393f395d7da5",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "ae254407-68ab-4c9e-9377-5f84e2ae0c85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "9b80b4d8-a0c6-4cd2-8aef-7e102df784f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae254407-68ab-4c9e-9377-5f84e2ae0c85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "761bf0ac-92bc-49f3-bf30-f19672d44482",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae254407-68ab-4c9e-9377-5f84e2ae0c85",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "1edcabbb-c1b1-4bbe-b0b6-a71b7613714c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "4d64e0f9-9c4e-4e76-afac-fb8344054b3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1edcabbb-c1b1-4bbe-b0b6-a71b7613714c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff624bf1-ee28-46e4-8c9d-69a6a5e5f07b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1edcabbb-c1b1-4bbe-b0b6-a71b7613714c",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "9b619762-d74b-4e9f-8aac-6f3dbd1438fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "31e85360-0159-4b3e-ba9e-42e035b8ec1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b619762-d74b-4e9f-8aac-6f3dbd1438fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aef0fd1a-c99b-44e2-9cbe-ab681e5220ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b619762-d74b-4e9f-8aac-6f3dbd1438fa",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "2806c14e-cf31-4f11-b642-0cfb071dae5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "f3ae7ac5-fc3a-4ee0-b2b9-df94c8c5f572",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2806c14e-cf31-4f11-b642-0cfb071dae5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e4bf0f4-6d02-44ea-b20a-78e86a08f9d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2806c14e-cf31-4f11-b642-0cfb071dae5f",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "5b86153d-ceee-4139-9f2b-8f8750ab0166",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "beb4b208-af65-4962-8f62-000ccfbd5720",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b86153d-ceee-4139-9f2b-8f8750ab0166",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51670371-2914-4998-bc0d-2f7603d53eb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b86153d-ceee-4139-9f2b-8f8750ab0166",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "c3e273c4-de01-43c9-89f2-a3088538a4c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "7db2bb7a-fd01-4cb0-acaa-cee328d3a343",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3e273c4-de01-43c9-89f2-a3088538a4c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c39e5ae-e897-4505-a469-0ff6bc35c535",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3e273c4-de01-43c9-89f2-a3088538a4c2",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "ec7c8070-a250-49c4-9da8-beb9e291bc34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "b3512e66-f815-490f-b70b-8d10f176ebc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec7c8070-a250-49c4-9da8-beb9e291bc34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a5a1c47-8372-4fe2-bd30-b61d5f484ec9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec7c8070-a250-49c4-9da8-beb9e291bc34",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "e11d6e34-8204-40c6-8bca-09b3124cb19c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "05857a62-5d67-4c08-b3ad-8e703a51d106",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e11d6e34-8204-40c6-8bca-09b3124cb19c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95126144-c78b-4ec1-b519-dd66888cef23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e11d6e34-8204-40c6-8bca-09b3124cb19c",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "0a8fbcf2-1bbd-4dbe-8bd9-39afc4d5b4a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "966d5a04-c851-4518-a94c-0b7ab0bcbee2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a8fbcf2-1bbd-4dbe-8bd9-39afc4d5b4a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ababae37-a8ce-4ab2-8a84-e6b630290f50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a8fbcf2-1bbd-4dbe-8bd9-39afc4d5b4a4",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "34fad076-bf65-46cd-923d-4e8fe1c1ac15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "fec0d99a-1faf-4397-902b-0fa2c6b9e7f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34fad076-bf65-46cd-923d-4e8fe1c1ac15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ab2099a-96a0-4589-b6f4-3883656195a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34fad076-bf65-46cd-923d-4e8fe1c1ac15",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "923a19ef-0081-497b-930a-ae5b50bd030a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "7ce1aa55-1db8-46ed-82b0-ad5552d6feca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "923a19ef-0081-497b-930a-ae5b50bd030a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76f9a38a-1341-470e-aa52-108d71cd974f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "923a19ef-0081-497b-930a-ae5b50bd030a",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "fa042fef-74e4-4528-ac55-96ad79685431",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "e3a15083-d010-48c5-9c49-51ee9f410074",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa042fef-74e4-4528-ac55-96ad79685431",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65d5c395-144d-485c-99ef-988f9b4209f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa042fef-74e4-4528-ac55-96ad79685431",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "bc2873e5-a246-4663-9870-daaa7f22e20b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "08cfbf84-f8a0-42f2-9f08-01b6a40d8c45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc2873e5-a246-4663-9870-daaa7f22e20b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f23a4354-e6a9-46bc-9ec2-5bb211919c45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc2873e5-a246-4663-9870-daaa7f22e20b",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "dadf69a8-0273-47c0-973b-48c19620546f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "2cc65589-26cc-46af-b81d-fc67dd3571ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dadf69a8-0273-47c0-973b-48c19620546f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e97dbc8-bd65-4e4a-a9aa-741c4cffa4af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dadf69a8-0273-47c0-973b-48c19620546f",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "7fb9c054-8a11-40c2-9d5a-1b23b99db553",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "fb9dcd7e-3e7f-430f-9564-05a6cf201381",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fb9c054-8a11-40c2-9d5a-1b23b99db553",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85b2adb6-623e-4da3-965a-1305e0f3ce7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fb9c054-8a11-40c2-9d5a-1b23b99db553",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "f0f9b6b8-f7bc-46ba-a12c-85d68664c084",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "382e9f07-b383-488a-af64-8f9388bc94b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0f9b6b8-f7bc-46ba-a12c-85d68664c084",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa50951c-b270-4210-8ac2-73925b93deb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0f9b6b8-f7bc-46ba-a12c-85d68664c084",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "2384feda-bf64-4a80-a773-621bcb84473a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "6d4bdfc7-9adb-4500-9e7f-acb15146cfd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2384feda-bf64-4a80-a773-621bcb84473a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a324d1a3-ef58-4604-8c0e-8814404bf27f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2384feda-bf64-4a80-a773-621bcb84473a",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "3c980fb1-14f0-49fa-99ed-8160c38301d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "8040ec3c-b459-4ad7-bd7a-d4f7c2aa3980",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c980fb1-14f0-49fa-99ed-8160c38301d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd793c55-8582-407e-bf19-4c8ccd439c5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c980fb1-14f0-49fa-99ed-8160c38301d7",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "19f76182-c96b-42ef-b91f-e5a5cadf6a7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "1c91a60b-4eda-4d87-9c34-92d6143c9802",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19f76182-c96b-42ef-b91f-e5a5cadf6a7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b58b1df8-d90d-40e6-bd9e-356a96850523",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19f76182-c96b-42ef-b91f-e5a5cadf6a7d",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "dad2064b-985a-4c11-a0a2-217d2fc2239d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "caa88e44-1662-4546-9e2a-105d28bf9092",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dad2064b-985a-4c11-a0a2-217d2fc2239d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3eaa2a95-98c1-4424-b49d-82aef44988c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dad2064b-985a-4c11-a0a2-217d2fc2239d",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "690e1abf-868b-43e7-ad3d-eb1bab29e598",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "c4c19edf-507a-40fe-86a6-2e9c86cd9d1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "690e1abf-868b-43e7-ad3d-eb1bab29e598",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a68fe679-551d-4e90-bca3-6bc219e549e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "690e1abf-868b-43e7-ad3d-eb1bab29e598",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "dd49f755-d61c-4a15-8fa9-6237cc746542",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "ca487c30-6e84-4907-988d-f6fab2f00812",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd49f755-d61c-4a15-8fa9-6237cc746542",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d500a412-789d-4ce8-87e4-01ca24abab68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd49f755-d61c-4a15-8fa9-6237cc746542",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "922fe498-ab46-4239-ae14-43c47a9f64a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "09285ba8-bc02-4933-b9ae-f7160d2eae80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "922fe498-ab46-4239-ae14-43c47a9f64a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04ad1e84-1286-4845-9ad0-7428b94fcaf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "922fe498-ab46-4239-ae14-43c47a9f64a8",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "27d5e506-c997-4571-a310-59b725088925",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "90894a03-9571-41b3-9fc6-429ce1a6c2f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27d5e506-c997-4571-a310-59b725088925",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96c990f0-c91d-41b8-ab51-bea3f1eb604d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27d5e506-c997-4571-a310-59b725088925",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "b26b9a82-5b25-4a83-a70b-cb8fe403e463",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "53482408-f6f0-48d3-9170-73403630083b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b26b9a82-5b25-4a83-a70b-cb8fe403e463",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7a734b8-7252-4f3a-aef5-be25f911d29b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b26b9a82-5b25-4a83-a70b-cb8fe403e463",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "622740bc-db36-47dc-8047-2092cab4ab8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "35da1738-d320-4ea7-9ce1-d75e286597bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "622740bc-db36-47dc-8047-2092cab4ab8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29b61993-b038-45c4-9c5a-a1245bb43876",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "622740bc-db36-47dc-8047-2092cab4ab8b",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "6f97e77d-2d8d-4e99-8e2b-d804e6332054",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "e021e589-7aac-430a-9961-87c57c6c974c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f97e77d-2d8d-4e99-8e2b-d804e6332054",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6fe964f-5c15-4767-b2e8-6fc74184c746",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f97e77d-2d8d-4e99-8e2b-d804e6332054",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "1c151769-e76d-47e1-a929-bb7c52e7968b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "09bd8ac6-c9c9-49a6-bfff-5693b58365c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c151769-e76d-47e1-a929-bb7c52e7968b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb4ee542-078c-4395-9724-a05faaa00e3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c151769-e76d-47e1-a929-bb7c52e7968b",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "14465286-6d00-4cd1-bbab-264dab8cb69c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "0c9b6319-d77e-4079-8cbd-ad82e5552462",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14465286-6d00-4cd1-bbab-264dab8cb69c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8774504f-fa2a-44a9-9995-7546f4423b48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14465286-6d00-4cd1-bbab-264dab8cb69c",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "815d954d-4355-4948-a4a5-b85a650e3b51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "acb2ed09-255a-4c81-8beb-0b13d802e862",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "815d954d-4355-4948-a4a5-b85a650e3b51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b44d576-e043-4b11-895d-d059dba0d843",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "815d954d-4355-4948-a4a5-b85a650e3b51",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "b4b6e30b-4502-4b3d-87bb-b1162b6c685f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "3fb3d1a3-ecd9-40f8-b157-6fef26553cc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4b6e30b-4502-4b3d-87bb-b1162b6c685f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b8c6ce4-7432-4071-a13d-99d0dd65d6ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4b6e30b-4502-4b3d-87bb-b1162b6c685f",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "07b7b782-729f-40a9-b722-d61d3716049e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "00c3bfc0-0f3c-4a27-a4f0-ebcc14856cd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07b7b782-729f-40a9-b722-d61d3716049e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "150e9a02-01e7-4d02-a812-04a6be288178",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07b7b782-729f-40a9-b722-d61d3716049e",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "109a7970-5e41-42ec-b09a-d041e39204a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "ab592d2f-2fbe-4bcf-80ea-956d2fdde8c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "109a7970-5e41-42ec-b09a-d041e39204a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6763b4e4-53b2-4439-b133-e6bf173e40b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "109a7970-5e41-42ec-b09a-d041e39204a6",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "a5c873a6-b9c0-47a6-8f41-231f512f285b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "7c3b4259-4865-4bac-9fa7-2f70c3b535ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5c873a6-b9c0-47a6-8f41-231f512f285b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4b71774-72b4-4df1-860d-48285a57b190",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5c873a6-b9c0-47a6-8f41-231f512f285b",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "935e33e8-c8fb-4926-8a6d-3b1f81588314",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "e45e8d80-36e2-4726-9f28-90f03c5937da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "935e33e8-c8fb-4926-8a6d-3b1f81588314",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08a4a46e-a4ee-4488-9798-af5f20d060dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "935e33e8-c8fb-4926-8a6d-3b1f81588314",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "32c78c0e-edaf-444d-a11b-d1bff15b2015",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "0873ade0-8d25-4304-aca3-fd2e263f48f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32c78c0e-edaf-444d-a11b-d1bff15b2015",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34b519c3-d769-46c3-906a-cdd2e9956375",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32c78c0e-edaf-444d-a11b-d1bff15b2015",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        },
        {
            "id": "3a30517e-62ab-402c-b328-d7e20acc9d17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "compositeImage": {
                "id": "c01159f5-e991-4112-9da7-958e01193d3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a30517e-62ab-402c-b328-d7e20acc9d17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9498ccf3-30c7-4410-a08c-b4209fc3062a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a30517e-62ab-402c-b328-d7e20acc9d17",
                    "LayerId": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "df4d1934-2c5a-45cd-9bcf-4482c17ac44d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a7e96491-2ad1-4a07-8292-69622c92e931",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 178,
    "xorig": 89,
    "yorig": 80
}