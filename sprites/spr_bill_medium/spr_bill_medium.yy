{
    "id": "a537bdd3-b3cd-48ab-8e0a-8e6530d48b58",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_medium",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 49,
    "bbox_right": 99,
    "bbox_top": 75,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6fabdf4c-f87e-4fbf-be58-0a5399269104",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a537bdd3-b3cd-48ab-8e0a-8e6530d48b58",
            "compositeImage": {
                "id": "defd28c3-6571-46db-9d18-e8b30a1f29dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fabdf4c-f87e-4fbf-be58-0a5399269104",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa1be024-527d-451b-8fb9-c417ac60b2c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fabdf4c-f87e-4fbf-be58-0a5399269104",
                    "LayerId": "3d816dd0-f0ab-46a8-ae3e-e1f7e4160d41"
                }
            ]
        },
        {
            "id": "d103b712-8477-45c0-a785-43f0c089a060",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a537bdd3-b3cd-48ab-8e0a-8e6530d48b58",
            "compositeImage": {
                "id": "c148fd4c-ca77-496f-a3c2-ddae8efef282",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d103b712-8477-45c0-a785-43f0c089a060",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b4a49d8-57cc-4918-b615-2c13ce7b6dd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d103b712-8477-45c0-a785-43f0c089a060",
                    "LayerId": "3d816dd0-f0ab-46a8-ae3e-e1f7e4160d41"
                }
            ]
        },
        {
            "id": "198785f1-b7db-4311-9cc2-c7b339446891",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a537bdd3-b3cd-48ab-8e0a-8e6530d48b58",
            "compositeImage": {
                "id": "b6ac5a82-b814-4617-b469-16770f7b4197",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "198785f1-b7db-4311-9cc2-c7b339446891",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cdf9bcc-2416-4936-95a0-04e399eefe20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "198785f1-b7db-4311-9cc2-c7b339446891",
                    "LayerId": "3d816dd0-f0ab-46a8-ae3e-e1f7e4160d41"
                }
            ]
        },
        {
            "id": "f48af3c6-2cb7-4d20-ae70-0a516ad8aa7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a537bdd3-b3cd-48ab-8e0a-8e6530d48b58",
            "compositeImage": {
                "id": "f572aa37-38c2-411f-87ce-9465ff623700",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f48af3c6-2cb7-4d20-ae70-0a516ad8aa7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fba1d74b-dee7-482f-920d-2dfb3abfe945",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f48af3c6-2cb7-4d20-ae70-0a516ad8aa7c",
                    "LayerId": "3d816dd0-f0ab-46a8-ae3e-e1f7e4160d41"
                }
            ]
        },
        {
            "id": "302e62d4-05af-4703-b7c9-e6d429b20487",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a537bdd3-b3cd-48ab-8e0a-8e6530d48b58",
            "compositeImage": {
                "id": "1215cda8-cb39-42cc-a52e-956399d9d788",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "302e62d4-05af-4703-b7c9-e6d429b20487",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91ee57dd-7c26-4c23-9281-f7eb6640bdd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "302e62d4-05af-4703-b7c9-e6d429b20487",
                    "LayerId": "3d816dd0-f0ab-46a8-ae3e-e1f7e4160d41"
                }
            ]
        },
        {
            "id": "e30bd253-16a2-44fd-b635-298cb3755b12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a537bdd3-b3cd-48ab-8e0a-8e6530d48b58",
            "compositeImage": {
                "id": "243cd7a2-ce3b-4a80-b5e8-cfcd8002f696",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e30bd253-16a2-44fd-b635-298cb3755b12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2580a8f5-1536-4e72-abd0-f30f5c847a5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e30bd253-16a2-44fd-b635-298cb3755b12",
                    "LayerId": "3d816dd0-f0ab-46a8-ae3e-e1f7e4160d41"
                }
            ]
        },
        {
            "id": "e9ffc772-9aa5-41b0-8833-7b7599db3a00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a537bdd3-b3cd-48ab-8e0a-8e6530d48b58",
            "compositeImage": {
                "id": "6962e3f3-7409-4055-9601-ba78d6164e35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9ffc772-9aa5-41b0-8833-7b7599db3a00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e6b8c17-7da5-44e0-8d1b-507a085cdd99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9ffc772-9aa5-41b0-8833-7b7599db3a00",
                    "LayerId": "3d816dd0-f0ab-46a8-ae3e-e1f7e4160d41"
                }
            ]
        },
        {
            "id": "3c8a7bfc-97a9-475a-84a7-b9197e7fd2d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a537bdd3-b3cd-48ab-8e0a-8e6530d48b58",
            "compositeImage": {
                "id": "9b3a92ef-fd7b-4be4-af17-ec2d293318b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c8a7bfc-97a9-475a-84a7-b9197e7fd2d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4309380-0e1f-4d40-bb49-4fe841079e13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c8a7bfc-97a9-475a-84a7-b9197e7fd2d6",
                    "LayerId": "3d816dd0-f0ab-46a8-ae3e-e1f7e4160d41"
                }
            ]
        },
        {
            "id": "0820ed6b-e69c-41b0-a384-21b049782ad2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a537bdd3-b3cd-48ab-8e0a-8e6530d48b58",
            "compositeImage": {
                "id": "0d1ce4f4-7c84-46a1-bceb-fd8a6346cf02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0820ed6b-e69c-41b0-a384-21b049782ad2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29efb6aa-8c8f-4373-823a-f749ce153b6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0820ed6b-e69c-41b0-a384-21b049782ad2",
                    "LayerId": "3d816dd0-f0ab-46a8-ae3e-e1f7e4160d41"
                }
            ]
        },
        {
            "id": "a07ee68f-9b3d-4dfe-94c1-6bc9b743b185",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a537bdd3-b3cd-48ab-8e0a-8e6530d48b58",
            "compositeImage": {
                "id": "9e041d77-573d-44ef-8014-b158fead72c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a07ee68f-9b3d-4dfe-94c1-6bc9b743b185",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a754b1f5-83a9-474c-a644-6bfd3753d361",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a07ee68f-9b3d-4dfe-94c1-6bc9b743b185",
                    "LayerId": "3d816dd0-f0ab-46a8-ae3e-e1f7e4160d41"
                }
            ]
        },
        {
            "id": "212dfa81-358a-403e-a41f-914b5ef6bbab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a537bdd3-b3cd-48ab-8e0a-8e6530d48b58",
            "compositeImage": {
                "id": "d6e3ef79-978c-4ee7-8211-82d6e996e324",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "212dfa81-358a-403e-a41f-914b5ef6bbab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5908288-897e-467b-bfbb-2bddcf7dc869",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "212dfa81-358a-403e-a41f-914b5ef6bbab",
                    "LayerId": "3d816dd0-f0ab-46a8-ae3e-e1f7e4160d41"
                }
            ]
        },
        {
            "id": "ee064d77-bc86-4692-936c-47e3f8dea84f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a537bdd3-b3cd-48ab-8e0a-8e6530d48b58",
            "compositeImage": {
                "id": "b521a003-361e-4ecf-9220-07e4ca06ec44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee064d77-bc86-4692-936c-47e3f8dea84f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "604ed4ae-af8d-4409-a74a-bc747e7d64c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee064d77-bc86-4692-936c-47e3f8dea84f",
                    "LayerId": "3d816dd0-f0ab-46a8-ae3e-e1f7e4160d41"
                }
            ]
        },
        {
            "id": "70227e6c-afb3-45e5-a062-abfe066fb8b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a537bdd3-b3cd-48ab-8e0a-8e6530d48b58",
            "compositeImage": {
                "id": "2e05527c-e56b-48be-ae55-d596403dd3d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70227e6c-afb3-45e5-a062-abfe066fb8b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ac5a18f-a18a-4831-9430-1e14e8c08516",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70227e6c-afb3-45e5-a062-abfe066fb8b3",
                    "LayerId": "3d816dd0-f0ab-46a8-ae3e-e1f7e4160d41"
                }
            ]
        },
        {
            "id": "2f839a3c-6127-4e7f-a962-60d13cb67367",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a537bdd3-b3cd-48ab-8e0a-8e6530d48b58",
            "compositeImage": {
                "id": "df2ab83e-a82a-48a1-872f-2a8d9da70f32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f839a3c-6127-4e7f-a962-60d13cb67367",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0df56089-6060-46b5-963e-14e3e51b1536",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f839a3c-6127-4e7f-a962-60d13cb67367",
                    "LayerId": "3d816dd0-f0ab-46a8-ae3e-e1f7e4160d41"
                }
            ]
        },
        {
            "id": "f1108833-779d-47d4-b067-9b8754c376a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a537bdd3-b3cd-48ab-8e0a-8e6530d48b58",
            "compositeImage": {
                "id": "5cad6202-654a-4c6f-a12f-e79b48a1a26e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1108833-779d-47d4-b067-9b8754c376a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e972a41a-fd6c-473f-8555-74eb370aeed4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1108833-779d-47d4-b067-9b8754c376a3",
                    "LayerId": "3d816dd0-f0ab-46a8-ae3e-e1f7e4160d41"
                }
            ]
        },
        {
            "id": "fedc6f46-6b74-4634-a509-388ec47b7160",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a537bdd3-b3cd-48ab-8e0a-8e6530d48b58",
            "compositeImage": {
                "id": "1cc9f0a9-05f0-429a-832f-d0c5b28847a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fedc6f46-6b74-4634-a509-388ec47b7160",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1680ab5f-9991-45da-801f-8f36e53ad5d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fedc6f46-6b74-4634-a509-388ec47b7160",
                    "LayerId": "3d816dd0-f0ab-46a8-ae3e-e1f7e4160d41"
                }
            ]
        },
        {
            "id": "4878ddd5-84b8-46d6-8a42-f67b51221d90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a537bdd3-b3cd-48ab-8e0a-8e6530d48b58",
            "compositeImage": {
                "id": "018a32a8-dad2-4d67-8d78-d1ec6ecbaa6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4878ddd5-84b8-46d6-8a42-f67b51221d90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0800c3ff-5a80-4870-946e-0dafb0d8ea1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4878ddd5-84b8-46d6-8a42-f67b51221d90",
                    "LayerId": "3d816dd0-f0ab-46a8-ae3e-e1f7e4160d41"
                }
            ]
        },
        {
            "id": "38e9515c-c91e-4e0b-a963-155ed37d5e57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a537bdd3-b3cd-48ab-8e0a-8e6530d48b58",
            "compositeImage": {
                "id": "7c69f0e6-1432-4ff0-949d-7aaa3c54a06b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38e9515c-c91e-4e0b-a963-155ed37d5e57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "beb59fcd-86b0-403a-877d-5916cf4adf17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38e9515c-c91e-4e0b-a963-155ed37d5e57",
                    "LayerId": "3d816dd0-f0ab-46a8-ae3e-e1f7e4160d41"
                }
            ]
        },
        {
            "id": "4276a362-86ba-46ce-8a74-24b49b047931",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a537bdd3-b3cd-48ab-8e0a-8e6530d48b58",
            "compositeImage": {
                "id": "397d7230-73c5-4fbb-8fdd-cce9ba1f8026",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4276a362-86ba-46ce-8a74-24b49b047931",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2087372a-b80c-4677-8ebf-baa94da23706",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4276a362-86ba-46ce-8a74-24b49b047931",
                    "LayerId": "3d816dd0-f0ab-46a8-ae3e-e1f7e4160d41"
                }
            ]
        },
        {
            "id": "368f6f41-b6d6-4834-8b3b-502803eb9608",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a537bdd3-b3cd-48ab-8e0a-8e6530d48b58",
            "compositeImage": {
                "id": "bc4210b9-dbfd-48e7-9fda-b87c6497b45a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "368f6f41-b6d6-4834-8b3b-502803eb9608",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8642f58a-894c-4289-b407-28f199757114",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "368f6f41-b6d6-4834-8b3b-502803eb9608",
                    "LayerId": "3d816dd0-f0ab-46a8-ae3e-e1f7e4160d41"
                }
            ]
        },
        {
            "id": "8b0d130f-f476-4e66-b7f6-92975501e603",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a537bdd3-b3cd-48ab-8e0a-8e6530d48b58",
            "compositeImage": {
                "id": "012ac37a-53a5-4d25-bcc7-dad791e61f67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b0d130f-f476-4e66-b7f6-92975501e603",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfa9beab-cfe0-477e-b276-94831a4238bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b0d130f-f476-4e66-b7f6-92975501e603",
                    "LayerId": "3d816dd0-f0ab-46a8-ae3e-e1f7e4160d41"
                }
            ]
        },
        {
            "id": "4083c105-2b42-4318-9420-997887618640",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a537bdd3-b3cd-48ab-8e0a-8e6530d48b58",
            "compositeImage": {
                "id": "0af664b9-97b8-4345-8ffa-ca2b1cd47b1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4083c105-2b42-4318-9420-997887618640",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8df79ba9-7d11-4c89-a027-1f25711b4f4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4083c105-2b42-4318-9420-997887618640",
                    "LayerId": "3d816dd0-f0ab-46a8-ae3e-e1f7e4160d41"
                }
            ]
        },
        {
            "id": "a3473a4a-3a80-4da0-aea8-65ae5e062f29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a537bdd3-b3cd-48ab-8e0a-8e6530d48b58",
            "compositeImage": {
                "id": "2587ba19-037d-45b7-a3ca-bc09543d97bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3473a4a-3a80-4da0-aea8-65ae5e062f29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64626ddb-f853-4066-819c-7f13d63898be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3473a4a-3a80-4da0-aea8-65ae5e062f29",
                    "LayerId": "3d816dd0-f0ab-46a8-ae3e-e1f7e4160d41"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "3d816dd0-f0ab-46a8-ae3e-e1f7e4160d41",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a537bdd3-b3cd-48ab-8e0a-8e6530d48b58",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}