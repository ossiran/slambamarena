{
    "id": "514e7951-1687-4729-b182-b762764e9b8c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_walkingf",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 16,
    "bbox_right": 46,
    "bbox_top": 25,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b7331667-456b-4670-ab41-0aba17c0e6e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "514e7951-1687-4729-b182-b762764e9b8c",
            "compositeImage": {
                "id": "644a7742-afbb-4a0d-938d-7e6c16b89592",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7331667-456b-4670-ab41-0aba17c0e6e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2758ab23-36e1-4fe6-b70c-4a3ada2aaf17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7331667-456b-4670-ab41-0aba17c0e6e9",
                    "LayerId": "e2556f63-12e3-4fee-ae67-6cc113e80439"
                }
            ]
        },
        {
            "id": "2bf93f2c-6c1b-4ee4-a075-b4649fbe3703",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "514e7951-1687-4729-b182-b762764e9b8c",
            "compositeImage": {
                "id": "926e6571-43ee-464f-84f4-0251c88a136f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bf93f2c-6c1b-4ee4-a075-b4649fbe3703",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8b4ef26-688d-4e4b-a60a-e43b89cbfd24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bf93f2c-6c1b-4ee4-a075-b4649fbe3703",
                    "LayerId": "e2556f63-12e3-4fee-ae67-6cc113e80439"
                }
            ]
        },
        {
            "id": "47e7730d-d788-4596-bf12-66b92e4ecb19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "514e7951-1687-4729-b182-b762764e9b8c",
            "compositeImage": {
                "id": "3b86e844-45db-41ee-9983-e247e33397e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47e7730d-d788-4596-bf12-66b92e4ecb19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4921ec1-a73b-4ebd-951e-a547c2f954b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47e7730d-d788-4596-bf12-66b92e4ecb19",
                    "LayerId": "e2556f63-12e3-4fee-ae67-6cc113e80439"
                }
            ]
        },
        {
            "id": "8a5e147f-9d91-404e-a076-35e8d71769dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "514e7951-1687-4729-b182-b762764e9b8c",
            "compositeImage": {
                "id": "ffc72874-3cce-409d-814f-c965e23889b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a5e147f-9d91-404e-a076-35e8d71769dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b47ea87-c4ff-438a-8ee3-a8804643f887",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a5e147f-9d91-404e-a076-35e8d71769dc",
                    "LayerId": "e2556f63-12e3-4fee-ae67-6cc113e80439"
                }
            ]
        },
        {
            "id": "2790101b-2e08-43db-bfe0-72611b8a7d8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "514e7951-1687-4729-b182-b762764e9b8c",
            "compositeImage": {
                "id": "2c3b50a1-0722-4e63-b841-92310633bbf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2790101b-2e08-43db-bfe0-72611b8a7d8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90b5ec82-be28-44cc-9bfd-d659303cee29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2790101b-2e08-43db-bfe0-72611b8a7d8e",
                    "LayerId": "e2556f63-12e3-4fee-ae67-6cc113e80439"
                }
            ]
        },
        {
            "id": "f70e8237-934f-458f-8b8d-ec7caf05cc2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "514e7951-1687-4729-b182-b762764e9b8c",
            "compositeImage": {
                "id": "c97f2453-e8b9-41e1-a14d-1c78f7ea6b29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f70e8237-934f-458f-8b8d-ec7caf05cc2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f82df3a8-24e9-44a2-ace3-15288e906585",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f70e8237-934f-458f-8b8d-ec7caf05cc2d",
                    "LayerId": "e2556f63-12e3-4fee-ae67-6cc113e80439"
                }
            ]
        },
        {
            "id": "5fb154f8-1e9d-42c1-b5d9-013a08d9b1d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "514e7951-1687-4729-b182-b762764e9b8c",
            "compositeImage": {
                "id": "54fcdd4c-e036-4b38-94e5-dcd8ffc94cac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fb154f8-1e9d-42c1-b5d9-013a08d9b1d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d12b57d-2d85-42fc-ba0a-dfb7bd190345",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fb154f8-1e9d-42c1-b5d9-013a08d9b1d2",
                    "LayerId": "e2556f63-12e3-4fee-ae67-6cc113e80439"
                }
            ]
        },
        {
            "id": "0bd0d8cd-a12b-4d5d-8577-feebecd68b1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "514e7951-1687-4729-b182-b762764e9b8c",
            "compositeImage": {
                "id": "cd3de92c-015d-4eac-8f8b-a559617559a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bd0d8cd-a12b-4d5d-8577-feebecd68b1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8fc06a1-bda3-4bf5-a66d-33af11d7a954",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bd0d8cd-a12b-4d5d-8577-feebecd68b1a",
                    "LayerId": "e2556f63-12e3-4fee-ae67-6cc113e80439"
                }
            ]
        },
        {
            "id": "54561eef-6121-499f-937e-70b0dd575d39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "514e7951-1687-4729-b182-b762764e9b8c",
            "compositeImage": {
                "id": "eece9c58-cc35-492a-a32f-ffc7e6311b6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54561eef-6121-499f-937e-70b0dd575d39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cd306f0-b3be-46ec-9c2e-6f90bc07e684",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54561eef-6121-499f-937e-70b0dd575d39",
                    "LayerId": "e2556f63-12e3-4fee-ae67-6cc113e80439"
                }
            ]
        },
        {
            "id": "8296bcad-825c-4fa4-99e7-066ab2448dd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "514e7951-1687-4729-b182-b762764e9b8c",
            "compositeImage": {
                "id": "dcea083d-f64e-4cc8-bbfd-b7362f7c2d70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8296bcad-825c-4fa4-99e7-066ab2448dd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e977d4b9-b526-441d-8be3-b5296fcd0748",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8296bcad-825c-4fa4-99e7-066ab2448dd5",
                    "LayerId": "e2556f63-12e3-4fee-ae67-6cc113e80439"
                }
            ]
        },
        {
            "id": "d3913440-3314-41e7-969b-3f7ffab6aa64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "514e7951-1687-4729-b182-b762764e9b8c",
            "compositeImage": {
                "id": "bea18b19-7f0a-46de-9d50-6ded3c095f0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3913440-3314-41e7-969b-3f7ffab6aa64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "679705e8-3638-4ee9-aa40-c83cd6730068",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3913440-3314-41e7-969b-3f7ffab6aa64",
                    "LayerId": "e2556f63-12e3-4fee-ae67-6cc113e80439"
                }
            ]
        },
        {
            "id": "cf66e6f3-34a6-4d77-9ad1-fd9ed29f8f54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "514e7951-1687-4729-b182-b762764e9b8c",
            "compositeImage": {
                "id": "d82236f2-fdd1-40c1-a406-73e24399d5f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf66e6f3-34a6-4d77-9ad1-fd9ed29f8f54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30d6feae-fde5-44c3-8fd3-351eb60467c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf66e6f3-34a6-4d77-9ad1-fd9ed29f8f54",
                    "LayerId": "e2556f63-12e3-4fee-ae67-6cc113e80439"
                }
            ]
        },
        {
            "id": "cb8b8723-9ded-407a-9c43-f68019882165",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "514e7951-1687-4729-b182-b762764e9b8c",
            "compositeImage": {
                "id": "e707ed38-08af-4703-a7ab-8d39193a2a31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb8b8723-9ded-407a-9c43-f68019882165",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9884dc11-b47d-417b-b8f1-0fab17765bb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb8b8723-9ded-407a-9c43-f68019882165",
                    "LayerId": "e2556f63-12e3-4fee-ae67-6cc113e80439"
                }
            ]
        },
        {
            "id": "3be45f06-2fcd-445c-91bf-ca5ad4eb2a3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "514e7951-1687-4729-b182-b762764e9b8c",
            "compositeImage": {
                "id": "75688fec-8272-4185-92ae-5c8950d6e7aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3be45f06-2fcd-445c-91bf-ca5ad4eb2a3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3eb20fdc-d6ac-443f-88e4-ea70f4cc1384",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3be45f06-2fcd-445c-91bf-ca5ad4eb2a3e",
                    "LayerId": "e2556f63-12e3-4fee-ae67-6cc113e80439"
                }
            ]
        },
        {
            "id": "f434b478-3397-45e9-b393-5649172ebd10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "514e7951-1687-4729-b182-b762764e9b8c",
            "compositeImage": {
                "id": "418d4072-e7bb-419d-9578-62296b8f8d7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f434b478-3397-45e9-b393-5649172ebd10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dc958ae-d96b-4033-8650-2c4196fc10eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f434b478-3397-45e9-b393-5649172ebd10",
                    "LayerId": "e2556f63-12e3-4fee-ae67-6cc113e80439"
                }
            ]
        },
        {
            "id": "2885bb6b-55af-4387-9e9d-30c2e9ca8dec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "514e7951-1687-4729-b182-b762764e9b8c",
            "compositeImage": {
                "id": "26b154fd-53ca-4af4-8133-962cd7ccb30c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2885bb6b-55af-4387-9e9d-30c2e9ca8dec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f996430-0302-475d-aee2-d05c1706c976",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2885bb6b-55af-4387-9e9d-30c2e9ca8dec",
                    "LayerId": "e2556f63-12e3-4fee-ae67-6cc113e80439"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "e2556f63-12e3-4fee-ae67-6cc113e80439",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "514e7951-1687-4729-b182-b762764e9b8c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 75,
    "xorig": 31,
    "yorig": 69
}