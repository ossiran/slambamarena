{
    "id": "d77984b4-8414-46cc-abb6-4789218a1844",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_mair",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 141,
    "bbox_left": 49,
    "bbox_right": 103,
    "bbox_top": 70,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2aa82f63-6b6f-4ecb-a673-ea4a55a7db82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d77984b4-8414-46cc-abb6-4789218a1844",
            "compositeImage": {
                "id": "8770a9bb-fd64-48e7-ba36-ba24906c360a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2aa82f63-6b6f-4ecb-a673-ea4a55a7db82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00843fc1-3e8e-4dc1-afa0-51c33ae1ac63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2aa82f63-6b6f-4ecb-a673-ea4a55a7db82",
                    "LayerId": "2af120ec-0308-4dbb-949a-b542df709595"
                }
            ]
        },
        {
            "id": "996b7cd9-56a5-4be1-8ddb-148697e985d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d77984b4-8414-46cc-abb6-4789218a1844",
            "compositeImage": {
                "id": "335c432b-727d-43dc-a5c3-db7c95e38824",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "996b7cd9-56a5-4be1-8ddb-148697e985d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a9e8336-3110-4ac6-9bce-268cf4a23ea5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "996b7cd9-56a5-4be1-8ddb-148697e985d9",
                    "LayerId": "2af120ec-0308-4dbb-949a-b542df709595"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "2af120ec-0308-4dbb-949a-b542df709595",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d77984b4-8414-46cc-abb6-4789218a1844",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}