{
    "id": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lsyn_jh",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 31,
    "bbox_right": 106,
    "bbox_top": 35,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "442291d2-f97f-491d-9132-ab8e7c2e8eba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "200dae1b-ee9a-4e32-bb21-dfb1d823a412",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "442291d2-f97f-491d-9132-ab8e7c2e8eba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "556b687b-7811-47d0-acb1-7118f2be579c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "442291d2-f97f-491d-9132-ab8e7c2e8eba",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "5a4713fb-57aa-4963-9c63-6f74cc87c80d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "f5a19e61-1b5b-4c97-a689-092be76a65d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a4713fb-57aa-4963-9c63-6f74cc87c80d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ff2383e-94d9-49b0-91c2-7667c45b3a7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a4713fb-57aa-4963-9c63-6f74cc87c80d",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "1688f74b-70bd-4f1f-9abc-fab3c09dc2d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "b6c5c69d-5265-42f8-888b-6494fdbfa487",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1688f74b-70bd-4f1f-9abc-fab3c09dc2d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f1097ee-36e8-4a10-8e77-4cba4688f744",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1688f74b-70bd-4f1f-9abc-fab3c09dc2d0",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "c16090ba-91df-49f3-87cd-34d3ce4aa1ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "4a23a723-ba30-4b14-8461-3dd15db8f8c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c16090ba-91df-49f3-87cd-34d3ce4aa1ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21be1575-71b3-469f-b2af-05302202b7b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c16090ba-91df-49f3-87cd-34d3ce4aa1ba",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "14d08fdc-1e0a-440b-808c-8734b81f840b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "a0ffd0a1-48a1-4a63-abcf-d3c5968c06f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14d08fdc-1e0a-440b-808c-8734b81f840b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c93024be-c2a1-4579-a97d-38f40443b2a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14d08fdc-1e0a-440b-808c-8734b81f840b",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "7dab4d3c-137a-4569-87e1-01f650e786a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "6cbfb3dc-22ea-4d6f-ab8b-ad0422596a6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dab4d3c-137a-4569-87e1-01f650e786a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11c98cf1-8815-414e-9fc6-49a1c2c8f8ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dab4d3c-137a-4569-87e1-01f650e786a9",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "3587dee9-2943-41e2-81d7-561674558f9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "4a56e08e-743f-44cb-9d6c-0d4fe97f45e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3587dee9-2943-41e2-81d7-561674558f9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ebb9b0b-c167-4e73-8783-6612d5344eb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3587dee9-2943-41e2-81d7-561674558f9c",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "797aac0e-4196-4ab6-bc03-185e9e7680f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "89494c6b-ff70-40c0-a6f9-9638e859592a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "797aac0e-4196-4ab6-bc03-185e9e7680f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85f23268-081a-4649-ae66-2985908f9927",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "797aac0e-4196-4ab6-bc03-185e9e7680f8",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "cb477ddf-762b-4ffa-95a4-310e85559bab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "8119ae3d-c83c-4434-bd53-a65e529830c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb477ddf-762b-4ffa-95a4-310e85559bab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f400aec4-ba2a-4d2e-8c1b-0c53ad594f55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb477ddf-762b-4ffa-95a4-310e85559bab",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "a6d8ca3d-2180-4670-8a72-c83c39e13d9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "394be259-3dc9-4ed3-a3fe-30bbb8fb2bbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6d8ca3d-2180-4670-8a72-c83c39e13d9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "206c0ee2-ec6f-4f95-9f4a-85cf7154aebd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6d8ca3d-2180-4670-8a72-c83c39e13d9e",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "e75f69b1-10f5-44e1-ad59-63e2b97c9f09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "f12b357a-36d0-476f-a55a-873ab06040a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e75f69b1-10f5-44e1-ad59-63e2b97c9f09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4af007bf-44cb-41b2-be4f-75729a40ea55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e75f69b1-10f5-44e1-ad59-63e2b97c9f09",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "635eab2b-e34b-4905-a7db-27168a119e1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "4283c46c-194e-4574-a8e6-39d3d894d648",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "635eab2b-e34b-4905-a7db-27168a119e1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba6c0068-340a-46af-b0c4-983bb0f2ecab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "635eab2b-e34b-4905-a7db-27168a119e1a",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "66823921-42ac-45c0-ba7b-69a9fe9003d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "87547e36-790d-4bd6-a555-7201c50f1c2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66823921-42ac-45c0-ba7b-69a9fe9003d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a2c4c79-2a84-491b-91eb-015b8975b3df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66823921-42ac-45c0-ba7b-69a9fe9003d8",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "0efa8d43-83a6-472c-9d03-3f6ad7ceb0b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "48a12863-63e1-4cd3-ab7e-8f0773bf38b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0efa8d43-83a6-472c-9d03-3f6ad7ceb0b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e92ad6dc-0e76-46b4-a83c-927fd14936c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0efa8d43-83a6-472c-9d03-3f6ad7ceb0b9",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "9f8c84b8-32b1-4255-8b95-cb2ee8ef457e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "f66baa92-26b4-497e-bf19-bbf5f29f1de5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f8c84b8-32b1-4255-8b95-cb2ee8ef457e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e8b280e-db61-4254-8164-40765ed6d505",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f8c84b8-32b1-4255-8b95-cb2ee8ef457e",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "3afeeaa8-ea27-427d-93d3-b18cfb25ae81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "0739ce08-afeb-418c-af2e-00db3288ce9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3afeeaa8-ea27-427d-93d3-b18cfb25ae81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc9c762d-7b8d-449d-abfc-dc66e408452d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3afeeaa8-ea27-427d-93d3-b18cfb25ae81",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "173933c8-e782-4475-9aa7-5750dbdf9d56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "9cfae10b-e348-4cd9-bcbd-fabd43bc281d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "173933c8-e782-4475-9aa7-5750dbdf9d56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6929c747-398c-46d8-ba70-95227e9c8bfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "173933c8-e782-4475-9aa7-5750dbdf9d56",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "afcce452-c11f-42ee-8fa4-2b4ddc778b94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "807a38ad-b840-424e-8094-e52e1085d8e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afcce452-c11f-42ee-8fa4-2b4ddc778b94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1221fb41-0186-4c3f-870f-498374f37356",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afcce452-c11f-42ee-8fa4-2b4ddc778b94",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "5fbf37cf-202c-45ae-b541-e9d3633d0486",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "664b6244-aad0-412b-b32d-ab06d5f5b196",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fbf37cf-202c-45ae-b541-e9d3633d0486",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bed2d547-d7fa-41be-8d33-771aaed4fb28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fbf37cf-202c-45ae-b541-e9d3633d0486",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "66903b12-ff4f-4305-ba9e-b7904cece234",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "813c657f-dd90-4664-9f0e-d8736d0daa25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66903b12-ff4f-4305-ba9e-b7904cece234",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94f2a23d-3e89-4a35-a4ca-206d18658f2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66903b12-ff4f-4305-ba9e-b7904cece234",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "c3dd105a-6709-4eac-aae5-ebb36873a3fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "982c0b5d-e2e2-4cc2-83f3-0b2b881494ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3dd105a-6709-4eac-aae5-ebb36873a3fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0f64002-9139-4d6c-b6d7-1095dbc7ca58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3dd105a-6709-4eac-aae5-ebb36873a3fb",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "8802b2ae-0e16-4ac7-9f8e-67b7c09be5b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "635c4115-ecdb-462e-b979-923b5dcd3544",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8802b2ae-0e16-4ac7-9f8e-67b7c09be5b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4a2acc9-aed0-466c-a4f6-dbe01e52ded4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8802b2ae-0e16-4ac7-9f8e-67b7c09be5b8",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "24327880-81cd-49d4-8dcb-e564e47ea0bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "e58577a3-6a92-429e-8d2c-48cd67fe06aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24327880-81cd-49d4-8dcb-e564e47ea0bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3156227d-27f3-4162-abb3-835a947c7374",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24327880-81cd-49d4-8dcb-e564e47ea0bd",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "ca443ff4-7ad2-47eb-baa6-b9ba4484f090",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "139bf8ab-146c-4f84-bf83-818b75730555",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca443ff4-7ad2-47eb-baa6-b9ba4484f090",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8ea06a0-bc23-46ec-b66e-c8990dc23c97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca443ff4-7ad2-47eb-baa6-b9ba4484f090",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "5e5b4322-f36b-41e4-a287-87a4c8ae5bbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "b2769eb6-006b-4f8a-960f-0cbab96eec24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e5b4322-f36b-41e4-a287-87a4c8ae5bbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d69fb10c-c444-42fc-94ee-e76b5478f3c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e5b4322-f36b-41e4-a287-87a4c8ae5bbc",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "99796505-5f78-401e-9098-b365ef3c9865",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "579c88b0-f884-4f99-99f7-a7f7c9df4874",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99796505-5f78-401e-9098-b365ef3c9865",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b077e8d3-8be0-46cd-80f6-66e501e41e4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99796505-5f78-401e-9098-b365ef3c9865",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "98fbc1ea-54ba-46d2-b5e6-39ce16a19dfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "f244f70f-21e5-4286-a76d-fcae290ab9cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98fbc1ea-54ba-46d2-b5e6-39ce16a19dfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "959aaac4-a357-46fb-975f-ee4c12c4243b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98fbc1ea-54ba-46d2-b5e6-39ce16a19dfb",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "feba065e-414b-432f-b9f6-c2d7609e1301",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "027c7f3c-0e49-4cfa-8717-fde06e14d01d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "feba065e-414b-432f-b9f6-c2d7609e1301",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de709d8d-f3f0-4ec1-9af4-13a1e7c083bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "feba065e-414b-432f-b9f6-c2d7609e1301",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "8834ce8d-bbe1-4735-b3d3-93cdfe3b5628",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "c783944e-c786-4461-b42f-d643ab594fdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8834ce8d-bbe1-4735-b3d3-93cdfe3b5628",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e541aace-18d5-40f0-a51d-72959ef29f9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8834ce8d-bbe1-4735-b3d3-93cdfe3b5628",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "24d337f7-fd69-42a0-94af-7f85410ce045",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "c78dece7-5d02-44be-995e-75e833e25207",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24d337f7-fd69-42a0-94af-7f85410ce045",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "109d8684-b141-4269-aef2-bc333391891d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24d337f7-fd69-42a0-94af-7f85410ce045",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "b6d072be-122f-4bcb-82ae-aa89d0939643",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "599f0422-a031-42c4-900d-6561b527334c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6d072be-122f-4bcb-82ae-aa89d0939643",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "675b5b28-d2b0-4c80-ba9d-3d7b83fc457f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6d072be-122f-4bcb-82ae-aa89d0939643",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "954779a4-a3e1-4fea-b9b8-350ab14823bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "be7904fb-73b7-4ead-9f0c-9288b504c594",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "954779a4-a3e1-4fea-b9b8-350ab14823bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6706264-8445-4171-ab16-6a686ab2a260",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "954779a4-a3e1-4fea-b9b8-350ab14823bf",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "05ecf981-20f5-4662-b441-c0b9cb9aed3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "c13da148-cd9a-4856-93ea-18032d2740dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05ecf981-20f5-4662-b441-c0b9cb9aed3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa8eda8d-06ce-41e6-8884-de9a0f0439e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05ecf981-20f5-4662-b441-c0b9cb9aed3a",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        },
        {
            "id": "b7455c94-5b08-4f95-b66a-f5d293cd2fe1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "compositeImage": {
                "id": "7f0f88a1-f2bb-4603-966b-f6c178fce37b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7455c94-5b08-4f95-b66a-f5d293cd2fe1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f37fe309-9346-439b-aee0-73e7da4e1e2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7455c94-5b08-4f95-b66a-f5d293cd2fe1",
                    "LayerId": "f7a5f608-9dda-472a-b508-9876dcefd086"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "f7a5f608-9dda-472a-b508-9876dcefd086",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d0b261e8-ee93-4eff-a2bf-41ee22197d2c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}