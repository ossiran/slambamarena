{
    "id": "c042c8ab-9604-410a-a946-5431cc9bf740",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_crouch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 24,
    "bbox_right": 115,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3fe39da6-3945-460a-a138-add98a865603",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c042c8ab-9604-410a-a946-5431cc9bf740",
            "compositeImage": {
                "id": "1c435c14-3418-4c5a-bb94-37b987d08225",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fe39da6-3945-460a-a138-add98a865603",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "868b49c9-f00b-4964-87db-19a96590dc0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fe39da6-3945-460a-a138-add98a865603",
                    "LayerId": "d121e230-a376-4ecf-92cc-d358538cfc58"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d121e230-a376-4ecf-92cc-d358538cfc58",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c042c8ab-9604-410a-a946-5431cc9bf740",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 63,
    "yorig": 87
}