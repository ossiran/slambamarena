{
    "id": "36dcb9fa-c7c0-473b-a160-6bbc6b9760c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_jl_ice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a333cea-ec57-4225-aec3-8a4e2dd7b647",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dcb9fa-c7c0-473b-a160-6bbc6b9760c1",
            "compositeImage": {
                "id": "6a04fea2-551e-4fb6-b182-376e7269f5c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a333cea-ec57-4225-aec3-8a4e2dd7b647",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5385ec91-ed85-4969-94d1-c03ca45950a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a333cea-ec57-4225-aec3-8a4e2dd7b647",
                    "LayerId": "b8b507da-65e6-486e-9cc9-c2c2dad543ba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b8b507da-65e6-486e-9cc9-c2c2dad543ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36dcb9fa-c7c0-473b-a160-6bbc6b9760c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}