live_call();

if(state_new == 1)
{
    with(hurtbox0)
	{
	    xoffset = -29;
	    yoffset = -36;
	    image_xscale  = 58;
	    image_yscale  = 68;
	    xshift  = -1;
	}
	
	with(hurtbox1)
	{
	    xoffset = -15;
	    yoffset = -61;
	    image_xscale  = 30;
	    image_yscale  = 25;
	    xshift = -3;
	}
	
	
	with(collisionbox)
	{
	    xoffset = -20;
	    yoffset = -22;
	    image_xscale  = 40;
	    image_yscale  = 54;
	    xshift = 0;
	}
	
	image_index = 29;
}




image_xscale = -frontOrBack;

    //right
    if(input_check(6,0,0,0,0))
    {
    	vx = Approach(vx,vxMax * image_xscale,ground_accel);
    	if((image_index > 30 && image_index < 35) || (image_index > 0 && image_index < 5))
        {
            vx += 1.5 * image_xscale;
        }
    }
    
    //left
    if(input_check(4,0,0,0,0))
    {
    	vx = Approach(vx,-vxMax * image_xscale,ground_accel);
    	if((image_index > 30 && image_index < 35) || (image_index > 0 && image_index < 5))
        {
            vx += -1.5 * image_xscale;
        }
    }
    
    if(sign(vx) == frontOrBack)
    {
        sprite_index = spr_momo_bwalk;
    }
    else
    {
        sprite_index = spr_momo_fwalk;
    }


//friction
if((key_left == 0 && key_right == 0)||(key_left == 1 && key_right == 1))
{
	vx = Approach(vx,0,ground_fric);
	state_switch("stand");
}

//jab
if(input_check(0,1,0,0,0))
{
    state_switch("light");
}

//clight
if(input_check(2,1,0,0,0) || input_check(1,1,0,0,0) || input_check(3,1,0,0,0))
{
    state_switch("clight");
}

//medium
if(input_check(0,0,1,0,0))
{
    state_switch("medium");
}

//cmedium
if(input_check(2,0,1,0,0) || input_check(1,0,1,0,0) || input_check(3,0,1,0,0))
{
    state_switch("cmedium");
}

//heavy
if(input_check(0,0,0,1,0))
{
    state_switch("heavy");
}


//cheavy
if(input_check(2,0,0,1,0))
{
    state_switch("cheavy");
}

//charge back
if(chb() == 1)
{
    if(input_check(6,0,0,1,0))
    {
        if(ex > 49.9)
        {
            state_switch("chbh");
        }
        else
        {
            state_switch("chbm");
        }
    }
    else if(input_check(6,0,1,0,0))
    {
        state_switch("chbm");
    }
    else if(input_check(6,1,0,0,0))
    {
        state_switch("chbl");
    }
}


//qcb
if(qcb_buffer > 0)
{
    if(input_check(0,0,0,1,0))
    {
        if(ex > 24.9)
        {
            state_switch("qcbh");
        }
        else
        {
            state_switch("qcbm");
        }
    }
    else if(input_check(0,0,1,0,0))
    {
        state_switch("qcbm");
    }
    else if(input_check(0,1,0,0,0))
    {
        state_switch("qcbl");
    }
}

//charge down
if(chd() == 1)
{
    if(input_check(8,0,0,1,0))
    {
        if(ex > 49.9)
        {
            state_switch("chdh");
        }
        else
        {
            state_switch("chdm");
        }
    }
    else if(input_check(8,0,1,0,0))
    {
        state_switch("chdm");
    }
    else if(input_check(8,1,0,0,0))
    {
        state_switch("chdl");
    }
}

//dtap
if(dd_buffer > 0)
{
    if(input_check(0,0,0,1,0))
    {
        if(ex > 49.9)
        {
            state_switch("ddh");
        }
        else
        {
            state_switch("ddl");
        }
    }
    else if(input_check(0,0,1,0,0))
    {
        state_switch("ddl");
    }
    else if(input_check(0,1,0,0,0))
    {
        state_switch("ddl");
    }
}

//grab
if(input_check(0,1,0,0,1))
{
    state_switch("grab");
}

//dash
//fdash
if(ff_buffer > 0 || (input_check(6, 1, 1, 0, 0)) || (input_check(5, 1, 1, 0, 0)))
{
        state_switch("fdash");
}
//bdash
if(bb_buffer > 0 || (input_check(4, 1, 1, 0, 0)))
{
        state_switch("bdash");
}

//jump
if(input_check(7,0,0,0,0) || input_check(8,0,0,0,0) || input_check(9,0,0,0,0))
{
	state_switch("jumpsquat");
}

//crouch
if(input_check(1,0,0,0,0) || input_check(2,0,0,0,0) || input_check(3,0,0,0,0))
{
     state_switch("crouch");   
}

//stand
if(input_check(5,0,0,0,0))
{
	state_switch("stand");
}

//block
if(blockstun > 0)
{
	state_switch("sblock");	
}

//hitstun
if(hitstun > 0)
{
    state_switch("hitstun");   
}

if(grabbed == 1)
{
 state_switch("grabbed");   
}