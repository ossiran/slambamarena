{
    "id": "dc67d410-d5fd-43f7-80f7-8ea48d8aaa6a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_hitstun_g",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 50,
    "bbox_right": 93,
    "bbox_top": 75,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51a1009d-a51e-4444-87d5-46b9113f8bbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc67d410-d5fd-43f7-80f7-8ea48d8aaa6a",
            "compositeImage": {
                "id": "6f2ad298-e590-49b3-8af0-06202b76a1d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51a1009d-a51e-4444-87d5-46b9113f8bbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97b16ad2-ecff-47f3-857d-d21730d140f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51a1009d-a51e-4444-87d5-46b9113f8bbe",
                    "LayerId": "008d6548-6171-4027-8bf2-61417795bf98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "008d6548-6171-4027-8bf2-61417795bf98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc67d410-d5fd-43f7-80f7-8ea48d8aaa6a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}