/// @description Insert description here
// You can write your code in this editor

if(mode == "icel")
{
    //making initial hitbox
    if(duration == 1)
    {
        hitbox0 = create_hitbox(-10,-10,35, 20, 7, 0, 14, 20, 8, 10, -1, 0, 400, 3, "projectilemid", "icemissle" + string(obj_system.state_timer))
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 2;
        hitbox0.juggle = 0;
        hitbox0.airxtrahitstun = -15;
        hitbox0.airxtrayhit = -11;
        hitbox0.owner = owner;
        hitbox0.following = other;
        hitbox0.projectile = -1;
        hitbox0.chipdamage = 5;
        if(owner.state_name == "chbh")
        {
            hitbox0.exgive = 0;
        }
    }

	    
	    vx = Approach(vx, vxMax * image_xscale, 0.15);
}

if(mode == "icem")
{
    //making initial hitbox
    if(duration == 1)
    {
        hitbox0 = create_hitbox(-10,-10,35, 20, 7, 0, 14, 20, 8, 10, -1, 0, 400, 3, "projectilemid", "icemissle" + string(obj_system.state_timer))
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 2;
        hitbox0.juggle = 0;
        hitbox0.airxtrahitstun = -15;
        hitbox0.airxtrayhit = -11;
        hitbox0.owner = owner;
        hitbox0.following = other;
        hitbox0.projectile = -1;
        hitbox0.chipdamage = 5;
        if(owner.state_name == "chbh")
        {
            hitbox0.exgive = 0;
        }
    }

	   
	vx = Approach(vx, vxMax * image_xscale, 0.3);
}

if(mode == "iceh")
{
    //making initial hitbox
    if(duration == 0)
    {
        hitbox0 = create_hitbox(-10,-10,35, 20, 7, 0, 14, 20, 8, 10, -1, 0, 400, 0, "projectilemid", "icemissle" + string(obj_system.state_timer))
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 2;
        hitbox0.juggle = 0;
        hitbox0.airxtrahitstun = -15;
        hitbox0.airxtrayhit = -11;
        hitbox0.owner = owner;
        hitbox0.following = other;
        hitbox0.projectile = -1;
        hitbox0.chipdamage = 5;
        wait = 3;
    }
	    
	if(hits < 2)
    {
        vx = Approach(vx, vxMax/2 * image_xscale, 0.4);
    }
    else
    {
	    vx = Approach(vx, vxMax * image_xscale, 0.4);
    }
    
    if(hitbox1 == -1 && hits == 1)
    {
        if(wait == 0)
        {
            hitbox1 = create_hitbox(-10,-10,35, 20, 7, 0, 14, 20, 8, 10, -1, 0, 400, 0, "projectilemid", "icemissle" + string(obj_system.state_timer))
            hitbox1.hitsound = snd_hitsound1;
            hitbox1.scaling = 2;
            hitbox1.juggle = 0;
            hitbox1.airxtrahitstun = -15;
            hitbox1.airxtrayhit = -11;
            hitbox1.owner = owner;
            hitbox1.following = other;
            hitbox1.projectile = -1;
            hitbox1.chipdamage = 5;
        }
        else
        {
            wait --;
        }
    }
}

if(mode == "firel")
{
    //making initial hitbox
    if(duration == 0)
    {
        hitbox0 = create_hitbox(-10,-10,35, 20, 2, -8, 6, 8, 2, 10, -1, 0, 400, 2, "projectilemid", "firemissle" + string(obj_system.state_timer))
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 0.8;
        hitbox0.juggle = 0;
        hitbox0.airxtrahitstun = -2;
        hitbox0.airxtrayhit = 5;
        hitbox0.owner = owner;
        hitbox0.following = other;
        hitbox0.projectile = -1;
        hitbox0.chipdamage = 4;
        if(owner.state_name == "chbh")
        {
            hitbox0.exgive = 0;
        }
        wait = 1;
    }

    if(hits < 2)
    {
        vx = 0;
    }
    else
    {
	    vx = Approach(vx, vxMax * image_xscale, 0.5);
    }

    if(hitbox1 == -1 && hits == 1)
    {
        if(wait == 0)
        {
            hitbox1 = create_hitbox(-10,-10,35, 20, 5, -7.5, 7, 6, 6, 8, -1, 0, 400, 2, "projectilemid", "firemissle" + string(obj_system.state_timer))
            hitbox1.hitsound = snd_hitsound1;
            hitbox1.scaling = 0.7;
            hitbox1.juggle = 0;
            hitbox1.airxtrahitstun = -2;
            hitbox1.owner = owner;
            hitbox1.following = other;
            hitbox1.projectile = -1;
            hitbox1.chipdamage = 4;
            if(owner.state_name == "chbh")
            {
                hitbox0.exgive = 0;
            }
        }
        else
        {
            wait--;
        }
    }
}

if(mode == "firem")
{
    //making initial hitbox
    if(duration == 0)
    {
        hitbox0 = create_hitbox(-10,-10,35, 20, 2, -8, 6, 8, 2, 10, -1, 0, 400, 2, "projectilemid", "firemissle" + string(obj_system.state_timer))
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 0.8;
        hitbox0.juggle = 0;
        hitbox0.airxtrahitstun = -2;
        hitbox0.airxtrayhit = 5;
        hitbox0.owner = owner;
        hitbox0.following = other;
        hitbox0.projectile = -1;
        hitbox0.chipdamage = 4;
        if(owner.state_name == "chbh")
        {
            hitbox0.exgive = 0;
        }
        wait = 1;
    }

    if(hits < 2)
    {
        vx = 0;
    }
    else
    {
	    vx = Approach(vx, vxMax * image_xscale, 0.75);
    }

    if(hitbox1 == -1 && hits == 1)
    {
        if(wait == 0)
        {
            hitbox1 = create_hitbox(-10,-10,35, 20, 5, -7.5, 7, 6, 6, 8, -1, 0, 400, 2, "projectilemid", "firemissle" + string(obj_system.state_timer))
            hitbox1.hitsound = snd_hitsound1;
            hitbox1.scaling = 0.7;
            hitbox1.juggle = 0;
            hitbox1.airxtrahitstun = -2;
            hitbox1.owner = owner;
            hitbox1.following = other;
            hitbox1.projectile = -1;
            hitbox1.chipdamage = 4;
            if(owner.state_name == "chbh")
            {
                hitbox0.exgive = 0;
            }
        }
        else
        {
            wait--;
        }
    }
}

if(mode == "fireh")
{
    //making initial hitbox
    if(duration == 0)
    {
        hitbox0 = create_hitbox(-10,-10,35, 20, 2, -8, 6, 8, 2, 10, -1, 0, 400, 0, "projectilemid", "firemissle" + string(obj_system.state_timer))
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 0.8;
        hitbox0.juggle = 0;
        hitbox0.airxtrahitstun = -2;
        hitbox0.airxtrayhit = 5;
        hitbox0.owner = owner;
        hitbox0.following = other;
        hitbox0.projectile = -1;
        hitbox0.chipdamage = 4;
        wait = 2;
    }

    if(hits < 2)
    {
        vx = 0;
    }
    else
    {
	    vx = Approach(vx, vxMax * image_xscale, 0.75);
    }

    if(hitbox1 == -1 && hits == 2)
    {
        if(wait == 0)
        {
            hitbox1 = create_hitbox(-10,-10,35, 20, 2, -8, 7, 6, 6, 8, -1, 0, 400, 0, "projectilemid", "firemissle" + string(obj_system.state_timer))
            hitbox1.hitsound = snd_hitsound1;
            hitbox1.scaling = 0.7;
            hitbox1.juggle = 0;
            hitbox1.airxtrahitstun = -2;
            hitbox1.owner = owner;
            hitbox1.following = other;
            hitbox1.projectile = -1;
            hitbox1.chipdamage = 4;
            wait = 2;
        }
        else
        {
            wait--;
        }
    }
    
    if(hitbox2 == -1 && hits == 1)
    {
        if(wait == 0)
        {
            hitbox1 = create_hitbox(-10,-10,35, 20, 5, -8, 7, 6, 6, 8, -1, 0, 400, 0, "projectilemid", "firemissle" + string(obj_system.state_timer))
            hitbox1.hitsound = snd_hitsound1;
            hitbox1.scaling = 0.7;
            hitbox1.juggle = 0;
            hitbox1.airxtrahitstun = -2;
            hitbox1.owner = owner;
            hitbox1.following = other;
            hitbox1.projectile = -1;
            hitbox1.chipdamage = 4;
        }
        else
        {
            wait--;
        }
    }
}


//vertical missles
if(mode == "iceul")
{
    //making initial hitbox
    if(duration == 1)
    {
        hitbox0 = create_hitbox(-15,-10,30, 30, 4.5, 0, 14, 20, 8, 10, -1, 0, 400, 3, "projectilemid", "icemissle" + string(obj_system.state_timer))
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 2;
        hitbox0.juggle = 0;
        hitbox0.airxtrahitstun = -15;
        hitbox0.airxtrayhit = -11;
        hitbox0.owner = owner;
        hitbox0.following = other;
        hitbox0.projectile = -1;
        hitbox0.chipdamage = 5;
    }

	    
	    vx = Approach(vx, vxMax * image_xscale, 0.15);
	    vy = Approach(vy, vyMax, 0.4);

    if(image_xscale == 1)
    {
	    image_angle = -65;
    }
    else
    {
        image_angle = 65;
    }
}

//vertical missles
if(mode == "iceum")
{
    //making initial hitbox
    if(duration == 1)
    {
        hitbox0 = create_hitbox(-15,-10,30, 30, 4.5, 0, 14, 20, 8, 10, -1, 0, 400, 3, "projectilemid", "icemissle" + string(obj_system.state_timer))
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 2;
        hitbox0.juggle = 0;
        hitbox0.airxtrahitstun = -15;
        hitbox0.airxtrayhit = -11;
        hitbox0.owner = owner;
        hitbox0.following = other;
        hitbox0.projectile = -1;
        hitbox0.chipdamage = 5;
    }

	    
	    vx = Approach(vx, vxMax * image_xscale, 0.15);
	    vy = Approach(vy, vyMax, 0.4);
	
    if(image_xscale == 1)
    {
	    image_angle = -45;
    }
    else
    {
        image_angle = 45;
    }
}

if(mode == "iceuh")
{
    //making initial hitbox
    if(duration == 0)
    {
        hitbox0 = create_hitbox(-10,-10,35, 20, 3, 0, 14, 20, 8, 10, -1, 0, 400, 0, "projectilemid", "icemissle" + string(obj_system.state_timer))
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 2;
        hitbox0.juggle = 0;
        hitbox0.airxtrahitstun = -15;
        hitbox0.airxtrayhit = -11;
        hitbox0.owner = owner;
        hitbox0.following = other;
        hitbox0.projectile = -1;
        hitbox0.chipdamage = 5;
        wait = 3;
    }
	    
	if(hits < 2)
    {
        vx = Approach(vx, vxMax/2 * image_xscale, 0.4);
        vy = Approach(vy, vyMax/2, 0.4 );
    }
    else
    {
	    vx = Approach(vx, vxMax * image_xscale, 0.4);
	    vy = Approach(vy, vyMax, 0.4 );
    }
    
    if(hitbox1 == -1 && hits == 1)
    {
        if(wait == 0)
        {
            hitbox1 = create_hitbox(-10,-10,35, 20, 7, 0, 14, 20, 8, 10, -1, 0, 400, 0, "projectilemid", "icemissle" + string(obj_system.state_timer))
            hitbox1.hitsound = snd_hitsound1;
            hitbox1.scaling = 2;
            hitbox1.juggle = 0;
            hitbox1.airxtrahitstun = -15;
            hitbox1.airxtrayhit = -11;
            hitbox1.owner = owner;
            hitbox1.following = other;
            hitbox1.projectile = -1;
            hitbox1.chipdamage = 5;
        }
        else
        {
            wait --;
        }
    }
}


if(mode == "fireul")
{
    //making initial hitbox
    if(duration == 0)
    {
        hitbox0 = create_hitbox(-10,-10,35, 20, 2, -8, 6, 8, 2, 10, -1, 0, 400, 2, "projectilemid", "firemissle" + string(obj_system.state_timer))
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 0.8;
        hitbox0.juggle = 0;
        hitbox0.airxtrahitstun = -2;
        hitbox0.airxtrayhit = 5;
        hitbox0.owner = owner;
        hitbox0.following = other;
        hitbox0.projectile = -1;
        hitbox0.chipdamage = 4;
        if(owner.state_name == "chbh")
        {
            hitbox0.exgive = 0;
        }
        wait = 1;
    }

    if(hits < 2)
    {
        vx = 0;
    }
    else
    {
	    vx = Approach(vx, vxMax * image_xscale, 0.3);
	    vy = Approach(vy, vyMax, 0.4);
    }

    if(hitbox1 == -1 && hits == 1)
    {
        if(wait == 0)
        {
            hitbox1 = create_hitbox(-10,-10,35, 20, 5, -7.5, 7, 6, 6, 8, -1, 0, 400, 2, "projectilemid", "firemissle" + string(obj_system.state_timer))
            hitbox1.hitsound = snd_hitsound1;
            hitbox1.scaling = 0.7;
            hitbox1.juggle = 0;
            hitbox1.airxtrahitstun = -2;
            hitbox1.owner = owner;
            hitbox1.following = other;
            hitbox1.projectile = -1;
            hitbox1.chipdamage = 4;
            if(owner.state_name == "chbh")
            {
                hitbox0.exgive = 0;
            }
        }
        else
        {
            wait--;
        }
    }
}

if(mode == "fireum")
{
    //making initial hitbox
    if(duration == 0)
    {
        hitbox0 = create_hitbox(-10,-10,35, 20, 2, -8, 6, 8, 2, 10, -1, 0, 400, 2, "projectilemid", "firemissle" + string(obj_system.state_timer))
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 0.8;
        hitbox0.juggle = 0;
        hitbox0.airxtrahitstun = -2;
        hitbox0.airxtrayhit = 5;
        hitbox0.owner = owner;
        hitbox0.following = other;
        hitbox0.projectile = -1;
        hitbox0.chipdamage = 4;
        if(owner.state_name == "chbh")
        {
            hitbox0.exgive = 0;
        }
        wait = 1;
    }

    if(hits < 2)
    {
        vx = 0;
    }
    else
    {
	    vx = Approach(vx, vxMax * image_xscale, 0.5);
	    vy = Approach(vy, vyMax, 0.4);
    }

    if(hitbox1 == -1 && hits == 1)
    {
        if(wait == 0)
        {
            hitbox1 = create_hitbox(-10,-10,35, 20, 5, -7.5, 7, 6, 6, 8, -1, 0, 400, 2, "projectilemid", "firemissle" + string(obj_system.state_timer))
            hitbox1.hitsound = snd_hitsound1;
            hitbox1.scaling = 0.7;
            hitbox1.juggle = 0;
            hitbox1.airxtrahitstun = -2;
            hitbox1.owner = owner;
            hitbox1.following = other;
            hitbox1.projectile = -1;
            hitbox1.chipdamage = 4;
            if(owner.state_name == "chbh")
            {
                hitbox0.exgive = 0;
            }
        }
        else
        {
            wait--;
        }
    }
}

if(mode == "fireuh")
{
    //making initial hitbox
    if(duration == 0)
    {
        hitbox0 = create_hitbox(-10,-10,35, 20, 2, -8, 6, 8, 2, 10, -1, 0, 400, 0, "projectilemid", "firemissle" + string(obj_system.state_timer))
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 0.8;
        hitbox0.juggle = 0;
        hitbox0.airxtrahitstun = -2;
        hitbox0.airxtrayhit = 5;
        hitbox0.owner = owner;
        hitbox0.following = other;
        hitbox0.projectile = -1;
        hitbox0.chipdamage = 4;
        wait = 2;
    }

    if(hits < 2)
    {
        vx = 0;
    }
    else
    {
	    vx = Approach(vx, vxMax * image_xscale, 0.75);
	    vy = Approach(vy, vyMax, 0.4);
    }

    if(hitbox1 == -1 && hits == 2)
    {
        if(wait == 0)
        {
            hitbox1 = create_hitbox(-10,-10,35, 20, 2, -8, 7, 6, 6, 8, -1, 0, 400, 0, "projectilemid", "firemissle" + string(obj_system.state_timer))
            hitbox1.hitsound = snd_hitsound1;
            hitbox1.scaling = 0.7;
            hitbox1.juggle = 0;
            hitbox1.airxtrahitstun = -2;
            hitbox1.owner = owner;
            hitbox1.following = other;
            hitbox1.projectile = -1;
            hitbox1.chipdamage = 4;
            wait = 2;
        }
        else
        {
            wait--;
        }
    }
    
    if(hitbox2 == -1 && hits == 1)
    {
        if(wait == 0)
        {
            hitbox1 = create_hitbox(-10,-10,35, 20, 5, -8, 7, 6, 6, 8, -1, 0, 400, 0, "projectilemid", "firemissle" + string(obj_system.state_timer))
            hitbox1.hitsound = snd_hitsound1;
            hitbox1.scaling = 0.7;
            hitbox1.juggle = 0;
            hitbox1.airxtrahitstun = -2;
            hitbox1.owner = owner;
            hitbox1.following = other;
            hitbox1.projectile = -1;
            hitbox1.chipdamage = 4;
        }
        else
        {
            wait--;
        }
    }
}

if(duration == 600)
{
    instance_destroy();
    
    if(object_exists(hitbox0))
    {
        instance_destroy(hitbox0)
    }
    
    if(object_exists(hitbox1))
    {
        instance_destroy(hitbox1)
    }
}

duration ++;

if(x < 0 || x > room_width || hits == 0)
{
    if(object_exists(hitbox0))
    {
        instance_destroy(hitbox0)
    }
    
    if(object_exists(hitbox1))
    {
        instance_destroy(hitbox1)
    }
    
    instance_destroy();
}

