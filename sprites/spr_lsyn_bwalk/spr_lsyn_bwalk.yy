{
    "id": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lsyn_bwalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 108,
    "bbox_left": 51,
    "bbox_right": 77,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b46a508d-7ec8-4429-a8de-aa8706653708",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "61cb2d8f-e00b-4333-ab0f-8e3d532380bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b46a508d-7ec8-4429-a8de-aa8706653708",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ac356d4-29eb-497a-be85-0dbca34ebbd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b46a508d-7ec8-4429-a8de-aa8706653708",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "3be0112a-85bc-4ce6-831b-dc9d5560c030",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "9a1faebf-8373-4fa5-8039-517b81ff4f84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3be0112a-85bc-4ce6-831b-dc9d5560c030",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8f2aa23-3f0f-4697-b545-2af96cd1642e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3be0112a-85bc-4ce6-831b-dc9d5560c030",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "a9bf41f5-31cb-4215-a9b8-bf951d9bf792",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "fb9829c1-0448-4519-b787-adce40c3bcd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9bf41f5-31cb-4215-a9b8-bf951d9bf792",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6eeaf4ca-6684-4f15-9d17-0998fb05bf90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9bf41f5-31cb-4215-a9b8-bf951d9bf792",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "94573e0e-8ca2-4119-85fd-3412266c1e70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "bf1db565-2ce4-4367-8d78-b3905abf4c12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94573e0e-8ca2-4119-85fd-3412266c1e70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b04ae91-b1e4-4568-a4dc-fd3fe5545066",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94573e0e-8ca2-4119-85fd-3412266c1e70",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "96d44643-5889-40d8-a592-0759b69e0008",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "c45c504c-5085-4b27-966e-e84a41af2ad9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96d44643-5889-40d8-a592-0759b69e0008",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24f27cf5-c007-48ae-adb5-757f8919fab1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96d44643-5889-40d8-a592-0759b69e0008",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "cae005e8-5cab-4b6e-b237-a6a061db8229",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "80a30122-482d-4dd9-8833-09ad389df839",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cae005e8-5cab-4b6e-b237-a6a061db8229",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df3a69c5-6105-4cd0-bb1a-c2486eeb866d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cae005e8-5cab-4b6e-b237-a6a061db8229",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "67863b5b-4ae3-4fe2-b615-f54473e0a44b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "c4ed7cf2-dea0-4181-85f3-dd76337e05b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67863b5b-4ae3-4fe2-b615-f54473e0a44b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ab17fd7-fc82-4fd7-af2b-9533f1f78737",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67863b5b-4ae3-4fe2-b615-f54473e0a44b",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "c23e86e0-0c11-4a44-984e-4edacefda107",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "db110d02-0d35-4f7a-ba38-4bd0d0f48460",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c23e86e0-0c11-4a44-984e-4edacefda107",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fde25d70-666e-4175-8d20-c87b7b6cc66b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c23e86e0-0c11-4a44-984e-4edacefda107",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "7aac3198-706c-4316-b932-b625cb09770b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "4505ad5c-3a9e-4693-86f7-6b0d5a6dcfed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7aac3198-706c-4316-b932-b625cb09770b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd9af1b6-f01b-46fd-b48e-91331a606c1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7aac3198-706c-4316-b932-b625cb09770b",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "5ca02a7e-35ff-4cc8-b095-2c177461b4d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "7cf8ac9a-ae14-4fd2-8f83-5e551b91d9ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ca02a7e-35ff-4cc8-b095-2c177461b4d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c97fa6fd-7e91-4510-b5e5-f284abedb1bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ca02a7e-35ff-4cc8-b095-2c177461b4d7",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "c925ad7e-6fb8-46f7-8285-0b6b75535c4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "98bcbc9b-14df-4a38-b6cb-9f33e6e1f8b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c925ad7e-6fb8-46f7-8285-0b6b75535c4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "569abaa1-f2f7-4d2e-b831-d7050dae39f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c925ad7e-6fb8-46f7-8285-0b6b75535c4f",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "2c765b4c-ebb0-4ba2-abd6-16e1a21df3f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "a7833471-1683-436c-a397-ea113db74eec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c765b4c-ebb0-4ba2-abd6-16e1a21df3f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0c82d59-e8b7-4b7a-939f-0151ac10dd4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c765b4c-ebb0-4ba2-abd6-16e1a21df3f9",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "ab71078f-97ea-4988-984c-840b3b3009ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "322f4037-fc77-4fbe-85a7-b2344f83b6cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab71078f-97ea-4988-984c-840b3b3009ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4972a978-2464-4662-b6a4-9b31a6fd277e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab71078f-97ea-4988-984c-840b3b3009ff",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "ea1a0d47-4f49-4462-a54a-dd65973a42fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "da9f1324-858e-4566-990b-7a7a5b57f4d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea1a0d47-4f49-4462-a54a-dd65973a42fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82aeef1f-a4ae-491e-bdfd-4ea3e9226174",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea1a0d47-4f49-4462-a54a-dd65973a42fd",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "e9855f3e-1fbf-4163-8b89-71120f42faef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "c225b905-6979-4cc2-aaef-4930f3de4bf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9855f3e-1fbf-4163-8b89-71120f42faef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed9838f9-1921-45e8-ad1a-3d856464011e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9855f3e-1fbf-4163-8b89-71120f42faef",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "9fe525a3-deea-4d70-b3d3-f9b8bbbe429f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "6479edbc-3084-4d44-9972-bfa834124f96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fe525a3-deea-4d70-b3d3-f9b8bbbe429f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "145a7f6d-6acd-46f6-bc4b-13d3a53b781b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fe525a3-deea-4d70-b3d3-f9b8bbbe429f",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "ef0a56a3-b942-4807-9c32-2b40416b53a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "e23cff4f-5dfc-4e12-a8ac-b86d008dfa38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef0a56a3-b942-4807-9c32-2b40416b53a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b587cf7b-c8d7-4976-bc53-4941a9b28795",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef0a56a3-b942-4807-9c32-2b40416b53a4",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "cf4db389-7a9b-4c8e-9c11-ebce2aa072a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "38037b3e-9cbf-41b8-bad0-2643aacec260",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf4db389-7a9b-4c8e-9c11-ebce2aa072a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07bc255c-0778-4529-bcdd-ac7308343792",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf4db389-7a9b-4c8e-9c11-ebce2aa072a0",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "a734e7d5-7cf7-4600-8c11-7846703ac8b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "03697f39-e321-4bb1-a472-ab6b4aa0c1f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a734e7d5-7cf7-4600-8c11-7846703ac8b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "656c4025-2a6b-46ae-b65c-379a1a9d07d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a734e7d5-7cf7-4600-8c11-7846703ac8b6",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "da1d79ed-69c5-47a7-b54b-b0b9ec4ce067",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "d40a0175-af35-4d52-8ae3-eb09c99da632",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da1d79ed-69c5-47a7-b54b-b0b9ec4ce067",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05760df2-0ac8-4b6a-8c65-63eb3ca51e70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da1d79ed-69c5-47a7-b54b-b0b9ec4ce067",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "b849dc94-9a16-42ab-819f-c512325790ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "2159e7fd-b489-4dd9-bfa6-ea4484e3ef5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b849dc94-9a16-42ab-819f-c512325790ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a65d3cd0-9c57-4502-9a0a-5adf592008d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b849dc94-9a16-42ab-819f-c512325790ea",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "b63ebe20-2877-46ae-aa60-7cdc5dd798d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "e780d0a6-2cb3-4ff4-8331-8b0d82e8ff91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b63ebe20-2877-46ae-aa60-7cdc5dd798d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcf1cdc2-df87-43a4-a5ac-ecce356d5d08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b63ebe20-2877-46ae-aa60-7cdc5dd798d1",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "f54b3c7f-85ac-43cb-b00d-d29a220f56bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "724ecd57-cbcd-46f1-8215-b1f8ccafafa1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f54b3c7f-85ac-43cb-b00d-d29a220f56bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da3dd033-cde8-41a5-8a84-b4be2b2fd835",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f54b3c7f-85ac-43cb-b00d-d29a220f56bf",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "4f8c23b7-9387-47b7-a1c2-fecf45a24eed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "3df5984a-c761-4224-8502-9711cee5d1d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f8c23b7-9387-47b7-a1c2-fecf45a24eed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08a3673d-122e-4f54-8da8-ef6cc8e09588",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f8c23b7-9387-47b7-a1c2-fecf45a24eed",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "85d31396-b62f-4bcf-be12-75159a4e1a97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "738525f9-74dd-41d5-9739-440ee87aed86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85d31396-b62f-4bcf-be12-75159a4e1a97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87be8723-ae2f-45c8-b4b3-eed72e8ca522",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85d31396-b62f-4bcf-be12-75159a4e1a97",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "b81170fe-a172-494e-ab80-1a6925624168",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "1d61ae8d-9ce8-44a5-a37f-1e2e35328e71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b81170fe-a172-494e-ab80-1a6925624168",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30b5f355-9a2b-4d09-a108-4f4d20590f15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b81170fe-a172-494e-ab80-1a6925624168",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "2fa97d32-2c6e-4077-8035-ed28b54508b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "88cf6275-0b2f-4f11-9d45-6fb5101bdf15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fa97d32-2c6e-4077-8035-ed28b54508b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58cba878-fc3e-488b-9cf4-250a96169eaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fa97d32-2c6e-4077-8035-ed28b54508b2",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "3d6a3a26-5a2f-40ce-8911-303a2a18de51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "d2509d19-0aff-4ce5-ba13-c3793d248cdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d6a3a26-5a2f-40ce-8911-303a2a18de51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1386f68d-8117-4734-82f3-ccd3348bf45e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d6a3a26-5a2f-40ce-8911-303a2a18de51",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "7751b685-8f24-49e4-9188-6d16fa915c8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "5a4265b4-b8d6-4aee-adc2-8598e7520a3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7751b685-8f24-49e4-9188-6d16fa915c8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fe48c85-3c8e-4a8d-b0b0-9a9df6cedeb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7751b685-8f24-49e4-9188-6d16fa915c8d",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "aa451a6c-ac98-471d-8db9-9ee5af2185dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "f1504d0c-0f77-41fb-8e30-9366324a2674",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa451a6c-ac98-471d-8db9-9ee5af2185dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf5790ad-8a8d-4b2b-a57c-de9d0bc7a432",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa451a6c-ac98-471d-8db9-9ee5af2185dd",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "7f380ffc-43f5-452c-b4ef-8d65fbda3578",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "3e6f2b0e-24b9-4096-bd31-8a8761e85650",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f380ffc-43f5-452c-b4ef-8d65fbda3578",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12ad3199-5726-43c6-9975-23b9e0d892f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f380ffc-43f5-452c-b4ef-8d65fbda3578",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "d2ec03c9-1c3a-45f4-bc99-2073714ed1b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "20e590d8-7037-44ac-bccd-9c83589a47a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2ec03c9-1c3a-45f4-bc99-2073714ed1b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c168613c-28c1-41db-85ff-c99a75e928f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2ec03c9-1c3a-45f4-bc99-2073714ed1b8",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "95231d98-1fc2-4552-9a64-7f55196c1a4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "b9cd4799-1e3b-446f-ab90-416a3851bb55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95231d98-1fc2-4552-9a64-7f55196c1a4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48ff764c-247b-4079-8242-2cd8a84e6dfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95231d98-1fc2-4552-9a64-7f55196c1a4c",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "085d0544-abaa-4e6e-8b75-5aab990e25df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "d10775c6-b98a-4eef-a006-f201c3bdb0de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "085d0544-abaa-4e6e-8b75-5aab990e25df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c57a148-d37b-4ce5-bc29-9c921441bd4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "085d0544-abaa-4e6e-8b75-5aab990e25df",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "eef552a8-b6e9-4f40-a6ec-59e7dea0a3a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "5c88c8c2-07db-4d9b-95f4-ad1ffe2564d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eef552a8-b6e9-4f40-a6ec-59e7dea0a3a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1b2e717-5cc3-425b-9dff-803e9b8d4bc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eef552a8-b6e9-4f40-a6ec-59e7dea0a3a6",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "232ea988-b977-4fe9-9e04-b3452009581a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "ea93ce97-ea39-44b1-b11a-bab5f29b420f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "232ea988-b977-4fe9-9e04-b3452009581a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd2542fa-1797-40b2-8f78-47ebe7c9d74f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "232ea988-b977-4fe9-9e04-b3452009581a",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "c877d691-bd30-44b3-9e10-dd4b83365fa8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "a1156989-2163-4ebe-925b-d7584c753be4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c877d691-bd30-44b3-9e10-dd4b83365fa8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "addc5ab3-1e37-49c0-8bc0-dfa4922a0ef4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c877d691-bd30-44b3-9e10-dd4b83365fa8",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "10d4ed57-9154-4dfc-bbeb-1260631b61a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "c97495c8-7b87-4a32-999a-1c2fe3c3a724",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10d4ed57-9154-4dfc-bbeb-1260631b61a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b2d641f-eeb6-4eb2-9763-5f57dba3cc75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10d4ed57-9154-4dfc-bbeb-1260631b61a2",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "6e442e40-7d76-4f71-a6ff-c04def37194b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "44774d7a-cb49-4607-92d4-bb3d448df816",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e442e40-7d76-4f71-a6ff-c04def37194b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28b12f82-8d7a-4229-9842-b3ae61b44560",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e442e40-7d76-4f71-a6ff-c04def37194b",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        },
        {
            "id": "d3accfbe-edc9-40d0-9000-793a1b754506",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "compositeImage": {
                "id": "766b2e8d-b97a-4d79-affb-98740e50122a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3accfbe-edc9-40d0-9000-793a1b754506",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43b52345-6c5a-4763-9ef5-520cb36143a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3accfbe-edc9-40d0-9000-793a1b754506",
                    "LayerId": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "7df780c6-d1e8-434f-b7b6-ef4602e77ccc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a4a743f3-2c04-4883-bffe-8e202ff1ed36",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}