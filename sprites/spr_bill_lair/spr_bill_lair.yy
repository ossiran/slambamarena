{
    "id": "681268b7-fc89-4315-9761-af3cee24ee4f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_lair",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 148,
    "bbox_left": 45,
    "bbox_right": 94,
    "bbox_top": 70,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ed3797ce-0cee-4885-8613-2a4aec02cefc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "681268b7-fc89-4315-9761-af3cee24ee4f",
            "compositeImage": {
                "id": "ae1dbe0d-68b1-4b42-9089-dd67e3b9a430",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed3797ce-0cee-4885-8613-2a4aec02cefc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3496d215-d872-4a5c-8ae5-3e61fa43610d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed3797ce-0cee-4885-8613-2a4aec02cefc",
                    "LayerId": "c8147422-6a81-4b00-aee5-668ad9a577b7"
                }
            ]
        },
        {
            "id": "2de66282-c4c1-4a55-a3f4-fe65d3bd6460",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "681268b7-fc89-4315-9761-af3cee24ee4f",
            "compositeImage": {
                "id": "912f5bca-204e-4d27-8a88-3f5bad66c2ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2de66282-c4c1-4a55-a3f4-fe65d3bd6460",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08bd818e-aa51-42d4-9ff4-660578ee426e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2de66282-c4c1-4a55-a3f4-fe65d3bd6460",
                    "LayerId": "c8147422-6a81-4b00-aee5-668ad9a577b7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "c8147422-6a81-4b00-aee5-668ad9a577b7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "681268b7-fc89-4315-9761-af3cee24ee4f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}