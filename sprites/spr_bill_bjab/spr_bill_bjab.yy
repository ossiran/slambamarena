{
    "id": "171b5941-56d2-4cf7-aa29-83820a320e7d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_bjab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 49,
    "bbox_right": 99,
    "bbox_top": 75,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "407f456e-f846-4ef6-acc2-6e09fe3b5a61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "171b5941-56d2-4cf7-aa29-83820a320e7d",
            "compositeImage": {
                "id": "db240e22-de2f-4c2f-9bd8-7a924b430d0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "407f456e-f846-4ef6-acc2-6e09fe3b5a61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "caf587f0-6143-4fba-81d2-120ab15f80fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "407f456e-f846-4ef6-acc2-6e09fe3b5a61",
                    "LayerId": "b84119ed-e009-4dec-bd79-502394a03d75"
                }
            ]
        },
        {
            "id": "6a7bfcd5-bce5-412d-8c26-b8371f528e38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "171b5941-56d2-4cf7-aa29-83820a320e7d",
            "compositeImage": {
                "id": "fbbda716-aa47-4e1b-98f7-adea11ee287b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a7bfcd5-bce5-412d-8c26-b8371f528e38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "270d684a-ed71-4eb5-beaa-3881cb1b3ae9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a7bfcd5-bce5-412d-8c26-b8371f528e38",
                    "LayerId": "b84119ed-e009-4dec-bd79-502394a03d75"
                }
            ]
        },
        {
            "id": "345f3a8d-d707-4e75-8d88-5c16ab408f9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "171b5941-56d2-4cf7-aa29-83820a320e7d",
            "compositeImage": {
                "id": "a2238f98-a1ed-452a-b74f-7587fe3f7f60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "345f3a8d-d707-4e75-8d88-5c16ab408f9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "deb2c48a-c0df-47dc-8b96-598d8d33b857",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "345f3a8d-d707-4e75-8d88-5c16ab408f9c",
                    "LayerId": "b84119ed-e009-4dec-bd79-502394a03d75"
                }
            ]
        },
        {
            "id": "733a2041-6e53-4fd0-bc3c-68a43deab62a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "171b5941-56d2-4cf7-aa29-83820a320e7d",
            "compositeImage": {
                "id": "c6c80a8e-e21c-4d90-a194-8cffbaf08bb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "733a2041-6e53-4fd0-bc3c-68a43deab62a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7792af29-4f54-41a0-be64-ec95e3c9d54f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "733a2041-6e53-4fd0-bc3c-68a43deab62a",
                    "LayerId": "b84119ed-e009-4dec-bd79-502394a03d75"
                }
            ]
        },
        {
            "id": "98ac30ca-3b0f-47b2-8342-4ad5d67cb75b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "171b5941-56d2-4cf7-aa29-83820a320e7d",
            "compositeImage": {
                "id": "e110dbe8-578b-41d7-b0e2-bced0821d65c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98ac30ca-3b0f-47b2-8342-4ad5d67cb75b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27acc612-0630-4746-8ad8-2fd53fd4dff3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98ac30ca-3b0f-47b2-8342-4ad5d67cb75b",
                    "LayerId": "b84119ed-e009-4dec-bd79-502394a03d75"
                }
            ]
        },
        {
            "id": "9167c6b8-622f-4433-a76a-585f83f2aa18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "171b5941-56d2-4cf7-aa29-83820a320e7d",
            "compositeImage": {
                "id": "9830bd2c-7174-4779-956f-ab8af6814be2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9167c6b8-622f-4433-a76a-585f83f2aa18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b16df7d-cbb7-4b88-a326-a7065e36b418",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9167c6b8-622f-4433-a76a-585f83f2aa18",
                    "LayerId": "b84119ed-e009-4dec-bd79-502394a03d75"
                }
            ]
        },
        {
            "id": "d3a74a6d-fd41-48c9-affc-98cabdeefde9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "171b5941-56d2-4cf7-aa29-83820a320e7d",
            "compositeImage": {
                "id": "b6619931-ab36-4dd7-82bd-f6829917bc03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3a74a6d-fd41-48c9-affc-98cabdeefde9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d68b52d-e4f8-47f3-9d8f-460187f87408",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3a74a6d-fd41-48c9-affc-98cabdeefde9",
                    "LayerId": "b84119ed-e009-4dec-bd79-502394a03d75"
                }
            ]
        },
        {
            "id": "a526dbf6-4416-4080-91d5-a61d877e5405",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "171b5941-56d2-4cf7-aa29-83820a320e7d",
            "compositeImage": {
                "id": "c109bd69-4c59-4254-9c04-524f2936e4a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a526dbf6-4416-4080-91d5-a61d877e5405",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1acb9d9-5f96-4344-85f9-9abc9723480e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a526dbf6-4416-4080-91d5-a61d877e5405",
                    "LayerId": "b84119ed-e009-4dec-bd79-502394a03d75"
                }
            ]
        },
        {
            "id": "362cdcea-dc96-429d-96ff-2ba385a35d1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "171b5941-56d2-4cf7-aa29-83820a320e7d",
            "compositeImage": {
                "id": "d95626a1-7ee7-4965-8a43-7d0a05decf6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "362cdcea-dc96-429d-96ff-2ba385a35d1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "682ada5c-666d-4850-806b-d2151e1d6e31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "362cdcea-dc96-429d-96ff-2ba385a35d1f",
                    "LayerId": "b84119ed-e009-4dec-bd79-502394a03d75"
                }
            ]
        },
        {
            "id": "e027ed78-7e72-4c72-99e7-a0eb06d51841",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "171b5941-56d2-4cf7-aa29-83820a320e7d",
            "compositeImage": {
                "id": "bf103c63-5df1-4f03-829b-fe4836e7b968",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e027ed78-7e72-4c72-99e7-a0eb06d51841",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3649e6f-b031-410a-87b0-35c06fc96f20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e027ed78-7e72-4c72-99e7-a0eb06d51841",
                    "LayerId": "b84119ed-e009-4dec-bd79-502394a03d75"
                }
            ]
        },
        {
            "id": "a4bf7238-fb73-444e-a033-2be7ba6e6ae5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "171b5941-56d2-4cf7-aa29-83820a320e7d",
            "compositeImage": {
                "id": "db7493a6-6ba0-47b5-8095-edbce79484bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4bf7238-fb73-444e-a033-2be7ba6e6ae5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c1729b6-22bb-4aea-9ebb-3340e0cd7431",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4bf7238-fb73-444e-a033-2be7ba6e6ae5",
                    "LayerId": "b84119ed-e009-4dec-bd79-502394a03d75"
                }
            ]
        },
        {
            "id": "94937809-4ea9-4778-b946-b22360f9379f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "171b5941-56d2-4cf7-aa29-83820a320e7d",
            "compositeImage": {
                "id": "4bab5ac6-9f89-417b-863b-207e07e7ba36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94937809-4ea9-4778-b946-b22360f9379f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2c163b3-8b6c-4912-bcc9-9f4b090380ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94937809-4ea9-4778-b946-b22360f9379f",
                    "LayerId": "b84119ed-e009-4dec-bd79-502394a03d75"
                }
            ]
        },
        {
            "id": "8c005e4a-a740-48cf-8b22-b962963737ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "171b5941-56d2-4cf7-aa29-83820a320e7d",
            "compositeImage": {
                "id": "79da897b-a7b7-4273-b35b-644f12cad709",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c005e4a-a740-48cf-8b22-b962963737ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63fb5e76-f4a3-453e-9f8f-943764390477",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c005e4a-a740-48cf-8b22-b962963737ca",
                    "LayerId": "b84119ed-e009-4dec-bd79-502394a03d75"
                }
            ]
        },
        {
            "id": "1fa5efe6-d278-4161-9258-f2e0d670b502",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "171b5941-56d2-4cf7-aa29-83820a320e7d",
            "compositeImage": {
                "id": "aba366a2-381e-47ab-97b2-4f780e2e4c36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fa5efe6-d278-4161-9258-f2e0d670b502",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c285b2fb-944b-4224-8d05-eb20392859ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fa5efe6-d278-4161-9258-f2e0d670b502",
                    "LayerId": "b84119ed-e009-4dec-bd79-502394a03d75"
                }
            ]
        },
        {
            "id": "5a0395df-f789-43fc-a41a-966b6b3d1e06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "171b5941-56d2-4cf7-aa29-83820a320e7d",
            "compositeImage": {
                "id": "62ad6435-8bd2-47c0-ad99-9344c0d0c8e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a0395df-f789-43fc-a41a-966b6b3d1e06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfb36fc2-a468-4691-b42d-141dec087f31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a0395df-f789-43fc-a41a-966b6b3d1e06",
                    "LayerId": "b84119ed-e009-4dec-bd79-502394a03d75"
                }
            ]
        },
        {
            "id": "efc81c80-2210-4b7a-9b82-69d1e62fca8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "171b5941-56d2-4cf7-aa29-83820a320e7d",
            "compositeImage": {
                "id": "e2763b8f-6026-466e-9549-41f3b212da7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efc81c80-2210-4b7a-9b82-69d1e62fca8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a3b6d94-9337-4c7b-928f-e1469a68095c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efc81c80-2210-4b7a-9b82-69d1e62fca8b",
                    "LayerId": "b84119ed-e009-4dec-bd79-502394a03d75"
                }
            ]
        },
        {
            "id": "d5752c9e-ca25-4ed6-8acf-521f64930f15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "171b5941-56d2-4cf7-aa29-83820a320e7d",
            "compositeImage": {
                "id": "84196807-c9f7-474e-8ca8-a8ef84de380a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5752c9e-ca25-4ed6-8acf-521f64930f15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa1168e0-a5ef-4128-a2d4-ab4bb714db4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5752c9e-ca25-4ed6-8acf-521f64930f15",
                    "LayerId": "b84119ed-e009-4dec-bd79-502394a03d75"
                }
            ]
        },
        {
            "id": "2d1a8016-0011-4b95-8257-14b5af9194a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "171b5941-56d2-4cf7-aa29-83820a320e7d",
            "compositeImage": {
                "id": "a141546e-f024-4b6b-aa36-44bde5f99304",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d1a8016-0011-4b95-8257-14b5af9194a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "266362d7-22b0-47e1-b558-b994ae789d1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d1a8016-0011-4b95-8257-14b5af9194a1",
                    "LayerId": "b84119ed-e009-4dec-bd79-502394a03d75"
                }
            ]
        },
        {
            "id": "9f64bba3-f24d-4a25-a23d-21dba5c3719a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "171b5941-56d2-4cf7-aa29-83820a320e7d",
            "compositeImage": {
                "id": "6bec27bf-fd45-4b10-81b3-56b3e9b85844",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f64bba3-f24d-4a25-a23d-21dba5c3719a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "659497c6-0015-406b-b6ae-bab8f027d00e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f64bba3-f24d-4a25-a23d-21dba5c3719a",
                    "LayerId": "b84119ed-e009-4dec-bd79-502394a03d75"
                }
            ]
        },
        {
            "id": "1a2e3eee-9ca1-4178-923b-b76e47485d11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "171b5941-56d2-4cf7-aa29-83820a320e7d",
            "compositeImage": {
                "id": "01b1d2d5-586a-4bff-8986-5659b7927b6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a2e3eee-9ca1-4178-923b-b76e47485d11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "286a53da-b77c-4016-b4d2-beba370e3ae2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a2e3eee-9ca1-4178-923b-b76e47485d11",
                    "LayerId": "b84119ed-e009-4dec-bd79-502394a03d75"
                }
            ]
        },
        {
            "id": "d5110190-c619-4712-b363-61115ce91efb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "171b5941-56d2-4cf7-aa29-83820a320e7d",
            "compositeImage": {
                "id": "8f65c72a-bb34-4fbc-a0e3-33e562a58b5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5110190-c619-4712-b363-61115ce91efb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f3fe78f-46af-4af3-9baf-4c032e13db9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5110190-c619-4712-b363-61115ce91efb",
                    "LayerId": "b84119ed-e009-4dec-bd79-502394a03d75"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "b84119ed-e009-4dec-bd79-502394a03d75",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "171b5941-56d2-4cf7-aa29-83820a320e7d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}