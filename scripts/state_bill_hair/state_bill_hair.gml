if(state_new == 1)
{
	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -43;
	    image_xscale  = 26;
	    image_yscale  = 55;
	    xshift  = -2;
	}
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 16;
	    image_yscale  = 37;
	    xshift = -2
	}
	
	attackframe0 = 6;
	endstate = 25;
}


//animation
sprite_index = spr_bill_hair2;


//attack
if(state_timer == attackframe0)
{
    var hitfalladd = 0;

    //juggling correctly
    with(otherplayer)
    {
        if(!onground() || vy < 0)
        {
            hitfalladd = 1;
        }
    }

    hitbox4 = create_hitbox(0,-27,52,49,4,6,15,18,7,10,0 - hitfalladd,1,2,2,"overhead",-1);
	hitbox4.hitsound = snd_hit;
}

attack_base(endstate,1,0,1,1,0);