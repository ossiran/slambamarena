live_call();
if (synmode > 0)
{
    if(state_new == 1)
    {
        with(hurtbox0)
    	{
    	    xoffset = -12;
    	    yoffset = -13;
    	    image_xscale  = 24;
    	    image_yscale  = 40;
    	    xshift  = 0;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -8;
    	    yoffset = -31;
    	    image_xscale  = 16;
    	    image_yscale  = 18;
    	    xshift = 0;
    	}
    	
    	with(collisionbox)
    	{
    	    xoffset = -8;
    	    yoffset = -13;
    	    image_xscale  = 16;
    	    image_yscale  = 40;
    	    xshift = 0;
    	}
    	
    	chainmove = 0;
    	attackframe0 = 8;
    	attackframe1 = 10;
    	endstate = 30   ;
    }
    
    //move
    if(state_timer < attackframe0 && state_timer > 2)
    {
        vx = Approach(vx,4 * image_xscale, 2);
    }
    else
    {
        vx = Approach(vx,0,ground_fric);
    }
    
    //attack
    if(state_timer == attackframe0)
    {
        hitbox0 = create_hitbox(9,-27,47,33,1,0,9,20,10,15,0,0,0,4,"mid",1);
        hitbox1 = create_hitbox(9,6,37,20,1,0,9,20,10,15,0,0,0,4,"mid",1);
        hitbox2 = create_hitbox(20,-36,36,12,1,0,9,20,10,15,0,0,0,4,"mid",1);
        attackhurtbox0 = create_hurtbox(8 * image_xscale, -17,22 * image_xscale,16,2,-2);
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 0.5;
        hitbox1.hitsound = snd_hitsound1;
        hitbox1.scaling = 0.5;
        hitbox2.hitsound = snd_hitsound1;
        hitbox2.scaling = 0.5;
        hitbox1.juggle = 1;
        hitbox2.juggle = 1;
    }
    
    if(state_timer == attackframe1)
    {
        hitbox0 = create_hitbox(9,-27,44,33,8,0,9,22,10,12,0,0,2,4,"mid",1);
        hitbox1 = create_hitbox(9,6,37,20,8,0,9,22,10,12,0,0,2,4,"mid",1);
        hitbox2 = create_hitbox(20,-36,36,12,8,0,9,22,10,11,0,0,2,4,"mid",1);
        attackhurtbox0 = create_hurtbox(8 * image_xscale, -17,22 * image_xscale,16,2,-2);
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 0.5;
        hitbox1.hitsound = snd_hitsound1;
        hitbox1.scaling = 0.5;
        hitbox2.hitsound = snd_hitsound1;
        hitbox2.scaling = 0.5;
        hitbox1.juggle = 1;
        hitbox2.juggle = 1;
        
        if(hitting == 1)
        {
            hitbox1.juggle = 0;
            hitbox2.juggle = 0;
        }
    }
    sprite_index = spr_lsyn_5h;
    
    //chainmove
    if(state_timer > 2)
    {
        
        if((input_check(2,0,0,1,0) || input_check(1,0,0,1,0) || input_check(3,0,0,1,0)))
        {
            chainmove = "cheavy";
        }
    }
    attack_base(endstate,0,0,1,1,0);
}
else if(synmode < 0)
{
    if(state_new == 1)
    {
        with(hurtbox0)
    	{
    	    xoffset = -12;
    	    yoffset = -13;
    	    image_xscale  = 24;
    	    image_yscale  = 40;
    	    xshift  = 0;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -8;
    	    yoffset = -31;
    	    image_xscale  = 16;
    	    image_yscale  = 18;
    	    xshift = 0;
    	}
    	
    	with(collisionbox)
    	{
    	    xoffset = -8;
    	    yoffset = -13;
    	    image_xscale  = 16;
    	    image_yscale  = 40;
    	    xshift = 0;
    	}
    	
    	chainmove = 0;
    	attackframe0 = 8;
    	endstate = 27   ;
    }
    
    //move
    if(state_timer < attackframe0 && state_timer > 2)
    {
        vx = Approach(vx,7 * image_xscale, 2);
    }
    else
    {
        vx = Approach(vx,0,ground_fric);
    }
    
    //attack
    if(state_timer == attackframe0)
    {
        hitbox0 = create_hitbox(9,-27,47,33,7,0,20,20,11,15,0,0,0,4,"mid",1);
        hitbox1 = create_hitbox(9,6,37,20,7,0,10,20,11,15,0,0,0,4,"mid",1);
        hitbox2 = create_hitbox(20,-36,36,12,7,0,20,20,11,15,0,0,0,4,"mid",1);
        attackhurtbox0 = create_hurtbox(8 * image_xscale, -17,22 * image_xscale,16,2,-2);
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 1;
        hitbox1.hitsound = snd_hitsound1;
        hitbox1.scaling = 1;
        hitbox2.hitsound = snd_hitsound1;
        hitbox2.scaling = 1;
        hitbox1.juggle = 1;
        hitbox2.juggle = 1;
    }
    
    sprite_index = spr_dsyn_5h;
    
    //chainmove
    if(state_timer > 2)
    {
        
        if((input_check(2,0,0,1,0) || input_check(1,0,0,1,0) || input_check(3,0,0,1,0)))
        {
            chainmove = "cheavy";
        }
    }
    attack_base(endstate,0,0,1,1,0);
}
