if(state_new == 1)
{
    	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -28;
	    xscale  = 26;
	    yscale  = 60;
	    xshift  = -2;
	}
	chainmove = 0;
	attackframe0 = 4;
	endstate = 19;
}

sprite_index = spr_bill_crjab;


if(state_timer == attackframe0)
{
    hitbox1 = create_hitbox(10,16,40,16,3.7,0,16,17,10,15,-1,0,2,3.5,"low",-1);
    attackhurtbox0 = create_hurtbox(12 *image_xscale, 20,24 *image_xscale,22,4,-2);
    hitbox1.hitsound = snd_hitsound2;
    hitbox1.scaling = 1.1;
    hitbox1.juggle = 1;
    audio_play_sound(snd_bill_airdash,1,0);
}

//chainmove
if(state_timer > 2)
{

    
    if(input_check(0,0,0,1,0))
    {
        chainmove = "heavy"
    }
    
    if((input_check(2,0,0,1,0) || input_check(1,0,0,1,0) || input_check(3,0,0,1,0)))
    {
        chainmove = "cheavy";
    }
}

attack_base(endstate,0,0,1,1,0);



/*
//chain

if(hitting == 1 && otherplayer.hitpause == 0 && (input_check(0,0,0,1,0) || chainmove == "heavy"))
{
    state_switch("heavy");
}

if(hitting == 1 && otherplayer.hitpause == 0 && ((input_check(2,0,0,1,0) || input_check(1,0,0,1,0) || input_check(3,0,0,1,0)) || chainmove == "cheavy"))
{
    state_switch("cheavy");
}
*/
