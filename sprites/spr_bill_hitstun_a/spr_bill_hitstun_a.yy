{
    "id": "2d8363b2-109a-4aaa-b051-a129827bb200",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_hitstun_a",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 48,
    "bbox_right": 99,
    "bbox_top": 75,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3942e613-47ef-442d-b828-39c3ccbb740f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d8363b2-109a-4aaa-b051-a129827bb200",
            "compositeImage": {
                "id": "a20f622c-0700-4af9-b0b0-8b3055793c37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3942e613-47ef-442d-b828-39c3ccbb740f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23456850-0e56-42a3-a277-050126d20b38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3942e613-47ef-442d-b828-39c3ccbb740f",
                    "LayerId": "23d74ea2-cbbb-4c4b-bde6-2a0178168218"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "23d74ea2-cbbb-4c4b-bde6-2a0178168218",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d8363b2-109a-4aaa-b051-a129827bb200",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}