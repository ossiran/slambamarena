{
    "id": "7da6f2a6-97c7-40a0-8f30-3398cc27732b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dsyn_falling",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 110,
    "bbox_left": 39,
    "bbox_right": 84,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2f972e2d-9616-4a34-88a3-4b6ea2263f83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7da6f2a6-97c7-40a0-8f30-3398cc27732b",
            "compositeImage": {
                "id": "62f4704c-fc73-4a5a-a12c-bcd2d76be660",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f972e2d-9616-4a34-88a3-4b6ea2263f83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37dc30ba-0e44-4359-b1b0-3c77c471fde0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f972e2d-9616-4a34-88a3-4b6ea2263f83",
                    "LayerId": "b8e4c42a-833e-4575-a1e9-867c9421b2a9"
                }
            ]
        },
        {
            "id": "a14143e9-ef2f-4e49-b2af-be9c47bf2883",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7da6f2a6-97c7-40a0-8f30-3398cc27732b",
            "compositeImage": {
                "id": "422abaae-253c-4e69-9f1c-86b1485c68cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a14143e9-ef2f-4e49-b2af-be9c47bf2883",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35a91654-e9fd-4158-a1db-c754646272cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a14143e9-ef2f-4e49-b2af-be9c47bf2883",
                    "LayerId": "b8e4c42a-833e-4575-a1e9-867c9421b2a9"
                }
            ]
        },
        {
            "id": "0d5b9abe-4418-4033-89d8-29cec20749f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7da6f2a6-97c7-40a0-8f30-3398cc27732b",
            "compositeImage": {
                "id": "f8e740b0-2687-4a9e-968c-c056c254f8bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d5b9abe-4418-4033-89d8-29cec20749f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49e23c8e-465c-4440-9991-e7070de1bd4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d5b9abe-4418-4033-89d8-29cec20749f9",
                    "LayerId": "b8e4c42a-833e-4575-a1e9-867c9421b2a9"
                }
            ]
        },
        {
            "id": "6a330c8a-c730-43c5-a4be-4ca1345b27c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7da6f2a6-97c7-40a0-8f30-3398cc27732b",
            "compositeImage": {
                "id": "d1d24e67-bbae-4f60-b673-e770d211872b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a330c8a-c730-43c5-a4be-4ca1345b27c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75a7fc97-c20b-4fba-857a-83f87a4f0044",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a330c8a-c730-43c5-a4be-4ca1345b27c4",
                    "LayerId": "b8e4c42a-833e-4575-a1e9-867c9421b2a9"
                }
            ]
        },
        {
            "id": "06c1a243-46ac-427b-9037-aaf80f4be801",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7da6f2a6-97c7-40a0-8f30-3398cc27732b",
            "compositeImage": {
                "id": "c4324644-8302-4ef0-b426-9167ad754b37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06c1a243-46ac-427b-9037-aaf80f4be801",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a6755e2-581a-4088-a143-bab9d7d14a27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06c1a243-46ac-427b-9037-aaf80f4be801",
                    "LayerId": "b8e4c42a-833e-4575-a1e9-867c9421b2a9"
                }
            ]
        },
        {
            "id": "a7e97a5f-5d02-448d-90d3-eedeb181d506",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7da6f2a6-97c7-40a0-8f30-3398cc27732b",
            "compositeImage": {
                "id": "655e4ec7-5369-41aa-a6f4-8d0555d7ccb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7e97a5f-5d02-448d-90d3-eedeb181d506",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93a145f7-83a5-4997-93c2-3a633cc2780d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7e97a5f-5d02-448d-90d3-eedeb181d506",
                    "LayerId": "b8e4c42a-833e-4575-a1e9-867c9421b2a9"
                }
            ]
        },
        {
            "id": "5f407460-485b-41ed-81f7-9729d42c23a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7da6f2a6-97c7-40a0-8f30-3398cc27732b",
            "compositeImage": {
                "id": "63039597-7909-4945-a141-b6bfcf01795e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f407460-485b-41ed-81f7-9729d42c23a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc382016-b3fa-4d2d-9bb0-aab8377b0927",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f407460-485b-41ed-81f7-9729d42c23a2",
                    "LayerId": "b8e4c42a-833e-4575-a1e9-867c9421b2a9"
                }
            ]
        },
        {
            "id": "248a7a57-c296-48cb-affd-3ed9c890d424",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7da6f2a6-97c7-40a0-8f30-3398cc27732b",
            "compositeImage": {
                "id": "afcca199-67eb-4b46-b920-0f873b80f9ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "248a7a57-c296-48cb-affd-3ed9c890d424",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16ef6324-8a20-41f5-bb76-0a6cb11ba830",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "248a7a57-c296-48cb-affd-3ed9c890d424",
                    "LayerId": "b8e4c42a-833e-4575-a1e9-867c9421b2a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "b8e4c42a-833e-4575-a1e9-867c9421b2a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7da6f2a6-97c7-40a0-8f30-3398cc27732b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}