{
    "id": "0f9f4b3c-6df7-47a8-9a4d-908f1f6d3984",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_charselect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8b112c9-6c8b-44a5-b74e-523770760d06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f9f4b3c-6df7-47a8-9a4d-908f1f6d3984",
            "compositeImage": {
                "id": "a3c59b18-51fa-49fb-b84c-1f3eeaa37371",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8b112c9-6c8b-44a5-b74e-523770760d06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1117dc4-c156-4449-82ed-4faa94857870",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8b112c9-6c8b-44a5-b74e-523770760d06",
                    "LayerId": "031da8fa-bd78-4177-9a20-a0911d239882"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "031da8fa-bd78-4177-9a20-a0911d239882",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f9f4b3c-6df7-47a8-9a4d-908f1f6d3984",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 0,
    "yorig": 0
}