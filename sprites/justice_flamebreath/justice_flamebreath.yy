{
    "id": "f116c327-1b7e-4b37-8c43-b707a618dc40",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "justice_flamebreath",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 145,
    "bbox_left": 48,
    "bbox_right": 147,
    "bbox_top": 73,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "38650128-de7c-4dd6-9711-34fe628b1ed8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "4881b79b-a508-4d17-ba86-d70733508e6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38650128-de7c-4dd6-9711-34fe628b1ed8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bba88e59-a7ee-4a60-b309-1269b9cd4338",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38650128-de7c-4dd6-9711-34fe628b1ed8",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "823c0589-1ebc-4b8a-b87d-0faa6f8d980c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "596479df-d37c-495e-8246-cac7eef081d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "823c0589-1ebc-4b8a-b87d-0faa6f8d980c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58e99fb4-2509-4123-9dd7-873e46a1419f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "823c0589-1ebc-4b8a-b87d-0faa6f8d980c",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "e7374818-9cd0-4e66-98c6-2bf825d87992",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "fcdf5d48-ca87-480c-a967-845aad114c51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7374818-9cd0-4e66-98c6-2bf825d87992",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff42ccf2-52b6-4e78-8c5f-1ff9755287a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7374818-9cd0-4e66-98c6-2bf825d87992",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "e3061d1e-1790-476f-afac-7d0d33f7713b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "ff6481bb-3977-4931-8eb4-5093b9a84d52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3061d1e-1790-476f-afac-7d0d33f7713b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd5ddd53-0aac-47a5-b4c9-40ee4d51e822",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3061d1e-1790-476f-afac-7d0d33f7713b",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "36af8688-0f9d-4650-bf55-ceed36641be8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "277195d1-a6cf-40fb-b14d-da51f93e0f89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36af8688-0f9d-4650-bf55-ceed36641be8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e22a4273-cd75-4d8d-962b-a86dd88472c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36af8688-0f9d-4650-bf55-ceed36641be8",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "fa0629fa-cd9b-4960-8db8-40b7891e446c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "afaa8a1f-619e-4217-b651-b3f8d055034e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa0629fa-cd9b-4960-8db8-40b7891e446c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50111e57-ac5c-4d57-8f03-5b84c6a73e13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa0629fa-cd9b-4960-8db8-40b7891e446c",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "638bd1cc-b57e-4cf2-be20-8e933c8a1640",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "fd736c87-af66-4600-9717-07d38bea7f12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "638bd1cc-b57e-4cf2-be20-8e933c8a1640",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f02a6839-2eda-46b2-ba19-413865383905",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "638bd1cc-b57e-4cf2-be20-8e933c8a1640",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "9bf8dcc7-fe2d-4b08-aaa1-83bba0e03427",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "ec41dde4-a36d-461c-b031-f90f98bade6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bf8dcc7-fe2d-4b08-aaa1-83bba0e03427",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0354b9d-ae77-4f59-a620-fed49013bc2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bf8dcc7-fe2d-4b08-aaa1-83bba0e03427",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "c4fdb06d-6815-445d-9fb8-a610e95f5333",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "2222aedf-b13f-4d04-b6b8-0780dbc0dcd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4fdb06d-6815-445d-9fb8-a610e95f5333",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ed808b4-d982-41fe-a570-0a1da68fac17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4fdb06d-6815-445d-9fb8-a610e95f5333",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "079e5fa4-0f42-4775-bab1-fa8e5df53805",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "1826bd88-50d9-475f-b62d-5b25cd1f32a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "079e5fa4-0f42-4775-bab1-fa8e5df53805",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41dfa84e-e5ed-4ae9-8d06-01b307f5d6c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "079e5fa4-0f42-4775-bab1-fa8e5df53805",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "976d6f54-5d20-4cc3-8a25-c1553b695f76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "7bcb8f69-099e-434c-81a4-c2b5a48e5ad6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "976d6f54-5d20-4cc3-8a25-c1553b695f76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "656cf75d-f53c-41d1-9b1d-009785bd69b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "976d6f54-5d20-4cc3-8a25-c1553b695f76",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "a7ca138f-faa8-4261-a32e-948e67afb6df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "a2ec30ba-a6b1-4663-8973-971a232c8cc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7ca138f-faa8-4261-a32e-948e67afb6df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8534211-af22-40eb-b545-3a64d4516314",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7ca138f-faa8-4261-a32e-948e67afb6df",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "fa627222-7534-40ca-961b-e41a58889354",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "42e01572-5b9b-4493-8c28-c6c278524594",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa627222-7534-40ca-961b-e41a58889354",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "890c9b55-6016-4000-82d3-eb594e31d535",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa627222-7534-40ca-961b-e41a58889354",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "a3d81b30-362d-460f-aa63-e6d2f6c6b761",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "dd4ace6a-8942-4d72-b35c-7a768b03fa5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3d81b30-362d-460f-aa63-e6d2f6c6b761",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "745ded9a-b17f-4448-bd84-e7e44913fda1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3d81b30-362d-460f-aa63-e6d2f6c6b761",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "82e51234-50b7-4281-bc3a-22f6adda5ee2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "919fd2a7-be90-46d6-80e1-3ee443c5c0da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82e51234-50b7-4281-bc3a-22f6adda5ee2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b43562b-36de-45e4-822a-8ee4df7c38f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82e51234-50b7-4281-bc3a-22f6adda5ee2",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "2ad1c2fe-8bad-4385-bcb6-2e7c1817435a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "16fd4b0b-32ee-44a8-bbee-2d705ca36fa6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ad1c2fe-8bad-4385-bcb6-2e7c1817435a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65576155-409a-4b0c-a286-1cf0a5aa627d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ad1c2fe-8bad-4385-bcb6-2e7c1817435a",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "c96db455-5e68-433b-9057-4792f050fef8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "542ef281-32e4-48dc-b41e-2fccdcdf3df5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c96db455-5e68-433b-9057-4792f050fef8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b85632a-4c6c-4f3d-8d48-4be1760df116",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c96db455-5e68-433b-9057-4792f050fef8",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "22c4f786-07be-409b-aa3c-e0adc52f8f34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "50d601bb-26c1-45bd-b56e-959f91978878",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22c4f786-07be-409b-aa3c-e0adc52f8f34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db4d9c59-2339-4cc2-8430-b566d3e90567",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22c4f786-07be-409b-aa3c-e0adc52f8f34",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "36bdcc5b-7676-461b-9d6a-32c50be51758",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "b89190e1-2099-4c24-8f33-272be0047b39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36bdcc5b-7676-461b-9d6a-32c50be51758",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdec8572-f335-4426-ab45-15cc76a0d1be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36bdcc5b-7676-461b-9d6a-32c50be51758",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "62fab460-310c-4c6b-93c5-96abedae6c07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "d7075a53-fa4e-4f8b-b503-352fbf60910c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62fab460-310c-4c6b-93c5-96abedae6c07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54d13e20-858f-47f8-b9fa-2aeb39f3aca6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62fab460-310c-4c6b-93c5-96abedae6c07",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "d8cefc4b-a8fa-4a8c-b09c-cb4483a97c54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "99cba698-e402-48ce-abe3-41f8b42c5ced",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8cefc4b-a8fa-4a8c-b09c-cb4483a97c54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb989661-3db7-4652-aeea-c2e02c6458fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8cefc4b-a8fa-4a8c-b09c-cb4483a97c54",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "ef0a881a-b23a-48f8-ae03-5e8aa99a7a69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "c498e0ff-8c4d-4aeb-823e-a94545f54c96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef0a881a-b23a-48f8-ae03-5e8aa99a7a69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cae6e15e-f11d-4e48-aef9-aa0badb5b408",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef0a881a-b23a-48f8-ae03-5e8aa99a7a69",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "9a508535-1d2c-4661-8c76-2e00cd762e80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "336f8513-7afb-479f-8e1d-12ffba0be2ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a508535-1d2c-4661-8c76-2e00cd762e80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fe56e83-de44-4717-8407-abf13059b933",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a508535-1d2c-4661-8c76-2e00cd762e80",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "130d603a-246c-4a64-9920-6b1ec57f98d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "9103a318-9099-4b46-afe4-386921841953",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "130d603a-246c-4a64-9920-6b1ec57f98d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2baf4aa-4c4a-41d0-bc74-1dfe0eff2127",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "130d603a-246c-4a64-9920-6b1ec57f98d7",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "08ab49f6-64a7-4ac2-918a-7c49dbbe49f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "4d5439a6-c638-4ed2-aade-9f8708711461",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08ab49f6-64a7-4ac2-918a-7c49dbbe49f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f561048-ea1a-42eb-bfd5-70e2e9cfb0c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08ab49f6-64a7-4ac2-918a-7c49dbbe49f0",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "1350e90a-f5cf-4cee-8466-fdf289df346e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "47f42b4e-adb2-4d3c-a06d-f4fcdd1d5624",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1350e90a-f5cf-4cee-8466-fdf289df346e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16887dd6-9537-44c1-b4f9-f604a2b392a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1350e90a-f5cf-4cee-8466-fdf289df346e",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "54cfeb45-73db-451d-b65b-fa2c8fb7bb42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "10001c01-482b-443c-be46-81c34a04d8e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54cfeb45-73db-451d-b65b-fa2c8fb7bb42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee96f9f6-ee99-48a6-b213-6e648f0091f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54cfeb45-73db-451d-b65b-fa2c8fb7bb42",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "8e8d8f47-fb05-4e2f-862c-5ef9d415994b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "d106f903-07c5-460b-ac17-a85f66a85592",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e8d8f47-fb05-4e2f-862c-5ef9d415994b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10e22248-849a-44b3-bf1b-7ba1955b0720",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e8d8f47-fb05-4e2f-862c-5ef9d415994b",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "437ec8ce-de88-4f7b-ba04-322430261ae3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "d2405dcb-cb66-43c4-bc86-f851f914f512",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "437ec8ce-de88-4f7b-ba04-322430261ae3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55f4e3a8-4f88-431c-97ca-3fcae3e6995d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "437ec8ce-de88-4f7b-ba04-322430261ae3",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "d7fd16b9-9108-407a-841f-f2792610222b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "d87f45e1-4658-4a71-b5e6-192fddbfa407",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7fd16b9-9108-407a-841f-f2792610222b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa063b90-6808-42ed-933d-3a21a081aa20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7fd16b9-9108-407a-841f-f2792610222b",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "3ee0e80b-3c0f-4380-95eb-cf075eca7542",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "2dbfe651-e28f-4151-afea-89e25e74a5a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ee0e80b-3c0f-4380-95eb-cf075eca7542",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c552a0f8-82d8-4643-a596-fd2c88afb14b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ee0e80b-3c0f-4380-95eb-cf075eca7542",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "b3b47317-e7eb-41dd-b943-26af890db9c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "44707e30-57de-45a5-8939-ea032868c030",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3b47317-e7eb-41dd-b943-26af890db9c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "812d18f0-dc20-47ae-9d54-ff3f61813a85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3b47317-e7eb-41dd-b943-26af890db9c7",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "085915c6-396a-421c-964a-ecdc6a1467cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "6978367e-b297-41f1-bcd9-03628efa3f10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "085915c6-396a-421c-964a-ecdc6a1467cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ddf48d9-06d5-4fa3-a531-329f4e9737d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "085915c6-396a-421c-964a-ecdc6a1467cb",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "a8018091-4804-4f35-94b0-44105a87a3fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "639d16fd-7174-43a2-bd82-c13f26826f36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8018091-4804-4f35-94b0-44105a87a3fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f925ac1-6573-4b21-8d73-306e351eaf30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8018091-4804-4f35-94b0-44105a87a3fc",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "0b587f49-6df3-4a8e-bbf8-62246e19d123",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "cec23a72-f222-42a5-9ba1-efbc52238db6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b587f49-6df3-4a8e-bbf8-62246e19d123",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3ff2fc0-4981-4ef8-a053-a53266a5514a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b587f49-6df3-4a8e-bbf8-62246e19d123",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "f63fd3c7-a975-4fc3-b231-7b3505ab04cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "0ee4c261-1007-45bd-bbb8-833cd9c8a68a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f63fd3c7-a975-4fc3-b231-7b3505ab04cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4a1a94e-6166-48a7-a017-7151703000a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f63fd3c7-a975-4fc3-b231-7b3505ab04cc",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "500f50df-2788-4b9a-b05d-6d7d14047586",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "14da9f11-2a1a-4baf-a024-834c39927d98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "500f50df-2788-4b9a-b05d-6d7d14047586",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea4bc1ff-8375-41a7-9b79-269bb57bf338",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "500f50df-2788-4b9a-b05d-6d7d14047586",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "84140752-bd4e-4c75-b473-bff862775e41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "18a6486a-352a-4a08-a090-dedc8ede3544",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84140752-bd4e-4c75-b473-bff862775e41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf30024e-7830-4e63-aa1f-2f948451db3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84140752-bd4e-4c75-b473-bff862775e41",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "9358eda5-2c08-4aa6-add5-f5ed1415982c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "d97fb86d-c019-4325-9778-03a7514fdd40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9358eda5-2c08-4aa6-add5-f5ed1415982c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6022b209-958e-4bf5-a1f6-3062262664c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9358eda5-2c08-4aa6-add5-f5ed1415982c",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "1384f30c-dbf4-4f6c-8dba-260f7b5548db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "2206e491-1df5-4133-8ce1-a3d2082b5118",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1384f30c-dbf4-4f6c-8dba-260f7b5548db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc1f607b-4628-42cb-8ca8-ede12878f534",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1384f30c-dbf4-4f6c-8dba-260f7b5548db",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "31ee49e8-4f1d-4531-a5be-a9bd1dcfcd98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "bb57b9fd-813c-4a04-a0ef-05abceacc6e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31ee49e8-4f1d-4531-a5be-a9bd1dcfcd98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "634e4ff0-6113-4682-9148-661ae43333ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31ee49e8-4f1d-4531-a5be-a9bd1dcfcd98",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "3cffa9ff-a31c-42ad-909b-f3bffaf82950",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "a0c94132-3a2d-4cf3-a2dd-8943545a950c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cffa9ff-a31c-42ad-909b-f3bffaf82950",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d791e54-51ed-46e7-9cf7-8a6b1125d268",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cffa9ff-a31c-42ad-909b-f3bffaf82950",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "07eb8ed2-8fd8-4b68-91bf-9b5e66592f3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "f96ab67c-6188-4e03-a357-54ebdc616261",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07eb8ed2-8fd8-4b68-91bf-9b5e66592f3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f94abbdc-b5b3-4ed3-8fda-15131dcfc4ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07eb8ed2-8fd8-4b68-91bf-9b5e66592f3c",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "cb546226-9357-4d50-819a-ee2816b197a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "d0109dec-cc7e-4d05-90fe-d175c75b4161",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb546226-9357-4d50-819a-ee2816b197a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48ad27ec-65b4-49b4-9cea-96e91b3b901f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb546226-9357-4d50-819a-ee2816b197a1",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "e8ecb49a-afcb-4014-924b-b36a5b6d42be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "1d326ac8-2643-431a-9b7c-1dde8fbf3e3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8ecb49a-afcb-4014-924b-b36a5b6d42be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f11683ee-b419-4895-9b76-e02747d2ff35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8ecb49a-afcb-4014-924b-b36a5b6d42be",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "623de60c-9abb-43a3-bdc0-63b73dd96f00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "c13e1380-b9fe-4df6-8074-df0e2c0f9c9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "623de60c-9abb-43a3-bdc0-63b73dd96f00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dcbd807-88ff-4a8e-be79-d4dc78c56048",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "623de60c-9abb-43a3-bdc0-63b73dd96f00",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "56811eab-5516-49c1-bbac-399d38844cd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "bfaedf65-1f2a-48d5-9f3b-d4cf3a36a4c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56811eab-5516-49c1-bbac-399d38844cd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7011b8c-ce63-476a-9a3b-e712281bcdbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56811eab-5516-49c1-bbac-399d38844cd2",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "177e19e9-2ee9-45bc-bce6-88cf5d5a9d24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "43c1ca60-ccc1-48db-9370-e8b37af02479",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "177e19e9-2ee9-45bc-bce6-88cf5d5a9d24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe2e173c-6c76-4d13-a2be-c1f504902bd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "177e19e9-2ee9-45bc-bce6-88cf5d5a9d24",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "77dcf423-5cf4-4150-b72a-119ac4ce5a71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "ba1402ee-66c8-44e8-a5e6-4bbfe8f49d33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77dcf423-5cf4-4150-b72a-119ac4ce5a71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7de8bc7a-6235-46ed-a67d-15b673e966de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77dcf423-5cf4-4150-b72a-119ac4ce5a71",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "f3618bd9-a676-4148-b0f9-1c4a3582b6e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "9fb749bd-f21c-4390-b6c3-0902732a45cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3618bd9-a676-4148-b0f9-1c4a3582b6e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73bbcd5d-ef8c-4df8-b9bc-2c0f0e35bbec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3618bd9-a676-4148-b0f9-1c4a3582b6e1",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "3acaed67-f0d7-4173-9110-b7c59ac85078",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "2df04aec-da88-4d05-b07b-3b119c923f49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3acaed67-f0d7-4173-9110-b7c59ac85078",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84ef1c16-36b8-44c6-bb95-fe5379ebad40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3acaed67-f0d7-4173-9110-b7c59ac85078",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "4bb86cd7-abf4-4213-8a48-a680d7f4c577",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "5f18303e-2217-4d5f-a65b-b572798550d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bb86cd7-abf4-4213-8a48-a680d7f4c577",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3eb1cf33-25a7-480c-9dee-5918bf4798f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bb86cd7-abf4-4213-8a48-a680d7f4c577",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "63139020-da8e-45f8-95b5-ed7713cc00ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "ef02f685-9060-41f3-9beb-d73ee2915390",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63139020-da8e-45f8-95b5-ed7713cc00ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99f7a426-c8ef-4843-90fc-3e844dbd3211",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63139020-da8e-45f8-95b5-ed7713cc00ca",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "5d6b3e6f-cc88-43e2-835f-01f6a2633de5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "65548fcb-01f2-475b-b391-d7583abc8ba6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d6b3e6f-cc88-43e2-835f-01f6a2633de5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f20a783-d403-4e9c-8593-a67d95c7a6f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d6b3e6f-cc88-43e2-835f-01f6a2633de5",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "3ea9495b-4286-46a7-8fd3-89a00ae90e7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "224ba0f7-5d60-459d-999c-c8289bb86557",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ea9495b-4286-46a7-8fd3-89a00ae90e7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e8ecf13-5b88-4888-ba8b-432b5074f474",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ea9495b-4286-46a7-8fd3-89a00ae90e7a",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "1657cf14-52ea-4680-a93d-028ddb2bdf1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "59d20825-9f8c-4912-9eb0-a5ecc8399166",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1657cf14-52ea-4680-a93d-028ddb2bdf1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad0b06f1-9a44-484e-949e-0408c801f61c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1657cf14-52ea-4680-a93d-028ddb2bdf1b",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "ce8c41e8-dfe4-4e4b-9f43-52c3d3bb22dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "6a9026a3-1e66-4d96-a911-7f8f02fe5ed0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce8c41e8-dfe4-4e4b-9f43-52c3d3bb22dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c80eb11-34b0-405c-9127-d0d03fe9cedc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce8c41e8-dfe4-4e4b-9f43-52c3d3bb22dd",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "1db71907-be7e-4bdc-8263-398857da8601",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "73425bfd-18cb-4c20-bbc1-70f6796845d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1db71907-be7e-4bdc-8263-398857da8601",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19328b5a-9b8f-4e15-909b-98e9c3d7bdd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1db71907-be7e-4bdc-8263-398857da8601",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "11cb7111-5f58-4d2e-9de9-e902f8255289",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "0183e695-a38b-4c2f-ac04-b4d039bd8185",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11cb7111-5f58-4d2e-9de9-e902f8255289",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f237b104-cb17-4356-9c1d-b3faa0acbef3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11cb7111-5f58-4d2e-9de9-e902f8255289",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        },
        {
            "id": "7b320ec0-2329-44e7-94ad-0a4848dbb539",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "compositeImage": {
                "id": "7d788b5f-6510-496c-9b04-832bc5aa6b3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b320ec0-2329-44e7-94ad-0a4848dbb539",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9e66d30-3d6a-4874-a824-d6f52f8242d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b320ec0-2329-44e7-94ad-0a4848dbb539",
                    "LayerId": "467710ba-f047-4422-b6bc-bc3b2c4618e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "467710ba-f047-4422-b6bc-bc3b2c4618e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f116c327-1b7e-4b37-8c43-b707a618dc40",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 79,
    "yorig": 121
}