/// @description Insert description here
// You can write your code in this editor
event_inherited();

state_machine_init();

state_create("stand",state_syn_stand);
state_create("walk",state_syn_walk);
state_create("fdash",state_syn_fdash);
state_create("bdash",state_syn_bdash);
state_create("crouch",state_syn_crouch);
state_create("jumpsquat",state_syn_jumpsquat);
state_create("air",state_syn_air);
state_create("airdash",state_syn_airdash);
state_create("hitstun",state_syn_hitstun);
state_create("hitfall",state_syn_hitfall);
state_create("bounce",state_syn_bounce);
state_create("knockdown",state_syn_knockdown);
state_create("stagger",state_syn_stagger);
state_create("recovery",state_syn_recovery);
state_create("sblock",state_syn_sblock);
state_create("cblock",state_syn_cblock);
state_create("ablock",state_syn_ablock);
state_create("light",state_syn_light);
state_create("clight",state_syn_clight);
state_create("medium",state_syn_medium);
state_create("cmedium",state_syn_cmedium);
state_create("dmedium",state_syn_dmedium);
state_create("heavy",state_syn_heavy);
state_create("cheavy",state_syn_cheavy);
state_create("dheavy",state_syn_dheavy);
state_create("qcbl",state_syn_qcbl);
state_create("qcbm",state_syn_qcbm);
state_create("qcbh",state_syn_qcbh);
state_create("hcbl",state_syn_hcbl);
state_create("hcbm",state_syn_hcbm);
state_create("hcbh",state_syn_hcbh);
state_create("qcfl",state_syn_qcfl);
state_create("qcfm",state_syn_qcfm);
state_create("qcfh",state_syn_qcfh);
state_create("srkl",state_syn_srkl);
state_create("srkm",state_syn_srkm);
state_create("srkh",state_syn_srkh);
state_create("dqcf",state_syn_dqcf);
state_create("grab",state_syn_grab);
state_create("lair",state_syn_lair);
state_create("mair",state_syn_mair);
state_create("hair",state_syn_hair);
state_create("aqcfl",state_syn_aqcfl);
state_create("aqcfm",state_syn_aqcfm);
state_create("aqcfh",state_syn_aqcfh);
state_create("aqcbl",state_syn_aqcbl);
state_create("aqcbm",state_syn_aqcbm);
state_create("aqcbh",state_syn_aqcbh);
state_create("agrab",state_syn_agrab);
state_create("bthrow",state_syn_bthrow);
state_create("fthrow",state_syn_fthrow);
state_create("athrow",state_syn_athrow);
state_create("grabbed",state_syn_grabbed);
state_create("grabtech",state_syn_grabtech);
state_create("geniemove",state_syn_geniemove);
state_create("landing",state_syn_landing);
state_create("transform",state_syn_transform);

state_init("stand");


ground_accel = 0.85;
air_accel = 0.12;
ground_fric = 0.9;
air_fric = 0.5;
grav = 0.75;
jumpheight = 10;
vxMax = 3;
vyMax = 7;
doublejump = 1;
dashdir = 0;

myname = "syn";

hurtbox0 = create_hurtbox(-13,-15,26,57,-1,0);
hurtbox1 = create_hurtbox(-7,-33,14,18,-1,0);

collisionbox = create_collisionbox(-5,-12,16,41,-2);

hurt_spriteG = spr_bill_hitstun_g;
hurt_spriteA = spr_bill_hitstun_a;
fall_sprite = spr_bill_hitfall;
knock_sprite = spr_bill_knockdown;
idle_sprite = spr_dsyn_idle;

hpmax = 190;
hp = hpmax;

//syndikate specific
synmode = -1800;

qcf_buffer = 0;
qcb_buffer = 0;
hcb_buffer = 0;
srk_buffer = 0;
dqcf_buffer = 0;
bb_buffer = 0;
ff_buffer = 0;

mask_index = spr_lsyn_idle;