{
    "id": "870e6763-be0d-4564-926d-0127d5654d63",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lsyn_srkx",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 49,
    "bbox_right": 91,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9b11739-5c40-4d65-91ba-41cc387ba7db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870e6763-be0d-4564-926d-0127d5654d63",
            "compositeImage": {
                "id": "2ba460a0-e2c9-493b-be33-5a9e8ac21fa2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9b11739-5c40-4d65-91ba-41cc387ba7db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84e28705-cf69-4922-b0f9-a9aca44c1d02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9b11739-5c40-4d65-91ba-41cc387ba7db",
                    "LayerId": "4b5afac0-3dd8-4960-9a52-bf97011cbab2"
                }
            ]
        },
        {
            "id": "46c202b6-4963-4a3f-b7ee-c61bdbdb90d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870e6763-be0d-4564-926d-0127d5654d63",
            "compositeImage": {
                "id": "70b59154-11a5-4a9e-92b6-3186f774f079",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46c202b6-4963-4a3f-b7ee-c61bdbdb90d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19bdbc4b-8011-42ef-8a6b-aad1fa57535d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46c202b6-4963-4a3f-b7ee-c61bdbdb90d1",
                    "LayerId": "4b5afac0-3dd8-4960-9a52-bf97011cbab2"
                }
            ]
        },
        {
            "id": "16056ed0-d807-4f15-b858-3d52302d89de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870e6763-be0d-4564-926d-0127d5654d63",
            "compositeImage": {
                "id": "8076ccf6-b5bd-44d3-808b-681ddd5cdfd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16056ed0-d807-4f15-b858-3d52302d89de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0518be46-5d0d-44a1-8906-97ce8d303345",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16056ed0-d807-4f15-b858-3d52302d89de",
                    "LayerId": "4b5afac0-3dd8-4960-9a52-bf97011cbab2"
                }
            ]
        },
        {
            "id": "7e20a98b-3a6e-4096-9e6a-7e5b89b584cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870e6763-be0d-4564-926d-0127d5654d63",
            "compositeImage": {
                "id": "1c1f22c9-62ff-4a11-b12e-62d4f01a9692",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e20a98b-3a6e-4096-9e6a-7e5b89b584cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f973042a-b9df-49ba-881b-6b40c15c4874",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e20a98b-3a6e-4096-9e6a-7e5b89b584cc",
                    "LayerId": "4b5afac0-3dd8-4960-9a52-bf97011cbab2"
                }
            ]
        },
        {
            "id": "a22a5dee-60b1-4b24-80cf-5374522d5c4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870e6763-be0d-4564-926d-0127d5654d63",
            "compositeImage": {
                "id": "c2e16a5d-4712-431e-86f4-d136b072c2d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a22a5dee-60b1-4b24-80cf-5374522d5c4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4f5ecc2-e293-4d46-8e04-ff4f0272fdf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a22a5dee-60b1-4b24-80cf-5374522d5c4d",
                    "LayerId": "4b5afac0-3dd8-4960-9a52-bf97011cbab2"
                }
            ]
        },
        {
            "id": "449efe4f-64ce-414d-8368-18622ff984d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870e6763-be0d-4564-926d-0127d5654d63",
            "compositeImage": {
                "id": "32f12c6b-aa68-4d6c-9df7-7d2a4bfc70c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "449efe4f-64ce-414d-8368-18622ff984d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adefd5a6-766c-40a2-9d54-d29e6001faab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "449efe4f-64ce-414d-8368-18622ff984d1",
                    "LayerId": "4b5afac0-3dd8-4960-9a52-bf97011cbab2"
                }
            ]
        },
        {
            "id": "33e79586-3d4f-41bc-a78a-69cabfb147cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870e6763-be0d-4564-926d-0127d5654d63",
            "compositeImage": {
                "id": "2341ceda-5a2b-4dbc-b77b-57295fd81e02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33e79586-3d4f-41bc-a78a-69cabfb147cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad26372b-cc56-49e4-bde1-238c28eaa061",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33e79586-3d4f-41bc-a78a-69cabfb147cb",
                    "LayerId": "4b5afac0-3dd8-4960-9a52-bf97011cbab2"
                }
            ]
        },
        {
            "id": "5f02e777-df23-43d3-968b-2d3cded9e250",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870e6763-be0d-4564-926d-0127d5654d63",
            "compositeImage": {
                "id": "4e230235-609b-48ff-b0a6-6061d17efec5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f02e777-df23-43d3-968b-2d3cded9e250",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef0a56a4-f5d0-4936-9dfd-d8d313ddc844",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f02e777-df23-43d3-968b-2d3cded9e250",
                    "LayerId": "4b5afac0-3dd8-4960-9a52-bf97011cbab2"
                }
            ]
        },
        {
            "id": "b76b8281-7550-4f56-853a-cc3984e14c83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870e6763-be0d-4564-926d-0127d5654d63",
            "compositeImage": {
                "id": "46746cc1-b029-4fca-a9be-166e9582635e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b76b8281-7550-4f56-853a-cc3984e14c83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2f676dc-5201-4e82-a59d-d0345e337dd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b76b8281-7550-4f56-853a-cc3984e14c83",
                    "LayerId": "4b5afac0-3dd8-4960-9a52-bf97011cbab2"
                }
            ]
        },
        {
            "id": "363f743a-21b2-4323-bc0f-987c100e763a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870e6763-be0d-4564-926d-0127d5654d63",
            "compositeImage": {
                "id": "5c578487-bd80-473f-8b94-f3faa2d69d46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "363f743a-21b2-4323-bc0f-987c100e763a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65ffeee3-f53e-4b78-ab30-ff627dcbc86f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "363f743a-21b2-4323-bc0f-987c100e763a",
                    "LayerId": "4b5afac0-3dd8-4960-9a52-bf97011cbab2"
                }
            ]
        },
        {
            "id": "c11b7b6e-ab0e-473d-8753-7636cbae0b03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870e6763-be0d-4564-926d-0127d5654d63",
            "compositeImage": {
                "id": "d7910e07-09ab-4e29-b5d8-003b64c0a7e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c11b7b6e-ab0e-473d-8753-7636cbae0b03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7eebf7ac-0c6d-494f-b113-360e6fab367f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c11b7b6e-ab0e-473d-8753-7636cbae0b03",
                    "LayerId": "4b5afac0-3dd8-4960-9a52-bf97011cbab2"
                }
            ]
        },
        {
            "id": "f299fa51-d736-4f54-980b-d08ec8c6734a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870e6763-be0d-4564-926d-0127d5654d63",
            "compositeImage": {
                "id": "d4dbb916-8d22-4454-b2cc-0a26f063f91c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f299fa51-d736-4f54-980b-d08ec8c6734a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88e8b813-b115-4e2f-87ce-31ec9cfc188f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f299fa51-d736-4f54-980b-d08ec8c6734a",
                    "LayerId": "4b5afac0-3dd8-4960-9a52-bf97011cbab2"
                }
            ]
        },
        {
            "id": "49e78b5a-a15d-47c4-bb0b-70eba80732db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870e6763-be0d-4564-926d-0127d5654d63",
            "compositeImage": {
                "id": "91b9e748-6f4d-42a5-804a-56d43180ab4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49e78b5a-a15d-47c4-bb0b-70eba80732db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b29884e-6a00-4bd6-addc-90f864776fe5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49e78b5a-a15d-47c4-bb0b-70eba80732db",
                    "LayerId": "4b5afac0-3dd8-4960-9a52-bf97011cbab2"
                }
            ]
        },
        {
            "id": "89aa2aeb-37dc-418e-92ae-2a4775341527",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870e6763-be0d-4564-926d-0127d5654d63",
            "compositeImage": {
                "id": "7cf79138-9eb6-4587-b15e-97bb9d3b3ee9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89aa2aeb-37dc-418e-92ae-2a4775341527",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db1a71e7-d35b-4fe2-82ff-f013ddea9d86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89aa2aeb-37dc-418e-92ae-2a4775341527",
                    "LayerId": "4b5afac0-3dd8-4960-9a52-bf97011cbab2"
                }
            ]
        },
        {
            "id": "7286c083-ea87-47f0-a307-822958804c5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870e6763-be0d-4564-926d-0127d5654d63",
            "compositeImage": {
                "id": "f9fc054d-9853-49f4-8b75-4c922e675079",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7286c083-ea87-47f0-a307-822958804c5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88806f5d-9123-4846-b2f1-643a9c07e69a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7286c083-ea87-47f0-a307-822958804c5d",
                    "LayerId": "4b5afac0-3dd8-4960-9a52-bf97011cbab2"
                }
            ]
        },
        {
            "id": "0b7b2bf5-1531-4e88-93c4-711437e713ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870e6763-be0d-4564-926d-0127d5654d63",
            "compositeImage": {
                "id": "a03fca36-da8d-49c6-8dc2-34e8e934b15d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b7b2bf5-1531-4e88-93c4-711437e713ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aef80416-bbd5-4fea-817b-848686d156e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b7b2bf5-1531-4e88-93c4-711437e713ce",
                    "LayerId": "4b5afac0-3dd8-4960-9a52-bf97011cbab2"
                }
            ]
        },
        {
            "id": "d0f17d34-c0bd-4934-af8d-2362500bba2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870e6763-be0d-4564-926d-0127d5654d63",
            "compositeImage": {
                "id": "8bd744a6-201c-4fdf-b096-3e74bf3011ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0f17d34-c0bd-4934-af8d-2362500bba2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5865574a-a5c2-4553-9f10-4656f024f417",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0f17d34-c0bd-4934-af8d-2362500bba2d",
                    "LayerId": "4b5afac0-3dd8-4960-9a52-bf97011cbab2"
                }
            ]
        },
        {
            "id": "eb3a1e88-638c-4920-ab6d-67668f56e6a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870e6763-be0d-4564-926d-0127d5654d63",
            "compositeImage": {
                "id": "6dc8c5e9-2690-4285-8911-39640056345a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb3a1e88-638c-4920-ab6d-67668f56e6a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7afe57d9-39a5-4ae0-a7a9-b89af0dd1de0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb3a1e88-638c-4920-ab6d-67668f56e6a0",
                    "LayerId": "4b5afac0-3dd8-4960-9a52-bf97011cbab2"
                }
            ]
        },
        {
            "id": "7f61e29d-35ae-4676-93b4-7a0f658c8956",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870e6763-be0d-4564-926d-0127d5654d63",
            "compositeImage": {
                "id": "dc062eba-e9a7-4ee4-9e05-8899f26b52fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f61e29d-35ae-4676-93b4-7a0f658c8956",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53d0ddef-6467-4081-9c7a-d467e7ca7b18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f61e29d-35ae-4676-93b4-7a0f658c8956",
                    "LayerId": "4b5afac0-3dd8-4960-9a52-bf97011cbab2"
                }
            ]
        },
        {
            "id": "c05dd211-20b0-4e02-bfbf-4f29e78a1150",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870e6763-be0d-4564-926d-0127d5654d63",
            "compositeImage": {
                "id": "61a9dd52-32b9-4e49-9f69-3a7716303ccb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c05dd211-20b0-4e02-bfbf-4f29e78a1150",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da8e15a1-bc23-4c74-b92c-b19bcdcb7cb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c05dd211-20b0-4e02-bfbf-4f29e78a1150",
                    "LayerId": "4b5afac0-3dd8-4960-9a52-bf97011cbab2"
                }
            ]
        },
        {
            "id": "841a0437-e6d1-4f31-9576-f7504fb1fc3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870e6763-be0d-4564-926d-0127d5654d63",
            "compositeImage": {
                "id": "9247d1cb-28e0-4290-8e6e-8c6d015d2530",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "841a0437-e6d1-4f31-9576-f7504fb1fc3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "155af824-4af0-416b-973b-b71b46367667",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "841a0437-e6d1-4f31-9576-f7504fb1fc3b",
                    "LayerId": "4b5afac0-3dd8-4960-9a52-bf97011cbab2"
                }
            ]
        },
        {
            "id": "d1d71f43-2061-4df5-8863-089a34996f49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870e6763-be0d-4564-926d-0127d5654d63",
            "compositeImage": {
                "id": "1cbe15f9-92d9-47a4-a6d3-f86eef6c0a06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1d71f43-2061-4df5-8863-089a34996f49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47ac6be6-7e8c-4376-8d99-52ed79dd1d53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1d71f43-2061-4df5-8863-089a34996f49",
                    "LayerId": "4b5afac0-3dd8-4960-9a52-bf97011cbab2"
                }
            ]
        },
        {
            "id": "31a6156b-6530-4345-a47a-fd79c0c5c54b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870e6763-be0d-4564-926d-0127d5654d63",
            "compositeImage": {
                "id": "2b22769b-846d-4a40-92ef-c850ebdf47b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31a6156b-6530-4345-a47a-fd79c0c5c54b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c46ce33-f0d4-4854-abac-728bf1922284",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31a6156b-6530-4345-a47a-fd79c0c5c54b",
                    "LayerId": "4b5afac0-3dd8-4960-9a52-bf97011cbab2"
                }
            ]
        },
        {
            "id": "ff5bfdab-d9de-41b6-8c27-1f0ab0774ed5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870e6763-be0d-4564-926d-0127d5654d63",
            "compositeImage": {
                "id": "bbd490dc-31d1-4189-8966-be1063d92d92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff5bfdab-d9de-41b6-8c27-1f0ab0774ed5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51cbf87d-5f49-4fc9-85ce-e75278d3127b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff5bfdab-d9de-41b6-8c27-1f0ab0774ed5",
                    "LayerId": "4b5afac0-3dd8-4960-9a52-bf97011cbab2"
                }
            ]
        },
        {
            "id": "21ee246d-9576-4bab-9743-418d95a679f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870e6763-be0d-4564-926d-0127d5654d63",
            "compositeImage": {
                "id": "baf72336-e589-4dfe-ba5f-3be0804d81dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21ee246d-9576-4bab-9743-418d95a679f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96aa971d-04e8-48e2-ae90-3c39096f32b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21ee246d-9576-4bab-9743-418d95a679f8",
                    "LayerId": "4b5afac0-3dd8-4960-9a52-bf97011cbab2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "4b5afac0-3dd8-4960-9a52-bf97011cbab2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "870e6763-be0d-4564-926d-0127d5654d63",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}