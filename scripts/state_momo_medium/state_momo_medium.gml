live_call();
if (heat > 50)
{
    if(state_new == 1)
    {
        with(hurtbox0)
    	{
    	    xoffset = -29;
    	    yoffset = -36;
    	    image_xscale  = 58;
    	    image_yscale  = 68;
    	    xshift  = -1;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -15;
    	    yoffset = -61;
    	    image_xscale  = 30;
    	    image_yscale  = 25;
    	    xshift = -3;
    	}
    	
    	with(collisionbox)
    	{
    	    xoffset = -20;
    	    yoffset = -22;
    	    image_xscale  = 40;
    	    image_yscale  = 54;
    	    xshift = 0;
    	}
    	
    	chainmove = 0;
    	attackframe0 = 7;
    	endstate = 24;
    }
    
    //animation
    sprite_index = spr_momo_5m_fire;
    
    if(state_timer < attackframe0 && state_timer > 1)
    {
        vx = 5 * image_xscale;
    }
    
    //attack
    if(state_timer == attackframe0)
    {
        hitbox0 = create_hitbox(9,-25,43,16,7.5,0,14,19,10,12,0,0,2,4,"mid",-1);
        attackhurtbox0 = create_hurtbox(8 * image_xscale, -17,22 * image_xscale,16,2,-2);
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 1;
        hitbox0.juggle = 1;
    }
    
    //chainmove
    if(state_timer > 3)
    {
        if(input_check(1,0,1,0,0) || input_check(2,0,1,0,0) || input_check(3,0,1,0,0))
        {
            chainmove = "cmedium"
        }
        
        if(input_check(0,0,0,1,0))
        {
            chainmove = "heavy"
        }
        
        if((input_check(2,0,0,1,0) || input_check(1,0,0,1,0) || input_check(3,0,0,1,0)))
        {
            chainmove = "cheavy";
        }
    }
    
    attack_base(endstate,0,0,1,1,0);
}
else if(heat < -50)
{
    if(state_new == 1)
    {
        with(hurtbox0)
    	{
    	    xoffset = -29;
    	    yoffset = -36;
    	    image_xscale  = 58;
    	    image_yscale  = 68;
    	    xshift  = -1;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -15;
    	    yoffset = -61;
    	    image_xscale  = 30;
    	    image_yscale  = 25;
    	    xshift = -3;
    	}
    	
    	
    	with(collisionbox)
    	{
    	    xoffset = -20;
    	    yoffset = -22;
    	    image_xscale  = 40;
    	    image_yscale  = 54;
    	    xshift = 0;
    	}
    	
    	chainmove = 0;
    	attackframe0 = 6;
    	endstate = 22;
    }
    
    //animation
    sprite_index = spr_momo_5m_ice;
    
    if(state_timer < attackframe0 && state_timer > 1)
    {
        vx = 5 * image_xscale;
    }
    
    //attack
    if(state_timer == attackframe0)
    {
        hitbox0 = create_hitbox(9,-25,46,16,7.5,0,15,19,10,10,0,0,2,4,"mid",-1);
        attackhurtbox0 = create_hurtbox(8 * image_xscale, -17,22 * image_xscale,16,2,-2);
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 1;
        hitbox0.juggle = 1;
    }
    
    //chainmove
    if(state_timer > 3)
    {
        if(input_check(1,0,1,0,0) || input_check(2,0,1,0,0) || input_check(3,0,1,0,0))
        {
            chainmove = "cmedium"
        }
        
        if(input_check(0,0,0,1,0))
        {
            chainmove = "heavy"
        }
        
        if((input_check(2,0,0,1,0) || input_check(1,0,0,1,0) || input_check(3,0,0,1,0)))
        {
            chainmove = "cheavy";
        }
    }
    
    attack_base(endstate,0,0,1,1,0);
}
else
{
    if(state_new == 1)
    {
        with(hurtbox0)
    	{
    	    xoffset = -29;
    	    yoffset = -36;
    	    image_xscale  = 58;
    	    image_yscale  = 68;
    	    xshift  = -1;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -15;
    	    yoffset = -61;
    	    image_xscale  = 30;
    	    image_yscale  = 25;
    	    xshift = -3;
    	}
    	
    	
    	with(collisionbox)
    	{
    	    xoffset = -20;
    	    yoffset = -22;
    	    image_xscale  = 40;
    	    image_yscale  = 54;
    	    xshift = 0;
    	}
    	
    	chainmove = 0;
    	attackframe0 = 6;
    	endstate = 22;
    }
    
    //animation
    sprite_index = spr_momo_5m;
    
    //forward movement
    if(state_timer < attackframe0 && state_timer > 3)
    {
        vx = Approach(vx,6 * image_xscale, 3);
    }
    
    //attack
    if(state_timer == attackframe0)
    {
        hitbox0 = create_hitbox(20,-36,63,38,7.5,0,15,19,10,14,0,0,2,4,"mid",-1);
        attackhurtbox0 = create_hurtbox(8 * image_xscale, -17,22 * image_xscale,16,2,-2);
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 1;
        hitbox0.juggle = 1;
    }
    
    
    //chainmove
    if(state_timer > 3)
    {
        if(input_check(1,0,1,0,0) || input_check(2,0,1,0,0) || input_check(3,0,1,0,0))
        {
            chainmove = "cmedium"
        }
        
        if(input_check(0,0,0,1,0))
        {
            chainmove = "heavy"
        }
        
        if((input_check(2,0,0,1,0) || input_check(1,0,0,1,0) || input_check(3,0,0,1,0)))
        {
            chainmove = "cheavy";
        }
    }
    
    attack_base(endstate,0,0,1,1,0);
}