{
    "id": "365deb05-c937-4470-9080-418237a85876",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_2l",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 16,
    "bbox_right": 137,
    "bbox_top": 29,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1afd6d94-478d-4729-97ac-f31fdc0340cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "365deb05-c937-4470-9080-418237a85876",
            "compositeImage": {
                "id": "fb6874fe-8bcd-438a-817c-20f31a93dc39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1afd6d94-478d-4729-97ac-f31fdc0340cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dc537a5-9a5f-48ec-84e5-afff85f89a3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1afd6d94-478d-4729-97ac-f31fdc0340cc",
                    "LayerId": "237b1fd5-2ecc-43fc-95a6-abfbda34fa58"
                }
            ]
        },
        {
            "id": "1f4bac04-b105-4fb6-b95c-f7af7e73cf73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "365deb05-c937-4470-9080-418237a85876",
            "compositeImage": {
                "id": "8fce92ac-55eb-459e-a3b9-1559d25ce4f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f4bac04-b105-4fb6-b95c-f7af7e73cf73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ff3c3e6-3828-48ee-a39f-4b326a632e32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f4bac04-b105-4fb6-b95c-f7af7e73cf73",
                    "LayerId": "237b1fd5-2ecc-43fc-95a6-abfbda34fa58"
                }
            ]
        },
        {
            "id": "1308ba80-362f-46b3-8542-d23e549081f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "365deb05-c937-4470-9080-418237a85876",
            "compositeImage": {
                "id": "5b14207e-7029-4594-b612-00026bd8cf3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1308ba80-362f-46b3-8542-d23e549081f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bb5c117-2941-40e9-bc84-bbec956fb82c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1308ba80-362f-46b3-8542-d23e549081f7",
                    "LayerId": "237b1fd5-2ecc-43fc-95a6-abfbda34fa58"
                }
            ]
        },
        {
            "id": "e5f16777-7a18-4562-afc0-0673b699178d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "365deb05-c937-4470-9080-418237a85876",
            "compositeImage": {
                "id": "5c78c05b-bd70-40f9-8cae-a27cc8c2324d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5f16777-7a18-4562-afc0-0673b699178d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73c38360-03b3-4220-be85-b9309d863f91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5f16777-7a18-4562-afc0-0673b699178d",
                    "LayerId": "237b1fd5-2ecc-43fc-95a6-abfbda34fa58"
                }
            ]
        },
        {
            "id": "a34217f2-8b88-4183-a71d-75070212eea3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "365deb05-c937-4470-9080-418237a85876",
            "compositeImage": {
                "id": "b9590928-1723-4b84-ba18-871864043db9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a34217f2-8b88-4183-a71d-75070212eea3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c89c1177-e94e-4343-933f-f38c4052c452",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a34217f2-8b88-4183-a71d-75070212eea3",
                    "LayerId": "237b1fd5-2ecc-43fc-95a6-abfbda34fa58"
                }
            ]
        },
        {
            "id": "8a68356c-f0f7-455c-95ca-703e820b6469",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "365deb05-c937-4470-9080-418237a85876",
            "compositeImage": {
                "id": "2521e225-cce6-4bd2-bf96-b7f3f275bebc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a68356c-f0f7-455c-95ca-703e820b6469",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f604022a-5e4c-427c-a693-e9c656c71930",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a68356c-f0f7-455c-95ca-703e820b6469",
                    "LayerId": "237b1fd5-2ecc-43fc-95a6-abfbda34fa58"
                }
            ]
        },
        {
            "id": "90f5362a-50a2-4e0f-a5b2-3f84d074083a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "365deb05-c937-4470-9080-418237a85876",
            "compositeImage": {
                "id": "d6792b3d-365e-4dc0-9102-d4c5c6ee9c29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90f5362a-50a2-4e0f-a5b2-3f84d074083a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "285f50af-5f3e-47f2-8b6c-677843c277ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90f5362a-50a2-4e0f-a5b2-3f84d074083a",
                    "LayerId": "237b1fd5-2ecc-43fc-95a6-abfbda34fa58"
                }
            ]
        },
        {
            "id": "0674ec65-2466-4502-98ce-f040d1c7268d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "365deb05-c937-4470-9080-418237a85876",
            "compositeImage": {
                "id": "bea17425-147e-407c-86bf-54ebb666a30d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0674ec65-2466-4502-98ce-f040d1c7268d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7789d01-9001-4e9b-8fbf-b4fd7fc0a0b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0674ec65-2466-4502-98ce-f040d1c7268d",
                    "LayerId": "237b1fd5-2ecc-43fc-95a6-abfbda34fa58"
                }
            ]
        },
        {
            "id": "a1b47f59-daed-4711-97d8-b4f9892b67eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "365deb05-c937-4470-9080-418237a85876",
            "compositeImage": {
                "id": "dcf9ec38-093c-4302-a765-a248e95a340e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1b47f59-daed-4711-97d8-b4f9892b67eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37faf29f-0b11-4bdc-8f16-ceb04da5258b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1b47f59-daed-4711-97d8-b4f9892b67eb",
                    "LayerId": "237b1fd5-2ecc-43fc-95a6-abfbda34fa58"
                }
            ]
        },
        {
            "id": "9f447898-69de-46f2-bebd-8199d05e0593",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "365deb05-c937-4470-9080-418237a85876",
            "compositeImage": {
                "id": "93ff72df-88cf-44ce-a139-c3c22f1805a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f447898-69de-46f2-bebd-8199d05e0593",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8da6c696-f4e6-415c-9fd7-af238b634ee7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f447898-69de-46f2-bebd-8199d05e0593",
                    "LayerId": "237b1fd5-2ecc-43fc-95a6-abfbda34fa58"
                }
            ]
        },
        {
            "id": "b20d32f9-17f8-4397-9bcf-bafad1efdb72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "365deb05-c937-4470-9080-418237a85876",
            "compositeImage": {
                "id": "976609c6-3a24-46bd-9b5c-bb8e1e4981ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b20d32f9-17f8-4397-9bcf-bafad1efdb72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8052d92-af7f-49de-bbbb-899888a53678",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b20d32f9-17f8-4397-9bcf-bafad1efdb72",
                    "LayerId": "237b1fd5-2ecc-43fc-95a6-abfbda34fa58"
                }
            ]
        },
        {
            "id": "204fb15f-b46a-469b-8643-c5cf58a68b77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "365deb05-c937-4470-9080-418237a85876",
            "compositeImage": {
                "id": "8dedf642-051e-4514-83b9-5d0688c1666f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "204fb15f-b46a-469b-8643-c5cf58a68b77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3e2f728-2da9-41bf-8de2-87d528557c1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "204fb15f-b46a-469b-8643-c5cf58a68b77",
                    "LayerId": "237b1fd5-2ecc-43fc-95a6-abfbda34fa58"
                }
            ]
        },
        {
            "id": "8324a10b-bda2-4109-9c3b-9b33edd3df93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "365deb05-c937-4470-9080-418237a85876",
            "compositeImage": {
                "id": "3612c3c2-9f86-4b8f-afb1-c6ff7bb3b0b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8324a10b-bda2-4109-9c3b-9b33edd3df93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12b3cb22-aabd-4fef-975a-f672d9912a69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8324a10b-bda2-4109-9c3b-9b33edd3df93",
                    "LayerId": "237b1fd5-2ecc-43fc-95a6-abfbda34fa58"
                }
            ]
        },
        {
            "id": "bf6fe7c8-1cd7-4a4d-bcb7-687f9eb5232f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "365deb05-c937-4470-9080-418237a85876",
            "compositeImage": {
                "id": "06f8ec7a-bca9-4058-a0f6-6887f81e8dd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf6fe7c8-1cd7-4a4d-bcb7-687f9eb5232f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83d8ffe7-b7cd-473a-975d-4fd9b76442c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf6fe7c8-1cd7-4a4d-bcb7-687f9eb5232f",
                    "LayerId": "237b1fd5-2ecc-43fc-95a6-abfbda34fa58"
                }
            ]
        },
        {
            "id": "bcb1f6fc-4158-467d-a8e7-adb2ca58aeca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "365deb05-c937-4470-9080-418237a85876",
            "compositeImage": {
                "id": "80b7de52-bba2-4ea3-adb9-e8cccdb52ed1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcb1f6fc-4158-467d-a8e7-adb2ca58aeca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "690024b4-b910-4a26-a1c9-54384e6a11c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcb1f6fc-4158-467d-a8e7-adb2ca58aeca",
                    "LayerId": "237b1fd5-2ecc-43fc-95a6-abfbda34fa58"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "237b1fd5-2ecc-43fc-95a6-abfbda34fa58",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "365deb05-c937-4470-9080-418237a85876",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 63,
    "yorig": 87
}