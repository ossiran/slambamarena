{
    "id": "3f8fb32e-4b8b-4580-91dc-c8e7625c2aaf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dsyn_2l",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 107,
    "bbox_left": 30,
    "bbox_right": 99,
    "bbox_top": 52,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d4bacc6-c9c6-46e5-9e18-f14681484117",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f8fb32e-4b8b-4580-91dc-c8e7625c2aaf",
            "compositeImage": {
                "id": "4d90e5e6-32d2-420e-bd60-6e9e41e97b0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d4bacc6-c9c6-46e5-9e18-f14681484117",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98419087-ddb6-4362-815d-663a3042fad3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d4bacc6-c9c6-46e5-9e18-f14681484117",
                    "LayerId": "81129197-1036-43fb-b8ee-ae879455e806"
                }
            ]
        },
        {
            "id": "7ecb4c43-8caf-4938-b91d-7a6c6085891b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f8fb32e-4b8b-4580-91dc-c8e7625c2aaf",
            "compositeImage": {
                "id": "81e74b16-6ed4-42b9-b25c-7736b9bfe249",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ecb4c43-8caf-4938-b91d-7a6c6085891b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7914867b-5471-4461-8384-b44e117b3205",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ecb4c43-8caf-4938-b91d-7a6c6085891b",
                    "LayerId": "81129197-1036-43fb-b8ee-ae879455e806"
                }
            ]
        },
        {
            "id": "cf13db92-dd38-49a9-a5db-6ce5a2eb168c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f8fb32e-4b8b-4580-91dc-c8e7625c2aaf",
            "compositeImage": {
                "id": "ad53b658-eb04-4dbe-a8ca-293e01aadaeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf13db92-dd38-49a9-a5db-6ce5a2eb168c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2723298-de25-41b7-9d71-0f28a7a3bddd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf13db92-dd38-49a9-a5db-6ce5a2eb168c",
                    "LayerId": "81129197-1036-43fb-b8ee-ae879455e806"
                }
            ]
        },
        {
            "id": "f7abaacd-895e-4daa-aeda-7e3f626177a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f8fb32e-4b8b-4580-91dc-c8e7625c2aaf",
            "compositeImage": {
                "id": "5c38eb49-740d-4c01-a846-195da784e7fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7abaacd-895e-4daa-aeda-7e3f626177a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3436a889-1ac3-4c2c-b71f-3e847fb93c8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7abaacd-895e-4daa-aeda-7e3f626177a1",
                    "LayerId": "81129197-1036-43fb-b8ee-ae879455e806"
                }
            ]
        },
        {
            "id": "9ecce9fb-2735-4180-a795-54277405d380",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f8fb32e-4b8b-4580-91dc-c8e7625c2aaf",
            "compositeImage": {
                "id": "7948202d-8f8b-4076-96ac-40954364558d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ecce9fb-2735-4180-a795-54277405d380",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "041d9e5d-2200-4c1d-9cfe-0d64b40e61fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ecce9fb-2735-4180-a795-54277405d380",
                    "LayerId": "81129197-1036-43fb-b8ee-ae879455e806"
                }
            ]
        },
        {
            "id": "d1f1195f-3614-42da-8b60-9b691b8c2b52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f8fb32e-4b8b-4580-91dc-c8e7625c2aaf",
            "compositeImage": {
                "id": "ee99f41b-3991-43cc-a4b6-dbdfd8550ca7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1f1195f-3614-42da-8b60-9b691b8c2b52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29a3e5be-b35f-472f-8fc5-8106fa72bf40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1f1195f-3614-42da-8b60-9b691b8c2b52",
                    "LayerId": "81129197-1036-43fb-b8ee-ae879455e806"
                }
            ]
        },
        {
            "id": "fa57fdbb-75dc-4576-b097-328e3c803e3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f8fb32e-4b8b-4580-91dc-c8e7625c2aaf",
            "compositeImage": {
                "id": "4542d9d6-628a-4280-bba5-48dd4aae2e21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa57fdbb-75dc-4576-b097-328e3c803e3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e12e39a-e60a-47a5-8d4d-077ffea13695",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa57fdbb-75dc-4576-b097-328e3c803e3c",
                    "LayerId": "81129197-1036-43fb-b8ee-ae879455e806"
                }
            ]
        },
        {
            "id": "ddce7727-25a6-4cc0-8896-973f8c62bd8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f8fb32e-4b8b-4580-91dc-c8e7625c2aaf",
            "compositeImage": {
                "id": "84460510-2369-42bc-bd3f-503c54e78b16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddce7727-25a6-4cc0-8896-973f8c62bd8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3214b06f-3d19-4204-b365-64561c655f8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddce7727-25a6-4cc0-8896-973f8c62bd8f",
                    "LayerId": "81129197-1036-43fb-b8ee-ae879455e806"
                }
            ]
        },
        {
            "id": "b70632dc-870e-4835-870a-7a1792e14d4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f8fb32e-4b8b-4580-91dc-c8e7625c2aaf",
            "compositeImage": {
                "id": "b703d30a-5861-4fea-9c56-b488ac006156",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b70632dc-870e-4835-870a-7a1792e14d4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af343fdb-6a2f-4066-85e7-b26f0b0b47ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b70632dc-870e-4835-870a-7a1792e14d4f",
                    "LayerId": "81129197-1036-43fb-b8ee-ae879455e806"
                }
            ]
        },
        {
            "id": "74d0273e-1cbc-45a0-8aa3-91087dc200b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f8fb32e-4b8b-4580-91dc-c8e7625c2aaf",
            "compositeImage": {
                "id": "d77b6262-77ee-419c-afa1-c35706c43821",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74d0273e-1cbc-45a0-8aa3-91087dc200b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48c41b7d-ce40-4da7-8e69-9834880b508a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74d0273e-1cbc-45a0-8aa3-91087dc200b8",
                    "LayerId": "81129197-1036-43fb-b8ee-ae879455e806"
                }
            ]
        },
        {
            "id": "5a0822af-377a-45c3-9040-466e6071c185",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f8fb32e-4b8b-4580-91dc-c8e7625c2aaf",
            "compositeImage": {
                "id": "13eb4f19-8793-42c6-be68-14efc69fbc8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a0822af-377a-45c3-9040-466e6071c185",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33f8c2bf-d349-4ab3-a617-10b3c0b8ab3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a0822af-377a-45c3-9040-466e6071c185",
                    "LayerId": "81129197-1036-43fb-b8ee-ae879455e806"
                }
            ]
        },
        {
            "id": "8edb5961-2569-4c15-bf54-ed855f66a580",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f8fb32e-4b8b-4580-91dc-c8e7625c2aaf",
            "compositeImage": {
                "id": "dd149c80-9773-489d-9363-14d2fdfc0acd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8edb5961-2569-4c15-bf54-ed855f66a580",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "780bcd18-7f81-45a5-b495-6432c4744ab8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8edb5961-2569-4c15-bf54-ed855f66a580",
                    "LayerId": "81129197-1036-43fb-b8ee-ae879455e806"
                }
            ]
        },
        {
            "id": "3fff2766-b084-4ccc-a217-0bf0f3a101cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f8fb32e-4b8b-4580-91dc-c8e7625c2aaf",
            "compositeImage": {
                "id": "ea0e9bbc-1b56-4913-b75d-186d6c18f9c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fff2766-b084-4ccc-a217-0bf0f3a101cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6a3e5d2-f9d5-4337-920a-5152a20e6965",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fff2766-b084-4ccc-a217-0bf0f3a101cc",
                    "LayerId": "81129197-1036-43fb-b8ee-ae879455e806"
                }
            ]
        },
        {
            "id": "7d0e383a-58be-45b2-b4c9-113b9037b055",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f8fb32e-4b8b-4580-91dc-c8e7625c2aaf",
            "compositeImage": {
                "id": "44a139b1-b41a-420a-bc6d-41e86b887146",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d0e383a-58be-45b2-b4c9-113b9037b055",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4987a7d0-7327-41ed-ba8d-6f5a52d72d02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d0e383a-58be-45b2-b4c9-113b9037b055",
                    "LayerId": "81129197-1036-43fb-b8ee-ae879455e806"
                }
            ]
        },
        {
            "id": "aa2b498c-8671-480b-9821-201d93147881",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f8fb32e-4b8b-4580-91dc-c8e7625c2aaf",
            "compositeImage": {
                "id": "2f55ba3b-4c60-4716-bff1-8bb5bcde5257",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa2b498c-8671-480b-9821-201d93147881",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b138b2db-bfaa-4b8f-9d35-73ff5167d72b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa2b498c-8671-480b-9821-201d93147881",
                    "LayerId": "81129197-1036-43fb-b8ee-ae879455e806"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "81129197-1036-43fb-b8ee-ae879455e806",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f8fb32e-4b8b-4580-91dc-c8e7625c2aaf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 76
}