{
    "id": "3f71f13e-1650-4279-94a0-b98cd956daa1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hitbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6d7852f9-0d34-4474-8fce-21e3dfcd58af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f71f13e-1650-4279-94a0-b98cd956daa1",
            "compositeImage": {
                "id": "72481912-1065-49a7-8f6b-a2220fd9f4d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d7852f9-0d34-4474-8fce-21e3dfcd58af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4453ea3d-de0e-4ab0-9964-617f65a2b751",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d7852f9-0d34-4474-8fce-21e3dfcd58af",
                    "LayerId": "21774706-3dc1-4519-ac36-fe95d01f4bdd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "21774706-3dc1-4519-ac36-fe95d01f4bdd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f71f13e-1650-4279-94a0-b98cd956daa1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}