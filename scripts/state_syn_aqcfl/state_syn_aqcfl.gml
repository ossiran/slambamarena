//


if(state_new == 1)
{
	with(hurtbox0)
	{
	    xoffset = -12;
	    yoffset = -13;
	    image_xscale  = 24;
	    image_yscale  = 40;
	    xshift  = 0;
	}
	
	with(hurtbox1)
	{
	    xoffset = -8;
	    yoffset = -31;
	    image_xscale  = 16;
	    image_yscale  = 18;
	    xshift = 0;
	}
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -13;
	    image_xscale  = 16;
	    image_yscale  = 40;
	    xshift = 0;
	}
	
	
	attackframe0 =  16;
	endstate = 36;
	
	vx = 0;
	vy = 0;
}




if(state_timer == attackframe0)
{
    
    hitbox1 = create_hitbox(20, - 15, 1000, 20, 5, -10, 16, 6, 10, 16, -1,0,3, 5, "mid", -1);
    hitbox1.hitsound = snd_hit;
    hitbox1.scaling  = 0.5;
    hitbox1.projectile = 2;
    hitbox1.airxtrayhit = -4;
    //hitbox1.airxtrahitstun = -10;
    hitbox1.chipdamage = 4;
    
}


sprite_index = spr_lsyn_bhr;

if(state_timer > attackframe0)
{
    attack_base(endstate,1,0,0,1,0);
    vy += -0.4;
}
else
{
    attack_base(endstate,0,0,0,1,0);

}
