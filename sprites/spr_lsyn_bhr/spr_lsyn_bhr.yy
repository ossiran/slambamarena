{
    "id": "93c43b5c-a774-4ea5-a891-95db28d15634",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lsyn_bhr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 47,
    "bbox_right": 77,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f44c8add-f572-4f5e-b449-777f9d8bcbcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "884a43ea-39f6-42be-860f-da5d46eb01fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f44c8add-f572-4f5e-b449-777f9d8bcbcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3bf7aa1-274c-4e2d-8afe-2198ddecc571",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f44c8add-f572-4f5e-b449-777f9d8bcbcb",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "6ddbda68-cfdf-455a-85ab-332431b8eb2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "8712fbc8-99b9-4b1f-a236-7f1042abf7aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ddbda68-cfdf-455a-85ab-332431b8eb2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86120519-8862-420a-b408-f5c4fc8f8679",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ddbda68-cfdf-455a-85ab-332431b8eb2a",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "89c37bf5-6d4a-4ddd-87a8-dfaa07c0380a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "36b4ff98-4a7d-452a-9aac-1083de428a54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89c37bf5-6d4a-4ddd-87a8-dfaa07c0380a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87e3f62e-2094-4c3a-a513-0b8bd89c721e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89c37bf5-6d4a-4ddd-87a8-dfaa07c0380a",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "152bb3ed-ae07-4d21-9b9a-5931d742594d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "1b240c76-83ce-402a-a4e5-433058d01e56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "152bb3ed-ae07-4d21-9b9a-5931d742594d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2953e901-fcaa-4f56-8ad1-b64b5f159765",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "152bb3ed-ae07-4d21-9b9a-5931d742594d",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "4348666a-40ff-461c-b77b-d5f633bbabd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "f867aac9-dd5b-4daf-bd31-e6ada7cb0a34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4348666a-40ff-461c-b77b-d5f633bbabd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "004e81ae-5126-4a2a-9134-aabec8a211af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4348666a-40ff-461c-b77b-d5f633bbabd8",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "d0f9672f-a5a5-4d24-9d9c-1d1035535444",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "ad4af746-8a16-4953-829f-0fd64fcdec51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0f9672f-a5a5-4d24-9d9c-1d1035535444",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90735061-ae60-4a57-b6fd-86499086fb77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0f9672f-a5a5-4d24-9d9c-1d1035535444",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "3b6cab4c-f3f9-4f5a-b391-e3d6ec94cd05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "2e126d82-beb1-4e60-b21d-7ea6a0eb87b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b6cab4c-f3f9-4f5a-b391-e3d6ec94cd05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddfc3cb5-50b9-455a-b310-614e54587a19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b6cab4c-f3f9-4f5a-b391-e3d6ec94cd05",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "794a6cde-ab60-4e19-8179-ba669cd5f5f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "9f8728ca-e1a1-408f-b0ef-ba306ab255f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "794a6cde-ab60-4e19-8179-ba669cd5f5f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "876dbe4d-f7a2-4771-8999-ea92327adbcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "794a6cde-ab60-4e19-8179-ba669cd5f5f4",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "3089f1f1-1ff8-466f-9bc8-58867c7bb5c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "6ac35065-3246-4d6f-9845-c88486c59193",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3089f1f1-1ff8-466f-9bc8-58867c7bb5c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b97058ef-5605-4af7-8257-7e00663bbf65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3089f1f1-1ff8-466f-9bc8-58867c7bb5c0",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "d3b70856-c4e8-42f0-892d-a80ef4db2308",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "5b810672-8093-428b-a3f4-d22b6b90b46a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3b70856-c4e8-42f0-892d-a80ef4db2308",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d228690f-5988-41cd-8e90-45890226ee19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3b70856-c4e8-42f0-892d-a80ef4db2308",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "40bd5d3c-f8b8-4053-8909-60901f1d20c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "bc2f13cc-dfad-4ed8-aee7-8512a8ce55ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40bd5d3c-f8b8-4053-8909-60901f1d20c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54618d7e-7792-4a04-bee1-ce456563f96d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40bd5d3c-f8b8-4053-8909-60901f1d20c1",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "4c6c0bad-1fb3-4523-8a33-584b323997d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "77e48ea1-128d-44ea-bed3-99970dc9bef5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c6c0bad-1fb3-4523-8a33-584b323997d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a4332d7-568e-4705-81f9-01bcdbfbe608",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c6c0bad-1fb3-4523-8a33-584b323997d3",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "333061ad-a04e-46dc-b7cb-fde723c5d76b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "f0cd0fd9-145c-48e9-8d6c-c13bd9bb8630",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "333061ad-a04e-46dc-b7cb-fde723c5d76b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08c6bd3d-6fd3-4b7a-81d8-3884103ac6bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "333061ad-a04e-46dc-b7cb-fde723c5d76b",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "1ce7d414-ff3e-4cb3-8a64-921bd1df9557",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "7da5e7d5-3807-4617-b137-0131a37358a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ce7d414-ff3e-4cb3-8a64-921bd1df9557",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd52481a-9617-449b-a9f7-a209975b30c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ce7d414-ff3e-4cb3-8a64-921bd1df9557",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "be5716c1-c945-42e2-8d26-6cc869bae4a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "b3f9d206-5683-4f51-98f7-d2133a43f5b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be5716c1-c945-42e2-8d26-6cc869bae4a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eac765e3-b067-4c16-9f53-d99812af056f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be5716c1-c945-42e2-8d26-6cc869bae4a1",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "3e863d61-e359-4074-ae38-0a9bf7f36154",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "0bc83d7c-6ca3-4cc3-a599-67f30c4a991e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e863d61-e359-4074-ae38-0a9bf7f36154",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad4e69be-5947-47e5-8f5b-133990a0f725",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e863d61-e359-4074-ae38-0a9bf7f36154",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "b1dab497-75b7-4d5b-942c-d7a0e7f8bb47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "228f14a2-1e77-409a-a8a7-7777611d49c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1dab497-75b7-4d5b-942c-d7a0e7f8bb47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54718964-68d6-4826-b0fc-12e75581bf85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1dab497-75b7-4d5b-942c-d7a0e7f8bb47",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "908db8da-87a8-4a89-bb8c-a8078757a566",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "2c1c2199-5843-4a1b-8834-e357de0ff688",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "908db8da-87a8-4a89-bb8c-a8078757a566",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1781b32-fd15-45a1-9fd3-e232e5c5dccb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "908db8da-87a8-4a89-bb8c-a8078757a566",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "5dbac550-133c-479c-b19e-cb48b0087d35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "9f7bb490-13fb-4857-a0be-c44113734dde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dbac550-133c-479c-b19e-cb48b0087d35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ead58a93-5803-4a87-8986-a2463ee29081",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dbac550-133c-479c-b19e-cb48b0087d35",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "fa4aa743-8e04-45c8-976e-54036ceef16f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "87b6f12b-2ba0-4545-997a-7c7c02fdb69f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa4aa743-8e04-45c8-976e-54036ceef16f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbeeeb9d-c3a9-4367-9cae-0e7ce73c70f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa4aa743-8e04-45c8-976e-54036ceef16f",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "2ad89cfa-4359-4a87-a243-5d407f90c622",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "64aa2cee-7ecc-46c4-9f9b-03934ec12d09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ad89cfa-4359-4a87-a243-5d407f90c622",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cf7d31b-efc2-42f4-8a27-852441cd75f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ad89cfa-4359-4a87-a243-5d407f90c622",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "55a9a928-b669-4fe4-88df-eeb1e7591799",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "c35421b1-5dff-4e90-9bc3-527eead922e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55a9a928-b669-4fe4-88df-eeb1e7591799",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d72f5ab-5f5b-41cc-86a2-1c0ce12af835",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55a9a928-b669-4fe4-88df-eeb1e7591799",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "95ce00a9-47dd-488c-a229-af5b6b6125ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "6067fac4-ead2-45aa-b875-570ead8f8368",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95ce00a9-47dd-488c-a229-af5b6b6125ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9eeb77de-d613-4e80-9037-5d98f83551ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95ce00a9-47dd-488c-a229-af5b6b6125ff",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "ef340369-352e-47f2-af8c-ecd7407e8534",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "978d8a9f-ca40-49df-8e86-b715a3686775",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef340369-352e-47f2-af8c-ecd7407e8534",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ea18828-4ce5-4fa4-80a6-e20b1642e2be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef340369-352e-47f2-af8c-ecd7407e8534",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "517ac86c-5bac-43f1-a3f6-31542e1dccf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "a3512e0a-0cb5-420c-8061-5f54a197647a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "517ac86c-5bac-43f1-a3f6-31542e1dccf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b72bd0e9-cbab-4fd7-a192-caeacbb215c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "517ac86c-5bac-43f1-a3f6-31542e1dccf4",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "df55969d-f604-4295-8355-c32be3a4403b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "151c61a2-747d-495f-9237-f2eda54e6978",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df55969d-f604-4295-8355-c32be3a4403b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5bc985c-cd15-46e8-bbdf-a83f0eddc8c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df55969d-f604-4295-8355-c32be3a4403b",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "afb8fc98-42fc-4970-9c48-0e9292d5ae9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "73144f04-0958-4874-ab4b-25e83a6d2a57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afb8fc98-42fc-4970-9c48-0e9292d5ae9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4a127e3-7d15-4238-be52-e8949978aaa1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afb8fc98-42fc-4970-9c48-0e9292d5ae9c",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "c2891446-3a47-4a1c-a4c3-6f4a849b384b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "64ced03b-bd61-4013-b12d-40e7bd318562",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2891446-3a47-4a1c-a4c3-6f4a849b384b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5ac74ab-55f5-453c-8e2b-ac83a0a57427",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2891446-3a47-4a1c-a4c3-6f4a849b384b",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "ddf7c27b-f235-47ad-8b20-fe8ccc0aeb99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "3319aa09-df73-40b2-aab5-6e34586c74eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddf7c27b-f235-47ad-8b20-fe8ccc0aeb99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5ee9906-9be9-4395-b364-7656ea570ccc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddf7c27b-f235-47ad-8b20-fe8ccc0aeb99",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "23087510-357a-40f2-a498-31925d9ed462",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "a5ffd238-aa2c-4c93-a39f-65ebc37558ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23087510-357a-40f2-a498-31925d9ed462",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f352a9c-98e3-4c04-a15a-4050ff19b836",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23087510-357a-40f2-a498-31925d9ed462",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "245fadea-8987-47cf-a774-2eab402a9600",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "a521432d-5a3f-4a70-b2f6-afcc9d617164",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "245fadea-8987-47cf-a774-2eab402a9600",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93f94774-b286-44e8-afdb-cceb72574047",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "245fadea-8987-47cf-a774-2eab402a9600",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "d707e048-207e-4826-918a-b80876fc84b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "3eaa9014-e9da-4df4-82be-11eb2ddcfb73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d707e048-207e-4826-918a-b80876fc84b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81ac4d15-2675-4aad-81f4-a7006521d23a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d707e048-207e-4826-918a-b80876fc84b5",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "d3d2a066-bdaf-42cb-9a00-389037d314d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "04788475-0ad4-461d-89eb-a75e8ccfa72a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3d2a066-bdaf-42cb-9a00-389037d314d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb5d1286-5927-437b-a887-73edf04b9bf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3d2a066-bdaf-42cb-9a00-389037d314d3",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "74679c3b-47a7-4326-91a7-66739f5b723c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "569f9f37-a42e-434b-b405-8abacf0ab4cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74679c3b-47a7-4326-91a7-66739f5b723c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b63bbbb1-129a-42b6-85d5-dac848e03b2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74679c3b-47a7-4326-91a7-66739f5b723c",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "63466316-9236-4dd2-a3d9-c29eeffa11dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "aa504f67-6d63-4b56-8947-7b5423eefe06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63466316-9236-4dd2-a3d9-c29eeffa11dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89fc438c-5538-4282-9800-937a94162d48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63466316-9236-4dd2-a3d9-c29eeffa11dc",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "c4e28f37-393b-4264-9d5a-d50a10eb0535",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "5b15a410-704c-423b-8de7-53e768f7ecd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4e28f37-393b-4264-9d5a-d50a10eb0535",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc8652a8-2241-4f25-94f2-3e280466c70d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4e28f37-393b-4264-9d5a-d50a10eb0535",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        },
        {
            "id": "b90c9112-1552-4c4a-8795-8d8cea6b3aee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "compositeImage": {
                "id": "90db37d4-f699-4353-a2a8-ddd9edc98b9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b90c9112-1552-4c4a-8795-8d8cea6b3aee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bd11b09-cb71-45ad-bf2e-57d6d16d1f0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b90c9112-1552-4c4a-8795-8d8cea6b3aee",
                    "LayerId": "a9f7efc6-a802-4009-8cbc-fe98685691a1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "a9f7efc6-a802-4009-8cbc-fe98685691a1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "93c43b5c-a774-4ea5-a891-95db28d15634",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}