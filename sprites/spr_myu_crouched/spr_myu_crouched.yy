{
    "id": "c820e8d5-c8ad-4112-964f-9660a6b4066a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_myu_crouched",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 13,
    "bbox_right": 42,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c618328f-2f08-48a5-8ce5-cd8b701a4707",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c820e8d5-c8ad-4112-964f-9660a6b4066a",
            "compositeImage": {
                "id": "06fcfc0c-1864-4de0-b0a3-6776c6f04c10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c618328f-2f08-48a5-8ce5-cd8b701a4707",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15b77687-ec9c-477a-ac6a-9fa18fa87858",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c618328f-2f08-48a5-8ce5-cd8b701a4707",
                    "LayerId": "a8572ba3-f93a-4ba2-9244-a6f20b526601"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a8572ba3-f93a-4ba2-9244-a6f20b526601",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c820e8d5-c8ad-4112-964f-9660a6b4066a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 32
}