{
    "id": "1679e2ee-ad9b-4879-9ce3-d18b99ac3302",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_controller2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 5,
    "bbox_right": 62,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a26eb11-62ef-4054-a186-69afa637306e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1679e2ee-ad9b-4879-9ce3-d18b99ac3302",
            "compositeImage": {
                "id": "f311dd1d-6311-4186-8cdc-27b93991bbdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a26eb11-62ef-4054-a186-69afa637306e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e2f4eee-35b8-4046-bb44-4d7f5d10e7f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a26eb11-62ef-4054-a186-69afa637306e",
                    "LayerId": "b8daff53-409c-449e-8a3f-3f364b9aa0aa"
                }
            ]
        },
        {
            "id": "555d9da3-bd9d-409d-b659-e2909b2aa6f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1679e2ee-ad9b-4879-9ce3-d18b99ac3302",
            "compositeImage": {
                "id": "8072ebee-99a5-4ef1-a281-370200c493e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "555d9da3-bd9d-409d-b659-e2909b2aa6f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f30aa17e-bb27-4ed7-8491-95db10032604",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "555d9da3-bd9d-409d-b659-e2909b2aa6f2",
                    "LayerId": "b8daff53-409c-449e-8a3f-3f364b9aa0aa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b8daff53-409c-449e-8a3f-3f364b9aa0aa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1679e2ee-ad9b-4879-9ce3-d18b99ac3302",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}