if(hitfall == -100 || juggle > 5)
{
    //need to proof this for multiple hurtboxes
    with(hurtbox0)
    {
    	xoffset = -13;
    	yoffset = -1000;
    	image_xscale  = 0;
    	image_yscale  = 0;
    	xshift  = 0;
    }
    
    if(hurtbox1 != -1)
    {
        with(hurtbox1)
        {
        	xoffset = -13;
        	yoffset = -1000;
        	image_xscale  = 0;
        	image_yscale  = 0;
        	xshift  = 0;
        }
    }
    
    if(hurtbox2 != -1)
    {
        with(hurtbox2)
        {
        	xoffset = -13;
        	yoffset = -1000;
        	image_xscale  = 0;
        	image_yscale  = 0;
        	xshift  = 0;
        }
    }
    
    with(collisionbox)
    {
    	 xoffset = -8;
    	 yoffset = -1000;
    	 image_xscale  = 0;
    	 image_yscale  = 0;
    	 xshift = -2
    }
}

//gravity
if(!onground())
{
	vy = Approach(vy,vyMax + hitfall,grav);
}



if(hitstun > 0 )
{
    //hit = 0;
    state_switch("hitstun");
    

    //prevents restands. Maybe want restands
    if(onground() == 1)
    {
        y -= 1;
    }

}


if(juggle > 5)
{
    state_switch("recovery");
}



//turns off hitboxes right before the ground. NOT A GOOD SOLUTION
/*
if(y > 723 && vy > 6)
{
    //need to proof this for multiple hurtboxes
    with(hurtbox0)
    {
    	xoffset = -13;
    	yoffset = -1000;
    	image_xscale  = 26;
    	image_yscale  = 75;
    	xshift  = -2;
    }
    	
    with(collisionbox)
    {
    	 xoffset = -8;
    	 yoffset = -1000;
    	 image_xscale  = 0;
    	 image_yscale  = 0;
    	 xshift = -2
    }
}
*/

/*
if(hitfall == 0 && hitstun == 0)
{
    xhit = 0;
    yhit = 0;
    vy = 0;
    state_switch("recovery");
    
    with(hurtbox0)
    {
    	   xoffset = 0;
    	   yoffset = -1000;
    	   image_xscale  = 0;
    	   image_yscale  = 0;
    	   xshift  = 0;
    }
}
*/

/*
if(hitfall > 0)
{
    hitfall = Approach(hitfall, 0, 1);
}
*/

sprite_index = fall_sprite;

//hitting ground causes knockdown
if(onground() && hitstun == 0)
{
        hitpause = 0;
        with(hurtbox0)
    	{
    	    xoffset = 0;
    	    yoffset = -1000;
    	    image_xscale  = 0;
    	    image_yscale  = 0;
    	    xshift  = -2;
    	}
        state_switch("knockdown");
}