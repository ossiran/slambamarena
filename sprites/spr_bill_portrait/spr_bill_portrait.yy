{
    "id": "7b250ad0-9f88-4780-be0e-4052d8d66cd5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_portrait",
    "For3D": false,
    "HTile": true,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 52,
    "bbox_right": 88,
    "bbox_top": 75,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "17304919-ab1f-4d84-a0bf-e640487f9b24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b250ad0-9f88-4780-be0e-4052d8d66cd5",
            "compositeImage": {
                "id": "786f9b60-51bd-4bb7-b5d9-5775d2f85303",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17304919-ab1f-4d84-a0bf-e640487f9b24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2685271-c5b4-4756-8b6a-46ea4a55a75b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17304919-ab1f-4d84-a0bf-e640487f9b24",
                    "LayerId": "4ad05341-dd46-40eb-a72f-34b5a3136ba6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4ad05341-dd46-40eb-a72f-34b5a3136ba6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7b250ad0-9f88-4780-be0e-4052d8d66cd5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}