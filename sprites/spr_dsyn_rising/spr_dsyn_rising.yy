{
    "id": "15a167c2-c747-4d1a-b0b8-ec13bbb68960",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dsyn_rising",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 49,
    "bbox_right": 86,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8103d753-7794-4966-b489-a3cbb365358b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15a167c2-c747-4d1a-b0b8-ec13bbb68960",
            "compositeImage": {
                "id": "d6622242-7ab4-4247-bb4e-8c39be0105f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8103d753-7794-4966-b489-a3cbb365358b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ecca166-db87-472a-b4aa-ad0679923de4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8103d753-7794-4966-b489-a3cbb365358b",
                    "LayerId": "df69bd16-01f2-424c-a1ae-aab0ffe9655b"
                }
            ]
        },
        {
            "id": "6f6fd982-a09c-4409-9225-abe3509c0002",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15a167c2-c747-4d1a-b0b8-ec13bbb68960",
            "compositeImage": {
                "id": "b19ad77f-89a5-4355-a95c-c9e1eb4c58ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f6fd982-a09c-4409-9225-abe3509c0002",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6ad71d2-67ea-4328-a2c9-4368de6c2ecd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f6fd982-a09c-4409-9225-abe3509c0002",
                    "LayerId": "df69bd16-01f2-424c-a1ae-aab0ffe9655b"
                }
            ]
        },
        {
            "id": "cc587009-476d-4bd5-bb4f-97cd0229a2fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15a167c2-c747-4d1a-b0b8-ec13bbb68960",
            "compositeImage": {
                "id": "012a27e4-137f-4bea-baad-1009f524b384",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc587009-476d-4bd5-bb4f-97cd0229a2fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69150508-acd1-4d7c-93b6-cbf7892befc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc587009-476d-4bd5-bb4f-97cd0229a2fa",
                    "LayerId": "df69bd16-01f2-424c-a1ae-aab0ffe9655b"
                }
            ]
        },
        {
            "id": "8a3d9fcb-1c3b-46e8-bc5e-f8f0e56f62d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15a167c2-c747-4d1a-b0b8-ec13bbb68960",
            "compositeImage": {
                "id": "8b19802c-2816-4840-a2a2-d483a71228b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a3d9fcb-1c3b-46e8-bc5e-f8f0e56f62d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a016ab82-6f96-4d94-8eb5-6c5c89076b77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a3d9fcb-1c3b-46e8-bc5e-f8f0e56f62d4",
                    "LayerId": "df69bd16-01f2-424c-a1ae-aab0ffe9655b"
                }
            ]
        },
        {
            "id": "6611190f-602f-4005-a4c5-491cc045b2b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15a167c2-c747-4d1a-b0b8-ec13bbb68960",
            "compositeImage": {
                "id": "5494bc41-3f06-45c8-88f5-93883a0ad13a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6611190f-602f-4005-a4c5-491cc045b2b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3240527-89e1-47ea-9c38-2c1881d95811",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6611190f-602f-4005-a4c5-491cc045b2b1",
                    "LayerId": "df69bd16-01f2-424c-a1ae-aab0ffe9655b"
                }
            ]
        },
        {
            "id": "9c6d9042-4e05-42de-82c3-d16e7503b550",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15a167c2-c747-4d1a-b0b8-ec13bbb68960",
            "compositeImage": {
                "id": "0c00b234-9165-40f4-846e-9a7f21835355",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c6d9042-4e05-42de-82c3-d16e7503b550",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd1e661c-d5fb-401c-9e57-a192ae27aee7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c6d9042-4e05-42de-82c3-d16e7503b550",
                    "LayerId": "df69bd16-01f2-424c-a1ae-aab0ffe9655b"
                }
            ]
        },
        {
            "id": "0f9f667b-c97f-415f-ba30-40e958625c4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15a167c2-c747-4d1a-b0b8-ec13bbb68960",
            "compositeImage": {
                "id": "c37d2c1b-86e2-48fc-8478-aa5dd1074ed4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f9f667b-c97f-415f-ba30-40e958625c4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a94ffb4-5cf9-4025-b2e3-cd1a668b58ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f9f667b-c97f-415f-ba30-40e958625c4c",
                    "LayerId": "df69bd16-01f2-424c-a1ae-aab0ffe9655b"
                }
            ]
        },
        {
            "id": "fd2450ff-533e-45f2-b38d-743d11bd8f92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15a167c2-c747-4d1a-b0b8-ec13bbb68960",
            "compositeImage": {
                "id": "beb1f4a7-e52d-4210-a55f-dd77111b556a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd2450ff-533e-45f2-b38d-743d11bd8f92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37783195-30cd-484b-8350-cad9e73b2ca1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd2450ff-533e-45f2-b38d-743d11bd8f92",
                    "LayerId": "df69bd16-01f2-424c-a1ae-aab0ffe9655b"
                }
            ]
        },
        {
            "id": "422740f7-eedd-453e-8eca-1ff367177988",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15a167c2-c747-4d1a-b0b8-ec13bbb68960",
            "compositeImage": {
                "id": "9d5709a4-6563-4998-a156-3e65620886c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "422740f7-eedd-453e-8eca-1ff367177988",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98462aff-29f8-49e1-bbfd-1494ddf54eed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "422740f7-eedd-453e-8eca-1ff367177988",
                    "LayerId": "df69bd16-01f2-424c-a1ae-aab0ffe9655b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "df69bd16-01f2-424c-a1ae-aab0ffe9655b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "15a167c2-c747-4d1a-b0b8-ec13bbb68960",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}