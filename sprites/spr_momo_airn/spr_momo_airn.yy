{
    "id": "78c74cfe-5f8b-477b-8dee-5d0217283268",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_airn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 120,
    "bbox_left": 24,
    "bbox_right": 115,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "090c0df0-e42b-4c36-9fc6-65d532490396",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78c74cfe-5f8b-477b-8dee-5d0217283268",
            "compositeImage": {
                "id": "2e41079b-a1e7-4fea-8b33-834496ad6088",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "090c0df0-e42b-4c36-9fc6-65d532490396",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b3d47e1-893b-41ec-9ad0-fada24ac9b48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "090c0df0-e42b-4c36-9fc6-65d532490396",
                    "LayerId": "a6ec454c-851e-463e-bd9a-ddc16de40c0c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "a6ec454c-851e-463e-bd9a-ddc16de40c0c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "78c74cfe-5f8b-477b-8dee-5d0217283268",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 63,
    "yorig": 87
}