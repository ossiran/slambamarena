{
    "id": "e5cbafcd-19bc-448f-bfdd-4dfef5af27a6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_grabbox",
    "eventList": [
        {
            "id": "9bfb6b53-f380-65c1-de82-fa344359ed5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e5cbafcd-19bc-448f-bfdd-4dfef5af27a6"
        },
        {
            "id": "c0822d6c-94e0-a8ce-efca-15e44829ba03",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e5cbafcd-19bc-448f-bfdd-4dfef5af27a6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3fea898e-6cb4-4c75-8ed5-404226e9e14c",
    "visible": true
}