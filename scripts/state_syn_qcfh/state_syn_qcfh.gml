//
if (synmode > 0)
{
    if(state_new == 1)
    {
    	with(hurtbox0)
    	{
    	    xoffset = -12;
    	    yoffset = -13;
    	    image_xscale  = 24;
    	    image_yscale  = 40;
    	    xshift  = 0;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -8;
    	    yoffset = -31;
    	    image_xscale  = 16;
    	    image_yscale  = 18;
    	    xshift = 0;
    	}
    	
    	with(collisionbox)
    	{
    	    xoffset = -8;
    	    yoffset = -13;
    	    image_xscale  = 16;
    	    image_yscale  = 40;
    	    xshift = 0;
    	}
    	
    	
    	attackframe0 =  17;
    	attackframe1 =  20;
    	endstate = 45;
    }
    
    
    
    if(state_timer == attackframe0)
    {
        
        hitbox1 = create_hitbox(20, - 15, 1000, 10, 5, 0, 12, 18, 10, 16, -1,0,1, 0, "mid", -1);
        hitbox1.hitsound = snd_hit;
        hitbox1.scaling  = 0.5;
        hitbox1.projectile = 2;
        hitbox1.airxtrayhit = -9;
        hitbox1.airxtrahitstun = -10;
        hitbox1.chipdamage = 3;
        vx = -1.5 * image_xscale;
    }
    
    if(state_timer == attackframe1)
    {
        
        
        for(i = 0; i < 20; i++)
        {
            hitbox1 = create_hitbox(5 + (15 * i), - 15 + (-7 * i), 25, 10, 5, 0, 15, 18, 10, 16, -1,0,3, 0, "mid", -1);
            hitbox1.hitsound = snd_hit;
            hitbox1.scaling  = 0.5;
            hitbox1.projectile = 2;
            hitbox1.airxtrayhit = -9;
            hitbox1.airxtrahitstun = -10;
            hitbox1.chipdamage = 3;
        }
        vx = -3.5 * image_xscale;
    }
    
    sprite_index = spr_lsyn_bhr;
    
    attack_base(endstate,0,0,0,1,0);
}
else
{
    if(state_new == 1)
    {
    	with(hurtbox0)
    	{
    	    xoffset = -12;
    	    yoffset = -13;
    	    image_xscale  = 24;
    	    image_yscale  = 40;
    	    xshift  = 0;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -8;
    	    yoffset = -31;
    	    image_xscale  = 16;
    	    image_yscale  = 18;
    	    xshift = 0;
    	}
    	
    	with(collisionbox)
    	{
    	    xoffset = -8;
    	    yoffset = -13;
    	    image_xscale  = 16;
    	    image_yscale  = 40;
    	    xshift = 0;
    	}
    	
    	
    	attackframe0 =  11;
    	attackframe1 = 22;
    	attackframe2 = 36;
    	endstate = 48;
    }
    
    if((state_timer > 4 && state_timer < attackframe0) || (state_timer > attackframe1 - 4 && state_timer < attackframe1 + 4) || (state_timer > attackframe2 - 4 && state_timer < attackframe2))
    {
        vx = Approach(vx, 8.5 * image_xscale, 3);
    }
    
    
    if(state_timer == attackframe0)
    {
        hitbox1 = create_hitbox(15, -26, 40, 52, 5, 0, 7, 18, 11, 16, -1,0,1, 0, "mid", -1);
        hitbox2 = create_hitbox(15, -36, 12, 10, 5, 0, 7, 18, 11, 16, -1,0,1, 0, "mid", -1);
        hitbox1.hitsound = snd_hit;
        hitbox1.scaling  = 0.5;
        hitbox1.projectile = 2;
        hitbox1.airxtrayhit = -5;
        hitbox1.airxtrahitstun = -10;
        hitbox1.chipdamage = 3;
        hitbox2.hitsound = snd_hit;
        hitbox2.scaling  = 0.5;
        hitbox2.projectile = 2;
        hitbox2.airxtrayhit = -5;
        hitbox2.airxtrahitstun = -10;
        hitbox2.chipdamage = 3;
        vx = 0;
    }
    
    if(state_timer == attackframe1)
    {
        hitbox1 = create_hitbox(18, -26, 40, 52, 5, -8, 7, 9, 11, 16, -1,0,1, 0, "mid", -1);
        hitbox2 = create_hitbox(18, -36, 10, 10, 5, -8, 7, 9, 11, 16, -1,0,1, 0, "mid", -1);
        hitbox1.hitsound = snd_hit;
        hitbox1.scaling  = 0.5;
        hitbox1.airxtrayhit = 3;
        hitbox1.chipdamage = 3;
        hitbox2.hitsound = snd_hit;
        hitbox2.scaling  = 0.5;
        hitbox2.airxtrayhit = 3;
        hitbox2.chipdamage = 3;
        vx = 0;
    }
    
    if(state_timer == attackframe2)
    {
        hitbox1 = create_hitbox(20, -24, 72, 30, 5, -16, 10, 8, 11, 16, 1,0,1, 0, "mid", -1);
        hitbox1.hitsound = snd_hit;
        hitbox1.scaling  = 0.5;
        hitbox1.airxtrayhit = 4;
        hitbox1.chipdamage = 3;
    }
    
    
    sprite_index = spr_dsyn_qcfh;
    
    attack_base(endstate,0,0,0,1,0);
}