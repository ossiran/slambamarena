live_call();

if(state_new == 1)
{
    with(hurtbox0)
	{
	    xoffset = -12;
	    yoffset = -13;
	    image_xscale  = 24;
	    image_yscale  = 40;
	    xshift  = 0;
	}
	
	with(hurtbox1)
	{
	    xoffset = -8;
	    yoffset = -31;
	    image_xscale  = 16;
	    image_yscale  = 18;
	    xshift = 0;
	}
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -13;
	    image_xscale  = 16;
	    image_yscale  = 40;
	    xshift = 0;
	}
	
	chainmove = 0;
	attackframe0 = 6;
	attackframe1 = 9;
	endstate = 25;
}

sprite_index = spr_lsyn_66m;

if(state_timer < attackframe0 + 1 && state_timer > 1)
{
    vx = Approach(vx,7.5 * image_xscale, 3);
}
//attack
if(state_timer == attackframe0)
{
    hitbox0 = create_hitbox(9,-24,40,15,0,0,12,20,10,15,-1,0,0,4,"mid",-1);
    attackhurtbox0 = create_hurtbox(8 * image_xscale, -17,22 * image_xscale,16,2,-2);
    hitbox0.hitsound = snd_hitsound1;
    hitbox0.scaling = 1;
}

if(state_timer == attackframe1)
{
    hitbox0 = create_hitbox(9,-24,40,15,7.95,0,12,18,12,15,-1,0,2,4,"mid",-1);
    attackhurtbox0 = create_hurtbox(8 * image_xscale, -17,22 * image_xscale,16,2,-2);
    hitbox0.hitsound = snd_hitsound1;
    hitbox0.scaling = 1;
}

attack_base(endstate,0,0,1,1,0);