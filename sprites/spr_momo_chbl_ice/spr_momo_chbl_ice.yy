{
    "id": "67a957e4-1688-4548-9cf6-8b69a9737449",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_chbl_ice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 26,
    "bbox_right": 139,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "805d9a33-ebc9-4081-be91-78664808a760",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "7c5fbe81-86c0-411a-b19e-b9f535a13bef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "805d9a33-ebc9-4081-be91-78664808a760",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd38e269-c776-47a1-92fc-1e769f61c654",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "805d9a33-ebc9-4081-be91-78664808a760",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "e4bba5bf-c76b-44a5-ab6c-fcf81c599abe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "aa6d9598-0c4c-4404-85aa-ea35d13ad0d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4bba5bf-c76b-44a5-ab6c-fcf81c599abe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4adfd88-9cc0-4a3f-a84c-ec16c2018ed8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4bba5bf-c76b-44a5-ab6c-fcf81c599abe",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "adcb595e-01c6-4870-8f9f-903acc52e980",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "b88c20eb-2799-4621-b3e9-3375f909382e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adcb595e-01c6-4870-8f9f-903acc52e980",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3efc2d2-d76e-4990-bd3b-63c9b84fa3be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adcb595e-01c6-4870-8f9f-903acc52e980",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "d099ac28-a914-47ad-b66e-4908f8ab49df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "3d6386ab-11d9-4b87-a072-a154fdd8cdc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d099ac28-a914-47ad-b66e-4908f8ab49df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54015043-c156-4e52-9f60-2dc857d84b3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d099ac28-a914-47ad-b66e-4908f8ab49df",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "e1d7f13e-3866-46ab-a844-7ec477c07ff2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "15860619-a6c2-4847-b44e-f572423f290d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1d7f13e-3866-46ab-a844-7ec477c07ff2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a42ff78c-06ea-4834-8d56-0b40db585677",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1d7f13e-3866-46ab-a844-7ec477c07ff2",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "461ca87d-0566-4159-ad17-cbcb354ee79b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "c2a55b38-0839-4414-98ad-ff12b135330a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "461ca87d-0566-4159-ad17-cbcb354ee79b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57836469-078c-499a-8ef0-76802fc5029c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "461ca87d-0566-4159-ad17-cbcb354ee79b",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "b61be3f7-7a5e-4f53-9ae2-9fcac29cbd0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "1ab44344-217f-44bf-8529-b9ce595630b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b61be3f7-7a5e-4f53-9ae2-9fcac29cbd0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d84bb3f9-7827-43da-9ec0-c6bd5c5bbcc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b61be3f7-7a5e-4f53-9ae2-9fcac29cbd0b",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "0c26068c-2b9a-4320-bc9e-372c3e590047",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "59905240-a8e3-4743-9a99-60f0caabf413",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c26068c-2b9a-4320-bc9e-372c3e590047",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af529750-1bb7-4c3e-819a-8f3d1914dde9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c26068c-2b9a-4320-bc9e-372c3e590047",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "3739790d-400e-4f2c-9396-56028243f404",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "fea867dc-e930-4292-b57f-5bf14058d688",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3739790d-400e-4f2c-9396-56028243f404",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0afb688c-074c-48c1-ba5c-2c651941084b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3739790d-400e-4f2c-9396-56028243f404",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "f0948b65-ad85-43af-b430-71697ffdae22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "73b90f74-8a54-4f14-9109-7c66581adfcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0948b65-ad85-43af-b430-71697ffdae22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26ef2c23-d94b-4dbb-92de-39a6594d7ad6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0948b65-ad85-43af-b430-71697ffdae22",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "b632923e-f1d8-4480-8ea4-c271f21ac6b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "3a9fd87a-d3e4-4469-b0bd-50c4d6ef2f08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b632923e-f1d8-4480-8ea4-c271f21ac6b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08c105f2-95e9-4d2b-a52e-1c26f3934d27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b632923e-f1d8-4480-8ea4-c271f21ac6b7",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "360bcded-28bb-45f9-84c5-aa517711dd31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "0c32498f-af85-40df-9c5d-b79aa1abf9af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "360bcded-28bb-45f9-84c5-aa517711dd31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24325060-fb0f-41c6-abcd-34b88f97bf24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "360bcded-28bb-45f9-84c5-aa517711dd31",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "8eb94cfa-5fca-4bc4-8f52-af8f1af05054",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "5fee34d5-dd27-422f-be3f-b16d20a275e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8eb94cfa-5fca-4bc4-8f52-af8f1af05054",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20232614-4a7b-4471-8300-a6d40e36aaeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8eb94cfa-5fca-4bc4-8f52-af8f1af05054",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "b832c7ba-0f76-494a-9703-147acc497b62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "1e7d4843-168e-44ad-a03c-b7cb399d5ce5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b832c7ba-0f76-494a-9703-147acc497b62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "774065da-6fd6-4543-844e-811aff6731ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b832c7ba-0f76-494a-9703-147acc497b62",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "4ef02d71-d674-4bb2-b67c-4208695bafe9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "efff62a0-a7ad-4dae-99b9-f287d1cbf554",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ef02d71-d674-4bb2-b67c-4208695bafe9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b31f78e-c346-4694-9855-0688781cc4a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ef02d71-d674-4bb2-b67c-4208695bafe9",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "82b23242-f586-4539-b891-b29e274556c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "1455769d-22b7-40b4-958c-232c88067c99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82b23242-f586-4539-b891-b29e274556c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "928d36ec-2d3f-4e2b-a46d-5eda12cf5c68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82b23242-f586-4539-b891-b29e274556c1",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "5637d419-1072-4b32-b8dc-b1cdfb808158",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "4bd1374c-3329-4562-8e50-4cfeb37299af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5637d419-1072-4b32-b8dc-b1cdfb808158",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa7735a4-e71d-41a4-9d8e-c0268d142bf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5637d419-1072-4b32-b8dc-b1cdfb808158",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "fcd67609-5fa8-4938-a109-6af6b1a7ac11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "82ab551e-7e6f-4a73-a2e3-4f90496b1d6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcd67609-5fa8-4938-a109-6af6b1a7ac11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a278f135-8885-4d57-8e81-e6cd4baafb46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcd67609-5fa8-4938-a109-6af6b1a7ac11",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "f9981981-5900-461c-b59f-d3c03c169b4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "7beda3cf-0f3b-4739-af25-8fa09d5c8d26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9981981-5900-461c-b59f-d3c03c169b4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc3d7698-a692-4753-86a7-bc7bdc16f63c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9981981-5900-461c-b59f-d3c03c169b4e",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "28e9adc2-3345-4ac8-b8ca-b77134b6afe4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "3393b1c4-c767-4835-a734-0ae70fa7c0c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28e9adc2-3345-4ac8-b8ca-b77134b6afe4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dfb872a-68fc-4a97-8c0d-cf57e4c7e5e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28e9adc2-3345-4ac8-b8ca-b77134b6afe4",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "aa3a5b37-f5cd-4f36-8c17-6ce2d8df3e1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "2535dbfe-a5a0-4cd2-8d28-93054aee8d5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa3a5b37-f5cd-4f36-8c17-6ce2d8df3e1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "935704d3-eb71-4477-90fa-978c0861fe62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa3a5b37-f5cd-4f36-8c17-6ce2d8df3e1f",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "373dce74-5e68-43b6-85f8-0a4477771b13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "a4e749d6-4481-482e-b77d-0c0a3579be9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "373dce74-5e68-43b6-85f8-0a4477771b13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b74280c5-3269-406b-aef8-06a0d03ae8f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "373dce74-5e68-43b6-85f8-0a4477771b13",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "df790aa7-4a5b-4c4a-9bd6-8dbe1d3bafb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "ad744847-1fa7-425b-bc12-352484245c05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df790aa7-4a5b-4c4a-9bd6-8dbe1d3bafb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56797ad5-7b7d-4c57-8710-9ae6195ffa17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df790aa7-4a5b-4c4a-9bd6-8dbe1d3bafb7",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "5c1b89e3-9044-4f1a-83ad-d36ce17c9b23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "b12f10b1-decb-44a6-bf65-c460536fd26e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c1b89e3-9044-4f1a-83ad-d36ce17c9b23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "443578e9-fdf9-47fc-8b49-073f65e0c27d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c1b89e3-9044-4f1a-83ad-d36ce17c9b23",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "6103d161-4a26-479a-a0a8-e4328f4960d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "108a7f47-709c-4f44-9156-beae8a672d95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6103d161-4a26-479a-a0a8-e4328f4960d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e868ac38-5c25-4d82-80ea-f31f1dc4a83c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6103d161-4a26-479a-a0a8-e4328f4960d0",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "81442f5e-1367-4f1f-9ac1-2a67ae70e1d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "90955c52-d6c0-40dd-8382-180535b181b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81442f5e-1367-4f1f-9ac1-2a67ae70e1d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c171f327-d467-4193-baa1-ff1fe94d00b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81442f5e-1367-4f1f-9ac1-2a67ae70e1d3",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "c5b3ba08-f9ed-472a-b6b6-a9942399b0c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "40b36c31-03d7-40ab-9f06-96e637a5c415",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5b3ba08-f9ed-472a-b6b6-a9942399b0c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "992993ff-d3e4-4b15-baeb-a4c5390adf91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5b3ba08-f9ed-472a-b6b6-a9942399b0c7",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "f267d192-c493-4600-87c2-f5a3ac925397",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "e575ec8f-29d2-4e58-b5e7-eb7166daa394",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f267d192-c493-4600-87c2-f5a3ac925397",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff5b5bb1-e351-4f85-a680-03afec3da2c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f267d192-c493-4600-87c2-f5a3ac925397",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "f7481a2f-91a1-4063-9594-5fe1dc5b7c4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "e3eec974-e542-48b9-b0cb-fa1c1fed70fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7481a2f-91a1-4063-9594-5fe1dc5b7c4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86f4d1fa-6900-4192-9812-547d82d6c50a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7481a2f-91a1-4063-9594-5fe1dc5b7c4b",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "5980c922-5836-45e2-b334-40220e0f035e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "bcfcfa64-43c1-4a53-8ac3-a1c7a07a062a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5980c922-5836-45e2-b334-40220e0f035e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a363347-502c-4481-a4e6-37a8f2283959",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5980c922-5836-45e2-b334-40220e0f035e",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "3cf1267e-9166-4975-b7e6-95c3112574eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "fc68684e-74f1-43bf-b8ce-e1f8705ab866",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cf1267e-9166-4975-b7e6-95c3112574eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "535115fc-da00-4d8b-9e85-4470c66f2a3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cf1267e-9166-4975-b7e6-95c3112574eb",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        },
        {
            "id": "bfe6bbda-8c99-4b9b-90cb-00712e9bdde7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "compositeImage": {
                "id": "ccc0821c-393f-44cd-bdb1-76c972eb2426",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfe6bbda-8c99-4b9b-90cb-00712e9bdde7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "861b631e-5279-4652-beeb-4a226e18144a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfe6bbda-8c99-4b9b-90cb-00712e9bdde7",
                    "LayerId": "66d02646-810c-4736-8e32-bf55276e8fec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "66d02646-810c-4736-8e32-bf55276e8fec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67a957e4-1688-4548-9cf6-8b69a9737449",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 175,
    "xorig": 63,
    "yorig": 87
}