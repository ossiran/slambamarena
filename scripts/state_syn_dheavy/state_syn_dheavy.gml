live_call();

if(state_new == 1)
{
    with(hurtbox0)
	{
	    xoffset = -12;
	    yoffset = -13;
	    image_xscale  = 24;
	    image_yscale  = 40;
	    xshift  = 0;
	}
	
	with(hurtbox1)
	{
	    xoffset = -8;
	    yoffset = -31;
	    image_xscale  = 16;
	    image_yscale  = 18;
	    xshift = 0;
	}
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -13;
	    image_xscale  = 16;
	    image_yscale  = 40;
	    xshift = 0;
	}
	
	chainmove = 0;
	attackframe0 = 10;
	attackframe1 = 12;
	attackframe2 = 14;
	endstate = 33;
}

sprite_index = spr_lsyn_66h;

if(state_timer < attackframe0 + 1 && state_timer > 1)
{
    vx = Approach(vx,5.5 * image_xscale, 3);
}
//attack
if(state_timer == attackframe0)
{
    hitbox0 = create_hitbox(-16,-56,25,20,6,4,10,20,12,15,-1,0,0,4,"antiair",-1);
    attackhurtbox0 = create_hurtbox(8 * image_xscale, -17,22 * image_xscale,16,8,-2);
    hitbox0.hitsound = snd_hitsound1;
    hitbox0.scaling = 1;
}

if(state_timer == attackframe1)
{
    hitbox0 = create_hitbox(9,-30,62,35,4,6,14,18,12,15,-1,0,0,4,"antiair",-1);
    hitbox1 = create_hitbox(9,-45,53,15,4,6,14,18,12,15,-1,0,0,4,"antiair",-1);
    hitbox2 = create_hitbox(2,-60,33,15,4,6,14,18,12,15,-1,0,0,4,"antiair",-1);
    hitbox0.hitsound = snd_hitsound1;
    hitbox0.scaling = 1;
    hitbox1.hitsound = snd_hitsound1;
    hitbox1.scaling = 1;
    hitbox2.hitsound = snd_hitsound1;
    hitbox2.scaling = 1;
}

if(state_timer == attackframe2)
{
    hitbox0 = create_hitbox(9,-24,62,32,8.25,1,14,19,12,15,-1,1,1,4,"mid",2);
    hitbox1 = create_hitbox(9,8,55,15,8.25,1,14,19,12,12,-1,1,1,4,"mid",2);
    hitbox0.hitsound = snd_hitsound1;
    hitbox0.scaling = 1;
    hitbox1.hitsound = snd_hitsound1;
    hitbox1.scaling = 1;
    hitbox2.hitsound = snd_hitsound1;
    hitbox2.scaling = 1;
}

attack_base(endstate,0,0,1,1,0);