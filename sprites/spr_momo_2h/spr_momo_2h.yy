{
    "id": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_2h",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 24,
    "bbox_right": 157,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "952c4a3b-e27d-4717-bc98-fcffcaa5d9c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "56a077c6-5174-458d-86b5-1ffa12ab5760",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "952c4a3b-e27d-4717-bc98-fcffcaa5d9c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79d0c2c0-0d03-44d4-a76e-e0589a4dda83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "952c4a3b-e27d-4717-bc98-fcffcaa5d9c1",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "0e4b2213-61d7-4b99-8cc5-768d79ddfff6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "15279771-cbf7-47d4-9289-f2084d53620d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e4b2213-61d7-4b99-8cc5-768d79ddfff6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d20dd22-12d5-4c1c-855e-420e64579171",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e4b2213-61d7-4b99-8cc5-768d79ddfff6",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "6a3f5e07-0eb5-43a0-85af-7373b7fcf678",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "30428398-5001-4044-be2f-fba2856c0dd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a3f5e07-0eb5-43a0-85af-7373b7fcf678",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b83c5f93-19cc-4038-b708-51841b4f15f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a3f5e07-0eb5-43a0-85af-7373b7fcf678",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "74eb44fd-b1e1-4b14-88e7-53a0f147f934",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "6a600932-ec31-417f-9e66-4566576a561f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74eb44fd-b1e1-4b14-88e7-53a0f147f934",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3c77d5e-d5f8-4f71-ab37-faa1ebd55100",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74eb44fd-b1e1-4b14-88e7-53a0f147f934",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "189421c2-04fe-4450-8c5d-92f4d3b5f9d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "e14e052a-9035-4fd1-9322-fb5bc47fe442",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "189421c2-04fe-4450-8c5d-92f4d3b5f9d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f1c948c-fea5-4201-819f-947dbd99d5fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "189421c2-04fe-4450-8c5d-92f4d3b5f9d6",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "798a6f6e-df01-4a9e-a22b-f3028bdfe0e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "47c9bbb3-c5e2-46b4-bb45-f7f6176ffc89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "798a6f6e-df01-4a9e-a22b-f3028bdfe0e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1425e8c6-0cb9-4f51-8a82-588486205617",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "798a6f6e-df01-4a9e-a22b-f3028bdfe0e4",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "5b425e39-0b8a-4224-ba07-586d898f989e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "69b35a70-af25-4c8b-ac7a-d2e158677580",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b425e39-0b8a-4224-ba07-586d898f989e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc80454e-1aa3-4c8a-8011-776d9731126d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b425e39-0b8a-4224-ba07-586d898f989e",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "6263ee8b-7eba-484f-9785-500f2592f8d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "df4d8931-0098-420b-97d8-3912a7a0a504",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6263ee8b-7eba-484f-9785-500f2592f8d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cc19e1d-d2ce-48d8-b6b4-ed614f77165c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6263ee8b-7eba-484f-9785-500f2592f8d8",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "2be2fc87-cd6f-42f4-a79f-a41b53b7c7f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "7064b650-0818-4425-ab5f-e9db1b9aa685",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2be2fc87-cd6f-42f4-a79f-a41b53b7c7f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7451aa6-59d7-43cb-990c-6f8d622cf27f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2be2fc87-cd6f-42f4-a79f-a41b53b7c7f4",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "7da82841-64dc-47f4-9432-8b73ec5a7958",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "bbea73e9-d310-4c97-a676-a6830c2eb05d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7da82841-64dc-47f4-9432-8b73ec5a7958",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a61e7aaf-8096-46a9-ac5f-c6911b194c6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7da82841-64dc-47f4-9432-8b73ec5a7958",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "0b5bedfc-06cf-4e6a-8ad0-91508fc6e43a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "dc6e4d4d-defb-4af9-bc7b-a2cda8058dc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b5bedfc-06cf-4e6a-8ad0-91508fc6e43a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb557013-d56d-475d-b027-188eb1a83cbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b5bedfc-06cf-4e6a-8ad0-91508fc6e43a",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "6e91394b-dce6-4b5e-940d-4f5cea2a3a14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "df8e7c33-1aff-4407-9b9c-c32413aa3d93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e91394b-dce6-4b5e-940d-4f5cea2a3a14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4ebd2e1-0b4d-4e82-810e-1bf6ace8394b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e91394b-dce6-4b5e-940d-4f5cea2a3a14",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "bfc41106-8a92-4a0b-83b6-c2e0f1947d71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "c21cc78c-00fb-44d3-adf5-f69cb0a5bef5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfc41106-8a92-4a0b-83b6-c2e0f1947d71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a4fc657-9b29-4de1-b880-4de866148a4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfc41106-8a92-4a0b-83b6-c2e0f1947d71",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "3a9134c3-d5b8-40b9-ba78-d27aadee89f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "4f62f079-47dd-4681-8570-bb9e066c6032",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a9134c3-d5b8-40b9-ba78-d27aadee89f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7b705c4-031e-4874-99b3-67f1f3fff291",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a9134c3-d5b8-40b9-ba78-d27aadee89f2",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "0dc641ea-cf8a-471f-8406-c2e7932ebfc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "1ac005a2-98f4-40b6-9a90-e80e459efe74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dc641ea-cf8a-471f-8406-c2e7932ebfc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bf1c4b5-4061-4cc7-b1aa-a02efad68902",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dc641ea-cf8a-471f-8406-c2e7932ebfc2",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "aae16c3e-ece7-483d-a1aa-a11dbaaedcf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "5980b803-6ff9-49f7-947e-4c677be4f0d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aae16c3e-ece7-483d-a1aa-a11dbaaedcf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb6c1191-79b4-4497-869f-eb7e4aa3d40a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aae16c3e-ece7-483d-a1aa-a11dbaaedcf7",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "7a865e11-446f-49fa-9cc2-64558a044ab3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "046bec75-77ab-4dc6-8ff0-f58c3b9aa14b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a865e11-446f-49fa-9cc2-64558a044ab3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "415f2b9a-cfd7-4ebe-9fc2-9104ff82c6e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a865e11-446f-49fa-9cc2-64558a044ab3",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "2d44d3f9-ca79-4153-bfd6-57ed093b6da8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "6c3c9bbc-ac94-4d27-a58e-92a010e994bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d44d3f9-ca79-4153-bfd6-57ed093b6da8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e487f5be-352d-41cc-964c-1321fea9c85f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d44d3f9-ca79-4153-bfd6-57ed093b6da8",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "cc016caa-5573-44c5-b80e-4fa88d40bd0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "f4201eab-8993-4574-8933-9df9b1d8f08e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc016caa-5573-44c5-b80e-4fa88d40bd0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31837f9d-d0b0-428a-a604-bb512b6ce445",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc016caa-5573-44c5-b80e-4fa88d40bd0f",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "ec49733c-ac51-44fa-b254-14b44fd8b88c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "2df67f7f-4810-4377-9ab2-315c04617786",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec49733c-ac51-44fa-b254-14b44fd8b88c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a466f84-8755-4a3a-be3c-8e9abfa4ef59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec49733c-ac51-44fa-b254-14b44fd8b88c",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "121bc61b-665b-41cb-9eec-e9e93acd5a74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "94fbc027-d48e-4e99-a199-26dc67d1de68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "121bc61b-665b-41cb-9eec-e9e93acd5a74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7743a8b3-0f5c-45d8-a45e-5a6fa05c04bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "121bc61b-665b-41cb-9eec-e9e93acd5a74",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "065089e9-6b0d-4f57-bd24-48aa465e8299",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "996a6ac4-5b07-4e82-b6c8-d59b4bd0aab5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "065089e9-6b0d-4f57-bd24-48aa465e8299",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ec48e1b-ca2b-4981-834a-6158fd019877",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "065089e9-6b0d-4f57-bd24-48aa465e8299",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "c613966d-8fc2-4c63-9bb2-55f62beb0d1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "6ef4b269-609c-4179-89b2-c430364ab037",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c613966d-8fc2-4c63-9bb2-55f62beb0d1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43168c54-939f-4f0b-85db-e348a8b204e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c613966d-8fc2-4c63-9bb2-55f62beb0d1a",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "75932ae2-5c65-4df8-813d-022efd8fa90f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "760252e9-91d0-40b5-8d27-29298124b44f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75932ae2-5c65-4df8-813d-022efd8fa90f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68c9c011-47c3-47af-b620-2ec6e7853e70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75932ae2-5c65-4df8-813d-022efd8fa90f",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "3cd323ee-e44c-4a46-a55d-478eb1e57e02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "8eba7055-312c-44c0-8cd3-8574ea7be91a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cd323ee-e44c-4a46-a55d-478eb1e57e02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ecd98fc-fe84-466b-8cca-580503921f43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cd323ee-e44c-4a46-a55d-478eb1e57e02",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "660356c6-4ce9-4db3-8ddd-28ed89f05b63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "356284e1-233b-4a5d-80ee-ddd002d51afb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "660356c6-4ce9-4db3-8ddd-28ed89f05b63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "333d5e37-6155-4950-b66f-ba6d590efbe2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "660356c6-4ce9-4db3-8ddd-28ed89f05b63",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "a3d8633f-351d-4df2-9bf0-647b976a6fc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "23ec7e92-1479-4cec-ac7f-37540f19da43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3d8633f-351d-4df2-9bf0-647b976a6fc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c615f039-65bd-4fb4-b29e-196caed7ffdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3d8633f-351d-4df2-9bf0-647b976a6fc3",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "0039eb01-8aa3-4a0e-a511-893c1b4a9abf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "7fefcca5-a4e7-4b2e-85ba-57cce36747d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0039eb01-8aa3-4a0e-a511-893c1b4a9abf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6407f137-09e9-4ede-8084-a9f8f53eb71e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0039eb01-8aa3-4a0e-a511-893c1b4a9abf",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "447f9317-fca7-4bd0-99e8-f421f36b1ef6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "76e21bb2-4cf4-4aa2-a7bc-2f3c19919590",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "447f9317-fca7-4bd0-99e8-f421f36b1ef6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32eba3f4-5df0-4465-93fa-847ea28167f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "447f9317-fca7-4bd0-99e8-f421f36b1ef6",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "d599d01e-db77-4bb8-9afa-3b3dbec27eae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "50516ee7-af2e-4720-acaf-05ddbb7ea8d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d599d01e-db77-4bb8-9afa-3b3dbec27eae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bcd5b46-9abc-4538-bdb0-e253e37c7d33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d599d01e-db77-4bb8-9afa-3b3dbec27eae",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "8ed4c614-1c17-49c3-b1ed-27d077bd2853",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "1aa55776-3e3c-45aa-8136-fe4f3a039a8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ed4c614-1c17-49c3-b1ed-27d077bd2853",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5cf9c7c-b0d7-4c02-9f26-e0f550f727a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ed4c614-1c17-49c3-b1ed-27d077bd2853",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "25e5f9b6-2afe-4090-8cc5-fac2b20c9f12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "34452631-e542-40b9-a515-9dd98e6f85d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25e5f9b6-2afe-4090-8cc5-fac2b20c9f12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e5a8011-c626-4811-abd6-1390dc805a9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25e5f9b6-2afe-4090-8cc5-fac2b20c9f12",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "e71d8192-5c50-48cf-8f67-96b4ee44349e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "cf4c5d3a-ce45-4fb5-bf47-8b92bd063cf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e71d8192-5c50-48cf-8f67-96b4ee44349e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e63de9d7-79ea-4d36-91bf-3bf08cb9a4a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e71d8192-5c50-48cf-8f67-96b4ee44349e",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "15442cfc-530d-4b2d-99c5-9d3a5687ca43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "9314adc6-0c47-4da1-89a6-ac5fcd9ce922",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15442cfc-530d-4b2d-99c5-9d3a5687ca43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "612152c0-e369-4513-ab13-a1170c482602",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15442cfc-530d-4b2d-99c5-9d3a5687ca43",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        },
        {
            "id": "dc73835d-fdbc-46e4-926e-f2dca44f2da3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "compositeImage": {
                "id": "9ab0a83a-73b9-40a6-a10b-cd1c93bbde19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc73835d-fdbc-46e4-926e-f2dca44f2da3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20ab55a9-3956-498c-affb-193db50e663a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc73835d-fdbc-46e4-926e-f2dca44f2da3",
                    "LayerId": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "abcb244b-73c0-4c9f-8a86-cbe7c22e870f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0bbf51e9-6fef-4422-9110-a59bd92cd935",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 196,
    "xorig": 63,
    "yorig": 87
}