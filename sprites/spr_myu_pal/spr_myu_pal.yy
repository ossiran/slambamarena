{
    "id": "bf2cdb4c-2715-4ca4-bbba-e790d66d682f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_myu_pal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51ab0866-2c85-4a7b-8e3e-fa2ffc78d453",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf2cdb4c-2715-4ca4-bbba-e790d66d682f",
            "compositeImage": {
                "id": "54c3c57a-8d24-4ea9-b0aa-86db81a0a75f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51ab0866-2c85-4a7b-8e3e-fa2ffc78d453",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88112d74-487a-4fb1-8d7f-bcff2d4bf7d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51ab0866-2c85-4a7b-8e3e-fa2ffc78d453",
                    "LayerId": "3b37e445-3239-48c6-a849-55dc4d122755"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "3b37e445-3239-48c6-a849-55dc4d122755",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf2cdb4c-2715-4ca4-bbba-e790d66d682f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 0,
    "yorig": 0
}