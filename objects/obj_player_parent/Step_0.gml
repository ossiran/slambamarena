/// @description controls


if(gxrecover > 0)
{
    gxrecover--;
}

//trail
if(trailduration > 0)
{
    trailduration--;
    trailarray[trailcounter,2] = x;
    trailarray[trailcounter,3] = y;
    trailarray[trailcounter,0] = sprite_index;
    trailarray[trailcounter,1] = image_index;
    trailarray[trailcounter,4] = image_xscale;
}
else
{
    trailcounter = 0;
    if(trailarray[0,0] != 0)
    {
        for(i = 0; i < 5; i++)
        {
            for(i2 = 0; i2 < 5; i2++)
            {
                trailarray[i,i2] = 0;
            }
        }
    }
}

if(gxrecover == 0 && gx != 900 && gbust == 0)
{
        gx = Approach(gx, 900, 1);
}

if(gbust > 0)
{
    if(gx > 0)
    {
        gx = Approach(gx,0,gbustrate);
    }
    else
    {
        gbust = 0;
    }
}

