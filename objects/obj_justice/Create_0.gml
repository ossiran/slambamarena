/// @description Insert description here
// You can write your code in this editor
state_machine_init();

state_create("qcf",state_justice_qcf);
state_create("qcb",state_justice_qcb);
state_create("summon",state_justice_summon);

state_init("summon");

//debug
myname = "justice";

endstate = 0;
attackframe0 = 0;
attackframe1 = 0;
attackframe2 = 0;
attackframe3 = 0;
attackframe4 = 0;
attackframe5 = 0;

hitting = 0;

owner = 0;
frontOrBack = 0;

hurtbox = create_hurtbox(-13,-1000,0,0,-1,-2);
hurtbox.owner = owner;
hurtbox.following = id;
hitbox0 = -1;
hitbox1 = -1;
hitbox2 = -2;

image_xscale = owner.image_xscale;



image_alpha = 0;