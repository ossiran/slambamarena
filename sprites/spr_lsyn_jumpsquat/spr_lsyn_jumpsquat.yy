{
    "id": "9fe6af1a-3e97-4b4c-9316-587b0f69d02d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lsyn_jumpsquat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 49,
    "bbox_right": 79,
    "bbox_top": 48,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87034bfb-8f2b-4625-835b-8a5ae70108cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9fe6af1a-3e97-4b4c-9316-587b0f69d02d",
            "compositeImage": {
                "id": "4d7b4e56-d6de-4907-a1f6-73326c5d0031",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87034bfb-8f2b-4625-835b-8a5ae70108cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7ba18bd-b1bc-42ab-ac72-5b95e8b2ebfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87034bfb-8f2b-4625-835b-8a5ae70108cd",
                    "LayerId": "21391160-0c2a-42c5-9a1e-c6b596b47b01"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "21391160-0c2a-42c5-9a1e-c6b596b47b01",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9fe6af1a-3e97-4b4c-9316-587b0f69d02d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}