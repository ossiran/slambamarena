if(state_new == 1)
{

    //gravity
    if(!onground() || vy < 0)
    {
    	sprite_index = hurt_spriteA;
    	stagger = 0;
    }
    else
    {
        sprite_index = hurt_spriteG;
        if(hit == 1)
        {
            hit = 0;
        }
    }
    
    /*
    //apply counter hit when hit
    if(onground() && last_state != 53)
    {
        hitair = 0;
    }
    */
    
    if(last_state == state_bill_hitfall)
        {
        	with(hurtbox0)
        	{
        	    yoffset = -26;
        	    xoffset = -38;
        	    image_yscale  = 44;
        	    image_xscale  = 76;
        	    xshift  = 0;
        	}
        	
        	with(collisionbox)
        	{
        	    xoffset = -8;
        	    yoffset = -15;
        	    image_xscale  = 16;
        	    image_yscale  = 47;
        	    xshift = -2
        	}
        	sprite_index = fall_sprite;
        }
}




if(!onground())
{
    if(hitfall == -2)
    {
        //need to proof this for multiple hurtboxes
        with(hurtbox0)
        {
        	xoffset = -13;
        	yoffset = -1000;
        	image_xscale  = 26;
        	image_yscale  = 75;
        	xshift  = -2;
        }
        	
        with(collisionbox)
        {
        	 xoffset = -8;
        	 yoffset = -1000;
        	 image_xscale  = 0;
        	 image_yscale  = 0;
        	 xshift = -2
        }
    }
    
    hitair = 1;
}

//hitpause
if(hitpause == 0)
{
    vy = yhit;
    // need to improve this. knockback decay
    
    /*
    if(onground())
    {
        xhit = xhit * 0.85;
    }
    else
    {
        xhit = xhit * 0.95;
    }
    */
    
    
    //if(abs(xhit) < 1)
    //{
    //    xhit = xhit * sign(xhit);
    //}
    
    
    if(yhit < 0)
    {
        yhit = yhit * 0.8;
    }
    else if(yhit > 0)
    {
        yhit = yhit * 1.1;
    }
    else if(!onground() || hitair = 1)
    {
        yhit = -2;
    }
    
    if(!onground())
    {
        vy = Approach(vy,vyMax,grav);
        vx = xhit/1.9;
    }
    else
    {
        vx = xhit;
        xhit = Approach(xhit,0, ground_fric);
        vx = Approach(vx, 0, ground_fric);
    }
    
    
    if(onground())
    {
        if(!place_meeting(x + 10,y, obj_solid) && !place_meeting(x - 10,y, obj_solid))
        {
            
            //pushback
            with(otherplayer)
            {
                if(other.lastmod != "projectilemid" && other.lastmod != "projectilelow" && other.lastmod != "projectileoverhead")
                {
                    if(onground())
                    {
                        vx = Approach(vx,-other.xhit /2 + ground_fric * (sign(-other.xhit)) + ( floor(other.scaling / 5 ) * sign(-other.xhit)), other.xhit * image_xscale);
                    }
                    else
                    {
                        //vx = Approach(vx,-other.xhit /4 + ground_fric * (sign(-other.xhit)) + ( floor(other.scaling / 5 ) * sign(-other.xhit)), other.xhit * image_xscale);
                    }
                }
            }
        }
        else
        {
            //pushback
            with(otherplayer)
            {
                if(other.lastmod != "projectilemid" && other.lastmod != "projectilelow" && other.lastmod != "projectileoverhead")
                {
                    if(onground())
                    {
                        vx =   Approach(vx,-other.xhit /1.5 + ground_fric * (sign(-other.xhit)) + ( floor(other.scaling / 5 ) * sign(-other.xhit)), other.xhit * image_xscale);
                    }
                    else
                    {
                        //vx = Approach(vx,-other.xhit /3.5 + ground_fric * (sign(-other.xhit)) + ( floor(other.scaling / 5 ) * sign(-other.xhit)), other.xhit * image_xscale);
                    }
                }
            }
        }
    }
}
else
{
    vx = 0;
    vy = 0;
    
    //otherplayer.xhit = -xhit;
}


//switch out when duration is over
if(hitstun <= 0)
{
        if(onground())
        {
            if(stagger == 1)
            {
                xhit = 0;
                yhit = 0;
                state_switch("stagger");
            }
            else
            {
                state_switch("stand");
                xhit = 0;
                yhit = 0;
                end_combo();
            }
        }
        else
        {
            state_switch("hitfall");
            xhit = 0;
            yhit = 0;
        }
}

//counter. pauses 1 frame when hit
if(hitstun > 0 && hitpause == 0)
{
    hitstun = Approach(hitstun, 0, 1);
}

if(hitpause > 0)
{
    hitpause = Approach(hitpause,0,1);

}


if(juggle > 5)
{
    if(hitpause == 0)
    {
        state_switch("recovery");
    }
    else
    {
        //need shifting hitboxes out of map
        with(hurtbox0)
        {
        	xoffset = -13;
        	yoffset = -1000;
        	image_xscale  = 0;
        	image_yscale  = 0;
        	xshift  = 0;
        }
        
        if(hurtbox1 != -1)
        {
            with(hurtbox1)
            {
            	xoffset = -13;
            	yoffset = -1000;
            	image_xscale  = 0;
            	image_yscale  = 0;
            	xshift  = 0;
            }
        }
        
        if(hurtbox2 != -1)
        {
            with(hurtbox2)
            {
            	xoffset = -13;
            	yoffset = -1000;
            	image_xscale  = 0;
            	image_yscale  = 0;
            	xshift  = 0;
            }
        }
        
        with(collisionbox)
        {
        	 xoffset = -8;
        	 yoffset = -1000;
        	 image_xscale  = 0;
        	 image_yscale  = 0;
        	 xshift = -2
        }
    }
}

//causing knockdown sooner than intended. if hit while touching the ground, or while bouncing out of knockdown they go into knockdown anyway
if(hitair > 0 && onground() == 1 && hitpause == 0 && yhit >= 0)
{
    state_switch("knockdown");
    hitstun = 0;
}

/*
if(grabbed == 1 && onground() && hitair == 0)
{
 state_switch("grabbed");   
}
*/


/*
if(hitfall > 0 && !onground() && hitpause == 0)
{
    hitstun = 0;
    hitair = 0;
    state_switch("recovery");
    hitair = 0;
}
*/