///input_check(key dir, Light, Medium, Heavy, Genie)
//example (6, 0, 1, 0 , 0)
//looks for a forward medium attack. assume you're always on the left.
 var directioncheck = 0;
 var dircheckdist = 0;
 var dircheckdir = 0;
 var lightcheck = 0;
 var mediumcheck = 0;
 var heavycheck = 0;
 var geniecheck = 0;

//light
if(argument1 == 1)
{
	if( input[inputcounter,1] > 0)
	{
	    lightcheck = 1;
	    if(input[inputcounter,1] > dircheckdist)
	    {
	        dircheckdist = 4 - input[inputcounter,1];
	    }
	}
}
//hold check
if(argument1 == 2)
{
	if( input[inputcounter,1] != 0)
	{
	    lightcheck = 1;
	}
}

//medium
if(argument2 == 1)
{
	if( input[inputcounter,2] > 0)
	{
	    mediumcheck = 1;
	    if(input[inputcounter,2] > dircheckdist)
	    {
	        dircheckdist = 4 - input[inputcounter,2];
	    }
	}
}
//hold check
if(argument2 == 2)
{
	if( input[inputcounter,2] != 0)
	{
	    mediumcheck = 1;
	}
}

//heavy
if(argument3 == 1)
{
	if( input[inputcounter,3] > 0)
	{
	    heavycheck = 1;
	    if(input[inputcounter,3] > dircheckdist)
	    {
	        dircheckdist = 4 - input[inputcounter,3];
	    }
	}
}
//hold check
if(argument3 == 2)
{
	if( input[inputcounter,3] != 0)
	{
	    heavycheck = 1;
	}
}

//genie
if(argument4 == 1)
{
	if( input[inputcounter,4] > 0)
	{
	    geniecheck = 1;
	    if(input[inputcounter,4] > dircheckdist)
	    {
	        dircheckdist = 4 - input[inputcounter,4];
	    }
	}
}
//hold check
if(argument4 == 2)
{
	if( input[inputcounter,2] != 0)
	{
	    geniecheck = 1;
	}
}

//direction
if(argument0 != 0)
{
    var dircheckdir = argument0;
    
    if(sign(image_xscale) = -1)
    {
        switch (dircheckdir)
        {
            case 5:
                break;
            case 8:
                break;
            case 2:
                break;
            case 4:
                dircheckdir = 6;
                break;
            case 6:
                dircheckdir = 4;
                break;
            case 1:
                dircheckdir = 3;
                break;
            case 3:
                dircheckdir = 1;
                break;
            case 7:
                dircheckdir = 9;
                break;
            case 9:
                dircheckdir = 7;
                break;
        }
    }
    
    if( input[wrap(inputcounter - dircheckdist, 0, 300),0] == dircheckdir)
    {
        directioncheck = 1;
    }
}

if(directioncheck == sign(argument0) && lightcheck == argument1 && mediumcheck == argument2 && heavycheck == argument3 && geniecheck == argument4)
{
    return 1;
    /*
    if(argument1 + argument2 + argument3 + argument4 > 1)
    {
        if(argument1 + argument2 + argument3 + argument4 == 2)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    else
    {
        if(lightcheck - mediumcheck - heavycheck - geniecheck > 1)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
    */
}
else
{
    return 0;
}