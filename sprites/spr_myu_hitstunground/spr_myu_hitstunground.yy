{
    "id": "a45fbae7-3286-459b-b556-9de479670026",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_myu_hitstunground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 11,
    "bbox_right": 42,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81936925-de3c-4c4d-8b81-5558efdd9d63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45fbae7-3286-459b-b556-9de479670026",
            "compositeImage": {
                "id": "ce8e22a6-e522-4f73-8366-d167fbb3796c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81936925-de3c-4c4d-8b81-5558efdd9d63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe8a70b1-b398-4fb0-ba16-c94c19e5dc14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81936925-de3c-4c4d-8b81-5558efdd9d63",
                    "LayerId": "17c887a6-0e07-4f56-87be-521540097cdc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "17c887a6-0e07-4f56-87be-521540097cdc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a45fbae7-3286-459b-b556-9de479670026",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 32
}