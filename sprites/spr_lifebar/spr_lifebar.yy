{
    "id": "e832d9f7-e67f-46ce-885e-101d2c98a490",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lifebar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 85,
    "bbox_right": 638,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d4e5aa29-580e-47b9-b9be-229970550d20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e832d9f7-e67f-46ce-885e-101d2c98a490",
            "compositeImage": {
                "id": "93ba08b4-8541-4106-94b4-9595db3fee4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4e5aa29-580e-47b9-b9be-229970550d20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb8e143a-19ed-4b4f-9aa9-4d96b540a346",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4e5aa29-580e-47b9-b9be-229970550d20",
                    "LayerId": "44bd3a63-fd6f-4e97-beea-87e166888933"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "44bd3a63-fd6f-4e97-beea-87e166888933",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e832d9f7-e67f-46ce-885e-101d2c98a490",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}