if(state_new == 1)
{
	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -43;
	    image_xscale  = 26;
	    image_yscale  = 75;
	    xshift  = -2;
	}
	
			with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 16;
	    image_yscale  = 47;
	    xshift = -2;
	}
	chainmove = 0;
	attackframe0 = 0;
    attackframe1 = 8;
	attackframe2 = 6;
	attackframe3 = 10;
	endstate = 23;
}

if(state_timer < attackframe1)
{
    vx = 6.5 * image_xscale;
}
else
{
    //friction
    vx = Approach(vx,0,ground_fric * 0.5);
}


if(state_timer == attackframe2)
{
    
    hitbox0 = create_hitbox(4,-9,36,17,2,0 ,8,18,10,10,-1,0,1,5,"mid",-1);
    hitbox0.hitsound = snd_hitsound2;
    hitbox0.scaling = 1.2;
    //hitbox0.counteryhit = -1;
    hitbox0.airxtrayhit = -1;
}

if(state_timer == attackframe3)
{
    hitbox0 = create_hitbox(4,-9,34,17,5.5,0,8,18,13,14,-1,0,3,5,"mid",-1);
    hitbox0.hitsound = snd_hitsound2;
    hitbox0.scaling = 1.2;
    hitbox0.airxtrayhit = -11;
    hitbox0.airxtrahitstun = - 9;
}


sprite_index = spr_bill_fjab;

if(state_timer >= attackframe3)
{
    attack_base(endstate,0,0,1,1,0);
}
else
{
    attack_base(endstate,0,0,0,0,0);
}
