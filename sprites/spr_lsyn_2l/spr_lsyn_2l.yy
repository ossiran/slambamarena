{
    "id": "a0c92b2f-38fd-4564-86d0-4295ffce553a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lsyn_2l",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 45,
    "bbox_right": 99,
    "bbox_top": 52,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d631adac-4d1c-4d43-bf1f-84158faa82da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0c92b2f-38fd-4564-86d0-4295ffce553a",
            "compositeImage": {
                "id": "08c89f94-01c9-499e-a86d-12f689b11719",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d631adac-4d1c-4d43-bf1f-84158faa82da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53b46530-51d6-4475-9b48-ed5a4b41b8eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d631adac-4d1c-4d43-bf1f-84158faa82da",
                    "LayerId": "e9a34de6-7663-4534-9863-16e299e6b6f9"
                }
            ]
        },
        {
            "id": "55951835-2890-49cf-b5bd-9b734a427ec4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0c92b2f-38fd-4564-86d0-4295ffce553a",
            "compositeImage": {
                "id": "64d7a64f-4842-485b-ac28-7f73a74942c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55951835-2890-49cf-b5bd-9b734a427ec4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80b8266d-549f-4365-acfc-761e6e7f9e9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55951835-2890-49cf-b5bd-9b734a427ec4",
                    "LayerId": "e9a34de6-7663-4534-9863-16e299e6b6f9"
                }
            ]
        },
        {
            "id": "65a4f6fc-fcff-40ca-a176-12fdf0ee375d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0c92b2f-38fd-4564-86d0-4295ffce553a",
            "compositeImage": {
                "id": "20380ae1-0e65-4c4a-be4e-853fa4d4f2db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65a4f6fc-fcff-40ca-a176-12fdf0ee375d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e87a3fd-5667-4e38-83b5-685257388bdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65a4f6fc-fcff-40ca-a176-12fdf0ee375d",
                    "LayerId": "e9a34de6-7663-4534-9863-16e299e6b6f9"
                }
            ]
        },
        {
            "id": "efe12be7-5666-4bab-9645-eab2902f12ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0c92b2f-38fd-4564-86d0-4295ffce553a",
            "compositeImage": {
                "id": "9f8b2d60-be0a-4242-89c5-2e9f3a01053e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efe12be7-5666-4bab-9645-eab2902f12ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bac739f-abc3-4488-bd8d-469bbf3e1ad7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efe12be7-5666-4bab-9645-eab2902f12ed",
                    "LayerId": "e9a34de6-7663-4534-9863-16e299e6b6f9"
                }
            ]
        },
        {
            "id": "3f96ed36-d426-4153-bba4-19d69da8d9c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0c92b2f-38fd-4564-86d0-4295ffce553a",
            "compositeImage": {
                "id": "f6feb406-79fd-4c4b-b34c-9123f1ff59c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f96ed36-d426-4153-bba4-19d69da8d9c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c3e9c3d-9218-4354-ac61-0d64b875e3ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f96ed36-d426-4153-bba4-19d69da8d9c7",
                    "LayerId": "e9a34de6-7663-4534-9863-16e299e6b6f9"
                }
            ]
        },
        {
            "id": "60a35e7c-bc45-4f6b-8551-4463536fed98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0c92b2f-38fd-4564-86d0-4295ffce553a",
            "compositeImage": {
                "id": "480b5045-c2e9-457f-8713-4fc290d11b1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60a35e7c-bc45-4f6b-8551-4463536fed98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbfe2058-4f90-4a02-ad7c-27bbfaa10ab4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60a35e7c-bc45-4f6b-8551-4463536fed98",
                    "LayerId": "e9a34de6-7663-4534-9863-16e299e6b6f9"
                }
            ]
        },
        {
            "id": "a86f3400-9486-44cb-9d1d-81c334c8db73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0c92b2f-38fd-4564-86d0-4295ffce553a",
            "compositeImage": {
                "id": "c5a7339c-8446-4e53-8bcd-f93faa048bc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a86f3400-9486-44cb-9d1d-81c334c8db73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e9b154a-8694-4475-964c-534f8eaa1773",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a86f3400-9486-44cb-9d1d-81c334c8db73",
                    "LayerId": "e9a34de6-7663-4534-9863-16e299e6b6f9"
                }
            ]
        },
        {
            "id": "ef91fa82-9436-4148-abf0-0c5e02d72e6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0c92b2f-38fd-4564-86d0-4295ffce553a",
            "compositeImage": {
                "id": "29533399-fe9a-47e6-a293-1bc69fd3f530",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef91fa82-9436-4148-abf0-0c5e02d72e6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fca6d7c-00e6-4210-a1ad-d5821d386804",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef91fa82-9436-4148-abf0-0c5e02d72e6d",
                    "LayerId": "e9a34de6-7663-4534-9863-16e299e6b6f9"
                }
            ]
        },
        {
            "id": "0e6d5220-65a4-4c0b-a2ec-0f67a68d4a66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0c92b2f-38fd-4564-86d0-4295ffce553a",
            "compositeImage": {
                "id": "7983663b-2bf7-491f-a03a-4ab7b0402a9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e6d5220-65a4-4c0b-a2ec-0f67a68d4a66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb6cbe4f-8502-4c3d-a5d0-e4e42139184f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e6d5220-65a4-4c0b-a2ec-0f67a68d4a66",
                    "LayerId": "e9a34de6-7663-4534-9863-16e299e6b6f9"
                }
            ]
        },
        {
            "id": "05a52533-78f3-4ef1-a111-029f0856eafe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0c92b2f-38fd-4564-86d0-4295ffce553a",
            "compositeImage": {
                "id": "0b571c58-a18b-476a-9d2d-b1c1d7c5d550",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05a52533-78f3-4ef1-a111-029f0856eafe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f82b027-f96a-42f4-85ef-1ad8293de430",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05a52533-78f3-4ef1-a111-029f0856eafe",
                    "LayerId": "e9a34de6-7663-4534-9863-16e299e6b6f9"
                }
            ]
        },
        {
            "id": "677b7782-d75b-4d8e-87e1-a753eebcf283",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0c92b2f-38fd-4564-86d0-4295ffce553a",
            "compositeImage": {
                "id": "08e36f06-6c71-4d3d-ac1c-bcce70b27db1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "677b7782-d75b-4d8e-87e1-a753eebcf283",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a977e603-4c60-41dd-b78d-fce68bfe6f88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "677b7782-d75b-4d8e-87e1-a753eebcf283",
                    "LayerId": "e9a34de6-7663-4534-9863-16e299e6b6f9"
                }
            ]
        },
        {
            "id": "6b1e847b-2dc7-473d-8533-652d67c2e949",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0c92b2f-38fd-4564-86d0-4295ffce553a",
            "compositeImage": {
                "id": "9fca9e76-bb2e-4284-95c8-8297dee7a7ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b1e847b-2dc7-473d-8533-652d67c2e949",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7a227ab-5525-44a8-9126-d4fac56c14e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b1e847b-2dc7-473d-8533-652d67c2e949",
                    "LayerId": "e9a34de6-7663-4534-9863-16e299e6b6f9"
                }
            ]
        },
        {
            "id": "d9f1610a-e16c-4ada-bb56-187cb343bdac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0c92b2f-38fd-4564-86d0-4295ffce553a",
            "compositeImage": {
                "id": "c4fa0dd8-a09f-49a4-9c28-30a1b7f4432e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9f1610a-e16c-4ada-bb56-187cb343bdac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8eccc8ed-4ef0-4871-bb1c-ff6946306415",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9f1610a-e16c-4ada-bb56-187cb343bdac",
                    "LayerId": "e9a34de6-7663-4534-9863-16e299e6b6f9"
                }
            ]
        },
        {
            "id": "464ddb11-e637-44f8-8f82-6c3a1a3269ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0c92b2f-38fd-4564-86d0-4295ffce553a",
            "compositeImage": {
                "id": "fe6f7c71-bdd0-4deb-8084-4ab38b42a9fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "464ddb11-e637-44f8-8f82-6c3a1a3269ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e2ff7aa-75a0-4aeb-9ade-5f9ea9869f6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "464ddb11-e637-44f8-8f82-6c3a1a3269ec",
                    "LayerId": "e9a34de6-7663-4534-9863-16e299e6b6f9"
                }
            ]
        },
        {
            "id": "0a49f339-55e9-47ff-9ac5-bd7928ef5615",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0c92b2f-38fd-4564-86d0-4295ffce553a",
            "compositeImage": {
                "id": "35a53363-f084-422e-8677-0fbe0f6240ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a49f339-55e9-47ff-9ac5-bd7928ef5615",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5770686c-8298-4932-92f9-1f08d9fde534",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a49f339-55e9-47ff-9ac5-bd7928ef5615",
                    "LayerId": "e9a34de6-7663-4534-9863-16e299e6b6f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "e9a34de6-7663-4534-9863-16e299e6b6f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a0c92b2f-38fd-4564-86d0-4295ffce553a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 76
}