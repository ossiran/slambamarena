{
    "id": "53e5cb33-02a2-4095-b1d8-8179a84c8406",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_bdash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 147,
    "bbox_left": 54,
    "bbox_right": 96,
    "bbox_top": 73,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ecc264d-5ad8-4954-ad01-34ccc5db25d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53e5cb33-02a2-4095-b1d8-8179a84c8406",
            "compositeImage": {
                "id": "2a372934-7dfb-4427-9089-a7f6dd67707d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ecc264d-5ad8-4954-ad01-34ccc5db25d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05000547-2fe7-4f3f-be78-bbbc040aaca5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ecc264d-5ad8-4954-ad01-34ccc5db25d1",
                    "LayerId": "b7519e3f-e6b9-4622-af80-d44e7fa9a4c1"
                }
            ]
        },
        {
            "id": "0444baad-9734-4ed1-bb4c-d5a62e894089",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53e5cb33-02a2-4095-b1d8-8179a84c8406",
            "compositeImage": {
                "id": "134530ad-9364-4972-a09a-b891301733dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0444baad-9734-4ed1-bb4c-d5a62e894089",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62a98cca-0b59-4ff3-8aa1-98949b88fedd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0444baad-9734-4ed1-bb4c-d5a62e894089",
                    "LayerId": "b7519e3f-e6b9-4622-af80-d44e7fa9a4c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "b7519e3f-e6b9-4622-af80-d44e7fa9a4c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53e5cb33-02a2-4095-b1d8-8179a84c8406",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}