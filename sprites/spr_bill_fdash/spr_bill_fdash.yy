{
    "id": "a52a3253-7684-40a1-86a7-ba3bcfd8cdd2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_fdash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 147,
    "bbox_left": 33,
    "bbox_right": 88,
    "bbox_top": 75,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "446dde6d-b092-4998-a949-cba5a3a8b758",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a52a3253-7684-40a1-86a7-ba3bcfd8cdd2",
            "compositeImage": {
                "id": "1c8ee651-0304-423d-ad98-b37fd0ab39cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "446dde6d-b092-4998-a949-cba5a3a8b758",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ab2aeaf-5aa7-4e79-821a-22ae90edca03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "446dde6d-b092-4998-a949-cba5a3a8b758",
                    "LayerId": "4b503342-7729-470e-972b-fe7d9c311867"
                }
            ]
        },
        {
            "id": "3cc75ecc-c643-465d-9fe2-eafb1f2af54f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a52a3253-7684-40a1-86a7-ba3bcfd8cdd2",
            "compositeImage": {
                "id": "1dbaeb6a-b134-4835-b559-c55c5fdb0b0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cc75ecc-c643-465d-9fe2-eafb1f2af54f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e06b1e9-80d5-4504-b52a-0ede727890ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cc75ecc-c643-465d-9fe2-eafb1f2af54f",
                    "LayerId": "4b503342-7729-470e-972b-fe7d9c311867"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "4b503342-7729-470e-972b-fe7d9c311867",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a52a3253-7684-40a1-86a7-ba3bcfd8cdd2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}