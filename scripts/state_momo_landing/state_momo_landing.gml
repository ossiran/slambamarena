live_call();


if(state_new == 1)
{
    with(hurtbox0)
	{
	    xoffset = -12;
	    yoffset = -3;
	    image_xscale  = 24;
	    image_yscale  = 34;
	    xshift  = 0;
	}
	
	with(hurtbox1)
	{
	    xoffset = -8;
	    yoffset = -21;
	    image_xscale  = 16;
	    image_yscale  = 18;
	    xshift = 0;
	}
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -9;
	    image_xscale  = 16;
	    image_yscale  = 40;
	    xshift = 0;
	}
}



image_index = -1;


//friction
vx = Approach(vx,0,ground_fric);
vy = 0;

if(state_timer == 2)
{
    if(input_check(1,0,0,0,0) || input_check(2,0,0,0,0) || input_check(3,0,0,0,0))
    {
        state_switch("crouch");
    }
    else
    {
        state_switch("stand");
    }
}

sprite_index = spr_momo_crouch;

//hitstun
if(hitstun > 0)
{
    state_switch("hitstun");   
}