{
    "id": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_chbl_fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 26,
    "bbox_right": 139,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5aae419-5e3c-41f1-912b-25b8a4ee34c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "8314c3e6-bd76-4b34-8fd8-f08938120899",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5aae419-5e3c-41f1-912b-25b8a4ee34c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd412dbf-e093-4471-a0d6-e3a1d6a9956d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5aae419-5e3c-41f1-912b-25b8a4ee34c2",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "8f8f806a-c2a3-4b07-85d0-8bab1b9c09d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "d02a20f2-5a23-424e-b524-ef5fa223b927",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f8f806a-c2a3-4b07-85d0-8bab1b9c09d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c222822d-d7e1-40e2-be19-0c6f42b1f988",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f8f806a-c2a3-4b07-85d0-8bab1b9c09d6",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "300ecffd-5fbf-43f2-a34c-70c83d5a8b3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "94a2c397-7ab8-4235-aa6a-6e433bca98d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "300ecffd-5fbf-43f2-a34c-70c83d5a8b3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b56f64f-a759-4a19-b60a-70530f040b0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "300ecffd-5fbf-43f2-a34c-70c83d5a8b3c",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "d0373ee8-167e-4760-9c4a-75727a87e433",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "6836e925-027d-48b1-8ff9-954a8e9c6a46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0373ee8-167e-4760-9c4a-75727a87e433",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d6a3f72-2735-467a-9d93-a40555e10971",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0373ee8-167e-4760-9c4a-75727a87e433",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "747ce3b8-4f5b-42f7-9bfc-15aa41e078a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "d42ae846-e640-4984-bc15-a58999dd6349",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "747ce3b8-4f5b-42f7-9bfc-15aa41e078a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50b5556c-862e-405d-9d5f-1db7e5af31dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "747ce3b8-4f5b-42f7-9bfc-15aa41e078a9",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "3c9f9708-e3b2-44f4-a6db-3548d9c32beb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "cf0c2ce1-ad28-404f-9d38-4cd5948a0375",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c9f9708-e3b2-44f4-a6db-3548d9c32beb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "630e7e73-8140-4ca0-879c-95201f599ed0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c9f9708-e3b2-44f4-a6db-3548d9c32beb",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "a1895efb-012c-4359-b1c0-ae31e6a7edb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "b6670f35-d01c-44f3-9c0c-fb6d61e187f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1895efb-012c-4359-b1c0-ae31e6a7edb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3b57f4b-2b8d-425f-8549-394338a3750e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1895efb-012c-4359-b1c0-ae31e6a7edb3",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "e646a179-b673-4bae-a638-52556bc922a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "0d0e260c-9d5f-4c11-81da-5b53cceee94e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e646a179-b673-4bae-a638-52556bc922a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "004b92d6-45bb-4574-bd5f-e86f132ae8c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e646a179-b673-4bae-a638-52556bc922a4",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "675f36eb-ae1f-49f3-aafb-92fe04152ca6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "f7c7f34b-5995-4c1f-9469-37bbcf8c6e70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "675f36eb-ae1f-49f3-aafb-92fe04152ca6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca2c5ba4-4b05-4e68-b2b8-1c0bc8300a38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "675f36eb-ae1f-49f3-aafb-92fe04152ca6",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "e73317a1-5b18-4421-9973-ccea699eb4ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "6663916e-20cd-4773-9336-e7e59d7fcac4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e73317a1-5b18-4421-9973-ccea699eb4ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aeff1407-1c2d-4a03-8482-2b350c005b5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e73317a1-5b18-4421-9973-ccea699eb4ef",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "ae0776e3-3dae-4954-9727-097dcca4c74e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "ae82352d-04b2-4a32-bab2-102b22a3491b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae0776e3-3dae-4954-9727-097dcca4c74e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9208b386-f6c1-4c02-97de-c8abec92dd20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae0776e3-3dae-4954-9727-097dcca4c74e",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "1824edbe-87bf-41b7-89a0-f87d2c7ca7aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "7e77207c-4ac7-45c8-9149-5df79116653a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1824edbe-87bf-41b7-89a0-f87d2c7ca7aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79c8e378-f149-49f9-b0be-bc0a50aee9f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1824edbe-87bf-41b7-89a0-f87d2c7ca7aa",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "2f9d64e0-6fdb-491c-93bf-3d4a4f2f8377",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "578cd141-b246-43bb-a5a4-2fd9b24d572c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f9d64e0-6fdb-491c-93bf-3d4a4f2f8377",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76390c9d-d995-438b-9b02-10410eec96b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f9d64e0-6fdb-491c-93bf-3d4a4f2f8377",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "4221e437-0302-4033-97dd-179b0fd42186",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "6abf6a7c-6ef8-4199-94f9-a2a82210c9ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4221e437-0302-4033-97dd-179b0fd42186",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e27416dd-e9dd-4783-a20f-40a6e1987282",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4221e437-0302-4033-97dd-179b0fd42186",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "1bb0a9e7-34bf-4ebb-968e-f00a23334e19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "089d23c5-f0f7-4417-8408-b5121522e561",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bb0a9e7-34bf-4ebb-968e-f00a23334e19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb1222a9-81b6-4248-841e-4c82fb134f71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bb0a9e7-34bf-4ebb-968e-f00a23334e19",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "f7535796-b089-4a02-adee-5cc7413ada35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "fb05c386-9f86-41d1-9fe1-df1338f723a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7535796-b089-4a02-adee-5cc7413ada35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c73fd1cf-b90e-4fad-bca7-d587ac1ff3ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7535796-b089-4a02-adee-5cc7413ada35",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "e8aab653-fdf9-4974-a105-3a93d7c7078b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "02b01f8f-0f8a-4d88-a19a-a410f1d7e378",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8aab653-fdf9-4974-a105-3a93d7c7078b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11ac371c-bf77-4b5e-9c9a-d9efb1ea4052",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8aab653-fdf9-4974-a105-3a93d7c7078b",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "577aeeb1-d109-446f-92e1-69b6ccbee26b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "9392f76c-f211-469c-a140-a96df1a7f368",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "577aeeb1-d109-446f-92e1-69b6ccbee26b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c7fca53-f63c-4b5c-af0a-c2a6c0fd3f5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "577aeeb1-d109-446f-92e1-69b6ccbee26b",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "692bfe82-6e12-4c72-bda0-3573086a80bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "3ec18c81-85fd-4d97-b84d-003788afe564",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "692bfe82-6e12-4c72-bda0-3573086a80bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b1031ab-262b-4849-9f87-6a2bdca09099",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "692bfe82-6e12-4c72-bda0-3573086a80bb",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "10d9b8c3-4469-45d8-b9a4-2b0e3b71730b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "c4394ce0-c3a2-4386-b37a-b789fc998bfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10d9b8c3-4469-45d8-b9a4-2b0e3b71730b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9787714d-0cdb-453e-8209-afe30d6957b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10d9b8c3-4469-45d8-b9a4-2b0e3b71730b",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "8d4a6d6d-d5ee-4d6b-9f2c-7c8160f484a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "d64ffd65-3feb-4b8d-8696-12a68f59933b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d4a6d6d-d5ee-4d6b-9f2c-7c8160f484a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c9ad4b3-2903-452d-92b9-79e22f61f49c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d4a6d6d-d5ee-4d6b-9f2c-7c8160f484a3",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "5b1946f0-32a2-4cc5-b0e8-c28db57dd87d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "1c4c8e96-76e8-4dd4-9dd5-cc8453a4905f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b1946f0-32a2-4cc5-b0e8-c28db57dd87d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19f56f01-ce12-4eb1-9992-2445ef1f3d02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b1946f0-32a2-4cc5-b0e8-c28db57dd87d",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "11c81ac9-179e-4982-bf36-70e233c7bbe8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "5475ba2b-a9de-4a8f-a8e4-cce29b9d8d48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11c81ac9-179e-4982-bf36-70e233c7bbe8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "029ba1e5-59e8-439a-8679-1daac057f059",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11c81ac9-179e-4982-bf36-70e233c7bbe8",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "0a3397d1-81de-4225-86c3-de77f3f2f2d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "a11ab392-fb5d-4b73-84d3-8f6ffc6d033d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a3397d1-81de-4225-86c3-de77f3f2f2d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c640a5bc-792e-4b56-8005-1aebc47d2fb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a3397d1-81de-4225-86c3-de77f3f2f2d4",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "21ca5487-98b2-4b3e-bcbe-762981e49e5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "cbf4b8e4-a9de-485d-a1b8-1c849093ab0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21ca5487-98b2-4b3e-bcbe-762981e49e5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a4a9797-c369-44c3-a45c-301d9e83f064",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21ca5487-98b2-4b3e-bcbe-762981e49e5c",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "3644567c-4845-4fd5-9d25-a6721aa2b966",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "537a75b9-957a-45ab-9085-86b835056a64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3644567c-4845-4fd5-9d25-a6721aa2b966",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "652025bd-a6c6-4bed-8248-1820de52628e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3644567c-4845-4fd5-9d25-a6721aa2b966",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "4a531993-8fe8-4c9f-838a-10cab1fd5350",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "32639283-3c47-4b13-b730-e05178cd43a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a531993-8fe8-4c9f-838a-10cab1fd5350",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "459d91a8-2fcc-4930-b820-9c8f1cc926f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a531993-8fe8-4c9f-838a-10cab1fd5350",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "0ce29bd4-ded3-4e2d-b219-92fe3e3bccb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "007d0ea0-b3aa-4fe3-80fb-3727903d73da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ce29bd4-ded3-4e2d-b219-92fe3e3bccb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "253fbca2-321b-4b4e-abe5-895eea80487c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ce29bd4-ded3-4e2d-b219-92fe3e3bccb9",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "1464133b-f3de-4393-821c-05a91c245033",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "e9261334-802d-414a-b140-7cfa5ca71098",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1464133b-f3de-4393-821c-05a91c245033",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa7b7b7b-0306-4dfd-ab3d-6562b17ee026",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1464133b-f3de-4393-821c-05a91c245033",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "ac289aa9-8797-47b6-adaa-b54ebccfb261",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "53966546-05f9-4744-bd87-491771abb5de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac289aa9-8797-47b6-adaa-b54ebccfb261",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bf394ed-4736-48c0-8eb5-baa6bf4cce33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac289aa9-8797-47b6-adaa-b54ebccfb261",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "15aafee8-1391-4b20-bc3a-c0b1803499eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "ebc0e927-9bc9-4f1f-94b9-f3455c0654fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15aafee8-1391-4b20-bc3a-c0b1803499eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0b9f80b-cf03-4c84-af84-a9e558dab18d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15aafee8-1391-4b20-bc3a-c0b1803499eb",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        },
        {
            "id": "9810c4ee-9df9-4cb3-b9e2-b8521cbe83e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "compositeImage": {
                "id": "c7ee64b7-71a3-4d12-8ded-ee5013b2a109",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9810c4ee-9df9-4cb3-b9e2-b8521cbe83e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae17ac1d-6cc8-49ad-8e3e-4c28905cb51e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9810c4ee-9df9-4cb3-b9e2-b8521cbe83e6",
                    "LayerId": "3783c24e-feba-47ba-ac62-4c2368381a23"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "3783c24e-feba-47ba-ac62-4c2368381a23",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "929340cd-11f9-4f40-b9cf-c9d7a1d62e09",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 175,
    "xorig": 63,
    "yorig": 87
}