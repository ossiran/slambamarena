{
    "id": "3e46ddb1-a5e7-4b59-8fd1-8f78df9bbd17",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_qcbm_ice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5bb3f251-03e1-40e9-b2e6-5e4bf0e979a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e46ddb1-a5e7-4b59-8fd1-8f78df9bbd17",
            "compositeImage": {
                "id": "01f137ec-9c7a-4424-a44c-0d4842a0d46b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bb3f251-03e1-40e9-b2e6-5e4bf0e979a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b17bfaf-c6ae-4ed4-b468-df73298933ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bb3f251-03e1-40e9-b2e6-5e4bf0e979a8",
                    "LayerId": "0f156565-ea6b-418f-9464-d13898d73334"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0f156565-ea6b-418f-9464-d13898d73334",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e46ddb1-a5e7-4b59-8fd1-8f78df9bbd17",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}