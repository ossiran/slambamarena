if(state_new == 1)
{
	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -43;
	    image_xscale  = 26;
	    image_yscale  = 75;
	    xshift  = -2;
	}
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -1000;
	    image_xscale  = 0;
	    image_yscale  = 0;
	    xshift = -2
	}
	
	otherplayer.x = x + 40 * image_xscale;
	otherplayer.y = y - 28;
	otherplayer.sprite_index = hurt_spriteA;
}
depth = 100;
otherplayer.depth = 101;

sprite_index = spr_bill_fthrow;
image_index = state_timer - 1;

if(otherplayer.xhit == 0 && state_timer < 30)
{
    otherplayer.x = x + 40 * image_xscale;
	otherplayer.y = y - 18;
}

if(otherplayer.grabbed == -1 )
{
    state_switch("grabtech");
}

if(state_timer == 8)
{
    var rand = random(1000);
    hitbox6 = create_hitbox(0, -37,10, 5, 0,0, 7,18,18,12,0,0,330,1,"mid","billnspecial" + string(rand));
	hitbox6.image_alpha = 1;
	hitbox6.hitsound = snd_hit;
	
	hitbox6.vx = 5.75 * image_xscale;
	hitbox6.scaling = 0;
	audio_play_sound(snd_laser,1,0);
	ex += 2;
}

if(state_timer == 14)
{
    var rand = random(1000);
    hitbox6 = create_hitbox(0, -37,15, 5, 0,0, 7,18,18,12,0,0,330,1,"mid","billnspecial" + string(rand));
	hitbox6.image_alpha = 1;
	hitbox6.hitsound = snd_hit;
	hitbox6.vx = 5.75 * image_xscale;
	hitbox6.scaling = 0;
	audio_play_sound(snd_laser,1,0);
	ex += 1;
}

if(state_timer == 20)
{
    var rand = random(1000);
    hitbox6 = create_hitbox(0, -37,15, 5, 0,0, 7,18,18,12,0,0,330,1,"mid","billnspecial" + string(rand));
	hitbox6.image_alpha = 1;
	hitbox6.hitsound = snd_hit;
	hitbox6.vx = 5.75 * image_xscale;
	hitbox6.scaling = 0;
	audio_play_sound(snd_laser,1,0);
	ex += 1;
}

if(state_timer == 26)
{
    var rand = random(1000);
    hitbox6 = create_hitbox(0, -37,15, 5, 3,-3, 8,12,4,7,-1,0,330,1,"mid","billnspecial" + string(rand));
	hitbox6.image_alpha = 1;
	hitbox6.hitsound = snd_hit;
	hitbox6.vx = 5.75 * image_xscale;
	hitbox6.scaling = 5;
	audio_play_sound(snd_laser,1,0);
	ex += 1;
}



if(state_timer == 52)
{
    state_switch("stand");
    otherplayer.grabbed = 0;
}