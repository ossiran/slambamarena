/// @description Insert description here
// You can write your code in this editor

draw_self();

//draw_text(x, y - 50, "duration :" + string(duration));
//draw_text(x,y - 75, "hitted :" + string(hitted));
if(hitted == 1 && hitsparkcounter < 8)
{
    if(projectile == 1)
    {
        draw_sprite_ext(spr_hitspark,hitsparkcounter,(x + owner.otherplayer.x)/2 , (y + owner.otherplayer.y)/2,random(1) + 0.25, random(1) + 0.25,random(359), c_white, 0.85);
    }
    else if(projectile == 2)
    {
        draw_sprite_ext(spr_hitspark,hitsparkcounter,owner.otherplayer.x , owner.otherplayer.y,random(1) + 0.25, random(1) + 0.25,random(359), c_white, 0.85);
    }
    else
    {
        draw_sprite_ext(spr_hitspark,hitsparkcounter,owner.otherplayer.x , y,random(1) + 0.75, random(1) + 0.75,random(359), c_white, 0.85);
    }
    
    hitsparkcounter ++;
}

if(extrasprite != -1)
{
    draw_sprite_ext(extrasprite, duration, x + image_xscale/2, y + image_yscale/2, sign(image_xscale), 1, 0 ,c_white, 1);
}