live_call();
if (heat > 50)
{
    if(state_new == 1)
    {
        with(hurtbox0)
    	{
    	    xoffset = -12;
    	    yoffset = -3;
    	    image_xscale  = 24;
    	    image_yscale  = 34;
    	    xshift  = 0;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -8;
    	    yoffset = -21;
    	    image_xscale  = 16;
    	    image_yscale  = 18;
    	    xshift = 0;
    	}
    	
    	with(collisionbox)
    	{
    	    xoffset = -8;
    	    yoffset = -9;
    	    image_xscale  = 16;
    	    image_yscale  = 40;
    	    xshift = 0;
    	}
    	
    	attackframe0 = 5;
    	endstate = 24;
    	
    }
    
    //animation
    sprite_index = spr_lsyn_2m;
    
    if(state_timer > 3 && state_timer < 19)
    {
        vx = 6 * image_xscale;
    }
    
    //attack
    if(state_timer == attackframe0)
    {
        hitbox0 = create_hitbox(0,-5,55,31,4.25,-11.5,16,6,11,12,-1,0,13,4,"low",-1);
        attackhurtbox0 = create_hurtbox(8 * image_xscale, -17,22 * image_xscale,16,2,-2);
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 1;
        hitbox0.airxtrayhit = 2.5;
        hitbox0.juggle = 1;
    }
    
    //chainmove
    if(state_timer > 2)
    {
    
        
        if(input_check(0,0,0,1,0))
        {
            chainmove = "heavy"
        }
        
        if((input_check(2,0,0,1,0) || input_check(1,0,0,1,0) || input_check(3,0,0,1,0)))
        {
            chainmove = "cheavy";
        }
    }
    
    attack_base(endstate,0,0,1,1,0);
}
else if(heat < -50)
{
    if(state_new == 1)
    {
        with(hurtbox0)
    	{
    	    xoffset = -29;
    	    yoffset = -31;
    	    image_xscale  = 58;
    	    image_yscale  = 63;
    	    xshift  = -1;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -15;
    	    yoffset = -56;
    	    image_xscale  = 30;
    	    image_yscale  = 25;
    	    xshift = -3;
    	}
    	
    	
    	with(collisionbox)
    	{
    	    xoffset = -20;
    	    yoffset = -22;
    	    image_xscale  = 40;
    	    image_yscale  = 54;
    	    xshift = 0;
    	}
    	
    	attackframe0 = 5;
    	endstate = 21;
    	
    }
    
    //animation
    sprite_index = spr_dsyn_2m;
    
    if(state_timer == attackframe0 - 1)
    {
        vx = 5.5 * image_xscale;
    }
    
    //attack
    if(state_timer == attackframe0)
    {
        hitbox0 = create_hitbox(-6,0,42,30,5.95,0,12,18,11,12,-1,0,2,4,"low",-1);
        attackhurtbox0 = create_hurtbox(8 * image_xscale, -7,22 * image_xscale,16,2,-2);
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 1;
        hitbox0.juggle = 1;
    }
    
    //chainmove
    if(state_timer > 2)
    {
    
        
        if(input_check(0,0,0,1,0))
        {
            chainmove = "heavy"
        }
        
        if((input_check(2,0,0,1,0) || input_check(1,0,0,1,0) || input_check(3,0,0,1,0)))
        {
            chainmove = "cheavy";
        }
    }
    
    attack_base(endstate,0,0,1,1,0);
}
else
{
    if(state_new == 1)
    {
        with(hurtbox0)
    	{
    	    xoffset = -29;
    	    yoffset = -31;
    	    image_xscale  = 58;
    	    image_yscale  = 63;
    	    xshift  = -1;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -15;
    	    yoffset = -56;
    	    image_xscale  = 30;
    	    image_yscale  = 25;
    	    xshift = -3;
    	}
    	
    	
    	with(collisionbox)
    	{
    	    xoffset = -20;
    	    yoffset = -22;
    	    image_xscale  = 40;
    	    image_yscale  = 54;
    	    xshift = 0;
    	}
    	
    	attackframe0 = 6;
    	endstate = 23;
    	
    }
    
    //animation
    sprite_index = spr_momo_2m;
    
    if(state_timer == attackframe0 - 1)
    {
        vx = 5.5 * image_xscale;
    }
    
    //attack
    if(state_timer == attackframe0)
    {
        hitbox0 = create_hitbox(15,-9,65,39,5.95,0,12,18,11,12,-1,0,2,4,"low",-1);
        attackhurtbox0 = create_hurtbox(20 * image_xscale, -2,55 * image_xscale,16,2,-1);
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 1;
        hitbox0.juggle = 1;
    }
    
    //chainmove
    if(state_timer > 2)
    {
    
        
        if(input_check(0,0,0,1,0))
        {
            chainmove = "heavy"
        }
        
        if((input_check(2,0,0,1,0) || input_check(1,0,0,1,0) || input_check(3,0,0,1,0)))
        {
            chainmove = "cheavy";
        }
    }
    
    attack_base(endstate,0,0,1,1,0);
}