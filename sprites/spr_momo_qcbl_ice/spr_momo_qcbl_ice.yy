{
    "id": "e125feea-66ae-4a4e-a7a7-63be1ebb97a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_qcbl_ice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29e55e84-4c73-415a-905a-053ae782d6f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e125feea-66ae-4a4e-a7a7-63be1ebb97a9",
            "compositeImage": {
                "id": "88d22217-7abd-436a-a312-f0f54f833480",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29e55e84-4c73-415a-905a-053ae782d6f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fc63075-423d-4e91-a936-d9f48471e59b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29e55e84-4c73-415a-905a-053ae782d6f4",
                    "LayerId": "88ed84b9-b5c0-40db-9260-a86d222dbd98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "88ed84b9-b5c0-40db-9260-a86d222dbd98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e125feea-66ae-4a4e-a7a7-63be1ebb97a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}