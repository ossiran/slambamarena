{
    "id": "d7c8964c-a6f0-4ecc-bbeb-37773f738975",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_myu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 12,
    "bbox_right": 36,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e23cc43f-c186-41f6-887d-897bb7b717c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7c8964c-a6f0-4ecc-bbeb-37773f738975",
            "compositeImage": {
                "id": "5d747c8e-de69-495e-8ecc-e40100ebb736",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e23cc43f-c186-41f6-887d-897bb7b717c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c15f5043-305c-4ff1-862a-a4d5b6c31b7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e23cc43f-c186-41f6-887d-897bb7b717c4",
                    "LayerId": "ef436677-9660-4029-997f-f20094dbfec9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ef436677-9660-4029-997f-f20094dbfec9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7c8964c-a6f0-4ecc-bbeb-37773f738975",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 32
}