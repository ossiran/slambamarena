
if(state_new == 1)
{
		with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 16;
	    image_yscale  = 47;
	    xshift = -2
	}

	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -28;
	    xscale  = 26;
	    yscale  = 40;
	    xshift  = -2;
	}
    sprite_index = spr_bill_clight;
	
	attackframe0 = 2;
	chainmove = 0;
	endstate = 9;
}



//attack
if(state_timer == attackframe0)
{
    hitbox1 = create_hitbox(12, -10,30, 18, 5.25,0, 8,14,8,13,-1,0,2,3.5,"mid",-1);
    attackhurtbox0 = create_hurtbox(12 * image_xscale, -11,25 * image_xscale,22,2,-2);
    hitbox1.scaling = 1.2;
    hitbox1.hitsound = snd_hitsound1;
    hitbox1.juggle = 2;
}

//chainmove
if(state_timer > 2)
{
    if(input_check(4,1,0,0,0) || input_check(5,1,0,0,0) || input_check(6,1,0,0,0))
    {
        chainmove = "light";
    }
    
    if(input_check(2,1,0,0,0) || input_check(1,1,0,0,0) || input_check(3,1,0,0,0))
    {
        chainmove = "clight";
    }
    
    if(input_check(4,0,1,0,0) || input_check(5,0,1,0,0) || input_check(6,0,1,0,0))
    {
        chainmove = "medium";
    }
    
    if(input_check(2,0,1,0,0) || input_check(1,0,1,0,0) || input_check(3,0,1,0,0))
    {
        chainmove = "cmedium";
    }
    
    if(input_check(4,0,0,1,0) || input_check(5,0,0,1,0) || input_check(6,0,0,1,0))
    {
        chainmove = "heavy";
    }
    
    if(input_check(2,0,0,1,0) || input_check(1,0,0,1,0))
    {
        chainmove = "cheavy";
    }
}

attack_base(endstate,0,0,1,1,0);

//chain lights
if(hitting == 1 && otherplayer.hitpause == 0 && otherplayer.hitstun < 12 && ((input_check(1,1,0,0,0) || input_check(2,1,0,0,0) || input_check(3,1,0,0,0)) || chainmove == "light"))
{
    state_timer = 0;
    if(hitbox1 != -1)
    {
        instance_destroy(hitbox1);
    }
}


/*



if(hitting == 1 && otherplayer.hitpause == 0 && otherplayer.hitstun < 12 && ((input_check(2,1,0,0,0) || input_check(1,1,0,0,0) || input_check(3,1,0,0,0)) || chainmove == "clight"))
{
    state_timer = 0;
    if(hitbox1 != -1)
    {
        instance_destroy(hitbox1);
    }
}


if(hitting == 1 && otherplayer.hitpause == 0 && (input_check(0,0,1,0,0) || chainmove == "medium"))
{
    state_switch("medium");
    end_move();
}

if(hitting == 1 && otherplayer.hitpause == 0 && ((input_check(2,0,1,0,0) || input_check(1,0,1,0,0) || input_check(3,0,1,0,0)) || chainmove == "cmedium"))
{
    state_switch("cmedium");
    end_move();
}

if(hitting == 1 && otherplayer.hitpause == 0 && (input_check(0,0,0,1,0) || chainmove == "heavy"))
{
    state_switch("heavy");
    end_move();
}

if(hitting == 1 && otherplayer.hitpause == 0 && (input_check(2,0,0,1,0) || input_check(1,0,0,1,0) || chainmove == "cheavy"))
{
    state_switch("cheavy");
    end_move();
}
*/