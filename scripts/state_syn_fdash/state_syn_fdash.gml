live_call();
if(state_new == 1)
{
    with(hurtbox0)
	{
	    xoffset = -12;
	    yoffset = -13;
	    image_xscale  = 24;
	    image_yscale  = 40;
	    xshift  = 0;
	}
	
	with(hurtbox1)
	{
	    xoffset = -8;
	    yoffset = -31;
	    image_xscale  = 16;
	    image_yscale  = 18;
	    xshift = 0;
	}
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -13;
	    image_xscale  = 16;
	    image_yscale  = 40;
	    xshift = 0;
	}
	

    dtap_right = 0;
    dtap_left = 0;
    light_buffer = 0;
    medium_buffer = 0;
	//sprite_index = spr_lsyn_fdash;
	image_index = 1;
}

if(synmode > 0)
{
    vx = Approach(vx, 5 * image_xscale, ground_accel*2);
    sprite_index = spr_lsyn_fdash;
}
else if(synmode < 0)
{
    vx = Approach(vx, 5.5 * image_xscale, ground_accel*2);
    sprite_index = spr_dsyn_fdash;
}


//crouch
if(key_down == 1 && state_timer > 3)
{
    state_switch("crouch");
}

//fmedium
if(input_check(6,0,1,0,0) && state_timer > 3)
{
    chainmove =  "dmedium";
}

//fheavy
if(input_check(6,0,0,1,0) && state_timer > 3)
{
    chainmove =  "dheavy";
}

if(state_timer > 3)
{
            //jab
        if(input_check(0,1,0,0,0))
        {
            state_switch("light");
        }
        
        //clight
        if(input_check(2,1,0,0,0))
        {
            state_switch("clight");
        }
        
        //medium
        if(input_check(0,0,1,0,0))
        {
            state_switch("medium");
        }
        
        //cmedium
        if(input_check(1,0,1,0,0) || input_check(2,0,1,0,0) || input_check(3,0,1,0,0))
        {
            state_switch("cmedium");
        }
        
        
        //heavy
        if(input_check(0,0,0,1,0))
        {
            state_switch("heavy");
        }
        
        //grab
        if(input_check(0,1,0,0,1))
        {
            state_switch("grab");
        }
        
        /*
        //qcf
        if(qcf_buffer > 0)
        {
            if(input_check(0,0,0,1,0))
            {
                if(ex > 49.9)
                {
                    state_switch("qcfh");
                }
                else
                {
                    state_switch("qcfm");
                }
            }
            else if(input_check(0,0,1,0,0))
            {
                state_switch("qcfm");
            }
            else if(input_check(0,1,0,0,0))
            {
                state_switch("qcfl");
            }
        }
        
        //qcb
        if(qcb_buffer > 0)
        {
            if(input_check(0,0,0,1,0))
            {
                if(ex > 49.9)
                {
                    state_switch("qcbh");
                }
                else
                {
                    state_switch("qcbm");
                }
            }
            else if(input_check(0,0,1,0,0))
            {
                state_switch("qcbm");
            }
            else if(input_check(0,1,0,0,0))
            {
                state_switch("qcbl");
            }
        }
        
        //srk
        if(srk_buffer > 0)
        {
            if(input_check(0,0,0,1,0))
            {
                if(ex > 49.9)
                {
                    state_switch("srkh");
                }
                else
                {
                    state_switch("srkm");
                }
            }
            else if(input_check(0,0,1,0,0))
            {
                state_switch("srkm");
            }
            else if(input_check(0,1,0,0,0))
            {
                state_switch("srkl");
            }
        }
        
        if(dqcf_buffer > 0 && ex > 99.9)
        {
            if(input_check(0,0,0,1,0))
            {
                state_switch("dqcf");
            }
            else if(input_check(0,0,1,0,0))
            {
                state_switch("dqcf");
            }
            else if(input_check(0,1,0,0,0))
            {
                state_switch("dqcf");
            }
        }
        */
        //fmedium
        if(input_check(6,0,1,0,0) || chainmove = "dmedium")
        {
            state_switch("dmedium");
        }
        
        //fheavy
        if(input_check(6,0,0,1,0) || chainmove = "dheavy")
        {
            state_switch("dheavy");
        }
}



//jump
if((key_up == 1 || key_jump == 1) && state_timer > 3)
{
	state_switch("jumpsquat")
	vx += 3.5 * image_xscale;
}

//stand
if(state_timer > 14 && (input_check(5,0,0,0,0) || input_check(4,0,0,0,0) || input_check(1,0,0,0,0) || input_check(2,0,0,0,0) || input_check(3,0,0,0,0)))
{
    state_switch("stand");
}

//air
if(!onground())
{
	state_switch("air");	
}



//hitstun
if(hitstun > 0)
{
    state_switch("hitstun");   
}

