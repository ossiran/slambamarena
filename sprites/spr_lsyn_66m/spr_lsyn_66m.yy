{
    "id": "572dff8e-bc91-4680-9597-65222788b0b8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lsyn_66m",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 43,
    "bbox_right": 114,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a781e4fe-6864-4128-9feb-d0fddf42ac16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "572dff8e-bc91-4680-9597-65222788b0b8",
            "compositeImage": {
                "id": "052a4e06-92da-427d-98bb-2618abeade85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a781e4fe-6864-4128-9feb-d0fddf42ac16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56ed3ecd-5e07-4795-9eed-08b9226ca060",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a781e4fe-6864-4128-9feb-d0fddf42ac16",
                    "LayerId": "a04bddc9-16e2-4288-a39b-f994a1f397fb"
                }
            ]
        },
        {
            "id": "a81d1423-8d15-4309-86fa-689bc0fa7242",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "572dff8e-bc91-4680-9597-65222788b0b8",
            "compositeImage": {
                "id": "c4d84d4c-4846-4fe7-b6ea-8d885b395ed4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a81d1423-8d15-4309-86fa-689bc0fa7242",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d798f139-7294-4089-85c1-1b824690adcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a81d1423-8d15-4309-86fa-689bc0fa7242",
                    "LayerId": "a04bddc9-16e2-4288-a39b-f994a1f397fb"
                }
            ]
        },
        {
            "id": "fa1d45be-1a40-4a1f-9fae-d95afece40b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "572dff8e-bc91-4680-9597-65222788b0b8",
            "compositeImage": {
                "id": "310fea11-d24e-4627-8865-be25b8c2abfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa1d45be-1a40-4a1f-9fae-d95afece40b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c8e817c-c202-4270-bd28-d459e8ce3835",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa1d45be-1a40-4a1f-9fae-d95afece40b2",
                    "LayerId": "a04bddc9-16e2-4288-a39b-f994a1f397fb"
                }
            ]
        },
        {
            "id": "d0b3d0be-7d85-4c29-bdb8-a289d4f6ed7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "572dff8e-bc91-4680-9597-65222788b0b8",
            "compositeImage": {
                "id": "217e8dd7-296b-43d3-82b5-d8c581b710dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0b3d0be-7d85-4c29-bdb8-a289d4f6ed7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fb7b82b-2b37-45f7-92b5-785720e714df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0b3d0be-7d85-4c29-bdb8-a289d4f6ed7b",
                    "LayerId": "a04bddc9-16e2-4288-a39b-f994a1f397fb"
                }
            ]
        },
        {
            "id": "7595eb16-75a3-4255-87bf-2cd3256ddd99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "572dff8e-bc91-4680-9597-65222788b0b8",
            "compositeImage": {
                "id": "d9b68d05-7bdf-4e9f-a23c-a7c5766d232b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7595eb16-75a3-4255-87bf-2cd3256ddd99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e5c9d31-a88b-471f-9ae4-e51c1dc38968",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7595eb16-75a3-4255-87bf-2cd3256ddd99",
                    "LayerId": "a04bddc9-16e2-4288-a39b-f994a1f397fb"
                }
            ]
        },
        {
            "id": "63fd3fe5-a989-4270-86b1-76cdcc6065e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "572dff8e-bc91-4680-9597-65222788b0b8",
            "compositeImage": {
                "id": "73c39649-070a-487e-b22a-1ba9e61135f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63fd3fe5-a989-4270-86b1-76cdcc6065e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3d87b17-2b6f-4b84-a2b7-50929774114f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63fd3fe5-a989-4270-86b1-76cdcc6065e1",
                    "LayerId": "a04bddc9-16e2-4288-a39b-f994a1f397fb"
                }
            ]
        },
        {
            "id": "31b5852e-f21e-48de-882c-ff510d819643",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "572dff8e-bc91-4680-9597-65222788b0b8",
            "compositeImage": {
                "id": "151f807e-a160-477e-8d6b-ad7a418e9461",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31b5852e-f21e-48de-882c-ff510d819643",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc6369b7-eb3f-4d37-bcb0-66bd9fc17653",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31b5852e-f21e-48de-882c-ff510d819643",
                    "LayerId": "a04bddc9-16e2-4288-a39b-f994a1f397fb"
                }
            ]
        },
        {
            "id": "87461194-7511-4e4e-a70c-84cd29acfcb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "572dff8e-bc91-4680-9597-65222788b0b8",
            "compositeImage": {
                "id": "55586f9f-7572-4f14-8d2a-0850c611948d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87461194-7511-4e4e-a70c-84cd29acfcb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14d77945-2cf4-4354-97e8-84328343ff2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87461194-7511-4e4e-a70c-84cd29acfcb2",
                    "LayerId": "a04bddc9-16e2-4288-a39b-f994a1f397fb"
                }
            ]
        },
        {
            "id": "f5f8b805-9605-4989-96f6-533dd7649dc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "572dff8e-bc91-4680-9597-65222788b0b8",
            "compositeImage": {
                "id": "572338cf-942e-4b45-bf5c-58af9661ff3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5f8b805-9605-4989-96f6-533dd7649dc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e147aed-fe92-440d-80ef-115c02035e21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5f8b805-9605-4989-96f6-533dd7649dc5",
                    "LayerId": "a04bddc9-16e2-4288-a39b-f994a1f397fb"
                }
            ]
        },
        {
            "id": "612b2f3e-e61e-4240-9ed0-760778d6ee10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "572dff8e-bc91-4680-9597-65222788b0b8",
            "compositeImage": {
                "id": "9c684647-db78-44f8-8c97-6aaaaa7b6949",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "612b2f3e-e61e-4240-9ed0-760778d6ee10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e2fd7e9-cfbc-4e8e-a6aa-fe86aa59e421",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "612b2f3e-e61e-4240-9ed0-760778d6ee10",
                    "LayerId": "a04bddc9-16e2-4288-a39b-f994a1f397fb"
                }
            ]
        },
        {
            "id": "1652387d-2758-4b77-81d9-1d515d0c5938",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "572dff8e-bc91-4680-9597-65222788b0b8",
            "compositeImage": {
                "id": "aff93f1b-9f02-49b1-b99d-ca1b5ba21a03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1652387d-2758-4b77-81d9-1d515d0c5938",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f18d7d1-d50f-4e1a-a5a3-6cecdfcddfbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1652387d-2758-4b77-81d9-1d515d0c5938",
                    "LayerId": "a04bddc9-16e2-4288-a39b-f994a1f397fb"
                }
            ]
        },
        {
            "id": "41cb9274-947b-423a-8eab-bfa6b30c267c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "572dff8e-bc91-4680-9597-65222788b0b8",
            "compositeImage": {
                "id": "d2d6c5a6-2d72-4cf9-be0d-48d440dc02b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41cb9274-947b-423a-8eab-bfa6b30c267c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52a4bfba-c9c7-436c-a1d9-33849338b343",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41cb9274-947b-423a-8eab-bfa6b30c267c",
                    "LayerId": "a04bddc9-16e2-4288-a39b-f994a1f397fb"
                }
            ]
        },
        {
            "id": "5783601c-d1b2-4295-b25b-53920a2d92cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "572dff8e-bc91-4680-9597-65222788b0b8",
            "compositeImage": {
                "id": "35cd5385-0752-4d6c-b212-67c95a555269",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5783601c-d1b2-4295-b25b-53920a2d92cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b87965f-c347-4f33-8779-b8d4cfc0c777",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5783601c-d1b2-4295-b25b-53920a2d92cb",
                    "LayerId": "a04bddc9-16e2-4288-a39b-f994a1f397fb"
                }
            ]
        },
        {
            "id": "86947c69-2459-4556-af64-b320edea87c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "572dff8e-bc91-4680-9597-65222788b0b8",
            "compositeImage": {
                "id": "bd6ae2f7-7816-46fc-a084-229618a2dfea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86947c69-2459-4556-af64-b320edea87c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d05edc5-8223-4ffc-8a07-baa69c07c0b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86947c69-2459-4556-af64-b320edea87c8",
                    "LayerId": "a04bddc9-16e2-4288-a39b-f994a1f397fb"
                }
            ]
        },
        {
            "id": "5b98356f-a4d2-4c2d-ab8d-f0eb126c6f73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "572dff8e-bc91-4680-9597-65222788b0b8",
            "compositeImage": {
                "id": "fafc2703-e5e8-4282-a2a0-0738a0907cd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b98356f-a4d2-4c2d-ab8d-f0eb126c6f73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f771a28-2ec0-4a72-b679-25a224976718",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b98356f-a4d2-4c2d-ab8d-f0eb126c6f73",
                    "LayerId": "a04bddc9-16e2-4288-a39b-f994a1f397fb"
                }
            ]
        },
        {
            "id": "80c31aa4-f123-4109-acfb-a4f44a45c4e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "572dff8e-bc91-4680-9597-65222788b0b8",
            "compositeImage": {
                "id": "0c5a45ce-c0b5-4274-83f1-bfb65aa87efc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80c31aa4-f123-4109-acfb-a4f44a45c4e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d45ab88-027a-40cb-bb8c-7dcae6750687",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80c31aa4-f123-4109-acfb-a4f44a45c4e7",
                    "LayerId": "a04bddc9-16e2-4288-a39b-f994a1f397fb"
                }
            ]
        },
        {
            "id": "7411cdad-4edc-4be0-8ae4-da340cdcfcb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "572dff8e-bc91-4680-9597-65222788b0b8",
            "compositeImage": {
                "id": "cfd8a286-1907-4bda-8c6b-1711ad6ae05a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7411cdad-4edc-4be0-8ae4-da340cdcfcb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cc8e55e-624b-4b50-a3f5-96e80e154315",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7411cdad-4edc-4be0-8ae4-da340cdcfcb0",
                    "LayerId": "a04bddc9-16e2-4288-a39b-f994a1f397fb"
                }
            ]
        },
        {
            "id": "612d7e04-3150-426f-8130-5ffdca4777fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "572dff8e-bc91-4680-9597-65222788b0b8",
            "compositeImage": {
                "id": "16c227b5-023d-4382-95b0-da9eb0de4565",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "612d7e04-3150-426f-8130-5ffdca4777fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fe815f3-c054-48d8-94ce-0056a47e1cde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "612d7e04-3150-426f-8130-5ffdca4777fc",
                    "LayerId": "a04bddc9-16e2-4288-a39b-f994a1f397fb"
                }
            ]
        },
        {
            "id": "42e5fbd1-7a12-40aa-8ab1-832105a5760a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "572dff8e-bc91-4680-9597-65222788b0b8",
            "compositeImage": {
                "id": "44fdf37a-72d0-41c4-933b-9d5ea2166486",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42e5fbd1-7a12-40aa-8ab1-832105a5760a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fba9110-93cd-42e6-822b-48e7da96d15d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42e5fbd1-7a12-40aa-8ab1-832105a5760a",
                    "LayerId": "a04bddc9-16e2-4288-a39b-f994a1f397fb"
                }
            ]
        },
        {
            "id": "edf59345-659b-46b4-857a-35057715e469",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "572dff8e-bc91-4680-9597-65222788b0b8",
            "compositeImage": {
                "id": "2465eddb-d537-4cec-b246-0a8c1496dee1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edf59345-659b-46b4-857a-35057715e469",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ae7621d-a6a3-4bdd-b7ac-6c130db7f4f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edf59345-659b-46b4-857a-35057715e469",
                    "LayerId": "a04bddc9-16e2-4288-a39b-f994a1f397fb"
                }
            ]
        },
        {
            "id": "b846fcff-3937-424f-8bc6-a9603ddc5515",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "572dff8e-bc91-4680-9597-65222788b0b8",
            "compositeImage": {
                "id": "14ee1edd-1dda-4858-8372-7a7ad4594c84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b846fcff-3937-424f-8bc6-a9603ddc5515",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d4dedcf-2ccd-449e-8775-c305e303a051",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b846fcff-3937-424f-8bc6-a9603ddc5515",
                    "LayerId": "a04bddc9-16e2-4288-a39b-f994a1f397fb"
                }
            ]
        },
        {
            "id": "ed0d216e-6169-4a4a-b6ce-bc6bc4e8c320",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "572dff8e-bc91-4680-9597-65222788b0b8",
            "compositeImage": {
                "id": "bfbbb5b0-0279-4f1a-bc1a-62f46125bfd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed0d216e-6169-4a4a-b6ce-bc6bc4e8c320",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbf8b82b-4558-4e81-9548-b8096ea37328",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed0d216e-6169-4a4a-b6ce-bc6bc4e8c320",
                    "LayerId": "a04bddc9-16e2-4288-a39b-f994a1f397fb"
                }
            ]
        },
        {
            "id": "668208f9-863c-4511-bf24-3e8b48399688",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "572dff8e-bc91-4680-9597-65222788b0b8",
            "compositeImage": {
                "id": "5c0bf1b0-6d24-4c74-bb16-8bbb50be965a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "668208f9-863c-4511-bf24-3e8b48399688",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb4533b6-593a-4777-be8d-614c6d52fafe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "668208f9-863c-4511-bf24-3e8b48399688",
                    "LayerId": "a04bddc9-16e2-4288-a39b-f994a1f397fb"
                }
            ]
        },
        {
            "id": "a8f22b9b-0028-4673-b69d-89b950e90ac9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "572dff8e-bc91-4680-9597-65222788b0b8",
            "compositeImage": {
                "id": "30342319-900b-4822-8fe1-1aa2d29b9921",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8f22b9b-0028-4673-b69d-89b950e90ac9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "139d236d-dee0-4e43-8321-2c1a107c2ab0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8f22b9b-0028-4673-b69d-89b950e90ac9",
                    "LayerId": "a04bddc9-16e2-4288-a39b-f994a1f397fb"
                }
            ]
        },
        {
            "id": "01bf798c-2e21-40c6-be30-ceef1532c7b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "572dff8e-bc91-4680-9597-65222788b0b8",
            "compositeImage": {
                "id": "6476cd71-fab1-43f5-9532-e2d5674f49be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01bf798c-2e21-40c6-be30-ceef1532c7b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a6f79f9-d39c-45eb-925f-e18ffde8d2ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01bf798c-2e21-40c6-be30-ceef1532c7b4",
                    "LayerId": "a04bddc9-16e2-4288-a39b-f994a1f397fb"
                }
            ]
        },
        {
            "id": "7ad12cf7-c2e4-41ba-8a93-26d91caf325e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "572dff8e-bc91-4680-9597-65222788b0b8",
            "compositeImage": {
                "id": "2889a4fb-20cb-468f-9803-2784efa98062",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ad12cf7-c2e4-41ba-8a93-26d91caf325e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eea48a99-8f1b-4317-ae93-8069033eb170",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ad12cf7-c2e4-41ba-8a93-26d91caf325e",
                    "LayerId": "a04bddc9-16e2-4288-a39b-f994a1f397fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "a04bddc9-16e2-4288-a39b-f994a1f397fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "572dff8e-bc91-4680-9597-65222788b0b8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}