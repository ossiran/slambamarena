{
    "id": "9083a777-fffb-43d2-9da2-c568da83544e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dsyn_jl",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 49,
    "bbox_right": 95,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c7329cb9-f4fc-4167-8e5a-0128d97c701c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9083a777-fffb-43d2-9da2-c568da83544e",
            "compositeImage": {
                "id": "8d81e507-40aa-4da7-8361-856089726f8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7329cb9-f4fc-4167-8e5a-0128d97c701c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d453cb40-aa86-4ca2-b178-bf2ad3fbb702",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7329cb9-f4fc-4167-8e5a-0128d97c701c",
                    "LayerId": "8290ec8f-4a99-45d8-890b-fdfdbbc90ed4"
                }
            ]
        },
        {
            "id": "8974ddb7-f9d1-41fd-9ab3-ff85b5ecfd56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9083a777-fffb-43d2-9da2-c568da83544e",
            "compositeImage": {
                "id": "ff4c411a-661f-4be3-908d-c33d5f48472e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8974ddb7-f9d1-41fd-9ab3-ff85b5ecfd56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9616bf3-6dc5-4d7b-b22b-601014323296",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8974ddb7-f9d1-41fd-9ab3-ff85b5ecfd56",
                    "LayerId": "8290ec8f-4a99-45d8-890b-fdfdbbc90ed4"
                }
            ]
        },
        {
            "id": "7708a759-49eb-48ca-8771-6963af9abb2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9083a777-fffb-43d2-9da2-c568da83544e",
            "compositeImage": {
                "id": "57bdf7b7-4c60-41a0-9f59-e5f5876527ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7708a759-49eb-48ca-8771-6963af9abb2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ad2fdc9-b58e-4e78-ad64-88207c9b67b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7708a759-49eb-48ca-8771-6963af9abb2a",
                    "LayerId": "8290ec8f-4a99-45d8-890b-fdfdbbc90ed4"
                }
            ]
        },
        {
            "id": "57c66a6a-d6c7-4d4f-8c26-5c15f13e36d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9083a777-fffb-43d2-9da2-c568da83544e",
            "compositeImage": {
                "id": "1549f884-ae66-4312-8a3b-4e3f07c6eb55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57c66a6a-d6c7-4d4f-8c26-5c15f13e36d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7f92387-9aa6-475e-82aa-74e37e5a8cdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57c66a6a-d6c7-4d4f-8c26-5c15f13e36d6",
                    "LayerId": "8290ec8f-4a99-45d8-890b-fdfdbbc90ed4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "8290ec8f-4a99-45d8-890b-fdfdbbc90ed4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9083a777-fffb-43d2-9da2-c568da83544e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}