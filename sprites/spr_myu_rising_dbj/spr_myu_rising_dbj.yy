{
    "id": "15d1f546-b9eb-4831-8cea-c2908c209e91",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_myu_rising_dbj",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 12,
    "bbox_right": 35,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f6047b7a-de67-4793-8dc6-1f11df64a5b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15d1f546-b9eb-4831-8cea-c2908c209e91",
            "compositeImage": {
                "id": "478a13c4-7b3f-4652-a82f-1cfd1f4bec88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6047b7a-de67-4793-8dc6-1f11df64a5b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f8add6b-d280-42d3-a950-9c51055839c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6047b7a-de67-4793-8dc6-1f11df64a5b2",
                    "LayerId": "897f58e3-1a8f-4fd2-aafc-fe7e5c6428e7"
                }
            ]
        },
        {
            "id": "4b93588b-fca8-4747-869e-d7534e061ffe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15d1f546-b9eb-4831-8cea-c2908c209e91",
            "compositeImage": {
                "id": "d3a02e27-85be-4a64-bc46-81745e60493b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b93588b-fca8-4747-869e-d7534e061ffe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b81bccf-259e-4471-a4d0-80ade4b720e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b93588b-fca8-4747-869e-d7534e061ffe",
                    "LayerId": "897f58e3-1a8f-4fd2-aafc-fe7e5c6428e7"
                }
            ]
        },
        {
            "id": "a7272cff-89ba-4560-93be-7e9368745f14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15d1f546-b9eb-4831-8cea-c2908c209e91",
            "compositeImage": {
                "id": "45e836ff-ce04-4cd3-8a9f-990943a07879",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7272cff-89ba-4560-93be-7e9368745f14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46de7300-bbe6-4c77-b1bd-04621da652ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7272cff-89ba-4560-93be-7e9368745f14",
                    "LayerId": "897f58e3-1a8f-4fd2-aafc-fe7e5c6428e7"
                }
            ]
        },
        {
            "id": "a51dcd77-593c-4357-8328-233b7cafb370",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15d1f546-b9eb-4831-8cea-c2908c209e91",
            "compositeImage": {
                "id": "97e59280-f9b7-4531-8073-c6aabb494e70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a51dcd77-593c-4357-8328-233b7cafb370",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dc51cc3-b885-4b10-a1c0-e542d0937dd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a51dcd77-593c-4357-8328-233b7cafb370",
                    "LayerId": "897f58e3-1a8f-4fd2-aafc-fe7e5c6428e7"
                }
            ]
        },
        {
            "id": "4f7dcff4-0bd3-448e-a44e-9066483857f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15d1f546-b9eb-4831-8cea-c2908c209e91",
            "compositeImage": {
                "id": "90af3b53-8bcc-42a0-87b3-37a70544b714",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f7dcff4-0bd3-448e-a44e-9066483857f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fa17c95-e904-4e1c-a742-041b0de6f1a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f7dcff4-0bd3-448e-a44e-9066483857f3",
                    "LayerId": "897f58e3-1a8f-4fd2-aafc-fe7e5c6428e7"
                }
            ]
        },
        {
            "id": "453f1f4f-fc2b-4f90-92f2-ee7741671c05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15d1f546-b9eb-4831-8cea-c2908c209e91",
            "compositeImage": {
                "id": "75c04856-c27b-4777-9f5f-fbf53799a89b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "453f1f4f-fc2b-4f90-92f2-ee7741671c05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04b61523-f9e5-446e-9a46-7379977d5c1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "453f1f4f-fc2b-4f90-92f2-ee7741671c05",
                    "LayerId": "897f58e3-1a8f-4fd2-aafc-fe7e5c6428e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "897f58e3-1a8f-4fd2-aafc-fe7e5c6428e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "15d1f546-b9eb-4831-8cea-c2908c209e91",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}