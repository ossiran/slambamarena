if(state_new == 1)
{
	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -43;
	    image_xscale  = 26;
	    image_yscale  = 75;
	    xshift  = -2;
	}
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -1000;
	    image_xscale  = 0;
	    image_yscale  = 0;
	    xshift = -2
	}
	
}


if(state_timer < 6 && input_check(0,1,0,0,1))
{
    grabbed = -1;
}

if(grabbed == -1)
{
    state_switch("grabtech");
}

if(grabbed == 0)
{
    state_switch("stand");
}

if(hitstun > 0)
{
    state_switch("hitstun");
    grabbed = 0;
}
