{
    "id": "4636c4d5-ab52-4e50-8686-9a473fa6b0d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_jm_ice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d87e2c7-0d5c-42b1-900c-2f17ac724d6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4636c4d5-ab52-4e50-8686-9a473fa6b0d1",
            "compositeImage": {
                "id": "fabaf37f-06d1-448b-b49f-35675fa01e47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d87e2c7-0d5c-42b1-900c-2f17ac724d6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13d67f9b-b85d-4f36-9087-0cfe1ca3afef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d87e2c7-0d5c-42b1-900c-2f17ac724d6e",
                    "LayerId": "19e80cf3-3288-4b80-9e6a-41b324b99de3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "19e80cf3-3288-4b80-9e6a-41b324b99de3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4636c4d5-ab52-4e50-8686-9a473fa6b0d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}