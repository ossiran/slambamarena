{
    "id": "052b4b01-677b-4347-8dc7-7da5f20626f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_5h",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 16,
    "bbox_right": 141,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7a1fcad0-5170-4a5f-afaa-e0fbf8b3be46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "3b19d53d-6a00-4aa8-a21a-34452ae4ff5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a1fcad0-5170-4a5f-afaa-e0fbf8b3be46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc8e0775-6a53-4018-9843-77857b4bfffd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a1fcad0-5170-4a5f-afaa-e0fbf8b3be46",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "79bf50a3-5910-4fff-b29c-0c7d3e06c959",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "5c685680-dfe6-471a-ab42-b22e4eed28f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79bf50a3-5910-4fff-b29c-0c7d3e06c959",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc9ed0dc-2bbb-40f3-82c9-ea4fb8d290c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79bf50a3-5910-4fff-b29c-0c7d3e06c959",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "9def1d32-66fc-45ee-a3d3-f4234afadf2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "85d2a62b-b04c-4750-b8b6-b483540b9cf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9def1d32-66fc-45ee-a3d3-f4234afadf2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bae161f9-8622-4e8d-bc38-4e6caf273d4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9def1d32-66fc-45ee-a3d3-f4234afadf2f",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "9ef1513d-b504-4201-bd2f-5595abcf1002",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "688c23d1-1622-4c99-9aec-b139810cd798",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ef1513d-b504-4201-bd2f-5595abcf1002",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efe6eecf-62a1-4b78-9f9a-9c440995a90d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ef1513d-b504-4201-bd2f-5595abcf1002",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "6e676d52-b68d-4573-b005-79e4fabc52b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "c7fe4ac1-8c30-4ebd-895c-149e456b23c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e676d52-b68d-4573-b005-79e4fabc52b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03622677-f635-4e4c-bfde-354831ae06ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e676d52-b68d-4573-b005-79e4fabc52b0",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "f6e492b5-9396-40c5-bb14-7dfcbec710b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "4290915f-1a6c-40db-8db6-56c8adeca75d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6e492b5-9396-40c5-bb14-7dfcbec710b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ced78e5d-8916-469b-b2e9-dd58ebce05a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6e492b5-9396-40c5-bb14-7dfcbec710b4",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "af8a4acc-ce8e-487d-96bf-0c79fb44e0c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "51099960-ea1d-4b1b-a758-5bc221c1e0f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af8a4acc-ce8e-487d-96bf-0c79fb44e0c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d999c91-6530-4107-9fd7-c9322ecc5f55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af8a4acc-ce8e-487d-96bf-0c79fb44e0c0",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "1b75916f-c24f-4028-a10e-7ae7290e83c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "6f93e75d-1d65-488b-adaa-9c7fd792e2c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b75916f-c24f-4028-a10e-7ae7290e83c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e58b58c-c81b-4145-adef-61776461d9e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b75916f-c24f-4028-a10e-7ae7290e83c7",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "05a29ede-b07a-4357-868a-bebc40eff230",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "180940ff-f1b1-4588-8f2c-3710483c6e65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05a29ede-b07a-4357-868a-bebc40eff230",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "395ebb57-3434-4b1f-befd-95c0461392d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05a29ede-b07a-4357-868a-bebc40eff230",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "805e59bf-8a6a-46cd-a048-99785aa78510",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "141a12d8-bdce-4b01-9743-71faee8f37cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "805e59bf-8a6a-46cd-a048-99785aa78510",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98c1d93f-7970-4010-8e30-ca1ac922ba60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "805e59bf-8a6a-46cd-a048-99785aa78510",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "f2d6d0f2-a1af-4a8f-9777-6a61229e5039",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "cdbef160-b917-43ee-82c0-c95e71094841",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2d6d0f2-a1af-4a8f-9777-6a61229e5039",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31bde90f-8042-408c-9faa-50b034fc8206",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2d6d0f2-a1af-4a8f-9777-6a61229e5039",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "65df46d1-75a2-4f4a-9d3e-f5ad31d4f70c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "afa86f1c-81db-4a9f-8292-a772f970475c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65df46d1-75a2-4f4a-9d3e-f5ad31d4f70c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2fe8369-0689-4e22-9bd4-55c719b3a33d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65df46d1-75a2-4f4a-9d3e-f5ad31d4f70c",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "313f9cc1-9742-4bee-96c6-d3764191c5e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "8b09375b-e97a-47bd-aec2-33681e53ac91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "313f9cc1-9742-4bee-96c6-d3764191c5e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "751b8459-0d91-4195-930a-ef64b962b8d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "313f9cc1-9742-4bee-96c6-d3764191c5e2",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "f78f8d93-b803-48e8-853f-d829bf64e530",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "76a58611-e527-4c04-8aae-c86b16849567",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f78f8d93-b803-48e8-853f-d829bf64e530",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94f40553-99df-421f-9cce-4b039014c920",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f78f8d93-b803-48e8-853f-d829bf64e530",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "326a17d6-6c04-4ed2-9b28-73c81806a08b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "35e3ec82-6634-4256-a0df-0888937f93c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "326a17d6-6c04-4ed2-9b28-73c81806a08b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf026c7d-9b38-40bd-851a-f1a472149bca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "326a17d6-6c04-4ed2-9b28-73c81806a08b",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "84db1c68-fe83-4c3e-a076-4999c5b591f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "4753c59a-1544-4828-bc33-6259f0802e7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84db1c68-fe83-4c3e-a076-4999c5b591f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05845412-853e-425a-8269-576ffe095d12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84db1c68-fe83-4c3e-a076-4999c5b591f7",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "3f2301b9-5ec1-4f31-b664-c89d84f43f7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "ea991020-4a6f-48f3-9538-7cdaf27974fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f2301b9-5ec1-4f31-b664-c89d84f43f7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6baf604-6c00-4a2b-9fd0-1a76f190e058",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f2301b9-5ec1-4f31-b664-c89d84f43f7a",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "f81abff1-cd18-4688-9952-80de07c8bc90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "1a42cbe9-f14e-4724-b3d4-209de171f4a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f81abff1-cd18-4688-9952-80de07c8bc90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fb7cfb3-7cb1-40c9-b855-391749ceda5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f81abff1-cd18-4688-9952-80de07c8bc90",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "a53a8949-685a-4002-a985-06564667ab19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "f563e326-148d-4b5a-8a75-c4aa06b079e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a53a8949-685a-4002-a985-06564667ab19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a38fa3c9-e49b-4aab-9157-ecb648156dfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a53a8949-685a-4002-a985-06564667ab19",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "1ae38e72-f913-433d-b38d-4595c52c1726",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "c0997abc-157f-439b-a028-0988021ba045",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ae38e72-f913-433d-b38d-4595c52c1726",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6330968c-7caa-4a9d-bc26-d2c3cec230ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ae38e72-f913-433d-b38d-4595c52c1726",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "b846abb5-7c37-4b10-ba4c-d2538828fe60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "bd20e984-5a4c-4efc-b933-2ef66688d553",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b846abb5-7c37-4b10-ba4c-d2538828fe60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86dc8109-461a-4b7b-a5b4-c0a86d943f51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b846abb5-7c37-4b10-ba4c-d2538828fe60",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "20d40fb8-bc02-4afc-98dd-80d97e5fde5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "e430753f-81f2-4910-9147-7db88a806a2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20d40fb8-bc02-4afc-98dd-80d97e5fde5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1c1f1d6-872b-4b96-b671-4a3900c48859",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20d40fb8-bc02-4afc-98dd-80d97e5fde5e",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "fcf99866-2d4c-47c3-8dff-b37d48e433a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "a4ecba04-214a-4747-a79f-f0a5a0d76afe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcf99866-2d4c-47c3-8dff-b37d48e433a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c647c7a-e034-483e-8967-045c6b01240e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcf99866-2d4c-47c3-8dff-b37d48e433a4",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "3b269c62-c72e-4167-8aae-a1acaaa7464e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "0d067ab8-98cf-497e-b6ed-247205504d26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b269c62-c72e-4167-8aae-a1acaaa7464e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f700edd-7886-47c7-b2c0-e90b492cd45f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b269c62-c72e-4167-8aae-a1acaaa7464e",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "76ab0b19-eeb1-4b62-81e0-74853bafc8ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "435efe36-387d-42fc-a38a-9b86c3bf88e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76ab0b19-eeb1-4b62-81e0-74853bafc8ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ca1307a-eaf2-499f-89e7-90588f2ea7da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76ab0b19-eeb1-4b62-81e0-74853bafc8ba",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "9a87fe66-f3e2-4cde-bfd6-130ddd54a036",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "eb71e74a-c09a-49e8-988f-0150659e223c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a87fe66-f3e2-4cde-bfd6-130ddd54a036",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1e1c928-9e7d-45ba-8f4d-7a4f57e16f8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a87fe66-f3e2-4cde-bfd6-130ddd54a036",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "b6e9c4ce-f018-4c71-9788-21ac88f6e14b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "dfaebbd1-72f8-4b7e-b31c-76d5157ed3ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6e9c4ce-f018-4c71-9788-21ac88f6e14b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "532e4336-9beb-4fd7-86ed-29cade49484d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6e9c4ce-f018-4c71-9788-21ac88f6e14b",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "3c41c55d-57a8-4f49-b506-9bdcf4cb966f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "e5aca1c8-d6a1-4b10-b2cd-a48688d8d09b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c41c55d-57a8-4f49-b506-9bdcf4cb966f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53588925-273b-4dfd-922b-f6619e8b84de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c41c55d-57a8-4f49-b506-9bdcf4cb966f",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "1f03a65e-5998-4efb-b88d-95dbe173fc26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "d04d7a2b-a054-4051-a2ab-124e60476f76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f03a65e-5998-4efb-b88d-95dbe173fc26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4f6a680-8345-4f6d-83ca-fd3974940eb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f03a65e-5998-4efb-b88d-95dbe173fc26",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        },
        {
            "id": "2fb78eb7-1a33-4625-805f-2075157b8fac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "compositeImage": {
                "id": "70516836-c604-4dce-8fa7-5b8c807b58f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fb78eb7-1a33-4625-805f-2075157b8fac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a24d379-f940-4d28-a889-401ab9b3f3d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fb78eb7-1a33-4625-805f-2075157b8fac",
                    "LayerId": "d7e7b6d8-f422-4ef1-9334-36ef7883a573"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d7e7b6d8-f422-4ef1-9334-36ef7883a573",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "052b4b01-677b-4347-8dc7-7da5f20626f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 190,
    "xorig": 63,
    "yorig": 87
}