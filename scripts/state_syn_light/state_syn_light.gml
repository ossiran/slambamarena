live_call();
if (synmode > 0)
{
    if(state_new == 1)
    {
        with(hurtbox0)
    	{
    	    xoffset = -12;
    	    yoffset = -13;
    	    image_xscale  = 24;
    	    image_yscale  = 40;
    	    xshift  = 0;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -8;
    	    yoffset = -31;
    	    image_xscale  = 16;
    	    image_yscale  = 18;
    	    xshift = 0;
    	}
    	
    	with(collisionbox)
    	{
    	    xoffset = -8;
    	    yoffset = -13;
    	    image_xscale  = 16;
    	    image_yscale  = 40;
    	    xshift = 0;
    	}
    	
    	chainmove = 0;
    	attackframe0 = 3;
    	endstate = 12;
    }
    
    //animation
    sprite_index = spr_lsyn_5l;
    
    //attack
    if(state_timer == attackframe0)
    {
        hitbox1 = create_hitbox(10, -24,29, 24, 5.75,0, 8,14,8,10,0,0,2,3.5,"mid",1);
        attackhurtbox0 = create_hurtbox(8 * image_xscale,-26,28 * image_xscale,24,2,-2);
        hitbox1.scaling = 1.2;
        hitbox1.hitsound = snd_hitsound1;
        hitbox1.juggle = 2;
        
    }
    
    //chainmove
    if( state_timer > 2)
    {
        if(input_check(4,1,0,0,0) || input_check(5,1,0,0,0) || input_check(6,1,0,0,0))
        {
            chainmove = "light";
        }
        
        if(input_check(2,1,0,0,0) || input_check(1,1,0,0,0) || input_check(3,1,0,0,0))
        {
            chainmove = "clight";
        }
    
        if(input_check(2,1,0,0,0) || input_check(1,1,0,0,0) || input_check(3,1,0,0,0))
        {
            chainmove = "clight";
        }
        
        if(input_check(4,0,1,0,0) || input_check(5,0,1,0,0) || input_check(6,0,1,0,0))
        {
            chainmove = "medium";
        }
        
        if(input_check(2,0,1,0,0) || input_check(1,0,1,0,0) || input_check(3,0,1,0,0))
        {
            chainmove = "cmedium";
        }
        
        if(input_check(4,0,0,1,0) || input_check(5,0,0,1,0) || input_check(6,0,0,1,0))
        {
            chainmove = "heavy";
        }
        
        if(input_check(2,0,0,1,0) || input_check(1,0,0,1,0))
        {
            chainmove = "cheavy";
        }
    }
    
    
    attack_base(endstate,0,0,1,1,0);
    
    
    if(hitting == 1 && otherplayer.hitpause == 0 && otherplayer.hitstun < 12 && ((input_check(4,1,0,0,0) || input_check(5,1,0,0,0) || input_check(6,1,0,0,0)) || chainmove == "light"))
    {
        state_timer = 0;
        if(hitbox1 != -1)
        {
            instance_destroy(hitbox1);
        }
    }
}
else if(synmode < 0)
{
    if(state_new == 1)
    {
        with(hurtbox0)
    	{
    	    xoffset = -12;
    	    yoffset = -13;
    	    image_xscale  = 24;
    	    image_yscale  = 40;
    	    xshift  = 0;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -8;
    	    yoffset = -31;
    	    image_xscale  = 16;
    	    image_yscale  = 18;
    	    xshift = 0;
    	}
    	
    	with(collisionbox)
    	{
    	    xoffset = -8;
    	    yoffset = -13;
    	    image_xscale  = 16;
    	    image_yscale  = 40;
    	    xshift = 0;
    	}
    	
    	chainmove = 0;
    	attackframe0 = 2;
    	endstate = 11;
    }
    
    //animation
    sprite_index = spr_dsyn_5l;
    
    //attack
    if(state_timer == attackframe0)
    {
        hitbox1 = create_hitbox(10, -24,29, 24, 5.75,0, 8,15,8,10,0,0,2,3.5,"mid",1);
        attackhurtbox0 = create_hurtbox(8 * image_xscale,-26,28 * image_xscale,24,2,-2);
        hitbox1.scaling = 1.2;
        hitbox1.hitsound = snd_hitsound1;
        hitbox1.juggle = 2;
        
    }
    
    //chainmove
    if( state_timer > 2)
    {
        if(input_check(4,1,0,0,0) || input_check(5,1,0,0,0) || input_check(6,1,0,0,0))
        {
            chainmove = "light";
        }
        
        if(input_check(2,1,0,0,0) || input_check(1,1,0,0,0) || input_check(3,1,0,0,0))
        {
            chainmove = "clight";
        }
    
        if(input_check(2,1,0,0,0) || input_check(1,1,0,0,0) || input_check(3,1,0,0,0))
        {
            chainmove = "clight";
        }
        
        if(input_check(4,0,1,0,0) || input_check(5,0,1,0,0) || input_check(6,0,1,0,0))
        {
            chainmove = "medium";
        }
        
        if(input_check(2,0,1,0,0) || input_check(1,0,1,0,0) || input_check(3,0,1,0,0))
        {
            chainmove = "cmedium";
        }
        
        if(input_check(4,0,0,1,0) || input_check(5,0,0,1,0) || input_check(6,0,0,1,0))
        {
            chainmove = "heavy";
        }
        
        if(input_check(2,0,0,1,0) || input_check(1,0,0,1,0))
        {
            chainmove = "cheavy";
        }
    }
    
    attack_base(endstate,0,0,1,1,0);
    
    
    if(hitting == 1 && otherplayer.hitpause == 0 && otherplayer.hitstun < 12 && ((input_check(4,1,0,0,0) || input_check(5,1,0,0,0) || input_check(6,1,0,0,0)) || chainmove == "light"))
    {
        state_timer = 0;
        if(hitbox1 != -1)
        {
            instance_destroy(hitbox1);
        }
    }
}
