{
    "id": "2f5ca857-a30c-45f7-b0e2-61e31a947b8d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_myu_block",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 13,
    "bbox_right": 39,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "40c9f37a-fdfd-470e-9bee-db557a3e13cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f5ca857-a30c-45f7-b0e2-61e31a947b8d",
            "compositeImage": {
                "id": "6c2b56da-eaa7-45fc-a525-9a179828073f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40c9f37a-fdfd-470e-9bee-db557a3e13cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "692bdd38-7f7a-450a-93c8-d63566c99aa5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40c9f37a-fdfd-470e-9bee-db557a3e13cd",
                    "LayerId": "1b4c4ffe-3bfb-4814-b461-4b28a7a28e55"
                }
            ]
        },
        {
            "id": "92af72d5-a354-4c8b-af00-682a61035cbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f5ca857-a30c-45f7-b0e2-61e31a947b8d",
            "compositeImage": {
                "id": "6a635886-e52f-4713-ae18-fbb62af7f8d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92af72d5-a354-4c8b-af00-682a61035cbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ca0a1b6-50b3-4c99-83e0-ba24283e58d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92af72d5-a354-4c8b-af00-682a61035cbd",
                    "LayerId": "1b4c4ffe-3bfb-4814-b461-4b28a7a28e55"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1b4c4ffe-3bfb-4814-b461-4b28a7a28e55",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2f5ca857-a30c-45f7-b0e2-61e31a947b8d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 32
}