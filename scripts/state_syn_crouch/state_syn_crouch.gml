live_call();

if(state_new == 1)
{
    with(hurtbox0)
	{
	    xoffset = -12;
	    yoffset = -3;
	    image_xscale  = 24;
	    image_yscale  = 34;
	    xshift  = 0;
	}
	
	with(hurtbox1)
	{
	    xoffset = -8;
	    yoffset = -21;
	    image_xscale  = 16;
	    image_yscale  = 18;
	    xshift = 0;
	}
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -9;
	    image_xscale  = 16;
	    image_yscale  = 40;
	    xshift = 0;
	}
}

if(key_down == 0)
{
    state_switch("stand")
}

vx = Approach(vx,0,ground_fric);

image_xscale = -frontOrBack;

//clight
if(input_check(2,1,0,0,0) || input_check(1,1,0,0,0) || input_check(3,1,0,0,0))
{
    state_switch("clight");
}

//cmedium
if(input_check(2,0,1,0,0) || input_check(1,0,1,0,0) || input_check(3,0,1,0,0))
{
    state_switch("cmedium");
}

//cheavy
if(input_check(2,0,0,1,0) || input_check(1,0,0,1,0) || input_check(3,0,0,1,0))
{
    state_switch("cheavy");
}

//hcb
if(hcb_buffer > 0)
{
    if(input_check(0,0,0,1,0))
    {
        if(ex > 49.9)
        {
            state_switch("hcbh");
        }
        else
        {
            state_switch("hcbm");
        }
    }
    else if(input_check(0,0,1,0,0))
    {
        state_switch("hcbm");
    }
    else if(input_check(0,1,0,0,0))
    {
        state_switch("hcbl");
    }
}

//qcf
if(qcf_buffer > 0)
{
    if(input_check(0,0,0,1,0))
    {
        if(ex > 49.9)
        {
            state_switch("qcfh");
        }
        else
        {
            state_switch("qcfm");
        }
    }
    else if(input_check(0,0,1,0,0))
    {
        state_switch("qcfm");
    }
    else if(input_check(0,1,0,0,0))
    {
        state_switch("qcfl");
    }
}

//srk
if(srk_buffer > 0)
{
    if(input_check(0,0,0,1,0))
    {
        if(ex > 49.9)
        {
            state_switch("srkh");
        }
        else
        {
            state_switch("srkm");
        }
    }
    else if(input_check(0,0,1,0,0))
    {
        state_switch("srkm");
    }
    else if(input_check(0,1,0,0,0))
    {
        state_switch("srkl");
    }
}

//dash
//fdash
if(ff_buffer > 0 || (input_check(6, 1, 1, 0, 0)) || (input_check(5, 1, 1, 0, 0)))
{
        state_switch("fdash");
}
//bdash
if(bb_buffer > 0 || (input_check(4, 1, 1, 0, 0)))
{
        state_switch("bdash");
}

//grab
if(input_check(0,1,0,0,1))
{
    state_switch("grab");
}


//block
if(blockstun > 0)
{
    state_switch("cblock")
}

//Synmode
if(synmode > 1)
{
    sprite_index = spr_lsyn_crouch;
}
else if(synmode < -1)
{
    sprite_index = spr_dsyn_crouch;
}
else
{
    state_switch("transform");
}

//hitstun
if(hitstun > 0)
{
    state_switch("hitstun");   
}