{
    "id": "83a6de8d-8c80-4aac-84c4-dd710c859817",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_myu_hitstun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 11,
    "bbox_right": 46,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cbc8ce9e-b510-4adc-ae7d-0b95aaea1c09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83a6de8d-8c80-4aac-84c4-dd710c859817",
            "compositeImage": {
                "id": "d15a64c0-ccc5-4e81-bc8e-ebde243117f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbc8ce9e-b510-4adc-ae7d-0b95aaea1c09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b236351-6f79-40af-8fa5-dbc1c7c5d22c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbc8ce9e-b510-4adc-ae7d-0b95aaea1c09",
                    "LayerId": "f1765aaa-4ca6-42fc-8c66-3df9dc34ef9b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f1765aaa-4ca6-42fc-8c66-3df9dc34ef9b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83a6de8d-8c80-4aac-84c4-dd710c859817",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 34
}