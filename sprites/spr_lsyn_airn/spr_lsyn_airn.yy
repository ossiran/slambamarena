{
    "id": "aa397c1c-8dd9-43d9-9c14-6535d624a645",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lsyn_airn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 50,
    "bbox_right": 82,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3917c3cf-0730-4c31-a55e-c74ebeffd6a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa397c1c-8dd9-43d9-9c14-6535d624a645",
            "compositeImage": {
                "id": "e20a33a9-3c25-4ebd-8521-385124bd0431",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3917c3cf-0730-4c31-a55e-c74ebeffd6a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ebb0ab3-db34-44e5-a5bb-2fb628f2cea0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3917c3cf-0730-4c31-a55e-c74ebeffd6a4",
                    "LayerId": "9ac4ce0b-f321-4329-94a2-a81b9570f77e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "9ac4ce0b-f321-4329-94a2-a81b9570f77e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa397c1c-8dd9-43d9-9c14-6535d624a645",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}