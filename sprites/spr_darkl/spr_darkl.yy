{
    "id": "4f972648-a197-48b3-a690-9ead94cf2df1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_darkl",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 118,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5204f529-8fb1-4ad2-b479-961b3a5eda98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f972648-a197-48b3-a690-9ead94cf2df1",
            "compositeImage": {
                "id": "9fa3181c-58b2-4f7f-8896-a2cb6892c62d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5204f529-8fb1-4ad2-b479-961b3a5eda98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "090c9ea6-4cee-49db-b8fe-d5818695c222",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5204f529-8fb1-4ad2-b479-961b3a5eda98",
                    "LayerId": "aaff930c-22fe-4553-bfcc-99cdd1e3ac58"
                }
            ]
        },
        {
            "id": "e41da51c-ba77-44e2-9cc7-ee19a6371c82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f972648-a197-48b3-a690-9ead94cf2df1",
            "compositeImage": {
                "id": "ef253417-9bd6-42f9-bd8b-c20f517702e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e41da51c-ba77-44e2-9cc7-ee19a6371c82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fcc1838-aa6e-45dd-b9ea-e786ff3a2d72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e41da51c-ba77-44e2-9cc7-ee19a6371c82",
                    "LayerId": "aaff930c-22fe-4553-bfcc-99cdd1e3ac58"
                }
            ]
        },
        {
            "id": "455647ca-af0e-4970-91fb-ba2d481dea64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f972648-a197-48b3-a690-9ead94cf2df1",
            "compositeImage": {
                "id": "21d20ac9-ad49-4fdc-a2dc-8c9aa5a49d1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "455647ca-af0e-4970-91fb-ba2d481dea64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0ab410d-19d0-4429-9afe-de8b1a335ac3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "455647ca-af0e-4970-91fb-ba2d481dea64",
                    "LayerId": "aaff930c-22fe-4553-bfcc-99cdd1e3ac58"
                }
            ]
        },
        {
            "id": "30040227-f9d0-4d7e-8086-6254a01de495",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f972648-a197-48b3-a690-9ead94cf2df1",
            "compositeImage": {
                "id": "a7ccfde1-edcc-4513-a26d-3428f8470639",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30040227-f9d0-4d7e-8086-6254a01de495",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12f9b2ca-7564-45e8-9ef9-ade91d2e99ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30040227-f9d0-4d7e-8086-6254a01de495",
                    "LayerId": "aaff930c-22fe-4553-bfcc-99cdd1e3ac58"
                }
            ]
        },
        {
            "id": "6fc578ef-9eba-4a41-90a6-5a7b57032a07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f972648-a197-48b3-a690-9ead94cf2df1",
            "compositeImage": {
                "id": "06dcfc05-48f8-4a02-bb3e-110f5828201d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fc578ef-9eba-4a41-90a6-5a7b57032a07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80fb3057-c04a-461e-8c8e-d4cb95f75916",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fc578ef-9eba-4a41-90a6-5a7b57032a07",
                    "LayerId": "aaff930c-22fe-4553-bfcc-99cdd1e3ac58"
                }
            ]
        },
        {
            "id": "5641cd34-46f1-4d6b-a458-0bf56510b5bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f972648-a197-48b3-a690-9ead94cf2df1",
            "compositeImage": {
                "id": "af861b2e-60f5-48fb-986e-34e0faf2a5f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5641cd34-46f1-4d6b-a458-0bf56510b5bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cde2ca22-1721-4307-ad51-25997a740e22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5641cd34-46f1-4d6b-a458-0bf56510b5bd",
                    "LayerId": "aaff930c-22fe-4553-bfcc-99cdd1e3ac58"
                }
            ]
        },
        {
            "id": "3f862d20-7df6-46f9-9a2e-45583cfeb3db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f972648-a197-48b3-a690-9ead94cf2df1",
            "compositeImage": {
                "id": "aab052c7-cbc4-457f-9d33-8396fef6bb6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f862d20-7df6-46f9-9a2e-45583cfeb3db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "364a5dba-2afe-4a90-9d01-b7d40fd80496",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f862d20-7df6-46f9-9a2e-45583cfeb3db",
                    "LayerId": "aaff930c-22fe-4553-bfcc-99cdd1e3ac58"
                }
            ]
        },
        {
            "id": "2744b792-6f94-414b-bff0-1cb553c9e8db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f972648-a197-48b3-a690-9ead94cf2df1",
            "compositeImage": {
                "id": "b7aa8e95-5c49-4885-a25d-e5e2cdbdce2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2744b792-6f94-414b-bff0-1cb553c9e8db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1705a294-81f6-4ef2-89d8-692d712bdebe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2744b792-6f94-414b-bff0-1cb553c9e8db",
                    "LayerId": "aaff930c-22fe-4553-bfcc-99cdd1e3ac58"
                }
            ]
        },
        {
            "id": "18b7c5a5-dfaf-45f2-98ca-7b2e07f77d92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f972648-a197-48b3-a690-9ead94cf2df1",
            "compositeImage": {
                "id": "f76f4d1a-8317-4083-b70f-5edeb2fed1c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18b7c5a5-dfaf-45f2-98ca-7b2e07f77d92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3a04e0c-b30d-481c-a5ef-9000a135ea8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18b7c5a5-dfaf-45f2-98ca-7b2e07f77d92",
                    "LayerId": "aaff930c-22fe-4553-bfcc-99cdd1e3ac58"
                }
            ]
        },
        {
            "id": "d568cf15-ad2e-4c7b-8605-b79c36447d8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f972648-a197-48b3-a690-9ead94cf2df1",
            "compositeImage": {
                "id": "b8bfc99d-179c-46bb-a5d0-77bf225eaa5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d568cf15-ad2e-4c7b-8605-b79c36447d8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d78a7223-4f40-4a6d-9f3f-29f2d33c6e6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d568cf15-ad2e-4c7b-8605-b79c36447d8d",
                    "LayerId": "aaff930c-22fe-4553-bfcc-99cdd1e3ac58"
                }
            ]
        },
        {
            "id": "30dd03f6-9a31-422e-b369-32c4d7c05608",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f972648-a197-48b3-a690-9ead94cf2df1",
            "compositeImage": {
                "id": "7d5cd13f-6ff3-4681-96a0-8c7b3122dce4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30dd03f6-9a31-422e-b369-32c4d7c05608",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "528ffdf0-2b1d-444c-a667-32647caf79c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30dd03f6-9a31-422e-b369-32c4d7c05608",
                    "LayerId": "aaff930c-22fe-4553-bfcc-99cdd1e3ac58"
                }
            ]
        },
        {
            "id": "88fdc55f-7388-422a-a509-5558e18c6f1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f972648-a197-48b3-a690-9ead94cf2df1",
            "compositeImage": {
                "id": "78eddf5d-8024-4261-ae59-9850712a150b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88fdc55f-7388-422a-a509-5558e18c6f1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "833a2abe-2a16-4801-8cde-9c06880dcb45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88fdc55f-7388-422a-a509-5558e18c6f1d",
                    "LayerId": "aaff930c-22fe-4553-bfcc-99cdd1e3ac58"
                }
            ]
        },
        {
            "id": "e5caf175-ce91-44f0-934c-891f3c764ddc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f972648-a197-48b3-a690-9ead94cf2df1",
            "compositeImage": {
                "id": "8f230768-0df7-4ad4-ab59-2b8fab4b3899",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5caf175-ce91-44f0-934c-891f3c764ddc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7e7cf40-1d41-476a-a09e-167ac7dbf345",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5caf175-ce91-44f0-934c-891f3c764ddc",
                    "LayerId": "aaff930c-22fe-4553-bfcc-99cdd1e3ac58"
                }
            ]
        },
        {
            "id": "0d83b311-4104-4462-aa46-b108e355ad40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f972648-a197-48b3-a690-9ead94cf2df1",
            "compositeImage": {
                "id": "4f635cd9-13eb-432e-ba6f-45dbec28d3e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d83b311-4104-4462-aa46-b108e355ad40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bbb242e-fde6-4bc5-b376-afaa3d462021",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d83b311-4104-4462-aa46-b108e355ad40",
                    "LayerId": "aaff930c-22fe-4553-bfcc-99cdd1e3ac58"
                }
            ]
        },
        {
            "id": "2a09546a-967d-411f-bea9-b03feb930775",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f972648-a197-48b3-a690-9ead94cf2df1",
            "compositeImage": {
                "id": "e6c0c3e0-52eb-4222-bf24-b8c0cd1c2fce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a09546a-967d-411f-bea9-b03feb930775",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43ad622b-7455-44df-8490-bf9a1cb2bd20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a09546a-967d-411f-bea9-b03feb930775",
                    "LayerId": "aaff930c-22fe-4553-bfcc-99cdd1e3ac58"
                }
            ]
        },
        {
            "id": "4fa310b2-4edd-459e-ba1d-a1b3f50796a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f972648-a197-48b3-a690-9ead94cf2df1",
            "compositeImage": {
                "id": "6ce50d21-d533-4961-8bc3-9796d9149d23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fa310b2-4edd-459e-ba1d-a1b3f50796a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c0a3335-d5ac-4799-bcd8-996c67abe944",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fa310b2-4edd-459e-ba1d-a1b3f50796a0",
                    "LayerId": "aaff930c-22fe-4553-bfcc-99cdd1e3ac58"
                }
            ]
        },
        {
            "id": "7a0338ae-27aa-4387-bf2c-e6975cbde48a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f972648-a197-48b3-a690-9ead94cf2df1",
            "compositeImage": {
                "id": "b3aca98a-9f0d-4846-abaf-4726e560dd69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a0338ae-27aa-4387-bf2c-e6975cbde48a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e5be1ab-9017-4cb1-8225-47daa6b9afe3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a0338ae-27aa-4387-bf2c-e6975cbde48a",
                    "LayerId": "aaff930c-22fe-4553-bfcc-99cdd1e3ac58"
                }
            ]
        },
        {
            "id": "5af1c0d0-d596-4341-9526-bea1cb9e0f6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f972648-a197-48b3-a690-9ead94cf2df1",
            "compositeImage": {
                "id": "7bb4d26d-f5dc-4c84-80dc-6d687cc9c504",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5af1c0d0-d596-4341-9526-bea1cb9e0f6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e15f96c-ccdc-4902-8f38-f3ab017307e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5af1c0d0-d596-4341-9526-bea1cb9e0f6a",
                    "LayerId": "aaff930c-22fe-4553-bfcc-99cdd1e3ac58"
                }
            ]
        },
        {
            "id": "b94707a1-eaeb-48bf-9253-c2fc735f93eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f972648-a197-48b3-a690-9ead94cf2df1",
            "compositeImage": {
                "id": "3dd110db-b2f0-4dac-abcf-51616bbe9867",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b94707a1-eaeb-48bf-9253-c2fc735f93eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93d0ba35-54c6-4954-853e-4fb4b7d3e7b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b94707a1-eaeb-48bf-9253-c2fc735f93eb",
                    "LayerId": "aaff930c-22fe-4553-bfcc-99cdd1e3ac58"
                }
            ]
        },
        {
            "id": "8af2b588-3fc5-4b6c-bf3d-96cbb5e46741",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f972648-a197-48b3-a690-9ead94cf2df1",
            "compositeImage": {
                "id": "a41bd18a-672d-4226-b80f-8d5b00ebb28b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8af2b588-3fc5-4b6c-bf3d-96cbb5e46741",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aab51845-14c2-4f7b-968b-222aa7ebb3e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8af2b588-3fc5-4b6c-bf3d-96cbb5e46741",
                    "LayerId": "aaff930c-22fe-4553-bfcc-99cdd1e3ac58"
                }
            ]
        },
        {
            "id": "89e75bc8-88f1-42e8-9972-17874c22f787",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f972648-a197-48b3-a690-9ead94cf2df1",
            "compositeImage": {
                "id": "8aff774c-1807-4742-8d77-b7c9c82fa89d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89e75bc8-88f1-42e8-9972-17874c22f787",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a45322f4-6b0e-49ad-9864-5f88dfdc0d0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89e75bc8-88f1-42e8-9972-17874c22f787",
                    "LayerId": "aaff930c-22fe-4553-bfcc-99cdd1e3ac58"
                }
            ]
        },
        {
            "id": "d912972f-5f17-481a-b895-7a012e0425ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f972648-a197-48b3-a690-9ead94cf2df1",
            "compositeImage": {
                "id": "1c095a0b-0530-47d6-9166-f6f1ec42fb52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d912972f-5f17-481a-b895-7a012e0425ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0311cf0a-20e1-42d5-b2a2-26461a40b753",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d912972f-5f17-481a-b895-7a012e0425ea",
                    "LayerId": "aaff930c-22fe-4553-bfcc-99cdd1e3ac58"
                }
            ]
        },
        {
            "id": "0983396c-d846-4845-95f6-5415128a27b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f972648-a197-48b3-a690-9ead94cf2df1",
            "compositeImage": {
                "id": "6e9285c7-a9bf-46e0-9a8b-ef4051472a48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0983396c-d846-4845-95f6-5415128a27b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76f82e60-1504-48ab-b658-baebb93b1be7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0983396c-d846-4845-95f6-5415128a27b8",
                    "LayerId": "aaff930c-22fe-4553-bfcc-99cdd1e3ac58"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 70,
    "layers": [
        {
            "id": "aaff930c-22fe-4553-bfcc-99cdd1e3ac58",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f972648-a197-48b3-a690-9ead94cf2df1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 24,
    "yorig": 24
}