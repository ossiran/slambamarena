{
    "id": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_chum_ice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 120,
    "bbox_left": 24,
    "bbox_right": 141,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e6f81a1-344f-40c2-89b6-6322bdc97bef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "compositeImage": {
                "id": "b7f9d5e6-f1c8-462a-8236-fc76b3376b68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e6f81a1-344f-40c2-89b6-6322bdc97bef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30be58e1-3afd-4595-86b4-2feff5cb9c2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e6f81a1-344f-40c2-89b6-6322bdc97bef",
                    "LayerId": "a5798c57-bca2-4e79-8683-816e196b5f04"
                }
            ]
        },
        {
            "id": "3aaa89a3-b593-4abb-9f1c-0f8ab27fe4b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "compositeImage": {
                "id": "9f30c8ac-1201-4432-b27d-cf640bd0198f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3aaa89a3-b593-4abb-9f1c-0f8ab27fe4b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4272eb03-643c-41a0-83f7-452fdd5965da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3aaa89a3-b593-4abb-9f1c-0f8ab27fe4b1",
                    "LayerId": "a5798c57-bca2-4e79-8683-816e196b5f04"
                }
            ]
        },
        {
            "id": "0448d3c6-78b6-4f76-9e13-0631d7e7d86e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "compositeImage": {
                "id": "bf86ab81-5255-48a5-96b8-ca77b16ccb7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0448d3c6-78b6-4f76-9e13-0631d7e7d86e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81cddb88-4d05-48c1-97d9-253348da83fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0448d3c6-78b6-4f76-9e13-0631d7e7d86e",
                    "LayerId": "a5798c57-bca2-4e79-8683-816e196b5f04"
                }
            ]
        },
        {
            "id": "599aa5eb-5845-483c-8a27-e1c8eb010664",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "compositeImage": {
                "id": "816a3a9f-7db6-499e-9cb6-dfea1ce34c1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "599aa5eb-5845-483c-8a27-e1c8eb010664",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c076655e-9d7a-45b4-86fe-aca6ffb69835",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "599aa5eb-5845-483c-8a27-e1c8eb010664",
                    "LayerId": "a5798c57-bca2-4e79-8683-816e196b5f04"
                }
            ]
        },
        {
            "id": "0c3004c0-5d4c-48b3-8fde-be62e3a5dc08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "compositeImage": {
                "id": "967f84b1-2cef-47f2-877a-95a4582c7ee2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c3004c0-5d4c-48b3-8fde-be62e3a5dc08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e898a6c-a986-43eb-8cf7-8d35c049f12c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c3004c0-5d4c-48b3-8fde-be62e3a5dc08",
                    "LayerId": "a5798c57-bca2-4e79-8683-816e196b5f04"
                }
            ]
        },
        {
            "id": "85126d00-e602-4c0a-addd-34ec54a5acdc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "compositeImage": {
                "id": "9afeedcd-3be9-43f2-979d-f5193eede565",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85126d00-e602-4c0a-addd-34ec54a5acdc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "098775b4-ac8c-43bd-8889-2d6f9efd6713",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85126d00-e602-4c0a-addd-34ec54a5acdc",
                    "LayerId": "a5798c57-bca2-4e79-8683-816e196b5f04"
                }
            ]
        },
        {
            "id": "232f62b8-f5a9-492f-a447-e1b673b9635f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "compositeImage": {
                "id": "b6145c2f-8978-4552-adc0-1c94a39dbb2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "232f62b8-f5a9-492f-a447-e1b673b9635f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a74d813b-29b6-416d-9dca-41cc8b33115c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "232f62b8-f5a9-492f-a447-e1b673b9635f",
                    "LayerId": "a5798c57-bca2-4e79-8683-816e196b5f04"
                }
            ]
        },
        {
            "id": "5592e37c-7cfe-4df1-bd51-e5556255efda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "compositeImage": {
                "id": "82a3d090-fb63-429b-adaf-0e697306a5a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5592e37c-7cfe-4df1-bd51-e5556255efda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84e949ef-be16-4730-a993-0d5f521396e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5592e37c-7cfe-4df1-bd51-e5556255efda",
                    "LayerId": "a5798c57-bca2-4e79-8683-816e196b5f04"
                }
            ]
        },
        {
            "id": "a036584b-0621-4121-8526-f3b8790d95fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "compositeImage": {
                "id": "92f38404-d488-4b18-9d10-e40cf201e88f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a036584b-0621-4121-8526-f3b8790d95fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb6cebd5-2bcd-4f0a-ace3-ed75c79fec11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a036584b-0621-4121-8526-f3b8790d95fe",
                    "LayerId": "a5798c57-bca2-4e79-8683-816e196b5f04"
                }
            ]
        },
        {
            "id": "b9ebd860-0086-41f1-8319-d22cbee19f2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "compositeImage": {
                "id": "573d4c01-c403-4887-962b-bfa2cfce70b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9ebd860-0086-41f1-8319-d22cbee19f2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7929b4ea-f5a8-4c4b-b782-6118efea644d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9ebd860-0086-41f1-8319-d22cbee19f2d",
                    "LayerId": "a5798c57-bca2-4e79-8683-816e196b5f04"
                }
            ]
        },
        {
            "id": "b63baf48-e4eb-4f5d-b986-215fee9efe93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "compositeImage": {
                "id": "c8c3aa19-c6a1-43eb-aaf0-4c5cd5c46094",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b63baf48-e4eb-4f5d-b986-215fee9efe93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66256375-69dc-4870-8c48-fbae7e5baca6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b63baf48-e4eb-4f5d-b986-215fee9efe93",
                    "LayerId": "a5798c57-bca2-4e79-8683-816e196b5f04"
                }
            ]
        },
        {
            "id": "5200979f-a91c-47a1-bcb4-18fd95424f2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "compositeImage": {
                "id": "8f3a421c-34a5-4509-b763-4eb422df5448",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5200979f-a91c-47a1-bcb4-18fd95424f2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2147384-2550-4374-91ef-b8f4c15889b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5200979f-a91c-47a1-bcb4-18fd95424f2b",
                    "LayerId": "a5798c57-bca2-4e79-8683-816e196b5f04"
                }
            ]
        },
        {
            "id": "e9e33d60-7c6f-4e65-aac7-1cb1883d6b32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "compositeImage": {
                "id": "e5856301-9fc0-46a2-a2c3-a3fdbdf656e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9e33d60-7c6f-4e65-aac7-1cb1883d6b32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4d1a7ec-9a13-4b54-99b6-45e644463101",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9e33d60-7c6f-4e65-aac7-1cb1883d6b32",
                    "LayerId": "a5798c57-bca2-4e79-8683-816e196b5f04"
                }
            ]
        },
        {
            "id": "900fc9e9-5bec-4a3a-bb48-825765f0ca80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "compositeImage": {
                "id": "c627105a-a695-4b2b-9bdd-7295335dadd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "900fc9e9-5bec-4a3a-bb48-825765f0ca80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b58fdf7-29f0-4532-8c4c-2d15e13a2e19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "900fc9e9-5bec-4a3a-bb48-825765f0ca80",
                    "LayerId": "a5798c57-bca2-4e79-8683-816e196b5f04"
                }
            ]
        },
        {
            "id": "fa7c20af-dab5-4e8d-a1c4-6c49df60549a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "compositeImage": {
                "id": "2083fa33-7b99-45b6-92b4-344554a647d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa7c20af-dab5-4e8d-a1c4-6c49df60549a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46695958-7cee-4e67-9b62-43252f05f2d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa7c20af-dab5-4e8d-a1c4-6c49df60549a",
                    "LayerId": "a5798c57-bca2-4e79-8683-816e196b5f04"
                }
            ]
        },
        {
            "id": "afcab4b1-9586-4f67-b09f-3e2cf7dbd98b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "compositeImage": {
                "id": "3c51a9e5-ad73-421e-afea-3ca217c82882",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afcab4b1-9586-4f67-b09f-3e2cf7dbd98b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ed1d7a2-d43f-47de-8d96-299c7fa3be1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afcab4b1-9586-4f67-b09f-3e2cf7dbd98b",
                    "LayerId": "a5798c57-bca2-4e79-8683-816e196b5f04"
                }
            ]
        },
        {
            "id": "bfefb3a7-b5be-442f-9846-210672cb9798",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "compositeImage": {
                "id": "68486ebc-ce19-4a4d-ab3c-c48adbdcb4c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfefb3a7-b5be-442f-9846-210672cb9798",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "802db591-cf4a-417e-9217-79cd6d85caee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfefb3a7-b5be-442f-9846-210672cb9798",
                    "LayerId": "a5798c57-bca2-4e79-8683-816e196b5f04"
                }
            ]
        },
        {
            "id": "26830f64-82ef-469e-b64e-b00ca105f822",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "compositeImage": {
                "id": "17ab299b-d5db-461f-a60b-87fc46405f30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26830f64-82ef-469e-b64e-b00ca105f822",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09775efc-4d81-498c-8ffa-1f633ce451d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26830f64-82ef-469e-b64e-b00ca105f822",
                    "LayerId": "a5798c57-bca2-4e79-8683-816e196b5f04"
                }
            ]
        },
        {
            "id": "7c4d3f85-296f-4a0b-a20f-90f07638bbad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "compositeImage": {
                "id": "215c6ca3-da21-410b-8adb-fd95f5642475",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c4d3f85-296f-4a0b-a20f-90f07638bbad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9951071c-ae4f-44b5-918c-1dafba4da7a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c4d3f85-296f-4a0b-a20f-90f07638bbad",
                    "LayerId": "a5798c57-bca2-4e79-8683-816e196b5f04"
                }
            ]
        },
        {
            "id": "ff335b48-e21a-486d-9014-2bcfff3a629c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "compositeImage": {
                "id": "82a8c166-899e-41fd-b970-8a42ffe31f11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff335b48-e21a-486d-9014-2bcfff3a629c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da130496-7afe-453e-aa51-4f63249bcaef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff335b48-e21a-486d-9014-2bcfff3a629c",
                    "LayerId": "a5798c57-bca2-4e79-8683-816e196b5f04"
                }
            ]
        },
        {
            "id": "b8ca639e-7670-4c4a-b1a6-5eb4cef89d16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "compositeImage": {
                "id": "c2bc6f0f-f526-42c0-b337-f1388df26dd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8ca639e-7670-4c4a-b1a6-5eb4cef89d16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc99f707-1e9b-4db7-bdd4-06ca0b0412c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8ca639e-7670-4c4a-b1a6-5eb4cef89d16",
                    "LayerId": "a5798c57-bca2-4e79-8683-816e196b5f04"
                }
            ]
        },
        {
            "id": "09fecc91-9066-4c25-87cb-1af56ca9b308",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "compositeImage": {
                "id": "134ef67e-a75d-4f33-93a5-b7e25fce5710",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09fecc91-9066-4c25-87cb-1af56ca9b308",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a39b084-7b84-47ca-9fec-e69c08bfab2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09fecc91-9066-4c25-87cb-1af56ca9b308",
                    "LayerId": "a5798c57-bca2-4e79-8683-816e196b5f04"
                }
            ]
        },
        {
            "id": "0c0b4a64-1c7e-409f-a74a-7bd74c56d1a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "compositeImage": {
                "id": "b6cdda81-2de4-4836-b4c1-dd042fe06b72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c0b4a64-1c7e-409f-a74a-7bd74c56d1a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "301e0de9-e41b-40f5-9883-9f069de24c73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c0b4a64-1c7e-409f-a74a-7bd74c56d1a0",
                    "LayerId": "a5798c57-bca2-4e79-8683-816e196b5f04"
                }
            ]
        },
        {
            "id": "7ddeebda-766c-4575-8c1f-e4f534fca048",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "compositeImage": {
                "id": "dd1fd988-d9a8-4580-a115-003effb1bcfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ddeebda-766c-4575-8c1f-e4f534fca048",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7582dbb8-73f2-45e3-9e42-74e1c98fd5b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ddeebda-766c-4575-8c1f-e4f534fca048",
                    "LayerId": "a5798c57-bca2-4e79-8683-816e196b5f04"
                }
            ]
        },
        {
            "id": "9f023218-245b-498e-82b1-ae5ee83a37f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "compositeImage": {
                "id": "50588965-e6a8-4d65-9b62-e9db2a579bd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f023218-245b-498e-82b1-ae5ee83a37f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e911c60-f6cf-4d86-951a-668c43fef5a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f023218-245b-498e-82b1-ae5ee83a37f5",
                    "LayerId": "a5798c57-bca2-4e79-8683-816e196b5f04"
                }
            ]
        },
        {
            "id": "8b276144-7922-40f5-a6e0-e7ac0412bc9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "compositeImage": {
                "id": "69ed8f62-04a4-49a9-9fc4-91ebfd59c64d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b276144-7922-40f5-a6e0-e7ac0412bc9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70b95410-4fc7-467a-a40e-aa3352f9b807",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b276144-7922-40f5-a6e0-e7ac0412bc9a",
                    "LayerId": "a5798c57-bca2-4e79-8683-816e196b5f04"
                }
            ]
        },
        {
            "id": "ce8c1185-6740-4cfd-8a18-2b1267199561",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "compositeImage": {
                "id": "ebf55a64-7f96-486c-baca-198a7c150c6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce8c1185-6740-4cfd-8a18-2b1267199561",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0daaca7b-91ef-4983-84fb-f986a43ddcbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce8c1185-6740-4cfd-8a18-2b1267199561",
                    "LayerId": "a5798c57-bca2-4e79-8683-816e196b5f04"
                }
            ]
        },
        {
            "id": "210eeb8d-9494-4d63-953f-9e454102c879",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "compositeImage": {
                "id": "5c467529-4820-4ebd-9af7-a86a187ae5fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "210eeb8d-9494-4d63-953f-9e454102c879",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3136ef76-3d53-423f-9b48-29dee7f1833d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "210eeb8d-9494-4d63-953f-9e454102c879",
                    "LayerId": "a5798c57-bca2-4e79-8683-816e196b5f04"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 148,
    "layers": [
        {
            "id": "a5798c57-bca2-4e79-8683-816e196b5f04",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "62c77ed1-e449-4f86-8cee-ad4580e527bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 148,
    "xorig": 63,
    "yorig": 87
}