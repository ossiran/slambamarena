if(state_timer == 0)
{
    //resetting menu
    menu_maxx = 1;
    menu_maxy = 0;
    menu_indexx = 0;
    menu_indexy = 0;
    
    menu_array[0,0] = rm_battlefield;
    menu_array[0,1] = spr_battlefield;
    
    menu_array[1,0] = rm_battlefield;
    menu_array[1,1] = spr_battlefield;
    
    with(obj_controller)
    {
        positionx_max = 1;
        positionx_min = 0;
        positiony_max = 0;
        positiony_min = 0;
        
        positionx = 0;
        positiony = 0;  
        
        ready_up = 0;
    }
}

with(obj_controller)
{
    if(key_essence == 1)
    {
        state_switch("charselect2");
        //resetting menu
        obj_system.menu_array[0,0] = 0;
        obj_system.menu_array[0,1] = 0;
    
        obj_system.menu_array[1,0] = 0;
        obj_system.menu_array[1,1] = 0;
        obj_system.menu_indexx = 0;
        obj_system.menu_indexy = 0;
    }
    
    if(ready_up == 1)
    {
        room_goto(obj_system.menu_array[positionx, 0]);
        //resetting menu
        menu_array[0,0] = 0;
        menu_array[0,1] = 0;

        menu_array[1,0] = 0;
        menu_array[1,1] = 0;
        menu_indexx = 0;
        menu_indexy = 0;
        instance_destroy(obj_controller);
        with(obj_system)
        {
            if(training = 0)
            {
                state_switch("start_match");
            }
            else
            {
                state_switch("training");
            }
        }
    }
}