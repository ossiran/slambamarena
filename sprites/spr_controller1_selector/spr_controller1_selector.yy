{
    "id": "00d73b79-9cef-4e68-bb30-cb8e9bb96acd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_controller1_selector",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c8b52760-5088-4e7e-adc4-75bf7027f25a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00d73b79-9cef-4e68-bb30-cb8e9bb96acd",
            "compositeImage": {
                "id": "51ddb69f-e1b3-4ef0-9600-8be74c41a301",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8b52760-5088-4e7e-adc4-75bf7027f25a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db42b9f6-17f0-4251-a527-1fdc3fcb08b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8b52760-5088-4e7e-adc4-75bf7027f25a",
                    "LayerId": "3234f87f-044c-43d1-bb06-fe10dd72d892"
                }
            ]
        },
        {
            "id": "44a089d9-6961-4cba-a973-e38af7b00b98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00d73b79-9cef-4e68-bb30-cb8e9bb96acd",
            "compositeImage": {
                "id": "d4ea3add-148c-42f6-baa4-b6b5fa00869e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44a089d9-6961-4cba-a973-e38af7b00b98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3be72021-4220-4de4-b086-a792384f1367",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44a089d9-6961-4cba-a973-e38af7b00b98",
                    "LayerId": "3234f87f-044c-43d1-bb06-fe10dd72d892"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3234f87f-044c-43d1-bb06-fe10dd72d892",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "00d73b79-9cef-4e68-bb30-cb8e9bb96acd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}