{
    "id": "155792bf-e969-4e25-8ede-78db2f80f43e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_jh_fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "857ba869-5ed9-4fe1-a9a4-7647a1e32ead",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "155792bf-e969-4e25-8ede-78db2f80f43e",
            "compositeImage": {
                "id": "c8e721d1-0a61-4dc5-8c5b-057e75f3dadb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "857ba869-5ed9-4fe1-a9a4-7647a1e32ead",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8245abf-c924-45b7-b8e0-a61ed17acf14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "857ba869-5ed9-4fe1-a9a4-7647a1e32ead",
                    "LayerId": "e792af2b-ba6a-4017-bb7f-95579ed1356d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e792af2b-ba6a-4017-bb7f-95579ed1356d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "155792bf-e969-4e25-8ede-78db2f80f43e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}