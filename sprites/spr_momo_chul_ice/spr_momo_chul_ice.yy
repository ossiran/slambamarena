{
    "id": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_chul_ice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 120,
    "bbox_left": 24,
    "bbox_right": 130,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f6aa800e-558b-4989-8107-ce1bc675b98b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "compositeImage": {
                "id": "043d2522-9949-4219-8d92-17de16bcf1dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6aa800e-558b-4989-8107-ce1bc675b98b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d987274e-1d5c-42c6-8165-b61da5a4676f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6aa800e-558b-4989-8107-ce1bc675b98b",
                    "LayerId": "94696140-db95-40c1-adce-6ad72a86f534"
                }
            ]
        },
        {
            "id": "ebec5248-3edd-4d5c-ad30-cb0c5aef52d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "compositeImage": {
                "id": "7c57addf-962c-4956-b137-1784934e76da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebec5248-3edd-4d5c-ad30-cb0c5aef52d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5642714e-ab7c-4a97-a93b-019125490b5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebec5248-3edd-4d5c-ad30-cb0c5aef52d0",
                    "LayerId": "94696140-db95-40c1-adce-6ad72a86f534"
                }
            ]
        },
        {
            "id": "764da866-8a7f-43aa-981d-b4c48ff3e8db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "compositeImage": {
                "id": "67f4ac36-bf54-43f8-ae90-9105f1b0d3f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "764da866-8a7f-43aa-981d-b4c48ff3e8db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3636f00-773b-450b-aa97-3714c9014362",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "764da866-8a7f-43aa-981d-b4c48ff3e8db",
                    "LayerId": "94696140-db95-40c1-adce-6ad72a86f534"
                }
            ]
        },
        {
            "id": "dbefeca1-f41f-4e9d-a8b8-48f58f497ca1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "compositeImage": {
                "id": "1c7d1e5c-6777-4530-b869-19b58eca8004",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbefeca1-f41f-4e9d-a8b8-48f58f497ca1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddc0c5de-7307-421a-99f7-1fc3ab06fd2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbefeca1-f41f-4e9d-a8b8-48f58f497ca1",
                    "LayerId": "94696140-db95-40c1-adce-6ad72a86f534"
                }
            ]
        },
        {
            "id": "811f29bb-2001-4068-86bc-cb26d16d06f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "compositeImage": {
                "id": "8ed02019-299a-40f7-a9be-6b2599cf056f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "811f29bb-2001-4068-86bc-cb26d16d06f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6286900-54fb-4201-8cbf-bfd491578684",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "811f29bb-2001-4068-86bc-cb26d16d06f6",
                    "LayerId": "94696140-db95-40c1-adce-6ad72a86f534"
                }
            ]
        },
        {
            "id": "2f5c5c5b-5409-406c-b598-ff00b444ab3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "compositeImage": {
                "id": "c66a547c-9c50-450d-9e89-c2cb6e7a884c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f5c5c5b-5409-406c-b598-ff00b444ab3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bdc387b-b78c-4006-a1f1-860832104de7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f5c5c5b-5409-406c-b598-ff00b444ab3e",
                    "LayerId": "94696140-db95-40c1-adce-6ad72a86f534"
                }
            ]
        },
        {
            "id": "45059466-3c72-4c49-914c-50f2c79e4fbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "compositeImage": {
                "id": "6591b4af-745d-43e6-893d-1279ecd92dd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45059466-3c72-4c49-914c-50f2c79e4fbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9721fb8c-47cd-4e1e-b00c-4164d7b3134d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45059466-3c72-4c49-914c-50f2c79e4fbe",
                    "LayerId": "94696140-db95-40c1-adce-6ad72a86f534"
                }
            ]
        },
        {
            "id": "c1f575c3-fb26-40ad-b49e-8f1d5cf78e65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "compositeImage": {
                "id": "8ca3198b-4960-426f-bba2-07cf4c7f29ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1f575c3-fb26-40ad-b49e-8f1d5cf78e65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b4e858b-7fb3-42e4-a90f-a00a426211f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1f575c3-fb26-40ad-b49e-8f1d5cf78e65",
                    "LayerId": "94696140-db95-40c1-adce-6ad72a86f534"
                }
            ]
        },
        {
            "id": "b8a498d4-c505-4c7e-b587-a7ecf342a0d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "compositeImage": {
                "id": "00295b83-f159-4cee-8f61-1dbbdfc60b06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8a498d4-c505-4c7e-b587-a7ecf342a0d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9930a8a7-7467-4416-a1f3-dc3521488d8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8a498d4-c505-4c7e-b587-a7ecf342a0d2",
                    "LayerId": "94696140-db95-40c1-adce-6ad72a86f534"
                }
            ]
        },
        {
            "id": "2a1d1117-ce14-4c85-86a1-64ad83989b15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "compositeImage": {
                "id": "6dbf34b7-d219-4918-a4cd-88dff759a5d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a1d1117-ce14-4c85-86a1-64ad83989b15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aeb7731f-cde7-4132-9785-81582edb760f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a1d1117-ce14-4c85-86a1-64ad83989b15",
                    "LayerId": "94696140-db95-40c1-adce-6ad72a86f534"
                }
            ]
        },
        {
            "id": "45460961-092d-482b-9b60-46803cf27b9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "compositeImage": {
                "id": "9062e3f9-bce2-46ff-b8b0-1f91bdc3b5b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45460961-092d-482b-9b60-46803cf27b9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbea7143-d4c2-42dc-967b-9e4b0ee30dc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45460961-092d-482b-9b60-46803cf27b9e",
                    "LayerId": "94696140-db95-40c1-adce-6ad72a86f534"
                }
            ]
        },
        {
            "id": "fcbc06ad-b1cf-4545-825f-c90733f754c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "compositeImage": {
                "id": "1659c5ad-7654-4982-928b-417ee2ec58a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcbc06ad-b1cf-4545-825f-c90733f754c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b37e2393-4dc0-47fc-a9fb-dc35c3c9de03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcbc06ad-b1cf-4545-825f-c90733f754c2",
                    "LayerId": "94696140-db95-40c1-adce-6ad72a86f534"
                }
            ]
        },
        {
            "id": "0c401b91-0c71-4609-8ecc-333b4554a956",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "compositeImage": {
                "id": "2a958a87-510e-4588-a9d0-50ffa0419aef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c401b91-0c71-4609-8ecc-333b4554a956",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17cc5df6-78ca-4a0e-a44f-bb877d52f8c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c401b91-0c71-4609-8ecc-333b4554a956",
                    "LayerId": "94696140-db95-40c1-adce-6ad72a86f534"
                }
            ]
        },
        {
            "id": "58c1684d-3d9d-43f8-bd23-8de615720455",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "compositeImage": {
                "id": "b6808dd6-e392-4659-9d36-41efbfe34204",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58c1684d-3d9d-43f8-bd23-8de615720455",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "841a1b77-6129-499e-bc25-42e0a5a4d480",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58c1684d-3d9d-43f8-bd23-8de615720455",
                    "LayerId": "94696140-db95-40c1-adce-6ad72a86f534"
                }
            ]
        },
        {
            "id": "3ccd9669-b237-44a7-bc48-741ef6e7bc41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "compositeImage": {
                "id": "73483692-0980-436a-8697-fd4e233d6749",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ccd9669-b237-44a7-bc48-741ef6e7bc41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "772ebcb0-521a-4fb2-a0d4-a4d1f305a4ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ccd9669-b237-44a7-bc48-741ef6e7bc41",
                    "LayerId": "94696140-db95-40c1-adce-6ad72a86f534"
                }
            ]
        },
        {
            "id": "7aa4d7b7-f3f8-4666-b935-85492c58b462",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "compositeImage": {
                "id": "4acf6c83-cd52-4010-b071-09276106b4db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7aa4d7b7-f3f8-4666-b935-85492c58b462",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0be3854-b212-4ce8-aebc-3a7188c1181f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7aa4d7b7-f3f8-4666-b935-85492c58b462",
                    "LayerId": "94696140-db95-40c1-adce-6ad72a86f534"
                }
            ]
        },
        {
            "id": "5c9b8b07-87dc-434b-a489-1167db35d3c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "compositeImage": {
                "id": "591595e1-0718-4518-8a68-0be11d80396d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c9b8b07-87dc-434b-a489-1167db35d3c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4a66b09-b316-4ed8-855b-32b2570357ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c9b8b07-87dc-434b-a489-1167db35d3c5",
                    "LayerId": "94696140-db95-40c1-adce-6ad72a86f534"
                }
            ]
        },
        {
            "id": "a976f23f-efd0-4a42-84e2-8aed764a669c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "compositeImage": {
                "id": "07868e0c-06ac-4c00-ae94-739ec94ec473",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a976f23f-efd0-4a42-84e2-8aed764a669c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcabeba3-b343-4dd0-b6a5-f3284fd613f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a976f23f-efd0-4a42-84e2-8aed764a669c",
                    "LayerId": "94696140-db95-40c1-adce-6ad72a86f534"
                }
            ]
        },
        {
            "id": "a4d3226e-ca7b-41fd-9a11-b5b4ffc86fc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "compositeImage": {
                "id": "7e684208-1cd0-4ddf-aa7a-2752961cf06d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4d3226e-ca7b-41fd-9a11-b5b4ffc86fc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d425c8dc-878d-427b-9d7c-c87107e7849f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4d3226e-ca7b-41fd-9a11-b5b4ffc86fc4",
                    "LayerId": "94696140-db95-40c1-adce-6ad72a86f534"
                }
            ]
        },
        {
            "id": "a2790f23-9056-48e7-808e-6caec11e1e46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "compositeImage": {
                "id": "b6e45c72-935d-4593-9237-66046aaa2fcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2790f23-9056-48e7-808e-6caec11e1e46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dc5008b-baa4-43dd-b673-678b891d30b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2790f23-9056-48e7-808e-6caec11e1e46",
                    "LayerId": "94696140-db95-40c1-adce-6ad72a86f534"
                }
            ]
        },
        {
            "id": "22bca7d3-10b9-41c0-8c7b-2310826575b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "compositeImage": {
                "id": "62cc9e62-1794-45a9-af06-e62eb38a7a3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22bca7d3-10b9-41c0-8c7b-2310826575b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14d3b274-fd3a-4b8b-8a6f-187fa71b5c8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22bca7d3-10b9-41c0-8c7b-2310826575b6",
                    "LayerId": "94696140-db95-40c1-adce-6ad72a86f534"
                }
            ]
        },
        {
            "id": "835dc56b-c5bb-43bd-8166-8384821beb2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "compositeImage": {
                "id": "2a50ab8a-b066-494f-9c61-fb855d30a385",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "835dc56b-c5bb-43bd-8166-8384821beb2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "977aadfa-a908-46b4-9794-8f2b3f95a71f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "835dc56b-c5bb-43bd-8166-8384821beb2e",
                    "LayerId": "94696140-db95-40c1-adce-6ad72a86f534"
                }
            ]
        },
        {
            "id": "c7462acc-ec8e-45a6-b1f5-a10bc37ce1be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "compositeImage": {
                "id": "824dd143-12ee-429d-a53e-d756e9574e57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7462acc-ec8e-45a6-b1f5-a10bc37ce1be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58b598ae-1f16-4c4e-b903-39655c3f02d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7462acc-ec8e-45a6-b1f5-a10bc37ce1be",
                    "LayerId": "94696140-db95-40c1-adce-6ad72a86f534"
                }
            ]
        },
        {
            "id": "072f4040-39e8-4d70-8e69-9fc6efcb5f34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "compositeImage": {
                "id": "987434ec-4a28-46aa-9bac-2f23af8faed3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "072f4040-39e8-4d70-8e69-9fc6efcb5f34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70d89095-331a-4cd1-b600-859f6b12809c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "072f4040-39e8-4d70-8e69-9fc6efcb5f34",
                    "LayerId": "94696140-db95-40c1-adce-6ad72a86f534"
                }
            ]
        },
        {
            "id": "8c2eb3a3-3cc6-42e4-99f9-18950de298cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "compositeImage": {
                "id": "532fb4f5-44cc-4832-9568-890a6b74fa26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c2eb3a3-3cc6-42e4-99f9-18950de298cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3ef161d-f03c-42d3-a67d-08b759f10b25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c2eb3a3-3cc6-42e4-99f9-18950de298cc",
                    "LayerId": "94696140-db95-40c1-adce-6ad72a86f534"
                }
            ]
        },
        {
            "id": "69527d3e-7b70-4d8c-92ca-3d246f24529d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "compositeImage": {
                "id": "912fc57e-cee2-437a-b2db-4a1636fb86de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69527d3e-7b70-4d8c-92ca-3d246f24529d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42cae26d-bcb6-45ed-acc3-e0ee0e73f4a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69527d3e-7b70-4d8c-92ca-3d246f24529d",
                    "LayerId": "94696140-db95-40c1-adce-6ad72a86f534"
                }
            ]
        },
        {
            "id": "c58ce2dc-09e0-457b-a6fa-aab9fbaecdda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "compositeImage": {
                "id": "639ca032-55da-43a7-b539-a9b687a51f7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c58ce2dc-09e0-457b-a6fa-aab9fbaecdda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9e40690-75b3-4381-9dcd-9cea7181e251",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c58ce2dc-09e0-457b-a6fa-aab9fbaecdda",
                    "LayerId": "94696140-db95-40c1-adce-6ad72a86f534"
                }
            ]
        },
        {
            "id": "085bdf1a-c305-4fd1-ad87-6d210b816842",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "compositeImage": {
                "id": "637a7c39-860b-4ece-a798-2acf631cb5ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "085bdf1a-c305-4fd1-ad87-6d210b816842",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa17fab0-5f0e-4f30-83bb-af083d5cb332",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "085bdf1a-c305-4fd1-ad87-6d210b816842",
                    "LayerId": "94696140-db95-40c1-adce-6ad72a86f534"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 148,
    "layers": [
        {
            "id": "94696140-db95-40c1-adce-6ad72a86f534",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b71b58a-cd7c-492d-bf52-2d2144f8b3f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 148,
    "xorig": 63,
    "yorig": 87
}