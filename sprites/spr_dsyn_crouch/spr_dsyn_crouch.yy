{
    "id": "8881fd28-71ed-424a-a975-c68ac942787d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dsyn_crouch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 50,
    "bbox_right": 76,
    "bbox_top": 52,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b74e7c5-97a9-4b74-ab82-69b996528850",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8881fd28-71ed-424a-a975-c68ac942787d",
            "compositeImage": {
                "id": "bb68303b-f819-43f0-951f-e15aa5af981e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b74e7c5-97a9-4b74-ab82-69b996528850",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60179983-6daa-49fe-af81-ccc215c3aa3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b74e7c5-97a9-4b74-ab82-69b996528850",
                    "LayerId": "57830b6a-a349-4d50-92f1-3f204de83249"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "57830b6a-a349-4d50-92f1-3f204de83249",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8881fd28-71ed-424a-a975-c68ac942787d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 76
}