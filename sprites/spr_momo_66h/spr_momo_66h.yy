{
    "id": "8d0e1140-3b64-4ec8-8638-48b307aa4bcc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_66h",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "07d49235-ec7c-42ac-b9e8-edd5578bbffe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d0e1140-3b64-4ec8-8638-48b307aa4bcc",
            "compositeImage": {
                "id": "296280b7-71c9-4148-b1cd-cb8feb938aaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07d49235-ec7c-42ac-b9e8-edd5578bbffe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5db6c36-3646-4eb6-b737-7d605407ff9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07d49235-ec7c-42ac-b9e8-edd5578bbffe",
                    "LayerId": "a31ff881-7ac4-4667-9dbd-1010b863e053"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a31ff881-7ac4-4667-9dbd-1010b863e053",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d0e1140-3b64-4ec8-8638-48b307aa4bcc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}