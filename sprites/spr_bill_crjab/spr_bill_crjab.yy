{
    "id": "9a87c4b2-4439-4d7a-9cf0-4f7585036306",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_crjab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 48,
    "bbox_right": 112,
    "bbox_top": 91,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2b5e3807-2a7a-4a13-b224-27af36db00a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a87c4b2-4439-4d7a-9cf0-4f7585036306",
            "compositeImage": {
                "id": "ee4ced8a-325c-44bc-9f6f-b3c18002ea5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b5e3807-2a7a-4a13-b224-27af36db00a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be19ebd7-cc71-439f-80b2-2a3ab3d34b6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b5e3807-2a7a-4a13-b224-27af36db00a9",
                    "LayerId": "58bf7fa5-e2ee-4842-9066-0bef4977561d"
                }
            ]
        },
        {
            "id": "5104380b-5083-4253-9080-fe538d46bfc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a87c4b2-4439-4d7a-9cf0-4f7585036306",
            "compositeImage": {
                "id": "05040e74-76cf-4ecf-9388-04be9a718a3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5104380b-5083-4253-9080-fe538d46bfc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6df0d913-3498-4787-b72b-ed5feae80dce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5104380b-5083-4253-9080-fe538d46bfc7",
                    "LayerId": "58bf7fa5-e2ee-4842-9066-0bef4977561d"
                }
            ]
        },
        {
            "id": "8bff01dd-1129-4615-92d7-94decae95b1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a87c4b2-4439-4d7a-9cf0-4f7585036306",
            "compositeImage": {
                "id": "e79f72ae-689f-4654-89c9-cbbacac51f9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bff01dd-1129-4615-92d7-94decae95b1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34b651a5-1707-4a80-af57-a3b8ecf7e54a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bff01dd-1129-4615-92d7-94decae95b1b",
                    "LayerId": "58bf7fa5-e2ee-4842-9066-0bef4977561d"
                }
            ]
        },
        {
            "id": "718303d8-1107-4cf2-a16c-5c7337a20f47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a87c4b2-4439-4d7a-9cf0-4f7585036306",
            "compositeImage": {
                "id": "4c127a6f-8d67-4ced-8be3-86b4a89c2c07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "718303d8-1107-4cf2-a16c-5c7337a20f47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc78ed41-3b47-4a20-83b5-f41b98abd735",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "718303d8-1107-4cf2-a16c-5c7337a20f47",
                    "LayerId": "58bf7fa5-e2ee-4842-9066-0bef4977561d"
                }
            ]
        },
        {
            "id": "37789090-91ef-4912-b5fe-ede5c6b3d1e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a87c4b2-4439-4d7a-9cf0-4f7585036306",
            "compositeImage": {
                "id": "6f3a347c-96e4-49f7-813b-6d5e820aad59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37789090-91ef-4912-b5fe-ede5c6b3d1e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f924ca8f-8ea0-4906-8be4-b706cef3e8b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37789090-91ef-4912-b5fe-ede5c6b3d1e8",
                    "LayerId": "58bf7fa5-e2ee-4842-9066-0bef4977561d"
                }
            ]
        },
        {
            "id": "a5c6479d-76f8-44ea-83b9-b44390a6e33d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a87c4b2-4439-4d7a-9cf0-4f7585036306",
            "compositeImage": {
                "id": "55e35372-9be2-4bf8-9864-54b638f35ab7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5c6479d-76f8-44ea-83b9-b44390a6e33d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57faf4aa-a9b7-4300-a28f-a15363f1735a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5c6479d-76f8-44ea-83b9-b44390a6e33d",
                    "LayerId": "58bf7fa5-e2ee-4842-9066-0bef4977561d"
                }
            ]
        },
        {
            "id": "79bbe784-9ecd-4923-b50d-fd7cf64a3fdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a87c4b2-4439-4d7a-9cf0-4f7585036306",
            "compositeImage": {
                "id": "306c0ebb-9b4b-4208-8f12-dc8bdebcbedc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79bbe784-9ecd-4923-b50d-fd7cf64a3fdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "209b7c35-ec17-47b4-8fa5-ab56899c66d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79bbe784-9ecd-4923-b50d-fd7cf64a3fdb",
                    "LayerId": "58bf7fa5-e2ee-4842-9066-0bef4977561d"
                }
            ]
        },
        {
            "id": "beeec148-0722-4d0d-a145-b927be37105f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a87c4b2-4439-4d7a-9cf0-4f7585036306",
            "compositeImage": {
                "id": "23853745-8271-49b8-827c-a6628ec4e844",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "beeec148-0722-4d0d-a145-b927be37105f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45bcaed4-847c-4c9e-b911-5cd2ae2cd275",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "beeec148-0722-4d0d-a145-b927be37105f",
                    "LayerId": "58bf7fa5-e2ee-4842-9066-0bef4977561d"
                }
            ]
        },
        {
            "id": "2404fe2e-4a77-4619-a11a-a816aa0f3c48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a87c4b2-4439-4d7a-9cf0-4f7585036306",
            "compositeImage": {
                "id": "d71c292d-b681-48f0-b061-b4b27f2fb53a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2404fe2e-4a77-4619-a11a-a816aa0f3c48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53129997-45e4-41bd-9200-a62798842510",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2404fe2e-4a77-4619-a11a-a816aa0f3c48",
                    "LayerId": "58bf7fa5-e2ee-4842-9066-0bef4977561d"
                }
            ]
        },
        {
            "id": "00202dd8-94c0-4be3-8e53-2d45115de59e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a87c4b2-4439-4d7a-9cf0-4f7585036306",
            "compositeImage": {
                "id": "d3edd9fb-9357-406b-a422-8a89c88626c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00202dd8-94c0-4be3-8e53-2d45115de59e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fc347d0-0d4b-4649-9a90-76b9aa57ff95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00202dd8-94c0-4be3-8e53-2d45115de59e",
                    "LayerId": "58bf7fa5-e2ee-4842-9066-0bef4977561d"
                }
            ]
        },
        {
            "id": "73cb110a-6364-4bf2-992f-dd9126d828e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a87c4b2-4439-4d7a-9cf0-4f7585036306",
            "compositeImage": {
                "id": "1748b5af-9a9b-483b-b1fc-cef4682b8885",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73cb110a-6364-4bf2-992f-dd9126d828e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b91340ef-bcca-4f62-8e48-759e47591675",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73cb110a-6364-4bf2-992f-dd9126d828e3",
                    "LayerId": "58bf7fa5-e2ee-4842-9066-0bef4977561d"
                }
            ]
        },
        {
            "id": "eb725654-a934-45e4-8baa-a733956527d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a87c4b2-4439-4d7a-9cf0-4f7585036306",
            "compositeImage": {
                "id": "caea1377-e866-46bf-a8eb-56be66980ffd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb725654-a934-45e4-8baa-a733956527d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "154ae9b7-701c-4855-bdc0-c772a85afb9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb725654-a934-45e4-8baa-a733956527d2",
                    "LayerId": "58bf7fa5-e2ee-4842-9066-0bef4977561d"
                }
            ]
        },
        {
            "id": "c4992d96-9166-47ef-b7bd-e1e223511933",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a87c4b2-4439-4d7a-9cf0-4f7585036306",
            "compositeImage": {
                "id": "579fb233-16f8-48ee-8c84-a84123ab093f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4992d96-9166-47ef-b7bd-e1e223511933",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e3d47a7-5bf2-464e-ba8b-fb5c3efc165e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4992d96-9166-47ef-b7bd-e1e223511933",
                    "LayerId": "58bf7fa5-e2ee-4842-9066-0bef4977561d"
                }
            ]
        },
        {
            "id": "cb1856b4-caae-44cd-b7d8-2f974b9e86bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a87c4b2-4439-4d7a-9cf0-4f7585036306",
            "compositeImage": {
                "id": "dd53de33-e89e-4eea-a2ca-5e9525216214",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb1856b4-caae-44cd-b7d8-2f974b9e86bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21e57af5-d5aa-4fd1-84db-3d124b56c4ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb1856b4-caae-44cd-b7d8-2f974b9e86bd",
                    "LayerId": "58bf7fa5-e2ee-4842-9066-0bef4977561d"
                }
            ]
        },
        {
            "id": "23fe4a49-75bf-45d3-be56-2bdc7248cdeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a87c4b2-4439-4d7a-9cf0-4f7585036306",
            "compositeImage": {
                "id": "9fe59fec-7091-4eb5-a5f2-dc71fd407c80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23fe4a49-75bf-45d3-be56-2bdc7248cdeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a905f36d-c667-4840-9550-dc29c385ec50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23fe4a49-75bf-45d3-be56-2bdc7248cdeb",
                    "LayerId": "58bf7fa5-e2ee-4842-9066-0bef4977561d"
                }
            ]
        },
        {
            "id": "1dca36f0-5ab4-43e0-86d8-28c1c8621825",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a87c4b2-4439-4d7a-9cf0-4f7585036306",
            "compositeImage": {
                "id": "eeae6ca6-8717-4b66-b8af-0cabd76d0ea3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dca36f0-5ab4-43e0-86d8-28c1c8621825",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dea5ce33-0b02-4e61-8caa-6262a36d452a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dca36f0-5ab4-43e0-86d8-28c1c8621825",
                    "LayerId": "58bf7fa5-e2ee-4842-9066-0bef4977561d"
                }
            ]
        },
        {
            "id": "1d980770-12f4-40e0-805a-be19d0fcd8b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a87c4b2-4439-4d7a-9cf0-4f7585036306",
            "compositeImage": {
                "id": "8508c519-aea7-44f5-a2d4-01b93f50acd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d980770-12f4-40e0-805a-be19d0fcd8b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36592ac2-90ef-4327-9352-4a407620c038",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d980770-12f4-40e0-805a-be19d0fcd8b3",
                    "LayerId": "58bf7fa5-e2ee-4842-9066-0bef4977561d"
                }
            ]
        },
        {
            "id": "01c2abe2-95ea-4a79-a34c-07071dc8b315",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a87c4b2-4439-4d7a-9cf0-4f7585036306",
            "compositeImage": {
                "id": "4bbb72db-641f-4a6a-9b54-414d2fa14a90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01c2abe2-95ea-4a79-a34c-07071dc8b315",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ed3233e-a201-4648-a428-8a450dbc4127",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01c2abe2-95ea-4a79-a34c-07071dc8b315",
                    "LayerId": "58bf7fa5-e2ee-4842-9066-0bef4977561d"
                }
            ]
        },
        {
            "id": "58afa0e8-9dc2-48fa-9e3c-73da5f8662fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a87c4b2-4439-4d7a-9cf0-4f7585036306",
            "compositeImage": {
                "id": "a0a94fdc-bd49-4ac8-b01f-b811dd9bfd8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58afa0e8-9dc2-48fa-9e3c-73da5f8662fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "181e8774-8c6b-41f5-aba5-4928bb44ee04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58afa0e8-9dc2-48fa-9e3c-73da5f8662fa",
                    "LayerId": "58bf7fa5-e2ee-4842-9066-0bef4977561d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "58bf7fa5-e2ee-4842-9066-0bef4977561d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a87c4b2-4439-4d7a-9cf0-4f7585036306",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}