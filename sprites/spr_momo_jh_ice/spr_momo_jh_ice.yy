{
    "id": "d3575997-2882-4a6d-b3df-88bbdfb499eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_jh_ice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "44954eaa-bfbc-4b4b-ad6a-9de15f152a11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3575997-2882-4a6d-b3df-88bbdfb499eb",
            "compositeImage": {
                "id": "7eff6fd5-89e5-4ab3-9608-8b892715204f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44954eaa-bfbc-4b4b-ad6a-9de15f152a11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c742a72-9a92-417e-b571-b44a797fbcaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44954eaa-bfbc-4b4b-ad6a-9de15f152a11",
                    "LayerId": "76de8072-8212-4824-8698-141fb60461cd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "76de8072-8212-4824-8698-141fb60461cd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d3575997-2882-4a6d-b3df-88bbdfb499eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}