{
    "id": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_chdh_ice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 125,
    "bbox_left": 24,
    "bbox_right": 154,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f7e08004-fadf-4f5f-84d6-84c2d864d8e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "a729f0ca-6f28-4a20-abe9-b65951893a51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7e08004-fadf-4f5f-84d6-84c2d864d8e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b9ad78c-1417-4d37-b9b7-98acc03cf6f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7e08004-fadf-4f5f-84d6-84c2d864d8e3",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "25887a75-a01f-4127-aee1-4df65cb7bda7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "74932551-a6da-4893-9715-b8c06e62d60b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25887a75-a01f-4127-aee1-4df65cb7bda7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd45da41-feb9-42b8-a49e-780bd6518105",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25887a75-a01f-4127-aee1-4df65cb7bda7",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "523697ad-dfc4-4fcc-bf1c-7f28620de7bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "a5c8c1f2-d999-45d1-a9cf-45c32c0a4404",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "523697ad-dfc4-4fcc-bf1c-7f28620de7bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72cbe2cc-73c1-46e6-998a-32f507eb0157",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "523697ad-dfc4-4fcc-bf1c-7f28620de7bc",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "834370c2-bea7-4f99-ba1d-4a13763276f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "16915118-fde4-4b86-9469-fd91651526c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "834370c2-bea7-4f99-ba1d-4a13763276f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0541a445-95f0-418f-85c9-05e01a820f68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "834370c2-bea7-4f99-ba1d-4a13763276f1",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "9ae5eb38-7f75-4c01-b8b6-74d6d307efc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "31ccb8a6-474a-473d-9d51-b00fea93ccb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ae5eb38-7f75-4c01-b8b6-74d6d307efc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56c29c75-9f50-45fa-ad46-606dc7fead6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ae5eb38-7f75-4c01-b8b6-74d6d307efc5",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "8f36ed76-d41f-4e02-b8eb-5518722ccb51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "422b5dfd-25f5-4c29-9f05-30ecfbd4aa75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f36ed76-d41f-4e02-b8eb-5518722ccb51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55f35aa3-a283-462c-808e-ca5e45ae9de9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f36ed76-d41f-4e02-b8eb-5518722ccb51",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "0c380d01-fd0f-40df-b31f-0260c99a9a8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "cf1a43fc-00ba-4a5d-9110-3b8c68f8dc35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c380d01-fd0f-40df-b31f-0260c99a9a8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2326d8e6-afd3-499a-b511-7d0a11b7daa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c380d01-fd0f-40df-b31f-0260c99a9a8e",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "54c3b1f0-b455-4f63-b0b3-30bdc22c281a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "b63473d9-aa2b-46e8-8c7e-741cc1c5f5b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54c3b1f0-b455-4f63-b0b3-30bdc22c281a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a2b1289-6397-47f9-8fbc-06d441bde71f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54c3b1f0-b455-4f63-b0b3-30bdc22c281a",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "20a6bab9-e1d5-4f54-895c-44cf8eef51ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "05582b40-fac7-4c09-a0a1-9b16456d45bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20a6bab9-e1d5-4f54-895c-44cf8eef51ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be52dea2-d339-4aef-aa21-e5783c972042",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20a6bab9-e1d5-4f54-895c-44cf8eef51ec",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "6a1f0ef0-e188-4c27-aa3b-9b70fd397628",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "8fe5e91f-8cfe-4143-aff7-e8e414b7ad6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a1f0ef0-e188-4c27-aa3b-9b70fd397628",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d79d3043-e642-41fb-a4c1-4842ef2cf26a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a1f0ef0-e188-4c27-aa3b-9b70fd397628",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "6cbf2581-26e0-4674-be8a-c311bfef81e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "b8300c98-a220-4d78-9baf-6df661cf923f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cbf2581-26e0-4674-be8a-c311bfef81e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51e5c2b2-de2e-4c4d-adf4-1cd530d9222c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cbf2581-26e0-4674-be8a-c311bfef81e5",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "07cd673e-5622-49f7-8eef-1b00aa8ae02e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "7f4b80b3-4475-41f6-9bde-23a8d08f6e5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07cd673e-5622-49f7-8eef-1b00aa8ae02e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38791341-ce38-4da6-9ad4-1538a5a51188",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07cd673e-5622-49f7-8eef-1b00aa8ae02e",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "fc1cb8c1-f61d-4841-b7d5-b38a8a4bdc7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "e2da1d55-1272-445a-8478-55863daced2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc1cb8c1-f61d-4841-b7d5-b38a8a4bdc7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d0e87cb-bae7-441c-8205-38c1db29b267",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc1cb8c1-f61d-4841-b7d5-b38a8a4bdc7d",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "448c487e-1e99-4446-9518-d34f01253440",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "ca4ac291-ff23-4c5b-a62f-b985ad3be582",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "448c487e-1e99-4446-9518-d34f01253440",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52eb42a5-19af-47b2-bef9-aa569b451291",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "448c487e-1e99-4446-9518-d34f01253440",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "dcc55969-30ae-4757-a38d-3c806779ddbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "a3056e49-f1fd-49dd-9afd-0706732bf849",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcc55969-30ae-4757-a38d-3c806779ddbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edac9402-dc76-4ea9-86cd-d70bfad332f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcc55969-30ae-4757-a38d-3c806779ddbd",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "2c7fad5b-2c30-4946-89b7-6350bae5a390",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "770899c7-2cdb-4e94-a805-6f6daa325be4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c7fad5b-2c30-4946-89b7-6350bae5a390",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38ad7abf-df08-40f3-95e7-9297465f0046",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c7fad5b-2c30-4946-89b7-6350bae5a390",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "f2f5e419-bd2c-4ef6-bdcb-b023ac57cab5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "0d6638f4-5307-45ef-aacb-163682fb5a82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2f5e419-bd2c-4ef6-bdcb-b023ac57cab5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "142973ac-0f2e-49b0-b11d-abeeed98d90c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2f5e419-bd2c-4ef6-bdcb-b023ac57cab5",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "ca1e777c-dc1c-4d3f-b4bd-3c827fef80a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "fb1b76e0-6dd6-497c-8214-f8400c1031ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca1e777c-dc1c-4d3f-b4bd-3c827fef80a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41bdfcb4-ac6c-448b-b79c-064bad162b15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca1e777c-dc1c-4d3f-b4bd-3c827fef80a4",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "fe75f57c-cd04-40ed-b452-cbc151e6fc75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "f3265902-d2f7-4110-97ef-a047fac9f933",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe75f57c-cd04-40ed-b452-cbc151e6fc75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c16f28c5-8ece-4606-a1bd-ed4bc5fe864f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe75f57c-cd04-40ed-b452-cbc151e6fc75",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "11502035-17ad-4ca4-b164-5404b455cd95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "c931afcd-38a0-4c70-ad7e-7e4558935035",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11502035-17ad-4ca4-b164-5404b455cd95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33d5a160-188d-4c2a-855c-28776d7456e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11502035-17ad-4ca4-b164-5404b455cd95",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "06d1d1cf-4bec-422e-8339-f87aef0234de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "e8d5476f-ba11-4fad-879f-690fa9deef37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06d1d1cf-4bec-422e-8339-f87aef0234de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b4c4d8b-6741-45f5-bec5-8eed4c66e9b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06d1d1cf-4bec-422e-8339-f87aef0234de",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "d666b470-a152-4639-bdf0-64d806377b8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "93a4f3cc-7086-475e-9255-ec61806b6738",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d666b470-a152-4639-bdf0-64d806377b8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0df9bc0c-48a9-4569-ace4-a99747f403a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d666b470-a152-4639-bdf0-64d806377b8c",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "d02aca67-38dd-414c-a7a4-0b1e3176f505",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "8e8e315f-b8d2-4b13-a22d-a2bf47057dcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d02aca67-38dd-414c-a7a4-0b1e3176f505",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df07025d-5968-4606-a451-8acab1329512",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d02aca67-38dd-414c-a7a4-0b1e3176f505",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "5e4172e4-eb25-41f9-b07c-6c30c65953c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "2e8776d1-a552-4a30-b58a-dc88b38c3191",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e4172e4-eb25-41f9-b07c-6c30c65953c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38e6451a-f632-4c87-9c9b-9f1dcab19f7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e4172e4-eb25-41f9-b07c-6c30c65953c1",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "8ff1e8ae-00f8-46ff-a4e7-bcbb97a3d7df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "62b389ed-1e3e-4e8d-8ba9-b1e892e82db1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ff1e8ae-00f8-46ff-a4e7-bcbb97a3d7df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef793b23-96c7-4a61-a022-dba260488bce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ff1e8ae-00f8-46ff-a4e7-bcbb97a3d7df",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "8c5f09c5-1ac1-469d-bcb4-4a5c25f82e4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "478dfaa1-8b0e-4a51-85b4-68f19c6e4f5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c5f09c5-1ac1-469d-bcb4-4a5c25f82e4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fc73de5-be89-4b1c-a8e2-fcb107670d75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c5f09c5-1ac1-469d-bcb4-4a5c25f82e4e",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "3a0656ea-010a-4e55-b3d6-d887bbf201f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "0b26dfb0-c267-4763-abf9-34f236c01c0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a0656ea-010a-4e55-b3d6-d887bbf201f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "571568ea-1431-4edb-b862-765b9562c3a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a0656ea-010a-4e55-b3d6-d887bbf201f0",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "eb3ea0dc-8bc3-4a43-a4ce-20b7e1d8ee7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "14e27943-bb8f-40cd-825f-cb412fb4301d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb3ea0dc-8bc3-4a43-a4ce-20b7e1d8ee7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a151cd7-914c-49df-b61e-5cf55d0442f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb3ea0dc-8bc3-4a43-a4ce-20b7e1d8ee7d",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "aaab34d9-4af4-4433-9279-60ba395e5506",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "2eb1cbfa-681c-4d99-b8da-43bfeb138838",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aaab34d9-4af4-4433-9279-60ba395e5506",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a650d64a-6e06-46b4-90d7-a6c3e604e144",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aaab34d9-4af4-4433-9279-60ba395e5506",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "c5124fd0-27f3-4eab-a3e6-3125770b400d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "de5723e2-e0fe-45da-8073-c823c75f232f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5124fd0-27f3-4eab-a3e6-3125770b400d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "980961e4-6a5d-42e9-a9e2-631f969a4f46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5124fd0-27f3-4eab-a3e6-3125770b400d",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "ed3db2e3-1fb4-458a-9a72-b0ae3abe0f04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "c4d0be57-667a-41b2-8ef1-352fb8243fc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed3db2e3-1fb4-458a-9a72-b0ae3abe0f04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a240a04-5d60-48e0-8024-0a9585df5f38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed3db2e3-1fb4-458a-9a72-b0ae3abe0f04",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "f8f8def1-061a-4601-a458-ce690ba3f14b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "4c077519-42d7-4faf-aae2-9d5ad1c3c0cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8f8def1-061a-4601-a458-ce690ba3f14b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5e051ba-8956-4212-b6e1-7c3f4e33f00d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8f8def1-061a-4601-a458-ce690ba3f14b",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "8088867c-7398-48d3-9fe9-cfb0c7df9ba1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "c1602f02-5db5-45cc-8907-881471cc8bf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8088867c-7398-48d3-9fe9-cfb0c7df9ba1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9999b525-99c9-4992-aa2c-f000895d6438",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8088867c-7398-48d3-9fe9-cfb0c7df9ba1",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "d74418b4-da74-47fa-a091-03ab0fc615e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "ad8be7e7-6454-486d-88cf-475d07c48db2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d74418b4-da74-47fa-a091-03ab0fc615e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a16f5e8-7c99-4223-8140-eaef12945561",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d74418b4-da74-47fa-a091-03ab0fc615e8",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "c427acde-0f62-4d49-9c1a-34f3af8ba118",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "c092711c-ee80-49b5-b010-77a6fd85d42e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c427acde-0f62-4d49-9c1a-34f3af8ba118",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e067e22f-0eb9-4e9f-a061-aa405109eebb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c427acde-0f62-4d49-9c1a-34f3af8ba118",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "87bdfe4e-6977-41bf-a058-2b15b93f6ba7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "934a7d59-bc42-4b36-9b32-09a691da805d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87bdfe4e-6977-41bf-a058-2b15b93f6ba7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dc75b8f-5d8d-4319-8085-a5ed953c8db6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87bdfe4e-6977-41bf-a058-2b15b93f6ba7",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "a63146be-155c-45de-b362-2339d554c116",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "d87981d0-542d-4650-bbb1-f8a8567d4e43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a63146be-155c-45de-b362-2339d554c116",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4e3d187-e45b-4dc3-91c7-654d6c08daad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a63146be-155c-45de-b362-2339d554c116",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "4da94b50-7401-4a19-9c55-24e792191153",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "200ffcce-8d27-497f-a0f5-1acf21e42419",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4da94b50-7401-4a19-9c55-24e792191153",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "016221eb-91d0-48ff-bafa-31addda452e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4da94b50-7401-4a19-9c55-24e792191153",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "592ef521-18f5-4abc-a2cf-6da01ca4d24b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "b9a5c548-af96-4be3-9105-dcf588c5b754",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "592ef521-18f5-4abc-a2cf-6da01ca4d24b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36b79e95-7211-43ed-b154-4ef895857eb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "592ef521-18f5-4abc-a2cf-6da01ca4d24b",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "a89c531c-276d-4a3b-a1b6-7d230de3b1fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "ccf74c3a-45fb-4f7e-bbae-49ae4eaa8908",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a89c531c-276d-4a3b-a1b6-7d230de3b1fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "472001ef-f9be-47da-add0-a34dc491e29c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a89c531c-276d-4a3b-a1b6-7d230de3b1fa",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "c3699c39-b636-404a-9229-2637b4d9157b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "34877d7b-501c-4495-8768-c123568b3bc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3699c39-b636-404a-9229-2637b4d9157b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b1591e8-467c-4324-88bb-797b257f5bf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3699c39-b636-404a-9229-2637b4d9157b",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "94679eb9-7a5f-4542-b265-743ac0f2cc03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "a1382933-97e2-49f3-bfc9-85bfdf3a06d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94679eb9-7a5f-4542-b265-743ac0f2cc03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc49fbdb-76dc-463c-8bbb-4e5e1be22afa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94679eb9-7a5f-4542-b265-743ac0f2cc03",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "2658ea72-17db-4828-aa0b-ea5315ae41fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "728dd1cb-7ca9-4051-ae05-0ec3b3032359",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2658ea72-17db-4828-aa0b-ea5315ae41fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63950bcc-0dca-4846-a258-923f4019b875",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2658ea72-17db-4828-aa0b-ea5315ae41fb",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "39961b1e-2da5-479d-b990-26fa4d221230",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "0f493348-cf74-483d-9155-47b7a3ed6b5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39961b1e-2da5-479d-b990-26fa4d221230",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05772580-3b8c-4ca0-8b3f-a74088e7cd98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39961b1e-2da5-479d-b990-26fa4d221230",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "6f75dcad-fef1-4b5f-aa94-2303ade36218",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "fa7c6cd1-668b-4787-ba81-c1c071e1d14f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f75dcad-fef1-4b5f-aa94-2303ade36218",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54876308-4a2f-432e-a743-c391034a2561",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f75dcad-fef1-4b5f-aa94-2303ade36218",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "e89dd354-b681-4a19-8703-18b63e5003a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "f32a7d3e-798a-4706-8995-d19c63c5a305",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e89dd354-b681-4a19-8703-18b63e5003a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd38db85-7fd0-4718-8ab3-60af05d096e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e89dd354-b681-4a19-8703-18b63e5003a5",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "a8de1316-f00d-4373-af5d-d579750508a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "9ec97036-35d5-4f82-8b83-19305c19cef3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8de1316-f00d-4373-af5d-d579750508a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62651995-7649-4261-8bf6-45856c9207e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8de1316-f00d-4373-af5d-d579750508a8",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "be29e9cb-503a-4394-b0aa-d032b4f52138",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "e7208233-88d7-4c7f-beee-f2baa7a148e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be29e9cb-503a-4394-b0aa-d032b4f52138",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "869df05a-0ff7-4e74-96cc-02ddacb53170",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be29e9cb-503a-4394-b0aa-d032b4f52138",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "0966b44b-7596-4644-a444-a3ae1121832f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "86c73c9a-a9bb-421a-abf1-f8fb5cb1b950",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0966b44b-7596-4644-a444-a3ae1121832f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb12a46a-5f10-40c0-b1d3-0728556f350b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0966b44b-7596-4644-a444-a3ae1121832f",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "1b804a21-dc62-4f05-97ac-c1157a82667d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "98907a2a-90aa-4f48-92da-61d25824ad7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b804a21-dc62-4f05-97ac-c1157a82667d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b59dab0-04ff-48bc-8381-ec2ab717f53c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b804a21-dc62-4f05-97ac-c1157a82667d",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "871ccaa8-2409-4adc-bb59-52569b28cb1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "72aadbfc-a722-427f-b3d6-d96abee59004",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "871ccaa8-2409-4adc-bb59-52569b28cb1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fcf617b-305c-460d-acca-103767ff7d7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "871ccaa8-2409-4adc-bb59-52569b28cb1e",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "a02f1f91-9ef8-44fb-81d6-eaad750267c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "9ab3586b-fe73-4b5f-84d8-c871f4fdfec0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a02f1f91-9ef8-44fb-81d6-eaad750267c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a351c844-712e-4239-94d0-dbaaf6b0ad87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a02f1f91-9ef8-44fb-81d6-eaad750267c9",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "fc28e4e7-f435-4255-a693-aa43956330e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "20846867-d353-4245-b250-6f2f1586ab17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc28e4e7-f435-4255-a693-aa43956330e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07256828-8281-46e4-9751-dc3cc3a9e471",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc28e4e7-f435-4255-a693-aa43956330e3",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "25c651dd-f448-4748-a4b8-d99f38179d9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "ab31b888-af94-481a-b374-1f062f2a53ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25c651dd-f448-4748-a4b8-d99f38179d9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acc6dc00-4f07-4f6d-b29f-9424951e273b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25c651dd-f448-4748-a4b8-d99f38179d9b",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "d9942215-0625-42b1-a6a9-bd99049e60f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "6e97c706-ca2b-48ee-b6a8-119cbb4368bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9942215-0625-42b1-a6a9-bd99049e60f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9d3c425-10b2-42fa-9707-edea5dd30d62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9942215-0625-42b1-a6a9-bd99049e60f8",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "17f943e3-0c7f-46c8-bc8b-598fcc0b7b80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "dd0f39af-f888-40b3-acdf-9f10725f8d9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17f943e3-0c7f-46c8-bc8b-598fcc0b7b80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a77963a-50b1-4d00-830c-e6af473fb606",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17f943e3-0c7f-46c8-bc8b-598fcc0b7b80",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "cba43cee-97f7-4d9f-86a9-4a42365bf251",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "8e2e578d-8d44-4455-86c7-a00c9fa32f47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cba43cee-97f7-4d9f-86a9-4a42365bf251",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9eec3347-7c27-4db7-85ca-0821128d4307",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cba43cee-97f7-4d9f-86a9-4a42365bf251",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "673d9c24-aaa6-4538-bf3f-30e0116c1c33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "1ee03791-e003-4bd3-818b-a45ef34aa346",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "673d9c24-aaa6-4538-bf3f-30e0116c1c33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95f30875-8b54-48eb-9c6c-450c79a401bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "673d9c24-aaa6-4538-bf3f-30e0116c1c33",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "f7270ad6-28f2-4de3-8c09-5c49f098d4d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "54bc9bc0-b03f-4e62-bf7c-9cb3ef3d0311",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7270ad6-28f2-4de3-8c09-5c49f098d4d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d175f6a5-7ae9-4f8a-bd7f-5bd2a62ab6c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7270ad6-28f2-4de3-8c09-5c49f098d4d0",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        },
        {
            "id": "ed9fc785-7ee0-4276-ba4d-7a08b31bbe0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "compositeImage": {
                "id": "6fa84029-e3bb-42d7-8481-757556111ee7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed9fc785-7ee0-4276-ba4d-7a08b31bbe0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b212d1d4-a69e-459a-bb81-da2d926dd94d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed9fc785-7ee0-4276-ba4d-7a08b31bbe0e",
                    "LayerId": "370c332a-f740-4131-995e-392532005f4f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 148,
    "layers": [
        {
            "id": "370c332a-f740-4131-995e-392532005f4f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5246179b-7e5d-4193-96ca-1cd0e98f92c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 168,
    "xorig": 63,
    "yorig": 87
}