{
    "id": "222c1698-940e-4f77-ab42-caf2ba3e185a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "JUSTICE",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "248e1c1b-25bd-411a-bc65-85dea4294070",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "222c1698-940e-4f77-ab42-caf2ba3e185a",
            "compositeImage": {
                "id": "ded44d57-9fd1-4a62-b9fa-93134b9e92d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "248e1c1b-25bd-411a-bc65-85dea4294070",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d790c61-eab2-4ea4-ae44-87289aa2abc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "248e1c1b-25bd-411a-bc65-85dea4294070",
                    "LayerId": "dfbdb2f8-e13f-46a4-94e1-bde3613af47e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "dfbdb2f8-e13f-46a4-94e1-bde3613af47e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "222c1698-940e-4f77-ab42-caf2ba3e185a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 17
}