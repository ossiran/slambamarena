{
    "id": "2d340546-231d-4fe5-abbb-9a843e30153a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_myu_attack_ujab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 11,
    "bbox_right": 43,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6fa95b94-c375-45de-9baa-b93a9681baa8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "039ee78a-8ef0-4bf4-962e-69f7039cf194",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fa95b94-c375-45de-9baa-b93a9681baa8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0695d7aa-050b-401e-8f03-cc37d8164f70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fa95b94-c375-45de-9baa-b93a9681baa8",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "ded2f393-514e-4772-aefc-a9bd0e011fed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "f66e1ce2-8429-4ebe-9a8c-6e58a0c45d1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ded2f393-514e-4772-aefc-a9bd0e011fed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e84b0d69-6f21-4380-8915-2b6914cc1e48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ded2f393-514e-4772-aefc-a9bd0e011fed",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "b8686b8d-3403-4efa-ae6a-3163c0983ac5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "8435a455-df86-4bbb-9aeb-5c2f268d7bf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8686b8d-3403-4efa-ae6a-3163c0983ac5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c9b1148-6405-46dc-b34b-124e5b81d15e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8686b8d-3403-4efa-ae6a-3163c0983ac5",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "d9eb956d-4a5d-401e-91b4-b0fd88b169d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "9a58a366-c374-4e28-b9a1-c431ebe2a376",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9eb956d-4a5d-401e-91b4-b0fd88b169d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e13eadf1-e317-4e2e-a3b0-3a7afc697d03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9eb956d-4a5d-401e-91b4-b0fd88b169d0",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "6336051c-721e-4133-a66d-20171a9588d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "ebf96323-cf93-415b-b7c1-3bf9316f4bc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6336051c-721e-4133-a66d-20171a9588d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43ce046d-afbf-4778-89f3-3a16bf0a6c49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6336051c-721e-4133-a66d-20171a9588d4",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "89059df7-0c21-4863-b8b0-f27747fb1d69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "a148e483-3882-416e-9679-aefc998dcc47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89059df7-0c21-4863-b8b0-f27747fb1d69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53dfb1f7-caa8-4620-801e-0db1627267bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89059df7-0c21-4863-b8b0-f27747fb1d69",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "7747baad-4c92-48a9-8c58-657f90552d31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "fc75ecef-35c1-40ad-b5af-93fdcc6e030b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7747baad-4c92-48a9-8c58-657f90552d31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52efeed3-c26c-4916-8835-d7107ba9ef24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7747baad-4c92-48a9-8c58-657f90552d31",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "db999775-0ada-49fc-8b17-16780c91fe1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "c3c91a89-1102-4cb3-a49f-623645409093",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db999775-0ada-49fc-8b17-16780c91fe1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "220210a9-3f2c-438f-833a-8e23689caa65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db999775-0ada-49fc-8b17-16780c91fe1b",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "36fc66e6-34ec-4057-b8bf-a229e270d2e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "222744bd-fc46-4e99-8dc0-5d5bd51c6b16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36fc66e6-34ec-4057-b8bf-a229e270d2e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "886f2c01-44d6-4956-b766-70a1224e122e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36fc66e6-34ec-4057-b8bf-a229e270d2e4",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "1187041e-c8e4-4a64-b7f8-eca1bf898dce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "e3e5e31d-60ed-4053-b9f0-f7d3c625584b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1187041e-c8e4-4a64-b7f8-eca1bf898dce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc0e91c2-5824-420d-8c0a-07628f280299",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1187041e-c8e4-4a64-b7f8-eca1bf898dce",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "7e3a155b-2489-40b1-9855-46ad6254e023",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "fbc457b1-dc58-4dc2-b66d-d5b7085c58c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e3a155b-2489-40b1-9855-46ad6254e023",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "417978d0-c4d3-4f2f-9f39-9b3aaa6228c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e3a155b-2489-40b1-9855-46ad6254e023",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "6be7ab53-4108-47d4-83b9-d4cd759e610f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "e8d81939-3d45-4304-b0f9-8bc19974df5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6be7ab53-4108-47d4-83b9-d4cd759e610f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b033ed2c-86f7-46a5-824f-e82d9a228c70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6be7ab53-4108-47d4-83b9-d4cd759e610f",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "bd57f5e8-c0b8-4310-9c30-582bbd55a5e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "e4a15f35-c234-4838-a4be-9b3213d336e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd57f5e8-c0b8-4310-9c30-582bbd55a5e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4da59be1-ca6b-493e-9ee7-6b8418f55e7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd57f5e8-c0b8-4310-9c30-582bbd55a5e2",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "df7cef2c-3fd3-4b92-a561-7ecb23106dd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "6902db98-7f19-406d-888f-8aaafa2c5bd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df7cef2c-3fd3-4b92-a561-7ecb23106dd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2ed2ba3-ed91-491b-8ac8-8f0d62269ba6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df7cef2c-3fd3-4b92-a561-7ecb23106dd4",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "92a6b764-ca28-4796-849e-f60cbf6bba47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "ea9b63f0-cba0-47e8-89f0-1fc9860b627c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92a6b764-ca28-4796-849e-f60cbf6bba47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c3add2d-41d5-41ec-82d8-8572dea4a536",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92a6b764-ca28-4796-849e-f60cbf6bba47",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "5864174d-a939-44ed-beaf-f6f0622b8e92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "76c29df7-bd15-45e7-a90e-f49992aa23a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5864174d-a939-44ed-beaf-f6f0622b8e92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c99fa26-842b-49db-baa0-bcdc0a82fd57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5864174d-a939-44ed-beaf-f6f0622b8e92",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "b5693088-a3fc-4ffb-b0d3-31500ad24320",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "e848fda1-93e2-4b3c-bc76-f506b3c97932",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5693088-a3fc-4ffb-b0d3-31500ad24320",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9664e31f-3a8d-4796-9bda-79aad8c3db37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5693088-a3fc-4ffb-b0d3-31500ad24320",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "40fb0bcc-a0b0-4ba3-90bf-334d284b778e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "f139aae5-8380-4505-9597-9f2d2620c890",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40fb0bcc-a0b0-4ba3-90bf-334d284b778e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4d50bcb-5507-4198-bfa9-b9d318e09048",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40fb0bcc-a0b0-4ba3-90bf-334d284b778e",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "9022f762-ccab-4dcc-be1e-cba9ae9acc6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "9ef75d62-36c8-4023-af8b-fca7b386b70c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9022f762-ccab-4dcc-be1e-cba9ae9acc6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9aae38c7-b4db-42e9-a2c4-ffdd5b6f6f30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9022f762-ccab-4dcc-be1e-cba9ae9acc6d",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "4a10a3e5-9120-4c90-b281-ef68fb5ac5ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "a2043235-7b4d-4d26-98bf-e8b87896053c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a10a3e5-9120-4c90-b281-ef68fb5ac5ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23a244ff-f90a-4388-846a-7775097b100f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a10a3e5-9120-4c90-b281-ef68fb5ac5ee",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "ca782fc7-4653-4de0-9148-d5342e464fee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "7705eeba-d09c-4dff-8b39-1a52ff6ace38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca782fc7-4653-4de0-9148-d5342e464fee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5b39b7d-0582-431d-b3b3-8723e82a8128",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca782fc7-4653-4de0-9148-d5342e464fee",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "8ce82e2a-28b9-4b07-bb4b-005dc7d587ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "6109223c-6dc4-47b5-8751-8a2a38a9093a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ce82e2a-28b9-4b07-bb4b-005dc7d587ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "705673a1-c50a-4b8c-98e9-52b3e4574df9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ce82e2a-28b9-4b07-bb4b-005dc7d587ce",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "09c3d80b-804a-497c-80a3-18359399716e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "18552df6-3725-47fc-a064-1419147bfaaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09c3d80b-804a-497c-80a3-18359399716e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd74c117-aa06-4e5a-9454-8106996bd276",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09c3d80b-804a-497c-80a3-18359399716e",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "c974d366-728b-4242-87a0-dd2e95a22962",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "a3cb16f8-16ec-4990-8a4d-239ea0bb5718",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c974d366-728b-4242-87a0-dd2e95a22962",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fc13e8e-5c8b-4091-8511-0ebab14af636",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c974d366-728b-4242-87a0-dd2e95a22962",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "0de3ee1c-a8cf-4696-ae82-f18232f451d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "3c4bcf5a-76d5-48ea-9a6f-aa5b9cacb48b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0de3ee1c-a8cf-4696-ae82-f18232f451d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49a88d86-1db3-4ed0-916c-26b3ff335779",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0de3ee1c-a8cf-4696-ae82-f18232f451d1",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "1c058b0a-e95f-4000-a9ba-1788d2e85729",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "a29ca914-0bde-4457-9147-62fd83763392",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c058b0a-e95f-4000-a9ba-1788d2e85729",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e95e5fa-f541-4f84-a9f5-e87a48e7f1a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c058b0a-e95f-4000-a9ba-1788d2e85729",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "aed3b141-236c-4326-ac9d-e5eb69d93512",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "27bfb8a9-4f0f-4110-9af7-256f4ca48de5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aed3b141-236c-4326-ac9d-e5eb69d93512",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c2302e6-c680-4f3b-8067-f54675c047ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aed3b141-236c-4326-ac9d-e5eb69d93512",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "346c0906-4d8f-496c-9747-d3d9f5cd7a69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "538015cd-85d0-4779-b4f6-0a2c1e61716d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "346c0906-4d8f-496c-9747-d3d9f5cd7a69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0e24a5e-5137-43e9-b98d-924995846827",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "346c0906-4d8f-496c-9747-d3d9f5cd7a69",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "25c0c987-4ed2-4dbf-92d1-cf8cd15b530f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "968756a6-0d1e-48ce-9362-70c5497ff820",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25c0c987-4ed2-4dbf-92d1-cf8cd15b530f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0334fc9e-3fee-48a6-a2e3-93db8f315d9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25c0c987-4ed2-4dbf-92d1-cf8cd15b530f",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        },
        {
            "id": "1c227fc1-a68c-4619-a3bb-8711441f9a92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "compositeImage": {
                "id": "dd7822af-663b-4ecc-989d-13409a503b25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c227fc1-a68c-4619-a3bb-8711441f9a92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91bac8e2-4478-4cb8-82c1-3f888dc57786",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c227fc1-a68c-4619-a3bb-8711441f9a92",
                    "LayerId": "a469e033-2af9-4056-b712-5351d0111cd9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a469e033-2af9-4056-b712-5351d0111cd9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d340546-231d-4fe5-abbb-9a843e30153a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 32
}