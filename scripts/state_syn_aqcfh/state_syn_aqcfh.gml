//


if(state_new == 1)
{
	with(hurtbox0)
	{
	    xoffset = -12;
	    yoffset = -13;
	    image_xscale  = 24;
	    image_yscale  = 40;
	    xshift  = 0;
	}
	
	with(hurtbox1)
	{
	    xoffset = -8;
	    yoffset = -31;
	    image_xscale  = 16;
	    image_yscale  = 18;
	    xshift = 0;
	}
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -13;
	    image_xscale  = 16;
	    image_yscale  = 40;
	    xshift = 0;
	}
	
	
	attackframe0 =  15;
	attackframe1 =  18;
	endstate = 42;
	
	vx = 0;
	vy = 0;
}




if(state_timer == attackframe0)
{
    for(i = 0; i < 20; i++)
    {
        hitbox1 = create_hitbox(5 + (15 * i), - 15 + (7 * i), 25, 10, 5, -15, 16, 7, 10, 16, -1,0,3, 0, "mid", -1);
        hitbox1.hitsound = snd_hit;
        hitbox1.scaling  = 0.5;
        hitbox1.projectile = 2;
        hitbox1.airxtrayhit = -1;
        // hitbox1.airxtrahitstun = -10;
        hitbox1.chipdamage = 4;
    }
}

if(state_timer == attackframe1)
{
    hitbox1 = create_hitbox(20, - 15, 1000, 20, 5, -10, 16, 7, 10, 16, -1,0,3, 0, "mid", -1);
    hitbox1.hitsound = snd_hit;
    hitbox1.scaling  = 0.5;
    hitbox1.projectile = 2;
    hitbox1.airxtrayhit = -4;
    //hitbox1.airxtrahitstun = -10;
    hitbox1.chipdamage = 4;
}


sprite_index = spr_lsyn_bhr;

if(state_timer > attackframe1)
{
    attack_base(endstate,1,0,0,1,0);
    vy += -0.4;
}
else
{
    attack_base(endstate,0,0,0,1,0);
}
