{
    "id": "b2b22020-e2a6-433f-8b20-f01f86e1a2d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_myu_portrait",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 9,
    "bbox_right": 40,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4854cc1c-89c6-40c1-939a-daf3d6d7af94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2b22020-e2a6-433f-8b20-f01f86e1a2d0",
            "compositeImage": {
                "id": "1a1f8663-7b83-474f-86be-e127cef14d99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4854cc1c-89c6-40c1-939a-daf3d6d7af94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "838e4dad-1141-4cf7-aa2a-cf48f7ac1cc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4854cc1c-89c6-40c1-939a-daf3d6d7af94",
                    "LayerId": "c3013f7d-6c65-4be6-8276-4c0b070a7dff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 46,
    "layers": [
        {
            "id": "c3013f7d-6c65-4be6-8276-4c0b070a7dff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b2b22020-e2a6-433f-8b20-f01f86e1a2d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 45,
    "xorig": 22,
    "yorig": 23
}