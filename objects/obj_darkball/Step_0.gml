/// @description Insert description here
// You can write your code in this editor


//movement
with(owner)
{
    
    if((key_light_hold == 0 && other.mode == "light") || (key_medium_hold == 0 && other.mode == "medium") || (key_heavy_hold == 0 && other.mode == "heavy"))
    {
        other.activate = 1;
    }
}
if(activate == 1)
{
    if(owner.otherplayer.hitpause == 0 || hitting == 0)
    {
        duration --;
    }
    
    //animation
    if(mode =="light")
    {
        sprite_index = spr_darkl;
        image_index =  abs(duration - 8);
    }
    else if(mode == "medium")
    {
        sprite_index = spr_darkm;
        image_index =  abs(duration - 8);
    }
    else
    {
        sprite_index = spr_darkh;
        image_index =  abs(duration - 8);
    }
}

if(duration == 0)
{
    if(mode == "light")
    {
        hitbox = create_hitbox(-5,-15, 95, 45, 4, -6, 15, 6, 6, 6, -1, 0, 1, 3, "low", "lightb");
        hitbox.owner = owner;
        hitbox.following = id;
        hitbox.hitsound = snd_hit;
        hitbox.scaling = 1.5;
        hitbox.projectile = 1;
        hitbox.airxtrayhit = -5;
        hitbox.airxtrahitstun = 3;
    }
    
    if(mode == "medium")
    {
        hitbox = create_hitbox(-5,-20, 110, 40, 2, 0, 5, 10, 6, 10, -1, 0, 0, 3, "mid", "mediumb");
        hitbox.owner = owner;
        hitbox.following = id;
        hitbox.hitsound = snd_hit;
        hitbox.scaling = 1.5;
        hitbox.projectile = 1;
        hitbox.airxtrayhit = -0.1;
        hitbox.airxtrahitstun = 3;
    }
    
    if(mode == "heavy")
    {
        hitbox = create_hitbox(-5,-20, 115, 45, 6, -2, 14, 12, 6, 8, -1, 1, 0, 3, "overhead", "heavyb");
        hitbox.owner = owner;
        hitbox.following = id;
        hitbox.hitsound = snd_hit;
        hitbox.scaling = 1.5;
        hitbox.projectile = 1;
        hitbox.airxtrayhit = 1.9;
    }
    
    duration --;
}

if(duration == -9)
{
    
    if(mode == "medium")
    {
        hitbox = create_hitbox(-5,-20, 110, 40, 5, 0, 14, 14, 6, 8, -1, 0, 0, 3, "mid", "mediumb");
        hitbox.owner = owner;
        hitbox.following = id;
        hitbox.hitsound = snd_hit;
        hitbox.scaling = 1.5;
        hitbox.projectile = 1;
        hitbox.airxtrayhit = -13;
        hitbox.airxtrahitstun = -3;
    }
    
    if(mode == "heavy")
    {
        hitbox = create_hitbox(-5,-15, 118, 45, 0, 9, 14, 12, 6, 8, -1, 1, 0, 3, "head", "heavyb");
        hitbox.owner = owner;
        hitbox.following = id;
        hitbox.hitsound = snd_hit;
        hitbox.scaling = 1.5;
        hitbox.projectile = 1;
        hitbox.airxtrahitstun = -3;
    }
    
    duration --;
}

if(duration == -16)
{
    if(mode == "light")
    {
        instance_destroy();
    }
}

if(duration == -24)
{
    instance_destroy();
}