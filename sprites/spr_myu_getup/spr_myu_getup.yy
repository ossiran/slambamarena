{
    "id": "9a7d9857-70a4-44a4-bc30-a23fa88a3b91",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_myu_getup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 6,
    "bbox_right": 43,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f1a200be-a134-4da5-8494-b8e4fd60473a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a7d9857-70a4-44a4-bc30-a23fa88a3b91",
            "compositeImage": {
                "id": "ba6910e5-4a66-44cc-84b4-da5e0cec5375",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1a200be-a134-4da5-8494-b8e4fd60473a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fa00079-34ae-4fea-b23d-a0d007bb893c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1a200be-a134-4da5-8494-b8e4fd60473a",
                    "LayerId": "3e479082-4a8a-40e8-bbb8-c84d56f97b7f"
                }
            ]
        },
        {
            "id": "0fcf7867-7e12-4e56-8b19-192b749a7c6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a7d9857-70a4-44a4-bc30-a23fa88a3b91",
            "compositeImage": {
                "id": "a2b50e48-e069-41a0-a4b9-f43579750eeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fcf7867-7e12-4e56-8b19-192b749a7c6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad23fc9a-1259-4919-b5eb-ed9829762a15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fcf7867-7e12-4e56-8b19-192b749a7c6e",
                    "LayerId": "3e479082-4a8a-40e8-bbb8-c84d56f97b7f"
                }
            ]
        },
        {
            "id": "cf1af171-d5f1-459b-8941-7c1f9cdac0e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a7d9857-70a4-44a4-bc30-a23fa88a3b91",
            "compositeImage": {
                "id": "8855602b-a780-4dc3-aad1-afd879651141",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf1af171-d5f1-459b-8941-7c1f9cdac0e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be8142a5-a66e-4284-8dd0-e8a02ee35bbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf1af171-d5f1-459b-8941-7c1f9cdac0e5",
                    "LayerId": "3e479082-4a8a-40e8-bbb8-c84d56f97b7f"
                }
            ]
        },
        {
            "id": "7eb6c75a-070b-4e5e-90fd-5d2645aa8a32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a7d9857-70a4-44a4-bc30-a23fa88a3b91",
            "compositeImage": {
                "id": "b8a1b2e7-a012-4bd8-8384-d98977e48915",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7eb6c75a-070b-4e5e-90fd-5d2645aa8a32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ab4d645-b649-40e7-9eae-6721c9f01a2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7eb6c75a-070b-4e5e-90fd-5d2645aa8a32",
                    "LayerId": "3e479082-4a8a-40e8-bbb8-c84d56f97b7f"
                }
            ]
        },
        {
            "id": "9dd60e25-e957-42e5-847b-e0027150d1f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a7d9857-70a4-44a4-bc30-a23fa88a3b91",
            "compositeImage": {
                "id": "f1983db0-3e11-484f-86c3-558060f91cad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dd60e25-e957-42e5-847b-e0027150d1f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ff65b1e-d4e2-4475-9651-c65f1ae228d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dd60e25-e957-42e5-847b-e0027150d1f2",
                    "LayerId": "3e479082-4a8a-40e8-bbb8-c84d56f97b7f"
                }
            ]
        },
        {
            "id": "706dcc80-8f1b-4601-b17c-ab8f13c91668",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a7d9857-70a4-44a4-bc30-a23fa88a3b91",
            "compositeImage": {
                "id": "cd0c120c-7cc9-404e-b552-2d0f1e4e0f17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "706dcc80-8f1b-4601-b17c-ab8f13c91668",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d425091b-d625-4faf-ada9-52fd2d7fa6cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "706dcc80-8f1b-4601-b17c-ab8f13c91668",
                    "LayerId": "3e479082-4a8a-40e8-bbb8-c84d56f97b7f"
                }
            ]
        },
        {
            "id": "45f4cf53-3f84-4dea-9cfa-dc2a0251bbbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a7d9857-70a4-44a4-bc30-a23fa88a3b91",
            "compositeImage": {
                "id": "5232b4a6-a9ef-4cf2-8eb2-7e5466ec4c68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45f4cf53-3f84-4dea-9cfa-dc2a0251bbbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3c3f2a8-6888-41eb-a3a4-f42e70dd030d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45f4cf53-3f84-4dea-9cfa-dc2a0251bbbf",
                    "LayerId": "3e479082-4a8a-40e8-bbb8-c84d56f97b7f"
                }
            ]
        },
        {
            "id": "29fbbc02-60e6-4692-97e4-d4ded3fe2e65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a7d9857-70a4-44a4-bc30-a23fa88a3b91",
            "compositeImage": {
                "id": "70ebef92-c979-4d15-afdd-58b7af53421c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29fbbc02-60e6-4692-97e4-d4ded3fe2e65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "560e607c-c564-4ae8-88b6-ed8089a1a625",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29fbbc02-60e6-4692-97e4-d4ded3fe2e65",
                    "LayerId": "3e479082-4a8a-40e8-bbb8-c84d56f97b7f"
                }
            ]
        },
        {
            "id": "3bb23178-4596-40f3-bc7b-cbba3de6c0a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a7d9857-70a4-44a4-bc30-a23fa88a3b91",
            "compositeImage": {
                "id": "aa5c663a-eedb-4189-812a-4082e4c50793",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bb23178-4596-40f3-bc7b-cbba3de6c0a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25545d29-ef89-4134-9f5d-209e34f06209",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bb23178-4596-40f3-bc7b-cbba3de6c0a9",
                    "LayerId": "3e479082-4a8a-40e8-bbb8-c84d56f97b7f"
                }
            ]
        },
        {
            "id": "e06531be-b079-485b-83b7-9e9b33a4dc5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a7d9857-70a4-44a4-bc30-a23fa88a3b91",
            "compositeImage": {
                "id": "df7111c9-75ef-4742-a808-40f02632c75a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e06531be-b079-485b-83b7-9e9b33a4dc5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9a4febf-ccf1-4d62-8f51-e3b34cc5fcab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e06531be-b079-485b-83b7-9e9b33a4dc5e",
                    "LayerId": "3e479082-4a8a-40e8-bbb8-c84d56f97b7f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3e479082-4a8a-40e8-bbb8-c84d56f97b7f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a7d9857-70a4-44a4-bc30-a23fa88a3b91",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 32
}