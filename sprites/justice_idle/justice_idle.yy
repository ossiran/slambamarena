{
    "id": "bbaabfed-20d2-47b4-9d52-ed9c255cd27b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "justice_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 145,
    "bbox_left": 54,
    "bbox_right": 105,
    "bbox_top": 74,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "365a17ed-7e60-4e9f-b844-22669c506ed8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbaabfed-20d2-47b4-9d52-ed9c255cd27b",
            "compositeImage": {
                "id": "869ac10d-cd51-47e8-9493-585b7acb4374",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "365a17ed-7e60-4e9f-b844-22669c506ed8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ece811fc-2a85-489b-a600-577bf0aedcee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "365a17ed-7e60-4e9f-b844-22669c506ed8",
                    "LayerId": "e0bb3937-96c5-4529-9c7b-8bdd5f1a88df"
                }
            ]
        },
        {
            "id": "9ef93c38-1eeb-419e-9bfc-2f93c910b7ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbaabfed-20d2-47b4-9d52-ed9c255cd27b",
            "compositeImage": {
                "id": "9f53de92-e189-44bf-9424-11a90fb9db48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ef93c38-1eeb-419e-9bfc-2f93c910b7ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a614202-3950-42c8-a91a-7ae5e5fd76da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ef93c38-1eeb-419e-9bfc-2f93c910b7ec",
                    "LayerId": "e0bb3937-96c5-4529-9c7b-8bdd5f1a88df"
                }
            ]
        },
        {
            "id": "7036eda6-baf7-465e-94f2-97f2c475b485",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbaabfed-20d2-47b4-9d52-ed9c255cd27b",
            "compositeImage": {
                "id": "1af01ff1-f9d8-4dca-9436-467a0f1b4b00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7036eda6-baf7-465e-94f2-97f2c475b485",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0b449e9-030a-4be1-b007-50fbb5d007c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7036eda6-baf7-465e-94f2-97f2c475b485",
                    "LayerId": "e0bb3937-96c5-4529-9c7b-8bdd5f1a88df"
                }
            ]
        },
        {
            "id": "3c7661a5-f8cc-4b56-b97e-d9a0dc4073f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbaabfed-20d2-47b4-9d52-ed9c255cd27b",
            "compositeImage": {
                "id": "499cb422-83d8-4e8e-9d47-8523d782e456",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c7661a5-f8cc-4b56-b97e-d9a0dc4073f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82b99340-2214-4b86-8e82-1b751a1432f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c7661a5-f8cc-4b56-b97e-d9a0dc4073f2",
                    "LayerId": "e0bb3937-96c5-4529-9c7b-8bdd5f1a88df"
                }
            ]
        },
        {
            "id": "342ae3d4-3afb-468e-8a51-aa1f48df2d1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbaabfed-20d2-47b4-9d52-ed9c255cd27b",
            "compositeImage": {
                "id": "0f9a8172-9255-4bf9-bcc7-f438b49144f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "342ae3d4-3afb-468e-8a51-aa1f48df2d1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07bfee92-4672-433b-b62a-ba817a4f8ff6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "342ae3d4-3afb-468e-8a51-aa1f48df2d1d",
                    "LayerId": "e0bb3937-96c5-4529-9c7b-8bdd5f1a88df"
                }
            ]
        },
        {
            "id": "de81c320-bc0b-4d0b-b529-f281d92d6908",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbaabfed-20d2-47b4-9d52-ed9c255cd27b",
            "compositeImage": {
                "id": "ab05d1f8-78e3-4b13-9715-ab61c04e1b82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de81c320-bc0b-4d0b-b529-f281d92d6908",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0572b5f1-e8c5-4822-bf37-802b59f58ee6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de81c320-bc0b-4d0b-b529-f281d92d6908",
                    "LayerId": "e0bb3937-96c5-4529-9c7b-8bdd5f1a88df"
                }
            ]
        },
        {
            "id": "4eef1398-7700-49a8-8f75-b3c6b01b3294",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbaabfed-20d2-47b4-9d52-ed9c255cd27b",
            "compositeImage": {
                "id": "1853099b-a745-4028-a5e5-5a46b7d78dd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4eef1398-7700-49a8-8f75-b3c6b01b3294",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf04388d-f32b-4264-9d63-7a981b8ffb8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4eef1398-7700-49a8-8f75-b3c6b01b3294",
                    "LayerId": "e0bb3937-96c5-4529-9c7b-8bdd5f1a88df"
                }
            ]
        },
        {
            "id": "71388259-51be-4aec-90c6-5c134d820939",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbaabfed-20d2-47b4-9d52-ed9c255cd27b",
            "compositeImage": {
                "id": "f184611a-17bd-4467-9b6e-b1d02c0f03ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71388259-51be-4aec-90c6-5c134d820939",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47df980a-57fc-4f75-b28c-402ab2837875",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71388259-51be-4aec-90c6-5c134d820939",
                    "LayerId": "e0bb3937-96c5-4529-9c7b-8bdd5f1a88df"
                }
            ]
        },
        {
            "id": "31d49609-e9ee-42a1-8a6a-984bc5ff04c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbaabfed-20d2-47b4-9d52-ed9c255cd27b",
            "compositeImage": {
                "id": "30b91da2-b9f1-4231-93cb-be9cab15c580",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31d49609-e9ee-42a1-8a6a-984bc5ff04c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "483122a6-eebc-41ca-ad9c-dcf1945d666a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31d49609-e9ee-42a1-8a6a-984bc5ff04c0",
                    "LayerId": "e0bb3937-96c5-4529-9c7b-8bdd5f1a88df"
                }
            ]
        },
        {
            "id": "3eb3afde-f8a5-4b72-8c64-8b7264736dae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbaabfed-20d2-47b4-9d52-ed9c255cd27b",
            "compositeImage": {
                "id": "25176e1b-6aea-4b94-a661-5d0863d636fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3eb3afde-f8a5-4b72-8c64-8b7264736dae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e444977-7ed8-49b8-b67b-ec4a663ab439",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3eb3afde-f8a5-4b72-8c64-8b7264736dae",
                    "LayerId": "e0bb3937-96c5-4529-9c7b-8bdd5f1a88df"
                }
            ]
        },
        {
            "id": "95ab77fb-7b70-45dd-a0f1-9b8dfa807719",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbaabfed-20d2-47b4-9d52-ed9c255cd27b",
            "compositeImage": {
                "id": "6f99aa52-0c8c-4b32-909d-2ffee2a6ffb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95ab77fb-7b70-45dd-a0f1-9b8dfa807719",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0579e215-5d89-4cac-8aba-0b25e7313b3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95ab77fb-7b70-45dd-a0f1-9b8dfa807719",
                    "LayerId": "e0bb3937-96c5-4529-9c7b-8bdd5f1a88df"
                }
            ]
        },
        {
            "id": "2b8eeb62-041b-4e26-b350-2b89b6c8669a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbaabfed-20d2-47b4-9d52-ed9c255cd27b",
            "compositeImage": {
                "id": "8f24ea10-a181-436b-9555-1cb9398c0082",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b8eeb62-041b-4e26-b350-2b89b6c8669a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c08cbcb3-e730-4edd-89e3-6f8eb1d2f831",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b8eeb62-041b-4e26-b350-2b89b6c8669a",
                    "LayerId": "e0bb3937-96c5-4529-9c7b-8bdd5f1a88df"
                }
            ]
        },
        {
            "id": "e216b1e3-5d47-4be9-9c82-21d6cafe3e09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbaabfed-20d2-47b4-9d52-ed9c255cd27b",
            "compositeImage": {
                "id": "feb8e3ca-4dfa-4e8f-aec6-8e691ffc9b65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e216b1e3-5d47-4be9-9c82-21d6cafe3e09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95357adb-0ee2-42b7-89f6-ed6f362412b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e216b1e3-5d47-4be9-9c82-21d6cafe3e09",
                    "LayerId": "e0bb3937-96c5-4529-9c7b-8bdd5f1a88df"
                }
            ]
        },
        {
            "id": "8f014f6d-536e-49a1-a106-6a2aaa1c1a73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbaabfed-20d2-47b4-9d52-ed9c255cd27b",
            "compositeImage": {
                "id": "72b0da2a-6241-49a8-ab1d-542f8ce4baeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f014f6d-536e-49a1-a106-6a2aaa1c1a73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57ad4e60-ab46-406e-923f-7461ee29fd28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f014f6d-536e-49a1-a106-6a2aaa1c1a73",
                    "LayerId": "e0bb3937-96c5-4529-9c7b-8bdd5f1a88df"
                }
            ]
        },
        {
            "id": "a717da01-c93d-4fcd-9734-d7b0450cf9b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbaabfed-20d2-47b4-9d52-ed9c255cd27b",
            "compositeImage": {
                "id": "8c5911b1-214d-4754-887d-0aa0eec5f23f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a717da01-c93d-4fcd-9734-d7b0450cf9b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "382c06f2-af99-4a3c-a0d5-c4e934d72452",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a717da01-c93d-4fcd-9734-d7b0450cf9b1",
                    "LayerId": "e0bb3937-96c5-4529-9c7b-8bdd5f1a88df"
                }
            ]
        },
        {
            "id": "1eb2c301-3229-4859-a02c-a9b789b59900",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbaabfed-20d2-47b4-9d52-ed9c255cd27b",
            "compositeImage": {
                "id": "38c5848f-0820-41dd-a301-532f868741a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1eb2c301-3229-4859-a02c-a9b789b59900",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfddc003-d0f0-49cb-bec1-67cadea4edb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1eb2c301-3229-4859-a02c-a9b789b59900",
                    "LayerId": "e0bb3937-96c5-4529-9c7b-8bdd5f1a88df"
                }
            ]
        },
        {
            "id": "d0c9a563-4088-4736-a357-79c73278ee32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbaabfed-20d2-47b4-9d52-ed9c255cd27b",
            "compositeImage": {
                "id": "6c72ac24-f214-49d7-9c67-818cd3e90329",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0c9a563-4088-4736-a357-79c73278ee32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c883e5e0-b1c1-429c-8f8c-7780ad3c64d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0c9a563-4088-4736-a357-79c73278ee32",
                    "LayerId": "e0bb3937-96c5-4529-9c7b-8bdd5f1a88df"
                }
            ]
        },
        {
            "id": "77b29a44-7f36-4fc5-a4a4-426e0b10f3d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbaabfed-20d2-47b4-9d52-ed9c255cd27b",
            "compositeImage": {
                "id": "04748961-3d50-455b-adc2-50fea73b7aab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77b29a44-7f36-4fc5-a4a4-426e0b10f3d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "426179d0-db10-4cd6-9bd9-0a9a4d4d0e15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77b29a44-7f36-4fc5-a4a4-426e0b10f3d5",
                    "LayerId": "e0bb3937-96c5-4529-9c7b-8bdd5f1a88df"
                }
            ]
        },
        {
            "id": "396f18c9-f1b2-4302-bedf-0929811d6f14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbaabfed-20d2-47b4-9d52-ed9c255cd27b",
            "compositeImage": {
                "id": "ebcaa023-addf-4468-b379-a9f584919e3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "396f18c9-f1b2-4302-bedf-0929811d6f14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e93fd5d3-4c4c-484a-9f97-cb6be21fcca7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "396f18c9-f1b2-4302-bedf-0929811d6f14",
                    "LayerId": "e0bb3937-96c5-4529-9c7b-8bdd5f1a88df"
                }
            ]
        },
        {
            "id": "744593d8-ffaa-4fab-9e54-88ec2e74a5fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbaabfed-20d2-47b4-9d52-ed9c255cd27b",
            "compositeImage": {
                "id": "e6c51545-4403-4be3-b328-a58833bf0911",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "744593d8-ffaa-4fab-9e54-88ec2e74a5fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3268dd4c-2a9b-4353-b2d3-604995ac45d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "744593d8-ffaa-4fab-9e54-88ec2e74a5fc",
                    "LayerId": "e0bb3937-96c5-4529-9c7b-8bdd5f1a88df"
                }
            ]
        },
        {
            "id": "ebcda80f-8a61-4b13-99cc-c1191d2cb2b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbaabfed-20d2-47b4-9d52-ed9c255cd27b",
            "compositeImage": {
                "id": "bf64a7cb-4c9c-46fd-9d42-a22b1facad05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebcda80f-8a61-4b13-99cc-c1191d2cb2b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "511b0807-8d48-4a39-8c11-daf645a157da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebcda80f-8a61-4b13-99cc-c1191d2cb2b7",
                    "LayerId": "e0bb3937-96c5-4529-9c7b-8bdd5f1a88df"
                }
            ]
        },
        {
            "id": "aa68e33b-8473-4a89-9673-a940589f8013",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbaabfed-20d2-47b4-9d52-ed9c255cd27b",
            "compositeImage": {
                "id": "ad10ef36-e055-4bf9-86ba-dd864bf04cc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa68e33b-8473-4a89-9673-a940589f8013",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87159be6-71ca-40dc-a56a-f4bfb45ac54a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa68e33b-8473-4a89-9673-a940589f8013",
                    "LayerId": "e0bb3937-96c5-4529-9c7b-8bdd5f1a88df"
                }
            ]
        },
        {
            "id": "b9fe8660-054b-4a6e-aee9-2b03e2a3223a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbaabfed-20d2-47b4-9d52-ed9c255cd27b",
            "compositeImage": {
                "id": "f9fc33b9-33fc-42ee-9c10-a2ba03e8f41e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9fe8660-054b-4a6e-aee9-2b03e2a3223a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e295ea64-1259-49fc-8fa3-c1708ffb1672",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9fe8660-054b-4a6e-aee9-2b03e2a3223a",
                    "LayerId": "e0bb3937-96c5-4529-9c7b-8bdd5f1a88df"
                }
            ]
        },
        {
            "id": "d97b9885-ca41-4ff4-b72f-e3cb0620da36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbaabfed-20d2-47b4-9d52-ed9c255cd27b",
            "compositeImage": {
                "id": "de27c232-8db9-4686-adcf-128a64978713",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d97b9885-ca41-4ff4-b72f-e3cb0620da36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78f5fa4d-b15d-4767-95de-2b134311b6f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d97b9885-ca41-4ff4-b72f-e3cb0620da36",
                    "LayerId": "e0bb3937-96c5-4529-9c7b-8bdd5f1a88df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "e0bb3937-96c5-4529-9c7b-8bdd5f1a88df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bbaabfed-20d2-47b4-9d52-ed9c255cd27b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 79,
    "yorig": 121
}