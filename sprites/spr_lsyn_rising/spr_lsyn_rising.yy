{
    "id": "5478d68a-9ff5-485a-8ff4-313b23e89be5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lsyn_rising",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 49,
    "bbox_right": 86,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5230bb82-9719-4480-9ace-44ac99e76da0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5478d68a-9ff5-485a-8ff4-313b23e89be5",
            "compositeImage": {
                "id": "eb78f11b-1812-4e0b-8e70-b7e2782aa0d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5230bb82-9719-4480-9ace-44ac99e76da0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "342441b2-6ac8-4167-8882-3e71853cf26f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5230bb82-9719-4480-9ace-44ac99e76da0",
                    "LayerId": "c1a60ba0-7185-42ca-95aa-f633085f8a8c"
                }
            ]
        },
        {
            "id": "75e763d3-913c-463f-bff4-f65d8083cd69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5478d68a-9ff5-485a-8ff4-313b23e89be5",
            "compositeImage": {
                "id": "fda66f71-dce3-4848-b776-3fac0bc9502b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75e763d3-913c-463f-bff4-f65d8083cd69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d533c92-811c-484c-bbe5-c27cefe7f883",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75e763d3-913c-463f-bff4-f65d8083cd69",
                    "LayerId": "c1a60ba0-7185-42ca-95aa-f633085f8a8c"
                }
            ]
        },
        {
            "id": "f1446735-eb7b-4137-86b3-c84a73640487",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5478d68a-9ff5-485a-8ff4-313b23e89be5",
            "compositeImage": {
                "id": "7be7a328-7bb0-46b3-9ea2-2454821ca00a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1446735-eb7b-4137-86b3-c84a73640487",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f32a0734-a4cf-4e26-8c68-4274165ef469",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1446735-eb7b-4137-86b3-c84a73640487",
                    "LayerId": "c1a60ba0-7185-42ca-95aa-f633085f8a8c"
                }
            ]
        },
        {
            "id": "767a0154-062d-44a4-a687-7edb4a7e52ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5478d68a-9ff5-485a-8ff4-313b23e89be5",
            "compositeImage": {
                "id": "d3249b09-b0ff-45e6-b10d-b401be23aabe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "767a0154-062d-44a4-a687-7edb4a7e52ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bef83b34-87c5-4ff1-9fb6-474420539bd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "767a0154-062d-44a4-a687-7edb4a7e52ce",
                    "LayerId": "c1a60ba0-7185-42ca-95aa-f633085f8a8c"
                }
            ]
        },
        {
            "id": "a4e54ffe-9c46-4ea1-a779-96e9f97c513e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5478d68a-9ff5-485a-8ff4-313b23e89be5",
            "compositeImage": {
                "id": "9ed47559-64bb-4fca-913d-7df437848667",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4e54ffe-9c46-4ea1-a779-96e9f97c513e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2dc8f60-470b-4a5c-9789-973fc5b092ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4e54ffe-9c46-4ea1-a779-96e9f97c513e",
                    "LayerId": "c1a60ba0-7185-42ca-95aa-f633085f8a8c"
                }
            ]
        },
        {
            "id": "6ff63682-e523-4d41-b8d0-077ae5e49044",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5478d68a-9ff5-485a-8ff4-313b23e89be5",
            "compositeImage": {
                "id": "0fe480ae-8372-43a7-bf1c-9e4b1797d39a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ff63682-e523-4d41-b8d0-077ae5e49044",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb37c1ca-1d41-4236-ac78-9b7f06ac1f0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ff63682-e523-4d41-b8d0-077ae5e49044",
                    "LayerId": "c1a60ba0-7185-42ca-95aa-f633085f8a8c"
                }
            ]
        },
        {
            "id": "6a5230f4-c6c5-44f2-abe2-4404bc2e21de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5478d68a-9ff5-485a-8ff4-313b23e89be5",
            "compositeImage": {
                "id": "1601fa6b-ea79-4b78-bb0d-f503a07221ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a5230f4-c6c5-44f2-abe2-4404bc2e21de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a669c2df-6a23-42e1-88be-280d9b709303",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a5230f4-c6c5-44f2-abe2-4404bc2e21de",
                    "LayerId": "c1a60ba0-7185-42ca-95aa-f633085f8a8c"
                }
            ]
        },
        {
            "id": "56d7ffdf-4213-43e2-9191-1dc39330449c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5478d68a-9ff5-485a-8ff4-313b23e89be5",
            "compositeImage": {
                "id": "ad3f1fff-1ee0-4a1d-8368-518630733ae0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56d7ffdf-4213-43e2-9191-1dc39330449c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c32f3fbf-caeb-44ab-9e6d-3e24366adfb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56d7ffdf-4213-43e2-9191-1dc39330449c",
                    "LayerId": "c1a60ba0-7185-42ca-95aa-f633085f8a8c"
                }
            ]
        },
        {
            "id": "0ea78d78-6b85-48da-b0ec-2714fcc6f190",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5478d68a-9ff5-485a-8ff4-313b23e89be5",
            "compositeImage": {
                "id": "bc6d3a25-fbce-49e0-ac20-8b2ade813017",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ea78d78-6b85-48da-b0ec-2714fcc6f190",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c16f2c65-6f53-4384-81b0-a72e342ccb12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ea78d78-6b85-48da-b0ec-2714fcc6f190",
                    "LayerId": "c1a60ba0-7185-42ca-95aa-f633085f8a8c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "c1a60ba0-7185-42ca-95aa-f633085f8a8c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5478d68a-9ff5-485a-8ff4-313b23e89be5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}