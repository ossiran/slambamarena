{
    "id": "f17c673d-c869-423e-9adc-b80b5e3fa744",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_proxbox",
    "eventList": [
        {
            "id": "d65144ad-ec91-4127-b405-406c0ad1fb82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f17c673d-c869-423e-9adc-b80b5e3fa744"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "023b8270-ab19-42e1-a7bf-3bce7b122b8a",
    "visible": true
}