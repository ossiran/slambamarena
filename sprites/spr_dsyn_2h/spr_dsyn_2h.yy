{
    "id": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dsyn_2h",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 37,
    "bbox_right": 101,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24260eb1-f73c-4f80-b509-78d98fd0631a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "0b729e3d-7fbb-40e8-bba4-b512912c6267",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24260eb1-f73c-4f80-b509-78d98fd0631a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2753605a-fd5c-42f6-a58f-209ebf4747cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24260eb1-f73c-4f80-b509-78d98fd0631a",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "ed6066f0-cf49-4415-8dec-58e4c1403552",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "9cb5e076-3aba-49ae-b820-6e2fb5b95491",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed6066f0-cf49-4415-8dec-58e4c1403552",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f347c81-d717-4bda-90da-2243fd7bc711",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed6066f0-cf49-4415-8dec-58e4c1403552",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "d62b29a2-999d-40fe-b56a-e3511f4507da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "cd771e88-aa01-4f58-8aa5-a0522286578e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d62b29a2-999d-40fe-b56a-e3511f4507da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5aa9f5b9-d0a2-49f1-b109-fe05ce85df1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d62b29a2-999d-40fe-b56a-e3511f4507da",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "e67da23b-50b4-4710-8549-406710ed48cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "b6101bee-cd08-48fc-a75b-cccbaad9231f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e67da23b-50b4-4710-8549-406710ed48cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "521f29fb-67fd-44b9-96f8-ccc007a262e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e67da23b-50b4-4710-8549-406710ed48cb",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "dcde94ac-3805-4d76-9cc6-be8a67c7669f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "a88f47a7-0b7b-46b4-98ba-15e4dd6052a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcde94ac-3805-4d76-9cc6-be8a67c7669f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90f7551a-1797-45aa-b49b-e930061ebcc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcde94ac-3805-4d76-9cc6-be8a67c7669f",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "38bcf55f-6bc5-43e1-9a84-5771a324e5b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "57c1285a-c5bb-4c76-9b26-cbfb98068daf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38bcf55f-6bc5-43e1-9a84-5771a324e5b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58a7d74e-2575-4172-b9b2-37b781cc58d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38bcf55f-6bc5-43e1-9a84-5771a324e5b9",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "dc4c2874-a03d-46c1-8f1d-58a33f24f9c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "e0b2daaa-5e48-44a7-a1a4-3e884aaa6a62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc4c2874-a03d-46c1-8f1d-58a33f24f9c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca0b4c8c-66eb-4e00-9b12-0af4fa2f394c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc4c2874-a03d-46c1-8f1d-58a33f24f9c4",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "7d20a976-c54d-41b8-8bb2-e73921796b46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "027a48b2-0e4f-40f8-9957-de8b96e7ef6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d20a976-c54d-41b8-8bb2-e73921796b46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e68c1704-3eba-4b66-85ce-4633219b9b00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d20a976-c54d-41b8-8bb2-e73921796b46",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "d30a66a1-dd8f-4b54-8256-89d635402dfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "d42c5f04-f544-48bc-918c-bc503a22505a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d30a66a1-dd8f-4b54-8256-89d635402dfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a9d520a-ff03-4294-ba06-1d713569a110",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d30a66a1-dd8f-4b54-8256-89d635402dfc",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "edda93d5-d4a4-40f2-9c41-cc365b215ae7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "bf48b270-b959-480f-972a-0b12fe4ace1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edda93d5-d4a4-40f2-9c41-cc365b215ae7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "905795b0-5900-4169-93e0-65e8e04fa895",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edda93d5-d4a4-40f2-9c41-cc365b215ae7",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "107b38b3-c328-47c7-bcee-5d7367a0d7ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "c11abe7e-c2b6-4f94-800a-8da1e53a9859",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "107b38b3-c328-47c7-bcee-5d7367a0d7ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ef488bf-7df9-46bc-b3d7-168ed6db3235",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "107b38b3-c328-47c7-bcee-5d7367a0d7ce",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "f8bb511d-61db-4622-889f-45054e9197b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "eec09c1b-bba8-485f-9188-d2e6a1e35aab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8bb511d-61db-4622-889f-45054e9197b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "162f415f-c844-408c-8fee-868a981428e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8bb511d-61db-4622-889f-45054e9197b2",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "7d497a35-9d67-473d-8e08-44e604a81d5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "36fac207-8dc3-4e5e-a7c0-07069ceabc8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d497a35-9d67-473d-8e08-44e604a81d5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b1c8eba-f8d2-4f42-81be-5c2dd3357c9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d497a35-9d67-473d-8e08-44e604a81d5c",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "554ddd64-3757-4770-97d5-ba7354c13854",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "3a9754c5-881e-4247-b2c0-8573a2f664b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "554ddd64-3757-4770-97d5-ba7354c13854",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf991c25-0bec-4868-94da-d28dbfb2c784",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "554ddd64-3757-4770-97d5-ba7354c13854",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "8d0d15cf-2349-4e15-a732-38dfcbec37c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "01e2ded5-bbd5-4d52-a303-542ba22fd638",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d0d15cf-2349-4e15-a732-38dfcbec37c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05253ec6-9c3f-4cda-8707-5284b270e3b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d0d15cf-2349-4e15-a732-38dfcbec37c6",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "c5512cc7-f889-48d6-833e-d4a13de505db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "51794486-1478-41b2-b903-bb9722dea3f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5512cc7-f889-48d6-833e-d4a13de505db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3f81279-39b0-47c2-a22d-95b9a63fc3c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5512cc7-f889-48d6-833e-d4a13de505db",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "30b737c2-b93a-4e83-b2e9-4c49f131ac17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "ca8d6bb8-ea3b-48dc-a145-478260fcfc6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30b737c2-b93a-4e83-b2e9-4c49f131ac17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7c5293b-2d78-4391-871f-5377a8a8ebe5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30b737c2-b93a-4e83-b2e9-4c49f131ac17",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "7f8d6996-a9a8-4d56-ab7d-86a20f44bd93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "cfd6b785-ac53-437a-891a-2c7f99c48853",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f8d6996-a9a8-4d56-ab7d-86a20f44bd93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b30cd983-ca11-4bb1-af62-b57ce6c53fc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f8d6996-a9a8-4d56-ab7d-86a20f44bd93",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "a025e4b4-038a-442d-a637-c1fe97c4a8b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "bb9575f8-53f3-4d43-983f-c473c9ee0834",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a025e4b4-038a-442d-a637-c1fe97c4a8b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "281739e9-6d3d-4039-8648-6f0c803abb37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a025e4b4-038a-442d-a637-c1fe97c4a8b0",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "51ca3a0f-4499-4c42-a5b2-07a2c6deab8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "b4185399-c13c-4a46-91b2-d817410ef565",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51ca3a0f-4499-4c42-a5b2-07a2c6deab8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "263a0359-279d-4810-936c-2722b3b1775a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51ca3a0f-4499-4c42-a5b2-07a2c6deab8f",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "5f7ce4e5-0c06-47ad-a771-0c3ecc8bca33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "177e58bf-3066-489b-a3bc-038638097ab3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f7ce4e5-0c06-47ad-a771-0c3ecc8bca33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b8355b9-8f8c-4bf9-b914-a545559b7b9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f7ce4e5-0c06-47ad-a771-0c3ecc8bca33",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "a3de282c-1659-401c-b32f-17d18844e146",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "12538414-9355-43f8-9de2-7d4ef6053177",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3de282c-1659-401c-b32f-17d18844e146",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f26f85c-4d6b-497e-b55b-dd5653c1dabf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3de282c-1659-401c-b32f-17d18844e146",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "190c959a-823f-4577-8bb4-5c731a402f02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "b6a411d4-d2ea-4fa8-84ed-6fb50aba55f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "190c959a-823f-4577-8bb4-5c731a402f02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7305e321-4943-483c-81bd-2e3e51466209",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "190c959a-823f-4577-8bb4-5c731a402f02",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "93df3ef5-7c0b-4a45-b72b-7f85d1dfa990",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "b312a539-da67-4384-836b-75bec5719bb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93df3ef5-7c0b-4a45-b72b-7f85d1dfa990",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b7c4e8a-2ced-4874-95a6-ac0a226c4f44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93df3ef5-7c0b-4a45-b72b-7f85d1dfa990",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "1a41f532-6739-4e7c-95b6-40be89b20af2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "a45705b6-c6b5-41bb-8ef5-c5111f7c7c2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a41f532-6739-4e7c-95b6-40be89b20af2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e84fd39-2bce-4c61-926b-ddd95bd7ca05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a41f532-6739-4e7c-95b6-40be89b20af2",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "bac6bf24-e9c1-413b-bf1b-713bd0890366",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "fd9dc431-5ec0-4fb7-a8f3-1565fa134136",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bac6bf24-e9c1-413b-bf1b-713bd0890366",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50de67a3-6142-42e2-b935-abc6a7161f8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bac6bf24-e9c1-413b-bf1b-713bd0890366",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "e427a6cc-9b42-400a-ad0f-6cff6ac015ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "5f5ca954-e8d6-4018-b45f-446e6ef609b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e427a6cc-9b42-400a-ad0f-6cff6ac015ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48497917-5c02-4950-ba67-bfef7741a414",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e427a6cc-9b42-400a-ad0f-6cff6ac015ec",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "75bfc184-7c23-424d-9568-3c6d27cfff96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "705663c5-ccec-4bc9-9763-97668807d78d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75bfc184-7c23-424d-9568-3c6d27cfff96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe9261b1-a30c-4e2a-b46b-7220a0640d20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75bfc184-7c23-424d-9568-3c6d27cfff96",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "21b49fc9-e473-43aa-8a3a-835987505f8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "17959f1d-a08a-4c93-8f82-651f5ffea96d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21b49fc9-e473-43aa-8a3a-835987505f8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07e1ea0f-89b5-4838-93df-c6fd5b5ec9c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21b49fc9-e473-43aa-8a3a-835987505f8b",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "1d1059e9-6578-48a3-8ad0-cc853799181b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "c24442fa-4411-4265-a4a4-1a306547d708",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d1059e9-6578-48a3-8ad0-cc853799181b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af2b659c-ce30-4a61-a1b6-cd56f29a1309",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d1059e9-6578-48a3-8ad0-cc853799181b",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "e532abe7-66c0-408c-a15b-f503c94aa799",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "3d72a72a-c1dd-478e-b019-366d40e4bb12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e532abe7-66c0-408c-a15b-f503c94aa799",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45f38e0f-f57d-4ea7-a9ae-ad2359400126",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e532abe7-66c0-408c-a15b-f503c94aa799",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "ab656e38-dcb4-47e2-bb45-010039110960",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "dba7fd17-10b8-448f-9732-f3390f319a49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab656e38-dcb4-47e2-bb45-010039110960",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "763fc935-945d-4eae-bd8e-87a429013771",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab656e38-dcb4-47e2-bb45-010039110960",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        },
        {
            "id": "708fa10b-bffa-4d92-a1a5-185429dba9d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "compositeImage": {
                "id": "fc0c43bf-e2f6-4eed-990d-46df8ea5ea9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "708fa10b-bffa-4d92-a1a5-185429dba9d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49f73341-a201-4538-8d71-e53854e5336c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "708fa10b-bffa-4d92-a1a5-185429dba9d1",
                    "LayerId": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "ad6e5cd7-978a-446f-a59b-3ba9213b5dd4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "09dccddd-cdaf-45ef-b0ae-9d2d30e43daa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 76
}