if(obj_system.players_control > 0)
{
    inputcounter = wrap(inputcounter + 1, 0, 300);
    
    
    if(key_left == 1 && key_down == 1 && key_right == 0 && key_up == 0)
    {
        key_dir = 1;
    }
    else if(key_right == 1 && key_down == 1 && key_left == 0 && key_up == 0)
    {
        key_dir = 3;
    }
    else if(key_left == 1 && key_up == 1 && key_right == 0 && key_down == 0)
    {
        key_dir = 7;
    }
    else if(key_right == 1 && key_up == 1 && key_left == 0 && key_down == 0)
    {
        key_dir = 9;
    }
    else if(key_left == 1 && key_right == 0)
    {
        key_dir = 4;
    }
    else if(key_right == 1 && key_left == 0)
    {
        key_dir = 6;
    }
    else if(key_down == 1 && key_up == 0)
    {
        key_dir = 2;
    }
    else if(key_up == 1 && key_down == 0)
    {
        key_dir = 8;
    }
    else
    {
        key_dir = 5;
    }
    
    
}
    input[inputcounter,0] = key_dir;
    input[wrap(inputcounter + 1, 0, 300),0] = 0;
    input[wrap(inputcounter + 1, 0, 300),1] = 0;
    input[wrap(inputcounter + 1, 0, 300),2] = 0;
    input[wrap(inputcounter + 1, 0, 300),3] = 0;
    input[wrap(inputcounter + 1, 0, 300),4] = 0;

if(obj_system.players_control > 1)
{
    //buffers
    
    //light
    if(light_buffer > 0)
    {
        light_buffer --;
        input[wrap(inputcounter, 0, 300),1] = light_buffer;
    }
    if(key_light == 1)
    {
        light_buffer = 4;
        input[wrap(inputcounter, 0, 300),1] = light_buffer;
    }
    //hold
    if(key_light_hold == 1 && light_buffer == 0)
    {
        input[wrap(inputcounter, 0, 300),1] = -1;
    }
    
    //medium
    if(medium_buffer > 0)
    {
        medium_buffer --;
        input[wrap(inputcounter, 0, 300),2] = medium_buffer;
    }
    if(key_medium == 1)
    {
        medium_buffer = 4;
        input[wrap(inputcounter, 0, 300),2] = medium_buffer;
    }
    //hold
    if(key_medium_hold == 1 && medium_buffer == 0)
    {
        input[wrap(inputcounter, 0, 300),2] = -1;
    }
    
    
    //heavy
    if(heavy_buffer > 0)
    {
        heavy_buffer --;
        input[wrap(inputcounter, 0, 300),3] = heavy_buffer;
    }
    
    if(key_heavy == 1)
    {
        heavy_buffer = 4;
        input[wrap(inputcounter, 0, 300),3] = heavy_buffer;
    }
    
    //hold
    if(key_heavy_hold == 1 && heavy_buffer == 0)
    {
        input[wrap(inputcounter, 0, 300),3] = -1;
    }
    
    
    
    if(key_dash == 1 )
    {
        light_buffer = 4;
        medium_buffer = 4;
    }
    
    
    //essence
    if(essence_buffer > 0)
    {
        essence_buffer --;
        input[wrap(inputcounter, 0, 300),4] = essence_buffer;
    }
    if(key_essence == 1)
    {
        essence_buffer = 4;
        input[wrap(inputcounter, 0, 300),4] = essence_buffer;
    }
    //hold
    if(key_essence_hold == 1 && essence_buffer == 0)
    {
        input[wrap(inputcounter, 0, 300),4] = -1;
    }
    
    
    //special buffer
    
    if(state_exists("dqcf") == 1)
    {
        if(dqcf_buffer > 0)
        {
            dqcf_buffer --;
        }
        
        if(dqcf() == 1 )
        {
            dqcf_buffer = 3;
        }
    }
    
    
    if(state_exists("qcfl") == 1)
    {
        if(qcf_buffer > 0)
        {
            qcf_buffer --;
        }
    
    
        if(qcf() == 1)
        {
            qcf_buffer = 3;
        }
    }
    
    if(state_exists("srkl") == 1)
    {
        if(srk_buffer > 0)
        {
            srk_buffer --;
        }
        
        if(srk() ==  1)
        {
            srk_buffer = 3;
        }
    }
    
    if(state_exists("qcbl") == 1)
    {
        if(qcb_buffer > 0)
        {
            qcb_buffer --;
        }
        
        if(qcb() == 1)
        {
            qcb_buffer = 3;
        }
    }
    
    if(state_exists("hcbl") == 1)
    {
        if(hcb_buffer > 0)
        {
            hcb_buffer --;
        }
        
        if(qcb() == 1)
        {
            hcb_buffer = 3;
        }
    }
    
    if(state_exists("ddl") == 1)
    {
        if(dd_buffer > 0)
        {
            dd_buffer --;
        }
        
        if(dtap(2) == 1)
        {
            dd_buffer = 3;
        }
    }
    
    if(state_exists("bhrl") == 1)
    {
        //light
        if(bhrl_buffer > 0)
        {
            bhrl_buffer --;
        }
        
        if(bholdrel(1) == 1)
        {
            bhrl_buffer = 3;
        }
        
        //medium
        if(bhrm_buffer > 0)
        {
            bhrm_buffer --;
        }
        
        if(bholdrel(2) == 1)
        {
            bhrm_buffer = 3;
        }
        
        //heavy
        if(bhrh_buffer > 0)
        {
            bhrh_buffer --;
        }
        
        if(bholdrel(3) == 1)
        {
            bhrh_buffer = 3;
        }
    }
    
    //dash buffers
        if(ff_buffer > 0)
        {
            ff_buffer --;
        }
        
        if(dtap(6) == 1)
        {
            ff_buffer = 3;
        }
    
    //dash buffers
        if(bb_buffer > 0)
        {
            bb_buffer --;
        }
        
        if(dtap(4) == 1)
        {
            bb_buffer = 3;
        }
}

/*
//hold
if(controller_num > -1)
{
    if(player_num == 1)
    {
        key_light_hold	=				gamepad_button_check(controller_num, obj_system.key_light_1);
        key_heavy_hold	=				gamepad_button_check(controller_num, obj_system.key_heavy_1);
        key_medium_hold =				gamepad_button_check(controller_num, obj_system.key_medium_1);
        key_essence_hold =				gamepad_button_check(controller_num, obj_system.key_essence_1);
    }
    else
    {
        key_light_hold	=				gamepad_button_check(controller_num, obj_system.key_light_2);
        key_heavy_hold	=				gamepad_button_check(controller_num, obj_system.key_heavy_2);
        key_medium_hold =				gamepad_button_check(controller_num, obj_system.key_medium_2);
        key_essence_hold =				gamepad_button_check(controller_num, obj_system.key_essence_2);
    }
}
else
{
    key_light_hold	=				keyboard_check_pressed(obj_system.keyboard_light);
    key_heavy_hold	=				keyboard_check_pressed(obj_system.keyboard_heavy);
    key_medium_hold =				keyboard_check_pressed(obj_system.keyboard_medium);
    key_essence_hold =				keyboard_check_pressed(obj_system.keyboard_essence);
*/