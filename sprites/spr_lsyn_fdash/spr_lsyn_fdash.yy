{
    "id": "f0602997-f2c3-49f7-9aa1-9eadffc842be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lsyn_fdash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 45,
    "bbox_right": 76,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "38eccb85-753f-4d45-b477-34b4b5426337",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0602997-f2c3-49f7-9aa1-9eadffc842be",
            "compositeImage": {
                "id": "b4c8c065-a6ef-41e0-8906-94683c4b7821",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38eccb85-753f-4d45-b477-34b4b5426337",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec80e53d-8cae-459b-aa64-0501a1eec436",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38eccb85-753f-4d45-b477-34b4b5426337",
                    "LayerId": "0463b009-6e5d-4f4c-859b-c5f718fba1d8"
                }
            ]
        },
        {
            "id": "42955ab1-2202-4284-8f00-092f9f5f29c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0602997-f2c3-49f7-9aa1-9eadffc842be",
            "compositeImage": {
                "id": "7d28ff3f-6c65-4172-bf7d-cee381e5aa01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42955ab1-2202-4284-8f00-092f9f5f29c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47579968-3bf4-414c-b265-5643a2619068",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42955ab1-2202-4284-8f00-092f9f5f29c8",
                    "LayerId": "0463b009-6e5d-4f4c-859b-c5f718fba1d8"
                }
            ]
        },
        {
            "id": "2d0a7e2b-2404-4742-a0b8-015065ab44ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0602997-f2c3-49f7-9aa1-9eadffc842be",
            "compositeImage": {
                "id": "8a3109e3-2647-4e32-8a5e-9bce60ed719e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d0a7e2b-2404-4742-a0b8-015065ab44ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "201c18c1-93be-4a67-af12-b1f8926cd2c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d0a7e2b-2404-4742-a0b8-015065ab44ed",
                    "LayerId": "0463b009-6e5d-4f4c-859b-c5f718fba1d8"
                }
            ]
        },
        {
            "id": "0bbb9589-8bdf-4348-93bd-807311776671",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0602997-f2c3-49f7-9aa1-9eadffc842be",
            "compositeImage": {
                "id": "1cc11085-f234-48e2-90c3-17543a5c2090",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bbb9589-8bdf-4348-93bd-807311776671",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae56d067-ab0c-4957-953e-3cf88fce39fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bbb9589-8bdf-4348-93bd-807311776671",
                    "LayerId": "0463b009-6e5d-4f4c-859b-c5f718fba1d8"
                }
            ]
        },
        {
            "id": "c19044a2-9d9b-4c71-beab-6c90f78220bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0602997-f2c3-49f7-9aa1-9eadffc842be",
            "compositeImage": {
                "id": "98eae5e9-8af7-423a-bb55-f2fcadbbe416",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c19044a2-9d9b-4c71-beab-6c90f78220bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7ab39b3-0db0-4dbe-8eb6-648468e472cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c19044a2-9d9b-4c71-beab-6c90f78220bd",
                    "LayerId": "0463b009-6e5d-4f4c-859b-c5f718fba1d8"
                }
            ]
        },
        {
            "id": "64426dd6-88e1-49b9-8c17-c74c29fb48ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0602997-f2c3-49f7-9aa1-9eadffc842be",
            "compositeImage": {
                "id": "ce59a96a-6d2c-407d-be7b-7cc44a35183b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64426dd6-88e1-49b9-8c17-c74c29fb48ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "650081bc-40b2-48cc-b740-e8eb95f7ccf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64426dd6-88e1-49b9-8c17-c74c29fb48ed",
                    "LayerId": "0463b009-6e5d-4f4c-859b-c5f718fba1d8"
                }
            ]
        },
        {
            "id": "d6e78c41-f18a-4563-86be-5a502de01c96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0602997-f2c3-49f7-9aa1-9eadffc842be",
            "compositeImage": {
                "id": "63e06964-9f88-4f89-b804-6ef4f9a12be6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6e78c41-f18a-4563-86be-5a502de01c96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9ca2203-8b55-48a9-ac3b-cf94bab3ba6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6e78c41-f18a-4563-86be-5a502de01c96",
                    "LayerId": "0463b009-6e5d-4f4c-859b-c5f718fba1d8"
                }
            ]
        },
        {
            "id": "233206d9-da49-45ee-92d0-97de440d60bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0602997-f2c3-49f7-9aa1-9eadffc842be",
            "compositeImage": {
                "id": "e0b311c2-fc45-4940-8915-174baa4a8882",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "233206d9-da49-45ee-92d0-97de440d60bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "830f6453-ed36-47d4-ae84-a89788987ca8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "233206d9-da49-45ee-92d0-97de440d60bb",
                    "LayerId": "0463b009-6e5d-4f4c-859b-c5f718fba1d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "0463b009-6e5d-4f4c-859b-c5f718fba1d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f0602997-f2c3-49f7-9aa1-9eadffc842be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}