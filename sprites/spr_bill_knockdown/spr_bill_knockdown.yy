{
    "id": "3e24b72b-680b-40e4-8312-a1c337e1f491",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_knockdown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 38,
    "bbox_right": 117,
    "bbox_top": 89,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ee3d85b2-282f-4c89-a83c-d7d1c724bb8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e24b72b-680b-40e4-8312-a1c337e1f491",
            "compositeImage": {
                "id": "9cc15c0e-2899-49ba-b46d-454a822c3905",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee3d85b2-282f-4c89-a83c-d7d1c724bb8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3268158b-80a7-4db7-a401-2c64310c3a7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee3d85b2-282f-4c89-a83c-d7d1c724bb8f",
                    "LayerId": "f0a83d7e-5221-4600-bba5-db459273520b"
                }
            ]
        },
        {
            "id": "bfd3c62f-8956-4ad5-bcd1-8a61a69f4b6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e24b72b-680b-40e4-8312-a1c337e1f491",
            "compositeImage": {
                "id": "e1188fc3-cdef-4970-8375-9a103900b09b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfd3c62f-8956-4ad5-bcd1-8a61a69f4b6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43b9baf0-3572-4500-b0a7-aa4d3f9641d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfd3c62f-8956-4ad5-bcd1-8a61a69f4b6e",
                    "LayerId": "f0a83d7e-5221-4600-bba5-db459273520b"
                }
            ]
        },
        {
            "id": "83f749fa-25bd-4452-8914-7f305643f1b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e24b72b-680b-40e4-8312-a1c337e1f491",
            "compositeImage": {
                "id": "8fa2a141-cd56-4fcf-ba8b-d3d3e3ec49ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83f749fa-25bd-4452-8914-7f305643f1b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "053fdb7c-b562-4268-86f0-625915ded3c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83f749fa-25bd-4452-8914-7f305643f1b4",
                    "LayerId": "f0a83d7e-5221-4600-bba5-db459273520b"
                }
            ]
        },
        {
            "id": "f0ae25f1-a4b1-4375-a1da-f5315235d340",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e24b72b-680b-40e4-8312-a1c337e1f491",
            "compositeImage": {
                "id": "d1fe7faf-b704-41e9-8b29-8698338b875b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0ae25f1-a4b1-4375-a1da-f5315235d340",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e46767c0-f0b9-4b46-b6d0-d96397154d0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0ae25f1-a4b1-4375-a1da-f5315235d340",
                    "LayerId": "f0a83d7e-5221-4600-bba5-db459273520b"
                }
            ]
        },
        {
            "id": "3623a125-1b5e-4e9f-95e5-77ed55f16f37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e24b72b-680b-40e4-8312-a1c337e1f491",
            "compositeImage": {
                "id": "2d6cf38d-ea98-4a89-8aeb-fd1da89b81bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3623a125-1b5e-4e9f-95e5-77ed55f16f37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9b292fe-2063-40b0-af26-55184a0f9836",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3623a125-1b5e-4e9f-95e5-77ed55f16f37",
                    "LayerId": "f0a83d7e-5221-4600-bba5-db459273520b"
                }
            ]
        },
        {
            "id": "57a99da2-0a40-4976-afcf-218a9b9129b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e24b72b-680b-40e4-8312-a1c337e1f491",
            "compositeImage": {
                "id": "830487bd-d954-4345-b66f-4539617967d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57a99da2-0a40-4976-afcf-218a9b9129b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e71b920f-e8ad-4007-a45d-eb74b269a097",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57a99da2-0a40-4976-afcf-218a9b9129b5",
                    "LayerId": "f0a83d7e-5221-4600-bba5-db459273520b"
                }
            ]
        },
        {
            "id": "aaa8b5c1-28fd-41e7-bcb7-b4fee83986a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e24b72b-680b-40e4-8312-a1c337e1f491",
            "compositeImage": {
                "id": "81841f08-7e33-4c23-a224-8c93093ef727",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aaa8b5c1-28fd-41e7-bcb7-b4fee83986a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94cafa63-418e-4e75-8a56-428d22bc593b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aaa8b5c1-28fd-41e7-bcb7-b4fee83986a7",
                    "LayerId": "f0a83d7e-5221-4600-bba5-db459273520b"
                }
            ]
        },
        {
            "id": "dc9304de-19c9-4fbc-bf06-be8ad05a22f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e24b72b-680b-40e4-8312-a1c337e1f491",
            "compositeImage": {
                "id": "8cc4f2b3-8f50-4860-af25-0878aa05c284",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc9304de-19c9-4fbc-bf06-be8ad05a22f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1eb7b351-92a8-4f9d-b0c2-39ff0f3176f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc9304de-19c9-4fbc-bf06-be8ad05a22f8",
                    "LayerId": "f0a83d7e-5221-4600-bba5-db459273520b"
                }
            ]
        },
        {
            "id": "cfb583be-9997-48d7-8c98-a66ec01db971",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e24b72b-680b-40e4-8312-a1c337e1f491",
            "compositeImage": {
                "id": "8845ad46-6832-4492-af96-2053ec9c5757",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfb583be-9997-48d7-8c98-a66ec01db971",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db6ff046-5d9f-424e-bcab-30b140cf9460",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfb583be-9997-48d7-8c98-a66ec01db971",
                    "LayerId": "f0a83d7e-5221-4600-bba5-db459273520b"
                }
            ]
        },
        {
            "id": "192bb119-d31b-4961-aae0-68e8723fced2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e24b72b-680b-40e4-8312-a1c337e1f491",
            "compositeImage": {
                "id": "e60a0b00-d866-484c-af97-05a803aa2ef6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "192bb119-d31b-4961-aae0-68e8723fced2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "083945a8-ab55-4b7e-a3d0-4f61b481264e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "192bb119-d31b-4961-aae0-68e8723fced2",
                    "LayerId": "f0a83d7e-5221-4600-bba5-db459273520b"
                }
            ]
        },
        {
            "id": "c7dfc85f-b907-4c04-8c63-a40d791ba7ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e24b72b-680b-40e4-8312-a1c337e1f491",
            "compositeImage": {
                "id": "5afbad26-4d2f-4048-97e5-82f2c4d21f92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7dfc85f-b907-4c04-8c63-a40d791ba7ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c07696c9-a557-4e56-962c-bd23b3fca82c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7dfc85f-b907-4c04-8c63-a40d791ba7ca",
                    "LayerId": "f0a83d7e-5221-4600-bba5-db459273520b"
                }
            ]
        },
        {
            "id": "c55b245c-c64c-4787-b12f-8ba2e51cd507",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e24b72b-680b-40e4-8312-a1c337e1f491",
            "compositeImage": {
                "id": "563c9bab-9a19-419b-bad0-7f1a1c2a1b26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c55b245c-c64c-4787-b12f-8ba2e51cd507",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "565e9302-3add-45da-91e8-fefde95d915f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c55b245c-c64c-4787-b12f-8ba2e51cd507",
                    "LayerId": "f0a83d7e-5221-4600-bba5-db459273520b"
                }
            ]
        },
        {
            "id": "f1869f86-94dd-4c88-a3f1-969b32815ca2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e24b72b-680b-40e4-8312-a1c337e1f491",
            "compositeImage": {
                "id": "a879fd6d-bf44-41e6-8389-9e2ca13388f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1869f86-94dd-4c88-a3f1-969b32815ca2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26621e53-16f2-49db-b819-916d08fc0912",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1869f86-94dd-4c88-a3f1-969b32815ca2",
                    "LayerId": "f0a83d7e-5221-4600-bba5-db459273520b"
                }
            ]
        },
        {
            "id": "30aa9264-80a8-4af3-9099-a7fe82aac198",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e24b72b-680b-40e4-8312-a1c337e1f491",
            "compositeImage": {
                "id": "1840b05c-c39d-4acb-915d-834c4b4edc3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30aa9264-80a8-4af3-9099-a7fe82aac198",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cb530d1-2675-4a89-820d-dbeb9b2c567e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30aa9264-80a8-4af3-9099-a7fe82aac198",
                    "LayerId": "f0a83d7e-5221-4600-bba5-db459273520b"
                }
            ]
        },
        {
            "id": "0dc81d86-34b4-49f4-bb39-1f25fc0ae6be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e24b72b-680b-40e4-8312-a1c337e1f491",
            "compositeImage": {
                "id": "6227e564-c9de-4fc2-9b3e-2c60fab42da0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dc81d86-34b4-49f4-bb39-1f25fc0ae6be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f026cc03-77ea-44f8-889e-c5e69a035639",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dc81d86-34b4-49f4-bb39-1f25fc0ae6be",
                    "LayerId": "f0a83d7e-5221-4600-bba5-db459273520b"
                }
            ]
        },
        {
            "id": "a3084f16-8b26-4464-8ed2-cea06bede845",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e24b72b-680b-40e4-8312-a1c337e1f491",
            "compositeImage": {
                "id": "8b6eb489-0bb6-46d1-82c7-83d795ec3f71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3084f16-8b26-4464-8ed2-cea06bede845",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e38b6664-649c-41b7-bec4-dd042c9dfbf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3084f16-8b26-4464-8ed2-cea06bede845",
                    "LayerId": "f0a83d7e-5221-4600-bba5-db459273520b"
                }
            ]
        },
        {
            "id": "1ba29258-2642-4342-bf79-8eb7c47b4de7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e24b72b-680b-40e4-8312-a1c337e1f491",
            "compositeImage": {
                "id": "ab623411-5a8a-44bf-964c-ecca7dc5fbbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ba29258-2642-4342-bf79-8eb7c47b4de7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65bd295b-6df7-4ed8-afc4-5937b5ea8309",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ba29258-2642-4342-bf79-8eb7c47b4de7",
                    "LayerId": "f0a83d7e-5221-4600-bba5-db459273520b"
                }
            ]
        },
        {
            "id": "6382cd15-593f-4ab1-8f27-60bc83bab324",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e24b72b-680b-40e4-8312-a1c337e1f491",
            "compositeImage": {
                "id": "a7aa52ed-218b-4cf7-8687-03ed3b0384ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6382cd15-593f-4ab1-8f27-60bc83bab324",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bb573b0-c1d5-45b5-b575-e99b28a9619e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6382cd15-593f-4ab1-8f27-60bc83bab324",
                    "LayerId": "f0a83d7e-5221-4600-bba5-db459273520b"
                }
            ]
        },
        {
            "id": "6541681b-8cc1-48bd-8a5c-c6cd91228095",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e24b72b-680b-40e4-8312-a1c337e1f491",
            "compositeImage": {
                "id": "f597530a-48f9-43e3-bd3c-4d53fed21f21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6541681b-8cc1-48bd-8a5c-c6cd91228095",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "663c4139-0fd0-4af5-83b0-446f8d85134c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6541681b-8cc1-48bd-8a5c-c6cd91228095",
                    "LayerId": "f0a83d7e-5221-4600-bba5-db459273520b"
                }
            ]
        },
        {
            "id": "2b30b546-d272-448d-852b-833dc9a9d3d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e24b72b-680b-40e4-8312-a1c337e1f491",
            "compositeImage": {
                "id": "e080764c-2b3c-4e73-916d-69553fe3a896",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b30b546-d272-448d-852b-833dc9a9d3d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "507572bc-10aa-4470-b823-00add7627930",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b30b546-d272-448d-852b-833dc9a9d3d4",
                    "LayerId": "f0a83d7e-5221-4600-bba5-db459273520b"
                }
            ]
        },
        {
            "id": "062afbbe-9bdc-48b7-8095-dae38ba1ff01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e24b72b-680b-40e4-8312-a1c337e1f491",
            "compositeImage": {
                "id": "cae49b99-116b-497d-bff4-c41c503b78ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "062afbbe-9bdc-48b7-8095-dae38ba1ff01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c9d55e0-dda2-4740-a58e-94229d67986b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "062afbbe-9bdc-48b7-8095-dae38ba1ff01",
                    "LayerId": "f0a83d7e-5221-4600-bba5-db459273520b"
                }
            ]
        },
        {
            "id": "e3318b87-2ed2-449a-a4fe-3a004d6b7455",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e24b72b-680b-40e4-8312-a1c337e1f491",
            "compositeImage": {
                "id": "7ca04200-41c5-4b68-b6eb-7ec53d708580",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3318b87-2ed2-449a-a4fe-3a004d6b7455",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01622491-b572-43ed-9911-3b35de031ca3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3318b87-2ed2-449a-a4fe-3a004d6b7455",
                    "LayerId": "f0a83d7e-5221-4600-bba5-db459273520b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "f0a83d7e-5221-4600-bba5-db459273520b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e24b72b-680b-40e4-8312-a1c337e1f491",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 70,
    "yorig": 118
}