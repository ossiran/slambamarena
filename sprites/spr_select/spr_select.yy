{
    "id": "0b198bc8-12db-4cbc-874d-c56a6a1d078d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_select",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "742524bf-04ae-44e7-a426-376b0d2bf0d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b198bc8-12db-4cbc-874d-c56a6a1d078d",
            "compositeImage": {
                "id": "9282aeba-c3cc-4e01-a65c-f8d108c52c78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "742524bf-04ae-44e7-a426-376b0d2bf0d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "220a7861-e6e4-4add-b44f-84d2bf4aa96c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "742524bf-04ae-44e7-a426-376b0d2bf0d6",
                    "LayerId": "e394eb00-b735-4dcd-99b5-7a4cf5ae3524"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "e394eb00-b735-4dcd-99b5-7a4cf5ae3524",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b198bc8-12db-4cbc-874d-c56a6a1d078d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 0,
    "yorig": 0
}