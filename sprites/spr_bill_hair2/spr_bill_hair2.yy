{
    "id": "c80281fb-646d-4446-a00e-3ddbbbda6b41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_hair2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 148,
    "bbox_left": 43,
    "bbox_right": 108,
    "bbox_top": 66,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b3236ec1-7654-431b-b3d6-8310a059522c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80281fb-646d-4446-a00e-3ddbbbda6b41",
            "compositeImage": {
                "id": "cad751e3-dea2-4dfa-ab4f-54a5d39c57e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3236ec1-7654-431b-b3d6-8310a059522c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18028eb5-73cd-4538-9a8d-d6f6275067e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3236ec1-7654-431b-b3d6-8310a059522c",
                    "LayerId": "b300204c-2275-4698-b7a0-0d7391453172"
                }
            ]
        },
        {
            "id": "e3fe97bb-aefb-4c6c-b28a-e4a591c22f95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80281fb-646d-4446-a00e-3ddbbbda6b41",
            "compositeImage": {
                "id": "cd07945a-0f32-4965-bbab-50b061d0d4d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3fe97bb-aefb-4c6c-b28a-e4a591c22f95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b32a44d2-644b-4b03-82ec-5634a11512c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3fe97bb-aefb-4c6c-b28a-e4a591c22f95",
                    "LayerId": "b300204c-2275-4698-b7a0-0d7391453172"
                }
            ]
        },
        {
            "id": "39f17630-6271-41f4-aee1-c243bc9ecd10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80281fb-646d-4446-a00e-3ddbbbda6b41",
            "compositeImage": {
                "id": "0fc544b3-0306-4af5-9660-d74ebc7c9a61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39f17630-6271-41f4-aee1-c243bc9ecd10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cab08088-e8af-4a59-a2ab-97644f3af5cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39f17630-6271-41f4-aee1-c243bc9ecd10",
                    "LayerId": "b300204c-2275-4698-b7a0-0d7391453172"
                }
            ]
        },
        {
            "id": "16cf2c58-e025-45ec-b79c-abac49ce4a60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80281fb-646d-4446-a00e-3ddbbbda6b41",
            "compositeImage": {
                "id": "21e0c106-13a4-45ec-9e99-ccfd27b2e2c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16cf2c58-e025-45ec-b79c-abac49ce4a60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef20057a-ec79-4c82-957e-4d53c7732c8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16cf2c58-e025-45ec-b79c-abac49ce4a60",
                    "LayerId": "b300204c-2275-4698-b7a0-0d7391453172"
                }
            ]
        },
        {
            "id": "2c7f11fe-aeb3-4b76-ba2c-4863dbfb6797",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80281fb-646d-4446-a00e-3ddbbbda6b41",
            "compositeImage": {
                "id": "55d4503f-08a8-4511-87ce-063ea05aa01f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c7f11fe-aeb3-4b76-ba2c-4863dbfb6797",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5ce13c7-edeb-4729-9cc9-c61715db4e2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c7f11fe-aeb3-4b76-ba2c-4863dbfb6797",
                    "LayerId": "b300204c-2275-4698-b7a0-0d7391453172"
                }
            ]
        },
        {
            "id": "b14c9bf2-01a9-416d-9e5b-2664fb1e8c19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80281fb-646d-4446-a00e-3ddbbbda6b41",
            "compositeImage": {
                "id": "337e1b41-b104-48a4-aa07-a8ab0160b425",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b14c9bf2-01a9-416d-9e5b-2664fb1e8c19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c38c890-74f3-4319-b773-79b1917199ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b14c9bf2-01a9-416d-9e5b-2664fb1e8c19",
                    "LayerId": "b300204c-2275-4698-b7a0-0d7391453172"
                }
            ]
        },
        {
            "id": "2c27fb4c-8310-438e-84f2-e41d38786ee2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80281fb-646d-4446-a00e-3ddbbbda6b41",
            "compositeImage": {
                "id": "772bd692-ea40-4555-875d-883c9fcc81a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c27fb4c-8310-438e-84f2-e41d38786ee2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4e47ec0-b179-42c6-9f1a-5c2957e1d5a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c27fb4c-8310-438e-84f2-e41d38786ee2",
                    "LayerId": "b300204c-2275-4698-b7a0-0d7391453172"
                }
            ]
        },
        {
            "id": "e96185cc-3eab-4700-a9c2-405464e33e02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80281fb-646d-4446-a00e-3ddbbbda6b41",
            "compositeImage": {
                "id": "8108a56a-b8f1-4523-952c-f34320abfc7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e96185cc-3eab-4700-a9c2-405464e33e02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a617930-371c-4c27-ae78-05c670877e33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e96185cc-3eab-4700-a9c2-405464e33e02",
                    "LayerId": "b300204c-2275-4698-b7a0-0d7391453172"
                }
            ]
        },
        {
            "id": "d59d7d15-da72-4ee8-8649-2676c1936848",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80281fb-646d-4446-a00e-3ddbbbda6b41",
            "compositeImage": {
                "id": "e38d2346-277f-446f-a913-1dd3683c27e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d59d7d15-da72-4ee8-8649-2676c1936848",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2f04f6d-d32d-4ccc-8e21-c0d468870fa9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d59d7d15-da72-4ee8-8649-2676c1936848",
                    "LayerId": "b300204c-2275-4698-b7a0-0d7391453172"
                }
            ]
        },
        {
            "id": "4a80e62c-061e-482b-a61f-4286e28da3ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80281fb-646d-4446-a00e-3ddbbbda6b41",
            "compositeImage": {
                "id": "b2d0cea2-65ec-4438-bdb0-f28b7933d1d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a80e62c-061e-482b-a61f-4286e28da3ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "856a4fa0-637b-441e-882a-c1b0f1f2eaef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a80e62c-061e-482b-a61f-4286e28da3ff",
                    "LayerId": "b300204c-2275-4698-b7a0-0d7391453172"
                }
            ]
        },
        {
            "id": "c8f14280-08aa-4e30-a414-c5053d2424bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80281fb-646d-4446-a00e-3ddbbbda6b41",
            "compositeImage": {
                "id": "8321f551-a196-4152-b193-223807e1b7c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8f14280-08aa-4e30-a414-c5053d2424bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59a45045-3e47-4bc9-9336-dc3549333993",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8f14280-08aa-4e30-a414-c5053d2424bd",
                    "LayerId": "b300204c-2275-4698-b7a0-0d7391453172"
                }
            ]
        },
        {
            "id": "3778966f-c623-4635-99c4-78a4a30962c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80281fb-646d-4446-a00e-3ddbbbda6b41",
            "compositeImage": {
                "id": "35e976dd-b472-4c12-9eae-ab66fbfe6bb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3778966f-c623-4635-99c4-78a4a30962c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5052a9b3-17e1-41ed-b058-5a57748653b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3778966f-c623-4635-99c4-78a4a30962c7",
                    "LayerId": "b300204c-2275-4698-b7a0-0d7391453172"
                }
            ]
        },
        {
            "id": "d6f8eb64-3d96-44ab-a489-4f1c78abf347",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80281fb-646d-4446-a00e-3ddbbbda6b41",
            "compositeImage": {
                "id": "b2417eb8-8e44-42c4-9323-e20cbc162f5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6f8eb64-3d96-44ab-a489-4f1c78abf347",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0bf28cb-972d-4580-8ced-728b99e0cc18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6f8eb64-3d96-44ab-a489-4f1c78abf347",
                    "LayerId": "b300204c-2275-4698-b7a0-0d7391453172"
                }
            ]
        },
        {
            "id": "55dd8935-4110-4e0a-b159-8f4cda74cc8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80281fb-646d-4446-a00e-3ddbbbda6b41",
            "compositeImage": {
                "id": "c7516a37-17e3-4493-a1c4-0e5b484fb743",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55dd8935-4110-4e0a-b159-8f4cda74cc8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e19cee8-e41f-4968-98a7-be8d52a6f093",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55dd8935-4110-4e0a-b159-8f4cda74cc8a",
                    "LayerId": "b300204c-2275-4698-b7a0-0d7391453172"
                }
            ]
        },
        {
            "id": "ed9b2387-162b-4c5f-a93a-ec98ad3d7a6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80281fb-646d-4446-a00e-3ddbbbda6b41",
            "compositeImage": {
                "id": "ad7648c8-6b84-4399-b79d-4e80d4f870b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed9b2387-162b-4c5f-a93a-ec98ad3d7a6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f9cff45-809d-4bfc-aeed-66455fa4f119",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed9b2387-162b-4c5f-a93a-ec98ad3d7a6d",
                    "LayerId": "b300204c-2275-4698-b7a0-0d7391453172"
                }
            ]
        },
        {
            "id": "68aa9fea-71bd-4441-bb26-771abd9a54fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80281fb-646d-4446-a00e-3ddbbbda6b41",
            "compositeImage": {
                "id": "1042c566-77df-4f21-b793-d5a6bf68c6f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68aa9fea-71bd-4441-bb26-771abd9a54fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2231dee-8635-4aea-9a8d-a8e6a5a4df59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68aa9fea-71bd-4441-bb26-771abd9a54fd",
                    "LayerId": "b300204c-2275-4698-b7a0-0d7391453172"
                }
            ]
        },
        {
            "id": "eb633d99-d955-4e4d-bc10-d36871b2cd06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80281fb-646d-4446-a00e-3ddbbbda6b41",
            "compositeImage": {
                "id": "a6c6fa93-4cda-49f7-996b-07e999beb158",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb633d99-d955-4e4d-bc10-d36871b2cd06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e12fed31-1bee-403f-a1eb-cf4e195607cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb633d99-d955-4e4d-bc10-d36871b2cd06",
                    "LayerId": "b300204c-2275-4698-b7a0-0d7391453172"
                }
            ]
        },
        {
            "id": "65cb2100-97c5-4c97-81be-56316c0773c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80281fb-646d-4446-a00e-3ddbbbda6b41",
            "compositeImage": {
                "id": "f459eb86-e33a-4846-ab33-0a0dc2b0583a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65cb2100-97c5-4c97-81be-56316c0773c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb684ba4-47e0-4d71-89b8-3566094c394f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65cb2100-97c5-4c97-81be-56316c0773c7",
                    "LayerId": "b300204c-2275-4698-b7a0-0d7391453172"
                }
            ]
        },
        {
            "id": "3203ecaf-9ce9-4ae3-8192-90e36f97c732",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80281fb-646d-4446-a00e-3ddbbbda6b41",
            "compositeImage": {
                "id": "2a030081-7924-4e0e-853f-d34e2bb02123",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3203ecaf-9ce9-4ae3-8192-90e36f97c732",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65a6a6ff-fd7c-4071-b49c-0c86d829b3cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3203ecaf-9ce9-4ae3-8192-90e36f97c732",
                    "LayerId": "b300204c-2275-4698-b7a0-0d7391453172"
                }
            ]
        },
        {
            "id": "5c4e8cf5-2ecf-4435-ad10-79ad4554db22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80281fb-646d-4446-a00e-3ddbbbda6b41",
            "compositeImage": {
                "id": "e49e01a8-cdaa-4ace-8b37-174e6b4546e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c4e8cf5-2ecf-4435-ad10-79ad4554db22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94ff72fb-9593-4d01-a0dc-fa5326b593a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c4e8cf5-2ecf-4435-ad10-79ad4554db22",
                    "LayerId": "b300204c-2275-4698-b7a0-0d7391453172"
                }
            ]
        },
        {
            "id": "e1b571ac-6f22-4b93-80d6-8c0a0e33d056",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80281fb-646d-4446-a00e-3ddbbbda6b41",
            "compositeImage": {
                "id": "2737bd99-e78f-4d0c-9d98-916f38fb4178",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1b571ac-6f22-4b93-80d6-8c0a0e33d056",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "979da9e8-82a6-4b12-a8c8-24cf3d0da7cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1b571ac-6f22-4b93-80d6-8c0a0e33d056",
                    "LayerId": "b300204c-2275-4698-b7a0-0d7391453172"
                }
            ]
        },
        {
            "id": "15ef9bff-4782-469c-9a87-0e912596e766",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80281fb-646d-4446-a00e-3ddbbbda6b41",
            "compositeImage": {
                "id": "1352dd23-30f8-4d5f-a3c6-29b5e36f94ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15ef9bff-4782-469c-9a87-0e912596e766",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ae566c2-c6b5-4dfb-bd09-b4b39b7b5bd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15ef9bff-4782-469c-9a87-0e912596e766",
                    "LayerId": "b300204c-2275-4698-b7a0-0d7391453172"
                }
            ]
        },
        {
            "id": "50bf164b-8260-4d6f-913f-7ec3a466482c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80281fb-646d-4446-a00e-3ddbbbda6b41",
            "compositeImage": {
                "id": "a8f1af4d-5283-40b3-984b-54966710ff33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50bf164b-8260-4d6f-913f-7ec3a466482c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05aada9f-dbe5-40a4-8c08-77a43419e3d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50bf164b-8260-4d6f-913f-7ec3a466482c",
                    "LayerId": "b300204c-2275-4698-b7a0-0d7391453172"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "b300204c-2275-4698-b7a0-0d7391453172",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c80281fb-646d-4446-a00e-3ddbbbda6b41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}