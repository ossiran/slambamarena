{
    "id": "21430bdf-7528-4290-b662-7289e8680327",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_hitfall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 52,
    "bbox_right": 97,
    "bbox_top": 81,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b24fc373-5f6b-471d-8ffc-3b3d925c2024",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21430bdf-7528-4290-b662-7289e8680327",
            "compositeImage": {
                "id": "d32f7e1d-7626-497e-a24e-d1df2eb03a99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b24fc373-5f6b-471d-8ffc-3b3d925c2024",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd7fa24b-b2ad-4d25-bccf-f13b6af04f48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b24fc373-5f6b-471d-8ffc-3b3d925c2024",
                    "LayerId": "8227d69f-3bbc-43ff-bc51-82338a830bac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "8227d69f-3bbc-43ff-bc51-82338a830bac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21430bdf-7528-4290-b662-7289e8680327",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}