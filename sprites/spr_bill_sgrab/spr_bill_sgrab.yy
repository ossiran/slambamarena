{
    "id": "71d2ef62-e1cb-4340-a2fd-f73b93866d8e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_sgrab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 50,
    "bbox_right": 107,
    "bbox_top": 75,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9eecae28-9b00-40da-b90b-a710e5de7f64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71d2ef62-e1cb-4340-a2fd-f73b93866d8e",
            "compositeImage": {
                "id": "da0de903-53c5-4653-8567-e5e5698af002",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9eecae28-9b00-40da-b90b-a710e5de7f64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e55b8d26-65ee-4f60-9b8f-fcfc464f604e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9eecae28-9b00-40da-b90b-a710e5de7f64",
                    "LayerId": "0c58201b-dd98-463c-87df-7b47f333418d"
                }
            ]
        },
        {
            "id": "9eb42bf8-6023-4ffa-9b6c-86720680f896",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71d2ef62-e1cb-4340-a2fd-f73b93866d8e",
            "compositeImage": {
                "id": "1362f5e8-cdcd-4d23-aaa9-64079ef1f0cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9eb42bf8-6023-4ffa-9b6c-86720680f896",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "478ec09a-184c-416d-87af-aa03d8b1dfde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9eb42bf8-6023-4ffa-9b6c-86720680f896",
                    "LayerId": "0c58201b-dd98-463c-87df-7b47f333418d"
                }
            ]
        },
        {
            "id": "db7e7823-9156-4218-b00e-5083d5c54f8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71d2ef62-e1cb-4340-a2fd-f73b93866d8e",
            "compositeImage": {
                "id": "17203a73-6d98-40e4-8a42-f18107b92c8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db7e7823-9156-4218-b00e-5083d5c54f8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "731dd9b3-a701-4ff2-a1fe-e623562cc9b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db7e7823-9156-4218-b00e-5083d5c54f8c",
                    "LayerId": "0c58201b-dd98-463c-87df-7b47f333418d"
                }
            ]
        },
        {
            "id": "cf11bfd5-503a-4e5b-8fd6-6ed035dc8929",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71d2ef62-e1cb-4340-a2fd-f73b93866d8e",
            "compositeImage": {
                "id": "96747fa4-965b-4c72-b897-779096ae0705",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf11bfd5-503a-4e5b-8fd6-6ed035dc8929",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "206858d3-88b4-46d4-af41-b310180b997d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf11bfd5-503a-4e5b-8fd6-6ed035dc8929",
                    "LayerId": "0c58201b-dd98-463c-87df-7b47f333418d"
                }
            ]
        },
        {
            "id": "254433d5-be64-44f0-99e4-5b8036098665",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71d2ef62-e1cb-4340-a2fd-f73b93866d8e",
            "compositeImage": {
                "id": "7c924846-e1a8-4835-8532-f020744743c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "254433d5-be64-44f0-99e4-5b8036098665",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f562e7f-911c-472f-a4d1-0a74cbac811e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "254433d5-be64-44f0-99e4-5b8036098665",
                    "LayerId": "0c58201b-dd98-463c-87df-7b47f333418d"
                }
            ]
        },
        {
            "id": "7d5eae24-9f44-429a-b0ab-ece2bc1d89c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71d2ef62-e1cb-4340-a2fd-f73b93866d8e",
            "compositeImage": {
                "id": "97b657da-9e7e-42c9-903d-e4c1255b06d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d5eae24-9f44-429a-b0ab-ece2bc1d89c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3242d55d-e9b1-4597-a321-37db56e49481",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d5eae24-9f44-429a-b0ab-ece2bc1d89c3",
                    "LayerId": "0c58201b-dd98-463c-87df-7b47f333418d"
                }
            ]
        },
        {
            "id": "d2861e63-2f4f-4a22-bff8-ef0c2f87239e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71d2ef62-e1cb-4340-a2fd-f73b93866d8e",
            "compositeImage": {
                "id": "9e9f208a-e1e1-4731-bb5e-719dab12d2e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2861e63-2f4f-4a22-bff8-ef0c2f87239e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5bf36b1-1817-479e-9fde-36d5c489e54b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2861e63-2f4f-4a22-bff8-ef0c2f87239e",
                    "LayerId": "0c58201b-dd98-463c-87df-7b47f333418d"
                }
            ]
        },
        {
            "id": "f77a4620-2c70-4cb0-a27e-7093cd05a2af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71d2ef62-e1cb-4340-a2fd-f73b93866d8e",
            "compositeImage": {
                "id": "9ee6534a-f8cd-46c8-ac56-1516339aeb44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f77a4620-2c70-4cb0-a27e-7093cd05a2af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6796f258-d8c7-47f0-a3f3-9ceadbafffc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f77a4620-2c70-4cb0-a27e-7093cd05a2af",
                    "LayerId": "0c58201b-dd98-463c-87df-7b47f333418d"
                }
            ]
        },
        {
            "id": "86566d3c-4167-472a-a0d6-751a85613b6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71d2ef62-e1cb-4340-a2fd-f73b93866d8e",
            "compositeImage": {
                "id": "b0e96a05-09fc-4959-bada-90f77f698635",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86566d3c-4167-472a-a0d6-751a85613b6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "351949cf-4858-4dcf-a66d-cea4474c3301",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86566d3c-4167-472a-a0d6-751a85613b6f",
                    "LayerId": "0c58201b-dd98-463c-87df-7b47f333418d"
                }
            ]
        },
        {
            "id": "84b1e8dc-9e10-44eb-bc1e-e02e01a919bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71d2ef62-e1cb-4340-a2fd-f73b93866d8e",
            "compositeImage": {
                "id": "4c196bdf-a260-433c-9542-b09d0eabee22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84b1e8dc-9e10-44eb-bc1e-e02e01a919bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a944626b-c530-46c1-8677-72d3bf5290e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84b1e8dc-9e10-44eb-bc1e-e02e01a919bd",
                    "LayerId": "0c58201b-dd98-463c-87df-7b47f333418d"
                }
            ]
        },
        {
            "id": "9f8cda68-efd6-4276-a664-889c95c6a510",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71d2ef62-e1cb-4340-a2fd-f73b93866d8e",
            "compositeImage": {
                "id": "836f2b27-b1ad-4680-a065-3c9dd219c34d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f8cda68-efd6-4276-a664-889c95c6a510",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bea9b0d-f22d-4513-8584-cbc5547a2551",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f8cda68-efd6-4276-a664-889c95c6a510",
                    "LayerId": "0c58201b-dd98-463c-87df-7b47f333418d"
                }
            ]
        },
        {
            "id": "ac9bedfa-ab4b-494f-a819-1076de797bcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71d2ef62-e1cb-4340-a2fd-f73b93866d8e",
            "compositeImage": {
                "id": "dab77be5-2cc7-4a61-b88f-0249a7ea8574",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac9bedfa-ab4b-494f-a819-1076de797bcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21cdfbf7-5bee-42b2-8100-ddfce8016404",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac9bedfa-ab4b-494f-a819-1076de797bcb",
                    "LayerId": "0c58201b-dd98-463c-87df-7b47f333418d"
                }
            ]
        },
        {
            "id": "35148247-2204-4649-9341-cb6ef7010d10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71d2ef62-e1cb-4340-a2fd-f73b93866d8e",
            "compositeImage": {
                "id": "b4e3f9b4-fa91-4cda-8fd9-74698655b0df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35148247-2204-4649-9341-cb6ef7010d10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e7f1426-a08b-4610-a6bf-630daa4666ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35148247-2204-4649-9341-cb6ef7010d10",
                    "LayerId": "0c58201b-dd98-463c-87df-7b47f333418d"
                }
            ]
        },
        {
            "id": "52418345-a7e5-45be-9258-0871386cd8ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71d2ef62-e1cb-4340-a2fd-f73b93866d8e",
            "compositeImage": {
                "id": "69414d26-ef10-472f-997c-3cbd6a1aa26f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52418345-a7e5-45be-9258-0871386cd8ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df29ee2c-8433-4056-94de-30a852b3f4e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52418345-a7e5-45be-9258-0871386cd8ff",
                    "LayerId": "0c58201b-dd98-463c-87df-7b47f333418d"
                }
            ]
        },
        {
            "id": "9f9a858f-f0b2-47ac-9f7c-393d08fa2def",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71d2ef62-e1cb-4340-a2fd-f73b93866d8e",
            "compositeImage": {
                "id": "b6f12958-88b8-402a-b0e0-d6f2fe747ad4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f9a858f-f0b2-47ac-9f7c-393d08fa2def",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fba8fca2-16c3-4021-8b28-24121eed7f71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f9a858f-f0b2-47ac-9f7c-393d08fa2def",
                    "LayerId": "0c58201b-dd98-463c-87df-7b47f333418d"
                }
            ]
        },
        {
            "id": "cd67d559-312d-405f-b89a-67ef31b52757",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71d2ef62-e1cb-4340-a2fd-f73b93866d8e",
            "compositeImage": {
                "id": "729ba20c-3487-49f5-b684-8c4806bf5864",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd67d559-312d-405f-b89a-67ef31b52757",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc1c0c6d-738c-4ff5-b398-dac42af98a51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd67d559-312d-405f-b89a-67ef31b52757",
                    "LayerId": "0c58201b-dd98-463c-87df-7b47f333418d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "0c58201b-dd98-463c-87df-7b47f333418d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "71d2ef62-e1cb-4340-a2fd-f73b93866d8e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}