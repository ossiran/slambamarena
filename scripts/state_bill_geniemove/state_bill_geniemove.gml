if(state_new == 1)
{
    with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -43;
	    image_xscale  = 26;
	    image_yscale  = 75;
	    xshift  = -2;
	}
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 16;
	    image_yscale  = 47;
	    xshift = -2
	}
}

vx = 0;
vy = 0;

if(genieobj = -1)
{
    state_switch_previous();
}

if(hitstun > 0)
{
    state_switch("hitstun");   
}