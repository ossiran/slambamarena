{
    "id": "4301a530-c551-4982-bb12-cdc70b0b4bad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lsyn_falling",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 39,
    "bbox_right": 84,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "665b59e3-fb37-4958-b141-90699136c8fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4301a530-c551-4982-bb12-cdc70b0b4bad",
            "compositeImage": {
                "id": "e1290bfd-7dc6-4ae0-a876-97254ae775ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "665b59e3-fb37-4958-b141-90699136c8fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89803866-60b5-4a05-a6b3-02c3a85e6a6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "665b59e3-fb37-4958-b141-90699136c8fa",
                    "LayerId": "a71bf9e5-79bf-411b-a0cf-954f5def9033"
                }
            ]
        },
        {
            "id": "b2709ec2-edba-4008-ab2e-c107a92de07d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4301a530-c551-4982-bb12-cdc70b0b4bad",
            "compositeImage": {
                "id": "bb0312d4-6b0a-48ed-ace9-34823fac0576",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2709ec2-edba-4008-ab2e-c107a92de07d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6f8a301-0588-462b-93a5-07311ddfce10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2709ec2-edba-4008-ab2e-c107a92de07d",
                    "LayerId": "a71bf9e5-79bf-411b-a0cf-954f5def9033"
                }
            ]
        },
        {
            "id": "63f01d51-eab0-419a-b13c-d2a137afc591",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4301a530-c551-4982-bb12-cdc70b0b4bad",
            "compositeImage": {
                "id": "a7311beb-9e79-4e40-b729-c7b85d9ff58d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63f01d51-eab0-419a-b13c-d2a137afc591",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "979239c5-ff81-4b15-bb16-18c128ee5226",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63f01d51-eab0-419a-b13c-d2a137afc591",
                    "LayerId": "a71bf9e5-79bf-411b-a0cf-954f5def9033"
                }
            ]
        },
        {
            "id": "cc5ce7fb-d670-4681-b67f-7960eaff0857",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4301a530-c551-4982-bb12-cdc70b0b4bad",
            "compositeImage": {
                "id": "e7e7ed41-ed6c-424f-b70d-22a38fa3c8ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc5ce7fb-d670-4681-b67f-7960eaff0857",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cafe535-4d60-429f-a14f-c3c9215f3a10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc5ce7fb-d670-4681-b67f-7960eaff0857",
                    "LayerId": "a71bf9e5-79bf-411b-a0cf-954f5def9033"
                }
            ]
        },
        {
            "id": "b6c79503-0859-4534-990a-18caaff41268",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4301a530-c551-4982-bb12-cdc70b0b4bad",
            "compositeImage": {
                "id": "e9861a2d-d3de-4763-924d-e563d674694c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6c79503-0859-4534-990a-18caaff41268",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbdf8b4c-aa6f-46a2-98f6-1d6333ef0781",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6c79503-0859-4534-990a-18caaff41268",
                    "LayerId": "a71bf9e5-79bf-411b-a0cf-954f5def9033"
                }
            ]
        },
        {
            "id": "7650869f-604f-42fc-9c4c-2082dbf63031",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4301a530-c551-4982-bb12-cdc70b0b4bad",
            "compositeImage": {
                "id": "1a2c4735-7103-4be9-a8dc-aad32a546cac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7650869f-604f-42fc-9c4c-2082dbf63031",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc453edd-7a7e-4223-92a1-e74d2dbbfeba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7650869f-604f-42fc-9c4c-2082dbf63031",
                    "LayerId": "a71bf9e5-79bf-411b-a0cf-954f5def9033"
                }
            ]
        },
        {
            "id": "bbba7981-5940-471e-9ccf-ca493bfcc213",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4301a530-c551-4982-bb12-cdc70b0b4bad",
            "compositeImage": {
                "id": "b3902fa3-4858-476e-9a3e-7d45bff17fb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbba7981-5940-471e-9ccf-ca493bfcc213",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a436a4b-0807-4b1c-82d7-0cfaec0b1c67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbba7981-5940-471e-9ccf-ca493bfcc213",
                    "LayerId": "a71bf9e5-79bf-411b-a0cf-954f5def9033"
                }
            ]
        },
        {
            "id": "92acd13f-4516-4e90-b4c9-672d9c52656f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4301a530-c551-4982-bb12-cdc70b0b4bad",
            "compositeImage": {
                "id": "a3ea9440-db90-4a82-8633-c6f48fd40298",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92acd13f-4516-4e90-b4c9-672d9c52656f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acb7f157-0baf-432f-9ccf-67f6629f5610",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92acd13f-4516-4e90-b4c9-672d9c52656f",
                    "LayerId": "a71bf9e5-79bf-411b-a0cf-954f5def9033"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "a71bf9e5-79bf-411b-a0cf-954f5def9033",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4301a530-c551-4982-bb12-cdc70b0b4bad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}