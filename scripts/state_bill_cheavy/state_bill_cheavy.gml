if(state_new == 1)
{
    	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -28;
	    xscale  = 26;
	    yscale  = 60;
	    xshift  = -2;
	}
	chainmove = 0;
	attackframe0 = 5;
	endstate = 24;
}

sprite_index = spr_bill_cheavy;

if(state_timer == attackframe0 - 1)
{
    vx = image_xscale * 5;
}
if(state_timer == attackframe0)
{
    hitbox1 = create_hitbox(4,15,42,17,4.5,-9.65,22,7,10,14,-1,0,3,8,"low",-1);
    attackhurtbox0 = create_hurtbox(12 *image_xscale, 20,28 *image_xscale,22,4,-2);
    hitbox1.hitsound = snd_hitsound2;
    hitbox1.scaling = 0.9;
    hitbox1.juggle = 1;
    
}

vx = Approach(vx, 0 , ground_fric);

attack_base(endstate,0,0,1,1,0);

/*
//chainmove
if((hitting == 0 || otherplayer.hitpause > 0) && state_timer > 2)
{
    if(input_check(0,0,0,1,0))
    {
        chainmove = "heavy"
    }
}

//chain
if(hitting == 1 && otherplayer.hitpause == 0 && (input_check(0,0,0,1,0) || chainmove == "heavy") && last_state != 35)
{
    state_switch("heavy");
}
*/