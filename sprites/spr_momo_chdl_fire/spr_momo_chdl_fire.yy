{
    "id": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_chdl_fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 122,
    "bbox_left": 24,
    "bbox_right": 153,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ec77d6f6-23e8-4daf-96d8-b48d87117174",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "58403428-0aa2-4d50-b18c-efcfb99c005c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec77d6f6-23e8-4daf-96d8-b48d87117174",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c23b203b-cfb0-458f-87b6-a331ee617100",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec77d6f6-23e8-4daf-96d8-b48d87117174",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "82ea1ab5-d72f-4d28-b447-fea1b5ef1842",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "ea2349f6-d6dc-4aba-85fe-4fa09dd94208",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82ea1ab5-d72f-4d28-b447-fea1b5ef1842",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41574ea1-6de5-469d-a307-c5efeeb9a11a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82ea1ab5-d72f-4d28-b447-fea1b5ef1842",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "dd2645e6-2879-4515-adb2-391456205dd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "9f61b2d3-3212-44dd-9c5f-213401a1414c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd2645e6-2879-4515-adb2-391456205dd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b0cbda5-367c-4eef-bdae-37a6916cf1f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd2645e6-2879-4515-adb2-391456205dd4",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "e0ba97a9-cf97-48ad-ac5a-a2d0a659f236",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "ce878cbc-430a-4700-8b76-955cae6dbe0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0ba97a9-cf97-48ad-ac5a-a2d0a659f236",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "218cf0f5-b782-405d-9569-757b3e96531b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0ba97a9-cf97-48ad-ac5a-a2d0a659f236",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "1cfa5e48-eb6c-4157-abfe-3ce2a4166d74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "dfe3653f-2ba1-48a0-8661-644208fd396e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cfa5e48-eb6c-4157-abfe-3ce2a4166d74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "967025c1-e7af-4324-a2e0-7721e8e952b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cfa5e48-eb6c-4157-abfe-3ce2a4166d74",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "167bc332-f6e8-4c6c-ad7e-5c0ca64a5412",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "8e24d894-c744-45d7-9019-9955be91195f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "167bc332-f6e8-4c6c-ad7e-5c0ca64a5412",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ac91c43-c6a2-49e9-b269-1c162af11b3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "167bc332-f6e8-4c6c-ad7e-5c0ca64a5412",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "c6019eb8-3fe8-4abc-9136-ddd9d44396cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "851961dd-7914-4e2c-bdc1-7c08adca65c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6019eb8-3fe8-4abc-9136-ddd9d44396cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e070163-e4d0-4a53-8564-1cd2c8a27cf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6019eb8-3fe8-4abc-9136-ddd9d44396cf",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "82a4a504-7ff7-4a8e-8e2b-b3b426b69ffd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "49aaef43-8f46-4e56-827a-ace56b79be21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82a4a504-7ff7-4a8e-8e2b-b3b426b69ffd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9281111-86ad-4e02-af5a-4c706b9982a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82a4a504-7ff7-4a8e-8e2b-b3b426b69ffd",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "b7d5044d-60be-4364-b91e-50e921ebf334",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "310bf8ca-a381-4643-9b78-7fe76ba6cd8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7d5044d-60be-4364-b91e-50e921ebf334",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48854eb1-74fd-48ca-a02f-6c291c3a5a9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7d5044d-60be-4364-b91e-50e921ebf334",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "88202433-1cdc-4ad7-a768-0cc973e110d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "4edebe4e-cdf2-4a81-835a-2e14b9c6db34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88202433-1cdc-4ad7-a768-0cc973e110d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70e20dcc-d9e6-4d0a-be12-af6a04b62509",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88202433-1cdc-4ad7-a768-0cc973e110d7",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "ffd40fcc-ae7b-4a7f-99fc-c0c588dabf9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "ccbbfe1f-7e7a-47fe-8133-fbef4cca4ad5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffd40fcc-ae7b-4a7f-99fc-c0c588dabf9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcd31c43-46d0-45ba-9884-5eee57c8dc8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffd40fcc-ae7b-4a7f-99fc-c0c588dabf9e",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "e25f5c9b-8b5c-4d29-82d7-1f344c56b2d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "39d0ba70-79dd-448e-a369-fa82ed464af8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e25f5c9b-8b5c-4d29-82d7-1f344c56b2d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa2be157-b7db-4a93-a731-2090ff16625e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e25f5c9b-8b5c-4d29-82d7-1f344c56b2d5",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "04f152fc-cdbe-4886-b30f-321e4d8951c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "e442825d-56b5-4fbe-bd67-34e8c9f92e92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04f152fc-cdbe-4886-b30f-321e4d8951c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52e827f6-a428-471c-a3e8-10c3f920051d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04f152fc-cdbe-4886-b30f-321e4d8951c6",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "80642349-73b8-4ed4-af64-da895c9a641c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "2b1df306-9bbe-4524-8eab-6b296f9b1a45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80642349-73b8-4ed4-af64-da895c9a641c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c55b7504-1695-49b3-b03a-0e146a933f57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80642349-73b8-4ed4-af64-da895c9a641c",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "65530491-4990-4b1f-9da2-c72c5fab1a8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "f85ee6ac-38ec-437b-a3cf-7018ec95b59c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65530491-4990-4b1f-9da2-c72c5fab1a8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c24a015b-6e06-4fb7-9e77-fc7ab844bf70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65530491-4990-4b1f-9da2-c72c5fab1a8b",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "0daaae55-d36b-4472-862b-7dd7c6026384",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "4a4c6f14-dc5a-4eed-8214-2addd79022fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0daaae55-d36b-4472-862b-7dd7c6026384",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48a70954-65cd-47d7-af3c-77a39572484f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0daaae55-d36b-4472-862b-7dd7c6026384",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "fa9bb3b2-6916-4e96-8926-679c48fb6c3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "e407bb61-9300-44d9-baa8-55ac32914886",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa9bb3b2-6916-4e96-8926-679c48fb6c3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fbebe49-2e1a-41f8-a8b9-c5082a95c940",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa9bb3b2-6916-4e96-8926-679c48fb6c3e",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "2734b199-c721-46d8-a183-b39de66d32f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "2672c740-1d46-45da-be70-a427bc58e849",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2734b199-c721-46d8-a183-b39de66d32f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb75285b-9676-45a4-968e-81068f58be4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2734b199-c721-46d8-a183-b39de66d32f7",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "a04edf35-2f07-4eb5-a572-dc4471db9971",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "1f9921e5-549a-417f-bb57-bbe09ee812a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a04edf35-2f07-4eb5-a572-dc4471db9971",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ad28282-cf89-4545-8cba-abd8375842f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a04edf35-2f07-4eb5-a572-dc4471db9971",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "39c01f1f-665a-444b-94ef-48f6ad68aaeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "64cd5303-5bff-4c25-a5bc-c3408f0c0f81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39c01f1f-665a-444b-94ef-48f6ad68aaeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0963370-58c4-4343-bbcd-390d979ade99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39c01f1f-665a-444b-94ef-48f6ad68aaeb",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "db583027-af81-4e15-99c9-c0bebf48b173",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "e427ebf3-a0ea-4b60-8d90-8a3e1e9fca27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db583027-af81-4e15-99c9-c0bebf48b173",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4b501ed-3c20-4885-b4d5-a4ad21ff0f23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db583027-af81-4e15-99c9-c0bebf48b173",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "7f37e101-5c73-425e-8599-c08d59a28902",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "07a717a0-2616-4754-b749-5e9b005add07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f37e101-5c73-425e-8599-c08d59a28902",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2396fee6-44af-48bd-a616-be2e3d137994",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f37e101-5c73-425e-8599-c08d59a28902",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "d9f2bbb3-753a-4172-8e8a-605138f9d229",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "d8c11a17-dbca-46f7-ba8f-c2a684d133b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9f2bbb3-753a-4172-8e8a-605138f9d229",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03fcbc47-c867-4869-bdca-1fb62ac1a7dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9f2bbb3-753a-4172-8e8a-605138f9d229",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "7277fb2d-9245-43e7-8aee-1ff4660865fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "10fea778-e3fe-4613-9197-dd1b9e7cca20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7277fb2d-9245-43e7-8aee-1ff4660865fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7e5c1cf-dc71-47ed-b2fb-9633cb27450d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7277fb2d-9245-43e7-8aee-1ff4660865fd",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "607f45c7-6c3d-4aeb-b83c-bd214421438e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "734ee9a9-0be2-4256-858a-509c3acabfa0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "607f45c7-6c3d-4aeb-b83c-bd214421438e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8a47663-b56e-468e-9902-d4ba5c106c33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "607f45c7-6c3d-4aeb-b83c-bd214421438e",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "8fd706fe-4872-4927-bcc0-163b4d89f95f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "296fa4d8-d132-4be0-87f6-36da2db8f3ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fd706fe-4872-4927-bcc0-163b4d89f95f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96a0e763-bb58-4677-a058-9495f1f03139",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fd706fe-4872-4927-bcc0-163b4d89f95f",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "ad6bc202-312d-46d0-b8ce-e70b4caba824",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "1b6b59cb-7b02-4fe7-a610-94c702976312",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad6bc202-312d-46d0-b8ce-e70b4caba824",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1b71de3-ec84-4846-9845-966a67f1b5c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad6bc202-312d-46d0-b8ce-e70b4caba824",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "f9e4782f-6592-4aa9-9695-a861babc99b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "cb8e1f2a-98a1-4c05-9d9d-a490c8f8fac5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9e4782f-6592-4aa9-9695-a861babc99b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19b4bb97-1cca-408b-a0f8-e6e23b8de875",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9e4782f-6592-4aa9-9695-a861babc99b4",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "fd27cfbe-53d7-4d51-923b-0a36514c409e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "b0923522-e9e5-4994-9cef-976fafebad94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd27cfbe-53d7-4d51-923b-0a36514c409e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61e0f657-a381-42d7-a497-c9db47bccada",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd27cfbe-53d7-4d51-923b-0a36514c409e",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "0cce8260-51f5-4667-8737-fee1ad23d94f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "6040f1e9-06c3-4bcf-a07b-e2a89316fe8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cce8260-51f5-4667-8737-fee1ad23d94f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77323b36-fb63-40fd-a102-050077bbc2f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cce8260-51f5-4667-8737-fee1ad23d94f",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "7219d6cc-0d30-48ac-a9d2-12dbef81eb84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "673cf832-dde7-458f-80f1-99fb3d1f96e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7219d6cc-0d30-48ac-a9d2-12dbef81eb84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2856e3bf-38eb-4063-ad8b-b064db6f089f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7219d6cc-0d30-48ac-a9d2-12dbef81eb84",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "7d75fe51-1a92-4f54-b0c3-67928d8d9f24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "de59ba5d-8726-423d-a4d0-f732ee572916",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d75fe51-1a92-4f54-b0c3-67928d8d9f24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83d1fb84-9b79-4559-aa8b-9cf23cb62366",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d75fe51-1a92-4f54-b0c3-67928d8d9f24",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "ff11b4d1-e18e-4bc7-8ba5-43b9907b6c80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "c988a66c-e6dc-4118-86a5-69afc380e68c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff11b4d1-e18e-4bc7-8ba5-43b9907b6c80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d83d6a8-bd00-448e-8162-c928216ba751",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff11b4d1-e18e-4bc7-8ba5-43b9907b6c80",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "f66e7264-1bf9-4f1a-97d6-659b6e0d6091",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "c4223b37-ba9b-41c3-8941-9020289c4cfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f66e7264-1bf9-4f1a-97d6-659b6e0d6091",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "940e5eea-9314-4228-ab98-6f5cc2811557",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f66e7264-1bf9-4f1a-97d6-659b6e0d6091",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "48cb81ee-7c2e-4af0-b80f-1d9d8426fbe4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "0afb9282-2f9e-4536-8520-dc08182a60d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48cb81ee-7c2e-4af0-b80f-1d9d8426fbe4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14e9c8af-84f5-4a71-88dd-d99818c3ccc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48cb81ee-7c2e-4af0-b80f-1d9d8426fbe4",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "fa72bb64-df97-47f8-a7ca-c8dd19b8d74c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "27abe712-8dc6-472b-9516-4ea7668a8b44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa72bb64-df97-47f8-a7ca-c8dd19b8d74c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0d5eafd-5f2a-4ce6-9b15-b189bc6f7331",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa72bb64-df97-47f8-a7ca-c8dd19b8d74c",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "49d4fa6b-35f4-4619-9bf1-617ef089f970",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "fe66cd14-81ca-4aed-8cd3-918e56878a9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49d4fa6b-35f4-4619-9bf1-617ef089f970",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4acf0e41-0e40-47c3-800f-9605369f8033",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49d4fa6b-35f4-4619-9bf1-617ef089f970",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "f8de58ba-3557-4a0b-9184-d8ad982425a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "6ffca678-08b3-4fac-bf89-b56cce0176dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8de58ba-3557-4a0b-9184-d8ad982425a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d929988-2563-4653-8431-fc0bdd454d4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8de58ba-3557-4a0b-9184-d8ad982425a8",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "eaca5d53-8b82-415a-a76e-4da11dead51c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "367a4655-f787-4bf3-8237-5008a1d3c703",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eaca5d53-8b82-415a-a76e-4da11dead51c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67959fed-94ea-4170-9b86-e791ad999b10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eaca5d53-8b82-415a-a76e-4da11dead51c",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "5f62cd8b-fceb-429e-9df8-1103a6c0f43c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "bb600f48-483d-4f00-a0a0-69d0d7b47023",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f62cd8b-fceb-429e-9df8-1103a6c0f43c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9c46cff-1b4b-4528-9aff-04469cfdf0df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f62cd8b-fceb-429e-9df8-1103a6c0f43c",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "c5eefbc2-8a61-4bb0-a062-f3b0059e9bf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "7eec9e21-2c1a-4774-9912-477aa255a484",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5eefbc2-8a61-4bb0-a062-f3b0059e9bf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81743736-07e4-4117-a53d-329bc6d7fee3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5eefbc2-8a61-4bb0-a062-f3b0059e9bf3",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "c68c1104-2296-436b-b4ae-9b5efac8268b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "8e64d11f-6e86-4cbc-95ba-e4ba87c83324",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c68c1104-2296-436b-b4ae-9b5efac8268b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a66f3b10-5136-46e2-aefb-47cc5ae50b66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c68c1104-2296-436b-b4ae-9b5efac8268b",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        },
        {
            "id": "d494b861-66e8-4081-8d1c-3c7e1a53d573",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "compositeImage": {
                "id": "47b4d238-4f6d-46ac-98b8-3c193be78a12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d494b861-66e8-4081-8d1c-3c7e1a53d573",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8e2e689-6259-497e-a19f-2faa2cb56a76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d494b861-66e8-4081-8d1c-3c7e1a53d573",
                    "LayerId": "32f029de-be92-4791-96e6-119aba4921fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 148,
    "layers": [
        {
            "id": "32f029de-be92-4791-96e6-119aba4921fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9c1dd3c4-af20-4c18-b78b-8ad6ae961cc6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 168,
    "xorig": 63,
    "yorig": 87
}