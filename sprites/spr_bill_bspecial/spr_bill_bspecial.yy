{
    "id": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_bspecial",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 28,
    "bbox_right": 105,
    "bbox_top": 66,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "464d360b-150f-44a1-a75c-c522543c0a92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "1e43a772-21c8-43ad-ad39-ce7b8f5f20a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "464d360b-150f-44a1-a75c-c522543c0a92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61ba7284-2ab2-407c-84cd-1990fa53a4d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "464d360b-150f-44a1-a75c-c522543c0a92",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "de4ea888-5516-489f-9a7c-8918321520e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "eca7cacd-88c5-432b-ba3e-23e1a6b46568",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de4ea888-5516-489f-9a7c-8918321520e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b10af300-2e22-4ab2-bf56-d6aa2879c022",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de4ea888-5516-489f-9a7c-8918321520e9",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "ce32078d-b6e8-4758-99ec-1f7b1725cf97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "97403e97-a2ba-46cb-b4dd-e4de87478d11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce32078d-b6e8-4758-99ec-1f7b1725cf97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de1b1c52-a350-4b99-b7af-7bb0c09c997d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce32078d-b6e8-4758-99ec-1f7b1725cf97",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "60d62a2c-9bf9-4497-a326-78b7100ab232",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "c453c3ea-66c1-4707-a0b3-fc117f9ffbf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60d62a2c-9bf9-4497-a326-78b7100ab232",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "480c17eb-89ad-44a3-8e48-fab217a57d40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60d62a2c-9bf9-4497-a326-78b7100ab232",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "9cfdaf38-79f8-4a15-b3a6-b465451d3c64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "4003f55f-1e46-4443-8472-c2579381c2ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cfdaf38-79f8-4a15-b3a6-b465451d3c64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "717a3bb1-4279-447b-984e-8eace7585fc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cfdaf38-79f8-4a15-b3a6-b465451d3c64",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "04b43716-da87-4cd1-ae5c-733becd2e6a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "fe0d5e3a-ed47-4475-b91b-575b07912846",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04b43716-da87-4cd1-ae5c-733becd2e6a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99ffeae8-473b-4035-8ece-6b48de9a761d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04b43716-da87-4cd1-ae5c-733becd2e6a8",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "91bec77e-7376-458d-a115-b70f8beb5d03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "8daf400c-6c45-46ad-bfd7-f82d030e029e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91bec77e-7376-458d-a115-b70f8beb5d03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "282c912a-a330-404c-8521-5c066914c7be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91bec77e-7376-458d-a115-b70f8beb5d03",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "ca0a5b92-684f-45dc-9ddd-3875f2f08f91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "4fb05ecc-b4db-458d-8a0d-6cf1742077ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca0a5b92-684f-45dc-9ddd-3875f2f08f91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e16ff45-fdbb-438d-8c08-f80fe26bd5d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca0a5b92-684f-45dc-9ddd-3875f2f08f91",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "45d55dd4-51bf-4314-bffc-1082ced14151",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "844e0099-c696-46d2-8142-de6d46f06d25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45d55dd4-51bf-4314-bffc-1082ced14151",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bf6be05-6568-4918-8841-4b66293cfa76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45d55dd4-51bf-4314-bffc-1082ced14151",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "2ba8874c-c6e3-40cd-a491-3dda09cf758a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "27ba543f-ff57-4106-b2c2-10ac7ea7585f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ba8874c-c6e3-40cd-a491-3dda09cf758a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "706c9f24-1770-4743-a8f6-ea6d6218cc35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ba8874c-c6e3-40cd-a491-3dda09cf758a",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "c779b0ce-c89b-454f-a357-c469cc002a11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "a13d18f8-e6a2-4bf8-9ddb-4f333e28cc3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c779b0ce-c89b-454f-a357-c469cc002a11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "623599b1-7007-465e-a015-c3206a70117d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c779b0ce-c89b-454f-a357-c469cc002a11",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "6e3ec647-4374-4608-a68f-10d252c05342",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "ac633db2-4ffe-4414-a9c4-47140e69c6a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e3ec647-4374-4608-a68f-10d252c05342",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c0a4191-08e3-4621-97f2-bdb2f9483522",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e3ec647-4374-4608-a68f-10d252c05342",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "cd105246-e33d-4c39-904b-632c3f08947e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "81632102-3342-433a-a630-6bcbe6c2be5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd105246-e33d-4c39-904b-632c3f08947e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e82c6c2-5ef6-4697-90cf-c77bdb7a1211",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd105246-e33d-4c39-904b-632c3f08947e",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "ecf99c83-8b53-4e3e-8fed-b324b90f23dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "8756b861-99bf-4618-9de3-3fd5a0d1675b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecf99c83-8b53-4e3e-8fed-b324b90f23dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bf3c5de-bf07-49f9-bf55-8e6a2c60755d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecf99c83-8b53-4e3e-8fed-b324b90f23dd",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "9bd8ede7-c992-47c2-8ab4-1e3b49af9a80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "1a80def6-5d03-411c-b89d-70b4b823dac2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bd8ede7-c992-47c2-8ab4-1e3b49af9a80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e05028c3-6abf-4160-b721-c8dd7241d43f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bd8ede7-c992-47c2-8ab4-1e3b49af9a80",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "0cb51128-33b9-4f4a-b2bc-e3aff8a8ad43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "c651dd4e-4a90-4355-9019-a45790a8f772",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cb51128-33b9-4f4a-b2bc-e3aff8a8ad43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fa97317-3e7c-4a1d-a190-1614b561316e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cb51128-33b9-4f4a-b2bc-e3aff8a8ad43",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "3da05890-e670-4659-8ef3-7465f1a4e8eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "c3fe0565-1926-452f-a29a-e647e314b5ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3da05890-e670-4659-8ef3-7465f1a4e8eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52231627-981a-4fb9-8dea-a0a3a6219d22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3da05890-e670-4659-8ef3-7465f1a4e8eb",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "8e643a07-f574-4347-aff4-d79ed77280f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "47fe3565-dee2-48e3-a273-8a82e3b9a449",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e643a07-f574-4347-aff4-d79ed77280f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc971c21-e106-417a-82fe-004d687d190e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e643a07-f574-4347-aff4-d79ed77280f7",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "99c91e97-cb71-45b7-8d47-e89da41dd11a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "f2675c11-31bf-42a3-821f-459dc8cacee1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99c91e97-cb71-45b7-8d47-e89da41dd11a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cbe9c60-638d-4e2b-ac1d-8df864885149",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99c91e97-cb71-45b7-8d47-e89da41dd11a",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "cfd30883-9e75-4c32-b84b-18059989c866",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "60f26d59-e39d-4d07-a7c0-6928958803cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfd30883-9e75-4c32-b84b-18059989c866",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47407398-d6de-4287-9dda-60bab14227a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfd30883-9e75-4c32-b84b-18059989c866",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "eda7db0b-00d3-4342-bb6a-23a7a504db00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "85ca122d-e4a7-4596-8fff-aa2120b6b601",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eda7db0b-00d3-4342-bb6a-23a7a504db00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9070c9f2-6e95-4fe7-9ea6-8c5b2f60231e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eda7db0b-00d3-4342-bb6a-23a7a504db00",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "97af1f7a-8e9f-4bef-a80a-0dd834a36e2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "d3430a93-1da3-4491-8309-7e4a98b44e98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97af1f7a-8e9f-4bef-a80a-0dd834a36e2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc4f032a-3138-401f-acf0-e18428c26905",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97af1f7a-8e9f-4bef-a80a-0dd834a36e2e",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "34431583-490d-472f-9a51-af89239cb02a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "eb353838-938c-42ef-ba5d-1c19b996ff5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34431583-490d-472f-9a51-af89239cb02a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8250757f-5962-4192-a778-67c1fa74bb49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34431583-490d-472f-9a51-af89239cb02a",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "32a41df3-f50d-43d5-a839-0551eb8b2931",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "edbfe930-2f63-4ff3-89e2-e655bba791bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32a41df3-f50d-43d5-a839-0551eb8b2931",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf58928d-f500-48db-8b5d-2f7c938872b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32a41df3-f50d-43d5-a839-0551eb8b2931",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "c66d3d74-eb0b-441e-8163-096b41fde996",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "013e214c-e002-4e6c-94b5-0c2ff4b176ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c66d3d74-eb0b-441e-8163-096b41fde996",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5dd45ac-725f-43df-aed6-a75525986b9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c66d3d74-eb0b-441e-8163-096b41fde996",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "9c32d7e7-ace3-4061-8a49-96ff56fa3196",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "6d9c15e9-492e-42bf-a7cb-3365c40a170f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c32d7e7-ace3-4061-8a49-96ff56fa3196",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75007571-6d2c-4e95-902b-260871cccf62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c32d7e7-ace3-4061-8a49-96ff56fa3196",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "87c77d90-41a8-4298-8c2d-4d8bc44fe849",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "dd247c75-f790-429f-9ed5-021851543e1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87c77d90-41a8-4298-8c2d-4d8bc44fe849",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3b8c4e1-353b-42be-9b06-0f16c5d37636",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87c77d90-41a8-4298-8c2d-4d8bc44fe849",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "4fb94ba8-7999-481c-93c3-13eab994acf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "b10c1ac0-dd76-4da0-b5fb-f0f98e7b6ac0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fb94ba8-7999-481c-93c3-13eab994acf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ebf5178-daa3-4e98-a604-599c406a5a42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fb94ba8-7999-481c-93c3-13eab994acf2",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "36491dda-b17c-4c06-a69d-53cdd80fd327",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "fe30ae91-5ebe-4e68-85d2-039b12bc2715",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36491dda-b17c-4c06-a69d-53cdd80fd327",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b95fdd5-0a05-4807-abf1-f80ad1097daf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36491dda-b17c-4c06-a69d-53cdd80fd327",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "42ced11e-f682-4e8b-8e99-c63f1a0f9134",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "a15a42a7-d09c-4817-b919-759ad1b133b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42ced11e-f682-4e8b-8e99-c63f1a0f9134",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7002e5f-d752-4839-8e98-efc344b206a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42ced11e-f682-4e8b-8e99-c63f1a0f9134",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "a220680f-e66f-46f7-9f11-d04553cd2fc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "c4de88f8-0873-442e-8b91-dfa2c3295689",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a220680f-e66f-46f7-9f11-d04553cd2fc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40b8e232-07ea-47e0-ab45-6d996b879fa7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a220680f-e66f-46f7-9f11-d04553cd2fc6",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "afb3ca78-dfbd-4f78-869b-d41fcb2f7b96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "3857651f-a5b6-4d7a-b500-5771f665a5b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afb3ca78-dfbd-4f78-869b-d41fcb2f7b96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb4deef9-3c91-47d1-86ec-fb481129dcdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afb3ca78-dfbd-4f78-869b-d41fcb2f7b96",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "c918d594-8735-4c75-9abb-9747d40ad265",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "293cb5b0-02d6-4ad1-a69f-3c6d95d9b0da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c918d594-8735-4c75-9abb-9747d40ad265",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5226d9e-5ba4-46ba-b3d3-1da00c30ac33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c918d594-8735-4c75-9abb-9747d40ad265",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "58760a5d-cd3f-484a-9ec3-67cd3824029f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "be90739a-1828-4ceb-88de-feca30ff2588",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58760a5d-cd3f-484a-9ec3-67cd3824029f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "985a8c9e-1c71-4d76-8fe0-444d70b3af6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58760a5d-cd3f-484a-9ec3-67cd3824029f",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "f74120ef-b157-417a-9c84-3f29b0be7abc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "fa46d3ec-caf8-4452-b3bf-6383a22987aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f74120ef-b157-417a-9c84-3f29b0be7abc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af79f4b9-6b85-44e6-a00b-66ebb9d6f683",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f74120ef-b157-417a-9c84-3f29b0be7abc",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "5400c255-66f3-42b9-a2e5-ae6de59d4e0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "a257bb1c-0402-44d0-bd8f-0a1b4f7fec72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5400c255-66f3-42b9-a2e5-ae6de59d4e0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fed91b39-e962-4b8f-a8c8-8a4d98786cfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5400c255-66f3-42b9-a2e5-ae6de59d4e0c",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "94720d0c-8ea2-4002-94e9-eccdbd173cfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "45f25fe4-185b-4baa-83dc-4386395afb06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94720d0c-8ea2-4002-94e9-eccdbd173cfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ccbb6de-ba0f-4be0-99b6-795caa18d273",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94720d0c-8ea2-4002-94e9-eccdbd173cfd",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "618e15e6-4fc8-4b73-b21e-32e420cd2ffa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "bd1a9e6b-247b-45e4-a8b4-d95bfdbf3406",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "618e15e6-4fc8-4b73-b21e-32e420cd2ffa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8fe1c11-2af2-44d4-8b67-c1a75517f0f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "618e15e6-4fc8-4b73-b21e-32e420cd2ffa",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "d3ac07c3-f560-4e84-b6f2-1421d2181290",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "249c8a28-158d-4c69-a504-8dad2cdf494e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3ac07c3-f560-4e84-b6f2-1421d2181290",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45678258-7f48-4dc3-85da-d8819ed0f3a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3ac07c3-f560-4e84-b6f2-1421d2181290",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "edc73bad-f8d5-4a9f-976f-9fab01c133c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "934dbd50-b2d1-4a13-8fe2-49f083070c1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edc73bad-f8d5-4a9f-976f-9fab01c133c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67aa0fda-a213-41e7-a722-442d1331fcf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edc73bad-f8d5-4a9f-976f-9fab01c133c5",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "4ec9cf63-50a7-41c8-b99d-6cc4e3606269",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "a8658cab-d47f-4c2a-a0db-eca85156744a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ec9cf63-50a7-41c8-b99d-6cc4e3606269",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f863e04-1c10-467c-99f0-91366a2d321e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ec9cf63-50a7-41c8-b99d-6cc4e3606269",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "05e8db42-99d2-4412-b6c3-e9d6df929f22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "29195302-c594-4c74-93b3-05bc4039d727",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05e8db42-99d2-4412-b6c3-e9d6df929f22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cdeff34-86f6-417f-9c3d-e6fb2ff75446",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05e8db42-99d2-4412-b6c3-e9d6df929f22",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "5fd6475e-b413-4b46-9fb0-30e19ac2b879",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "0c4867ca-0a0e-429a-907e-f5bde5050518",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fd6475e-b413-4b46-9fb0-30e19ac2b879",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca72a70e-a541-4008-8893-77f1b1fa12b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fd6475e-b413-4b46-9fb0-30e19ac2b879",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "5c4d8e80-b8e4-4828-8b12-f32c61147c6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "c517f458-c28f-43eb-bbef-222e228f93dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c4d8e80-b8e4-4828-8b12-f32c61147c6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f42d354f-98ce-4f4a-a9fb-3a07dbad2386",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c4d8e80-b8e4-4828-8b12-f32c61147c6f",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "3b8c8561-58cb-4b4b-aca3-8c0e510046a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "72eb478b-a3b4-4e6d-9888-69b2e63529c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b8c8561-58cb-4b4b-aca3-8c0e510046a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91b2adc2-6139-4365-867a-2fdaef727334",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b8c8561-58cb-4b4b-aca3-8c0e510046a9",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "f7f0205c-e897-40f3-80cb-3514b71a96b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "437b923b-19cc-4318-859e-ed57d37f0fc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7f0205c-e897-40f3-80cb-3514b71a96b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df0e1e8c-308e-41d3-a32a-f1a977d73728",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7f0205c-e897-40f3-80cb-3514b71a96b8",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "a5f78423-b5b3-4940-b93c-3b2acdec7528",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "f1eaa4b2-5dbd-43fa-84f6-cfd60babd8ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5f78423-b5b3-4940-b93c-3b2acdec7528",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b257d22-284b-4f58-81a9-6337cf482bad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5f78423-b5b3-4940-b93c-3b2acdec7528",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "aee6f094-27bd-4bfb-966f-68c335869ab7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "e52818b2-36a7-4251-99f9-60ea5c5ece43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aee6f094-27bd-4bfb-966f-68c335869ab7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18bbd72f-68d0-4580-82a3-f7505a457a56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aee6f094-27bd-4bfb-966f-68c335869ab7",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "df4bfb2a-6d8f-4388-bf17-1ba6f41d0240",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "dca9d9da-cf19-4a79-95f0-506933323f3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df4bfb2a-6d8f-4388-bf17-1ba6f41d0240",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e4e0a9e-d4cd-4735-a870-ae88ec026b34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df4bfb2a-6d8f-4388-bf17-1ba6f41d0240",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "975a3fcd-50d4-4f27-afdb-27b7bd6ee33c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "82131782-2dc0-4691-9fec-5f9a745b10aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "975a3fcd-50d4-4f27-afdb-27b7bd6ee33c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43d383d5-fb9a-4d9b-82e2-08b55e2f267e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "975a3fcd-50d4-4f27-afdb-27b7bd6ee33c",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "864c2965-4d55-45f0-b8f5-cf9ba3fed50b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "19b9a61a-70aa-46ae-b315-d86649101d17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "864c2965-4d55-45f0-b8f5-cf9ba3fed50b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "679d8db6-9e4f-4cbe-ae4a-808c14636dd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "864c2965-4d55-45f0-b8f5-cf9ba3fed50b",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "36a3dce8-cc72-4844-996f-bf5536d09901",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "4f26b570-808d-44bb-a533-6f0f8bd8dcad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36a3dce8-cc72-4844-996f-bf5536d09901",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f6b57d0-e36e-4ef8-a69f-a43c4357a9fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36a3dce8-cc72-4844-996f-bf5536d09901",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "22e4a26d-13ee-44f1-a6e9-a319513a0394",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "cade2f13-9102-447d-8057-f4af270150e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22e4a26d-13ee-44f1-a6e9-a319513a0394",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b850bdb-a9b7-427a-87bc-7282ad3d422d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22e4a26d-13ee-44f1-a6e9-a319513a0394",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "60c3b8f5-ede7-4605-9818-431821f38a7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "c3335ffa-a0fa-42e2-9ad3-d968f65f211c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60c3b8f5-ede7-4605-9818-431821f38a7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78de1c65-4715-4c3c-a2b3-42cd7d3d6b7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60c3b8f5-ede7-4605-9818-431821f38a7c",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "6f55d5bd-369f-4e46-9b6e-bfe50451ccbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "5c859f2a-8ac6-462f-a2f9-2f7606a3b122",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f55d5bd-369f-4e46-9b6e-bfe50451ccbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f3e4088-2543-4294-9002-57f80bd57dc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f55d5bd-369f-4e46-9b6e-bfe50451ccbb",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "d0eed6c1-ad0d-45ae-a25b-eb334fb96467",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "3cba9e06-9040-4d21-b74b-bec9a9f5673c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0eed6c1-ad0d-45ae-a25b-eb334fb96467",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc7187da-65d9-470e-8dd7-749aed320a5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0eed6c1-ad0d-45ae-a25b-eb334fb96467",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "80a90ea8-4597-46c2-be12-fe8c53f002f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "cb89c080-b8c6-4d0a-90d3-8b44195648c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80a90ea8-4597-46c2-be12-fe8c53f002f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35b216fe-3e55-4394-a608-7c0f3994548d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80a90ea8-4597-46c2-be12-fe8c53f002f2",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "bc411e3d-67a3-43fe-af51-5b98ae357a97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "cf4cbb4d-a237-426a-9790-d91a0a86003d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc411e3d-67a3-43fe-af51-5b98ae357a97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf43f2ea-f9f0-4276-a6d4-b581365a60d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc411e3d-67a3-43fe-af51-5b98ae357a97",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "220e278a-7cb7-4b31-8ce4-3d502c60f02e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "afd4e35a-a444-4d1b-ac2c-d5570161fc08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "220e278a-7cb7-4b31-8ce4-3d502c60f02e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a266f586-15be-451d-803a-59652eff11e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "220e278a-7cb7-4b31-8ce4-3d502c60f02e",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "1635f411-6e24-4f42-87cb-21254cc3de18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "39a36d2f-d216-4a7b-a5db-87848186e54a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1635f411-6e24-4f42-87cb-21254cc3de18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b7a6d99-405f-4374-97e2-97c81a091355",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1635f411-6e24-4f42-87cb-21254cc3de18",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "76fa7bfe-2642-496c-bf8f-38ce7cca8fde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "f8f76ba4-2487-4bc1-ad7e-053a8a2fe492",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76fa7bfe-2642-496c-bf8f-38ce7cca8fde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4950837a-16f7-4388-a948-4bb4e12f8394",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76fa7bfe-2642-496c-bf8f-38ce7cca8fde",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "d5b4491f-737e-4bd0-aab6-adc3c91017e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "9348e220-a53a-44a1-bc2f-84d28e619cd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5b4491f-737e-4bd0-aab6-adc3c91017e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbc9a3ce-7018-4a45-90af-ebf08616773f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5b4491f-737e-4bd0-aab6-adc3c91017e5",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "6df79596-48fe-447c-8feb-69229522370a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "3ed08d38-4853-43a3-9a07-3aaa114d801f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6df79596-48fe-447c-8feb-69229522370a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8f210fe-99ee-4cc3-835f-8d18c9c30c8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6df79596-48fe-447c-8feb-69229522370a",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "1b19f518-1001-46ac-8935-f7198de97d41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "22d66963-dd43-4b4b-8ba2-2e9d3b8fea96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b19f518-1001-46ac-8935-f7198de97d41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19575f21-1b82-438e-9a68-ac8c497b1247",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b19f518-1001-46ac-8935-f7198de97d41",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "f859f32d-97af-47dd-9655-ea296e604295",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "654ad4e3-49cb-4a9c-944e-c504031c4195",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f859f32d-97af-47dd-9655-ea296e604295",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45e3d9fb-d445-4d3b-9188-3560495e39f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f859f32d-97af-47dd-9655-ea296e604295",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "e180b90b-8c26-4462-9883-097ccad2f7c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "5cc45cde-008d-408b-ab62-e50169314915",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e180b90b-8c26-4462-9883-097ccad2f7c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "596a1c66-efe7-4c9a-bd6f-3a8d73363a1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e180b90b-8c26-4462-9883-097ccad2f7c1",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "84218f94-0288-4d29-95f8-83ac2d027674",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "c3d0be13-ec81-4890-bd98-ecefa42155cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84218f94-0288-4d29-95f8-83ac2d027674",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc81d1ad-531b-4577-b7c1-dd6dd04cca5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84218f94-0288-4d29-95f8-83ac2d027674",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "704b0d0d-191f-475f-b72f-885b39d5bb37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "09c6f38f-95db-489e-b732-c9c8a8c67c92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "704b0d0d-191f-475f-b72f-885b39d5bb37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e90933dc-d7a9-41f4-af9d-af76cb3bd8f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "704b0d0d-191f-475f-b72f-885b39d5bb37",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "3451b5e9-62fa-4f97-b08b-aee1c6068e14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "ed347b60-d916-4904-a85b-ec3b657802a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3451b5e9-62fa-4f97-b08b-aee1c6068e14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e77600c6-2afe-46e3-85f5-6675f90024a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3451b5e9-62fa-4f97-b08b-aee1c6068e14",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        },
        {
            "id": "1b0516d3-0c71-4e44-92a6-9f00832306a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "compositeImage": {
                "id": "4d5e0451-07d8-4308-9169-148b1874941a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b0516d3-0c71-4e44-92a6-9f00832306a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52c8349a-177d-4008-84ad-c23e15bafa43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b0516d3-0c71-4e44-92a6-9f00832306a0",
                    "LayerId": "5d38ddf5-e8c9-4e02-830f-39e40076843e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "5d38ddf5-e8c9-4e02-830f-39e40076843e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b25fc72a-bbdf-41d9-8c81-0f70416c2772",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 100,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}