{
    "id": "4afd69b2-4a6e-4ec1-9d36-9ecc197d3fbf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "justice_medium",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 145,
    "bbox_left": 55,
    "bbox_right": 141,
    "bbox_top": 74,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "78cf11fe-5e3a-40bf-9357-749364008725",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4afd69b2-4a6e-4ec1-9d36-9ecc197d3fbf",
            "compositeImage": {
                "id": "1c63306e-a5dd-43e9-9b58-f7435931fe12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78cf11fe-5e3a-40bf-9357-749364008725",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15fe5325-2d94-4557-a749-79035f44523b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78cf11fe-5e3a-40bf-9357-749364008725",
                    "LayerId": "75266a02-0517-4fce-a964-964dc14f80e3"
                }
            ]
        },
        {
            "id": "683d79c0-904a-4640-b4cb-c16cde23ae3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4afd69b2-4a6e-4ec1-9d36-9ecc197d3fbf",
            "compositeImage": {
                "id": "d2651a50-2f6d-441e-bfce-4b8ae87854df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "683d79c0-904a-4640-b4cb-c16cde23ae3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c328179d-e8a6-4415-90ca-43754dda1cb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "683d79c0-904a-4640-b4cb-c16cde23ae3f",
                    "LayerId": "75266a02-0517-4fce-a964-964dc14f80e3"
                }
            ]
        },
        {
            "id": "2be5f50a-6b9a-4c87-8667-1150d1f529b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4afd69b2-4a6e-4ec1-9d36-9ecc197d3fbf",
            "compositeImage": {
                "id": "4ad30b2f-5e79-4c9e-9d47-8dfa7749f1d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2be5f50a-6b9a-4c87-8667-1150d1f529b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04c76b76-0618-4e16-bb86-bef57565ee4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2be5f50a-6b9a-4c87-8667-1150d1f529b2",
                    "LayerId": "75266a02-0517-4fce-a964-964dc14f80e3"
                }
            ]
        },
        {
            "id": "f0423d37-4919-4e49-8940-a7693a807d4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4afd69b2-4a6e-4ec1-9d36-9ecc197d3fbf",
            "compositeImage": {
                "id": "db5c7bf8-9727-47c2-abd7-45ee63b4b6a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0423d37-4919-4e49-8940-a7693a807d4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a13584f2-57f9-4220-af43-1cafa37ff96f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0423d37-4919-4e49-8940-a7693a807d4d",
                    "LayerId": "75266a02-0517-4fce-a964-964dc14f80e3"
                }
            ]
        },
        {
            "id": "13e334ad-1b4f-4dde-91c7-20d1ccfbfdec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4afd69b2-4a6e-4ec1-9d36-9ecc197d3fbf",
            "compositeImage": {
                "id": "e0590c21-b06b-4694-a4c7-39a3c043cbf2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13e334ad-1b4f-4dde-91c7-20d1ccfbfdec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55bddfb1-4722-4ac0-a013-c4d3392f6746",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13e334ad-1b4f-4dde-91c7-20d1ccfbfdec",
                    "LayerId": "75266a02-0517-4fce-a964-964dc14f80e3"
                }
            ]
        },
        {
            "id": "fb543fdb-fd54-4538-a1c2-01e9cde95415",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4afd69b2-4a6e-4ec1-9d36-9ecc197d3fbf",
            "compositeImage": {
                "id": "c34fdccb-6cd8-42e2-bee1-3e5fbd4c10e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb543fdb-fd54-4538-a1c2-01e9cde95415",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71aec722-df86-4e1c-afa0-eeb5b97800dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb543fdb-fd54-4538-a1c2-01e9cde95415",
                    "LayerId": "75266a02-0517-4fce-a964-964dc14f80e3"
                }
            ]
        },
        {
            "id": "b86335ab-5ddb-492b-a10f-cc58aefdacf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4afd69b2-4a6e-4ec1-9d36-9ecc197d3fbf",
            "compositeImage": {
                "id": "17d62540-7380-49bd-bcd4-e5c2e47da3d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b86335ab-5ddb-492b-a10f-cc58aefdacf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b92493b-737d-4980-b133-765988bd096f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b86335ab-5ddb-492b-a10f-cc58aefdacf8",
                    "LayerId": "75266a02-0517-4fce-a964-964dc14f80e3"
                }
            ]
        },
        {
            "id": "8ddb442f-a68b-44fe-82f8-1c4f02189f22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4afd69b2-4a6e-4ec1-9d36-9ecc197d3fbf",
            "compositeImage": {
                "id": "c7880a42-3105-4efe-8895-2470b6ebc6e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ddb442f-a68b-44fe-82f8-1c4f02189f22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8320a86b-b379-49eb-b5d9-43efd700fcbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ddb442f-a68b-44fe-82f8-1c4f02189f22",
                    "LayerId": "75266a02-0517-4fce-a964-964dc14f80e3"
                }
            ]
        },
        {
            "id": "d6273498-a24b-40be-ad72-3be1966d2435",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4afd69b2-4a6e-4ec1-9d36-9ecc197d3fbf",
            "compositeImage": {
                "id": "5b885df7-3a04-47d9-83a1-35aee4f0d844",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6273498-a24b-40be-ad72-3be1966d2435",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "568df6f7-2895-43f7-b255-54dcae417297",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6273498-a24b-40be-ad72-3be1966d2435",
                    "LayerId": "75266a02-0517-4fce-a964-964dc14f80e3"
                }
            ]
        },
        {
            "id": "747eab85-30ae-4d90-b035-a5f3655528e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4afd69b2-4a6e-4ec1-9d36-9ecc197d3fbf",
            "compositeImage": {
                "id": "8ebd5e07-3466-43ea-a16c-3810c5849cc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "747eab85-30ae-4d90-b035-a5f3655528e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f0e6cbb-30e7-4abb-a4d3-f80bd85303ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "747eab85-30ae-4d90-b035-a5f3655528e4",
                    "LayerId": "75266a02-0517-4fce-a964-964dc14f80e3"
                }
            ]
        },
        {
            "id": "8e1634bb-33cd-4534-a29c-0653dfdfaf20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4afd69b2-4a6e-4ec1-9d36-9ecc197d3fbf",
            "compositeImage": {
                "id": "8403bf45-b928-4da6-99bb-ec7104f42b26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e1634bb-33cd-4534-a29c-0653dfdfaf20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a3c8bec-6ed4-43dc-b9bf-3d3675fda0ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e1634bb-33cd-4534-a29c-0653dfdfaf20",
                    "LayerId": "75266a02-0517-4fce-a964-964dc14f80e3"
                }
            ]
        },
        {
            "id": "e371865c-d2cb-43ba-9c31-dc91a3bb4239",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4afd69b2-4a6e-4ec1-9d36-9ecc197d3fbf",
            "compositeImage": {
                "id": "39af0dca-063d-4d1b-bbe7-1c2c76dd19c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e371865c-d2cb-43ba-9c31-dc91a3bb4239",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45cafaa6-47f3-4677-a2ff-458edc985943",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e371865c-d2cb-43ba-9c31-dc91a3bb4239",
                    "LayerId": "75266a02-0517-4fce-a964-964dc14f80e3"
                }
            ]
        },
        {
            "id": "351b5b31-b705-4631-bb9f-377663631180",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4afd69b2-4a6e-4ec1-9d36-9ecc197d3fbf",
            "compositeImage": {
                "id": "e11568f1-8c1e-43a6-b19d-c6d8496dfe88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "351b5b31-b705-4631-bb9f-377663631180",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bf81188-4114-42b7-bc9e-cfa436ddae3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "351b5b31-b705-4631-bb9f-377663631180",
                    "LayerId": "75266a02-0517-4fce-a964-964dc14f80e3"
                }
            ]
        },
        {
            "id": "da610382-9c31-4688-a43a-f632f8e32451",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4afd69b2-4a6e-4ec1-9d36-9ecc197d3fbf",
            "compositeImage": {
                "id": "6aa04f51-843f-4ba3-82ea-e6133bba75c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da610382-9c31-4688-a43a-f632f8e32451",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83c7e8e1-3402-4df8-b1a6-1a66bebef801",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da610382-9c31-4688-a43a-f632f8e32451",
                    "LayerId": "75266a02-0517-4fce-a964-964dc14f80e3"
                }
            ]
        },
        {
            "id": "4a31c4e5-89e4-4f37-b4ca-1d7ead376b0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4afd69b2-4a6e-4ec1-9d36-9ecc197d3fbf",
            "compositeImage": {
                "id": "9469c544-2f30-438c-b688-da4cc07207d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a31c4e5-89e4-4f37-b4ca-1d7ead376b0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ade6ea2f-da81-4799-8c27-79f86d835729",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a31c4e5-89e4-4f37-b4ca-1d7ead376b0a",
                    "LayerId": "75266a02-0517-4fce-a964-964dc14f80e3"
                }
            ]
        },
        {
            "id": "60b1c052-e3f7-48df-9e31-473634b514d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4afd69b2-4a6e-4ec1-9d36-9ecc197d3fbf",
            "compositeImage": {
                "id": "d77eb8e7-2759-42a4-8d4b-44c554d4a2e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60b1c052-e3f7-48df-9e31-473634b514d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66aa17c9-3cdb-4ab0-85f0-8fd64ae2f0d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60b1c052-e3f7-48df-9e31-473634b514d7",
                    "LayerId": "75266a02-0517-4fce-a964-964dc14f80e3"
                }
            ]
        },
        {
            "id": "6fafdd44-bceb-40b8-9af2-e098762af8d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4afd69b2-4a6e-4ec1-9d36-9ecc197d3fbf",
            "compositeImage": {
                "id": "a52438f2-2da4-408e-b083-0c235f72b214",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fafdd44-bceb-40b8-9af2-e098762af8d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b6a244b-76d2-43d0-a363-187b3c904b6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fafdd44-bceb-40b8-9af2-e098762af8d7",
                    "LayerId": "75266a02-0517-4fce-a964-964dc14f80e3"
                }
            ]
        },
        {
            "id": "5a58043c-f637-4a7f-bac5-45d433ac934b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4afd69b2-4a6e-4ec1-9d36-9ecc197d3fbf",
            "compositeImage": {
                "id": "74ca70b3-9514-4d44-a575-6ee0125ba803",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a58043c-f637-4a7f-bac5-45d433ac934b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "491cf0ae-d511-4672-84fa-da879b0ef147",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a58043c-f637-4a7f-bac5-45d433ac934b",
                    "LayerId": "75266a02-0517-4fce-a964-964dc14f80e3"
                }
            ]
        },
        {
            "id": "68dd2e64-3a2a-481f-8f27-4039139d8dee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4afd69b2-4a6e-4ec1-9d36-9ecc197d3fbf",
            "compositeImage": {
                "id": "1aa74ac1-9a01-48fe-965c-c30a6b32d4b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68dd2e64-3a2a-481f-8f27-4039139d8dee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eed5e438-9379-4b9b-b2f8-cfc78ae64ff0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68dd2e64-3a2a-481f-8f27-4039139d8dee",
                    "LayerId": "75266a02-0517-4fce-a964-964dc14f80e3"
                }
            ]
        },
        {
            "id": "52dd9d60-8333-48b5-b2ec-cf9c94409d56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4afd69b2-4a6e-4ec1-9d36-9ecc197d3fbf",
            "compositeImage": {
                "id": "1fb1934d-ad9d-477c-9cfa-9a771f22e0f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52dd9d60-8333-48b5-b2ec-cf9c94409d56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3db2904-e533-4162-a330-8dbb47aa28b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52dd9d60-8333-48b5-b2ec-cf9c94409d56",
                    "LayerId": "75266a02-0517-4fce-a964-964dc14f80e3"
                }
            ]
        },
        {
            "id": "12704204-2112-4d72-9b12-9f9c7578a6ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4afd69b2-4a6e-4ec1-9d36-9ecc197d3fbf",
            "compositeImage": {
                "id": "81f7c900-c05a-49a0-a0de-5b104eb7b745",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12704204-2112-4d72-9b12-9f9c7578a6ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3f01cb3-39e2-4eb4-b022-c2e06c8cdc58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12704204-2112-4d72-9b12-9f9c7578a6ac",
                    "LayerId": "75266a02-0517-4fce-a964-964dc14f80e3"
                }
            ]
        },
        {
            "id": "7853fb12-4026-49fa-8f3a-3a1c683807ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4afd69b2-4a6e-4ec1-9d36-9ecc197d3fbf",
            "compositeImage": {
                "id": "34bb1e8f-ee29-4441-bbd9-b8fb18016c4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7853fb12-4026-49fa-8f3a-3a1c683807ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da5c61b4-14be-4292-83a6-14a388b5f2a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7853fb12-4026-49fa-8f3a-3a1c683807ea",
                    "LayerId": "75266a02-0517-4fce-a964-964dc14f80e3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "75266a02-0517-4fce-a964-964dc14f80e3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4afd69b2-4a6e-4ec1-9d36-9ecc197d3fbf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 79,
    "yorig": 121
}