{
    "id": "af17ed92-35f2-4594-ade4-fdc942e9486b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_2l_fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4565d329-fea6-40c1-8753-46da299acc3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af17ed92-35f2-4594-ade4-fdc942e9486b",
            "compositeImage": {
                "id": "cb85d30b-a204-4179-b511-ed646d15836b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4565d329-fea6-40c1-8753-46da299acc3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77ba20c3-4a53-4b17-b0b4-6be3d8cb0024",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4565d329-fea6-40c1-8753-46da299acc3a",
                    "LayerId": "effa9f91-328e-4bf3-bcd3-06dd23d975a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "effa9f91-328e-4bf3-bcd3-06dd23d975a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af17ed92-35f2-4594-ade4-fdc942e9486b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}