{
    "id": "152bef07-861a-4de2-819c-ca800923f5f7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hitbox",
    "eventList": [
        {
            "id": "fad4c759-664d-436c-9cbd-fccac1d3fc44",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "152bef07-861a-4de2-819c-ca800923f5f7"
        },
        {
            "id": "72699756-9ac6-492e-b1ad-5f7650e30392",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "152bef07-861a-4de2-819c-ca800923f5f7"
        },
        {
            "id": "ad3aa5ac-883a-4839-83e4-79edf0044ebb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "152bef07-861a-4de2-819c-ca800923f5f7"
        },
        {
            "id": "ab7d8019-249b-4b1c-9bee-5b1fde567a27",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "152bef07-861a-4de2-819c-ca800923f5f7"
        },
        {
            "id": "2acfd3f4-27c3-41ea-a103-7e87dc85a95c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "152bef07-861a-4de2-819c-ca800923f5f7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3f71f13e-1650-4279-94a0-b98cd956daa1",
    "visible": true
}