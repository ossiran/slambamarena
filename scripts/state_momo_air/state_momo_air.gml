live_call();

if(state_new == 1)
{
    /*
    with(hurtbox0)
	{
	    xoffset = -12;
	    yoffset = -13;
	    image_xscale  = 24;
	    image_yscale  = 40;
	    xshift  = 0;
	}
	
	with(hurtbox1)
	{
	    xoffset = -8;
	    yoffset = -31;
	    image_xscale  = 16;
	    image_yscale  = 18;
	    xshift = 0;
	}
	*/
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -13;
	    image_xscale  = 16;
	    image_yscale  = 40;
	    xshift = 0;
	}
}


//sprites
if(vy < -1)
{
    if(doublejump == 0)
    {
        sprite_index = spr_momo_rising;
    }
    else
    {
    sprite_index = spr_momo_rising;
    }
}
if(vy < 1 && vy > -1)
{
    sprite_index = spr_momo_airn;
}
if(vy > 1)
{
    sprite_index = spr_momo_falling;
}


//lair
if(input_check(0,1,0,0,0))
{
    state_switch("lair");
}


//lair
if(input_check(0,0,1,0,0))
{
    state_switch("mair");
}

//hair
if(input_check(0,0,0,1,0))
{
    state_switch("hair");
}

//charge up
if(chu() == 1)
{
    if(input_check(2,0,0,1,0))
    {
        if(ex > 49.9)
        {
            state_switch("chuh");
        }
        else
        {
            state_switch("chum");
        }
    }
    else if(input_check(2,0,1,0,0))
    {
        state_switch("chum");
    }
    else if(input_check(2,1,0,0,0))
    {
        state_switch("chul");
    }
}

//qcb
if(qcb_buffer > 0)
{
    if(input_check(0,0,0,1,0))
    {
        if(ex > 49.9)
        {
            state_switch("aqcbh");
        }
        else
        {
            state_switch("aqcbm");
        }
    }
    else if(input_check(0,0,1,0,0))
    {
        state_switch("aqcbm");
    }
    else if(input_check(0,1,0,0,0))
    {
        state_switch("aqcbl");
    }
}

//landing 
if(onground() && vy == 0)
{
	audio_play_sound(snd_landing,1,0);
	state_switch("stand");
	dashdir = 0;
	vx = 0;
}
else
{
	vy = Approach(vy,vyMax,grav);
}

//hitstun
if(hitstun > 0)
{
    hit = 0;
    state_switch("hitstun");   
}

if(blockstun > 0)
{
    state_switch("ablock");
}