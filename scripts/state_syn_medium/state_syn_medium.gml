live_call();
if (synmode > 0)
{
    if(state_new == 1)
    {
        with(hurtbox0)
    	{
    	    xoffset = -12;
    	    yoffset = -13;
    	    image_xscale  = 24;
    	    image_yscale  = 40;
    	    xshift  = 0;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -8;
    	    yoffset = -31;
    	    image_xscale  = 16;
    	    image_yscale  = 18;
    	    xshift = 0;
    	}
    	
    	with(collisionbox)
    	{
    	    xoffset = -8;
    	    yoffset = -13;
    	    image_xscale  = 16;
    	    image_yscale  = 40;
    	    xshift = 0;
    	}
    	
    	chainmove = 0;
    	attackframe0 = 6;
    	endstate = 22;
    }
    
    //animation
    sprite_index = spr_lsyn_5m;
    
    if(state_timer < attackframe0 && state_timer > 1)
    {
        vx = 5 * image_xscale;
    }
    
    //attack
    if(state_timer == attackframe0)
    {
        hitbox0 = create_hitbox(9,-25,43,16,7.5,0,14,19,10,12,0,0,2,4,"mid",-1);
        attackhurtbox0 = create_hurtbox(8 * image_xscale, -17,22 * image_xscale,16,2,-2);
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 1;
        hitbox0.juggle = 1;
    }
    
    //chainmove
    if(state_timer > 3)
    {
        if(input_check(1,0,1,0,0) || input_check(2,0,1,0,0) || input_check(3,0,1,0,0))
        {
            chainmove = "cmedium"
        }
        
        if(input_check(0,0,0,1,0))
        {
            chainmove = "heavy"
        }
        
        if((input_check(2,0,0,1,0) || input_check(1,0,0,1,0) || input_check(3,0,0,1,0)))
        {
            chainmove = "cheavy";
        }
    }
    
    attack_base(endstate,0,0,1,1,0);
}
else if(synmode < 0)
{
    if(state_new == 1)
    {
        with(hurtbox0)
    	{
    	    xoffset = -12;
    	    yoffset = -13;
    	    image_xscale  = 24;
    	    image_yscale  = 40;
    	    xshift  = 0;
    	}
    	
    	with(hurtbox1)
    	{
    	    xoffset = -8;
    	    yoffset = -31;
    	    image_xscale  = 16;
    	    image_yscale  = 18;
    	    xshift = 0;
    	}
    	
    	with(collisionbox)
    	{
    	    xoffset = -8;
    	    yoffset = -13;
    	    image_xscale  = 16;
    	    image_yscale  = 40;
    	    xshift = 0;
    	}
    	
    	chainmove = 0;
    	attackframe0 = 6;
    	endstate = 22;
    }
    
    //animation
    sprite_index = spr_dsyn_5m;
    
    if(state_timer < attackframe0 && state_timer > 1)
    {
        vx = 5 * image_xscale;
    }
    
    //attack
    if(state_timer == attackframe0)
    {
        hitbox0 = create_hitbox(9,-25,43,16,7.5,0,15,19,10,10,0,0,2,4,"mid",-1);
        attackhurtbox0 = create_hurtbox(8 * image_xscale, -17,22 * image_xscale,16,2,-2);
        hitbox0.hitsound = snd_hitsound1;
        hitbox0.scaling = 1;
        hitbox0.juggle = 1;
    }
    
    //chainmove
    if(state_timer > 3)
    {
        if(input_check(1,0,1,0,0) || input_check(2,0,1,0,0) || input_check(3,0,1,0,0))
        {
            chainmove = "cmedium"
        }
        
        if(input_check(0,0,0,1,0))
        {
            chainmove = "heavy"
        }
        
        if((input_check(2,0,0,1,0) || input_check(1,0,0,1,0) || input_check(3,0,0,1,0)))
        {
            chainmove = "cheavy";
        }
    }
    
    attack_base(endstate,0,0,1,1,0);
}
