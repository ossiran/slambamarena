{
    "id": "9e301794-26db-4ff8-8cbb-50301b64423e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lsyn_2h",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 107,
    "bbox_left": 24,
    "bbox_right": 108,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "17a659a1-c89e-4bd4-a9dc-1ce4424b3964",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "1941be25-d9d7-4fd2-8280-5d7db77814e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17a659a1-c89e-4bd4-a9dc-1ce4424b3964",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdc0488a-9c81-4b8f-8580-d56875050f0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17a659a1-c89e-4bd4-a9dc-1ce4424b3964",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "e048f08e-69bf-4ddf-aba0-e1ddce9731f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "22f9466d-c507-4c66-8ab3-28f460f8344e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e048f08e-69bf-4ddf-aba0-e1ddce9731f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4163d89a-0697-497d-99d8-1a9bf0adb961",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e048f08e-69bf-4ddf-aba0-e1ddce9731f7",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "e5500cd0-ddf7-4a8e-825a-edc6b3e9e05f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "07b7d111-ab34-4f3a-9865-2c0c34576109",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5500cd0-ddf7-4a8e-825a-edc6b3e9e05f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edc9c279-8f4d-4a19-b6aa-c4bb2bc1480f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5500cd0-ddf7-4a8e-825a-edc6b3e9e05f",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "965ee72a-da11-4f67-a974-c233cd4eb84b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "1aca4fc8-3074-45d1-8c83-f2cbaaa5cfb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "965ee72a-da11-4f67-a974-c233cd4eb84b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a11484b5-fcc4-4656-b0f9-909e314d54c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "965ee72a-da11-4f67-a974-c233cd4eb84b",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "c1d4a1a1-d109-4c05-b267-d030413327a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "e9395d28-8fa5-4a4c-ad6b-5569a60f9991",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1d4a1a1-d109-4c05-b267-d030413327a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "deda08f7-357e-4465-b73e-09cac90fbdd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1d4a1a1-d109-4c05-b267-d030413327a8",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "274ec0df-c2f7-43e5-87b3-7398e44e28b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "7a176a55-18d8-4360-a983-187a4c4f79a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "274ec0df-c2f7-43e5-87b3-7398e44e28b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2da8cc83-d063-4b65-8ce1-d2387151b70f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "274ec0df-c2f7-43e5-87b3-7398e44e28b7",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "57f94c25-7309-4c0f-ac06-cc0e482e251c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "80d3d0f0-7b05-40b7-89a7-768c51a86fbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57f94c25-7309-4c0f-ac06-cc0e482e251c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b66befb-877d-48ba-b8f1-c7d0ce6e5529",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57f94c25-7309-4c0f-ac06-cc0e482e251c",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "0a641e39-08ef-4ec5-8035-af1323fbf121",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "c22b1619-c7de-4205-808d-27759f5b5147",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a641e39-08ef-4ec5-8035-af1323fbf121",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47bf1c05-c906-4307-a326-98291efb6142",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a641e39-08ef-4ec5-8035-af1323fbf121",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "c2011c79-5706-4a23-8d76-c7c0b5db0b5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "93aaef70-b8db-4a23-b52b-987fbc58749e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2011c79-5706-4a23-8d76-c7c0b5db0b5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c581fd4-1f57-45b6-9cf2-a633c20bc445",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2011c79-5706-4a23-8d76-c7c0b5db0b5b",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "520359a6-4bd9-47b1-b0e8-b15a96c97280",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "3504abb6-7027-4853-a939-e5dc2319c918",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "520359a6-4bd9-47b1-b0e8-b15a96c97280",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d141a27-9749-4847-b8e1-713abcf541be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "520359a6-4bd9-47b1-b0e8-b15a96c97280",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "65729c25-be41-4805-bdc8-e41ad452933f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "50c01ec6-1a67-4db8-8ad2-7cb9661c80f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65729c25-be41-4805-bdc8-e41ad452933f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3b4236e-9d4b-42cd-b322-17ef9e5062c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65729c25-be41-4805-bdc8-e41ad452933f",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "6ed5666c-2c37-4e9c-b871-087d3e728b61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "3b95c470-0a3b-40a9-8f46-2ec3639b9c94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ed5666c-2c37-4e9c-b871-087d3e728b61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "314243e4-7ec4-4e4e-a458-c588bf66ff91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ed5666c-2c37-4e9c-b871-087d3e728b61",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "35ca3c7c-1c63-4b8e-ad01-1837058a1e87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "4ec53b86-343d-4340-8959-57f342775831",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35ca3c7c-1c63-4b8e-ad01-1837058a1e87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e60b979-5151-4a20-aa80-8c5f992527ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35ca3c7c-1c63-4b8e-ad01-1837058a1e87",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "32128abd-0d87-4dbe-a9e1-154b9c4e8e5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "9048aa8d-1060-480d-82ba-b9c0b86ee815",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32128abd-0d87-4dbe-a9e1-154b9c4e8e5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "527b69c6-ff42-47c6-8d6a-f0ead087544e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32128abd-0d87-4dbe-a9e1-154b9c4e8e5e",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "d7828097-5754-43d5-87d8-21ca696d65d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "8cbbbfa6-a9cb-43e7-89c0-3a76c31ddd41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7828097-5754-43d5-87d8-21ca696d65d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8c63315-1a05-4afa-b095-af840fafeca8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7828097-5754-43d5-87d8-21ca696d65d1",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "20e262cf-6c09-449f-9902-44ee070bc502",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "5b169039-64bf-4db9-b6a5-e514d537acea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20e262cf-6c09-449f-9902-44ee070bc502",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ea3804a-3361-41b6-9b47-a880ebacdee9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20e262cf-6c09-449f-9902-44ee070bc502",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "d91a49ef-8edf-4cc8-9d51-ed9129f1ddbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "4d328cfb-9696-4de1-aba4-6340e5694487",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d91a49ef-8edf-4cc8-9d51-ed9129f1ddbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbd86a29-fa53-4d6a-86dc-6bdc82d2053a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d91a49ef-8edf-4cc8-9d51-ed9129f1ddbc",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "bee1469c-8e8d-4763-aa1f-0faef3d95347",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "a8dedf0a-eaea-4c8b-ba64-45746173021e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bee1469c-8e8d-4763-aa1f-0faef3d95347",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae20cf23-dc7b-4eec-bb45-9a54fea0e9c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bee1469c-8e8d-4763-aa1f-0faef3d95347",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "c965de4d-e088-4a3d-84d8-d83579a93683",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "7b80d9ab-69aa-49d5-aaf6-ed9f5fafa024",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c965de4d-e088-4a3d-84d8-d83579a93683",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a4bc93c-9612-48d8-a8ec-2f2d3b41a848",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c965de4d-e088-4a3d-84d8-d83579a93683",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "0edc509f-4fdb-4c23-9447-e3ae452c5206",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "9bd402f0-7958-4f5e-a8b8-514bf1cc105f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0edc509f-4fdb-4c23-9447-e3ae452c5206",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77b31149-4e6c-4858-8501-785c6ebcbe09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0edc509f-4fdb-4c23-9447-e3ae452c5206",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "0f0dae32-05fb-43b3-bb8f-ced4556fb20a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "c12d162e-280d-4d46-876f-4d2ff47bda93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f0dae32-05fb-43b3-bb8f-ced4556fb20a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0eb53a93-d7bf-470f-91ef-cd8503e063eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f0dae32-05fb-43b3-bb8f-ced4556fb20a",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "3e62138d-9891-439d-bb13-264091371f28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "1384ffda-ab37-4122-91f8-2c1672e9bbaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e62138d-9891-439d-bb13-264091371f28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dab7914-8ba5-40ab-b2cc-68532adaba92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e62138d-9891-439d-bb13-264091371f28",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "0810780a-f95b-4b1d-8967-963102215abc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "fcbeaec4-38a2-4b81-9d93-192d02d048bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0810780a-f95b-4b1d-8967-963102215abc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dab0a41-1ee8-470f-ada0-2313d9e67fd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0810780a-f95b-4b1d-8967-963102215abc",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "e4ef0d64-349c-4af1-9c35-78217a6ba4c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "f44b0b17-a244-40ae-bac3-00a88bc94281",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4ef0d64-349c-4af1-9c35-78217a6ba4c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a17059d7-5927-45aa-9b15-e00f6a0dd811",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4ef0d64-349c-4af1-9c35-78217a6ba4c1",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "eb8c4bf7-06b3-4785-980f-11936475588d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "f18a1dbf-cdf0-4aab-8179-16879efcee51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb8c4bf7-06b3-4785-980f-11936475588d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51f39266-ad81-4b50-a485-a898409a1b52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb8c4bf7-06b3-4785-980f-11936475588d",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "4be4152f-cb61-4216-9b45-2e0f731a1668",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "fed4f6cf-866b-4df0-89a0-e8e1e123cf1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4be4152f-cb61-4216-9b45-2e0f731a1668",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59e610f8-119d-42e9-89f4-2d26d289a1e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4be4152f-cb61-4216-9b45-2e0f731a1668",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "88f104ff-6543-4f85-8be0-2d1762b0fad6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "b5aa34de-b3fc-412f-a7b9-62292668485e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88f104ff-6543-4f85-8be0-2d1762b0fad6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34d21fd3-4f77-4d57-87eb-3b25c4ce5439",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88f104ff-6543-4f85-8be0-2d1762b0fad6",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "93d1aa73-5dad-43e0-88a3-31a7be8d4db1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "d347a209-607a-4894-aad5-51c7d2b8b2b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93d1aa73-5dad-43e0-88a3-31a7be8d4db1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a04500c-74f6-48c3-890c-eafbe362fea1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93d1aa73-5dad-43e0-88a3-31a7be8d4db1",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "e603b77a-a146-4446-96d1-adebe807b36d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "29006864-d57e-45e6-a9ba-726e827b801b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e603b77a-a146-4446-96d1-adebe807b36d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57a7ca5a-928a-455a-83bb-99b5b09f2bfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e603b77a-a146-4446-96d1-adebe807b36d",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "2f2ca2a2-969a-473c-a7ac-9b1c35f6afb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "709467ac-d236-442a-b1da-1195c627bcc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f2ca2a2-969a-473c-a7ac-9b1c35f6afb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5479d294-08f5-4d3b-bed2-0d8837f3e0f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f2ca2a2-969a-473c-a7ac-9b1c35f6afb5",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "89e12be9-d1d6-4f37-9276-3fcd24c1ce7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "6d664b3f-33f5-4c2d-b187-578b87469a6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89e12be9-d1d6-4f37-9276-3fcd24c1ce7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a9dd662-9897-4c49-bd74-a9cb34f8c3c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89e12be9-d1d6-4f37-9276-3fcd24c1ce7c",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "2a81fd09-47e9-41c9-9dc9-3b31fb10472c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "a6ea1a0c-a277-4fe0-860a-aa1883cf6eda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a81fd09-47e9-41c9-9dc9-3b31fb10472c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2569a96c-6f87-4a58-9718-2e26411898f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a81fd09-47e9-41c9-9dc9-3b31fb10472c",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "72b11f31-36de-49ba-a2a4-32601508bda5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "da26daa4-07d9-441d-8dc4-7da432da2b32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72b11f31-36de-49ba-a2a4-32601508bda5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bf87675-1e6a-4222-ba27-383feb18e3c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72b11f31-36de-49ba-a2a4-32601508bda5",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "4d86dd88-3f27-43d0-ab23-a512965d66d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "97d3af15-58d5-4f59-89fe-0c92aa5e1e13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d86dd88-3f27-43d0-ab23-a512965d66d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e69deda-a12e-4239-b2f5-6d4c2e9d9eb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d86dd88-3f27-43d0-ab23-a512965d66d3",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "8d66581b-3023-4044-9ee3-6fb00e7acb5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "23326b30-e384-4aac-af51-275ac50eced4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d66581b-3023-4044-9ee3-6fb00e7acb5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8a44585-90f0-49cd-8c45-a63fc5e1192a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d66581b-3023-4044-9ee3-6fb00e7acb5e",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        },
        {
            "id": "6d95afa2-38ba-48c0-86bc-4c9b1d3e8ec1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "compositeImage": {
                "id": "9bfe5209-710d-4103-9601-dd56afc58093",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d95afa2-38ba-48c0-86bc-4c9b1d3e8ec1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c8872ad-93e2-4a0b-91ac-0e9cf72ad014",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d95afa2-38ba-48c0-86bc-4c9b1d3e8ec1",
                    "LayerId": "a294a6c4-b946-4570-986c-2e58bbff3acc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "a294a6c4-b946-4570-986c-2e58bbff3acc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9e301794-26db-4ff8-8cbb-50301b64423e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 76
}