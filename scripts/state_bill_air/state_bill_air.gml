

//sprites
if(vy < -1)
{
    if(doublejump == 0)
    {
        sprite_index = spr_bill_jumpUU;
    }
    else
    {
    sprite_index = spr_bill_jumpU;
    }
}
if(vy < 1 && vy > -1)
{
    sprite_index = spr_bill_jumpN;
}
if(vy > 1)
{
    sprite_index = spr_bill_jumpD;
}

if(state_new == 1)
{
    	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -43;
	    image_xscale  = 26;
	    image_yscale  = 75;
	    xshift  = -2;
	}
	
	with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 16;
	    image_yscale  = 40;
	    xshift = -2
	}
}




//double jump	
if(tap(7) && doublejump == 1)
{
		doublejump --;
		vy = -jumpheight * 0.85;
		vx = -4 * image_xscale;
		audio_play_sound(snd_bill_rocketjump,1,0);
}

//double jump	
if(tap(8) && doublejump == 1)
{
		doublejump --;
		vx = 0;
		vy = -jumpheight;
		audio_play_sound(snd_bill_rocketjump,1,0);
}

//double jump	
if(tap(9) && doublejump == 1)
{
		doublejump --;
		vy = -jumpheight * 0.85;
		vx = 4 * image_xscale;
		audio_play_sound(snd_bill_rocketjump,1,0);
}

//lair
if(input_check(0,1,0,0,0))
{
    state_switch("lair");
}


//lair
if(input_check(0,0,1,0,0))
{
    state_switch("mair");
}

//hair
if(input_check(0,0,0,1,0))
{
    state_switch("hair");
}

//qcb
if(qcb_buffer > 0)
{
    if(input_check(0,0,0,1,0))
    {
        state_switch("aqcbh");
    }
    else if(input_check(0,0,1,0,0))
    {
        state_switch("aqcbm");
    }
    else if(input_check(0,1,0,0,0))
    {
        state_switch("aqcbl");
    }
}

//airdash
if((ff_buffer > 0 || (input_check(6,1,1,0,0))) && airdash == 1)
{
    state_switch("airdash");
    dashdir = image_xscale;
    airdash = 0;
}
if((bb_buffer > 0 || (input_check(4,1,1,0,0))) && airdash == 1)
{
    state_switch("airdash");
    dashdir = -image_xscale;
    airdash = 0;
}

//landing 
if(onground() && vy == 0)
{
	audio_play_sound(snd_landing,1,0);
	state_switch("stand");
	dashdir = 0;
	vx = 0;
}
else
{
	vy = Approach(vy,vyMax,grav);
}

//hitstun
if(hitstun > 0)
{
    hit = 0;
    state_switch("hitstun");   
}

if(blockstun > 0)
{
    state_switch("ablock");
}