{
    "id": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "justice_heavy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 145,
    "bbox_left": 56,
    "bbox_right": 138,
    "bbox_top": 73,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29c980b8-c1f4-454c-8aa3-8100c0231e35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "dc93cca2-8075-41f7-bd31-d4c382ee8db3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29c980b8-c1f4-454c-8aa3-8100c0231e35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3731c8a-77a1-4ed7-9f94-82ca9db7051f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29c980b8-c1f4-454c-8aa3-8100c0231e35",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "9ac75a67-a4d1-4365-ad69-e0c237c10119",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "c4e00f12-22df-4a1d-9451-0e59c249cdd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ac75a67-a4d1-4365-ad69-e0c237c10119",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a26cd6d2-3079-4671-8671-54b49bdb1087",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ac75a67-a4d1-4365-ad69-e0c237c10119",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "c1ad5d7f-1169-4376-839a-a27252c81d09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "0650afb7-fafa-4562-a7a6-36b5519a53f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1ad5d7f-1169-4376-839a-a27252c81d09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df5565f9-eae8-47e4-aea7-20ac7edc72c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1ad5d7f-1169-4376-839a-a27252c81d09",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "894a0ad7-f4bf-4f98-9c8d-027c10c1c54e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "34de6a41-85ba-4ce4-bdf6-a26ef8545c10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "894a0ad7-f4bf-4f98-9c8d-027c10c1c54e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5232eb1-d417-4052-a015-1747384aa2fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "894a0ad7-f4bf-4f98-9c8d-027c10c1c54e",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "6b4c7009-d20f-4a1f-b8d0-1f2e69895d24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "f2d6b42c-5fc8-4897-8d22-44752c278158",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b4c7009-d20f-4a1f-b8d0-1f2e69895d24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4eae2b89-1b26-43e7-9eac-05f500641e5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b4c7009-d20f-4a1f-b8d0-1f2e69895d24",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "8673df3c-a770-45d6-8b71-f533adb234fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "0e111aab-10e8-4c88-bfcf-d4928db9fae4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8673df3c-a770-45d6-8b71-f533adb234fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "241bd470-aa0e-4186-ba87-34990f930d14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8673df3c-a770-45d6-8b71-f533adb234fd",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "36cbf7c9-c8c9-4adf-9f14-c73da29276bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "3f037c1b-f955-4c6c-88f3-b1e6c3e8224c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36cbf7c9-c8c9-4adf-9f14-c73da29276bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39ef5dac-2281-40e5-9e65-23d33b83be29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36cbf7c9-c8c9-4adf-9f14-c73da29276bc",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "e8ac0d0a-9c2e-4e46-b41f-6db1fcde6094",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "9f57c3cc-602c-44bc-ba30-c3918be1ba1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8ac0d0a-9c2e-4e46-b41f-6db1fcde6094",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe41a0b8-70a8-40a8-9b3a-4320cea4943a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8ac0d0a-9c2e-4e46-b41f-6db1fcde6094",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "6f0eb7fd-c157-470e-a9ea-6eb765a8c191",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "80e60fda-a89f-4a5c-a904-25d1673735c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f0eb7fd-c157-470e-a9ea-6eb765a8c191",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4844dfa1-3642-438f-b8cc-99d8064404df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f0eb7fd-c157-470e-a9ea-6eb765a8c191",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "4891c687-7cd6-400d-a971-fcfbc8318308",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "0220e463-d632-4ebd-8d67-5f1237c41614",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4891c687-7cd6-400d-a971-fcfbc8318308",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7c51819-554f-490d-aacd-6e9d57bdadb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4891c687-7cd6-400d-a971-fcfbc8318308",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "66fc8d39-b24e-418f-8c38-133c3806ce6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "a87e1666-f33a-40fa-95e1-dc69e6d7eacb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66fc8d39-b24e-418f-8c38-133c3806ce6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc280c6d-09f2-40d0-9c13-f6f7a0337035",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66fc8d39-b24e-418f-8c38-133c3806ce6d",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "8d73a6ee-66a5-4058-a49e-faf302cba1da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "970c0817-8970-4995-ad54-f97a1c47604f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d73a6ee-66a5-4058-a49e-faf302cba1da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3df315e-1772-4f7a-b380-9bd868cb1bbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d73a6ee-66a5-4058-a49e-faf302cba1da",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "2b23960e-148e-42dd-8f39-684b3cc6cb2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "eb136027-c41b-4804-8fc4-25583f8d45a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b23960e-148e-42dd-8f39-684b3cc6cb2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "974fe03b-1dcb-4763-a639-b06e0270542a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b23960e-148e-42dd-8f39-684b3cc6cb2f",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "c68efef1-5d8d-448e-ae09-3fbfd13535f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "916d02de-201c-469e-bb28-5e49d1be1627",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c68efef1-5d8d-448e-ae09-3fbfd13535f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d2f2a13-afe3-4f4e-b22a-a010dd3e986b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c68efef1-5d8d-448e-ae09-3fbfd13535f4",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "f3e56bea-43a4-4e49-957a-3a0d2d2892fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "272ad171-4271-45f8-8ddc-3f80d93b7fd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3e56bea-43a4-4e49-957a-3a0d2d2892fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49ac7754-405e-45be-92d6-3241ec17899c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3e56bea-43a4-4e49-957a-3a0d2d2892fc",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "1e03470c-2e64-4c09-9b99-93db9087dced",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "f8ec46a8-5ac0-41c2-9679-acd2e8b0eef5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e03470c-2e64-4c09-9b99-93db9087dced",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c12e052e-a825-49d0-b83a-777b772f647c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e03470c-2e64-4c09-9b99-93db9087dced",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "79978094-a6ca-4a25-b91b-642bcab92f32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "aceb5b7c-8aee-4537-bfd1-3672b3f0653d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79978094-a6ca-4a25-b91b-642bcab92f32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10b8a4d9-6abc-449e-aa3a-250f7e6e4767",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79978094-a6ca-4a25-b91b-642bcab92f32",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "0782fe6b-505f-4b61-9769-e1bef7079f86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "00149daf-9d6b-4d5a-99cb-a09f570b3f41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0782fe6b-505f-4b61-9769-e1bef7079f86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d08139b0-12f9-4b57-acba-d797946df039",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0782fe6b-505f-4b61-9769-e1bef7079f86",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "187f0446-5cb1-4350-a928-9baa0b4e3108",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "dda77ae1-e6c6-458b-9682-82e2f8188ffe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "187f0446-5cb1-4350-a928-9baa0b4e3108",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e06b24b4-8277-429f-bfe3-3324c10dd3e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "187f0446-5cb1-4350-a928-9baa0b4e3108",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "bae2d1a0-f02d-4330-b588-68d9a9d563f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "cfc72297-ccf4-4d13-9b21-5cad5314919e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bae2d1a0-f02d-4330-b588-68d9a9d563f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78828fa0-0b82-4967-83d3-0b9196921279",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bae2d1a0-f02d-4330-b588-68d9a9d563f0",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "f5c752d1-34ae-44f2-b299-a40da05328e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "b59e4017-c136-4778-99a8-0b230d32ba58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5c752d1-34ae-44f2-b299-a40da05328e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de66cfa2-f211-4220-96ff-aaa4ba4cdcf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5c752d1-34ae-44f2-b299-a40da05328e1",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "aec9e340-2ec5-405f-9040-d0c728a7e56f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "4c53cab6-62bb-42e5-a412-b8df80ff58bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aec9e340-2ec5-405f-9040-d0c728a7e56f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c0bbad0-6010-44f7-b230-dc9458ada169",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aec9e340-2ec5-405f-9040-d0c728a7e56f",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "e9b6c871-51d8-40d9-bc4f-721fb2f5f5fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "40a51a72-31b6-4329-88cc-6024d30dd0ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9b6c871-51d8-40d9-bc4f-721fb2f5f5fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cc0f58c-cf23-4ef6-bcb0-e4b38ad11f81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9b6c871-51d8-40d9-bc4f-721fb2f5f5fb",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "419e9c78-ef1e-467a-9215-1b5b4aaf7026",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "74566648-b479-40a0-a735-294d15f9a88f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "419e9c78-ef1e-467a-9215-1b5b4aaf7026",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "686e4ec0-0649-4eca-b98c-6c4ef37a15eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "419e9c78-ef1e-467a-9215-1b5b4aaf7026",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "03783e60-d7cc-4ea2-baf4-a4fceac5082b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "5cd87405-85c1-4f62-99be-d907a472ff59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03783e60-d7cc-4ea2-baf4-a4fceac5082b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d52f9e42-b072-4e32-a5c1-43b6bbbf8307",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03783e60-d7cc-4ea2-baf4-a4fceac5082b",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "360d483d-48fb-4335-afdf-7cedd3b58fa8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "e9ee978b-bbcf-48a9-8cbc-86fae396c259",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "360d483d-48fb-4335-afdf-7cedd3b58fa8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c53b7d29-b413-41d1-86ac-aae5707e5535",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "360d483d-48fb-4335-afdf-7cedd3b58fa8",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "96cbe2f9-c269-41c3-b627-b40d9f9de830",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "74e98931-c502-4d85-ae5d-b22dfc217a15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96cbe2f9-c269-41c3-b627-b40d9f9de830",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ef1df1d-4b3e-4e9f-a7d7-abe7daf7377e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96cbe2f9-c269-41c3-b627-b40d9f9de830",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "e1a7154c-4368-47a3-b022-50a249efa444",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "2f6f5f89-d242-40c3-adda-c9f42efc0d46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1a7154c-4368-47a3-b022-50a249efa444",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a759016-5f09-4f18-a3bb-aa47b25bae44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1a7154c-4368-47a3-b022-50a249efa444",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "d20d783a-a7a3-4cdb-b9f2-0f4f6f4c4185",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "31e92d43-64c2-43df-8545-ce641e338e60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d20d783a-a7a3-4cdb-b9f2-0f4f6f4c4185",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18f6343c-8fb0-4bc8-8d7a-ed954432ec06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d20d783a-a7a3-4cdb-b9f2-0f4f6f4c4185",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "6971903d-8c81-404c-8165-7f292ab8c268",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "bfdaa740-111d-4a37-bbb1-8ad605bafcac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6971903d-8c81-404c-8165-7f292ab8c268",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5532e819-5376-4947-87e9-59bc441478fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6971903d-8c81-404c-8165-7f292ab8c268",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "ac72556d-7eac-4949-b883-9ebd3ce0f331",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "965abf7f-5043-4fed-a816-5000ef9c6761",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac72556d-7eac-4949-b883-9ebd3ce0f331",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78fbae91-34da-4df1-b0ec-1f6b7891f0eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac72556d-7eac-4949-b883-9ebd3ce0f331",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "aef01871-d9e8-4363-88ef-e87b025e23bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "c607b15f-f66a-474c-a98d-b4095060f62e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aef01871-d9e8-4363-88ef-e87b025e23bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ac7230d-89b2-4a6e-af33-68b477e8a6ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aef01871-d9e8-4363-88ef-e87b025e23bb",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "b2ec2384-b8e3-445f-a029-2d8938882ab5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "99bd1965-ba1d-478a-91e5-84e4fd1da8eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2ec2384-b8e3-445f-a029-2d8938882ab5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a6b8183-cf46-4299-bfd1-6f294182c34a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2ec2384-b8e3-445f-a029-2d8938882ab5",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "cc111682-14b8-45ed-a6ab-3c78d3af80b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "da713930-a9cf-40e9-b539-8aee7057aa75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc111682-14b8-45ed-a6ab-3c78d3af80b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "339d795b-7561-496d-8c60-762b6f3f8d63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc111682-14b8-45ed-a6ab-3c78d3af80b7",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "2d898c8b-a931-45c6-b317-23468e3aec95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "d5fb260a-4336-407f-a805-d5e93c0d7d54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d898c8b-a931-45c6-b317-23468e3aec95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b5b82de-9362-4602-8beb-8d5d52cd1266",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d898c8b-a931-45c6-b317-23468e3aec95",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "d9dec332-69a1-4d71-8044-882206490dbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "1aefd9ad-59ec-448e-b640-6e9258d75970",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9dec332-69a1-4d71-8044-882206490dbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34af34d8-c53f-41fc-8744-4b069c6fd57f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9dec332-69a1-4d71-8044-882206490dbb",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "b5908f82-9d3c-4288-b62f-8ff52d116c9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "281074f8-5271-439b-bc55-992cd870d2c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5908f82-9d3c-4288-b62f-8ff52d116c9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "645389b2-c521-48cf-8523-df63230623c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5908f82-9d3c-4288-b62f-8ff52d116c9e",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "4e002445-7f72-4912-a719-8fa4ad565957",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "2a18d257-2e72-4a20-a93b-d2826e8e1ed4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e002445-7f72-4912-a719-8fa4ad565957",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa78581a-cde9-414f-8f3a-71f4deb52fb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e002445-7f72-4912-a719-8fa4ad565957",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "eece0d19-06dc-430c-b01b-26dc34f008ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "28443476-e93a-49c3-b9d9-c3765201b943",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eece0d19-06dc-430c-b01b-26dc34f008ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c118b96-b1a3-4284-8dcb-4a95c09af34a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eece0d19-06dc-430c-b01b-26dc34f008ea",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "a72ab4ba-bb12-4c06-ae97-ce44c98c2188",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "d1b0da1b-0dbf-4dbc-b408-45f78ec95cf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a72ab4ba-bb12-4c06-ae97-ce44c98c2188",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12d4626b-7b56-4792-9bbc-2eae27b22b84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a72ab4ba-bb12-4c06-ae97-ce44c98c2188",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "78fc9d20-1862-43c4-8004-23b0165cb94f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "952aec0c-4f5d-4ab4-9947-2d0de8042f04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78fc9d20-1862-43c4-8004-23b0165cb94f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3bcc297-adf9-4bdf-8ec5-d1bf76133ae3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78fc9d20-1862-43c4-8004-23b0165cb94f",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "c7007852-d694-4fc4-ad69-c13ffcf3554b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "400f05e0-e010-4aa9-86c5-6b24235e0ef5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7007852-d694-4fc4-ad69-c13ffcf3554b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b097e65a-7dcc-48d3-9c29-52e3bae4c6f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7007852-d694-4fc4-ad69-c13ffcf3554b",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "943e65e3-dbc3-49c8-9a8f-9a4c01a5cd03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "f5475d1b-89b4-42e2-a110-db793dcc39ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "943e65e3-dbc3-49c8-9a8f-9a4c01a5cd03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53b1835a-aca7-40ff-b69e-bd0b658cd851",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "943e65e3-dbc3-49c8-9a8f-9a4c01a5cd03",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "e755d021-fa69-432e-889f-cc2d77ddb2ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "9f5085f5-e85e-4bbb-be4f-d10b8e4dda76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e755d021-fa69-432e-889f-cc2d77ddb2ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8696ea97-d0dd-4f54-a745-22b2dce1d7c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e755d021-fa69-432e-889f-cc2d77ddb2ae",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        },
        {
            "id": "7dbe067f-5138-4fd3-8924-a034cfde90b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "compositeImage": {
                "id": "46ab704b-c57c-4d83-846e-f35aa8a2d3d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dbe067f-5138-4fd3-8924-a034cfde90b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f3606e1-077c-4417-be45-b9927cccf852",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dbe067f-5138-4fd3-8924-a034cfde90b3",
                    "LayerId": "991bae9e-55c4-418c-a598-f0326f478f3f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "991bae9e-55c4-418c-a598-f0326f478f3f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a4fd1512-4486-4e24-8f46-49e6fcc80adb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 79,
    "yorig": 121
}