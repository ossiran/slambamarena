if(state_new == 1)
{
	with(hurtbox0)
	{
	    xoffset = -13;
	    yoffset = -43;
	    image_xscale  = 26;
	    image_yscale  = 75;
	    xshift  = -2;
	}
	
			with(collisionbox)
	{
	    xoffset = -8;
	    yoffset = -15;
	    image_xscale  = 16;
	    image_yscale  = 47;
	    xshift = -2
	}
	attackframe0 = 4;
	endstate = 21;
}


//friction
vx = Approach(vx,0,ground_fric * 0.5);

if(state_timer < attackframe0 && state_timer > 1)
{
    vx = 3 * image_xscale;
}
else
{
    vx = 0;
}

if(state_timer == attackframe0)
{
    hitbox0 = create_hitbox(9,-22,28,18,6,0,15.5,19,11,18,-1,0,2,4,"mid",-1);
    attackhurtbox0 = create_hurtbox(12 * image_xscale, -25,22 * image_xscale,16,2,-2);
    hitbox0.hitsound = snd_hitsound1;
    hitbox0.scaling = 1;
    hitbox0.juggle = 1;
}

sprite_index = spr_bill_bjab;

//chainmove
if(state_timer > 3)
{
    if(input_check(1,0,1,0,0) || input_check(2,0,1,0,0) || input_check(3,0,1,0,0))
    {
        chainmove = "cmedium"
    }
    
    if(input_check(0,0,0,1,0))
    {
        chainmove = "heavy"
    }
    
    if((input_check(2,0,0,1,0) || input_check(1,0,0,1,0) || input_check(3,0,0,1,0)))
    {
        chainmove = "cheavy";
    }
}

attack_base(endstate,0,0,1,1,0);




/*
//chain

if(hitting == 1 && otherplayer.hitpause == 0 && ((input_check(1,0,1,0,0) || input_check(2,0,1,0,0) || input_check(3,0,1,0,0)) || chainmove == "cmedium") && last_state != 34)
{
    state_switch("cmedium");
}

if(hitting == 1 && otherplayer.hitpause == 0 && (input_check(0,0,0,1,0) || chainmove == "heavy"))
{
    state_switch("heavy");
}

if(hitting == 1 && otherplayer.hitpause == 0 && ((input_check(2,0,0,1,0) || input_check(1,0,0,1,0) || input_check(3,0,0,1,0)) || chainmove == "cheavy"))
{
    state_switch("cheavy");
}
*/
