{
    "id": "4a7276dd-ab22-435c-94d7-8553264b844b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_2h_ice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0f84f1a5-24f0-4e56-8860-244b785f3951",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a7276dd-ab22-435c-94d7-8553264b844b",
            "compositeImage": {
                "id": "0f0e5648-bf71-4e8a-a6fe-a3ae1d2a04cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f84f1a5-24f0-4e56-8860-244b785f3951",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a95c840b-aed2-46bf-a884-53b4ac92ab04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f84f1a5-24f0-4e56-8860-244b785f3951",
                    "LayerId": "82af288f-6401-4413-b7bd-238abb8d242c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "82af288f-6401-4413-b7bd-238abb8d242c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4a7276dd-ab22-435c-94d7-8553264b844b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}