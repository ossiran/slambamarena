{
    "id": "018249a4-bcf8-46f8-aff1-3120abb42b1f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_falling",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 120,
    "bbox_left": 28,
    "bbox_right": 113,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ff94c663-9b12-4529-8a2d-c23127c0a18a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "018249a4-bcf8-46f8-aff1-3120abb42b1f",
            "compositeImage": {
                "id": "4463a51a-87a7-44a5-a396-e4a1ae891bf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff94c663-9b12-4529-8a2d-c23127c0a18a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4823e564-48db-4e8a-b32c-920083526282",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff94c663-9b12-4529-8a2d-c23127c0a18a",
                    "LayerId": "2e707411-82f2-4aa0-9eb0-b731e23adfe5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "2e707411-82f2-4aa0-9eb0-b731e23adfe5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "018249a4-bcf8-46f8-aff1-3120abb42b1f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 63,
    "yorig": 87
}