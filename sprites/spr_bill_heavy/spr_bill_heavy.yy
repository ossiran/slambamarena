{
    "id": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_heavy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 43,
    "bbox_right": 114,
    "bbox_top": 75,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "37092061-953b-4ce9-b4b0-2a033968970e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "compositeImage": {
                "id": "a115cf07-94ac-4da0-b1f7-4c8c7ebd6365",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37092061-953b-4ce9-b4b0-2a033968970e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15b99e94-5c60-4b7c-9e6c-50541097beaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37092061-953b-4ce9-b4b0-2a033968970e",
                    "LayerId": "f25df08c-7665-47f6-a2dd-88502ae24d1c"
                }
            ]
        },
        {
            "id": "d476f1b4-eac1-491a-bf37-cc29f9d6c941",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "compositeImage": {
                "id": "619a2fe1-ef4f-4e3e-958b-7dd060008f50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d476f1b4-eac1-491a-bf37-cc29f9d6c941",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad638ff7-7945-4f9d-8939-9e0c09064fb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d476f1b4-eac1-491a-bf37-cc29f9d6c941",
                    "LayerId": "f25df08c-7665-47f6-a2dd-88502ae24d1c"
                }
            ]
        },
        {
            "id": "7b5fec82-db03-48bb-826e-1927abfae9f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "compositeImage": {
                "id": "c18de37e-b92b-4b02-aeb4-545023df07bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b5fec82-db03-48bb-826e-1927abfae9f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3c73dd6-7271-45c3-a64b-6191f3c109c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b5fec82-db03-48bb-826e-1927abfae9f5",
                    "LayerId": "f25df08c-7665-47f6-a2dd-88502ae24d1c"
                }
            ]
        },
        {
            "id": "181daf01-d1a1-47c2-a1ce-ef855aa0067d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "compositeImage": {
                "id": "15cd112a-fa91-499c-8a52-4b4bf94ec556",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "181daf01-d1a1-47c2-a1ce-ef855aa0067d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7634b89f-89c1-4906-8cbb-9ef198b8c367",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "181daf01-d1a1-47c2-a1ce-ef855aa0067d",
                    "LayerId": "f25df08c-7665-47f6-a2dd-88502ae24d1c"
                }
            ]
        },
        {
            "id": "9d51713e-79d2-484f-8968-1d783305b184",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "compositeImage": {
                "id": "ddeaf011-fdc3-4d0f-906c-88b9908ea21f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d51713e-79d2-484f-8968-1d783305b184",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95007820-0f69-4c93-ba28-50689228bb62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d51713e-79d2-484f-8968-1d783305b184",
                    "LayerId": "f25df08c-7665-47f6-a2dd-88502ae24d1c"
                }
            ]
        },
        {
            "id": "6bf439fb-9707-43d3-980d-5b151745b8f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "compositeImage": {
                "id": "80799e76-0145-4699-90bd-4979a81f4e4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bf439fb-9707-43d3-980d-5b151745b8f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f85a1951-46e2-413a-9175-3c5c06629ac2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bf439fb-9707-43d3-980d-5b151745b8f6",
                    "LayerId": "f25df08c-7665-47f6-a2dd-88502ae24d1c"
                }
            ]
        },
        {
            "id": "45d231f4-446c-4cec-ba24-7698c47b77c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "compositeImage": {
                "id": "35301411-b94f-4eb0-999c-663fa2425055",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45d231f4-446c-4cec-ba24-7698c47b77c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa04e9f5-a09c-4cd4-b316-8521acca6f9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45d231f4-446c-4cec-ba24-7698c47b77c4",
                    "LayerId": "f25df08c-7665-47f6-a2dd-88502ae24d1c"
                }
            ]
        },
        {
            "id": "72400f6d-35e1-4509-8ba2-bb94a5e206ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "compositeImage": {
                "id": "66d60f39-707b-48fb-a79f-7961a2d2eb9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72400f6d-35e1-4509-8ba2-bb94a5e206ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43a7a9f7-1c34-4840-bbf1-1acca41686b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72400f6d-35e1-4509-8ba2-bb94a5e206ed",
                    "LayerId": "f25df08c-7665-47f6-a2dd-88502ae24d1c"
                }
            ]
        },
        {
            "id": "c7a4d6d1-7ea2-4b34-bd13-a8a82081f4ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "compositeImage": {
                "id": "448a5ddd-03b2-4c10-ad68-4871511edf42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7a4d6d1-7ea2-4b34-bd13-a8a82081f4ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54dc0564-93a4-475e-a64b-5ee8dfc88852",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7a4d6d1-7ea2-4b34-bd13-a8a82081f4ee",
                    "LayerId": "f25df08c-7665-47f6-a2dd-88502ae24d1c"
                }
            ]
        },
        {
            "id": "d2d7117e-d5ca-403b-8726-0912fbcd42ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "compositeImage": {
                "id": "f1623a33-6467-40b3-adbf-2a426190b936",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2d7117e-d5ca-403b-8726-0912fbcd42ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d72106b8-23ee-4998-8ab5-2f3004658cdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2d7117e-d5ca-403b-8726-0912fbcd42ea",
                    "LayerId": "f25df08c-7665-47f6-a2dd-88502ae24d1c"
                }
            ]
        },
        {
            "id": "b6046503-ceea-4899-9c06-a3a80a1a2b3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "compositeImage": {
                "id": "2cfa8b48-6907-49cc-86ff-2a8a6d5d88dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6046503-ceea-4899-9c06-a3a80a1a2b3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26611616-1fae-4202-adf7-7c8b8bde7c3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6046503-ceea-4899-9c06-a3a80a1a2b3b",
                    "LayerId": "f25df08c-7665-47f6-a2dd-88502ae24d1c"
                }
            ]
        },
        {
            "id": "c08ed26f-fd0f-4122-bebd-934c345c0888",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "compositeImage": {
                "id": "427b4604-00cc-4d93-a0d0-b219712a99f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c08ed26f-fd0f-4122-bebd-934c345c0888",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19a0bd0f-4cd7-4ca8-8dc5-b7e4653250f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c08ed26f-fd0f-4122-bebd-934c345c0888",
                    "LayerId": "f25df08c-7665-47f6-a2dd-88502ae24d1c"
                }
            ]
        },
        {
            "id": "1fd8872c-1d55-40d7-9c40-5d4cc18f4111",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "compositeImage": {
                "id": "b10a3b91-40b9-427c-a2e3-a7db7dcd8821",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fd8872c-1d55-40d7-9c40-5d4cc18f4111",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be86a564-7cb0-4ffa-b5e5-28ab2626f20e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fd8872c-1d55-40d7-9c40-5d4cc18f4111",
                    "LayerId": "f25df08c-7665-47f6-a2dd-88502ae24d1c"
                }
            ]
        },
        {
            "id": "3f3e72e8-c2b3-4197-979b-cccb1b0300ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "compositeImage": {
                "id": "2dc1ac15-e3dd-4d31-8176-c14db7da099a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f3e72e8-c2b3-4197-979b-cccb1b0300ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "499bf750-0673-4f24-8ccf-a64fb53f539d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f3e72e8-c2b3-4197-979b-cccb1b0300ce",
                    "LayerId": "f25df08c-7665-47f6-a2dd-88502ae24d1c"
                }
            ]
        },
        {
            "id": "f1628179-7619-435b-a284-a327227a71f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "compositeImage": {
                "id": "54aaa8d6-0a2b-4328-876c-18e780ac902e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1628179-7619-435b-a284-a327227a71f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8f44c63-96d1-4587-a547-b05ef700701e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1628179-7619-435b-a284-a327227a71f8",
                    "LayerId": "f25df08c-7665-47f6-a2dd-88502ae24d1c"
                }
            ]
        },
        {
            "id": "1691dbdd-bee0-4515-a515-3d260343b3f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "compositeImage": {
                "id": "548352c2-9d49-48bd-b8b5-280fe34901e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1691dbdd-bee0-4515-a515-3d260343b3f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f22d767-ff1e-4b03-b63a-94acd0f522bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1691dbdd-bee0-4515-a515-3d260343b3f8",
                    "LayerId": "f25df08c-7665-47f6-a2dd-88502ae24d1c"
                }
            ]
        },
        {
            "id": "1883ef34-9874-4b92-8f66-42850c11026f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "compositeImage": {
                "id": "cb890cb1-b7fb-4a6f-92eb-33778b8ff6ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1883ef34-9874-4b92-8f66-42850c11026f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85a30282-0417-4471-af49-f807bbb68cc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1883ef34-9874-4b92-8f66-42850c11026f",
                    "LayerId": "f25df08c-7665-47f6-a2dd-88502ae24d1c"
                }
            ]
        },
        {
            "id": "a2b343b5-2348-4005-8d9f-8086a9442b7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "compositeImage": {
                "id": "587263ff-25eb-4223-9182-dbc1f172b0e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2b343b5-2348-4005-8d9f-8086a9442b7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7b1343b-8002-4f6e-955b-803ec45206bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2b343b5-2348-4005-8d9f-8086a9442b7e",
                    "LayerId": "f25df08c-7665-47f6-a2dd-88502ae24d1c"
                }
            ]
        },
        {
            "id": "25eebcc0-ec25-42cc-ac0a-64402ae31268",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "compositeImage": {
                "id": "d13b7cee-c70b-409f-babb-e9e7ebbe6336",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25eebcc0-ec25-42cc-ac0a-64402ae31268",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e1a9d1c-391f-4b1c-9894-c62e450fa413",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25eebcc0-ec25-42cc-ac0a-64402ae31268",
                    "LayerId": "f25df08c-7665-47f6-a2dd-88502ae24d1c"
                }
            ]
        },
        {
            "id": "95710d1f-daf5-4c97-9e8b-03546f046b10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "compositeImage": {
                "id": "7edf61b1-8146-4495-96fb-3eb83626e587",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95710d1f-daf5-4c97-9e8b-03546f046b10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61cff171-e022-49e1-abaf-861cb224a75a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95710d1f-daf5-4c97-9e8b-03546f046b10",
                    "LayerId": "f25df08c-7665-47f6-a2dd-88502ae24d1c"
                }
            ]
        },
        {
            "id": "f4b388e7-cda4-4ca5-beab-41fb95960cde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "compositeImage": {
                "id": "d4968156-cffc-4f1c-83c2-29f303f04ad9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4b388e7-cda4-4ca5-beab-41fb95960cde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e88ded3a-1067-42c6-a9e6-c75c1d16a35b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4b388e7-cda4-4ca5-beab-41fb95960cde",
                    "LayerId": "f25df08c-7665-47f6-a2dd-88502ae24d1c"
                }
            ]
        },
        {
            "id": "5b5df0eb-58b2-488c-b563-5a6fa95f943b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "compositeImage": {
                "id": "4abfdafd-ec04-41ce-8812-706cde2c6b21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b5df0eb-58b2-488c-b563-5a6fa95f943b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3124c6dc-8ecd-439b-830d-2842d7f693c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b5df0eb-58b2-488c-b563-5a6fa95f943b",
                    "LayerId": "f25df08c-7665-47f6-a2dd-88502ae24d1c"
                }
            ]
        },
        {
            "id": "3694f881-f3b0-47dd-b293-b682b642586d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "compositeImage": {
                "id": "64d4ce03-261a-4b81-bae1-2bca1753f140",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3694f881-f3b0-47dd-b293-b682b642586d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "decb80ef-ae17-4a1e-91f4-18d0a3ecd1f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3694f881-f3b0-47dd-b293-b682b642586d",
                    "LayerId": "f25df08c-7665-47f6-a2dd-88502ae24d1c"
                }
            ]
        },
        {
            "id": "0f629d76-df21-4c9a-a6b1-9c60c73996ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "compositeImage": {
                "id": "6cfeef90-f5e9-43fe-bb95-3cdea72d6652",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f629d76-df21-4c9a-a6b1-9c60c73996ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e39d7cb6-8761-4917-a6c4-0a8af8b31c26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f629d76-df21-4c9a-a6b1-9c60c73996ed",
                    "LayerId": "f25df08c-7665-47f6-a2dd-88502ae24d1c"
                }
            ]
        },
        {
            "id": "5caf73c4-a178-423f-8230-1e872d41ff8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "compositeImage": {
                "id": "1330b196-ebcf-4efa-8899-dc050421a5fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5caf73c4-a178-423f-8230-1e872d41ff8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d1dc286-0041-4229-9850-f93f91ff1ea3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5caf73c4-a178-423f-8230-1e872d41ff8b",
                    "LayerId": "f25df08c-7665-47f6-a2dd-88502ae24d1c"
                }
            ]
        },
        {
            "id": "ee843c32-29df-401f-a46c-f0b65216fa0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "compositeImage": {
                "id": "82101fc6-ab7b-4d01-99b1-8d514ff60289",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee843c32-29df-401f-a46c-f0b65216fa0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "175dd3b0-561f-475d-8d1e-dd32d3893013",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee843c32-29df-401f-a46c-f0b65216fa0f",
                    "LayerId": "f25df08c-7665-47f6-a2dd-88502ae24d1c"
                }
            ]
        },
        {
            "id": "9e47133e-ecd6-442b-8fdb-0b41e8a75bdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "compositeImage": {
                "id": "33b22d06-8f27-4c26-8346-1467cbb65e35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e47133e-ecd6-442b-8fdb-0b41e8a75bdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0a09500-8c59-494b-9692-660746d25200",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e47133e-ecd6-442b-8fdb-0b41e8a75bdf",
                    "LayerId": "f25df08c-7665-47f6-a2dd-88502ae24d1c"
                }
            ]
        },
        {
            "id": "42d09579-a4ce-43d3-98db-2e2b9e578c46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "compositeImage": {
                "id": "d6d63131-c6f5-4de9-9580-b74f70bc2891",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42d09579-a4ce-43d3-98db-2e2b9e578c46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a834cd2f-9be8-46b7-a8e2-5d3251b31c25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42d09579-a4ce-43d3-98db-2e2b9e578c46",
                    "LayerId": "f25df08c-7665-47f6-a2dd-88502ae24d1c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "f25df08c-7665-47f6-a2dd-88502ae24d1c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3831f240-d8f3-4fdb-98f7-34683f6016ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}