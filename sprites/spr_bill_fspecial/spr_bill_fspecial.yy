{
    "id": "ae913c09-075f-4e7c-bed9-e7edec71f679",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_fspecial",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 49,
    "bbox_right": 107,
    "bbox_top": 74,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "60a8010e-7ba0-4f4b-ab5e-35408349b851",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "db9f2749-0e33-4147-870b-db78c931e679",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60a8010e-7ba0-4f4b-ab5e-35408349b851",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6553cfe9-0f61-4343-876e-ebab66ba6a43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60a8010e-7ba0-4f4b-ab5e-35408349b851",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "a29bcf2d-1a84-484f-98c6-cc834dcebe9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "01d93213-c27f-46ac-8b7b-a7db7a872407",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a29bcf2d-1a84-484f-98c6-cc834dcebe9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61c3d1b1-130b-4325-80cc-86a7b8cfc221",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a29bcf2d-1a84-484f-98c6-cc834dcebe9d",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "f59cac13-3ab7-4e28-a439-e3d15e6776e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "70078e0b-41c1-489c-a3f9-d9a9bb884d6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f59cac13-3ab7-4e28-a439-e3d15e6776e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c94aadbb-bdbe-4aa7-a3b0-cea0c32ba6a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f59cac13-3ab7-4e28-a439-e3d15e6776e5",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "ede971bc-a87b-44b1-980c-21921169326c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "1a10c40b-84e2-4a90-86fc-828c2d0d554f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ede971bc-a87b-44b1-980c-21921169326c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3968dffa-3717-4aab-9d77-0aa6c1caadef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ede971bc-a87b-44b1-980c-21921169326c",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "e096edda-1312-4aad-8ef1-706bf70ff419",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "9ea0ac19-aecf-417e-8ad0-417ee0f82843",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e096edda-1312-4aad-8ef1-706bf70ff419",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26046631-f4b5-4815-9ab0-0c4b570143b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e096edda-1312-4aad-8ef1-706bf70ff419",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "57605c1f-bb70-4b67-ba33-0b118060f53b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "f95b395c-51a9-4fea-8078-16d7cb6485e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57605c1f-bb70-4b67-ba33-0b118060f53b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a12ba2dc-7dfb-4fec-aefb-85d58d119e7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57605c1f-bb70-4b67-ba33-0b118060f53b",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "7299c8b4-f3b6-4a25-b86a-347a730110ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "8b780eca-f082-465f-80ac-eb319fe99c1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7299c8b4-f3b6-4a25-b86a-347a730110ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8311ce1c-8f40-4b37-a53b-297094da5ca0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7299c8b4-f3b6-4a25-b86a-347a730110ab",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "f4e5eccf-bd0b-41ea-a89d-0ce6f05b484e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "1b6573df-2984-42c5-95f9-8014167aa6eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4e5eccf-bd0b-41ea-a89d-0ce6f05b484e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84f71ab6-c57c-4790-bd99-d208b799c5f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4e5eccf-bd0b-41ea-a89d-0ce6f05b484e",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "c7875de1-8958-45e7-95ee-4c9d23b30e54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "f07ed174-7186-423a-9085-bc7a277bc4f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7875de1-8958-45e7-95ee-4c9d23b30e54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "811c71da-1226-4d58-960c-97a238d87a77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7875de1-8958-45e7-95ee-4c9d23b30e54",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "17528309-67f7-47c0-a8b3-b8ab22046f89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "46c967f7-6fe3-40a9-8663-98bae358062e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17528309-67f7-47c0-a8b3-b8ab22046f89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39b3823f-0b22-4d5f-9276-db542829e8b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17528309-67f7-47c0-a8b3-b8ab22046f89",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "c70af15e-5c97-43fb-8b2f-38eac2edac3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "9c3639d7-75e4-46b4-88f0-1526f04077c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c70af15e-5c97-43fb-8b2f-38eac2edac3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9fb6210-323a-41fc-9d01-c5352753c832",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c70af15e-5c97-43fb-8b2f-38eac2edac3a",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "37720a8e-3e39-4ab2-b70e-33cf35b1e0bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "a15f5145-7078-43b9-babe-59e2eae24b5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37720a8e-3e39-4ab2-b70e-33cf35b1e0bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ed7f197-f645-40d5-b941-0f5a7e1ba8d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37720a8e-3e39-4ab2-b70e-33cf35b1e0bb",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "71e8f572-5a1f-43a8-aca2-62eb58dbca2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "002b5357-607a-4605-b241-3eb549be6067",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71e8f572-5a1f-43a8-aca2-62eb58dbca2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5caac0eb-275d-4d4e-acec-2c66e1940db4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71e8f572-5a1f-43a8-aca2-62eb58dbca2c",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "5de64499-1784-4e9d-92de-2230806fa388",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "f48d1bdc-fea9-4902-a2d6-40916af04c35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5de64499-1784-4e9d-92de-2230806fa388",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea68ccbf-e48a-463b-8fab-1157af1becce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5de64499-1784-4e9d-92de-2230806fa388",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "61a713b6-07b2-4c4c-bbe9-5a5d625cc2cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "98ab1988-f398-4432-8cd1-e786e29e6152",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61a713b6-07b2-4c4c-bbe9-5a5d625cc2cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "525f6bac-92aa-4032-9c0f-185ba1bc995b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61a713b6-07b2-4c4c-bbe9-5a5d625cc2cd",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "79f51bf4-8349-4728-9af6-d09ff96ad4c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "518260ab-4291-407d-8235-d6cd20775a91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79f51bf4-8349-4728-9af6-d09ff96ad4c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70244844-ae6f-41ce-a567-d0f46bbb365a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79f51bf4-8349-4728-9af6-d09ff96ad4c0",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "114b6944-fabb-4158-9f90-52c6dc2d44b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "429e9413-77ee-43f8-b722-14f58de676c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "114b6944-fabb-4158-9f90-52c6dc2d44b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d141550-4b79-4c60-9a88-6e79235e5be8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "114b6944-fabb-4158-9f90-52c6dc2d44b2",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "a0f21dba-f173-4f59-a050-a8ac407e4818",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "dd4f5ea5-b058-44da-a457-771d42f29f90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0f21dba-f173-4f59-a050-a8ac407e4818",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1f8163e-792a-40b7-9063-2f714caf2e88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0f21dba-f173-4f59-a050-a8ac407e4818",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "9579b7b5-47b4-478b-b8de-a94d8a80f7b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "d05b4ac9-f10b-40d5-b487-b624eb407348",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9579b7b5-47b4-478b-b8de-a94d8a80f7b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "475201d7-db50-4981-a513-2bd124584772",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9579b7b5-47b4-478b-b8de-a94d8a80f7b3",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "877fe8cb-1def-4cb7-a409-2760ef660673",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "ab6b75d4-9afc-4db2-afe8-5b4689f13f06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "877fe8cb-1def-4cb7-a409-2760ef660673",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9896aa68-d0cc-4844-a8f8-0f219912e520",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "877fe8cb-1def-4cb7-a409-2760ef660673",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "d5a94ce9-a51b-4efc-8377-670ed801defb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "fba573cb-3be0-462c-9af0-21f7be480e03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5a94ce9-a51b-4efc-8377-670ed801defb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "844582af-9931-45a4-8cd0-4a1567e6add6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5a94ce9-a51b-4efc-8377-670ed801defb",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "9e978c49-38c3-4be5-ba4e-093779271014",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "8f665ad9-be9f-41f7-a030-a896d21c58e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e978c49-38c3-4be5-ba4e-093779271014",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ccea64c-41e2-4f39-b41d-758b89760024",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e978c49-38c3-4be5-ba4e-093779271014",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "720978cb-30e6-46a9-a4c4-9b23c3ae944a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "5a2ab64c-0ade-4f87-ba06-437b76725a8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "720978cb-30e6-46a9-a4c4-9b23c3ae944a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4045821f-f0f0-484d-8682-8fa37696e00b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "720978cb-30e6-46a9-a4c4-9b23c3ae944a",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "88970fe3-ee96-400d-b0f8-72fd6c1f0bad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "7f2ee185-32ab-433f-b4dd-e1e9074c2767",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88970fe3-ee96-400d-b0f8-72fd6c1f0bad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67eca00e-da92-46b1-a5fc-3c4469dccbe3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88970fe3-ee96-400d-b0f8-72fd6c1f0bad",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "987adaaf-db5c-4a2f-95c1-03dbbac98c84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "4d17c23e-6921-41c2-b010-8205d7bb3742",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "987adaaf-db5c-4a2f-95c1-03dbbac98c84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1241f5f-aa2c-4c9f-afc9-d9852c8cb976",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "987adaaf-db5c-4a2f-95c1-03dbbac98c84",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "8b7345fc-145e-49dd-86ea-bfa554c3ee5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "540ec782-294a-46dc-ac99-4b77711372e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b7345fc-145e-49dd-86ea-bfa554c3ee5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8385697-5c30-45fc-a050-5f32bc48bf68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b7345fc-145e-49dd-86ea-bfa554c3ee5c",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "80d40fe6-9950-469f-81e2-b92120dbbe74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "a5d4adee-95d3-4b81-87bf-2b2c54c29d88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80d40fe6-9950-469f-81e2-b92120dbbe74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "056f2d47-9665-4ce5-a265-4c36de20f502",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80d40fe6-9950-469f-81e2-b92120dbbe74",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "e5a5d862-1a59-4482-9c43-f746f6c7484b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "63ce1f41-0707-4d8c-9c78-b2f1989c3b4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5a5d862-1a59-4482-9c43-f746f6c7484b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bea7da28-f805-421f-a410-f50a9748367f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5a5d862-1a59-4482-9c43-f746f6c7484b",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "94986c9f-a288-4b81-aa28-b3d9ee8225df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "7bec18ff-704e-4cac-8a7b-b11cde344e7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94986c9f-a288-4b81-aa28-b3d9ee8225df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae0fafb4-3a51-4cc9-8467-13e23104db1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94986c9f-a288-4b81-aa28-b3d9ee8225df",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "d93e659f-3e0b-4918-bf64-593e0c212280",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "1728a6de-f5ab-407f-85f1-348ecf3494b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d93e659f-3e0b-4918-bf64-593e0c212280",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47075c31-e358-4623-acff-5fff07c43d10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d93e659f-3e0b-4918-bf64-593e0c212280",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "bfa0d4b8-d565-40a5-94d1-f4ba70f0cf22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "fbfa32c7-4ca3-4228-85fd-e2c4ac73b1cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfa0d4b8-d565-40a5-94d1-f4ba70f0cf22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfff9725-f062-4b2f-8fe8-752dfaf32fb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfa0d4b8-d565-40a5-94d1-f4ba70f0cf22",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "e28049a9-9ca7-425e-8f54-65cc005b6812",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "bacf94c8-0534-4e8e-b45a-ce76ba32f31d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e28049a9-9ca7-425e-8f54-65cc005b6812",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7affbea5-d822-45c3-af16-184b7088c4db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e28049a9-9ca7-425e-8f54-65cc005b6812",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "28d881a2-e8e0-440a-b14b-28d5b711d0e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "cdc7bc7e-ef45-4153-b89e-3e2a6aa1ddea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28d881a2-e8e0-440a-b14b-28d5b711d0e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f70d7ce4-1365-43e1-919a-7067b07a7bfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28d881a2-e8e0-440a-b14b-28d5b711d0e1",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "2bcdb1d5-3b78-415d-90a0-33a88811ac8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "8a84ffc8-275a-4019-b3ef-e7d185d029f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bcdb1d5-3b78-415d-90a0-33a88811ac8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aafe8ed5-33af-437d-ae7b-e42ee9b1cbfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bcdb1d5-3b78-415d-90a0-33a88811ac8e",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "a8e82b8b-1d22-4874-a5ff-d8ff5041bbef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "e4f9103f-cc5d-4d17-8193-a12bd443a528",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8e82b8b-1d22-4874-a5ff-d8ff5041bbef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b12f4c6e-3b50-4643-bd9b-f450116e2687",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8e82b8b-1d22-4874-a5ff-d8ff5041bbef",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "38579812-c81e-4f0c-98d1-cee56885420b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "a90b5d70-9dce-4c32-9d03-4d3c968087fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38579812-c81e-4f0c-98d1-cee56885420b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1bd2167-778b-4732-bba2-f35fa5e53235",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38579812-c81e-4f0c-98d1-cee56885420b",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "623435c7-0747-4867-9e54-37ae5dad2c76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "a2d205f7-c922-4c41-b3a0-5052f001f34f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "623435c7-0747-4867-9e54-37ae5dad2c76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af4d1e46-726e-433e-b016-19814cd70134",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "623435c7-0747-4867-9e54-37ae5dad2c76",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "657180a2-55a8-40ca-9383-cda2685ca1cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "7f97ce41-b8fb-422f-9825-3c7944affb29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "657180a2-55a8-40ca-9383-cda2685ca1cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67b95636-ca76-4618-8ec7-15209eb35339",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "657180a2-55a8-40ca-9383-cda2685ca1cf",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "8c54beaa-6d6e-4349-ad23-87ef0fba08a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "05a7a436-3a6d-4aba-ae76-3d6cb460d698",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c54beaa-6d6e-4349-ad23-87ef0fba08a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ef4f27d-bd75-4baa-8b84-3df10d5c2878",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c54beaa-6d6e-4349-ad23-87ef0fba08a8",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "392454e4-6300-40e4-8b36-87b27097c687",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "511a1629-849a-43b8-b654-56b2409a5fc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "392454e4-6300-40e4-8b36-87b27097c687",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f433d9d-0bc6-486c-a0fd-3a36065429a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "392454e4-6300-40e4-8b36-87b27097c687",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "15aabeee-a812-49aa-a7ba-d2d531cc8483",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "d586930a-262b-4bc4-8950-72fa1b93acc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15aabeee-a812-49aa-a7ba-d2d531cc8483",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "979292ba-40d4-4e09-8e93-48de52757dcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15aabeee-a812-49aa-a7ba-d2d531cc8483",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "6ab4b38f-caba-464f-9dae-3c00b133cc07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "7e1c869e-e89d-4033-90ad-204218eee509",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ab4b38f-caba-464f-9dae-3c00b133cc07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae57a01f-40dd-412d-beec-16741ea93a54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ab4b38f-caba-464f-9dae-3c00b133cc07",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "f83a7056-85ee-43d0-a509-09d477d2e586",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "66273eb5-a48b-4866-b192-394e5bca9250",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f83a7056-85ee-43d0-a509-09d477d2e586",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "267f588a-06ba-4ed8-aa1f-5be357af11ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f83a7056-85ee-43d0-a509-09d477d2e586",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "871086ec-540b-4d78-bd05-c387307e3662",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "e10def06-af10-4174-8b13-deab0d229e9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "871086ec-540b-4d78-bd05-c387307e3662",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e89045f3-5c81-42d7-b9eb-90dc13f08b5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "871086ec-540b-4d78-bd05-c387307e3662",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "66fe8ef6-80a3-414e-a93d-12cc48e61b78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "86459daf-5ca1-4d0d-a3c4-2570a44d7850",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66fe8ef6-80a3-414e-a93d-12cc48e61b78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c11942c-8262-45f6-a6fb-5015163a0566",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66fe8ef6-80a3-414e-a93d-12cc48e61b78",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "8d1a3ad9-4091-4107-b49d-3a16267911af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "767ff9a4-5411-40d8-95b9-6e8ee9f24a82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d1a3ad9-4091-4107-b49d-3a16267911af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5be64d7b-af08-48cd-9113-aa1a3c95894e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d1a3ad9-4091-4107-b49d-3a16267911af",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "a33edebf-e77f-4709-9bec-e88006bec616",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "dac3947c-b222-4631-8adc-b2028fe90ad8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a33edebf-e77f-4709-9bec-e88006bec616",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cea32bab-ddef-4580-9457-a02f4c24244b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a33edebf-e77f-4709-9bec-e88006bec616",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "6cc9be53-1395-40c4-833b-dd24f9641f14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "fff8755f-1e3f-47d3-a479-a4c6768275a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cc9be53-1395-40c4-833b-dd24f9641f14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6acb014f-1c72-4e94-85ed-cd0c73bf8e1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cc9be53-1395-40c4-833b-dd24f9641f14",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "ea7a3ef4-b298-44cf-bdc7-b46240d68cea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "89eba3c1-79b8-4294-9b9a-c5098adfeece",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea7a3ef4-b298-44cf-bdc7-b46240d68cea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f1a32af-a6e4-4cbc-a408-29fa491061d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea7a3ef4-b298-44cf-bdc7-b46240d68cea",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "f459e542-ea3e-4ca0-b0b5-ba12078ce86b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "9ea612ab-2795-44e8-828a-de717c4ff65c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f459e542-ea3e-4ca0-b0b5-ba12078ce86b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2eb86206-6ea5-49f2-b6f1-69092646107d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f459e542-ea3e-4ca0-b0b5-ba12078ce86b",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "d0e2ce8a-d549-4d0a-b46d-338eedb6c707",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "696d0238-132d-4ac0-af2b-c728675c82e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0e2ce8a-d549-4d0a-b46d-338eedb6c707",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa3b9f07-fc73-4296-9ded-aa57773a9356",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0e2ce8a-d549-4d0a-b46d-338eedb6c707",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "8415716e-6a5a-46f6-b89d-3af96099ce0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "1f2ea3f7-55fa-435a-9958-c4e1b569fdaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8415716e-6a5a-46f6-b89d-3af96099ce0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43818b58-91e5-4d98-8812-1a8bfdd4b513",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8415716e-6a5a-46f6-b89d-3af96099ce0b",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "4561cbca-261f-4ed0-b573-2e338f350eee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "b7b52baa-bf1e-404c-8131-e47795c9e5d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4561cbca-261f-4ed0-b573-2e338f350eee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf2826c9-e1a6-4127-a4a3-4e4efd060e57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4561cbca-261f-4ed0-b573-2e338f350eee",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "32c31c1b-adc4-4075-b3b5-10a9bb97914b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "67b96976-1e7e-4fbd-a6e8-5bd1e6b1e4b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32c31c1b-adc4-4075-b3b5-10a9bb97914b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b876c16-a4c8-494b-a5d6-ed6611916e85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32c31c1b-adc4-4075-b3b5-10a9bb97914b",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "1b471a95-69ec-4a7a-8d12-df537eb6e057",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "912ef10c-8967-4e51-ab91-a70bc4bda169",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b471a95-69ec-4a7a-8d12-df537eb6e057",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "157d7ead-60e0-41b6-9ae3-c3361d882e06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b471a95-69ec-4a7a-8d12-df537eb6e057",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "b1ebfb7d-7631-45ed-ba9b-ee7ebad045b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "435fdc61-f844-4841-beca-23af9290746f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1ebfb7d-7631-45ed-ba9b-ee7ebad045b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e547c79b-e993-467f-91bd-f38a72d14908",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1ebfb7d-7631-45ed-ba9b-ee7ebad045b2",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "4a28a38e-08a0-444d-b645-f52c2f1a4a3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "a6e25cbe-6eb6-4b4f-90f1-bb3b35cdc633",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a28a38e-08a0-444d-b645-f52c2f1a4a3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36b3642b-f14d-4bac-bd24-b869935c5656",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a28a38e-08a0-444d-b645-f52c2f1a4a3e",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "cf1cb721-7f9a-4860-8882-e5a1c270b079",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "83019c95-9f93-4097-a712-fd5d660f531b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf1cb721-7f9a-4860-8882-e5a1c270b079",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ad9a17e-72c9-44a1-be4c-daeb6a833261",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf1cb721-7f9a-4860-8882-e5a1c270b079",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "f7f99311-be61-458b-8d19-02b7dff7f8ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "0adc60b9-6ba7-4511-adf2-31d692ee97c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7f99311-be61-458b-8d19-02b7dff7f8ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c340f23-56bf-45a0-8783-45c853e98f68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7f99311-be61-458b-8d19-02b7dff7f8ee",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "bb6d90e2-ed41-41ae-a6de-af46818cc5ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "29e1ecf2-8ab9-4d93-b450-8b61ef0a9c9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb6d90e2-ed41-41ae-a6de-af46818cc5ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "876a2ca9-4123-4a56-9349-286baa175378",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb6d90e2-ed41-41ae-a6de-af46818cc5ef",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "f9b21a18-7d09-4c92-ad0e-f6f541372105",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "e9c18768-1745-4cda-86d2-7c0a16f7040c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9b21a18-7d09-4c92-ad0e-f6f541372105",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa3424b8-b0b4-41d1-938b-6fc2f3b88afa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9b21a18-7d09-4c92-ad0e-f6f541372105",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "1e59d236-1d0c-4d57-8d74-70da95858540",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "d035be09-57d6-4c1c-9199-f4df9478925b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e59d236-1d0c-4d57-8d74-70da95858540",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85c8e1c3-6f6b-4700-9caf-7ec2b643b367",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e59d236-1d0c-4d57-8d74-70da95858540",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "204cdfe3-8429-4be0-ac54-b69adb8df5ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "0233b9b1-92d5-47a2-aa22-82a60282cc48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "204cdfe3-8429-4be0-ac54-b69adb8df5ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af1b4d89-ba9d-4a35-b320-c334b8f9335c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "204cdfe3-8429-4be0-ac54-b69adb8df5ee",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "d47fcf9f-22b6-4e06-8ce5-aeb162295e4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "a4d0d5fc-af62-47b3-a7f5-ac9c803ad837",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d47fcf9f-22b6-4e06-8ce5-aeb162295e4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf22c49b-26de-459e-bd3e-37a169b3244d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d47fcf9f-22b6-4e06-8ce5-aeb162295e4e",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "2f115ad2-744b-4503-8717-ff3a4417fb98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "14f7e617-c5e0-4fa3-bc96-5af337483ea8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f115ad2-744b-4503-8717-ff3a4417fb98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c320bf54-c05a-4ff8-9a26-67851010e8bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f115ad2-744b-4503-8717-ff3a4417fb98",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "bdd99263-0a7d-45e9-bbdc-39c73acf1cc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "99af3310-9b93-452f-8ca9-3d3275fcd496",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdd99263-0a7d-45e9-bbdc-39c73acf1cc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b2e97ef-6012-4329-a72b-79948e36330a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdd99263-0a7d-45e9-bbdc-39c73acf1cc1",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "8da37227-3022-4c9a-93d6-56b2a62c1bc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "2d236e43-b045-4c83-b26c-3606de6d6d43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8da37227-3022-4c9a-93d6-56b2a62c1bc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb9ca7de-6629-42f9-bd3b-9118000fc189",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8da37227-3022-4c9a-93d6-56b2a62c1bc3",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        },
        {
            "id": "82f9aab7-c823-44c1-b895-19f71a529f8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "compositeImage": {
                "id": "6908bcf1-d170-4387-b7b8-e0c9d78f9fb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82f9aab7-c823-44c1-b895-19f71a529f8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4752c697-25cc-42ec-8414-6f4aa499963e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82f9aab7-c823-44c1-b895-19f71a529f8c",
                    "LayerId": "c84210b5-25e1-4c83-bd1d-13c93278b90c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "c84210b5-25e1-4c83-bd1d-13c93278b90c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae913c09-075f-4e7c-bed9-e7edec71f679",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}