{
    "id": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dsyn_jm",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 45,
    "bbox_right": 113,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "001d380e-4365-43ed-ba4e-7d8ffa07e7f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "compositeImage": {
                "id": "7b95ddaa-984e-4ca3-945a-c7f3384fa398",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "001d380e-4365-43ed-ba4e-7d8ffa07e7f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64622c81-8bee-498d-b628-9917a86e9d4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "001d380e-4365-43ed-ba4e-7d8ffa07e7f4",
                    "LayerId": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4"
                }
            ]
        },
        {
            "id": "51456642-ac78-4f9f-8567-73fec863dfe9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "compositeImage": {
                "id": "783dbd40-a527-4b34-b41d-862444fc449c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51456642-ac78-4f9f-8567-73fec863dfe9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "913bd7ee-cd3c-4040-bdf8-a864916aa814",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51456642-ac78-4f9f-8567-73fec863dfe9",
                    "LayerId": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4"
                }
            ]
        },
        {
            "id": "7b7ad5d1-1e8a-45e1-9abf-5576dcb0bb84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "compositeImage": {
                "id": "48505621-6e18-435c-bc38-d1ee57ac4a4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b7ad5d1-1e8a-45e1-9abf-5576dcb0bb84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61d82efe-82a1-4491-9e5f-bbb2fea2a114",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b7ad5d1-1e8a-45e1-9abf-5576dcb0bb84",
                    "LayerId": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4"
                }
            ]
        },
        {
            "id": "89588bd5-1482-4d32-8e20-3280388ba092",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "compositeImage": {
                "id": "aff8553e-5a57-44ed-9b31-a8760bf65d22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89588bd5-1482-4d32-8e20-3280388ba092",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4c071ed-026e-4a77-8bda-35fc4152c0aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89588bd5-1482-4d32-8e20-3280388ba092",
                    "LayerId": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4"
                }
            ]
        },
        {
            "id": "ca92571d-60b7-47d1-baf4-e8f4d9163ae3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "compositeImage": {
                "id": "0f5dce9b-e336-4fda-b023-ffefcbd5762b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca92571d-60b7-47d1-baf4-e8f4d9163ae3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04a25df5-20ab-4f86-830b-5d274ce83326",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca92571d-60b7-47d1-baf4-e8f4d9163ae3",
                    "LayerId": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4"
                }
            ]
        },
        {
            "id": "93bae3d9-09c1-40e2-b2c7-4426942e9170",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "compositeImage": {
                "id": "290d77c5-54b5-4908-8a51-17aa839a85b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93bae3d9-09c1-40e2-b2c7-4426942e9170",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3b9e4d7-6ad6-4931-b4d5-788589e99479",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93bae3d9-09c1-40e2-b2c7-4426942e9170",
                    "LayerId": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4"
                }
            ]
        },
        {
            "id": "cbd099e6-f73f-4d3e-a928-e521e4205793",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "compositeImage": {
                "id": "9697a406-c713-4c1a-a30c-e7726bfa5ab4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbd099e6-f73f-4d3e-a928-e521e4205793",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbd88cb8-0f05-4f7e-b792-20a1af868b74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbd099e6-f73f-4d3e-a928-e521e4205793",
                    "LayerId": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4"
                }
            ]
        },
        {
            "id": "83be349c-c6f6-4e25-8820-4927495a36ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "compositeImage": {
                "id": "240784a0-7195-4e2d-8da4-204c73664d43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83be349c-c6f6-4e25-8820-4927495a36ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd31aefc-33cb-478b-907b-671411687657",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83be349c-c6f6-4e25-8820-4927495a36ce",
                    "LayerId": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4"
                }
            ]
        },
        {
            "id": "f0975514-6cf8-4652-9813-84bf68e76339",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "compositeImage": {
                "id": "a68c8ae5-be1b-428e-bd2b-274e2996f772",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0975514-6cf8-4652-9813-84bf68e76339",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7614969e-bd6d-40d1-8217-88c66bbf7775",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0975514-6cf8-4652-9813-84bf68e76339",
                    "LayerId": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4"
                }
            ]
        },
        {
            "id": "710b939a-1251-4ef2-94be-09aba9feb141",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "compositeImage": {
                "id": "acbe0c5c-dd71-4194-9d1a-b89093684e1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "710b939a-1251-4ef2-94be-09aba9feb141",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19a88a36-6653-42d2-a9cf-263811138a93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "710b939a-1251-4ef2-94be-09aba9feb141",
                    "LayerId": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4"
                }
            ]
        },
        {
            "id": "a0df8df8-34ec-424a-81e0-61f4627bf2d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "compositeImage": {
                "id": "ececf0b6-e7a8-4178-8bff-e34c65ff6374",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0df8df8-34ec-424a-81e0-61f4627bf2d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e31a241b-2224-4ff9-8645-b4878b4ce844",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0df8df8-34ec-424a-81e0-61f4627bf2d8",
                    "LayerId": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4"
                }
            ]
        },
        {
            "id": "72c66420-fede-48a7-9642-fe40d59cc5bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "compositeImage": {
                "id": "9edf2a09-3222-4693-844c-254b811b0984",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72c66420-fede-48a7-9642-fe40d59cc5bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5eb52612-561a-4798-bc3e-8df17826a3de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72c66420-fede-48a7-9642-fe40d59cc5bc",
                    "LayerId": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4"
                }
            ]
        },
        {
            "id": "83edb494-7559-450a-b431-c3202addfbb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "compositeImage": {
                "id": "df96025d-b8e4-428d-a3b1-cf2a72059d89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83edb494-7559-450a-b431-c3202addfbb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac234502-ed43-4e85-984f-aac76bbb6a66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83edb494-7559-450a-b431-c3202addfbb2",
                    "LayerId": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4"
                }
            ]
        },
        {
            "id": "7c0d3b78-f99b-44bb-9cb0-a2bbbd34fc2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "compositeImage": {
                "id": "26c43334-677b-4279-835e-cfe4336d640e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c0d3b78-f99b-44bb-9cb0-a2bbbd34fc2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02aa0343-526d-485b-ae65-34e68eaf1398",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c0d3b78-f99b-44bb-9cb0-a2bbbd34fc2e",
                    "LayerId": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4"
                }
            ]
        },
        {
            "id": "e89f2cb7-a916-4284-9428-eea4b79ba653",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "compositeImage": {
                "id": "9d06250d-41de-4c73-bc8d-c80db66b66d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e89f2cb7-a916-4284-9428-eea4b79ba653",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11c84a1d-6a35-46b6-9e06-b49b2b2ab21d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e89f2cb7-a916-4284-9428-eea4b79ba653",
                    "LayerId": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4"
                }
            ]
        },
        {
            "id": "94ea3ab4-5d2e-40c9-ac67-93ad65a974b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "compositeImage": {
                "id": "c65010ed-4d78-42b3-89dc-96945d24d14b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94ea3ab4-5d2e-40c9-ac67-93ad65a974b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a278775c-3789-49fe-ab3b-588f612f09e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94ea3ab4-5d2e-40c9-ac67-93ad65a974b7",
                    "LayerId": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4"
                }
            ]
        },
        {
            "id": "999b6731-3ebb-4258-a565-ae24dad54524",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "compositeImage": {
                "id": "f41b9b94-2f2f-4cbf-ac59-8ec57a5fb89d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "999b6731-3ebb-4258-a565-ae24dad54524",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ac03c21-073e-4dfe-9e7b-cfd2c1decaf5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "999b6731-3ebb-4258-a565-ae24dad54524",
                    "LayerId": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4"
                }
            ]
        },
        {
            "id": "9d391a99-d05f-4c3f-8e2e-02f85a8d7139",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "compositeImage": {
                "id": "f90bf40b-aff4-405b-9924-950fb8604b9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d391a99-d05f-4c3f-8e2e-02f85a8d7139",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c5b738e-d7f7-434a-908e-d09e34fd8cd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d391a99-d05f-4c3f-8e2e-02f85a8d7139",
                    "LayerId": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4"
                }
            ]
        },
        {
            "id": "2ee93e5f-bb08-464f-8f41-495879806e17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "compositeImage": {
                "id": "0c769817-e764-4d8a-a4eb-5cdc5e24abb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ee93e5f-bb08-464f-8f41-495879806e17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d72762c-5ea7-414d-8284-b447a4597aea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ee93e5f-bb08-464f-8f41-495879806e17",
                    "LayerId": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4"
                }
            ]
        },
        {
            "id": "9d63e1e7-59ea-4d6b-a4e0-dfab11d4a8c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "compositeImage": {
                "id": "b2f4f479-56bf-4813-bc03-d4035194f5df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d63e1e7-59ea-4d6b-a4e0-dfab11d4a8c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c02db1f-8833-4dda-b5aa-e0f79a5d05ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d63e1e7-59ea-4d6b-a4e0-dfab11d4a8c4",
                    "LayerId": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4"
                }
            ]
        },
        {
            "id": "7d8f5557-8f84-44b8-ab96-8fe8c947e876",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "compositeImage": {
                "id": "80b84aff-f5ae-45af-9c84-27c492693774",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d8f5557-8f84-44b8-ab96-8fe8c947e876",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ba7b92f-938c-46dc-a34d-42742a72e12a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d8f5557-8f84-44b8-ab96-8fe8c947e876",
                    "LayerId": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4"
                }
            ]
        },
        {
            "id": "c528a562-3847-4a30-ae0c-9979fc5a39cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "compositeImage": {
                "id": "a2f7ebc5-c732-43ed-9980-8a33b79788ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c528a562-3847-4a30-ae0c-9979fc5a39cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e27317d0-62f7-4f24-8d1d-358778929eef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c528a562-3847-4a30-ae0c-9979fc5a39cc",
                    "LayerId": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4"
                }
            ]
        },
        {
            "id": "5e9e149f-4c6c-46e7-8336-651d36df83c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "compositeImage": {
                "id": "af950774-c8e4-4394-b4b0-8d9488be27d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e9e149f-4c6c-46e7-8336-651d36df83c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18ef39f2-01c5-44e4-bfae-3a91be42c781",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e9e149f-4c6c-46e7-8336-651d36df83c7",
                    "LayerId": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4"
                }
            ]
        },
        {
            "id": "ffbba196-171a-4ff1-a175-51aab05bb565",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "compositeImage": {
                "id": "6c05d0e5-3ca8-4413-b4b6-1e726b3d732d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffbba196-171a-4ff1-a175-51aab05bb565",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7956b41e-c9b9-4567-8f9c-918e5c18c3b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffbba196-171a-4ff1-a175-51aab05bb565",
                    "LayerId": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4"
                }
            ]
        },
        {
            "id": "bfefa6a2-80d9-45ca-a92b-ab830c07d6a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "compositeImage": {
                "id": "9fb4b5ea-29ed-4fe5-aee6-34339ddeac2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfefa6a2-80d9-45ca-a92b-ab830c07d6a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3e7ad6b-84f2-4f4c-b753-d5d71f1e89d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfefa6a2-80d9-45ca-a92b-ab830c07d6a8",
                    "LayerId": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4"
                }
            ]
        },
        {
            "id": "b6c86e65-86ac-43f0-b6d9-ca5ad8fe10c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "compositeImage": {
                "id": "88fa76bc-b832-4360-a236-c852f080779b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6c86e65-86ac-43f0-b6d9-ca5ad8fe10c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06068eec-2029-4a8c-bd84-e046560b34b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6c86e65-86ac-43f0-b6d9-ca5ad8fe10c7",
                    "LayerId": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4"
                }
            ]
        },
        {
            "id": "698b8804-ba45-4ef6-9272-705c4c74fb0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "compositeImage": {
                "id": "506286a6-9510-438a-87dd-c6144c43e717",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "698b8804-ba45-4ef6-9272-705c4c74fb0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc8abda7-2161-4102-862d-802c516a8503",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "698b8804-ba45-4ef6-9272-705c4c74fb0b",
                    "LayerId": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4"
                }
            ]
        },
        {
            "id": "22a1a75d-5272-4229-b209-46e9bc98fd20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "compositeImage": {
                "id": "d00cb7d8-c06e-4058-910f-946590c367f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22a1a75d-5272-4229-b209-46e9bc98fd20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4d59a55-dbc8-4517-86e7-8aa623dc1d12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22a1a75d-5272-4229-b209-46e9bc98fd20",
                    "LayerId": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "6a293af6-0ec7-41ae-983a-f1cbdfd1a5b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "04283ee4-8f2a-4c69-bfbc-3653d11230fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}