{
    "id": "47070b47-56b2-4c21-9f5d-3044e679b4d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_crouch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 48,
    "bbox_right": 94,
    "bbox_top": 91,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3a1c7c24-ab8c-4c88-b357-7712b29fbc49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47070b47-56b2-4c21-9f5d-3044e679b4d0",
            "compositeImage": {
                "id": "381383d0-a37b-4a47-9991-1e15290265bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a1c7c24-ab8c-4c88-b357-7712b29fbc49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "262852f1-0dbb-433e-8dbd-adb2d1f9c44b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a1c7c24-ab8c-4c88-b357-7712b29fbc49",
                    "LayerId": "e558a518-b160-4692-8772-873b3275666d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "e558a518-b160-4692-8772-873b3275666d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "47070b47-56b2-4c21-9f5d-3044e679b4d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}