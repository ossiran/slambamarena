{
    "id": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_cspecial",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a9d4a002-fa7d-4040-a15c-8ccd4d54ac30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "0d69b52a-50a6-4537-b816-1269fae4af90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9d4a002-fa7d-4040-a15c-8ccd4d54ac30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2a09b8a-3fee-4a2e-9836-6397a789fbef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9d4a002-fa7d-4040-a15c-8ccd4d54ac30",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "039cec96-4b19-43d2-a68b-890396e08ddc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "06f8c481-cdbe-4eee-9677-370fd4ef484c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "039cec96-4b19-43d2-a68b-890396e08ddc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "556ff68f-06b3-45d5-9537-31a1e1ae3f00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "039cec96-4b19-43d2-a68b-890396e08ddc",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "b727c9c1-948a-4aa7-ad86-cdd480ace1d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "72545766-4b5f-4758-b5c9-64d7d42ee2af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b727c9c1-948a-4aa7-ad86-cdd480ace1d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73fc8f0a-4775-4609-88f5-8715d427a63d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b727c9c1-948a-4aa7-ad86-cdd480ace1d8",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "05fa4224-6257-4249-b360-a13e868922b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "f3e985a4-ec12-45dd-adeb-a3a0c9c68f52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05fa4224-6257-4249-b360-a13e868922b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88921d31-46ef-4450-a21b-5cf43d6c1bd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05fa4224-6257-4249-b360-a13e868922b5",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "96bf1368-6fac-4a62-a9e7-63fb3b53240a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "46645f92-8f7f-487b-b0c2-7ecda0c4f955",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96bf1368-6fac-4a62-a9e7-63fb3b53240a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4e3bcd8-7dc0-406d-816d-c06ef564a4d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96bf1368-6fac-4a62-a9e7-63fb3b53240a",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "0f037a65-b2de-48a8-b4be-a84cf7493353",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "5760e025-29e6-4448-9c44-b952a5dd6263",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f037a65-b2de-48a8-b4be-a84cf7493353",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9ff1400-0863-4f53-98d0-a89d658357ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f037a65-b2de-48a8-b4be-a84cf7493353",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "2a460b3a-ba7d-4881-81cb-6c4e3936f899",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "87ddf11e-d7d1-43f1-8626-f87a96b7187e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a460b3a-ba7d-4881-81cb-6c4e3936f899",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "531bf356-12e8-4c34-9bbd-7685e9b2d0bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a460b3a-ba7d-4881-81cb-6c4e3936f899",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "b1e05ad4-ab14-496c-96e7-2e41002ec449",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "8d45df65-0af7-468b-9c8e-6751cab8cf4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1e05ad4-ab14-496c-96e7-2e41002ec449",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a81e9375-0bfe-4c23-a8fe-f54eb16d2876",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1e05ad4-ab14-496c-96e7-2e41002ec449",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "c5f0598a-e68f-48e6-9722-23016721a84a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "43c29e5d-a08d-42b7-a70f-75444196a443",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5f0598a-e68f-48e6-9722-23016721a84a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f15ba444-ef5f-4c05-81f9-820efe6325d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5f0598a-e68f-48e6-9722-23016721a84a",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "cc11780b-afe9-4ccf-882a-02fda3f3a110",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "b6c2c5ae-3cd6-436b-abaf-a9809c35455a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc11780b-afe9-4ccf-882a-02fda3f3a110",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f890b7db-f8aa-4ca3-87ac-4db0131ed473",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc11780b-afe9-4ccf-882a-02fda3f3a110",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "0554b4ee-0574-41cd-8a7e-2d1d90bd2e2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "24f919ee-113a-4da7-9477-cb62e8ebd5c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0554b4ee-0574-41cd-8a7e-2d1d90bd2e2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d852ac5d-80a1-4b1d-bae6-585e77333c71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0554b4ee-0574-41cd-8a7e-2d1d90bd2e2d",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "95a70414-04e3-46e9-8a77-6d64eb54fdd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "1e9cd9f5-9e73-47fc-bd2b-006df0365aa7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95a70414-04e3-46e9-8a77-6d64eb54fdd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48825e33-5a8b-4d67-81be-6eb4b59dce09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95a70414-04e3-46e9-8a77-6d64eb54fdd4",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "e0cddd15-0131-4c8e-ab7c-31a172c8a03f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "3795d946-4371-460f-abb4-05baad8a916d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0cddd15-0131-4c8e-ab7c-31a172c8a03f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0256abe8-e6fe-4adb-9851-a7624db77409",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0cddd15-0131-4c8e-ab7c-31a172c8a03f",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "d48ce945-faf4-4bd1-8ba1-8d1b9bebd75b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "10cfd970-5bff-420d-9026-ef809178035b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d48ce945-faf4-4bd1-8ba1-8d1b9bebd75b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad04d0d9-c90f-48ca-9e12-7d10b8eba4e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d48ce945-faf4-4bd1-8ba1-8d1b9bebd75b",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "7fca63aa-ef49-4354-933f-e85d3bd7c721",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "8fef83c5-55d6-4e86-a20a-b64bd9cb97b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fca63aa-ef49-4354-933f-e85d3bd7c721",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cb9ef32-459b-453e-a9e5-70f8cc20f38a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fca63aa-ef49-4354-933f-e85d3bd7c721",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "6935531e-e9c1-485c-8353-b7b82b4213ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "0115af63-5b0e-48f1-8b27-6330e0f1c276",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6935531e-e9c1-485c-8353-b7b82b4213ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee661e5d-d16b-4070-abe3-3e3d3a229661",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6935531e-e9c1-485c-8353-b7b82b4213ed",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "7cef4997-5350-4957-acc1-a38ca415c48c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "73050c13-8f66-4eff-879e-a3d31b66ce9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cef4997-5350-4957-acc1-a38ca415c48c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b4e092f-0ede-4376-99e6-ce085e93ccbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cef4997-5350-4957-acc1-a38ca415c48c",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "b3aa0125-d0de-4227-a2c5-0ead827acdbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "4ca50fd6-3e50-482c-ae2b-878f7137c127",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3aa0125-d0de-4227-a2c5-0ead827acdbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2f46217-49fb-4677-8c57-21a43030b72e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3aa0125-d0de-4227-a2c5-0ead827acdbe",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "79d9e5be-3b83-484f-a96f-63c67d9ed08e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "1576678e-c84b-4ef3-9bbf-74acd6c2bc7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79d9e5be-3b83-484f-a96f-63c67d9ed08e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fae8109-05ec-4b1d-871d-f65ef44a740f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79d9e5be-3b83-484f-a96f-63c67d9ed08e",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "94c3aba8-bc64-4b4a-ac86-c9bd5b7a8dcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "5df7c246-cd3c-4c32-8253-0677b153c189",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94c3aba8-bc64-4b4a-ac86-c9bd5b7a8dcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ac8302f-168e-46b8-abbc-83cc93209d05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94c3aba8-bc64-4b4a-ac86-c9bd5b7a8dcc",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "2735d456-d339-495f-a835-1b554a1ba981",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "6c0ace70-b393-4b42-960b-0c2a6f971ea0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2735d456-d339-495f-a835-1b554a1ba981",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb2b63d2-1db0-497e-a502-c4329e8637f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2735d456-d339-495f-a835-1b554a1ba981",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "13af1aa6-feea-4b5b-bf81-01b2c7356d00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "76b3563c-52f3-4b58-a6b8-63129e6ba628",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13af1aa6-feea-4b5b-bf81-01b2c7356d00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3307b4a-2105-47c6-9a44-5c9336f1a5a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13af1aa6-feea-4b5b-bf81-01b2c7356d00",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "6b4106a9-add8-40c4-826c-a87e841b9be1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "d8f00d9e-d1a6-413f-b184-547c41ed0a03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b4106a9-add8-40c4-826c-a87e841b9be1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c9a5c5e-ac99-4494-b7b5-4082f55521b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b4106a9-add8-40c4-826c-a87e841b9be1",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "2d948dbf-6dfe-4d26-8cf8-45a835137356",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "f3dc5db5-08fa-4692-baff-1a9cc6554fba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d948dbf-6dfe-4d26-8cf8-45a835137356",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5426107a-33a9-4a0d-8360-cb57a5933359",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d948dbf-6dfe-4d26-8cf8-45a835137356",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "91601632-908c-4dc2-a3a9-c4eef856a5fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "2ea4d131-7671-4c41-b2cd-1164bc538452",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91601632-908c-4dc2-a3a9-c4eef856a5fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13545402-53ce-421f-a0bb-71cc152b0f23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91601632-908c-4dc2-a3a9-c4eef856a5fa",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "7e6e629b-4ae5-40b0-914b-ba0f44a0a677",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "1ce6dd7c-d371-4f76-9e4b-a313bb88288d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e6e629b-4ae5-40b0-914b-ba0f44a0a677",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "390f501e-d6ef-4766-a3a7-daf489be00b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e6e629b-4ae5-40b0-914b-ba0f44a0a677",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "15a7f452-1a94-4288-82c6-b1ecd2fbe3dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "5f7b5a33-ffd0-4437-a5dc-797f6d5eab84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15a7f452-1a94-4288-82c6-b1ecd2fbe3dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d9ea319-5fa9-4e40-8ccb-db6a5709cc05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15a7f452-1a94-4288-82c6-b1ecd2fbe3dc",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "7952e427-6100-4c01-9633-341ea130d73a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "bcc492c1-f255-41c3-ae42-bf688f84e404",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7952e427-6100-4c01-9633-341ea130d73a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faf64ad1-ac4d-4440-828f-67c601468672",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7952e427-6100-4c01-9633-341ea130d73a",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "670da8dd-9f95-4d6b-8e4c-36dd77ebc591",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "cbeb1f85-e8e0-4480-a09c-0fb6a6ea09cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "670da8dd-9f95-4d6b-8e4c-36dd77ebc591",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfe8baa6-3f65-4f0d-8be5-0198a740a40c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "670da8dd-9f95-4d6b-8e4c-36dd77ebc591",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "622dee96-ff4a-4aba-bc25-d54a235538ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "16fa28b4-e46a-4442-8426-c16e436e285e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "622dee96-ff4a-4aba-bc25-d54a235538ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "281ae9f9-2916-4ea0-998f-e046da4f58fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "622dee96-ff4a-4aba-bc25-d54a235538ed",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "727e312c-0afb-436c-87b5-b4c8131ae185",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "0227053a-144a-441b-bb38-049ecdf45d53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "727e312c-0afb-436c-87b5-b4c8131ae185",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c2c5561-817a-495e-98fc-7b76e53de5e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "727e312c-0afb-436c-87b5-b4c8131ae185",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "3ae3bb24-7d22-4bb5-a039-dd82b76a83fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "016053dd-5cfe-4f7b-821a-f0ab258f36c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ae3bb24-7d22-4bb5-a039-dd82b76a83fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6aed5ca6-cd79-46c7-8b44-cf3d8c33a139",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ae3bb24-7d22-4bb5-a039-dd82b76a83fc",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "d6e5667d-2ed3-4880-b1dd-73dba24bb3e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "3315549f-cb9c-47db-82bc-4c3d25588c96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6e5667d-2ed3-4880-b1dd-73dba24bb3e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62e01ad4-9c7e-4ba5-a4c4-6ea482e0d663",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6e5667d-2ed3-4880-b1dd-73dba24bb3e7",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "a0747a74-9bd5-4adb-8ea7-ee73199da45b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "6beb7760-e000-4078-84d6-c5da58ce1d1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0747a74-9bd5-4adb-8ea7-ee73199da45b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edf457b8-16c1-4a5c-81b3-552d4222cc3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0747a74-9bd5-4adb-8ea7-ee73199da45b",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "d2745e54-b4c9-45e7-874c-35e70e4957ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "3467b9ab-309a-4527-af9f-81b6b6c10eaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2745e54-b4c9-45e7-874c-35e70e4957ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0590ea56-07d1-4251-9a85-1ffc6ae6899a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2745e54-b4c9-45e7-874c-35e70e4957ea",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "0a8e5c7d-94d7-40c2-8132-c781d8968f7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "42f91b92-34ce-48a1-a92f-2143b2235f3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a8e5c7d-94d7-40c2-8132-c781d8968f7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "953b9d8b-877c-4c82-bace-8485720d0795",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a8e5c7d-94d7-40c2-8132-c781d8968f7a",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "3f5c3d09-01fd-42cd-9317-31384fca03fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "2f0a5a0d-d7c6-40a2-9d92-4a9023bc9743",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f5c3d09-01fd-42cd-9317-31384fca03fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d6a5977-28d1-4745-aff8-7a6393926182",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f5c3d09-01fd-42cd-9317-31384fca03fe",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "1e9ae6e4-c2f1-4246-8a67-18f00f6b09f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "381bf658-8fe4-4ef8-a06e-6d07909c4687",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e9ae6e4-c2f1-4246-8a67-18f00f6b09f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7232514c-aae7-407f-b48a-7064ac8cf9a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e9ae6e4-c2f1-4246-8a67-18f00f6b09f2",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "9e83bd18-9d54-4f51-8042-935f32e9a8d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "eace3b04-f394-40a8-966a-c32e2260fdbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e83bd18-9d54-4f51-8042-935f32e9a8d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "650f41df-c432-408f-b060-7bb599fe0e97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e83bd18-9d54-4f51-8042-935f32e9a8d5",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "28058f73-7e12-4750-9ce5-e3c5175a40e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "0c8ab7a1-21d1-4c23-89a2-a35066652911",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28058f73-7e12-4750-9ce5-e3c5175a40e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0007f8ce-7a56-495a-9904-62215d13ff03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28058f73-7e12-4750-9ce5-e3c5175a40e7",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "baabcdfd-c587-4eba-b9e4-33416c08f8d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "cb76f285-abc9-4a0c-916f-ab265caada58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "baabcdfd-c587-4eba-b9e4-33416c08f8d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a191a070-8b7f-40e2-bf1c-114de6af7445",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "baabcdfd-c587-4eba-b9e4-33416c08f8d3",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "7d0f8a9b-1d69-499b-804a-811a2cf45ac5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "e506f758-9d3b-4085-862f-35077ccd69b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d0f8a9b-1d69-499b-804a-811a2cf45ac5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d043e227-2543-411b-aed6-77e1123dba91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d0f8a9b-1d69-499b-804a-811a2cf45ac5",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "69103052-bf9b-45ac-bfda-e91b4d0cbf37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "19451819-9fa0-43b1-a2a2-89f939862c97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69103052-bf9b-45ac-bfda-e91b4d0cbf37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fc150ca-acf8-4bfb-9853-705f287df39e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69103052-bf9b-45ac-bfda-e91b4d0cbf37",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "cb85edd8-79a4-4237-a811-22282cb0e024",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "b9377ac5-3b10-40de-abc7-3d71f84d7c25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb85edd8-79a4-4237-a811-22282cb0e024",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13691cd7-f63e-4ff2-b761-21377e7d48b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb85edd8-79a4-4237-a811-22282cb0e024",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "372f736f-053f-4371-b481-cf84eece4fb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "d007aa78-ca96-4aab-aea2-632c375e5509",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "372f736f-053f-4371-b481-cf84eece4fb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a7c9e9d-eeef-4604-98c8-35bc22971375",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "372f736f-053f-4371-b481-cf84eece4fb4",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "c79349dd-e3d8-4e87-9e6d-3b2e01d80d80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "ef63cd3f-e041-480a-9c2c-ba096c61628c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c79349dd-e3d8-4e87-9e6d-3b2e01d80d80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a20dec6-2ccb-4893-80fe-a453d7bb6215",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c79349dd-e3d8-4e87-9e6d-3b2e01d80d80",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "88cb2759-473d-4da9-b164-b6b2771fedab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "6c85fdae-13e1-4b37-942e-05ee5c4f0b96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88cb2759-473d-4da9-b164-b6b2771fedab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25eb1190-759c-4a91-a9f0-14b5f363cbe6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88cb2759-473d-4da9-b164-b6b2771fedab",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "b21a5e22-2aae-4b71-84d0-4cdbf43ee3d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "9103813c-a936-41e4-947e-de058b8a1aa2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b21a5e22-2aae-4b71-84d0-4cdbf43ee3d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81c1052b-a99d-40cc-b9f3-5a26e4c20fbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b21a5e22-2aae-4b71-84d0-4cdbf43ee3d9",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "26f69770-4c2f-45d8-85e3-b9ada4cf012f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "fc4e9212-ccf1-431e-ac9a-1727915c59e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26f69770-4c2f-45d8-85e3-b9ada4cf012f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "844b3aad-5305-4495-9609-b85ccf4f0061",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26f69770-4c2f-45d8-85e3-b9ada4cf012f",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "8539c813-6a73-4f6d-af8e-068b15e34883",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "a23cb734-64aa-4c18-bc00-6354757a5d61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8539c813-6a73-4f6d-af8e-068b15e34883",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcc1cdc5-0a99-420f-ab34-5ece2edfe92d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8539c813-6a73-4f6d-af8e-068b15e34883",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "dff9683c-7816-4804-8f48-47651257b46f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "3ffced9d-f88a-4b8f-9992-748a9ec46809",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dff9683c-7816-4804-8f48-47651257b46f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57e92a63-eff6-417e-acfd-cbf20536ae7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dff9683c-7816-4804-8f48-47651257b46f",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "e82d63f5-86b6-44cd-89ab-505de175a639",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "14354c1a-3915-49ba-a386-74523d8dbb9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e82d63f5-86b6-44cd-89ab-505de175a639",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e4eedde-1aa8-4350-9763-647c25bc144c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e82d63f5-86b6-44cd-89ab-505de175a639",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "ce9ac17b-a15c-4b4b-82d9-ab794309084c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "199b245a-7ef7-454d-98a6-817002bd8dff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce9ac17b-a15c-4b4b-82d9-ab794309084c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0eaca5e8-1aa7-4530-bd2b-62a8700b648d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce9ac17b-a15c-4b4b-82d9-ab794309084c",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "21cad1a1-8c2d-42eb-9c53-0de2dbe87f1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "d7c45ea3-8d40-4734-a14c-1ccbf06b5629",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21cad1a1-8c2d-42eb-9c53-0de2dbe87f1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cc1ed66-ad7e-4c96-ade3-4f6b2a0a8dd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21cad1a1-8c2d-42eb-9c53-0de2dbe87f1c",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "1147a377-ccb9-4cf2-a7c7-542fc9f7aba5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "830c8232-301e-4b87-b079-56e44162d497",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1147a377-ccb9-4cf2-a7c7-542fc9f7aba5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "306505a7-47b2-4ccf-a4c6-372e5558273f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1147a377-ccb9-4cf2-a7c7-542fc9f7aba5",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "67e982a2-741a-4da7-bd04-9cec0d1aade8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "ea7451b9-e87a-4a40-9939-f0b4631f4ab2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67e982a2-741a-4da7-bd04-9cec0d1aade8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "626b7511-2549-4969-87ea-19eda9b38b96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67e982a2-741a-4da7-bd04-9cec0d1aade8",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "490359a2-cec1-46e7-9d99-b4250994432a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "d4f826ea-1b80-4081-8b87-099eed0a5c84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "490359a2-cec1-46e7-9d99-b4250994432a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f671f90a-a5bc-4cb6-9bea-69a7aa1cc8d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "490359a2-cec1-46e7-9d99-b4250994432a",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "588904e7-0ce6-4c84-8672-37c3224c90a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "811e7cbd-4545-4acb-b421-ac02dc14200c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "588904e7-0ce6-4c84-8672-37c3224c90a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bda61cae-9db9-4294-97c2-832ca2d61ca7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "588904e7-0ce6-4c84-8672-37c3224c90a8",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "638c7848-5454-45a2-b6c7-2922494b910b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "93f3154b-9382-4c58-b5b2-51aab9e1b510",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "638c7848-5454-45a2-b6c7-2922494b910b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3aba3ffb-66eb-4d7c-9403-1cde616c2a63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "638c7848-5454-45a2-b6c7-2922494b910b",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "7737493e-2112-4930-b7db-c829a91202ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "0660e769-df0e-4b8d-9407-4a882e54afd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7737493e-2112-4930-b7db-c829a91202ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4688aec-e723-470c-ab26-460512791e4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7737493e-2112-4930-b7db-c829a91202ca",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "d4b86d46-19fc-4363-9934-c04ce7d0c345",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "42da0899-6407-429d-9327-8e47ce735efc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4b86d46-19fc-4363-9934-c04ce7d0c345",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a244b0ba-992a-49ad-a6f1-220b894cd708",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4b86d46-19fc-4363-9934-c04ce7d0c345",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "58b2c3a9-d5c3-483c-8d7f-96b241f85d9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "53980afa-eaba-4f07-8ed7-3692fca7d66a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58b2c3a9-d5c3-483c-8d7f-96b241f85d9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c18f2d8-5aa2-4d3d-95d0-c6cb491bc727",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58b2c3a9-d5c3-483c-8d7f-96b241f85d9a",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "79896b69-5724-4074-be97-1bff64f57ed8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "66967889-236a-46df-8679-e4170d7a3c2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79896b69-5724-4074-be97-1bff64f57ed8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c78da7fb-bd13-4146-a54a-b74df2e538e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79896b69-5724-4074-be97-1bff64f57ed8",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "c2f1ea96-14d1-4ad2-b84d-2dfae6d3c674",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "41323c88-6787-41fe-b706-1af53037d123",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2f1ea96-14d1-4ad2-b84d-2dfae6d3c674",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21bc49ff-8929-400c-b065-8f1c6f2a9b49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2f1ea96-14d1-4ad2-b84d-2dfae6d3c674",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "b5388cc9-7c00-48b6-bf4d-1514b113eac1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "6cd9e770-e8a3-4d40-9e7e-4498b3fc8eb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5388cc9-7c00-48b6-bf4d-1514b113eac1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64d18afa-4fd6-480b-a860-b54968e4913b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5388cc9-7c00-48b6-bf4d-1514b113eac1",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "3302da8f-8efc-4a6b-9bfb-685d8734af6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "ce0b0d0c-aa1c-4057-9c81-9a91cf5cd9d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3302da8f-8efc-4a6b-9bfb-685d8734af6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d129a90c-9b0e-4c83-8662-e780793d6308",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3302da8f-8efc-4a6b-9bfb-685d8734af6d",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "420e0a8f-2acc-436f-a6e5-96c2815dfe60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "b1c5ef0d-9e35-4646-adec-ba45b73df240",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "420e0a8f-2acc-436f-a6e5-96c2815dfe60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43b3d928-1b08-4725-ba04-6ac1cc272fe8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "420e0a8f-2acc-436f-a6e5-96c2815dfe60",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "7b04dc30-8710-4f05-aca9-b4bb2866287d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "a253ff80-d244-4bf2-b862-196e41eeb2bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b04dc30-8710-4f05-aca9-b4bb2866287d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "788d5af8-a628-4d14-9a86-5841a0cebcde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b04dc30-8710-4f05-aca9-b4bb2866287d",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "09c711a8-87c2-4e8d-b906-b365bb96f9c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "f5969aec-c47a-4e5c-88e5-d220fd88d29d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09c711a8-87c2-4e8d-b906-b365bb96f9c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c8b52dd-5171-4a66-b132-8b6cb5a83d69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09c711a8-87c2-4e8d-b906-b365bb96f9c6",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "d6b60f5a-d87f-4135-966c-1192cbf8d193",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "d5eaee5a-e9a7-4fd3-b145-a12ca4098fe9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6b60f5a-d87f-4135-966c-1192cbf8d193",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "664e25d8-7c9b-4a07-957c-ab468cd98d71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6b60f5a-d87f-4135-966c-1192cbf8d193",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "c8fcbbc8-54be-4140-b5d9-a9b14367471c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "47bac0db-1a39-4bc5-8cbd-0d54b356806b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8fcbbc8-54be-4140-b5d9-a9b14367471c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97545582-d470-4877-b0cb-35dac5852db1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8fcbbc8-54be-4140-b5d9-a9b14367471c",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "7956cdec-a67e-47ae-a0dc-24cef99cd60d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "1715ceea-c77f-4baf-83ea-162320cbe070",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7956cdec-a67e-47ae-a0dc-24cef99cd60d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67b71f1e-fa0d-4878-b7c2-06f42d1386b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7956cdec-a67e-47ae-a0dc-24cef99cd60d",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "f06c6f6e-d4a4-4198-8568-59e60d14a124",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "27c52c29-c5ce-4919-ad46-5874ef8c9e26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f06c6f6e-d4a4-4198-8568-59e60d14a124",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b349e349-0230-44a5-9c82-bea1dc3e7f2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f06c6f6e-d4a4-4198-8568-59e60d14a124",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "38d45a92-154c-436d-8d6e-872a5e79ba1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "83b12554-7b17-4f2e-9dbf-d187aee73e54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38d45a92-154c-436d-8d6e-872a5e79ba1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1adaba0e-1c1c-4531-a6a9-df0682581fb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38d45a92-154c-436d-8d6e-872a5e79ba1a",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "9d875a39-eaeb-4373-95e1-798320e9f9b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "3ae8bfc7-9cf2-4220-be4b-178ee0246a73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d875a39-eaeb-4373-95e1-798320e9f9b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1abdccfc-c8a8-449b-8633-c8b09dd7877c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d875a39-eaeb-4373-95e1-798320e9f9b8",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "78adcccb-6f52-499f-b328-5cd97a2a02a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "fb8a2bc5-0cf1-4ba4-9688-6b3fead855a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78adcccb-6f52-499f-b328-5cd97a2a02a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a6c7a19-e3c4-423f-89ef-7b752ab10fa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78adcccb-6f52-499f-b328-5cd97a2a02a7",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "ff0ac63b-41cb-42d0-92e3-0703fdef6b3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "5acdaa71-5eb4-4cb0-a4d0-72c420fa29b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff0ac63b-41cb-42d0-92e3-0703fdef6b3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c06d6e6d-9d12-4e23-a1c1-d4fbcbc58509",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff0ac63b-41cb-42d0-92e3-0703fdef6b3b",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "b0612271-d8fd-478c-b51c-8fc006c6afa1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "77351688-d6b3-4feb-9260-32c492d4f7ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0612271-d8fd-478c-b51c-8fc006c6afa1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf6c8f2d-36a4-4f66-843e-b85de6b2fc21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0612271-d8fd-478c-b51c-8fc006c6afa1",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "6e0841e5-ed8e-4489-bb5d-344ae6ce8086",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "7f2ac3ce-a759-4357-8f53-cd6a43d5ffb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e0841e5-ed8e-4489-bb5d-344ae6ce8086",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28daf301-7492-4b7b-a281-831e1cce4fc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e0841e5-ed8e-4489-bb5d-344ae6ce8086",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "fd95cdc8-c945-4830-a778-93fb90631b80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "8d8bf4c9-1df3-4aa1-9bbf-ce79a9f80757",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd95cdc8-c945-4830-a778-93fb90631b80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9379c14a-748b-4f04-8701-e0ca13b793d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd95cdc8-c945-4830-a778-93fb90631b80",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "f94c8312-46e9-44ae-9769-02da0ae15e4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "4343ec14-0bce-4a55-801c-a2df3625a152",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f94c8312-46e9-44ae-9769-02da0ae15e4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "781899f3-fa2e-4674-8557-7835277199e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f94c8312-46e9-44ae-9769-02da0ae15e4f",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "9c2d3e39-3737-452f-9bf1-286eef02af50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "b9088cd9-59ae-498f-8b19-4e51db7ee452",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c2d3e39-3737-452f-9bf1-286eef02af50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "335a91c6-92eb-451e-8e31-052f9f3a1b5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c2d3e39-3737-452f-9bf1-286eef02af50",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        },
        {
            "id": "d2c1a230-1efa-4252-a4f4-4c99571b24d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "compositeImage": {
                "id": "c5e0be11-f470-4782-ab7c-edbdba8f6b23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2c1a230-1efa-4252-a4f4-4c99571b24d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "217d620f-35bd-446b-ab11-c55bc981537c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2c1a230-1efa-4252-a4f4-4c99571b24d0",
                    "LayerId": "d6f0e96a-bbf5-4d45-9134-c96667b19534"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "d6f0e96a-bbf5-4d45-9134-c96667b19534",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5bcfd6b-3d96-4c80-a65d-5d252413faf9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}