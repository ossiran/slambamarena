{
    "id": "02384d83-8c14-4cd0-86b0-04c62ef38d9f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_5m_fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "998b3f35-0534-4be7-b98c-3b04d7ea2d72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02384d83-8c14-4cd0-86b0-04c62ef38d9f",
            "compositeImage": {
                "id": "9a63a7cb-ffd7-46a1-8ce8-902486661802",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "998b3f35-0534-4be7-b98c-3b04d7ea2d72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50f4e08f-7406-40f1-af6e-97ce7f48f76e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "998b3f35-0534-4be7-b98c-3b04d7ea2d72",
                    "LayerId": "5cf71886-ab93-48f4-acac-fe8a032ba70b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5cf71886-ab93-48f4-acac-fe8a032ba70b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "02384d83-8c14-4cd0-86b0-04c62ef38d9f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}