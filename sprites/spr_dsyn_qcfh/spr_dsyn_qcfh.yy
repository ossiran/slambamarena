{
    "id": "48af6e33-9a4a-4188-8243-9147f4bb9537",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dsyn_qcfh",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 43,
    "bbox_right": 127,
    "bbox_top": 36,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "80909897-31fc-4197-b709-79d6f4da3ff5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "69a882c1-61c7-4a61-990b-42d6b8ddbbb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80909897-31fc-4197-b709-79d6f4da3ff5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d378cba-72b5-45c2-a5d9-bf4323e34972",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80909897-31fc-4197-b709-79d6f4da3ff5",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "c19b6e6a-b702-43e4-a3a6-6cd7b66355a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "a82560de-b618-486e-889d-65dfe8183c12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c19b6e6a-b702-43e4-a3a6-6cd7b66355a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58e1608a-b9ab-428a-92be-733dd104f9a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c19b6e6a-b702-43e4-a3a6-6cd7b66355a5",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "15ddca2f-c8cb-41ce-bd7c-2d8b743f2753",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "eac87e84-60b6-4593-8046-acd9404f250e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15ddca2f-c8cb-41ce-bd7c-2d8b743f2753",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32ed9a35-df05-4587-9ec6-cde6dd7d7215",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15ddca2f-c8cb-41ce-bd7c-2d8b743f2753",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "787cc642-b963-40e3-b3ab-9f195527cf97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "fdb0f28d-961d-4c09-be36-e86b3653fce3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "787cc642-b963-40e3-b3ab-9f195527cf97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e228bd9-2f34-4c88-8d4b-3535defbf1e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "787cc642-b963-40e3-b3ab-9f195527cf97",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "afea1bed-0b35-4dd5-ac93-2b16632c2801",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "0a06cd83-b454-4d12-825c-c2315ff4fcf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afea1bed-0b35-4dd5-ac93-2b16632c2801",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53b1a372-6274-4d28-bcc2-d994c0b1b52d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afea1bed-0b35-4dd5-ac93-2b16632c2801",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "44265dfe-869f-44e0-9971-755617d855b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "6cda7a85-f8e4-4a82-b7c2-de2bcfa84ba0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44265dfe-869f-44e0-9971-755617d855b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4a61620-cdaf-46c6-ad95-14bed26244c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44265dfe-869f-44e0-9971-755617d855b5",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "b3f23421-a964-423e-9b89-85e42a4ffaee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "c4238400-8dba-4368-bce4-8b84312c7b79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3f23421-a964-423e-9b89-85e42a4ffaee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "911a5b68-8836-4603-b6d5-23f3b11fd6f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3f23421-a964-423e-9b89-85e42a4ffaee",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "db1a6756-04c3-42fc-987f-311a63f288dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "31225e6d-a040-436c-b5dd-ea3d14850c07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db1a6756-04c3-42fc-987f-311a63f288dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51cc4f4e-1ebb-445f-8175-12fe2353f7ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db1a6756-04c3-42fc-987f-311a63f288dc",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "dfa1b2b9-7d9d-4736-8c77-6e939cd7715f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "4e218864-e3d1-4bb4-989f-697064ef8b0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfa1b2b9-7d9d-4736-8c77-6e939cd7715f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69788282-1241-4f2f-9827-4e76c58a014a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfa1b2b9-7d9d-4736-8c77-6e939cd7715f",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "74c41988-5387-4378-9597-88a7da536dab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "1dd9897c-ff5e-42ea-b141-505837997902",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74c41988-5387-4378-9597-88a7da536dab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "335cd925-61a3-4c7a-a9fe-3f0521b4fee4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74c41988-5387-4378-9597-88a7da536dab",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "39716e23-35d3-452d-b557-c5f92a583e0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "c6fae2fc-511f-40b4-9070-218e022014e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39716e23-35d3-452d-b557-c5f92a583e0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51eaaf27-c194-49ed-95d9-c24f622b5eea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39716e23-35d3-452d-b557-c5f92a583e0d",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "d9876921-e2b4-4d10-9973-771e77f08deb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "3a524c69-4f3b-4dc7-ad31-efbe20a26a35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9876921-e2b4-4d10-9973-771e77f08deb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "720d7e4e-a7d0-4ec5-bb13-ad5df197d115",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9876921-e2b4-4d10-9973-771e77f08deb",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "fc133bf4-29cf-4873-87d1-409f11a7007e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "78b5210b-ae21-44f4-9650-84a9ce353ba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc133bf4-29cf-4873-87d1-409f11a7007e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "138434d0-9edb-4c39-8d35-2437a1fb7453",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc133bf4-29cf-4873-87d1-409f11a7007e",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "a4274181-10fd-4dac-bb15-7123423a5418",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "4090684c-850f-444f-aa47-6fd33d100b54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4274181-10fd-4dac-bb15-7123423a5418",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df2da2ec-bedc-4803-a531-f37e49aefffa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4274181-10fd-4dac-bb15-7123423a5418",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "583366d3-e667-48dd-80f7-586657d5b1a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "265190f1-ca36-4041-9d19-3657d594c970",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "583366d3-e667-48dd-80f7-586657d5b1a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c59e2a94-b52b-4123-b647-0db44975e71c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "583366d3-e667-48dd-80f7-586657d5b1a3",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "c54ba0c8-48d0-40c9-bbf1-ef03ad90f110",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "32d0c78e-ae04-4794-92f4-82017191ce38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c54ba0c8-48d0-40c9-bbf1-ef03ad90f110",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bea325ef-7471-43cc-9f21-86550062e5c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c54ba0c8-48d0-40c9-bbf1-ef03ad90f110",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "fde33aad-93c6-48fd-a916-a6091781d45e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "46b76706-a440-4858-875b-a9c05422db17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fde33aad-93c6-48fd-a916-a6091781d45e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a708e132-870b-4eb1-86a2-37c41bd7d90c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fde33aad-93c6-48fd-a916-a6091781d45e",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "be80268d-15fb-45f8-a9e2-26eb33dad169",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "6167dbcc-c4db-4aca-b47d-1fedca2ecfb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be80268d-15fb-45f8-a9e2-26eb33dad169",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "671235ac-e628-490e-ac51-d274df3464b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be80268d-15fb-45f8-a9e2-26eb33dad169",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "5e470c5c-ebb0-4c67-b0e1-b5ff96e75e4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "a86e841a-5139-4c76-b027-2efcbefc6ce4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e470c5c-ebb0-4c67-b0e1-b5ff96e75e4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b04a2ab8-50f7-4ec3-b177-512eebc76a6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e470c5c-ebb0-4c67-b0e1-b5ff96e75e4d",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "5b412f76-e158-48ce-a5be-4e082f73b27f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "5bbede29-52a9-4e13-8eab-10e9176d98b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b412f76-e158-48ce-a5be-4e082f73b27f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "867146ac-9fd0-4518-bacc-850d2b8ed8cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b412f76-e158-48ce-a5be-4e082f73b27f",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "e906a75e-7de0-49e8-900a-681c4e390fad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "607b2aaf-0347-4425-b22b-08a93a0708b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e906a75e-7de0-49e8-900a-681c4e390fad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67a50dae-3f1d-4d4b-9ba2-830914aa62d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e906a75e-7de0-49e8-900a-681c4e390fad",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "d39a02f3-2f1d-46d8-bb6b-de4c3f7ec57e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "f4971ba1-03af-4bb4-8d00-dc94aa4915ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d39a02f3-2f1d-46d8-bb6b-de4c3f7ec57e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "819ee9eb-c131-478b-b7bc-a3c5cb36a4fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d39a02f3-2f1d-46d8-bb6b-de4c3f7ec57e",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "fab69d25-eaf6-401c-8972-c5c04c5d5a4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "e85a16dc-1e94-4a93-973f-1c1532671ac2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fab69d25-eaf6-401c-8972-c5c04c5d5a4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9e58360-8d97-4d1c-a697-bed9866b4c7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fab69d25-eaf6-401c-8972-c5c04c5d5a4c",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "97e8f6de-6cb7-4ac7-a38f-7f45b1d50c2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "5deb20e8-da5f-4d42-9f84-761a9ebf724b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97e8f6de-6cb7-4ac7-a38f-7f45b1d50c2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f73cfb8a-83c4-4420-9031-2a84f83d0ba2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97e8f6de-6cb7-4ac7-a38f-7f45b1d50c2a",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "ab86a246-7930-492a-9162-1edc934cbed9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "3a59420a-b887-48f1-9405-70a5e3a67722",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab86a246-7930-492a-9162-1edc934cbed9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c10d7fca-7122-40be-91de-9588243ecd03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab86a246-7930-492a-9162-1edc934cbed9",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "b3dcd0cf-7737-4230-9fa4-6c6f3892f607",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "22380427-0a1b-4a6d-913e-194c7984c8b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3dcd0cf-7737-4230-9fa4-6c6f3892f607",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66c1a26f-1413-42ec-8403-ec457f07875c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3dcd0cf-7737-4230-9fa4-6c6f3892f607",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "6dbd3fb0-97f0-4282-93cc-ee9a6b5630be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "7f9c868d-3d35-4cb7-b407-e5a53427b679",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dbd3fb0-97f0-4282-93cc-ee9a6b5630be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1cd8373-8625-433b-9d37-d2ac4e2a15f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dbd3fb0-97f0-4282-93cc-ee9a6b5630be",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "eb450be9-4b7f-459b-9f39-13a4ce5935a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "2e080c9d-08c0-45a1-ac10-92f837546f71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb450be9-4b7f-459b-9f39-13a4ce5935a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de0a5aa9-bcf0-49d6-b043-5e5073d3a7da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb450be9-4b7f-459b-9f39-13a4ce5935a7",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "7f665eb2-8649-4811-8e9f-62e9f8e74cca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "ba06a2d4-6638-4a0e-af14-1115cab396b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f665eb2-8649-4811-8e9f-62e9f8e74cca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d08a23b7-390e-4e10-8d89-14f74b8e3593",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f665eb2-8649-4811-8e9f-62e9f8e74cca",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "c5252394-58b8-4b30-a04f-2512a339f36c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "7b8bf733-e871-46dc-8e96-d0068fa63d6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5252394-58b8-4b30-a04f-2512a339f36c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "741a28f3-7e70-4b77-9236-53b7aac750a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5252394-58b8-4b30-a04f-2512a339f36c",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "8ee5c3f4-f5ac-4ab5-a1f0-35279d9ee034",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "1406777f-cb30-46e7-90a6-0e923fb3a4fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ee5c3f4-f5ac-4ab5-a1f0-35279d9ee034",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "987524ef-5c83-4595-a970-08306f23c4c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ee5c3f4-f5ac-4ab5-a1f0-35279d9ee034",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "e9eddf74-661a-4a02-9c9c-54e8e192ed3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "2a98a8b8-2309-4e21-af97-f14b6093084a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9eddf74-661a-4a02-9c9c-54e8e192ed3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4636c95f-aad6-4988-b44f-b13e07b6e894",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9eddf74-661a-4a02-9c9c-54e8e192ed3a",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "c66e64ec-ae1b-4afa-8378-915403d1f92f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "358dec49-f2f2-4ab7-b8da-e006b2570e67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c66e64ec-ae1b-4afa-8378-915403d1f92f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4215143c-2b79-4e45-a16f-ed710eb97315",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c66e64ec-ae1b-4afa-8378-915403d1f92f",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "2ab9551f-1eb6-4394-9118-0a75caa58da8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "6cf3b1cf-ac2d-4ae5-afb4-67c8ec1019d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ab9551f-1eb6-4394-9118-0a75caa58da8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab7492e6-9021-4a84-bb45-5a6b2897a975",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ab9551f-1eb6-4394-9118-0a75caa58da8",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "c275e8de-4805-4782-b9b9-c719b2c37bcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "12d44b84-8683-44dc-b5cf-10f010352000",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c275e8de-4805-4782-b9b9-c719b2c37bcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc646fa4-42ef-4495-9214-fa177bae2b83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c275e8de-4805-4782-b9b9-c719b2c37bcc",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "0d04f889-3b96-4092-9fa9-13a651a3cb90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "65a9c165-02a2-4884-88bd-e541ab60926a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d04f889-3b96-4092-9fa9-13a651a3cb90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ced96ae-0469-4e98-b1cd-a8ab876e0708",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d04f889-3b96-4092-9fa9-13a651a3cb90",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "d6509b0c-61e2-4131-a5bf-b1ea06d7abc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "a5377682-cb44-4573-9655-5c5a1740786b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6509b0c-61e2-4131-a5bf-b1ea06d7abc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8873e46d-8430-4b1e-9005-8bf96b2964b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6509b0c-61e2-4131-a5bf-b1ea06d7abc1",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "2e7081ba-6991-497b-8eb5-bfe99ce18458",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "a24fd009-90ca-4fdd-8389-30002f41e29b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e7081ba-6991-497b-8eb5-bfe99ce18458",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3ca0375-8830-406a-83ea-2a3f255f0c9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e7081ba-6991-497b-8eb5-bfe99ce18458",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "c9c342f7-eba3-4ce8-abf7-557a4ccdeb7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "f71810fd-a6d8-4dda-a411-5095dd7038a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9c342f7-eba3-4ce8-abf7-557a4ccdeb7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3603a3d9-8e50-4788-9dbe-d3a23bd36cc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9c342f7-eba3-4ce8-abf7-557a4ccdeb7b",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "ca9d8666-e207-4da7-b0d2-fce7528b06cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "b031cc16-c5c9-457e-a503-b7c9260e854a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca9d8666-e207-4da7-b0d2-fce7528b06cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc65c2dd-a472-4c12-8ff1-fc919e1ff640",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca9d8666-e207-4da7-b0d2-fce7528b06cd",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "0de31c42-8187-4fe3-8e8f-894dc746686c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "ad94c9d4-d153-4c2f-b346-9fcccf69fc3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0de31c42-8187-4fe3-8e8f-894dc746686c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "243e0f09-f2da-480c-8f6b-529e2d357c0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0de31c42-8187-4fe3-8e8f-894dc746686c",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "80e222f9-e5cc-43df-8844-35e495ce398f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "24753acc-a8cd-412f-8a60-7b75ea7d667f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80e222f9-e5cc-43df-8844-35e495ce398f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce08e398-ebb4-4b76-9557-e57523ffaf61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80e222f9-e5cc-43df-8844-35e495ce398f",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "85677c0f-e295-4b26-b655-5e5975671b6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "f2dc7dad-4a94-426c-b872-720b1167bb13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85677c0f-e295-4b26-b655-5e5975671b6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dee5b790-70a8-4a41-a79b-991832198fc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85677c0f-e295-4b26-b655-5e5975671b6a",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "78522024-c089-434e-8a4f-60afc3f86675",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "cb69b5da-03e5-4b43-a4dd-7d1457440ad2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78522024-c089-434e-8a4f-60afc3f86675",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90e98c4c-ac6b-4d6a-aa4c-7263bc0f97a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78522024-c089-434e-8a4f-60afc3f86675",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "ef75de46-f7c1-4c01-880b-0f5b62071e4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "d489d107-03b5-4614-a2bf-90b25ef4507b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef75de46-f7c1-4c01-880b-0f5b62071e4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "118da08e-f686-4219-8799-b75ff5281054",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef75de46-f7c1-4c01-880b-0f5b62071e4f",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "8d2b0539-4bda-453d-9a1a-37c09202457b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "39415d6d-3dfc-4c6a-9016-37305d0fd304",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d2b0539-4bda-453d-9a1a-37c09202457b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a44a630-a02d-43c8-b1e1-e47bd8a5ae33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d2b0539-4bda-453d-9a1a-37c09202457b",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "11825763-6252-4ca1-897a-671e906c2752",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "8034e7a2-31e0-4284-9d3f-b35229edfbf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11825763-6252-4ca1-897a-671e906c2752",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83313a69-3afd-4b1e-8e89-bcbc44e003af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11825763-6252-4ca1-897a-671e906c2752",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "0ae3817e-0059-4b2b-ba2f-d400a80920e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "92e06a55-4a79-4218-8b2a-53bc615ec044",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ae3817e-0059-4b2b-ba2f-d400a80920e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2878d66c-f9ae-4213-a2de-6378b8e7269f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ae3817e-0059-4b2b-ba2f-d400a80920e4",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "b2e0e5c4-29d1-4aff-8027-ad1698d58dd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "6cbf6ada-14d3-4c5f-b171-591c100a4021",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2e0e5c4-29d1-4aff-8027-ad1698d58dd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "598889a3-8a3a-4933-8dea-50ee876bd3a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2e0e5c4-29d1-4aff-8027-ad1698d58dd7",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "6f2d6bec-ffbb-4f34-af8e-31a62baae89b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "56d6fa70-1044-4f08-8184-d94f1e71fd82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f2d6bec-ffbb-4f34-af8e-31a62baae89b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "647b5a6b-e5bc-4036-b5dd-b0fb631494ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f2d6bec-ffbb-4f34-af8e-31a62baae89b",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "5bda14c9-2abc-418f-a456-b8c79615023a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "b2b9c650-48a0-4152-9811-990042d868fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bda14c9-2abc-418f-a456-b8c79615023a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44f3cda7-bc85-46d4-8a94-b18625afc0c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bda14c9-2abc-418f-a456-b8c79615023a",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        },
        {
            "id": "0b5c7b86-b038-4677-be27-9187badacef4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "compositeImage": {
                "id": "3969e429-b39d-478b-a55e-25d84a19c22d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b5c7b86-b038-4677-be27-9187badacef4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e35a364a-5ad8-4f9c-bf4a-0b39ca0a7261",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b5c7b86-b038-4677-be27-9187badacef4",
                    "LayerId": "a29e3598-6f6d-46a3-b847-6b40a29b506b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "a29e3598-6f6d-46a3-b847-6b40a29b506b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48af6e33-9a4a-4188-8243-9147f4bb9537",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 80
}