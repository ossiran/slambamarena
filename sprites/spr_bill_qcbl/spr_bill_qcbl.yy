{
    "id": "7bc22718-4410-45f8-90a8-8d823678d65a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_qcbl",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 36,
    "bbox_right": 122,
    "bbox_top": 69,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7de9b737-2da5-42e2-9325-c3ff1f930997",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "6d2474be-34b8-40cf-8e9a-5fabf2232738",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7de9b737-2da5-42e2-9325-c3ff1f930997",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f1b6eea-97ec-46cf-8874-2315695ba763",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7de9b737-2da5-42e2-9325-c3ff1f930997",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "34826822-04b4-4cde-8f30-caef1062fafb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "85af0071-8be2-4d0a-93b6-b96e7283828f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34826822-04b4-4cde-8f30-caef1062fafb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c648bab-a880-491c-8054-079dc065d023",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34826822-04b4-4cde-8f30-caef1062fafb",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "2a81b3c8-cd6c-4fa8-8ae8-85fb727c3997",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "886d3c3f-d05d-4c50-8c88-421b59325d3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a81b3c8-cd6c-4fa8-8ae8-85fb727c3997",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9fecedd-f60b-477e-a1c3-a8e015cabbf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a81b3c8-cd6c-4fa8-8ae8-85fb727c3997",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "77897f6e-4260-437d-b67c-d04a8978d80d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "55a2f97a-01cd-44b5-87b9-beaae9e23f26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77897f6e-4260-437d-b67c-d04a8978d80d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0fcca8f-3679-4382-999a-541cd737cef9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77897f6e-4260-437d-b67c-d04a8978d80d",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "29d3e536-51f2-4f9d-bc82-df92808fadd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "086279a8-1a91-4b7e-867d-bdce5a95ace5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29d3e536-51f2-4f9d-bc82-df92808fadd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc97b9ce-b88d-4a33-86df-7e3bcc1f233d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29d3e536-51f2-4f9d-bc82-df92808fadd6",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "76b63970-c140-407f-aa4a-d43508ce4b82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "3610687f-b925-475b-86af-fa667d3febe0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76b63970-c140-407f-aa4a-d43508ce4b82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c33dbafc-9ad7-47d9-bc4b-e3a01857d197",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76b63970-c140-407f-aa4a-d43508ce4b82",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "fde53f7f-fa71-4da1-a70b-90f3bb3b400a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "c65e477f-fa42-4912-9c25-696836cf3235",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fde53f7f-fa71-4da1-a70b-90f3bb3b400a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d941a7ee-d1c1-4ae1-a9d2-3eb13ed08b5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fde53f7f-fa71-4da1-a70b-90f3bb3b400a",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "f3a4e7e8-c825-4a00-afce-d9b8c1914f84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "655d9cf4-48a4-48ec-9a3f-ec8e91c09838",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3a4e7e8-c825-4a00-afce-d9b8c1914f84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "024b10d0-c731-4968-8148-4d24779dfb87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3a4e7e8-c825-4a00-afce-d9b8c1914f84",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "e0851f45-8d62-490b-9289-04e247ec7873",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "71189a43-6bb3-4484-b6dd-2c0cc1c9186d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0851f45-8d62-490b-9289-04e247ec7873",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c6e2ad0-3010-4d6e-9fcf-03f7a64d8225",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0851f45-8d62-490b-9289-04e247ec7873",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "78e7c8ae-1a64-40fb-b2f2-7a5354e2f1a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "49553b8f-2d70-42e1-a360-febe317daf84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78e7c8ae-1a64-40fb-b2f2-7a5354e2f1a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "857aeba8-65e1-483f-81c6-2f0de9af4eea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78e7c8ae-1a64-40fb-b2f2-7a5354e2f1a4",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "07bcdad7-84bd-41b3-84cd-c59ad7ec9693",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "1476b178-d16c-43bf-8152-b4bbe52cff03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07bcdad7-84bd-41b3-84cd-c59ad7ec9693",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9c725f9-75e0-4c2a-a2a0-2d4d3c063076",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07bcdad7-84bd-41b3-84cd-c59ad7ec9693",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "3b9bd2dc-a7be-42d1-b6ca-00405f251958",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "bf953ab5-f764-425c-ae0f-3ecbacee52d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b9bd2dc-a7be-42d1-b6ca-00405f251958",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57c7ba55-ec88-4494-bb42-f45a44299086",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b9bd2dc-a7be-42d1-b6ca-00405f251958",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "dd31bb29-7b09-4ae9-8cc1-aef96d9c80a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "904d0c8a-44d5-4bc3-ac15-5ee5f66d554a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd31bb29-7b09-4ae9-8cc1-aef96d9c80a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edb99f61-aec1-4ae1-be4e-efc0e39ff52f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd31bb29-7b09-4ae9-8cc1-aef96d9c80a7",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "666b3871-3851-46d1-b5f9-a33f16409e0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "0ed353c2-3dd9-41f5-9321-248b999ced1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "666b3871-3851-46d1-b5f9-a33f16409e0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cb78d37-d831-4ce5-99e4-e5ee3225ade8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "666b3871-3851-46d1-b5f9-a33f16409e0c",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "e5b5f19b-6b16-49dc-8315-d7b6bd08d882",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "91fa9109-784e-4a5d-829e-45fb8242f689",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5b5f19b-6b16-49dc-8315-d7b6bd08d882",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a46c0886-1465-4eab-9d8a-f89db16c314d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5b5f19b-6b16-49dc-8315-d7b6bd08d882",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "dc08a46c-012a-4e32-8598-369a8be0de1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "70295528-b398-4628-8644-2799370c4e1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc08a46c-012a-4e32-8598-369a8be0de1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ceecb4dc-efb5-47eb-8fb5-ec87b95f563e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc08a46c-012a-4e32-8598-369a8be0de1f",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "baee35bf-c8af-4971-bafb-e20051efcee9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "66d92790-c1eb-4c45-9a0d-20543d118ac9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "baee35bf-c8af-4971-bafb-e20051efcee9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39c7b159-c807-4d8d-a427-71aa10f35b5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "baee35bf-c8af-4971-bafb-e20051efcee9",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "560fa38d-4c86-45d5-b479-8798c7110abf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "fc24dc48-ba2f-4f90-b927-1e24226670f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "560fa38d-4c86-45d5-b479-8798c7110abf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4810d963-59af-460d-b704-30cf5dc90ab3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "560fa38d-4c86-45d5-b479-8798c7110abf",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "e051b7b8-f870-46f9-a314-49b98ca2b335",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "1b27b5c4-c0df-40ed-bb31-9752743a069a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e051b7b8-f870-46f9-a314-49b98ca2b335",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd3c2458-b450-4cc0-8d29-8c294ee9f06c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e051b7b8-f870-46f9-a314-49b98ca2b335",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "4b145128-220a-4695-a51e-636597b86f11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "0f88938d-01f7-4f1b-ba83-87f8004245d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b145128-220a-4695-a51e-636597b86f11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76602a96-617b-4115-8964-0e56f45610a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b145128-220a-4695-a51e-636597b86f11",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "b957408e-5a20-406a-afad-40bf26a072d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "f5d5aed9-83c6-4b46-8e3e-5e513ab89b4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b957408e-5a20-406a-afad-40bf26a072d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9445ac1-902a-404a-8a32-f003682e7458",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b957408e-5a20-406a-afad-40bf26a072d1",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "7e70b131-e4e1-4d75-99f0-6c244b924bd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "4181fa1f-bfaf-42fe-9f88-2eacff8e7978",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e70b131-e4e1-4d75-99f0-6c244b924bd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bad29c06-4a64-4fa7-9a48-d6bc1a195f2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e70b131-e4e1-4d75-99f0-6c244b924bd6",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "65389147-c45b-45dc-a112-38d7bc89a0ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "5ab238cc-1813-4be2-8134-240525a94a1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65389147-c45b-45dc-a112-38d7bc89a0ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9abe94f-9028-4c28-a093-b4b69817b3c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65389147-c45b-45dc-a112-38d7bc89a0ee",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "9df7699b-d717-4276-93f3-faf34fcf19ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "3bf178a1-36e0-4eba-b441-9e5d99ef24ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9df7699b-d717-4276-93f3-faf34fcf19ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26bca0b7-f219-4a95-8d8a-15fa88dc6c0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9df7699b-d717-4276-93f3-faf34fcf19ad",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "ede2c913-ff2d-4015-acaf-3a9989623392",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "af0e96ba-8249-4781-802d-6372407ab75e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ede2c913-ff2d-4015-acaf-3a9989623392",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df2a0018-231d-4dff-9ceb-8de26305baf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ede2c913-ff2d-4015-acaf-3a9989623392",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "779b0c8f-1864-4cb0-bf82-5212cd311932",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "455f225a-420b-4ee7-8091-92bbb93995e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "779b0c8f-1864-4cb0-bf82-5212cd311932",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45e7fe79-d354-489a-9b83-f30e2f1d430c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "779b0c8f-1864-4cb0-bf82-5212cd311932",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "40336547-55a8-4709-9b70-c956f05f9b49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "a073625d-3f01-47c6-8430-89b3662156fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40336547-55a8-4709-9b70-c956f05f9b49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3134fbe-e8d0-4e66-9834-1af39bcecebd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40336547-55a8-4709-9b70-c956f05f9b49",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "04c19ede-2816-4e44-9cc3-3fb384fb529f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "d358f56e-9516-4e94-afd6-28c22aa20b1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04c19ede-2816-4e44-9cc3-3fb384fb529f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34d1527b-87e2-4635-ab3f-a0e2f2026c0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04c19ede-2816-4e44-9cc3-3fb384fb529f",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "f57c2691-d3af-4cbf-b065-57076a49145a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "956aa934-c82b-4476-a970-c02fe274039f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f57c2691-d3af-4cbf-b065-57076a49145a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35aa2b07-4e35-4688-b0ba-18d3c5271ff0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f57c2691-d3af-4cbf-b065-57076a49145a",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "4f10e0e6-1f42-46e3-964f-cc550091d610",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "0d046ad9-b440-447b-9395-98601a723c42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f10e0e6-1f42-46e3-964f-cc550091d610",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0f22ba4-fa06-4273-b26f-76db89a4b110",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f10e0e6-1f42-46e3-964f-cc550091d610",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "33d88ff8-51c9-493a-bda4-2d383798ab06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "7efeaf00-377e-40fb-b57f-9c7bc26762ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33d88ff8-51c9-493a-bda4-2d383798ab06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11430098-2215-4e69-8d1d-7f65a8dd9d68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33d88ff8-51c9-493a-bda4-2d383798ab06",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "c5806c74-c4f9-40c9-a6cd-78862aa953f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "4c6a8dd0-ff2a-4915-be66-4220f03bd95c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5806c74-c4f9-40c9-a6cd-78862aa953f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca1fcdf8-f4e6-4876-9ab9-d9b4ae23ba4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5806c74-c4f9-40c9-a6cd-78862aa953f1",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "ad13f812-d4e8-4507-a82d-23a75bb09780",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "96b3ee8f-0fb3-464e-b820-83fd4a470fcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad13f812-d4e8-4507-a82d-23a75bb09780",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81e2b954-939f-4c09-8c2e-929bb86b47f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad13f812-d4e8-4507-a82d-23a75bb09780",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "4cb66a03-5804-4632-aa45-927aa560b669",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "7f57772c-c460-47c0-85a5-a935f57c98ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cb66a03-5804-4632-aa45-927aa560b669",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab0502c2-9ae0-44e9-a6b1-16e260184b99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cb66a03-5804-4632-aa45-927aa560b669",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        },
        {
            "id": "f0e93330-2273-45e9-bace-286995817bae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "compositeImage": {
                "id": "f408ff95-aafe-449b-be43-088ab7b4390e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0e93330-2273-45e9-bace-286995817bae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f685e0f0-5846-41b6-a13f-8fb204bd0208",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0e93330-2273-45e9-bace-286995817bae",
                    "LayerId": "10e4637e-a786-4106-a426-0b74dcce6d7e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "10e4637e-a786-4106-a426-0b74dcce6d7e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7bc22718-4410-45f8-90a8-8d823678d65a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}