{
    "id": "7309fd95-9da0-46bc-b4d9-a59cb528b40d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_2m",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 16,
    "bbox_right": 136,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d281605-f60c-4bcc-98ee-43be0c8c63dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7309fd95-9da0-46bc-b4d9-a59cb528b40d",
            "compositeImage": {
                "id": "00fb017e-076a-4947-9ab0-a478d2daf672",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d281605-f60c-4bcc-98ee-43be0c8c63dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2b6af54-6970-4c9d-8678-aacbcb5e3831",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d281605-f60c-4bcc-98ee-43be0c8c63dc",
                    "LayerId": "d27f61a8-d4f2-4bb5-8d3c-864724119541"
                }
            ]
        },
        {
            "id": "9adb3f91-219d-4827-86d4-4777193d290d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7309fd95-9da0-46bc-b4d9-a59cb528b40d",
            "compositeImage": {
                "id": "bab5089b-92c4-44c2-852d-51632d920113",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9adb3f91-219d-4827-86d4-4777193d290d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ac7c0a6-242f-49ca-9187-591e70245e4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9adb3f91-219d-4827-86d4-4777193d290d",
                    "LayerId": "d27f61a8-d4f2-4bb5-8d3c-864724119541"
                }
            ]
        },
        {
            "id": "216a1cbe-c876-4682-ba91-c105accca543",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7309fd95-9da0-46bc-b4d9-a59cb528b40d",
            "compositeImage": {
                "id": "a8b0d9f6-24eb-4eb9-8776-6f3a661a6f04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "216a1cbe-c876-4682-ba91-c105accca543",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21172011-9e84-42b9-a9df-58337ce10c7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "216a1cbe-c876-4682-ba91-c105accca543",
                    "LayerId": "d27f61a8-d4f2-4bb5-8d3c-864724119541"
                }
            ]
        },
        {
            "id": "17db4c1e-b90c-4d93-8187-639c52193b99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7309fd95-9da0-46bc-b4d9-a59cb528b40d",
            "compositeImage": {
                "id": "e1ff3eea-4542-4eba-ab5a-97ca3df42a14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17db4c1e-b90c-4d93-8187-639c52193b99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "deaa5ea0-5a30-47c0-b2f6-1205a12d88bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17db4c1e-b90c-4d93-8187-639c52193b99",
                    "LayerId": "d27f61a8-d4f2-4bb5-8d3c-864724119541"
                }
            ]
        },
        {
            "id": "ecf1353b-cd72-4dab-8697-c079c40ce91f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7309fd95-9da0-46bc-b4d9-a59cb528b40d",
            "compositeImage": {
                "id": "4b037955-191c-4c65-99d3-007bd5953884",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecf1353b-cd72-4dab-8697-c079c40ce91f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b1ce135-03cd-47fa-9e56-644c68ac84ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecf1353b-cd72-4dab-8697-c079c40ce91f",
                    "LayerId": "d27f61a8-d4f2-4bb5-8d3c-864724119541"
                }
            ]
        },
        {
            "id": "d9a7552a-c11a-4faf-88c2-6833b78ec47f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7309fd95-9da0-46bc-b4d9-a59cb528b40d",
            "compositeImage": {
                "id": "d96e5722-a6ef-4751-8575-fc04c70a201a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9a7552a-c11a-4faf-88c2-6833b78ec47f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57d1a20a-c05d-42f6-9b24-1879f823d78f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9a7552a-c11a-4faf-88c2-6833b78ec47f",
                    "LayerId": "d27f61a8-d4f2-4bb5-8d3c-864724119541"
                }
            ]
        },
        {
            "id": "1bfaa7d7-ce07-448a-b899-c4c37dbc35ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7309fd95-9da0-46bc-b4d9-a59cb528b40d",
            "compositeImage": {
                "id": "bbceca71-c327-4a2c-b7bd-de6353f2048e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bfaa7d7-ce07-448a-b899-c4c37dbc35ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9eadc05b-9d3c-48a8-9cb3-73d99aef1c04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bfaa7d7-ce07-448a-b899-c4c37dbc35ac",
                    "LayerId": "d27f61a8-d4f2-4bb5-8d3c-864724119541"
                }
            ]
        },
        {
            "id": "b57383d3-a505-489d-8510-bbdf6535d70b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7309fd95-9da0-46bc-b4d9-a59cb528b40d",
            "compositeImage": {
                "id": "62ca3cdf-7bae-43b0-9b45-942701c3d6ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b57383d3-a505-489d-8510-bbdf6535d70b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b32790ee-847e-4a2e-9d06-ee734f858925",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b57383d3-a505-489d-8510-bbdf6535d70b",
                    "LayerId": "d27f61a8-d4f2-4bb5-8d3c-864724119541"
                }
            ]
        },
        {
            "id": "3c1064c5-5286-42f4-8dee-715050803e8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7309fd95-9da0-46bc-b4d9-a59cb528b40d",
            "compositeImage": {
                "id": "a6bb2088-5b0d-4f18-8758-ce639fb35728",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c1064c5-5286-42f4-8dee-715050803e8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66691560-38aa-4c57-888a-347388fb9982",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c1064c5-5286-42f4-8dee-715050803e8a",
                    "LayerId": "d27f61a8-d4f2-4bb5-8d3c-864724119541"
                }
            ]
        },
        {
            "id": "b3b338f3-2c9c-4e15-ad99-984532b3d572",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7309fd95-9da0-46bc-b4d9-a59cb528b40d",
            "compositeImage": {
                "id": "e34b3780-2609-4223-9304-ccb328253f4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3b338f3-2c9c-4e15-ad99-984532b3d572",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f7b5903-6822-4164-a9d3-300e463931d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3b338f3-2c9c-4e15-ad99-984532b3d572",
                    "LayerId": "d27f61a8-d4f2-4bb5-8d3c-864724119541"
                }
            ]
        },
        {
            "id": "9a29463c-b922-410c-b52e-b34510f1666d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7309fd95-9da0-46bc-b4d9-a59cb528b40d",
            "compositeImage": {
                "id": "7d119fc8-d461-4b46-950a-9a2a5085257c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a29463c-b922-410c-b52e-b34510f1666d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7313622d-3a8a-4e0a-9b42-e6c0fd527ee4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a29463c-b922-410c-b52e-b34510f1666d",
                    "LayerId": "d27f61a8-d4f2-4bb5-8d3c-864724119541"
                }
            ]
        },
        {
            "id": "75d3b40b-5d5a-4b64-b4f5-62651f12d5b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7309fd95-9da0-46bc-b4d9-a59cb528b40d",
            "compositeImage": {
                "id": "a555ca07-791c-43ec-822d-be3c4fe2da55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75d3b40b-5d5a-4b64-b4f5-62651f12d5b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4834ef51-9dd8-4c09-b5a0-7a907176ed4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75d3b40b-5d5a-4b64-b4f5-62651f12d5b5",
                    "LayerId": "d27f61a8-d4f2-4bb5-8d3c-864724119541"
                }
            ]
        },
        {
            "id": "8265fbda-bbdc-40c5-bef0-7aef8a01ed43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7309fd95-9da0-46bc-b4d9-a59cb528b40d",
            "compositeImage": {
                "id": "9f57757a-bd26-423e-9a65-eb8eba76a58b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8265fbda-bbdc-40c5-bef0-7aef8a01ed43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4252a32e-9557-4790-a7a3-eea028b58ebb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8265fbda-bbdc-40c5-bef0-7aef8a01ed43",
                    "LayerId": "d27f61a8-d4f2-4bb5-8d3c-864724119541"
                }
            ]
        },
        {
            "id": "f12c2852-eb57-4d87-895b-cc112e9b8b1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7309fd95-9da0-46bc-b4d9-a59cb528b40d",
            "compositeImage": {
                "id": "94740933-b0b6-4cbc-b1a7-e57c58a50892",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f12c2852-eb57-4d87-895b-cc112e9b8b1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3f90c79-77bc-4c24-95b0-88cd8de9e5ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f12c2852-eb57-4d87-895b-cc112e9b8b1d",
                    "LayerId": "d27f61a8-d4f2-4bb5-8d3c-864724119541"
                }
            ]
        },
        {
            "id": "4523e1fd-9313-44c7-b5a9-f99f018f7fcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7309fd95-9da0-46bc-b4d9-a59cb528b40d",
            "compositeImage": {
                "id": "dc2c0794-4a01-44ed-ae7e-ffec7d2ddf71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4523e1fd-9313-44c7-b5a9-f99f018f7fcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9f62c67-58cf-4d99-972e-a4393e22efec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4523e1fd-9313-44c7-b5a9-f99f018f7fcc",
                    "LayerId": "d27f61a8-d4f2-4bb5-8d3c-864724119541"
                }
            ]
        },
        {
            "id": "21ded6a6-ce4c-4ec3-a67b-3ef14ff0d7c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7309fd95-9da0-46bc-b4d9-a59cb528b40d",
            "compositeImage": {
                "id": "a5748f4a-a27d-4a7f-8156-90ca1ddc920e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21ded6a6-ce4c-4ec3-a67b-3ef14ff0d7c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5233f756-7c97-42ce-897e-fcb67b2ea6f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21ded6a6-ce4c-4ec3-a67b-3ef14ff0d7c6",
                    "LayerId": "d27f61a8-d4f2-4bb5-8d3c-864724119541"
                }
            ]
        },
        {
            "id": "39593c9c-16d4-4f13-9647-674792eb05ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7309fd95-9da0-46bc-b4d9-a59cb528b40d",
            "compositeImage": {
                "id": "19cea63b-86e3-4f1f-b990-90a63ef88204",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39593c9c-16d4-4f13-9647-674792eb05ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f64bc5c7-dc27-435c-a93f-08dc5c58f90a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39593c9c-16d4-4f13-9647-674792eb05ef",
                    "LayerId": "d27f61a8-d4f2-4bb5-8d3c-864724119541"
                }
            ]
        },
        {
            "id": "0772d55c-e4ee-41cb-b8cc-bc87c3486568",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7309fd95-9da0-46bc-b4d9-a59cb528b40d",
            "compositeImage": {
                "id": "121ea840-204d-4c9a-a41a-2d3e867dcf76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0772d55c-e4ee-41cb-b8cc-bc87c3486568",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89e73ce7-3173-40fb-acc9-63b2025dd658",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0772d55c-e4ee-41cb-b8cc-bc87c3486568",
                    "LayerId": "d27f61a8-d4f2-4bb5-8d3c-864724119541"
                }
            ]
        },
        {
            "id": "03a008b3-7ca1-4dfb-b82c-7960603694ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7309fd95-9da0-46bc-b4d9-a59cb528b40d",
            "compositeImage": {
                "id": "66c7db23-50ac-4670-8cdf-563ea65c7e5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03a008b3-7ca1-4dfb-b82c-7960603694ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77e269d6-4d54-408a-8790-8e48dc5c5b99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03a008b3-7ca1-4dfb-b82c-7960603694ea",
                    "LayerId": "d27f61a8-d4f2-4bb5-8d3c-864724119541"
                }
            ]
        },
        {
            "id": "7da4cbe8-9c83-42ec-bf82-b4407452ff48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7309fd95-9da0-46bc-b4d9-a59cb528b40d",
            "compositeImage": {
                "id": "bf47c5ad-4a82-4691-90c5-0477bcaa9c79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7da4cbe8-9c83-42ec-bf82-b4407452ff48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3198fafc-8d0f-4218-8658-9527c6f9cfb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7da4cbe8-9c83-42ec-bf82-b4407452ff48",
                    "LayerId": "d27f61a8-d4f2-4bb5-8d3c-864724119541"
                }
            ]
        },
        {
            "id": "10033472-df91-4704-86bb-89aacd5ee592",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7309fd95-9da0-46bc-b4d9-a59cb528b40d",
            "compositeImage": {
                "id": "4f8f6de7-c326-490b-9341-d6260366691a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10033472-df91-4704-86bb-89aacd5ee592",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "deb742c8-6149-4a31-8998-3f253ce5bea2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10033472-df91-4704-86bb-89aacd5ee592",
                    "LayerId": "d27f61a8-d4f2-4bb5-8d3c-864724119541"
                }
            ]
        },
        {
            "id": "5086756c-e4cc-4066-9849-902b07d961f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7309fd95-9da0-46bc-b4d9-a59cb528b40d",
            "compositeImage": {
                "id": "f2ac1719-168a-4160-acb4-d63866943de0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5086756c-e4cc-4066-9849-902b07d961f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f62c182-a02f-4593-9cae-c3b170933f6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5086756c-e4cc-4066-9849-902b07d961f0",
                    "LayerId": "d27f61a8-d4f2-4bb5-8d3c-864724119541"
                }
            ]
        },
        {
            "id": "ec2616a9-ce77-4219-9af1-44caefa2ff8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7309fd95-9da0-46bc-b4d9-a59cb528b40d",
            "compositeImage": {
                "id": "95878ef8-7615-44f8-8070-c80e1b25c5ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec2616a9-ce77-4219-9af1-44caefa2ff8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "478bdb41-d382-4ada-8968-528f73a91911",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec2616a9-ce77-4219-9af1-44caefa2ff8a",
                    "LayerId": "d27f61a8-d4f2-4bb5-8d3c-864724119541"
                }
            ]
        },
        {
            "id": "f1231229-8ccc-46e1-b3d9-e0f1c2b66d3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7309fd95-9da0-46bc-b4d9-a59cb528b40d",
            "compositeImage": {
                "id": "759b0700-5d39-424b-98ac-0df5d8bfec4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1231229-8ccc-46e1-b3d9-e0f1c2b66d3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12e48987-b1e0-49f0-9ffa-5e1f25554721",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1231229-8ccc-46e1-b3d9-e0f1c2b66d3b",
                    "LayerId": "d27f61a8-d4f2-4bb5-8d3c-864724119541"
                }
            ]
        },
        {
            "id": "67201def-5347-4c5a-8722-c116a38b5c6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7309fd95-9da0-46bc-b4d9-a59cb528b40d",
            "compositeImage": {
                "id": "a6669cf7-0b92-43dc-a018-2765bf689961",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67201def-5347-4c5a-8722-c116a38b5c6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae7fd338-761b-4e99-b27e-9e6516a25123",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67201def-5347-4c5a-8722-c116a38b5c6d",
                    "LayerId": "d27f61a8-d4f2-4bb5-8d3c-864724119541"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d27f61a8-d4f2-4bb5-8d3c-864724119541",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7309fd95-9da0-46bc-b4d9-a59cb528b40d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 63,
    "yorig": 87
}