{
    "id": "2ddc6ca6-ef98-4d29-9f84-d7519d00d3df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lsyn_crouch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 50,
    "bbox_right": 76,
    "bbox_top": 52,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ef07b0ca-d13d-44ff-a7d0-c9d3d802c85b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ddc6ca6-ef98-4d29-9f84-d7519d00d3df",
            "compositeImage": {
                "id": "eab667d4-efb4-4618-9bc8-8e3d63ffb372",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef07b0ca-d13d-44ff-a7d0-c9d3d802c85b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b95cd276-3a61-425f-b5f7-6acbba64c6c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef07b0ca-d13d-44ff-a7d0-c9d3d802c85b",
                    "LayerId": "774a0af5-69e5-4007-956f-f3d9e9834e01"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "774a0af5-69e5-4007-956f-f3d9e9834e01",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ddc6ca6-ef98-4d29-9f84-d7519d00d3df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 76
}