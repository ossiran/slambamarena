{
    "id": "47b106da-f841-4d89-aabb-51ab0e339b20",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bill_cblock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 48,
    "bbox_right": 92,
    "bbox_top": 91,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05ca6fe5-a7dc-49c8-af8a-78ba82dc5a7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47b106da-f841-4d89-aabb-51ab0e339b20",
            "compositeImage": {
                "id": "65561549-6c07-4907-8c83-10c9f10bbf2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05ca6fe5-a7dc-49c8-af8a-78ba82dc5a7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbc3543d-c335-4fb6-a749-be62ad53a28a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05ca6fe5-a7dc-49c8-af8a-78ba82dc5a7b",
                    "LayerId": "698ea54e-3b16-4f9c-a176-3c2b4446b17f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "698ea54e-3b16-4f9c-a176-3c2b4446b17f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "47b106da-f841-4d89-aabb-51ab0e339b20",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 67,
    "yorig": 119
}