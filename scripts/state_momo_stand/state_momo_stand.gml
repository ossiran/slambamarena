live_call();
if(state_new == 1)
{
    with(hurtbox0)
	{
	    xoffset = -29;
	    yoffset = -36;
	    image_xscale  = 58;
	    image_yscale  = 68;
	    xshift  = -1;
	}
	
	with(hurtbox1)
	{
	    xoffset = -15;
	    yoffset = -61;
	    image_xscale  = 30;
	    image_yscale  = 25;
	    xshift = -3;
	}
	
	
	with(collisionbox)
	{
	    xoffset = -20;
	    yoffset = -22;
	    image_xscale  = 40;
	    image_yscale  = 54;
	    xshift = 0;
	}
}


image_xscale = -frontOrBack;
//walk
if(input_check(4,0,0,0,0) || input_check(6,0,0,0,0))
{
	state_switch("walk");
}

//crouch
if(input_check(1,0,0,0,0) || input_check(2,0,0,0,0) || input_check(3,0,0,0,0))
{
     state_switch("crouch");   
}

//friction
vx = Approach(vx,0,ground_fric*2);

//jab
if(input_check(0,1,0,0,0))
{
    state_switch("light");
}

//clight
if(input_check(2,1,0,0,0) || input_check(1,1,0,0,0) || input_check(3,1,0,0,0))
{
    state_switch("clight");
}

//medium
if(input_check(0,0,1,0,0))
{
    state_switch("medium");
}

//cmedium
if(input_check(2,0,1,0,0) || input_check(1,0,1,0,0) || input_check(3,0,1,0,0))
{
    state_switch("cmedium");
}

//heavy
if(input_check(0,0,0,1,0))
{
    state_switch("heavy");
}


//cheavy
if(input_check(2,0,0,1,0))
{
    state_switch("cheavy");
}

//charge back
if(chb() == 1)
{
    if(input_check(6,0,0,1,0))
    {
        if(ex > 49.9)
        {
            state_switch("chbh");
        }
        else
        {
            state_switch("chbm");
        }
    }
    else if(input_check(6,0,1,0,0))
    {
        state_switch("chbm");
    }
    else if(input_check(6,1,0,0,0))
    {
        state_switch("chbl");
    }
}


//qcb
if(qcb_buffer > 0)
{
    if(input_check(0,0,0,1,0))
    {
        if(ex > 24.9)
        {
            state_switch("qcbh");
        }
        else
        {
            state_switch("qcbm");
        }
    }
    else if(input_check(0,0,1,0,0))
    {
        state_switch("qcbm");
    }
    else if(input_check(0,1,0,0,0))
    {
        state_switch("qcbl");
    }
}

//charge down
if(chd() == 1)
{
    if(input_check(8,0,0,1,0))
    {
        if(ex > 49.9)
        {
            state_switch("chdh");
        }
        else
        {
            state_switch("chdm");
        }
    }
    else if(input_check(8,0,1,0,0))
    {
        state_switch("chdm");
    }
    else if(input_check(8,1,0,0,0))
    {
        state_switch("chdl");
    }
}

//dtap
if(dd_buffer > 0)
{
    if(input_check(0,0,0,1,0))
    {
        if(ex > 49.9)
        {
            state_switch("ddh");
        }
        else
        {
            state_switch("ddl");
        }
    }
    else if(input_check(0,0,1,0,0))
    {
        state_switch("ddl");
    }
    else if(input_check(0,1,0,0,0))
    {
        state_switch("ddl");
    }
}

//grab
if(input_check(0,1,0,0,1))
{
    state_switch("grab");
}

//dash
//fdash
if(ff_buffer > 0 || (input_check(6, 1, 1, 0, 0)) || (input_check(5, 1, 1, 0, 0)))
{
        state_switch("fdash");
}
//bdash
if(bb_buffer > 0 || (input_check(4, 1, 1, 0, 0)))
{
        state_switch("bdash");
}

//jump
if(input_check(7,0,0,0,0) || input_check(8,0,0,0,0) || input_check(9,0,0,0,0))
{
	state_switch("jumpsquat");
}


sprite_index = spr_momo_idle;


//air
if(!onground())
{
	state_switch("air");	
}

//hitstun
if(hitstun > 0)
{
    state_switch("hitstun");   
}

if(grabbed == 1)
{
 state_switch("grabbed");   
}


