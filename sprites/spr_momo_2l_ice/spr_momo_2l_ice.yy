{
    "id": "bd2056ab-e3c0-4c40-af89-46ca4eb9685f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_momo_2l_ice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6795aea8-849f-4d07-aaa6-92dd81660702",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd2056ab-e3c0-4c40-af89-46ca4eb9685f",
            "compositeImage": {
                "id": "38b6d3e7-c41b-42cb-9061-17d1e66a6267",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6795aea8-849f-4d07-aaa6-92dd81660702",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "938e5c5b-211f-4170-b744-9f319146913d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6795aea8-849f-4d07-aaa6-92dd81660702",
                    "LayerId": "502bdf82-8523-4f69-aed4-a5fb556fac8b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "502bdf82-8523-4f69-aed4-a5fb556fac8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bd2056ab-e3c0-4c40-af89-46ca4eb9685f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}